BIBASSE (Gestion du Budget)
===========================

1.Description générale
-----------------------

Le module BIBASSE de la sphère GFC du PGI Cocktail est le module de saisie du budget (provisoire, initial, reliquat, dbm) et de mise en place des crédits (ventilations et virements). BIBASSE permet de saisir un budget de gestion (par actions) et un budget par nature (comptes). La validation du budget saisi pour une UB ou un CR est réalisée en trois étapes : verrouillage, contrôle, clôture.

2.Principales fonctionnalités
-----------------------------

### Types de budget :

- Budget provisoire, initial, reliquat et DBM,
- Budget par nature et budget par destination LOLF (de gestion),
- Niveau de saisie : UB ou CR.

### Gestion du budget :

- Ouverture et saisie budget,
- Pilotage,
- Verrou, contrôle et vote,
- Répartition du budget après vote :Ventilations CR => sous-CR,
- Virements de crédits.

### Editions du budget :

- Budget de gestion établissement (global),
- Budget par nature établissement,
- Budget gestion / nature hors SACD,
- Budget gestion / nature par CR ou UB (selon vote),
- Budget RCE,
- Impression d'un élément ponctuel via organigramme

###### Bibasse needs more power

+ [Scala](http://www.scala-lang.org/),
+ [Akka Actor](http://akka.io/),
+ [Play2 Framework](http://www.playframework.com/),
+ [AngularJS](http://angularjs.org/)

