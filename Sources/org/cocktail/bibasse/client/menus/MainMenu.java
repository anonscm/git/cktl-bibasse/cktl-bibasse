/* MainMenu.java created by cpinsard on Fri 11-Apr-2003 */
package org.cocktail.bibasse.client.menus;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.LimitativiteCtrl;
import org.cocktail.bibasse.client.Parametrage;
import org.cocktail.bibasse.client.masques.MasqueSaisie;
import org.cocktail.bibasse.client.utils.ConnectedUsersCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class MainMenu extends JMenuBar 
{
	ApplicationClient NSApp;

	JMenu	menuApplication, menuAgent, menuOutils, menuContrat, menuEditions, menuBudget, menuParametrage;

	JMenuItem		itemMasques, itemParametrage, itemChangerSaisieBudget, itemConsolider, itemConsoliderTousBudgets, itemLimitativite;
	JMenuItem		itemConnectedUsers;

	protected EOEditingContext ec;

	/** Constructeur */
	public MainMenu(EOEditingContext globalEc, ApplicationClient app)	{
		
		NSApp = app;
		ec = globalEc;

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("setMenuEnabled",new Class[] {NSNotification.class}), "NotifSetMenuEnabled",null);

		menuApplication = new JMenu("Application");
		menuParametrage = new JMenu("Paramétrages");
		menuEditions = new JMenu("Editions");
		menuOutils = new JMenu("Outils");

		// Application

		itemMasques = new JMenuItem(new ActionMasque("Masques de Saisie")); 
		itemParametrage = new JMenuItem(new ActionParametrage("Paramètres Généraux")); 
		itemChangerSaisieBudget = new JMenuItem(new ActionChangerSaisieBudget("Modifier le type de budget")); 

		itemLimitativite = new JMenuItem(new ActionLimitativite("Limitativité des comptes")); 

		itemConsolider = new JMenuItem(new ActionConsolider("Consolider le budget")); 

		itemConsoliderTousBudgets = new JMenuItem(new ActionConsoliderBudgetsVotes("Consolider TOUS les budgets")); 

		itemConnectedUsers = new JMenuItem(new ActionConnectedUsers()); 

		menuApplication.add(itemConnectedUsers);
		menuApplication.add(new JMenuItem(new ActionQuitter("Quitter")));

		menuParametrage.add(itemParametrage);
		menuParametrage.add(itemMasques);
		menuParametrage.add(itemLimitativite);

		menuOutils.add(itemConsolider);
		menuOutils.add(itemConsoliderTousBudgets);
		menuOutils.add(itemChangerSaisieBudget);

		// Ajout des menus
		add(menuApplication);
		add(menuParametrage);
		//add(menuEditions);
		add(menuOutils);

		add(new MenuAide(NSApp, "?"));

	}

	public void updateMenus()	{
		
		itemParametrage.setEnabled(NSApp.fonctionParametrage());
	//	itemMasques.setEnabled(NSApp.fonctionMasque());

	}
	
	public void setMenuEnabled(NSNotification notif)	{}
	
	public void setAllMenusEnabled(boolean yn)	{}	

/** Quitte l'application */
	public class ActionQuitter extends AbstractAction
	{
		public ActionQuitter (String name)
		{
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_QUIT);
		}

		public void actionPerformed(ActionEvent event)
		{
			NSApp.superviseur().quitterApplication();
		}
	}
	
	
	/** Quitte l'application */
	private class ActionLimitativite extends AbstractAction	{
		public ActionLimitativite (String name)	{
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_PARAMETRES);
		}

		public void actionPerformed(ActionEvent event)	{
			LimitativiteCtrl.sharedInstance(ec).open();
		}
	}
	
	
	/** Quitte l'application */
	private class ActionParametrage extends AbstractAction	{
		public ActionParametrage (String name)
		{
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_PARAMETRES);
		}

		public void actionPerformed(ActionEvent event)
		{
			Parametrage.sharedInstance(ec, NSApp.superviseur().mainFrame()).updateParametres();
		}
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ActionConsolider extends AbstractAction	{
		public ActionConsolider (String name)
		{
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CONTROLE_BUDGET);
		}

		public void actionPerformed(ActionEvent event)	{
			
			NSApp.superviseur().consoliderBudget();
			
		}
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ActionConsoliderBudgetsVotes extends AbstractAction	{
		public ActionConsoliderBudgetsVotes (String name)
		{
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CONTROLE_BUDGET);
		}

		public void actionPerformed(ActionEvent event)	{
			
			NSApp.superviseur().consoliderBudgetsVotes();
			
		}
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ActionMasque extends AbstractAction	{
		public ActionMasque (String name)
		{
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
		}

		public void actionPerformed(ActionEvent event)
		{
			MasqueSaisie.sharedInstance(ec).open();
		}
	}
	
	/** */
	private class ActionChangerSaisieBudget extends AbstractAction	{
		public ActionChangerSaisieBudget (String name) {
			putValue(Action.NAME,name);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SELECT);
		}

		public void actionPerformed(ActionEvent event)	{
			
			NSApp.superviseur().changerTypeSaisieBudget();
			
		}
	}
	
	   private final class ActionConnectedUsers extends AbstractAction {
	       public ActionConnectedUsers() {
	           super("Utilisateurs connectés");
	           //            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
	       }

	       public void actionPerformed(ActionEvent e) {
	           ConnectedUsersCtrl win = new ConnectedUsersCtrl(ec);
	           try {
	               win.openDialog(NSApp.getMainWindow(), true);
	           } catch (Exception e1) {
	        	  e1.printStackTrace();
	               EODialogs.runErrorDialog("ERREUR", e1.getMessage());
	           }
	       }
	   } 

}