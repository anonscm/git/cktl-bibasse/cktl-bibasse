package org.cocktail.bibasse.client.menus;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.cocktail.bibasse.client.ApplicationClient;

public class MenuAide extends JMenu
{
	public ApplicationClient NSApp;

	protected JMenuItem itemVersion, itemForums, itemSiteWeb, itemLogs;

	/** Constructeur */
	public MenuAide (ApplicationClient app, String titre)
	{
		super (titre);
		NSApp = app;
		initObject();
	}

	/** */
	public void initObject()	{

		//itemForums = new JMenuItem(new ActionForums("Forums"));
		//itemSiteWeb = new JMenuItem(new ActionSiteWeb("Documentation / Site Web"));

		itemLogs = new JMenuItem(new ActionLogs("Logs Client / Serveur"));

		//add(itemForums);
		//add(itemSiteWeb);
		add(itemLogs);
	}


/** */
	public class ActionForums extends AbstractAction
	{
		public ActionForums (String name)
		{
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)
		{
			NSApp.openURL("http://www.univ-lr.fr/apps/forums/wa/forum?m=forum-appli-jefyco");

		}
	}

/** */
	public class ActionSiteWeb extends AbstractAction
	{
		public ActionSiteWeb (String name)
		{
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)
		{
			NSApp.openURL("http://www.univ-lr.fr/apps/support/bibasse/");
		}
	}

	/** */
	public class ActionLogs extends AbstractAction	{
		public ActionLogs (String name)
		{
			putValue(Action.NAME,name);
		}

		public void actionPerformed(ActionEvent event)
		{
			NSApp.showLogs();
		}
	}

}
