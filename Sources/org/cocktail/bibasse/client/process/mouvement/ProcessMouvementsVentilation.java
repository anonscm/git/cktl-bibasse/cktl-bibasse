/*
 * @author RIVALLAND FREDERIC <br>
 *                URD P5 <br>
 *                DSI 
 *  
 */
package org.cocktail.bibasse.client.process.mouvement;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOTypeSens;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;



public class ProcessMouvementsVentilation extends ProcessMouvements {

    /**
     * @param withLog
     */
    public ProcessMouvementsVentilation(boolean withLog) {
        super(withLog);
    }

 
    /**
     * 
     * @param monEOEditingContext
     * @param setUtilisateur
     * @param setTypeMouvementBudgetaire
     * @param setExercice
     * @param setTypeEtatValide
     * @param setBmouLibelle
     * @param setBmouDateCreation
     * @return
     * @throws Exception
     */
	public EOBudgetMouvements creerMouvementVentilation(EOEditingContext monEOEditingContext,
			EOUtilisateur setUtilisateur,
			EOTypeMouvementBudgetaire setTypeMouvementBudgetaire,
			EOExercice setExercice,
			EOTypeEtat setTypeEtatValide,
			String setBmouLibelle,
			NSTimestamp setBmouDateCreation) throws Exception{

		EOBudgetMouvements monEOBudgetMouvements =null;

		if (setTypeMouvementBudgetaire.tymbLibelle().equals(EOTypeMouvementBudgetaire.MOUVEMENT_VENTILATION))
		{
			monEOBudgetMouvements =
				this.creerMouvement(
						monEOEditingContext,
						setUtilisateur,
						setTypeMouvementBudgetaire,
						setExercice,
						setTypeEtatValide,
						setBmouLibelle,
						setBmouDateCreation);
		}else
			throw new Exception(EOTypeMouvementBudgetaire.PROBLEME_TYPE_VENTILATION);


		return monEOBudgetMouvements;
	}

	
	/**
	 * 
	 * @param monEOEditingContext
	 * @param monEOBudgetMouvements
	 * @param setUtilisateur
	 * @param setTypeEtatAnnuler
	 * @param lesCredits
	 * @throws Exception
	 */
	public void annulerMouvementVentilation ( 
			EOEditingContext monEOEditingContext,
			EOBudgetMouvements monEOBudgetMouvements,
			EOUtilisateur setUtilisateur,
			EOTypeEtat setTypeEtatAnnuler,
			NSArray lesCredits
			) throws Exception
			{
		this.annulerMouvement( 
				monEOEditingContext,
				 monEOBudgetMouvements,
				 setUtilisateur,
				 setTypeEtatAnnuler,
				 lesCredits,
				 null,
				 null
				 );
			}
	
	
	/**
	 * 
	 * @param monEOEditingContext
	 * @param setTypeSens
	 * @param setTypeCredit
	 * @param setOrgan
	 * @param setBudgetMouvements
	 * @param setExercice
	 * @param setUtilisateur
	 * @param setTypeEtat
	 * @param setBdmcMontant
	 * @return
	 * @throws Exception
	 */
	public  EOBudgetMouvCredit creerUneLigneEOBudgetMouvCreditVentilation(
			EOEditingContext monEOEditingContext,
            EOTypeSens setTypeSens,
            EOTypeCredit setTypeCredit,
            EOOrgan setOrgan,
            EOBudgetMouvements setBudgetMouvements,
            EOExercice setExercice,
            EOUtilisateur setUtilisateur,
            EOTypeEtat setTypeEtat,
            BigDecimal setBdmcMontant) throws Exception
            {
		
		return this.creerUneLigneEOBudgetMouvCredit(
				 monEOEditingContext,
	             setTypeSens,
	             setTypeCredit,
	             setOrgan,
	             setBudgetMouvements,
	             setExercice,
	             setUtilisateur,
	             setTypeEtat,
	             setBdmcMontant);
            }
	
	
	/**
	 * 
	 * @param monEOEditingContext
	 * @param setBudgetMouvCredit
	 * @throws Exception
	 */
	public  void annulerUneLigneEOBudgetMouvCreditVentilation(
			EOEditingContext monEOEditingContext,
			EOBudgetMouvCredit setBudgetMouvCredit
) throws Exception
	{

 }
}
