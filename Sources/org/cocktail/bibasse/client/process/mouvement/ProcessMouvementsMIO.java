package org.cocktail.bibasse.client.process.mouvement;

import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class ProcessMouvementsMIO  extends ProcessMouvements {

	
    public ProcessMouvementsMIO(boolean withLog) {
        super( withLog);
    }
    

	public EOBudgetMouvements creerMouvementMIO(EOEditingContext monEOEditingContext,
			EOUtilisateur setUtilisateur,
			EOTypeMouvementBudgetaire setTypeMouvementBudgetaire,
			EOExercice setExercice,
			EOTypeEtat setTypeEtatValide,
			String setBmouLibelle,
			NSTimestamp setBmouDateCreation) throws Exception{

		EOBudgetMouvements monEOBudgetMouvements =null;

		if (setTypeMouvementBudgetaire.tymbLibelle().equals(EOTypeMouvementBudgetaire.MOUVEMENT_MIO))
		{
			monEOBudgetMouvements =
				this.creerMouvement(
						monEOEditingContext,
						setUtilisateur,
						setTypeMouvementBudgetaire,
						setExercice,
						setTypeEtatValide,
						setBmouLibelle,
						setBmouDateCreation);
		}else
			throw new Exception(EOTypeMouvementBudgetaire.PROBLEME_TYPE_MIO);


		return monEOBudgetMouvements;
	}
	
}
