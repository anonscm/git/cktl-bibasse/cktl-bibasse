/*
 * @author RIVALLAND FREDERIC <br>
 *                URD P5 <br>
 *                DSI 
 *  
 */
package org.cocktail.bibasse.client.process.mouvement;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.factory.FactoryBudgetMouvements;
import org.cocktail.bibasse.client.factory.FactoryBudgetMouvementsCredit;
import org.cocktail.bibasse.client.factory.FactoryBudgetMouvementsGestion;
import org.cocktail.bibasse.client.factory.FactoryBudgetMouvementsNature;
import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvGestion;
import org.cocktail.bibasse.client.metier.EOBudgetMouvNature;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOTypeSens;
import org.cocktail.bibasse.client.metier.EOUtilisateur;
import org.cocktail.bibasse.client.process.Process;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class  ProcessMouvements  extends Process {

	public ProcessMouvements(boolean withLog) {
		super(withLog);

	}



	public EOBudgetMouvements creerMouvement(EOEditingContext monEOEditingContext,
			EOUtilisateur setUtilisateur,
			EOTypeMouvementBudgetaire setTypeMouvementBudgetaire,
			EOExercice setExercice,
			EOTypeEtat setTypeEtatValide,
			String setBmouLibelle,
			NSTimestamp setBmouDateCreation) throws Exception{

		FactoryBudgetMouvements  maFactoryBudgetMouvements = new FactoryBudgetMouvements(withLogs());
		EOBudgetMouvements monEOBudgetMouvements =
			maFactoryBudgetMouvements.creerEOBudgetMouvements(
					monEOEditingContext,
					setUtilisateur,
					setTypeMouvementBudgetaire,
					setExercice,
					setTypeEtatValide,
					setBmouLibelle,
					setBmouDateCreation);
		return monEOBudgetMouvements;
	}


	protected void annulerMouvement ( EOEditingContext monEOEditingContext,
			EOBudgetMouvements monEOBudgetMouvements,
			EOUtilisateur setUtilisateur,
			EOTypeEtat setTypeEtatAnnuler,
			NSArray lesCredits,
			NSArray lesNatures,
			NSArray lesGestions
	) throws Exception
	{

		// verifier que le mouvement n est pas CLOT ou TRAITE ou ANNULE
		if (monEOBudgetMouvements.typeEtat().tyetLibelle().equals(EOTypeEtat.ERROR_MESS_ETAT_CLOT))
			throw new Exception(EOTypeEtat.ERROR_MESS_ETAT_NON_CLOT);

		if (monEOBudgetMouvements.typeEtat().tyetLibelle().equals(EOTypeEtat.ERROR_MESS_ETAT_TRAITE))
			throw new Exception(EOTypeEtat.ERROR_MESS_ETAT_NON_TRAITE);

		if (monEOBudgetMouvements.typeEtat().tyetLibelle().equals(EOTypeEtat.ERROR_MESS_ETAT_ANNULE))
			throw new Exception(EOTypeEtat.ERROR_MESS_ETAT_NON_ANNULE);


		if (setTypeEtatAnnuler.tyetLibelle().equals(EOTypeEtat.ETAT_ANNULE) )	{

			// creation des factory
			FactoryBudgetMouvements mafactoryBudgetMouvements = null;
			mafactoryBudgetMouvements = new FactoryBudgetMouvements(withLogs());

			// on annule le mouvement
			mafactoryBudgetMouvements.annulerEOBudgetMouvements  (
					monEOEditingContext,
					monEOBudgetMouvements,
					setUtilisateur,
					setTypeEtatAnnuler
			);
		}else
			throw new Exception(EOTypeEtat.ERROR_MESS_ETAT_NON_ANNULE);

		// on annule les descriptions gestion nature et fonctionnement
		int i = 0;
		if (lesCredits != null )
		{for (i=0;i<lesCredits.count();i++)
		{
			this.annulerUneLigneEOBudgetMouvCredit(monEOEditingContext,(EOBudgetMouvCredit)lesCredits.objectAtIndex(i));
		}
		}
		
		if (lesNatures!= null )
		{for (i=0;i<lesNatures.count();i++)
		{
			this.annulerUneLigneEOBudgetMouvNature(monEOEditingContext,(EOBudgetMouvNature)lesNatures.objectAtIndex(i));
		}
		}
		
		if (lesGestions != null )
		{for (i=0;i<lesGestions.count();i++)
		{
			this.annulerUneLigneEOBudgetMouvGestion(monEOEditingContext,(EOBudgetMouvGestion)lesCredits.objectAtIndex(i));
		}
		}
	}


	public  EOBudgetMouvCredit creerUneLigneEOBudgetMouvCredit(
			EOEditingContext monEOEditingContext,
			EOTypeSens setTypeSens,
			EOTypeCredit setTypeCredit,
			EOOrgan setOrgan,
			EOBudgetMouvements setBudgetMouvements,
			EOExercice setExercice,
			EOUtilisateur setUtilisateur,
			EOTypeEtat setTypeEtat,
			BigDecimal setBdmcMontant) throws Exception
			{
		EOBudgetMouvCredit newEOBudgetMouvCredit = null;
		FactoryBudgetMouvementsCredit maFactoryBudgetMouvementsCredit = null;

		maFactoryBudgetMouvementsCredit= new FactoryBudgetMouvementsCredit(withLogs());
		newEOBudgetMouvCredit=maFactoryBudgetMouvementsCredit.creerEOBudgetMouvementsCredit
		(
				monEOEditingContext,
				setTypeSens,
				setTypeCredit,
				setOrgan,
				setBudgetMouvements,
				setExercice,
				setTypeEtat,
				setBdmcMontant
		);     
		//newEOBudgetMouvCredit.addUtilisateur(newEOBudgetMouvCredit, setUtilisateur);

		return newEOBudgetMouvCredit;
			}

	public  EOBudgetMouvGestion creerUneLigneEOBudgetMouvGestion(
			EOEditingContext monEOEditingContext,
			EOTypeSens setTypeSens,
			EOTypeCredit setTypeCredit,
			EOTypeAction setTypeAction,
			EOOrgan setOrgan,
			EOBudgetMouvements setBudgetMouvements,
			EOExercice setExercice,
			EOUtilisateur setUtilisateur,
			EOTypeEtat setTypeEtat,
			BigDecimal setBdmgMontant) throws Exception
			{
		EOBudgetMouvGestion newEOBudgetMouvGestion = null;
		FactoryBudgetMouvementsGestion maFactoryBudgetMouvementsGestion = null;

		maFactoryBudgetMouvementsGestion= new FactoryBudgetMouvementsGestion(withLogs());
		newEOBudgetMouvGestion=maFactoryBudgetMouvementsGestion.creerEOBudgetMouvementsGestion
		(
				monEOEditingContext,
				setTypeSens,
				setTypeCredit,
				setTypeAction,
				setOrgan,
				setBudgetMouvements,
				setExercice,
				setTypeEtat,
				setBdmgMontant
		);     

		return newEOBudgetMouvGestion;
			}


	public  EOBudgetMouvNature creerUneLigneEOBudgetMouvNature(
			EOEditingContext monEOEditingContext,
			EOTypeSens setTypeSens,
			EOTypeCredit setTypeCredit,
			EOPlanComptable setPlanComptable,
			EOOrgan setOrgan,
			EOBudgetMouvements setBudgetMouvements,
			EOExercice setExercice,
			EOUtilisateur setUtilisateur,
			EOTypeEtat setTypeEtat,
			BigDecimal setBdmnMontant) throws Exception
			{
		EOBudgetMouvNature newEOBudgetMouvNature = null;
		FactoryBudgetMouvementsNature maFactoryBudgetMouvementsNature = null;

		maFactoryBudgetMouvementsNature= new FactoryBudgetMouvementsNature(withLogs());
		newEOBudgetMouvNature=maFactoryBudgetMouvementsNature.creerEOBudgetMouvementsNature
		(
				monEOEditingContext,
				setTypeSens,
				setTypeCredit,
				setPlanComptable,
				setOrgan,
				setBudgetMouvements,
				setExercice,
				setTypeEtat,
				setBdmnMontant
		);   

		return newEOBudgetMouvNature;
			}



	protected  void annulerUneLigneEOBudgetMouvCredit(
			EOEditingContext monEOEditingContext,
			EOBudgetMouvCredit setBudgetMouvCredit
	) throws Exception
	{

	}



	protected  void annulerUneLigneEOBudgetMouvNature(
			EOEditingContext monEOEditingContext,
			EOBudgetMouvNature setBudgetMouvNature
	) throws Exception
	{

	}



	protected  void annulerUneLigneEOBudgetMouvGestion(
			EOEditingContext monEOEditingContext,
			EOBudgetMouvGestion setBudgetMouvGestion
	) throws Exception
	{

	}

}
