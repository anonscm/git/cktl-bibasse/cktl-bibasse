/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.process;

import java.math.BigDecimal;
import java.util.Iterator;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

public class Process {

    boolean withLogs;
    
    public Process() {
        super();
        setWithLogs(false);
    }
    
    public Process(boolean logs) {
        super();
        setWithLogs(logs);
    }



    public void trace(final String prefix, final NSArray a) {
        for (int i = 0; i < a.count(); i++) {
            trace(prefix +"  "+i+"-->" , a.objectAtIndex(i));
        }
    }

    
//    public void trace(String m) {
//        if (withLogs())
//            System.out.println(m);
//    }
//    
//    public void trace(final String prefix, String m) {
//        if (withLogs())
//            System.out.println(m);
//    }
//    
    public void trace(final Object obj) { 
        trace("", obj);
    }
    
    public void trace(final String prefix, final Object obj) {
        if (withLogs()) {
            if (obj==null) {
                System.out.println(prefix+"null");
            }
            else {
                if (obj instanceof NSArray) {
                    trace (prefix, (NSArray)obj);
                }
                else if (obj instanceof EOEnterpriseObject) {
                    trace (prefix, (EOEnterpriseObject)obj);
                }
                else {
                    System.out.println (prefix + obj.toString());
                }
            }
            
        }
    }    
    


    
    public void trace(final String prefix, final EOEnterpriseObject object) {
        if (object!=null) {
            final Iterator iter = object.attributeKeys().vector().iterator();
            while (iter.hasNext()) {
                String obj = (String) iter.next();
                trace(prefix+"  "+ obj  +"-->", object.valueForKey(obj) );
            }

            Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
            while (iter2.hasNext()) {
                String obj = (String) iter2.next();
                trace(prefix+"  "+ obj  +"-->" + object.valueForKey(obj) );
            }
            Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
            while (iter3.hasNext()) {
                String obj = (String) iter3.next();
                if (prefix != null && prefix.length()>250) {
                    trace(prefix+"  "+ obj  +"-->" + object.valueForKey(obj) );
                }
                else {
                    trace(prefix+"  "+ obj  +"-->" , object.valueForKey(obj) );
                }
                
            }
        }
    }    
    
    /**
     * @param withLogs  The withLogs to set.
     * @uml.property  name="withLogs"
     */
    public void setWithLogs(boolean b) {
        this.withLogs = b;
    }

    public boolean withLogs() {
        return withLogs;
    }
    
    
    public BigDecimal computeSumForKeyBigDecimal(NSArray eo, String keyBigDecimal) {

        BigDecimal total = new BigDecimal(0.0);
        int i = 0;
        while (i < eo.count()) {
            total = total.add((BigDecimal) ((EOEnterpriseObject) eo.objectAtIndex(i)).valueForKey(keyBigDecimal));
            i = i + 1;
        }
        return total;

    }
    
    public static boolean inferieur(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean inferieurOuEgal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean superieur(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    public static boolean superieurOuEgal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

    public static boolean egal(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = false;

        if (resultat == 0)
            reponse = true;

        if (resultat == 1)
            reponse = false;

        return reponse;
    }

    public static boolean different(BigDecimal a, BigDecimal b) {
        boolean reponse = false;

        int resultat = a.compareTo(b);

        if (resultat == -1)
            reponse = true;

        if (resultat == 0)
            reponse = false;

        if (resultat == 1)
            reponse = true;

        return reponse;
    }

}
