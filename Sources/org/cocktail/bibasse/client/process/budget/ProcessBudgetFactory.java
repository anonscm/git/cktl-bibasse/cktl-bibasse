/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.process.budget;

import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class ProcessBudgetFactory extends ProcessBudget {
    
    
    public final static String PROCESS_PROVISOIRE = "PROCESS_PROVISOIRE";
    public final static String PROCESS_INITIAL = "PROCESS_INITIAL";
    public final static String PROCESS_RELIQUAT = "PROCESS_RELIQUAT";
    public final static String PROCESS_DBM = "PROCESS_DBM";
    public final static String PROCESS_CONVENTION = "PROCESS_CONVENTION";
    public final static String PROCESS_MIO = "PROCESS_MIO";
    
    final static String PROBLEME_TYPE_PROCESS = "PROBLEME LE TYPE DE PROCESS N EST PAS POSITIONNE";
    
    String processType =null;
    
    /** Permet de creer un controleur pour gerer une saisie budgetaire
     * 
     * @param withLog afficher les logs 
     * @param typeProcess definir sur quelle type de saisie travail le controleur.
     */
    public ProcessBudgetFactory(boolean withLog, String typeProcess) {
        super(withLog);
        this.setProcessType(typeProcess);
    }
    
    /** Permet de creer un EOBudgetSaisie
     * 
     * @param monEOEditingContext editingb context de travail
     * @param setTypeEtatValide etat valide
     * @param setTypeSaisie definir le type de saisie DBM,INIT...
     * @param setExercice exercice 
     * @param setBdsaLibelle libelle de cette saisie
     * @param setUtilisateur utilisateur createur
     * @return une saisie
     * @throws Exception
     */
    public EOBudgetSaisie ajouterEOBudgetSaisie (
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatValide,
            EOTypeSaisie setTypeSaisie,
            EOExercice setExercice,
            String setBdsaLibelle,
            EOUtilisateur setUtilisateur
    ) throws Exception
    {
        
        EOBudgetSaisie monEOBudgetSaisie=null;
        
        if (getProcessType().equals(PROCESS_CONVENTION))
        {
            // VERIFICATIONS
            verifierSaisieConvention(setTypeSaisie);
            // traitement
            monEOBudgetSaisie=this.creerUnEOBudgetSaisiePourConvention(
                    monEOEditingContext,
                    setTypeEtatValide,
                    setTypeSaisie,
                    setExercice,
                    setBdsaLibelle,
                    setUtilisateur);
        }
        else
        {
            // VERIFICATIONS
            if ( getProcessType().equals(PROCESS_PROVISOIRE))
                verifierSaisieProvisoire(setTypeSaisie);
            
            
            if ( getProcessType().equals(PROCESS_INITIAL))
                verifierSaisieInitial(setTypeSaisie);
            
            
            if ( getProcessType().equals(PROCESS_RELIQUAT))
                verifierSaisieReliquat(setTypeSaisie);
            
            
            if ( getProcessType().equals(PROCESS_DBM))
                verifierSaisieDbm(setTypeSaisie);
            
            
            if ( getProcessType().equals(PROCESS_MIO))
                verifierSaisieMio(setTypeSaisie);
            
            
            // traitement
            monEOBudgetSaisie=this.creerUnEOBudgetSaisieHorsConvention(
                    monEOEditingContext,
                    setTypeEtatValide,
                    setTypeSaisie,
                    setExercice,
                    setBdsaLibelle,
                    setUtilisateur);
        }
        
        return monEOBudgetSaisie;
    }
    
    /** Permet d'annuler une saisie budgetaire
     * 
     * @param monEOEditingContext editing c ontext de travail
     * @param monEOBudgetSaisie EOBudgetSaisie a annuler
     * @param setUtilisateur utilisateur qui annule
     * @param setTypeEtatAnnule etat annule
     * @throws Exception problemes d'etat
     */
    public  void annulerEOBudgetSaisie(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisie monEOBudgetSaisie,
            EOUtilisateur setUtilisateur,
            EOTypeEtat setTypeEtatAnnule
    ) throws Exception
    {
        // factoryBudgetSaisie
        this.annulerUnEOBudgetSaisie(
                monEOEditingContext,
                monEOBudgetSaisie,
                setUtilisateur,
                setTypeEtatAnnule);
    }
    
    /** permet de cloturer une saisie budgetaire
     * 
     * @param monEOEditingContext editing context de travail
     * @param setTypeEtatClot etat clot
     * @param monEOBudgetSaisie EOBudgetSaisie a cloturer
     * @param setUtilisateurValidation utilisateur qui cloture
     * @throws Exception problemes d etat
     */
    public  void cloturerEOBudgetSaisie (
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatClot,
            EOBudgetSaisie  monEOBudgetSaisie,
            EOUtilisateur setUtilisateurValidation
    ) throws Exception
    {
        this.cloturerUnEOBudgetSaisie(
                monEOEditingContext,
                setTypeEtatClot,
                monEOBudgetSaisie,
                setUtilisateurValidation);
    }
    
    
    /** Permet de verifier la bonne saisie des budgets de gestion et par nature
     *  des de la saisie en cours
     *  
     * @param monEOEditingContext editing context de travail
     * @param monEOBudgetSaisie EOBudgetSaisie en cours de verification
     * @param arrayEOTypeCreditDepense les types de credit pour la depense
     * @param arrayEOBudgetSaisieGestion les lignes des budgets de gestion
     * @param arrayEOBudgetSaisieNature les lignes des budgets par nature
     * @throws Exception probleme d'etat,convention ,etc....
     */
    public  void verifierEOBudgetSaisieAvantVote (
            EOEditingContext monEOEditingContext,
            EOBudgetSaisie  monEOBudgetSaisie,
            NSArray arrayEOTypeCreditDepense,
            NSArray arrayEOBudgetSaisieGestion,
            NSArray arrayEOBudgetSaisieNature,
            EOBudgetSaisieNature  resultatBrutDepenseCompte,
            EOBudgetSaisieNature  resultatBrutRecetteCompte,
            EOBudgetSaisieNature  cafCompte,
            EOBudgetSaisieNature  iafCompte, 
            EOBudgetSaisieNature  augmentationFDRCompte,
            EOBudgetSaisieNature  diminutionFDRCompte
    ) throws Exception
    {
        this.verificationEOBudgetSaisieAvantVote(
                monEOEditingContext,
                monEOBudgetSaisie,
                arrayEOTypeCreditDepense,
                arrayEOBudgetSaisieGestion,
                arrayEOBudgetSaisieNature,
                resultatBrutDepenseCompte,
                resultatBrutRecetteCompte,
                cafCompte,
                iafCompte, 
                augmentationFDRCompte,
                diminutionFDRCompte);
        
        if ( getProcessType().equals(PROCESS_MIO))
            verificationMIO(arrayEOBudgetSaisieNature);
    }
    
    
    /** Permet d'annuler  des budgets de gestion et par nature pour une EOBudgetSaisie
     * 
     * @param monEOEditingContext editing context de travail
     * @param setTypeEtatAnnule etat annuler
     * @param arrayBudgetSaisieGestion les lignes du du budget de gestion a annuler
     * @param arrayBudgetSaisieNature les lignes du budget par nature a annuler
     * @param setUtilisateur utilisateur qui annule
     * @throws Exception 
     */
    public void annulerSaisieDesBudgetsPourOrgan (
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatAnnule,
            NSArray arrayBudgetSaisieGestion,
            NSArray arrayBudgetSaisieNature,
            EOUtilisateur setUtilisateur 
    ) throws Exception
    {
        // traitement
        this.annulerLesBudgets(
                monEOEditingContext,
                setTypeEtatAnnule,
                arrayBudgetSaisieGestion,
                arrayBudgetSaisieNature,
                setUtilisateur) ;
    }
    
    /** Permet de cloturer  des budgets de gestion et par nature pour une EOBudgetSaisie
     * 
     * @param monEOEditingContext editing context de travail
     * @param setTypeEtatAnnule etat CLOT
     * @param arrayBudgetSaisieGestion les lignes du du budget de gestion a cloturer
     * @param arrayBudgetSaisieNature les lignes du budget par nature a cloturer
     * @param setUtilisateur utilisateur qui cloture
     * @throws Exception 
     */
    public void cloturerSaisieDesBudgetsPourOrgan (
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatClot,
            NSArray arrayBudgetSaisieGestion,
            NSArray arrayBudgetSaisieNature,
            EOUtilisateur setUtilisateur 
    ) throws Exception
    {
        // VERIFICATIONS
        
        // traitement
        this.cloturerLesBudgets(
                monEOEditingContext,
                setTypeEtatClot,
                arrayBudgetSaisieGestion,
                arrayBudgetSaisieNature,
                setUtilisateur ) ;
    }
    
    
    /** Permet d'ajouter (initialiser) )des lignes de saisie budgetaire nature et gestion 
     * 
     * @param monEOEditingContext editing context de travail
     * @param setTypeEtatValide etat VALIDE
     * @param arrayPlancoCreditBudget les comptes d'imputation et type de credit pour le budget par nature
     * @param arrayTypeCreditD les type de credit depense
     * @param arrayActionsD les actions lolfs depense
     * @param arrayTypeCreditR les type de credit recette
     * @param arrayActionsR les actions lolfs recette
     * @param setTypeSaisie type de saisie
     * @param setOrgan ligne budgetaire de la saisie
     * @param setExercice exercice de saisie
     * @param setBdsaLibelle libelle de la saisie
     * @param setUtilisateur utilisateur qui saisie
     * @param monEOBudgetSaisie BudgetSaisie en cours de modification
     * @throws Exception
     */
    public void ajouterSaisieDesBudgetsPourOrgan
    (
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatValide,
            NSArray arrayPlancoCreditBudget,
            NSArray arrayTypeCreditD,
            NSArray arrayActionsD,
            NSArray arrayTypeCreditR,
            NSArray arrayActionsR,
            EOTypeSaisie setTypeSaisie,
            EOOrgan setOrgan,
            EOExercice setExercice,
            String setBdsaLibelle,
            EOUtilisateur setUtilisateur ,
            EOBudgetSaisie monEOBudgetSaisie
    ) throws Exception
    {
        // VERIFICATIONS
        if (getProcessType().equals(PROCESS_CONVENTION))
            verifierSaisieConvention(setTypeSaisie);
        
        
        if ( getProcessType().equals(PROCESS_PROVISOIRE))
            verifierSaisieProvisoire(setTypeSaisie);
        
        
        if ( getProcessType().equals(PROCESS_INITIAL))
            verifierSaisieInitial(setTypeSaisie);
        
        
        if ( getProcessType().equals(PROCESS_RELIQUAT))
            verifierSaisieReliquat(setTypeSaisie);
        
        
        if ( getProcessType().equals(PROCESS_DBM))
            verifierSaisieDbm(setTypeSaisie);
        
        
        if ( getProcessType().equals(PROCESS_MIO))
            verifierSaisieMio(setTypeSaisie);
        
        
        // traitement
        this.ajouterLesBudgets(
                monEOEditingContext,
                setTypeEtatValide,
                arrayPlancoCreditBudget,
                arrayTypeCreditD,
                arrayActionsD,
                arrayTypeCreditR,
                arrayActionsR,
                setTypeSaisie,
                setOrgan,
                setExercice,
                setBdsaLibelle,
                setUtilisateur ,
                monEOBudgetSaisie) ;
    }
    
    /** Permet de verifier la bonne saisie ou modification des montant
     * pour le budget provisoire,initial et reliquat : montant toujours positifs
     * 
     * @param arrayEOBudgetSaisieGestion les lignes du budget de gestion
     * @param arrayEOBudgetSaisieNature les lignes du budget par nature
     * @throws Exception probleme de montants negatifs
     */
    public  void verifierLesSaisiesMontantsPositifs (
            NSArray  arrayEOBudgetSaisieGestion,
            NSArray  arrayEOBudgetSaisieNature ) throws Exception
            {
        // VERIFICATIONS DES BUDGET OBLIGATOIREMENTS DE MONTANT POSITIF 
        if ( getProcessType().equals(PROCESS_PROVISOIRE))
            this.verifierLesBudgetsPositifs(arrayEOBudgetSaisieGestion,arrayEOBudgetSaisieNature);
        
        if ( getProcessType().equals(PROCESS_INITIAL))
            this.verifierLesBudgetsPositifs(arrayEOBudgetSaisieGestion,arrayEOBudgetSaisieNature);
        
        if ( getProcessType().equals(PROCESS_RELIQUAT))
            this.verifierLesBudgetsPositifs(arrayEOBudgetSaisieGestion,arrayEOBudgetSaisieNature);
            }
    
    // methodes outils
    private void setProcessType (String s)
    {
        processType=s;
    }
    
    private String getProcessType () throws Exception
    {
        if (processType == null)
            throw new Exception (PROBLEME_TYPE_PROCESS);
        
        return getProcessType();
    }
    
    
    
    private void verificationMIO (NSArray  arrayEOBudgetSaisieNature)throws Exception
    {
        //TODO
        // verifier que les + et les - reviennent a une variation de  masse de credit nulle
        
        //  lister les types de credit depense
        
        //  lister les types de credit recette
        
        
        // faire la somme des saisies positives et negtives : exection si != zero.
        
        
        
    }
    
    
}
