/*
 * Created on 17 juil. 2004
 *
 */
package org.cocktail.bibasse.client.process.budget;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.factory.FactoryBibasse;
import org.cocktail.bibasse.client.factory.FactoryBudgetSaisie;
import org.cocktail.bibasse.client.factory.FactoryBudgetSaisieGestion;
import org.cocktail.bibasse.client.factory.FactoryBudgetSaisieNature;
import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.finder.FinderPlanComptable;
import org.cocktail.bibasse.client.finder.FinderTypeCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOBudgetParametres;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeOrgan;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.metier.EOUtilisateur;
import org.cocktail.bibasse.client.process.Process;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;



/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 *
 */
public abstract class ProcessBudget  extends Process {

	FactoryBudgetSaisie factoryBudgetSaisie ;
	FactoryBudgetSaisieGestion factoryBudgetSaisieGestion ;
	FactoryBudgetSaisieNature factoryBudgetSaisieNature;

	final static String ERROR_MESS_BUDGET_GESTION_NEGATIF = "PAS DE MONTANTS NEGATIFS POUR CETTE SAISIE DU BUDGET GESTION";
	final static String ERROR_MESS_BUDGET_NATURE_NEGATIF =  "PAS DE MONTANTS NEGATIFS POUR CETTE SAISIE DU BUDGET NATURE";
	final static String ERROR_MESS_BUDGET_NON_EQUILIBRE_PAR_CREDIT =  "BUDGET PAR CREDIT NON EQUILIBRE";
	final static String ERROR_MESS_BUDGET_NON_EQUILIBRE_NATURE = "BUDGET PAR NATURE NON EQUILIBRE";
	final static String ERROR_MESS_BUDGET_NON_EQUILIBRE = "LES BUDGETS NE SONT PAS EQUILIBRES";
	final static String ERROR_MESS_BUDGET_LIGNE_ANNULE = "PROBLMES , CERTAINES LIGNES SONT A L ETAT ANNULE";


	/**
	 * @param withLog
	 */
	public ProcessBudget(boolean withLog) {
		super();
		factoryBudgetSaisie = new FactoryBudgetSaisie(withLog);
		factoryBudgetSaisieGestion = new FactoryBudgetSaisieGestion(withLog);
		factoryBudgetSaisieNature = new FactoryBudgetSaisieNature(withLog);
	}

	/** Methode qui permet de creer une saisie budgetaire UB CR.
	 *
	 * @param monEOEditingContext editingContext de travail
	 * @param setTypeEtatValide  EOTypeEtat VALIDE
	 * @param setTypeSaisie     EOTypeSaisie
	 * @param setExercice EOExercice de saisie
	 * @param setBdsaLibelle libelle de la saisie
	 * @param setUtilisateur EOUtilisateur ouvrant la saisie
	 * @return une EOBudgetSaisie
	 * @throws Exception
	 */
	public EOBudgetSaisie creerUnEOBudgetSaisieHorsConvention(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			EOTypeSaisie setTypeSaisie,
			EOExercice setExercice,
			String setBdsaLibelle,
			EOUtilisateur setUtilisateur
	) throws Exception {

		EOBudgetSaisie  monEOBudgetSaisie = factoryBudgetSaisie.creerEOBudgetSaisieHorsConvention(
				monEOEditingContext,
				setUtilisateur,
				setTypeSaisie,
				setTypeEtatValide,
				setExercice,
				setBdsaLibelle,
				new NSTimestamp());

		return monEOBudgetSaisie;
	}

	/** Methode qui permet de creer une saisie budgetaire CONVENTION.
	 *
	 * @param monEOEditingContext editingContext de travail
	 * @param setTypeEtatValide  EOTypeEtat VALIDE
	 * @param setTypeSaisie     EOTypeSaisie
	 * @param setExercice EOExercice de saisie
	 * @param setBdsaLibelle libelle de la saisie
	 * @param setUtilisateur EOUtilisateur ouvrant la saisie
	 * @return une EOBudgetSaisie
	 * @throws Exception
	 */
	public  EOBudgetSaisie creerUnEOBudgetSaisiePourConvention(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			EOTypeSaisie setTypeSaisie,
			EOExercice setExercice,
			String setBdsaLibelle,
			EOUtilisateur setUtilisateur
	) throws Exception {

		// factoryBudgetSaisie
		EOBudgetSaisie  monEOBudgetSaisie = factoryBudgetSaisie.creerEOBudgetSaisiePourConvention(
				monEOEditingContext,
				setUtilisateur,
				setTypeSaisie,
				setTypeEtatValide,
				setExercice,
				setBdsaLibelle,
				new NSTimestamp());


		return monEOBudgetSaisie;
	}


	/** Permet d'annuler une EOBudgetSaisie.
	 *
	 * @param monEOEditingContext editingContext de travail
	 * @param monEOBudgetSaisie saisie a annuler
	 * @param setUtilisateur utilisateur demandant l'annulation
	 * @param setTypeEtatAnnule etat annule
	 * @throws Exception probleme d'etat
	 */
	protected  void annulerUnEOBudgetSaisie(
			EOEditingContext monEOEditingContext,
			EOBudgetSaisie monEOBudgetSaisie,
			EOUtilisateur setUtilisateur,
			EOTypeEtat setTypeEtatAnnule
	) throws Exception {

		// factoryBudgetSaisie
		factoryBudgetSaisie.annulerEOBudgetSaisie(
				monEOEditingContext,
				monEOBudgetSaisie,
				setUtilisateur,
				setTypeEtatAnnule);

	}

	/** Cloture la saisie d'une budgetSaisie pour vote.
	 *
	 * @param monEOEditingContext editingContext de travail
	 * @param setTypeEtatClot etat CLOT
	 * @param monEOBudgetSaisie budgetSaisie a cloturer
	 * @param setUtilisateurValidation utilisateur qui cloture
	 * @throws Exception problem d'etat
	 */
	protected  void cloturerUnEOBudgetSaisie(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatClot,
			EOBudgetSaisie monEOBudgetSaisie,
			EOUtilisateur setUtilisateurValidation
	) throws Exception {

		factoryBudgetSaisie.cloturerEOBudgetSaisie(
				monEOEditingContext,
				monEOBudgetSaisie,
				setUtilisateurValidation,
				new NSTimestamp(),
				setTypeEtatClot);
	}


	/** Verification de la saisie pour un organ.
	 *
	 * @param monEOEditingContext edting contexte de travail
	 * @param monEOBudgetSaisie saisie a verifier
	 * @param arrayEOTypeCreditDepense
	 * @param arrayEOBudgetSaisieGestion Les lignes du budget de gestion
	 * @param arrayEOBudgetSaisieNature Les lignes du budget par nature
	 * @param resultatBrutDepenseCompte EOBudgetSaisieNature du compte comptable concerné
	 * @param resultatBrutRecetteCompte EOBudgetSaisieNature du compte comptable concerné
	 * @param cafCompte EOBudgetSaisieNature du compte comptable concerné
	 * @param iafCompte EOBudgetSaisieNature du compte comptable concerné
	 * @param augmentationFDRCompte EOBudgetSaisieNature du compte comptable concerné
	 * @param diminutionFDRCompte EOBudgetSaisieNature du compte comptable concerné
	 * @throws Exception
	 */

	protected  void verificationEOBudgetSaisieAvantVote(
			EOEditingContext monEOEditingContext,
			EOBudgetSaisie  monEOBudgetSaisie,
			NSArray arrayEOTypeCreditDepense,
			NSArray arrayEOBudgetSaisieGestion,
			NSArray arrayEOBudgetSaisieNature,
			EOBudgetSaisieNature  resultatBrutDepenseCompte,
			EOBudgetSaisieNature  resultatBrutRecetteCompte,
			EOBudgetSaisieNature  cafCompte,
			EOBudgetSaisieNature  iafCompte,
			EOBudgetSaisieNature  augmentationFDRCompte,
			EOBudgetSaisieNature  diminutionFDRCompte
	) throws Exception {

		//      verification Complete avant appel de la procedure stockée !!!
		//      pour passe a l etat TRAITE ....

		//      verifier etats
		this.verifierEtat(
				arrayEOBudgetSaisieGestion,
				arrayEOBudgetSaisieNature);

		boolean compte777EnProduit=false;
		String param = FinderBudgetParametres.getValue(monEOEditingContext, monEOBudgetSaisie.exercice(), EOBudgetParametres.param_777_EN_PRODUIT);
		if (param != null && param.equals("OUI"))
			compte777EnProduit=true;

		boolean compte775FDR=false;
		param = FinderBudgetParametres.getValue(monEOEditingContext, monEOBudgetSaisie.exercice(), EOBudgetParametres.param_775_FDR);
		if (param!=null && param.equals("OUI"))
			compte775FDR=true;

		// verifier des comptes budgetaires 0XX
		this.calculBudgetNatureComptesBudgetaires (monEOEditingContext, arrayEOBudgetSaisieNature,
				resultatBrutDepenseCompte, resultatBrutRecetteCompte, cafCompte, iafCompte,
				augmentationFDRCompte, diminutionFDRCompte, compte777EnProduit, compte775FDR);

		// equilibre nature = gestion = credit
		this.verifierEquilibreBudgetaire(
				arrayEOTypeCreditDepense,
				arrayEOBudgetSaisieGestion,
				arrayEOBudgetSaisieNature
		);
	}



	/** Annuler les saisies budgetaires.
	 *
	 * @param monEOEditingContext editing context de travail
	 * @param setTypeEtatAnnule etat annule
	 * @param arrayBudgetSaisieGestion les lignes du budget de gestion
	 * @param arrayBudgetSaisieNature les lignes du budget par nature
	 * @param setUtilisateur utilisateur qui annnule la saisie
	 * @throws Exception
	 */
	protected void annulerLesBudgets(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatAnnule,
			NSArray arrayBudgetSaisieGestion,
			NSArray arrayBudgetSaisieNature,
			EOUtilisateur setUtilisateur
	) throws Exception {
		int i = 0;
		while (arrayBudgetSaisieGestion.count() > i) {
			factoryBudgetSaisieGestion.annulerEOBudgetSaisieGestion( monEOEditingContext,(EOBudgetSaisieGestion) arrayBudgetSaisieGestion.objectAtIndex(i),setTypeEtatAnnule );
			i++;
		}

		i = 0;
		while (arrayBudgetSaisieNature.count() > i) {
			factoryBudgetSaisieNature.annulerEOBudgetSaisieNature( monEOEditingContext,(EOBudgetSaisieNature) arrayBudgetSaisieNature.objectAtIndex(i),setTypeEtatAnnule );
			i++;
		}
	}

	/** Cloturer la saisie.
	 *
	 * @param monEOEditingContext editing context de travail
	 * @param setTypeEtatClot etat clot
	 * @param arrayBudgetSaisieGestion lignes du budet de gestion
	 * @param arrayBudgetSaisieNature lignes du budget par nature
	 * @param setUtilisateur utilisateur qui cloture la saisie
	 * @throws Exception
	 */
	protected void cloturerLesBudgets(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatClot,
			NSArray arrayBudgetSaisieGestion,
			NSArray arrayBudgetSaisieNature,
			EOUtilisateur setUtilisateur
	) throws Exception {
		int i = 0;
		while (arrayBudgetSaisieGestion.count() > i) {
			factoryBudgetSaisieGestion.cloturerEOBudgetSaisieGestion(monEOEditingContext,(EOBudgetSaisieGestion) arrayBudgetSaisieGestion.objectAtIndex(i),setTypeEtatClot );
			i++;
		}

		i = 0;
		while (arrayBudgetSaisieNature.count() > i) {
			factoryBudgetSaisieNature.cloturerEOBudgetSaisieNature( monEOEditingContext,(EOBudgetSaisieNature) arrayBudgetSaisieNature.objectAtIndex(i),setTypeEtatClot );
			i++;
		}
	}

	/** ajouter les budgets pour une composante
	 *
	 * @param monEOEditingContext editing context
	 * @param setTypeEtatValide etat valide
	 * @param arrayPlancoCreditBudget les Planco Credit parametre par l'agence comptable ou service financier
	 * @param arrayTypeCredit les types de credit
	 * @param arrayActions les action s LOLF
	 * @param setTypeSaisie  le type de saisie budgetaire (PROV,INIT,RELIQ....)
	 * @param setOrgan la ligne organ (UB,CR ou conv)
	 * @param setExercice l'exercice
	 * @param setBdsaLibelle le libelle
	 * @param setUtilisateur l'utilisateur
	 * @param monEOBudgetSaisie la saisie budgetaire concernée
	 * @throws Exception
	 */
	protected void ajouterLesBudgets(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			NSArray arrayPlancoCreditBudget,
			NSArray arrayTypeCreditD,
			NSArray arrayActionsD,
			NSArray arrayTypeCreditR,
			NSArray arrayActionsR,
			EOTypeSaisie setTypeSaisie,
			EOOrgan setOrgan,
			EOExercice setExercice,
			String setBdsaLibelle,
			EOUtilisateur setUtilisateur ,
			EOBudgetSaisie monEOBudgetSaisie
	) throws Exception {
		verifierPlanComptableBudgetaire(arrayPlancoCreditBudget) ;
		verifierActions(arrayActionsD);
		verifierCredits(arrayTypeCreditD);
		verifierActions(arrayActionsR);
		verifierCredits(arrayTypeCreditR);

		verifierOrgan(setOrgan);
		verifierUtilisateur(setUtilisateur) ;
		verifierExercice(setExercice) ;

		this.initialiserBudgetNature
		(             monEOEditingContext,
				setTypeEtatValide,
				arrayPlancoCreditBudget,
				setOrgan,
				setExercice,
				setUtilisateur,
				monEOBudgetSaisie);


		this.initialiserBudgetGestion
		(             monEOEditingContext,
				setTypeEtatValide,
				arrayTypeCreditD,
				arrayActionsD,
				arrayTypeCreditR,
				arrayActionsR,
				setOrgan,
				setExercice,
				setUtilisateur,
				monEOBudgetSaisie) ;

	}




	// verification TYPE DE SAISIE
	protected void verifierSaisieProvisoire (EOTypeSaisie  laSaisie) throws Exception
	{
		if (! EOTypeSaisie.SAISIE_PROVISOIRE.equals(laSaisie.tysaLibelle()))
			throw new Exception("verifier type saisie SAISIE_PROVISOIRE : "+EOTypeSaisie.ERROR_MESS_SAISIE_PROVISOIRE);
	}

	protected void verifierSaisieInitial (EOTypeSaisie  laSaisie) throws Exception
	{
		if (! EOTypeSaisie.SAISIE_INITIAL.equals(laSaisie.tysaLibelle()))
			throw new Exception("verifier type saisie SAISIE_INITIAL : "+EOTypeSaisie.ERROR_MESS_SAISIE_INITIAL);
	}

	protected void verifierSaisieReliquat (EOTypeSaisie  laSaisie) throws Exception
	{
		if (! EOTypeSaisie.SAISIE_RELIQUAT.equals(laSaisie.tysaLibelle()))
			throw new Exception("verifier type saisie SAISIE_RELIQUAT : "+EOTypeSaisie.ERROR_MESS_SAISIE_RELIQUAT);
	}

	protected void verifierSaisieDbm(EOTypeSaisie  laSaisie) throws Exception
	{
		if (! EOTypeSaisie.SAISIE_DBM.equals(laSaisie.tysaLibelle()))
			throw new Exception("verifier type saisie SAISIE_DBM : "+EOTypeSaisie.ERROR_MESS_SAISIE_DBM);
	}


	protected void verifierSaisieConvention (EOTypeSaisie  laSaisie) throws Exception
	{
		if (! EOTypeSaisie.SAISIE_CONVENTION.equals(laSaisie.tysaLibelle()))
			throw new Exception("verifier type saisie SAISIE_CONVENTION : "+EOTypeSaisie.ERROR_MESS_SAISIE_CONVENTION);
	}


	protected void verifierSaisieMio (EOTypeSaisie  laSaisie) throws Exception
	{
		if (! EOTypeSaisie.SAISIE_MIO.equals(laSaisie.tysaLibelle()))
			throw new Exception("verifier type saisie SAISIE_MIO : "+EOTypeSaisie.ERROR_MESS_SAISIE_MIO);
	}



	// methodes  outils

	private void verifierPlanComptableBudgetaire(NSArray arrayLesComptes) throws Exception
	{

		//  verfier validite
		for (int i = 0; i < arrayLesComptes.count(); i++) {
			if (((EOPlanComptable)arrayLesComptes.objectAtIndex(i)).pcoValidite().equals(EOPlanComptable.ETAT_VALIDE))
				throw new Exception(EOPlanComptable.ERROR_MESS_ETAT_VALIDE);
		}

		//  verifier compte budgetaire
		for ( int i = 0; i < arrayLesComptes.count(); i++) {
			if (((EOPlanComptable)arrayLesComptes.objectAtIndex(i)).pcoBudgetaire().equals(EOPlanComptable.ETAT_BUDGETAIRE))
				throw new Exception(EOPlanComptable.ERROR_MESS_ETAT_BUDGETAIRE);
		}
	}

	private void verifierActions(NSArray arrayActions) throws Exception
	{
		//  verifier niveau ACTION
		for ( int i = 0; i < arrayActions.count(); i++) {
			if (((EOTypeAction)arrayActions.objectAtIndex(i)).tyacNiveau().equals(EOTypeAction.NIVEAU_ACTION))
				throw new Exception(EOTypeAction.ERROR_MESS_NIVEAU_ACTION);
		}

	}

	private void verifierUtilisateur(EOUtilisateur unUtilistateur) throws Exception
	{
		//  verifier VALIDITE
		if (  unUtilistateur.utlFermeture() != null )
			throw new Exception(EOUtilisateur.ERROR_MESS_FERMETURE);
	}

	private void verifierExercice(EOExercice unExercice) throws Exception
	{
		//  verifier EXERCICE OUVERT
		if (  unExercice.exeCloture() != null )
			throw new Exception(EOExercice.ERROR_MESS_FERMETURE);
	}

	private void verifierCredits(NSArray arrayCredits) throws Exception
	{

	}

	private void verifierOrgan(EOOrgan  laLigneBudgetaire) throws Exception
	{
		//  verifier NIVEAU UB CR CONVENTION
		if (! ((EOTypeOrgan)laLigneBudgetaire.typeOrgan()).tyorLibelle().equals(EOTypeOrgan.TYPE_CONVENTION))
		{
			if (  laLigneBudgetaire.orgNiveau().intValue() < 2 )
				throw new Exception(EOOrgan.ERROR_MESS_NIVEAU_SAISIE);
		}
	}

	/**
	 *
	 * @param monEOEditingContext
	 * @param setTypeEtatValide
	 * @param arrayPlancoCreditBudget
	 * @param setOrgan
	 * @param setExercice
	 * @param setUtilisateur
	 * @param monEOBudgetSaisie
	 * @throws Exception
	 */
	public void initialiserBudgetNature (EOEditingContext monEOEditingContext, EOTypeEtat setTypeEtatValide, NSArray arrayMasqueNatureBudget,
			EOOrgan setOrgan, EOExercice setExercice, EOUtilisateur setUtilisateur, EOBudgetSaisie monEOBudgetSaisie) throws Exception {

		NSMutableArray lesBudgetsCrees=new NSMutableArray();

		// boucle sur   arrayPlancoCreditBudget
		int i = 0;
		while (arrayMasqueNatureBudget.count() > i) {

			EOTypeCredit typeCredit=(EOTypeCredit)((EOBudgetMasqueNature) arrayMasqueNatureBudget.objectAtIndex(i)).typeCredit();
			EOPlanComptable planComptable=(EOPlanComptable)((EOBudgetMasqueNature) arrayMasqueNatureBudget.objectAtIndex(i)).planComptable();

			NSMutableArray andQualifier=new NSMutableArray();
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.PLAN_COMPTABLE_KEY+"=%@", new NSArray(planComptable)));
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.ORGAN_KEY+"=%@", new NSArray(setOrgan)));
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.EXERCICE_KEY+"=%@", new NSArray(setExercice)));
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.BUDGET_SAISIE_KEY+"=%@", new NSArray(monEOBudgetSaisie)));

			if (EOQualifier.filteredArrayWithQualifier(lesBudgetsCrees, new EOAndQualifier(andQualifier)).count()==0)
				lesBudgetsCrees.addObject(this.creerUneLigneNature(monEOEditingContext, setTypeEtatValide, typeCredit, planComptable,
						setOrgan, setExercice, monEOBudgetSaisie));
			i++;
		}

		EOTypeCredit typeCreditReserve = FinderTypeCredit.findTypeCreditForCode(monEOEditingContext, setExercice, FinderBudgetParametres.getValue(monEOEditingContext, setExercice,"CODE_TCD_RESERVE"));


		// Insertion du compte IAF
		creerUneLigneNature(monEOEditingContext,
				setTypeEtatValide,
				typeCreditReserve,
				FinderPlanComptable.findCompte(monEOEditingContext, FinderBudgetParametres.getValue(monEOEditingContext,setExercice, "COMPTE_IAF"),
						setExercice),
				setOrgan,
				setExercice,
				monEOBudgetSaisie);

		// Insertion du compte CAF
		creerUneLigneNature(monEOEditingContext,
				setTypeEtatValide,
				typeCreditReserve,
				FinderPlanComptable.findCompte(monEOEditingContext, FinderBudgetParametres.getValue(monEOEditingContext,setExercice, "COMPTE_CAF"),
						setExercice),
				setOrgan,
				setExercice,
				monEOBudgetSaisie);

		// Insertion du compte Augmentation FDR
		creerUneLigneNature(monEOEditingContext,
				setTypeEtatValide,
				typeCreditReserve,
				FinderPlanComptable.findCompte(monEOEditingContext, FinderBudgetParametres.getValue(monEOEditingContext, setExercice,"COMPTE_AUGMENTATION_FDR"),
						setExercice),
				setOrgan,
				setExercice,
				monEOBudgetSaisie);

		// Insertion du compte Diminution FDR
		creerUneLigneNature(monEOEditingContext,
				setTypeEtatValide,
				typeCreditReserve,
				FinderPlanComptable.findCompte(monEOEditingContext, FinderBudgetParametres.getValue(monEOEditingContext,setExercice, "COMPTE_DIMINUTION_FDR"),
						setExercice),
				setOrgan,
				setExercice,
				monEOBudgetSaisie);

		// Insertion du compte Excedent Fonctionnement
		creerUneLigneNature(monEOEditingContext,
				setTypeEtatValide,
				typeCreditReserve,
				FinderPlanComptable.findCompte(monEOEditingContext, FinderBudgetParametres.getValue(monEOEditingContext,setExercice,"COMPTE_EXCEDENT_FONC"),
						setExercice),
				setOrgan,
				setExercice,
				monEOBudgetSaisie);

		// Insertion du compte Deficit Fonctionnement
		creerUneLigneNature(monEOEditingContext,
				setTypeEtatValide,
				typeCreditReserve,
				FinderPlanComptable.findCompte(monEOEditingContext, FinderBudgetParametres.getValue(monEOEditingContext,setExercice, "COMPTE_DEFICIT_FONC"),
						setExercice),
				setOrgan,
				setExercice,
				monEOBudgetSaisie);

	}


	public void initialiserBudgetGestion
	(            EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			NSArray arrayTypeCreditD,
			NSArray arrayActionsD,
			NSArray arrayTypeCreditR,
			NSArray arrayActionsR,
			EOOrgan setOrgan,
			EOExercice setExercice,
			EOUtilisateur setUtilisateur,
			EOBudgetSaisie monEOBudgetSaisie) throws Exception
			{
		int i = 0;
		int j = 0;

		System.out.println("ProcessBudget.initialiserBudgetGestion() EXERCICE : " + setExercice);

		while (arrayTypeCreditD.count() > i) {
			while (arrayActionsD.count() > j) {
				this.creerUneLigneGestion(
						monEOEditingContext,
						setTypeEtatValide,
						(EOTypeCredit) arrayTypeCreditD.objectAtIndex(i),
						(EOTypeAction) arrayActionsD.objectAtIndex(j),
						setOrgan,
						setExercice,
						monEOBudgetSaisie
				);
				j++;
			}
			j = 0;
			i++;
		}

		i = 0;
		j = 0;
		while (arrayTypeCreditR.count() > i) {
			while (arrayActionsR.count() > j) {
				this.creerUneLigneGestion(
						monEOEditingContext,
						setTypeEtatValide,
						(EOTypeCredit) arrayTypeCreditR.objectAtIndex(i),
						(EOTypeAction) arrayActionsR.objectAtIndex(j),
						setOrgan,
						setExercice,
						monEOBudgetSaisie
				);
				j++;
			}
			j = 0;
			i++;
		}
			}


	public EOBudgetSaisieNature creerUneLigneNature (
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			EOTypeCredit setTypeCredit,
			EOPlanComptable setPlanComptable,
			EOOrgan setOrgan,
			EOExercice setExercice,
			EOBudgetSaisie setBudgetSaisie
	) throws Exception
	{

		// factoryBudgetSaisieNature
		return factoryBudgetSaisieNature.creerEOBudgetSaisieNature(
				monEOEditingContext,
				setTypeEtatValide,
				setTypeCredit,
				setPlanComptable,
				setOrgan,
				setExercice,
				setBudgetSaisie,
				new BigDecimal (0),
				new BigDecimal (0),
				new BigDecimal (0));
	}

	public EOBudgetSaisieNature creerUneLigneNatureReserve (EOEditingContext monEOEditingContext, EOTypeEtat setTypeEtatValide, EOPlanComptable setPlanComptable,
			EOOrgan setOrgan, EOExercice setExercice, EOBudgetSaisie setBudgetSaisie) throws Exception {
		EOTypeCredit typeCreditReserve = FinderTypeCredit.findTypeCreditForCode(monEOEditingContext, setExercice, FinderBudgetParametres.getValue(monEOEditingContext, setExercice,"CODE_TCD_RESERVE"));
		EOBudgetSaisieNature bud=factoryBudgetSaisieNature.creerEOBudgetSaisieNature(monEOEditingContext, setTypeEtatValide, typeCreditReserve, setPlanComptable, setOrgan,
				setExercice, setBudgetSaisie, new BigDecimal (0), new BigDecimal (0), new BigDecimal (0));
		return bud;
	}

	private void creerUneLigneGestion(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			EOTypeCredit setTypeCredit,
			EOTypeAction setTypeAction,
			EOOrgan setOrgan,
			EOExercice setExercice,
			EOBudgetSaisie setBudgetSaisie
	) throws Exception
	{
		// factoryBudgetSaisieGestion
		factoryBudgetSaisieGestion.creerEOBudgetSaisieGestion(
				monEOEditingContext,
				setTypeEtatValide,
				setTypeCredit,
				setTypeAction,
				setOrgan,
				setExercice,
				setBudgetSaisie,
				new BigDecimal(0.0),
				new BigDecimal(0.0),
				new BigDecimal(0.0)
		);
	}


	protected void verifierEquilibreBudgetaire (
			NSArray arrayEOTypeCreditDepense,
			NSArray arrayEOBudgetSaisieGestion,
			NSArray arrayEOBudgetSaisieNature
	) throws Exception
	{
		BigDecimal totalNatureCreditDepense = new BigDecimal(0.0);
		BigDecimal totalGestionCreditDepense = new BigDecimal(0.0);

		BigDecimal totalGestionDepense = new BigDecimal(0.0);

		BigDecimal totalNatureDepense = new BigDecimal(0.0);
		BigDecimal totalNatureRecette = new BigDecimal(0.0);

		//  somme(credit) nature = somme(credit) gestion
		EOTypeCredit currentEOTypeCredit =null;

		for (int i = 0; i < arrayEOTypeCreditDepense.count(); i++) {
			currentEOTypeCredit = (EOTypeCredit) arrayEOTypeCreditDepense.objectAtIndex(i);

			for (int j = 0; j < arrayEOBudgetSaisieGestion.count(); j++) {
				if (((EOBudgetSaisieGestion)arrayEOBudgetSaisieGestion.objectAtIndex(j)).typeCredit().equals(currentEOTypeCredit))
					totalGestionCreditDepense.add(((EOBudgetSaisieGestion)arrayEOBudgetSaisieGestion.objectAtIndex(j)).bdsgMontant());
			}

			for (int j = 0; j < arrayEOBudgetSaisieNature.count(); j++) {
				if (((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(j)).typeCredit().equals(currentEOTypeCredit))
					totalNatureCreditDepense.add(((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(j)).bdsnMontant());
			}
		}


		if (ProcessBudget.different(totalNatureCreditDepense,totalGestionCreditDepense))
			throw new Exception(ERROR_MESS_BUDGET_NON_EQUILIBRE_PAR_CREDIT+
					" totalNatureCreditDepense" +totalNatureCreditDepense+" totalGestionCreditDepense "+totalGestionCreditDepense);

		//verifier somme(nature depense) = somme(nature recette)
		for (int j = 0; j < arrayEOBudgetSaisieNature.count(); j++) {
			if (((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(j)).planComptable().pcoNature().equals(EOPlanComptable.TYPE_RECETTE))
				totalNatureRecette.add(((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(j)).bdsnMontant());
		}

		for (int j = 0; j < arrayEOBudgetSaisieNature.count(); j++) {
			if (((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(j)).planComptable().pcoNature().equals(EOPlanComptable.TYPE_DEPENSE))
				totalNatureDepense.add(((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(j)).bdsnMontant());
		}


		if (ProcessBudget.different(totalNatureDepense,totalNatureRecette))
			throw new Exception(ERROR_MESS_BUDGET_NON_EQUILIBRE_NATURE
					+ "totalNatureDepense "+totalNatureDepense+" totalNatureRecette "+totalNatureRecette);

		// somme du budget de gestion depense
		for (int j = 0; j < arrayEOBudgetSaisieGestion.count(); j++) {
			if (((EOBudgetSaisieGestion)arrayEOBudgetSaisieGestion.objectAtIndex(j)).typeAction().tyacType(). equals(EOTypeAction.ACTION_DEPENSE))
				totalGestionDepense.add(((EOBudgetSaisieGestion)arrayEOBudgetSaisieGestion.objectAtIndex(j)).bdsgMontant());
		}

		// verifier les trois budgets en depense??
		if (ProcessBudget.different(totalNatureDepense,totalGestionCreditDepense))
			throw new Exception(ERROR_MESS_BUDGET_NON_EQUILIBRE
					+" totalNatureDepense "+totalNatureDepense+" totalGestionCreditDepense "+totalGestionCreditDepense);

		if (ProcessBudget.different(totalNatureDepense,totalGestionDepense))
			throw new Exception(ERROR_MESS_BUDGET_NON_EQUILIBRE
					+" NatureDepense "+totalNatureDepense+" totalGestionDepense "+totalGestionDepense);

	}

	/**
	 *
	 * @param monEOEditingContext
	 * @param arrayEOBudgetSaisieNature
	 * @param resultatBrutDepenseCompte
	 * @param resultatBrutRecetteCompte
	 * @param cafCompte
	 * @param iafCompte
	 * @param augmentationFDRCompte
	 * @param diminutionFDRCompte
	 * @throws Exception
	 */
	public void calculBudgetNatureComptesBudgetaires (
			EOEditingContext monEOEditingContext,
			NSArray  arrayEOBudgetSaisieNature,
			EOBudgetSaisieNature  resultatBrutDepenseCompte,
			EOBudgetSaisieNature  resultatBrutRecetteCompte,
			EOBudgetSaisieNature  cafCompte,
			EOBudgetSaisieNature  iafCompte,
			EOBudgetSaisieNature  augmentationFDRCompte,
			EOBudgetSaisieNature  diminutionFDRCompte,
			boolean compte777_en_produit, boolean compte775FDR
	) throws Exception
	{
		// calculs
		BigDecimal  chap60_66 = calculerSommechap60_66(arrayEOBudgetSaisieNature, "bdsnMontant");
		BigDecimal  chap67_68 = calculerSommechap67_68(arrayEOBudgetSaisieNature, "bdsnMontant");
		BigDecimal  chap675_68 = calculerSommechap675_68(arrayEOBudgetSaisieNature, "bdsnMontant");
		BigDecimal  chap70_76 = calculerSommechap70_76(arrayEOBudgetSaisieNature, "bdsnMontant");
		BigDecimal  chap77_78 = calculerSommechap77_78(arrayEOBudgetSaisieNature, "bdsnMontant");
		BigDecimal  chap775_776_777_78 = calculerSommechap771_775_776_777_78(arrayEOBudgetSaisieNature, "bdsnMontant", compte777_en_produit);
		BigDecimal  chap775=new BigDecimal(0);
		if (compte775FDR)
			chap775=calculerSommechap775(arrayEOBudgetSaisieNature, "bdsnMontant");

		BigDecimal  classe2 = calculerSommeclasse2(arrayEOBudgetSaisieNature, "bdsnMontant");
		BigDecimal  classe1 = calculerSommeclasse1(arrayEOBudgetSaisieNature, "bdsnMontant");

		// Brut Depense = Total Classe 7 - Total Classe 6
		BigDecimal resultatBrutDepense = (chap70_76.add(chap77_78)).subtract(chap60_66.add(chap67_68));
		resultatBrutDepenseCompte.setBdsnMontant(zeroSiNegatif(resultatBrutDepense));

		// Brut Recette = Total Classe 6 - Total Classe 7
		BigDecimal resultatBrutRecette = (chap60_66.add(chap67_68)).subtract(chap70_76.add(chap77_78));
		resultatBrutRecetteCompte.setBdsnMontant(zeroSiNegatif(resultatBrutRecette));

		// CAF = (Total_7 - Total_6) + Total(67_68) - Total(77_78)
		BigDecimal caf = (chap70_76.add(chap77_78)).subtract(chap60_66.add(chap67_68)).add(chap675_68).subtract(chap775_776_777_78);


		// IAF = - CAF
		BigDecimal iaf = zeroSiNegatif(caf.multiply(new BigDecimal(-1)));

		caf = zeroSiNegatif(caf);

		cafCompte.setBdsnMontant(caf);
		//cafCompte.setBdsnSaisi(cafCompte.bdsnMontant().subtract(cafCompte.bdsnVote()));

		iafCompte.setBdsnMontant(iaf);
		//iafCompte.setBdsnSaisi(iafCompte.bdsnMontant().subtract(iafCompte.bdsnVote()));

		// Augmentation FDR = zeroSiNegatif ( (CAF+classe1+Compte775) - (IAF+classe2) )
		augmentationFDRCompte.setBdsnMontant(zeroSiNegatif( (caf.add(classe1).add(chap775)).subtract(iaf.add(classe2)) ));
		augmentationFDRCompte.setBdsnSaisi(augmentationFDRCompte.bdsnMontant().subtract(augmentationFDRCompte.bdsnVote()));

		// Diminution FDR = zeroSiNegatif ( (IAF+classe2) - (CAF+classe1+Compte775) )
		diminutionFDRCompte.setBdsnMontant(zeroSiNegatif( (iaf.add(classe2)).subtract(caf.add(classe1).add(chap775)) ));
		diminutionFDRCompte.setBdsnSaisi(diminutionFDRCompte.bdsnMontant().subtract(diminutionFDRCompte.bdsnVote()));

	}

	/**
	 *
	 * @param b
	 * @return
	 * @throws Exception
	 */
	private BigDecimal zeroSiNegatif(BigDecimal b)  throws Exception	{
		if (FactoryBibasse.inferieur(b,new BigDecimal(0.00)))
			return new BigDecimal(0.00).setScale(2);
		else
			return b;
	}

	/**
	 *
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private BigDecimal calculerSommechap60_66(NSArray  arrayEOBudgetSaisieNature, String columnName)	{

		NSArray comptesReferences = new NSArray(new String[] {"60","61","62","63","64","65","66","69"});
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			String currentCompte = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable().pcoNum().substring(0,2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}

		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}



	private BigDecimal calculerSommechap67_68(NSArray  arrayEOBudgetSaisieNature, String columnName)    {

		NSArray comptesReferences = new NSArray(new String[] {"67","68"});
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			EOPlanComptable planco = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable();

			String currentCompte = planco.pcoNum().substring(0,2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}



	private BigDecimal calculerSommechap675_68(NSArray  arrayEOBudgetSaisieNature, String columnName)    {

		NSArray comptesReferences = new NSArray(new String[] {"675","68"});
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			EOPlanComptable planco = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable();

			String currentCompte2 = planco.pcoNum().substring(0,2);
			String currentCompte3 = planco.pcoNum().substring(0,2);
			if (planco.pcoNum().length() > 2)
				currentCompte3 = planco.pcoNum().substring(0,3);

			if (comptesReferences.containsObject(currentCompte2) || comptesReferences.containsObject(currentCompte3))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}


	private BigDecimal calculerSommechap70_76(NSArray  arrayEOBudgetSaisieNature, String columnName)    {

		NSArray comptesReferences = new NSArray(new String[] {"70","71","72","73","74","75","76","79"});
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			String currentCompte = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable().pcoNum().substring(0,2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private BigDecimal calculerSommechap77_78(NSArray  arrayEOBudgetSaisieNature, String columnName)	{

		NSArray comptesReferences = new NSArray(new String[] {"77","78"});
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			String currentCompte = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable().pcoNum().substring(0,2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}



	/**
	 *
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private BigDecimal calculerSommechap771_775_776_777_78(NSArray  arrayEOBudgetSaisieNature, String columnName, boolean compte777_en_produit)	{

		NSArray comptesReferences = new NSArray(new String[] {"775", "776", "777" ,"78"});
		if (compte777_en_produit)
			comptesReferences = new NSArray(new String[] {"775", "776", "78"});

		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			EOPlanComptable planco = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable();

			String currentCompte2 = planco.pcoNum().substring(0,2);
			String currentCompte3 = planco.pcoNum().substring(0,2);

			if (planco.pcoNum().length() > 2)
				currentCompte3 = planco.pcoNum().substring(0,3);

			if (comptesReferences.containsObject(currentCompte2) || comptesReferences.containsObject(currentCompte3))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private BigDecimal calculerSommechap775(NSArray  arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences = new NSArray(new String[] {"775"});

		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			EOPlanComptable planco = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptable();

			String currentCompte2 = planco.pcoNum().substring(0,2);
			String currentCompte3 = planco.pcoNum().substring(0,2);

			if (planco.pcoNum().length() > 2)
				currentCompte3 = planco.pcoNum().substring(0,3);

			if (comptesReferences.containsObject(currentCompte2) || comptesReferences.containsObject(currentCompte3))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}


	/**
	 *
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private BigDecimal calculerSommeclasse1(NSArray  arrayEOBudgetSaisieNature, String columnName)	{
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			EOTypeCredit typeCredit = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).typeCredit();

			if (typeCredit.tcdType().equals("RECETTE") && typeCredit.tcdSect().equals("2"))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}


	/**
	 *
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private BigDecimal calculerSommeclasse2(NSArray  arrayEOBudgetSaisieNature, String columnName)	{
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {

			EOTypeCredit typeCredit = ((EOBudgetSaisieNature)arrayEOBudgetSaisieNature.objectAtIndex(i)).typeCredit();

			if (typeCredit.tcdType().equals("DEPENSE") && typeCredit.tcdSect().equals("2"))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return FactoryBibasse.calcSommeOfBigDecimals(pourLesComptes, columnName);
	}


	protected void verifierLesBudgetsPositifs (
			NSArray  arrayEOBudgetSaisieGestion,
			NSArray  arrayEOBudgetSaisieNature
	) throws Exception
	{
		//  verifier saisie positive : PROV INIT RELIQ
		for (int i = 0; i < arrayEOBudgetSaisieGestion.count(); i++) {
			if ( ProcessBudget.inferieur(((EOBudgetSaisieGestion) arrayEOBudgetSaisieGestion.objectAtIndex(i)).bdsgMontant(),new BigDecimal(0.0)))
				throw new Exception(ProcessBudget.ERROR_MESS_BUDGET_GESTION_NEGATIF);
		}

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			if ( ProcessBudget.inferieur(((EOBudgetSaisieNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).bdsnMontant(),new BigDecimal(0.0)))
				throw new Exception(ProcessBudget.ERROR_MESS_BUDGET_NATURE_NEGATIF);
		}
	}



	protected void verifierEtat (
			NSArray  arrayEOBudgetSaisieGestion,
			NSArray  arrayEOBudgetSaisieNature
	) throws Exception
	{
		//  verifier saisie positive : PROV INIT RELIQ
		for (int i = 0; i < arrayEOBudgetSaisieGestion.count(); i++) {
			if ( EOTypeEtat.ETAT_ANNULE.equals(((EOBudgetSaisieGestion) arrayEOBudgetSaisieGestion.objectAtIndex(i)).typeEtat().tyetLibelle()))
				throw new Exception(ProcessBudget.ERROR_MESS_BUDGET_LIGNE_ANNULE);
		}

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			if ( EOTypeEtat.ETAT_ANNULE.equals(((EOBudgetSaisieNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).typeEtat().tyetLibelle()))
				throw new Exception(ProcessBudget.ERROR_MESS_BUDGET_LIGNE_ANNULE);
		}
	}

	/************************************************************************/

	/**
	 *
	 * @param monEOEditingContext
	 * @param arrayEOBudgetSaisieNature
	 * @param resultatBrutDepenseCompte
	 * @param resultatBrutRecetteCompte
	 * @param cafCompte
	 * @param iafCompte
	 * @param augmentationFDRCompte
	 * @param diminutionFDRCompte
	 * @throws Exception
	 */
	public void calculBudgetNatureSaisiComptesBudgetaires (
			EOEditingContext monEOEditingContext,
			NSArray  arrayEOBudgetSaisieNature,
			EOBudgetSaisieNature  resultatBrutDepenseCompte,
			EOBudgetSaisieNature  resultatBrutRecetteCompte,
			EOBudgetSaisieNature  cafCompte,
			EOBudgetSaisieNature  iafCompte,
			EOBudgetSaisieNature  augmentationFDRCompte,
			EOBudgetSaisieNature  diminutionFDRCompte,
			boolean compte777_en_produit, boolean compte775FDR
	) throws Exception
	{
		// calculs
		BigDecimal  chap60_66 = calculerSommechap60_66(arrayEOBudgetSaisieNature, "bdsnSaisi");
		BigDecimal  chap67_68 = calculerSommechap67_68(arrayEOBudgetSaisieNature, "bdsnSaisi");
		BigDecimal  chap675_68 = calculerSommechap675_68(arrayEOBudgetSaisieNature, "bdsnSaisi");
		BigDecimal  chap70_76 = calculerSommechap70_76(arrayEOBudgetSaisieNature, "bdsnSaisi");
		BigDecimal  chap77_78 = calculerSommechap77_78(arrayEOBudgetSaisieNature, "bdsnSaisi");
		BigDecimal  chap775_776_777_78 = calculerSommechap771_775_776_777_78(arrayEOBudgetSaisieNature, "bdsnSaisi", compte777_en_produit);
		BigDecimal  chap775=new BigDecimal(0);
		if (compte775FDR)
			chap775=calculerSommechap775(arrayEOBudgetSaisieNature, "bdsnSaisi");

		BigDecimal  classe2 = calculerSommeclasse2(arrayEOBudgetSaisieNature, "bdsnSaisi");
		BigDecimal  classe1 = calculerSommeclasse1(arrayEOBudgetSaisieNature, "bdsnSaisi");

		// Brut Depense = Total Classe 7 - Total Classe 6
		BigDecimal resultatBrutDepense = (chap70_76.add(chap77_78)).subtract(chap60_66.add(chap67_68));
		resultatBrutDepenseCompte.setBdsnSaisi(zeroSiNegatif(resultatBrutDepense));

		// Brut Recette = Total Classe 6 - Total Classe 7
		BigDecimal resultatBrutRecette = (chap60_66.add(chap67_68)).subtract(chap70_76.add(chap77_78));
		resultatBrutRecetteCompte.setBdsnSaisi(zeroSiNegatif(resultatBrutRecette));

		// CAF = (Total_7 - Total_6) + Total(67_68) - Total(77_78)
		BigDecimal caf =
			(chap70_76.add(chap77_78)).subtract(chap60_66.add(chap67_68))
			.add(chap675_68)
			.subtract(chap775_776_777_78);


		// IAF = - CAF
		BigDecimal iaf = zeroSiNegatif(caf.multiply(new BigDecimal(-1)));

		caf = zeroSiNegatif(caf);

		cafCompte.setBdsnSaisi(caf);
		//cafCompte.setBdsnSaisi(cafCompte.bdsnMontant().subtract(cafCompte.bdsnVote()));

		iafCompte.setBdsnSaisi(iaf);
		// iafCompte.setBdsnSaisi(iafCompte.bdsnMontant().subtract(iafCompte.bdsnVote()));

		// Augmentation FDR = zeroSiNegatif ( (CAF+classe1) - (IAF+classe2) )
		augmentationFDRCompte.setBdsnSaisi(zeroSiNegatif( (caf.add(classe1).add(chap775)).subtract(iaf.add(classe2)) ));
		//augmentationFDRCompte.setBdsnSaisi(augmentationFDRCompte.bdsnMontant().subtract(augmentationFDRCompte.bdsnVote()));

		// Diminution FDR = zeroSiNegatif ( (IAF+classe2) - (CAF+classe1) )
		diminutionFDRCompte.setBdsnSaisi(zeroSiNegatif( (iaf.add(classe2)).subtract(caf.add(classe1).add(chap775)) ));
		//diminutionFDRCompte.setBdsnSaisi(diminutionFDRCompte.bdsnMontant().subtract(diminutionFDRCompte.bdsnVote()));

	}






}
