package org.cocktail.bibasse.client.virements;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.finder.FinderBudgetVoteGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetVoteNature;
import org.cocktail.bibasse.client.metier.EOBudgetVoteGestion;
import org.cocktail.bibasse.client.metier.EOBudgetVoteNature;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class VirementsOrigine {
	
	private static VirementsOrigine sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;
	
	protected	JDialog mainWindow;
	private		JPanel mainPanel;
	private 	CellEditor myCellEditor = new CellEditor(new JTextField());
			
	private JPanel viewTableNatureDepense, viewTableGestionDepense;
	private EODisplayGroup eodNatureDepense, eodGestionDepense;
	private ZEOTable myEOTableNatureDepense, myEOTableGestionDepense;
	private ZEOTableModel myTableModelNatureDepense, myTableModelGestionDepense;
	private TableSorter myTableSorterNatureDepense, myTableSorterGestionDepense;
	
	private ListenerGestionDepense myListenerGestionDepense = new ListenerGestionDepense();
	private ListenerNatureDepense myListenerNatureDepense = new ListenerNatureDepense();

	private NatureRendererDepense	rendererNatureDepense = new NatureRendererDepense();
	private GestionRendererDepense	rendererGestionDepense = new GestionRendererDepense();

	private JPanel viewTableNatureRecette, viewTableGestionRecette;
	private EODisplayGroup eodNatureRecette, eodGestionRecette;
	private ZEOTable myEOTableNatureRecette, myEOTableGestionRecette;
	private ZEOTableModel myTableModelNatureRecette, myTableModelGestionRecette;
	private TableSorter myTableSorterNatureRecette, myTableSorterGestionRecette;
	
	private ListenerGestionRecette myListenerGestionRecette = new ListenerGestionRecette();
	private ListenerNatureRecette myListenerNatureRecette = new ListenerNatureRecette();

	private NatureRendererRecette	rendererNatureRecette = new NatureRendererRecette();
	private GestionRendererRecette	rendererGestionRecette = new GestionRendererRecette();
	
	private JTextField montantNatureDepense, montantGestionDepense;
	private JTextField montantNatureRecette, montantGestionRecette;
	
	private EOOrgan currentOrganOrigine;

	/**
	 * 
	 *
	 */
	public VirementsOrigine(EOEditingContext editingContext)	{
		
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		initGUI();
		
		initView();
	}
	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static VirementsOrigine sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new VirementsOrigine(editingContext);
		return sharedInstance;
	}
	
	

	/**
	 * 
	 * @return
	 */
	private JPanel buildCenterPanel()	{
		
		JPanel panel = new JPanel(new BorderLayout());
				
		JLabel titreTotalNatureDepense = new JLabel("TOTAL NATURE DEPENSE : ");
		JLabel titreTotalNatureRecette = new JLabel("TOTAL NATURE RECETTE : ");
		
		montantNatureDepense = new JTextField("0.00");
		montantNatureDepense.setPreferredSize(new Dimension(125,21));
		montantNatureDepense.setEditable(false);
		montantNatureDepense.setFocusable(false);
		
		montantNatureDepense.setHorizontalAlignment(JTextField.CENTER);

		montantNatureRecette = new JTextField("0.00");
		montantNatureRecette.setPreferredSize(new Dimension(125,21));
		montantNatureRecette.setEditable(false);
		montantNatureRecette.setFocusable(false);
		
		montantNatureRecette.setHorizontalAlignment(JTextField.CENTER);

		JPanel panelNatureDepense = new JPanel(new BorderLayout());
		
		panelNatureDepense.add(new JLabel("Bubdget par Nature - DEPENSES"), BorderLayout.NORTH);
		panelNatureDepense.add(viewTableNatureDepense, BorderLayout.CENTER);

		JPanel panelNatureRecette = new JPanel(new BorderLayout());
		panelNatureRecette.add(new JLabel("Bubdget par Nature - RECETTES"), BorderLayout.NORTH);
		panelNatureRecette.add(viewTableNatureRecette, BorderLayout.CENTER);

		ArrayList arrayList = new ArrayList();
		arrayList.add(panelNatureDepense);
		arrayList.add(panelNatureRecette);
		
		JPanel panelNature = new JPanel(new BorderLayout());
		panelNature.add(ZUiUtil.buildBoxLine(arrayList), BorderLayout.CENTER);
		panelNature.setBorder(BorderFactory.createEmptyBorder(5,2, 5,0));

		JPanel southPanelNature = new JPanel(new FlowLayout());
		southPanelNature.add(titreTotalNatureDepense);
		southPanelNature.add(montantNatureDepense);
		southPanelNature.add(titreTotalNatureRecette);
		southPanelNature.add(montantNatureRecette);
		panelNature.add(southPanelNature, BorderLayout.SOUTH);

		// GESTION
		JLabel titreTotalGestionDepense = new JLabel("TOTAL GESTION DEPENSE : ");
		JLabel titreTotalGestionRecette = new JLabel("TOTAL GESTION RECETTE : ");

		montantGestionDepense = new JTextField("0.00");
		montantGestionDepense.setPreferredSize(new Dimension(125,21));
		montantGestionDepense.setEditable(false);
		montantGestionDepense.setFocusable(false);
		
		montantGestionDepense.setHorizontalAlignment(JTextField.CENTER);

		montantGestionRecette = new JTextField("0.00");
		montantGestionRecette.setPreferredSize(new Dimension(125,21));
		montantGestionRecette.setEditable(false);
		montantGestionRecette.setFocusable(false);
		
		montantGestionRecette.setHorizontalAlignment(JTextField.CENTER);

		arrayList = new ArrayList();
		
		JPanel panelGestionDepense = new JPanel(new BorderLayout());
		panelGestionDepense.add(new JLabel("Bubdget de Gestion - DEPENSES"), BorderLayout.NORTH);
		panelGestionDepense.add(viewTableGestionDepense, BorderLayout.CENTER);

		JPanel panelGestionRecette = new JPanel(new BorderLayout());
		panelGestionRecette.add(new JLabel("Bubdget de Gestion - RECETTES"), BorderLayout.NORTH);
		panelGestionRecette.add(viewTableGestionRecette, BorderLayout.CENTER);

		arrayList.add(panelGestionDepense);
		arrayList.add(panelGestionRecette);
		
		JPanel panelGestion = new JPanel(new BorderLayout());
		panelGestion.add(ZUiUtil.buildBoxLine(arrayList), BorderLayout.CENTER);
		panelGestion.setBorder(BorderFactory.createEmptyBorder(5,2, 5,0));
		
		JPanel southPanelGestion = new JPanel(new FlowLayout());
		southPanelGestion.add(titreTotalGestionDepense);
		southPanelGestion.add(montantGestionDepense);
		
		southPanelGestion.add(titreTotalGestionRecette);
		southPanelGestion.add(montantGestionRecette);
		panelGestion.add(southPanelGestion, BorderLayout.SOUTH);
		
		arrayList = new ArrayList();
		arrayList.add(panelNature);
		arrayList.add(panelGestion);
		
		panel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.CENTER);
				
		return panel;
		
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
		mainWindow = new JDialog(NSApp.superviseur().mainFrame(), "Virements", true);
		mainWindow.setTitle("BIBASSE - VIREMENTS");
		
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.setPreferredSize(new Dimension(900, 650));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		mainPanel.add(buildCenterPanel(), BorderLayout.CENTER);
				
		mainWindow.setContentPane(mainPanel);
		mainWindow.pack();
		
	}
	
	/**
	 * 
	 * @return
	 */
	public JPanel getMainPanel()	{
		return mainPanel;
	}
		
	public void clean()	{
		
		currentOrganOrigine = null;
		
		eodNatureDepense.setObjectArray(new NSArray());
		eodNatureRecette.setObjectArray(new NSArray());
		eodGestionDepense.setObjectArray(new NSArray());
		eodGestionRecette.setObjectArray(new NSArray());
		
		myEOTableNatureDepense.updateData();
		myEOTableNatureRecette.updateData();
		myEOTableGestionDepense.updateData();
		myEOTableGestionRecette.updateData();		
	}
	
	/**
	 * 
	 *
	 */
	public void open()	{
		
		clean();
		
		updateUI();
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
		
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class CellEditor extends ZEOTableModelColumn.ZEONumFieldTableCellEditor {
		
		private  JTextField myTextField;
		
		public CellEditor(JTextField textField) {
			super(textField, ConstantesCocktail.FORMAT_DECIMAL);
		}
		
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

			myTextField = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
			
			myTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
			myTextField.setEditable(true);
			
			return myTextField;
		}
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void initGUI() {
		
		eodNatureDepense = new EODisplayGroup();
		eodGestionDepense = new EODisplayGroup();

		eodNatureRecette = new EODisplayGroup();
		eodGestionRecette = new EODisplayGroup();

		viewTableGestionDepense = new JPanel();	
		viewTableNatureDepense = new JPanel();	

		viewTableGestionRecette = new JPanel();	
		viewTableNatureRecette = new JPanel();	

		initTableModel();
		initTable();
		
		myEOTableGestionDepense.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableGestionDepense.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableGestionDepense.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableGestionDepense.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableGestionDepense.removeAll();
		viewTableGestionDepense.setLayout(new BorderLayout());
		viewTableGestionDepense.add(new JScrollPane(myEOTableGestionDepense), BorderLayout.CENTER);
		
		myEOTableNatureDepense.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableNatureDepense.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableNatureDepense.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableNatureDepense.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableNatureDepense.removeAll();
		viewTableNatureDepense.setLayout(new BorderLayout());
		viewTableNatureDepense.add(new JScrollPane(myEOTableNatureDepense), BorderLayout.CENTER);

		
		myEOTableGestionRecette.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableGestionRecette.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableGestionRecette.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableGestionRecette.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableGestionRecette.removeAll();
		viewTableGestionRecette.setLayout(new BorderLayout());
		viewTableGestionRecette.add(new JScrollPane(myEOTableGestionRecette), BorderLayout.CENTER);
		
		myEOTableNatureRecette.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableNatureRecette.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableNatureRecette.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableNatureRecette.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableNatureRecette.removeAll();
		viewTableNatureRecette.setLayout(new BorderLayout());
		viewTableNatureRecette.add(new JScrollPane(myEOTableNatureRecette), BorderLayout.CENTER);

		
	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{
		
		myEOTableGestionDepense= new ZEOTable(myTableSorterGestionDepense, rendererGestionDepense);
		myTableModelGestionDepense.addTableModelListener(myListenerGestionDepense);
//		myTableSorterGestionDepense.setTableHeader(myEOTableGestionDepense.getTableHeader());		
		
		myEOTableNatureDepense= new ZEOTable(myTableSorterNatureDepense, rendererNatureDepense);
		myTableModelNatureDepense.addTableModelListener(myListenerNatureDepense);
//		myTableSorterNatureDepense.setTableHeader(myEOTableNatureDepense.getTableHeader());		

		
		myEOTableGestionRecette= new ZEOTable(myTableSorterGestionRecette, rendererGestionRecette);
		myTableModelGestionRecette.addTableModelListener(myListenerGestionRecette);
//		myTableSorterGestionRecette.setTableHeader(myEOTableGestionRecette.getTableHeader());		
		
		myEOTableNatureRecette= new ZEOTable(myTableSorterNatureRecette, rendererNatureRecette);
		myTableModelNatureRecette.addTableModelListener(myListenerNatureRecette);
//		myTableSorterNatureRecette.setTableHeader(myEOTableNatureRecette.getTableHeader());		

	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodNatureDepense, "TCD", "TCD", 40);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureDepense, "COMPTE", "Compte",  250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureDepense, "VOTE", "Voté", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureDepense, "OUVERT", "Ouvert", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureDepense, "SAISI", "Saisi", 75);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setColumnClass(BigDecimal.class);
		col.setTableCellEditor(myCellEditor);	
		col.setEditable(true);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureDepense, "RESULTAT", "Résultat", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		
		myTableModelNatureDepense = new ZEOTableModel(eodNatureDepense, myCols);
		myTableSorterNatureDepense = new TableSorter(myTableModelNatureDepense);
		
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodGestionDepense, "TCD", "TCD", 40);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionDepense, "ACTION", "Action / Sous-Action", 250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionDepense, "VOTE", "Voté", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionDepense, "OUVERT", "Ouvert", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionDepense, "SAISI", "Saisi", 75);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setColumnClass(BigDecimal.class);
		col.setTableCellEditor(myCellEditor);	
		col.setEditable(true);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionDepense, "RESULTAT", "Résultat", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		myTableModelGestionDepense = new ZEOTableModel(eodGestionDepense, myCols);
		myTableSorterGestionDepense = new TableSorter(myTableModelGestionDepense);
		

		// RECETTES
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodNatureRecette, "TCD", "TCD", 40);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureRecette, "COMPTE", "Compte",  250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureRecette, "VOTE", "Voté", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureRecette, "OUVERT", "Ouvert", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureRecette, "SAISI", "Saisi", 75);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setColumnClass(BigDecimal.class);
		col.setTableCellEditor(myCellEditor);	
		col.setEditable(true);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodNatureRecette, "RESULTAT", "Résultat", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		
		myTableModelNatureRecette = new ZEOTableModel(eodNatureRecette, myCols);
		myTableSorterNatureRecette = new TableSorter(myTableModelNatureRecette);
		
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodGestionRecette, "TCD", "TCD", 40);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionRecette, "ACTION", "Action / Sous-Action", 250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionRecette, "VOTE", "Voté", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionRecette, "OUVERT", "Ouvert", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionRecette, "SAISI", "Saisi", 75);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setColumnClass(BigDecimal.class);
		col.setTableCellEditor(myCellEditor);	
		col.setEditable(true);
		myCols.add(col);
		
		col = new ZEOTableModelColumn(eodGestionRecette, "RESULTAT", "Résultat", 75);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);
		
		myTableModelGestionRecette = new ZEOTableModel(eodGestionRecette, myCols);
		myTableSorterGestionRecette = new TableSorter(myTableModelGestionRecette);

	}
	
	
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class GestionRendererDepense extends ZEOTableCellRenderer		{
		
		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_FOND_INACTIF = new Color(200,200,200);
		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
						
			if (column < 2)	{ 				
					leComposant.setBackground(ConstantesCocktail.BG_COLOR_DEPENSES);
					leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);
			}
			else	{
				if (column != 4)	{ 	
					leComposant.setBackground(COULEUR_FOND_INACTIF);
					leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);
				}
				else	{
					leComposant.setBackground(COULEUR_FOND_SAISIE);
					leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);																
				}
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(ConstantesCocktail.BG_COLOR_WHITE);
			}
			
			return leComposant;
		}
	}
	
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class GestionRendererRecette extends ZEOTableCellRenderer		{
		
		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_FOND_INACTIF = new Color(200,200,200);
		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
						
			if (column < 2)	{ 					
					leComposant.setBackground(ConstantesCocktail.BG_COLOR_RECETTES);
					leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);
			}
			else	{
				if (column != 4)	{ 	
					leComposant.setBackground(COULEUR_FOND_INACTIF);
					leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);
				}
				else	{
					leComposant.setBackground(COULEUR_FOND_SAISIE);
					leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);																
				}
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(ConstantesCocktail.BG_COLOR_WHITE);
			}
			
			return leComposant;
		}
	}

	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class NatureRendererDepense extends ZEOTableCellRenderer		{
				
		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_FOND_INACTIF = new Color(200,200,200);
		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);																
			
			if (column < 2)	{ 					
				leComposant.setBackground(ConstantesCocktail.BG_COLOR_DEPENSES);
			}
			else	{
				if (column != 4)
					leComposant.setBackground(COULEUR_FOND_INACTIF);
				else
					leComposant.setBackground(COULEUR_FOND_SAISIE);
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(ConstantesCocktail.BG_COLOR_WHITE);
			}
			
			return leComposant;
		}
	}
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class NatureRendererRecette extends ZEOTableCellRenderer		{
				
		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_FOND_INACTIF = new Color(200,200,200);
		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			leComposant.setForeground(ConstantesCocktail.BG_COLOR_BLACK);																
			
			if (column < 2)	{ 					
				leComposant.setBackground(ConstantesCocktail.BG_COLOR_RECETTES);
			}
			else	{
				if (column != 4)
					leComposant.setBackground(COULEUR_FOND_INACTIF);
				else
					leComposant.setBackground(COULEUR_FOND_SAISIE);
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(ConstantesCocktail.BG_COLOR_WHITE);
			}
			
			return leComposant;
		}
	}

	
	
	/**
	 * 
	 *
	 */
	public void updateUI()	{
		
	}
	
	
	/**
	 * 
	 *
	 */
	public void updateMasquesSaisie()	{
		
		NSMutableArray arrayNaturesDepense = new NSMutableArray();				
		NSMutableArray arrayNaturesRecette = new NSMutableArray();

		NSMutableDictionary dicoNature = new NSMutableDictionary();

		// Nature
		NSArray natures = FinderBudgetVoteNature.findBudgetsVoteForVirement(ec, currentOrganOrigine,NSApp.getExerciceBudgetaire());
		
		for (int i=0;i<natures.count();i++)		{
			
			EOBudgetVoteNature budget = (EOBudgetVoteNature)natures.objectAtIndex(i);
			EOTypeCredit typeCredit = budget.typeCredit();
			
			dicoNature = new NSMutableDictionary();
			dicoNature.setObjectForKey(budget,"EO_BUDGET_VOTE_NATURE");
			dicoNature.setObjectForKey(typeCredit.tcdCode(),"TCD");
			dicoNature.setObjectForKey(budget.planComptable().pcoNum()+" - "+budget.planComptable().pcoLibelle(),"COMPTE");
			dicoNature.setObjectForKey(budget.bdvnVotes(),"VOTE");
			dicoNature.setObjectForKey(budget.bdvnOuverts(),"OUVERT");
			dicoNature.setObjectForKey(new BigDecimal(0.0),"SAISI");
			dicoNature.setObjectForKey(budget.bdvnOuverts(),"RESULTAT");
			
			if (typeCredit.isDepense())
				arrayNaturesDepense.addObject(dicoNature);
			else
				arrayNaturesRecette.addObject(dicoNature);
		}
		
		eodNatureDepense.setObjectArray(arrayNaturesDepense);
		myEOTableNatureDepense.updateData();
		myTableModelNatureDepense.fireTableDataChanged();
		
		eodNatureRecette.setObjectArray(arrayNaturesRecette);
		myEOTableNatureRecette.updateData();
		myTableModelNatureRecette.fireTableDataChanged();
		
		// GESTION
		NSMutableArray arrayGestionsDepense = new NSMutableArray();
		NSMutableArray arrayGestionsRecette = new NSMutableArray();

		NSMutableDictionary dicoGestion = new NSMutableDictionary();

		NSArray gestions = FinderBudgetVoteGestion.findBudgetsVoteForVirement(ec, currentOrganOrigine, NSApp.getExerciceBudgetaire());
		
		for (int i=0;i<gestions.count();i++)		{
			
			EOBudgetVoteGestion budget = (EOBudgetVoteGestion)gestions.objectAtIndex(i);
			EOTypeCredit typeCredit = budget.typeCredit();

			dicoGestion = new NSMutableDictionary();
			dicoGestion.setObjectForKey(budget,"EO_BUDGET_VOTE_GESTION");
			dicoGestion.setObjectForKey(typeCredit.tcdCode(),"TCD");
			dicoGestion.setObjectForKey(budget.typeAction().tyacCode()+" - "+budget.typeAction().tyacLibelle(),"ACTION");
			dicoGestion.setObjectForKey(budget.bdvgOuverts(),"OUVERT");
			dicoGestion.setObjectForKey(budget.bdvgVotes(),"VOTE");
			dicoGestion.setObjectForKey(new BigDecimal(0.0),"SAISI");
			dicoGestion.setObjectForKey(budget.bdvgOuverts(),"RESULTAT");
			
			if (typeCredit.isDepense())
				arrayGestionsDepense.addObject(dicoGestion);
			else
				arrayGestionsRecette.addObject(dicoGestion);
		}
		
		eodGestionDepense.setObjectArray(arrayGestionsDepense);
		myEOTableGestionDepense.updateData();
		myTableModelGestionDepense.fireTableDataChanged();

		eodGestionRecette.setObjectArray(arrayGestionsRecette);
		myEOTableGestionRecette.updateData();
		myTableModelGestionRecette.fireTableDataChanged();

		
	}
		
	/**
	 * 
	 * @param organ
	 */
	public void setOrganOrigine(EOOrgan organ)	{
	
		currentOrganOrigine = organ;
		
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerGestionRecette implements TableModelListener 	{
		
		public void tableChanged(TableModelEvent e) {
			
			updateRowGestion((NSMutableDictionary)eodGestionRecette.selectedObject());
			updateCumuls();
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerGestionDepense implements TableModelListener 	{
		
		public void tableChanged(TableModelEvent e) {
			
			updateRowGestion((NSMutableDictionary)eodGestionDepense.selectedObject());
			updateCumuls();
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerNatureRecette implements TableModelListener 	{
		
		public void tableChanged(TableModelEvent e) {
						
			updateRowNature((NSMutableDictionary)eodNatureRecette.selectedObject());
			updateCumuls();
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerNatureDepense implements TableModelListener 	{
		
		public void tableChanged(TableModelEvent e) {
			
				updateRowNature((NSMutableDictionary)eodNatureDepense.selectedObject());
				updateCumuls();
		}
	}
	
	
	/**
	 * 
	 *
	 */
	public void updateRowGestion(NSMutableDictionary myRow)	{
		
		myTableModelGestionDepense.removeTableModelListener(myListenerGestionDepense);
		
		try {
			NSMutableDictionary selectedRow = new NSMutableDictionary(myRow);
			
			BigDecimal total = new BigDecimal(0.0);
			
			String saisi = StringCtrl.replace(selectedRow.objectForKey("SAISI").toString(), ",",".");
			String vote = StringCtrl.replace(selectedRow.objectForKey("OUVERT").toString(), ",",".");
			
			total = new BigDecimal(vote).subtract(new BigDecimal(saisi));
			
			myRow.setObjectForKey(total, "RESULTAT");			
			
			
		}
		catch (Exception e)	{
		}
		myTableModelGestionDepense.addTableModelListener(myListenerGestionDepense);
	}
	
	
	
	/**
	 * 
	 *
	 */
	public void updateCumuls()	{
		
		BigDecimal sumS1 = NSApp.computeSumForKey(eodNatureDepense, "SAISI");
		BigDecimal sumS2 = NSApp.computeSumForKey(eodGestionDepense, "SAISI");
				
		montantNatureDepense.setText(sumS1.toString());
		montantGestionDepense.setText(sumS2.toString());

		
		BigDecimal sumS11 = NSApp.computeSumForKey(eodNatureRecette, "SAISI");
		BigDecimal sumS22 = NSApp.computeSumForKey(eodGestionRecette, "SAISI");
				
		montantNatureRecette.setText(sumS11.toString());
		montantGestionRecette.setText(sumS22.toString());

	}

	
	
	/**myListenerGestion
	 * 
	 *
	 */
	public void updateRowNature(NSMutableDictionary myRow)	{
		
		myTableModelNatureDepense.removeTableModelListener(myListenerNatureDepense);
		try {
			NSMutableDictionary selectedRow = new NSMutableDictionary(myRow);
			
			BigDecimal total = new BigDecimal(0.0);
			
			String saisi = StringCtrl.replace(selectedRow.objectForKey("SAISI").toString(), ",",".");
			String vote = StringCtrl.replace(selectedRow.objectForKey("VOTE").toString(), ",",".");
			
			total = new BigDecimal(vote).subtract(new BigDecimal(saisi));
			
			myRow.setObjectForKey(total, "RESULTAT");		
			
		}
		catch (Exception e)	{
		}
		myTableModelNatureDepense.addTableModelListener(myListenerNatureDepense);
	}
	
	
	/**
	 * 
	 * @return
	 */
	public boolean testSaisieValide()	{

		BigDecimal totalGestionDepense = new BigDecimal(montantGestionDepense.getText());
		BigDecimal totalNatureDepense = new BigDecimal(montantNatureDepense.getText());

		BigDecimal totalGestionRecette = new BigDecimal(montantGestionRecette.getText());
		BigDecimal totalNatureRecette = new BigDecimal(montantNatureRecette.getText());

		if (totalGestionDepense.floatValue()!=totalNatureDepense.floatValue())	{
			EODialogs.runInformationDialog("ATTENTION","VIREMENTS DEPENSE NATURE <> VIREMENTS DEPENSE GESTION !");			
			return false;			
		}
		
		if (totalGestionRecette.floatValue()!=totalNatureRecette.floatValue())	{
			EODialogs.runInformationDialog("ATTENTION","VIREMENTS RECETTE NATURE <> VIREMENTS RECETTE GESTION !");			
			return false;			
		}
		
		if (totalGestionDepense.floatValue()==0.0 && totalGestionRecette.floatValue()==0.0)	{
			EODialogs.runInformationDialog("ATTENTION","Le montant global des virements doit être > 0 !");			
			return false;			
		}
		
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public BigDecimal getCumulNatureForTypeCredit(EOTypeCredit tcd)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("TCD = %@", new NSArray(tcd.tcdCode())));
		
		NSMutableArray budgetsDepenses = new NSMutableArray(eodNatureDepense.displayedObjects());
		
		NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsDepenses,  new EOAndQualifier(mesQualifiers));
		
		EODisplayGroup myEod = new EODisplayGroup();
		myEod.setObjectArray(filtres);
		
		return NSApp.computeSumForKey(myEod, "SAISI");
		
	}


	public NSArray getDepensesNature()	{
		return eodNatureDepense.displayedObjects();
	}

	public NSArray getDepensesGestion()	{
		return eodGestionDepense.displayedObjects();
	}

	public NSArray getRecettesNature()	{
		return eodNatureRecette.displayedObjects();
	}

	public NSArray getRecettesGestion()	{
		return eodGestionRecette.displayedObjects();
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getCumulGestionForTypeCredit(EOTypeCredit tcd)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("TCD = %@", new NSArray(tcd.tcdCode())));
		
		NSMutableArray budgetsDepenses = new NSMutableArray(eodGestionDepense.displayedObjects());
		
		NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsDepenses,  new EOAndQualifier(mesQualifiers));
		
		EODisplayGroup myEod = new EODisplayGroup();
		myEod.setObjectArray(filtres);
				
		return NSApp.computeSumForKey(myEod, "SAISI");
		
	}

}
