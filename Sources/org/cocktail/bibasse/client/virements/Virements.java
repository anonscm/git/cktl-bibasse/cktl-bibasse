package org.cocktail.bibasse.client.virements;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.finder.FinderTypeCredit;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.finder.FinderTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.finder.FinderTypeSens;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOBudgetVoteGestion;
import org.cocktail.bibasse.client.metier.EOBudgetVoteNature;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOTypeSens;
import org.cocktail.bibasse.client.process.mouvement.ProcessMouvements;
import org.cocktail.bibasse.client.selectors.OrganSelectPanel;
import org.cocktail.bibasse.client.utils.CocktailUtilities;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class Virements {

	private static Virements sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	protected	JDialog 	mainWindow;
	private		JPanel 		mainPanel;

	private JLabel labelOrigine, labelDestination, labelTypeCredit, labelDisponible, libelleDisponible;
	private JTextField libelleOrigine, libelleDestination, libelleVirement;
	private JButton buttonGetOrigine, buttonGetDestination;

	private		JTabbedPane	onglets;

	private EOOrgan currentOrganOrigine, currentOrganDestination;

	private ActionGetOrigine actionGetOrigine = new ActionGetOrigine();
	private ActionGetDestination actionGetDestination = new ActionGetDestination();

	private ActionHistoVirements actionHistoVirements = new ActionHistoVirements();

	private ActionCancel actionCancel = new ActionCancel();
	private ActionValidate actionValidate = new ActionValidate();

	/**
	 *
	 *
	 */
	public Virements(EOEditingContext editingContext)	{

		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		initView();
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static Virements sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new Virements(editingContext);
		return sharedInstance;
	}



	/**
	 *
	 * @return
	 */
	private JPanel buildSouthPanel()	{

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionValidate);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 22));

		arrayList = new ArrayList();
		arrayList.add(actionHistoVirements);
		JPanel panelWestButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 175, 22));

		actionHistoVirements.setEnabled(false);

		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelWestButtons, BorderLayout.WEST);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		return panelSouth;

	}


	/**
	 *
	 *
	 */
	private void initButtons()	{

		buttonGetOrigine = new JButton(actionGetOrigine);
		buttonGetOrigine.setPreferredSize(new Dimension(21,21));

		buttonGetDestination = new JButton(actionGetDestination);
		buttonGetDestination.setPreferredSize(new Dimension(21,21));

	}


	/**
	 *
	 *
	 */
	private void initTextFields()	{

		labelTypeCredit = new JLabel("Type de crédit : ");
		labelTypeCredit.setHorizontalAlignment(JTextField.RIGHT);
		labelTypeCredit.setPreferredSize(new Dimension(100, 20));

		labelDisponible = new JLabel("Disponible : ");
		labelDisponible.setHorizontalAlignment(JTextField.RIGHT);
		labelDisponible.setPreferredSize(new Dimension(100, 20));

		libelleDisponible = new JLabel("");
		libelleDisponible.setHorizontalAlignment(JTextField.RIGHT);
		libelleDisponible.setPreferredSize(new Dimension(100, 20));
		libelleDisponible.setForeground(ConstantesCocktail.BG_COLOR_BLUE);

		labelOrigine = new JLabel("CR Origine : ");
		labelOrigine.setHorizontalAlignment(JTextField.RIGHT);
		labelOrigine.setPreferredSize(new Dimension(100, 20));
		labelDestination = new JLabel("CR Destination : ");
		labelDestination.setPreferredSize(new Dimension(100, 20));
		labelDestination.setHorizontalAlignment(JTextField.RIGHT);

		libelleVirement = new JTextField("");
		libelleVirement.setHorizontalAlignment(JTextField.CENTER);
		libelleVirement.setColumns(40);

		libelleOrigine = new JTextField("");
		libelleOrigine.setHorizontalAlignment(JTextField.CENTER);
		libelleOrigine.setColumns(30);
		CocktailUtilities.initTextField(libelleOrigine, false, false);

		libelleDestination = new JTextField("");
		libelleDestination.setHorizontalAlignment(JTextField.CENTER);
		libelleDestination.setColumns(30);
		CocktailUtilities.initTextField(libelleDestination, false, false);

	}


	/**
	 *
	 * @return
	 */
	private JPanel buildNorthPanel()	{

		JPanel panel = new JPanel(new BorderLayout());

		JPanel panelOrigine = (JPanel)ZUiUtil.buildBoxLine(new Component[] {labelOrigine, libelleOrigine, buttonGetOrigine}, BorderLayout.WEST);
		panelOrigine.setBorder(BorderFactory.createEmptyBorder(5,2, 5, 20));

		JPanel panelDestination = (JPanel)ZUiUtil.buildBoxLine(new Component[] {labelDestination, libelleDestination, buttonGetDestination}, BorderLayout.WEST);
		panelDestination.setBorder(BorderFactory.createEmptyBorder(5,2, 5, 0));

		ArrayList arrayListPanels = new ArrayList();

		ArrayList arrayList = new ArrayList();
		arrayList.add(panelOrigine);
		arrayList.add(panelDestination);

		arrayListPanels.add(ZUiUtil.buildBoxLine(arrayList));

		panel.add(ZUiUtil.buildBoxColumn(arrayListPanels), BorderLayout.NORTH);

		return panel;

	}


	/**
	 *
	 * @return
	 */
	public JPanel buildCenterPanel()	{

		JPanel panel = new JPanel(new BorderLayout());

		panel.add(onglets, BorderLayout.CENTER);

		JLabel labelLibelle = new JLabel("Libellé VIREMENT : ");
		labelLibelle.setForeground(ConstantesCocktail.BG_COLOR_RED);

		JPanel southPanelLibelle = new JPanel(new FlowLayout());
		southPanelLibelle.add(labelLibelle);
		southPanelLibelle.add(libelleVirement);
		panel.add(southPanelLibelle, BorderLayout.SOUTH);

		return panel;

	}

	/**
	 *
	 *
	 */
	private void initView()	{

		mainWindow = new JDialog(NSApp.superviseur().mainFrame(), "Virements", true);
		mainWindow.setTitle("BIBASSE - VIREMENTS");

		mainPanel = new JPanel(new BorderLayout());
		mainPanel.setPreferredSize(new Dimension(1100, 650));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		// Les onglets
		onglets = new JTabbedPane();
		onglets.addTab("   	Origine   ", null, VirementsOrigine.sharedInstance(ec).getMainPanel());
		onglets.addTab("   	Destination   ", null, VirementsDestination.sharedInstance(ec).getMainPanel());

		onglets.setBackgroundAt(0,new Color(166,173,201));

		initTextFields();

		initButtons();

		mainPanel.add(buildNorthPanel(), BorderLayout.NORTH);

		mainPanel.add(buildCenterPanel(), BorderLayout.CENTER);

		mainPanel.add(buildSouthPanel(), BorderLayout.SOUTH);

		mainWindow.setContentPane(mainPanel);
		mainWindow.pack();
	}


	/**
	 *
	 *
	 */
	public void clean()	{

		currentOrganDestination = null;
		currentOrganOrigine = null;

		libelleDestination.setText("");
		libelleOrigine.setText("");

		libelleVirement.setText("");
	}

	/**
	 *
	 *
	 */
	public void open()	{

		clean();

		updateUI();
		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();

	}

	/**
	 *
	 *
	 */
	public void updateUI()	{

		actionGetDestination.setEnabled(currentOrganOrigine != null);

		actionHistoVirements.setEnabled(currentOrganOrigine != null);

		onglets.setEnabledAt(0, currentOrganOrigine != null);

		onglets.setEnabledAt(1, false);//currentOrganDestination != null);

	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionGetOrigine extends AbstractAction {

		public ActionGetOrigine() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SELECT);
		}


		public void actionPerformed(ActionEvent e) {

			EOOrgan origine = OrganSelectPanel.sharedInstance(ec, NSApp.superviseur().mainFrame()).getOrgan(NSApp.getExerciceBudgetaire(), NSApp.getUserOrgans());

			if (origine != null)	{

				if (NSApp.isSaisieCr())	{
					if (origine.orgNiveau().intValue() != 3)	{
						EODialogs.runErrorDialog("ERREUR","La saisie du budget a été faite au niveau CR." +
								"\nVous devez donc sélectionner une origine de niveau 3.");
						return;
					}
				}
				else	{
					if (origine.orgNiveau().intValue() != 2)	{
						EODialogs.runErrorDialog("ERREUR","La saisie du budget a été faite au niveau UB." +
								"\nVous devez donc sélectionner une origine de niveau 2.");
						return;
					}
				}

				currentOrganOrigine = origine;
				libelleOrigine.setText(currentOrganOrigine.getLongString());

				VirementsOrigine.sharedInstance(ec).setOrganOrigine(currentOrganOrigine);

				VirementsOrigine.sharedInstance(ec).updateMasquesSaisie();
			}

			updateUI();
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionGetDestination extends AbstractAction {

		public ActionGetDestination() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SELECT);
		}


		public void actionPerformed(ActionEvent e) {

			EOOrgan destination = OrganSelectPanel.sharedInstance(ec, NSApp.superviseur().mainFrame()).getOrgan(NSApp.getExerciceBudgetaire(), NSApp.getUserOrgans());

			if (destination != null)	{

				if (NSApp.isSaisieCr())	{
					if (destination.orgNiveau().intValue() != 3)	{
						EODialogs.runErrorDialog("ERREUR","La saisie du budget a été faite au niveau CR." +
								"\nVous devez donc sélectionner une destination de niveau 3.");
						return;
					}
				}
				else	{
					if (destination.orgNiveau().intValue() != 2)	{
						EODialogs.runErrorDialog("ERREUR","La saisie du budget a été faite au niveau UB." +
								"\nVous devez donc sélectionner une destination de niveau 2.");
						return;
					}
				}

				currentOrganDestination = destination;
				libelleDestination.setText(currentOrganDestination.getLongString());

				VirementsDestination.sharedInstance(ec).setOrganDestination(currentOrganDestination);

				VirementsDestination.sharedInstance(ec).updateMasquesSaisie();

				updateUI();
			}
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionValidate extends AbstractAction {

		public ActionValidate() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
		}


		public void actionPerformed(ActionEvent e) {

			// Mouvement de virement
			EOBudgetMouvements myMouvement = performVirements();

			if (myMouvement != null)	{

				ec.saveChanges();

				// Traitement SQL du mouvement
				try {
					NSMutableDictionary parametres = new NSMutableDictionary();

					parametres.setObjectForKey((EOEnterpriseObject)myMouvement, "EOBudgetMouvement");
					ServerProxy.clientSideRequestTraiterMouvement(ec, parametres);

				}
				catch (Exception ex)	{

					ex.printStackTrace();
					EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement des virements !"+NSApp.getErrorDialog(ex));

					try {

						ex.printStackTrace();
						ec.revert();

						// Annulation des mouvements inseres dans la base de donnees
						NSMutableDictionary parametres = new NSMutableDictionary();

						parametres.setObjectForKey((EOEnterpriseObject)myMouvement, "EOBudgetMouvement");
						ServerProxy.clientSideRequestAnnulerMouvement(ec, parametres);

						return;
					}
					catch (Exception exception)	{
						exception.printStackTrace();
						EODialogs.runErrorDialog("ERREUR","Erreur d'annulation des mouvements effectues !!!");
					}

				}

				EODialogs.runInformationDialog("OK","Les virements ont bien été effectués de la ligne " +
						currentOrganOrigine.getLongString() + " vers la ligne " + currentOrganDestination.getLongString() + "!");

				clean();
			}

		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

		public ActionCancel() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
		}


		public void actionPerformed(ActionEvent e) {

			try {
				ec.revert();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
			}
			mainWindow.dispose();

		}
	}


	/**
	 *
	 *
	 */
	public void showHistoriqueVirements()	{

		VirementsHistorique.sharedInstance(ec, NSApp.superviseur().mainFrame()).openHistorique(currentOrganOrigine);

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionHistoVirements extends AbstractAction {

		public ActionHistoVirements() {
			super("Historique des virements");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
		}

		public void actionPerformed(ActionEvent e) {
			showHistoriqueVirements();
		}
	}

	/**
	 *
	 * @return
	 */
	private boolean testSaisieValide()	{

		if (currentOrganOrigine == null)	{
			EODialogs.runInformationDialog("ATTENTION","Veuillez renseigner une ligne budgétaire ORIGINE !");
			return false;
		}

		if (currentOrganDestination == null)	{
			EODialogs.runInformationDialog("ATTENTION","Veuillez renseigner une ligne budgétaire DESTINATION !");
			return false;
		}

		if (StringCtrl.chaineVide(libelleVirement.getText()))	{
			EODialogs.runInformationDialog("ATTENTION","Veuillez renseigner un LIBELLE pour ce virement !");
			return false;
		}

		if (!VirementsOrigine.sharedInstance(ec).testSaisieValide())	{
			return false;
		}

		if (!VirementsDestination.sharedInstance(ec).testSaisieValide())	{
			return false;
		}

		return true;

	}

	/**
	 *
	 *
	 */
	private EOBudgetMouvements performVirements() {


		EOBudgetMouvements currentMouvement = null;

		// Verifier que toutes les sommes ne soient pas a 0
		if (!testSaisieValide())
			return null;

		try {

			// Creation d'un mouvement
			ProcessMouvements myProcessMouvements = new ProcessMouvements(false);

			currentMouvement = myProcessMouvements.creerMouvement(ec,
					NSApp.getUtilisateur(),
					FinderTypeMouvementBudgetaire.findTypeMouvement(ec, EOTypeMouvementBudgetaire.MOUVEMENT_VIREMENT),
					NSApp.getExerciceBudgetaire(),
					FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
					StringCtrl.chaineVide(libelleVirement.getText())?"VIREMENT":libelleVirement.getText(),
							new NSTimestamp());

			EOTypeSens typeSensDebit = FinderTypeSens.findTypeSens(ec, EOTypeSens.TYPE_SENS_DEBIT);
			EOTypeSens typeSensCredit = FinderTypeSens.findTypeSens(ec, EOTypeSens.TYPE_SENS_CREDIT);

			NSArray saisiesDepenseNature = VirementsOrigine.sharedInstance(ec).getDepensesNature();
			NSArray saisiesDepenseGestion = VirementsOrigine.sharedInstance(ec).getDepensesGestion();;
			NSArray saisiesRecetteNature = VirementsOrigine.sharedInstance(ec).getRecettesNature();;
			NSArray saisiesRecetteGestion = VirementsOrigine.sharedInstance(ec).getRecettesGestion();;

			//
			// Creation des lignes de virement NATURE DEPENSE
			for (int i=0;i<saisiesDepenseNature.count();i++)	{

				NSDictionary dico = (NSDictionary)saisiesDepenseNature.objectAtIndex(i);

				EOBudgetVoteNature budget = (EOBudgetVoteNature)dico.objectForKey("EO_BUDGET_VOTE_NATURE");

				BigDecimal saisi = new BigDecimal(dico.objectForKey("SAISI").toString());
				BigDecimal ouvert = new BigDecimal(dico.objectForKey("OUVERT").toString());

				if (saisi.floatValue() < 0.0)	{
					EODialogs.runErrorDialog("ERREUR","Veuillez ne saisir que des montants positifs !");
					return null;
				}

				if (!(saisi.floatValue() == 0.0))	{

					// On regarde qu'il y ait bien assez de dispo sur le CR pour effectuer la ventilation
					if (saisi.floatValue() > ouvert.floatValue())	{
						EODialogs.runErrorDialog("ERREUR","Crédits Ouverts Insuffisants pour le compte " + budget.planComptable().pcoNum());
						return null;
					}

					// Origine
					myProcessMouvements.creerUneLigneEOBudgetMouvNature(
							ec,
							typeSensDebit,
							budget.typeCredit(),
							budget.planComptable(),
							currentOrganOrigine,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi.multiply(new BigDecimal(-1))
					);

					// Destination
					myProcessMouvements.creerUneLigneEOBudgetMouvNature(
							ec,
							typeSensCredit,
							budget.typeCredit(),
							budget.planComptable(),
							currentOrganDestination,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi
					);
				}
			}


			// Creation des lignes de virement GESTION DEPENSE
			for (int i=0;i<saisiesDepenseGestion.count();i++)	{

				NSDictionary dico = (NSDictionary)saisiesDepenseGestion.objectAtIndex(i);

				EOBudgetVoteGestion budget = (EOBudgetVoteGestion)dico.objectForKey("EO_BUDGET_VOTE_GESTION");

				BigDecimal saisi = new BigDecimal(dico.objectForKey("SAISI").toString());
				BigDecimal ouvert = new BigDecimal(dico.objectForKey("OUVERT").toString());

				if (saisi.floatValue() < 0.0)	{
					EODialogs.runErrorDialog("ERREUR","Veuillez ne saisir que des montants positifs !");
					return null;
				}

				if (!(saisi.floatValue() == 0.0))	{

					// On regarde qu'il y ait bien assez de dispo sur le CR pour effectuer le virement
					if (saisi.floatValue() > ouvert.floatValue())	{
						EODialogs.runErrorDialog("ERREUR","Crédits Ouverts Insuffisants pour l'action " + budget.typeAction().tyacCode());
						return null;
					}

					// Origine
					myProcessMouvements.creerUneLigneEOBudgetMouvGestion(
							ec,
							typeSensDebit,
							budget.typeCredit(),
							budget.typeAction(),
							currentOrganOrigine,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi.multiply(new BigDecimal(-1))
					);

					// Destination
					myProcessMouvements.creerUneLigneEOBudgetMouvGestion(
							ec,
							typeSensCredit,
							budget.typeCredit(),
							budget.typeAction(),
							currentOrganDestination,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi
					);
				}
			}



			// Creation des lignes de virement NATURE RECETTE
			for (int i=0;i<saisiesRecetteNature.count();i++)	{

				NSDictionary dico = (NSDictionary)saisiesRecetteNature.objectAtIndex(i);

				EOBudgetVoteNature budget = (EOBudgetVoteNature)dico.objectForKey("EO_BUDGET_VOTE_NATURE");

				BigDecimal saisi = new BigDecimal(dico.objectForKey("SAISI").toString());
				BigDecimal ouvert = new BigDecimal(dico.objectForKey("OUVERT").toString());

				if (saisi.floatValue() < 0.0)	{
					EODialogs.runErrorDialog("ERREUR","Veuillez ne saisir que des montants positifs !");
					return null;
				}

				if (!(saisi.floatValue() == 0.0))	{

					// On regarde qu'il y ait bien assez de dispo sur le CR pour effectuer la ventilation
					if (saisi.floatValue() > ouvert.floatValue())	{
						EODialogs.runErrorDialog("ERREUR","Crédits Ouverts Insuffisants pour le compte " + budget.planComptable().pcoNum());
						return null;
					}

					// Origine
					myProcessMouvements.creerUneLigneEOBudgetMouvNature(
							ec,
							typeSensDebit,
							budget.typeCredit(),
							budget.planComptable(),
							currentOrganOrigine,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi.multiply(new BigDecimal(-1))
					);

					// Destination
					myProcessMouvements.creerUneLigneEOBudgetMouvNature(
							ec,
							typeSensCredit,
							budget.typeCredit(),
							budget.planComptable(),
							currentOrganDestination,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi
					);
				}
			}


			// Creation des lignes de virement GESTION RECETTE
			for (int i=0;i<saisiesRecetteGestion.count();i++)	{

				NSDictionary dico = (NSDictionary)saisiesRecetteGestion.objectAtIndex(i);

				EOBudgetVoteGestion budget = (EOBudgetVoteGestion)dico.objectForKey("EO_BUDGET_VOTE_GESTION");

				BigDecimal saisi = new BigDecimal(dico.objectForKey("SAISI").toString());
				BigDecimal ouvert = new BigDecimal(dico.objectForKey("OUVERT").toString());

				if (saisi.floatValue() < 0.0)	{
					EODialogs.runErrorDialog("ERREUR","Veuillez ne saisir que des montants positifs !");
					return null;
				}

				if (!(saisi.floatValue() == 0.0))	{

					// On regarde qu'il y ait bien assez de dispo sur le CR pour effectuer le virement
					if (saisi.floatValue() > ouvert.floatValue())	{
						EODialogs.runErrorDialog("ERREUR","Crédits Ouverts Insuffisants pour l'action " + budget.typeAction().tyacCode());
						return null;
					}

					// Origine
					myProcessMouvements.creerUneLigneEOBudgetMouvGestion(
							ec,
							typeSensDebit,
							budget.typeCredit(),
							budget.typeAction(),
							currentOrganOrigine,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi.multiply(new BigDecimal(-1))
					);

					// Destination
					myProcessMouvements.creerUneLigneEOBudgetMouvGestion(
							ec,
							typeSensCredit,
							budget.typeCredit(),
							budget.typeAction(),
							currentOrganDestination,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							saisi
					);
				}
			}




			// BUDGET DE FONCTIONNEMENT
			NSArray typesCreditDepense = FinderTypeCredit.findTypesCredit(ec ,NSApp.getExerciceBudgetaire(), null, "DEPENSE");

			for (int i=0;i<typesCreditDepense.count();i++)	 {

				EOTypeCredit typeCredit = (EOTypeCredit)typesCreditDepense.objectAtIndex(i);

				BigDecimal montantTypeCreditNature = VirementsOrigine.sharedInstance(ec).getCumulNatureForTypeCredit(typeCredit);
				BigDecimal montantTypeCreditGestion = VirementsOrigine.sharedInstance(ec).getCumulGestionForTypeCredit(typeCredit);

				if (montantTypeCreditGestion.floatValue() != montantTypeCreditNature.floatValue()){

					String messageException = "Pour le type de crédit " + typeCredit.tcdCode() +
							", le montant des dépenses en nature (" + montantTypeCreditNature.toString() + ") " +
							" est différent du montant des dépenses en gestion ("+montantTypeCreditGestion.toString()+")";
					EODialogs.runErrorDialog("ERREUR",messageException);
					return null;
				}

				if (montantTypeCreditNature.floatValue() > 0)	{

					myProcessMouvements.creerUneLigneEOBudgetMouvCredit(
							ec,
							typeSensDebit,
							typeCredit,
							currentOrganOrigine,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							montantTypeCreditNature.multiply(new BigDecimal(-1))
					);

					myProcessMouvements.creerUneLigneEOBudgetMouvCredit(
							ec,
							typeSensCredit,
							typeCredit,
							currentOrganDestination,
							currentMouvement,
							NSApp.getExerciceBudgetaire(),
							NSApp.getUtilisateur(),
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
							montantTypeCreditNature
					);
				}
			}


    		return currentMouvement;

		} catch (Exception ex)	{
			return null;
		}

	}

}
