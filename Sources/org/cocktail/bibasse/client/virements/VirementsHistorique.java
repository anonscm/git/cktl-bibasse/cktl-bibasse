package org.cocktail.bibasse.client.virements;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.editions.EditionsCtrl;
import org.cocktail.bibasse.client.finder.FinderBudgetMouvGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetMouvNature;
import org.cocktail.bibasse.client.finder.FinderBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOBudgetMouvGestion;
import org.cocktail.bibasse.client.metier.EOBudgetMouvNature;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.utils.CocktailUtilities;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class VirementsHistorique  {

	
	private static VirementsHistorique sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eodMouvements, eodGestion, eodNature;
	private ZEOTable myEOTableMouvements, myEOTableGestion, myEOTableNature;
	private ZEOTableModel myTableModelMouvements, myTableModelGestion, myTableModelNature;
	private TableSorter myTableSorterMouvements, myTableSorterGestion, myTableSorterNature;

	protected ActionClose actionClose = new ActionClose();
	protected ActionPrint actionPrint = new ActionPrint();
	protected ActionSelectAll actionSelectAll = new ActionSelectAll();

	protected ActionGetDateDebut actionGetDateDebut = new ActionGetDateDebut();
	protected ActionGetDateFin actionGetDateFin = new ActionGetDateFin();

//	protected ActionFind actionFind = new ActionFind();
	
	private JTextField	filtreDateDebut, filtreDateFin;
	private JButton		buttonGetDateDebut, buttonGetDateFin, buttonSelectAll;

	protected JPanel viewTableMouvements, viewTableGestion, viewTableNature;
	
	private	EOOrgan currentOrgan;
	private	EOBudgetMouvements currentMouvement;

	/**
	 * 
	 *
	 */
	public VirementsHistorique(EOEditingContext editingContext, JFrame frame)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		mainFrame = frame;
		
		initGUI();
		initView();
		
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static VirementsHistorique sharedInstance(EOEditingContext editingContext, JFrame frame)	{
		if (sharedInstance == null)	
			sharedInstance = new VirementsHistorique(editingContext, frame);
		return sharedInstance;
	}
	
	private void initButtons()	{
		
		buttonGetDateDebut = new JButton(actionGetDateDebut);
		buttonGetDateDebut.setPreferredSize(new Dimension(20,20));
		
		buttonGetDateFin = new JButton(actionGetDateFin);
		buttonGetDateFin.setPreferredSize(new Dimension(20,20));
		buttonGetDateFin.setFocusable(false);

		buttonSelectAll = new JButton(actionSelectAll);
		buttonSelectAll.setPreferredSize(new Dimension(150,18));

	}
	
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
        mainWindow = new JDialog(mainFrame, "Historique des virements", true);

        viewTableMouvements.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        
        initButtons();
        
        filtreDateDebut = new JTextField("");
        filtreDateDebut.setColumns(10);
        CocktailUtilities.initTextField(filtreDateDebut, false, false);

        filtreDateFin = new JTextField("");
        filtreDateFin.setColumns(10);
        CocktailUtilities.initTextField(filtreDateFin, false, false);

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionPrint);
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 22));                
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		arrayList = new ArrayList();
		arrayList.add(viewTableGestion);
		arrayList.add(viewTableNature);
		
		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.add(ZUiUtil.buildBoxColumn(arrayList));
		
		JPanel westPanel = new JPanel(new BorderLayout());
		westPanel.add(viewTableMouvements);
		westPanel.add(buttonSelectAll, BorderLayout.SOUTH);
		westPanel.setPreferredSize(new Dimension(450,500));

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setPreferredSize(new Dimension(850, 600));
		mainView.add(westPanel, BorderLayout.WEST);
		mainView.add(centerPanel, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
		
	
	/**
	 * 
	 * @return
	 */
	public void openHistorique(EOOrgan organOrigine)	{

		currentOrgan = organOrigine;
		
		eodMouvements.setObjectArray(FinderBudgetMouvements.findMouvementsVirements(ec, NSApp.getExerciceBudgetaire(), currentOrgan));
		myEOTableMouvements.updateData();
		
		ZUiUtil.centerWindowInContainer(mainWindow);
		mainWindow.show();
		
	}
	
	/**
	 * 
	 *
	 */
	public void updateData()	{
			

			
	}
	
	public void updateUI()	{
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eodMouvements = new EODisplayGroup();
		eodGestion = new EODisplayGroup();
		eodNature = new EODisplayGroup();

		NSMutableArray mySortMouvements = new NSMutableArray();
		mySortMouvements.addObject(new EOSortOrdering("bmouDateCreation", EOSortOrdering.CompareDescending));
		eodMouvements.setSortOrderings(mySortMouvements);

		NSMutableArray mySortGestion = new NSMutableArray();
		mySortGestion.addObject(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareDescending));
		mySortGestion.addObject(new EOSortOrdering("bdmgMontant", EOSortOrdering.CompareAscending));
		eodGestion.setSortOrderings(mySortGestion);
		
		NSMutableArray mySortNature = new NSMutableArray();
		mySortNature.addObject(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareDescending));
		mySortNature.addObject(new EOSortOrdering("bdmnMontant", EOSortOrdering.CompareAscending));
		eodNature.setSortOrderings(mySortNature);

		viewTableMouvements = new JPanel();
        viewTableGestion = new JPanel();
        viewTableNature = new JPanel();
		
		initTableModel();
		initTable();
		
		myEOTableMouvements.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableMouvements.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableMouvements.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableMouvements.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableMouvements.removeAll();
		viewTableMouvements.setLayout(new BorderLayout());
		viewTableMouvements.add(new JScrollPane(myEOTableMouvements), BorderLayout.CENTER);

		
		myEOTableGestion.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableGestion.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableGestion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableGestion.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableGestion.removeAll();
		viewTableGestion.setLayout(new BorderLayout());
		viewTableGestion.add(new JScrollPane(myEOTableGestion), BorderLayout.CENTER);

		
		myEOTableNature.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableNature.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableNature.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableNature.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableNature.removeAll();
		viewTableNature.setLayout(new BorderLayout());
		viewTableNature.add(new JScrollPane(myEOTableNature), BorderLayout.CENTER);
	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTableMouvements = new ZEOTable(myTableSorterMouvements);
		myEOTableMouvements.addListener(new ListenerMouvement());
		myTableSorterMouvements.setTableHeader(myEOTableMouvements.getTableHeader());		

		myEOTableGestion = new ZEOTable(myTableSorterGestion, new RendererGestion());
//		myEOTableGestion.addListener(new ListenerGestion());
//		myTableSorterGestion.setTableHeader(myEOTableGestion.getTableHeader());		
		
		myEOTableNature = new ZEOTable(myTableSorterNature, new RendererNature());
//		myEOTableNature.addListener(new ListenerNature());
//		myTableSorterNature.setTableHeader(myEOTableNature.getTableHeader());		


	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodMouvements, "bmouDateCreation", "Date", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay((DateFormat)new SimpleDateFormat("dd/MM/yyyy"));
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMouvements, "bmouLibelle", "Libellé", 250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMouvements, "utilisateur.individu.nomUsuel", "Utilisateur", 100);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		myTableModelMouvements = new ZEOTableModel(eodMouvements, myCols);
		myTableSorterMouvements = new TableSorter(myTableModelMouvements);


		// GESTION
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodGestion, "organ.orgUb", "UB", 30);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodGestion, "organ.orgCr", "CR", 60);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodGestion, "organ.orgSouscr", "SOUS CR", 80);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodGestion, "typeCredit.tcdCode", "Tcd", 30);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodGestion, "typeAction.tyacCode", "Action", 60);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodGestion, "bdmgMontant", "Montant", 80);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);

		myTableModelGestion = new ZEOTableModel(eodGestion, myCols);
		myTableSorterGestion = new TableSorter(myTableModelGestion);

	
		// NATURE
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodNature, "organ.orgUb", "UB", 30);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodNature, "organ.orgCr", "CR", 60);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodNature, "organ.orgSouscr", "SOUS CR", 80);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodNature, "typeCredit.tcdCode", "Tcd", 30);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodNature, "planComptable.pcoNum", "Compte", 60);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodNature, "bdmnMontant", "Montant", 80);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);

		myTableModelNature = new ZEOTableModel(eodNature, myCols);
		myTableSorterNature = new TableSorter(myTableModelNature);
}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	myTableModelMouvements.fireTableDataChanged();
        	mainWindow.dispose();
        }  
	} 
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionPrint extends AbstractAction {

	    public ActionPrint() {
            super("Imprimer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
        }
	    
        public void actionPerformed(ActionEvent e) {

        	NSMutableArray ids = new NSMutableArray();
        	
        	for (int i=0;i<eodMouvements.selectedObjects().count();i++)	{
        		
        		EOBudgetMouvements mouvement = (EOBudgetMouvements)eodMouvements.selectedObjects().objectAtIndex(i);        		
        		ids.addObject((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, mouvement)).objectForKey("bmouId"));
        		
        	}

    		EditionsCtrl.sharedInstance(ec).printVirementsGestion(NSApp.getExerciceBudgetaire(), currentOrgan, ids);
    		EditionsCtrl.sharedInstance(ec).printVirementsNature(NSApp.getExerciceBudgetaire(), currentOrgan, ids);
        	
        }  
	} 
		
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelectAll extends AbstractAction {

	    public ActionSelectAll() {
            super("Tout Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SELECT);
        }
	    
        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainFrame);
            eodMouvements.selectObjectsIdenticalTo(eodMouvements.allObjects());
            myEOTableMouvements.updateData();
        	NSApp.setDefaultCursor(mainFrame);
        	
        }  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionGetDateDebut extends AbstractAction {
		
		public ActionGetDateDebut() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CALENDAR);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			NSApp.setMyDateTextField(filtreDateDebut);
			NSApp.showDatePickerPanel(new Dialog(mainFrame));
			
			
		}  
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionGetDateFin extends AbstractAction {
		
		public ActionGetDateFin() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CALENDAR);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			NSApp.setMyDateTextField(filtreDateFin);
			NSApp.showDatePickerPanel(new Dialog(mainFrame));				
			
		}  
	}
	
	
	/**
	 * Listener sur le deuxieme niveau de l'arborescence budgetaire
	 * Mise à jour du troisieme niveau si deuxieme niveau selectionne
	 */
	private class ListenerMouvement implements ZEOTable.ZEOTableListener {
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}
		
		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		
			NSApp.setWaitCursor(mainFrame);

			currentMouvement = (EOBudgetMouvements)eodMouvements.selectedObject();
			
			if (currentMouvement != null && eodMouvements.selectedObjects().count() == 1)	{
				eodGestion.setObjectArray(FinderBudgetMouvGestion.findMouvementsGestionForMouvement(ec, currentMouvement));
				eodNature.setObjectArray(FinderBudgetMouvNature.findMouvementsNatureForMouvement(ec, currentMouvement));
			}
			else	{
				eodGestion.setObjectArray(new NSArray());
				eodNature.setObjectArray(new NSArray());
			}
			
			myEOTableGestion.updateData();
			myEOTableNature.updateData();
			NSApp.setDefaultCursor(mainFrame);
			
		}
		
	}

	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererGestion extends ZEOTableCellRenderer		{
		
		public final Color COULEUR_FOND_DEBITS=new Color(255,165,158);
		public final Color COULEUR_TEXTE_DEBITS = new Color(0,0,0);
		
		public final Color COULEUR_FOND_CREDITS=new Color(153,255,155);
		public final Color COULEUR_TEXTE_CREDITS = new Color(0,0,0);
		
		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);
		
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			String sens = ((EOBudgetMouvGestion)eodGestion.displayedObjects().objectAtIndex(row)).typeSens().tyseLibelle();

			if ("DEBIT".equals(sens))	{
						leComposant.setBackground(COULEUR_FOND_DEBITS);
						leComposant.setForeground(COULEUR_TEXTE_DEBITS);
			}
			else	{
				leComposant.setBackground(COULEUR_FOND_CREDITS);
				leComposant.setForeground(COULEUR_TEXTE_CREDITS);
				
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}
			
			return leComposant;
		}
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererNature extends ZEOTableCellRenderer		{
		
		public final Color COULEUR_FOND_DEBITS=new Color(255,165,158);
		public final Color COULEUR_TEXTE_DEBITS = new Color(0,0,0);
		
		public final Color COULEUR_FOND_CREDITS=new Color(153,255,155);
		public final Color COULEUR_TEXTE_CREDITS = new Color(0,0,0);
		
		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);
		
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			String sens = ((EOBudgetMouvNature)eodNature.displayedObjects().objectAtIndex(row)).typeSens().tyseLibelle();

			if ("DEBIT".equals(sens))	{
						leComposant.setBackground(COULEUR_FOND_DEBITS);
						leComposant.setForeground(COULEUR_TEXTE_DEBITS);
			}
			else	{
				leComposant.setBackground(COULEUR_FOND_CREDITS);
				leComposant.setForeground(COULEUR_TEXTE_CREDITS);
				
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}
			
			return leComposant;
		}
	}

	
}
