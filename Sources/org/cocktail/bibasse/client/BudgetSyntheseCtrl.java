package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.finder.FinderTypeCredit;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionRecettes;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureRecettes;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class BudgetSyntheseCtrl {


	private static BudgetSyntheseCtrl sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	protected	JFrame window;

	private EODisplayGroup eodSyntheseDepense;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private EODisplayGroup eodSyntheseRecette;
	private ZEOTable myEOTableRecette;
	private ZEOTableModel myTableModelRecette;
	private TableSorter myTableSorterRecette;

	private EOOrgan currentOrgan;
	private NSArray typesCreditDepense = new NSArray();
	private NSArray typesCreditRecette = new NSArray();

	protected ActionClose 		actionClose = new ActionClose();

	private  DepensesRenderer myDepensesRenderer = new DepensesRenderer();
	private  RecettesRenderer myRecettesRenderer = new RecettesRenderer();

	protected JPanel viewTable, viewTableRecettes;
	private JPanel panelRecettes;

	/**
	 *
	 *
	 */
	public BudgetSyntheseCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		initGUI();
		initView();
		initData();
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static BudgetSyntheseCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null) {
			sharedInstance = new BudgetSyntheseCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	public void initView()	{

        window = new JFrame();//Dialog(mainFrame);
        window.setSize(450, 275);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 25));

		JPanel panelDepenses = new JPanel(new BorderLayout());

		JTextField titreDepenses = new JTextField("DEPENSES");
		titreDepenses.setForeground(Color.WHITE);
		titreDepenses.setBackground(new Color(80,130,145));
		titreDepenses.setEditable(false);
		titreDepenses.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		titreDepenses.setHorizontalAlignment(0);

		panelDepenses.add(titreDepenses, BorderLayout.NORTH);
		panelDepenses.add(viewTable, BorderLayout.CENTER);

		this.panelRecettes = initPanelRecette();

		arrayList = new ArrayList();
		arrayList.add(panelDepenses);
		arrayList.add(this.panelRecettes);
		Component panelDepensesRecettes = ZUiUtil.buildBoxColumn(arrayList);


		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.add(panelDepensesRecettes, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);


		window.setContentPane(mainView);
	}

	private JPanel initPanelRecette() {
		JPanel panelRecettes = new JPanel(new BorderLayout());
		panelRecettes.setPreferredSize(new Dimension(450,100));

		JTextField titreRecettes = new JTextField("RECETTES");
		titreRecettes.setForeground(Color.WHITE);
		titreRecettes.setBackground(new Color(255,105,98));
		titreRecettes.setEditable(false);
		titreRecettes.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		titreRecettes.setHorizontalAlignment(0);

		panelRecettes.add(titreRecettes, BorderLayout.NORTH);
		panelRecettes.add(viewTableRecettes, BorderLayout.CENTER);

		return panelRecettes;
	}

	/**
	 *
	 *
	 */
	public void initData()	{
		typesCreditDepense = FinderTypeCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), null, "DEPENSE");
		typesCreditRecette = FinderTypeCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), null, "RECETTE");
	}

	/**
	 *
	 *
	 */
	public void open()	{

        window.show();

	}

	/**
	 *
	 *
	 */
	public void close()	{
		window.hide();
	}

	/**
	 *
	 * @param organ
	 */
	public void setCurrentOrgan(EOOrgan organ)	{
		currentOrgan = organ;

		if (currentOrgan != null)	{

			String title = "";
			switch (currentOrgan.orgNiveau().intValue())	{
			case 1 : title = "Synthèse Budget - " + currentOrgan.orgEtab();break;
			case 2 : title = "Synthèse Budget - " + currentOrgan.orgEtab()+"/"+currentOrgan.orgUb();break;
			case 3 : title = "Synthèse Budget - " + currentOrgan.orgEtab()+"/"+currentOrgan.orgUb()+"/"+currentOrgan.orgCr();break;
			}
			window.setTitle(title);
		}
	}

	/**
	 *
	 * @return
	 */
	public boolean isVisible()	 {
		return window.isVisible();
	}

	public void updateSynthese() {
		// cacher ou afficher recette en fn du parametres.
		setPanelRecettesVisibility(NSApp.isCtrlSaisieRecette());

		updateSyntheseDepense();
		updateSyntheseRecette();
	}

	/**
	 *
	 *
	 */
	private void updateSyntheseDepense()	{
		NSMutableArray syntheseDepense = buildSyntheseDepense(this.typesCreditDepense);
		eodSyntheseDepense.setObjectArray(syntheseDepense);
		myEOTable.updateData();
		myTableModel.fireTableDataChanged();
	}

	/**
	 *
	 *
	 */
	private void updateSyntheseRecette()	{
		NSMutableArray syntheseRecette = buildSyntheseRecette(this.typesCreditRecette);
		eodSyntheseRecette.setObjectArray(syntheseRecette);
		myEOTableRecette.updateData();
		myTableModelRecette.fireTableDataChanged();
	}

	private NSMutableArray buildSyntheseDepense(NSArray listeTypesCreditsDepense) {
		NSMutableArray syntheseDepense = new NSMutableArray();
		NSMutableDictionary dicoSynthese = new NSMutableDictionary();

		BigDecimal cumulGestion = new BigDecimal(0.0);
		BigDecimal cumulNature = new BigDecimal(0.0);
		BigDecimal cumulDiff = new BigDecimal(0.0);

		for (int i = 0; i < listeTypesCreditsDepense.count(); i++)	{

			EOTypeCredit type = (EOTypeCredit) listeTypesCreditsDepense.objectAtIndex(i);

			dicoSynthese = new NSMutableDictionary();

			dicoSynthese.setObjectForKey(type.tcdCode(), "TCD");

			BigDecimal totalGestion = SaisieBudgetGestionDepenses.sharedInstance(ec).getCumulForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalGestion, "CUMUL_GESTION");

			BigDecimal totalGestionVote = SaisieBudgetGestionDepenses.sharedInstance(ec).getCumulVoteForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalGestionVote, "VOTE_GESTION");

			BigDecimal totalNature = SaisieBudgetNatureDepenses.sharedInstance(ec).getCumulSaisiForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalNature, "CUMUL_NATURE");

			BigDecimal totalNatureVote = SaisieBudgetNatureDepenses.sharedInstance(ec).getCumulVoteForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalNatureVote, "VOTE_NATURE");

			BigDecimal totalDiff = totalGestion.subtract(totalNature);
			dicoSynthese.setObjectForKey(totalDiff, "CUMUL_DIFF");

			try {
				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey((EOEnterpriseObject) currentOrgan, "EOOrgan");
				parametres.setObjectForKey((EOEnterpriseObject) type, "EOTypeCredit");
				parametres.setObjectForKey((EOEnterpriseObject) NSApp.getExerciceBudgetaire(), "EOExercice");

				Number dispo = ServerProxy.clientSideRequestGetDisponible(ec, parametres);
				dicoSynthese.setObjectForKey(dispo,"DISPO");
			} catch (Exception e)	{
				//e.printStackTrace();
				dicoSynthese.setObjectForKey("??????","DISPO");
			}
			syntheseDepense.addObject(dicoSynthese);

			cumulGestion = (cumulGestion.add(totalGestion)).add(totalGestionVote);
			cumulNature = (cumulNature.add(totalNature)).add(totalNatureVote);
			cumulDiff = cumulDiff.add(totalDiff);
		}

		// TOTAUX
		dicoSynthese = new NSMutableDictionary();

		dicoSynthese.setObjectForKey("","TCD");

		dicoSynthese.setObjectForKey(cumulGestion ,"CUMUL_GESTION");
		dicoSynthese.setObjectForKey(cumulNature,"CUMUL_NATURE");
		dicoSynthese.setObjectForKey(cumulDiff, "CUMUL_DIFF");

		syntheseDepense.addObject(dicoSynthese);

		return syntheseDepense;
	}


	private NSMutableArray buildSyntheseRecette(NSArray listeTypesCreditsRecette) {
		NSMutableArray syntheseRecette = new NSMutableArray();
		NSMutableDictionary dicoSynthese = new NSMutableDictionary();

		BigDecimal cumulGestion = new BigDecimal(0.0);
		BigDecimal cumulNature = new BigDecimal(0.0);
		BigDecimal cumulDiff = new BigDecimal(0.0);

		for (int i = 0; i < listeTypesCreditsRecette.count(); i++)	{

			EOTypeCredit type = (EOTypeCredit) listeTypesCreditsRecette.objectAtIndex(i);

			dicoSynthese = new NSMutableDictionary();

			dicoSynthese.setObjectForKey(type.tcdCode(),"TCD");

			BigDecimal totalGestion = SaisieBudgetGestionRecettes.sharedInstance(ec).getCumulForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalGestion ,"CUMUL_GESTION");

			BigDecimal totalGestionVote = SaisieBudgetGestionRecettes.sharedInstance(ec).getCumulVoteForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalGestionVote ,"VOTE_GESTION");

			BigDecimal totalNature = SaisieBudgetNatureRecettes.sharedInstance(ec).getCumulSaisiForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalNature,"CUMUL_NATURE");

			BigDecimal totalNatureVote = SaisieBudgetNatureRecettes.sharedInstance(ec).getCumulVoteForTypeCredit(type);
			dicoSynthese.setObjectForKey(totalNatureVote ,"VOTE_NATURE");

			BigDecimal totalDiff = totalGestion.subtract(totalNature);
			dicoSynthese.setObjectForKey(totalDiff,"CUMUL_DIFF");

			syntheseRecette.addObject(dicoSynthese);

			cumulGestion = (cumulGestion.add(totalGestion)).add(totalGestionVote);
			cumulNature = (cumulNature.add(totalNature)).add(totalNatureVote);
			cumulDiff = cumulDiff.add(totalDiff);
		}

		// TOTAUX
		dicoSynthese = new NSMutableDictionary();

		dicoSynthese.setObjectForKey("","TCD");

		dicoSynthese.setObjectForKey(cumulGestion ,"CUMUL_GESTION");
		dicoSynthese.setObjectForKey(cumulNature,"CUMUL_NATURE");
		dicoSynthese.setObjectForKey(cumulDiff, "CUMUL_DIFF");

		syntheseRecette.addObject(dicoSynthese);

		return syntheseRecette;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void initGUI() {

		eodSyntheseDepense = new EODisplayGroup();
		eodSyntheseRecette = new EODisplayGroup();

        viewTable = new JPanel();
        viewTableRecettes = new JPanel();

		initTableModel();
		initTable();

		myEOTable.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTable.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);


		myEOTableRecette.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableRecette.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableRecette.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableRecettes.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableRecettes.removeAll();
		viewTableRecettes.setLayout(new BorderLayout());
		viewTableRecettes.add(new JScrollPane(myEOTableRecette), BorderLayout.CENTER);
	}

	private void setPanelRecettesVisibility(boolean visible) {
		this.panelRecettes.setVisible(visible);
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter, myDepensesRenderer);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTableRecette = new ZEOTable(myTableSorterRecette, myRecettesRenderer);
		myTableSorterRecette.setTableHeader(myEOTableRecette.getTableHeader());

	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		myTableModel = new ZEOTableModel(eodSyntheseDepense, buildColsDepense(eodSyntheseDepense));
		myTableSorter = new TableSorter(myTableModel);

		myTableModelRecette = new ZEOTableModel(eodSyntheseRecette, buildColsRecette(eodSyntheseRecette));
		myTableSorterRecette = new TableSorter(myTableModelRecette);
	}

	private List<ZEOTableModelColumn> buildColsDepense(EODisplayGroup dgSynthese) {
		List<ZEOTableModelColumn> depenseCols = new ArrayList<ZEOTableModelColumn>();

		ZEOTableModelColumn col = new ZEOTableModelColumn(dgSynthese, "TCD", "TCD", 35);
		col.setAlignment(SwingConstants.CENTER);
		col.setEditable(false);
		depenseCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "DISPO", "Dispo", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		depenseCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "VOTE_GESTION", "Ges Voté", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		depenseCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "CUMUL_GESTION", "Gestion", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		depenseCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "VOTE_NATURE", "Nat Voté", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		depenseCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "CUMUL_NATURE", "Nature", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		depenseCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "CUMUL_DIFF", "Diff", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		depenseCols.add(col);

		return depenseCols;
	}

	private List<ZEOTableModelColumn> buildColsRecette(EODisplayGroup dgSynthese) {
		List<ZEOTableModelColumn> recetteCols = new ArrayList<ZEOTableModelColumn>();

		ZEOTableModelColumn col = new ZEOTableModelColumn(dgSynthese, "TCD", "TCD", 35);
		col.setAlignment(SwingConstants.CENTER);
		col.setEditable(false);
		recetteCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "VOTE_GESTION", "Ges Voté", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		recetteCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "CUMUL_GESTION", "Gestion", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		recetteCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "VOTE_NATURE", "Nat Voté", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		recetteCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "CUMUL_NATURE", "Nature", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		recetteCols.add(col);

		col = new ZEOTableModelColumn(dgSynthese, "CUMUL_DIFF", "Diff", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		recetteCols.add(col);

		return recetteCols;
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class DepensesRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_TOTAL_GENERAL=new Color(218,221,255);
		public final Color COULEUR_TEXTE_TOTAL_GENERAL=new Color(0,0,0);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			leComposant.setBackground(COULEUR_FOND_TOTAL_GENERAL);
			leComposant.setForeground(COULEUR_TEXTE_TOTAL_GENERAL);

			if (column == 6 || column == 0)
				leComposant.setFont(new Font("Arial", Font.BOLD, 12));

			if (column == 2 || column == 4)
				leComposant.setForeground(new Color(150,150,150));
			else
				leComposant.setForeground(new Color(0,0,0));

			if (row == typesCreditDepense.count())	{
				leComposant.setFont(new Font("Arial", Font.BOLD, 12));

				if (column == 6)	{

					leComposant.setForeground(Color.BLACK);

					BigDecimal difference = (BigDecimal)((NSDictionary)eodSyntheseDepense.displayedObjects().objectAtIndex(row)).objectForKey("CUMUL_DIFF");

					if (difference.floatValue() == 0)
						leComposant.setBackground(new Color(153,255,155));
					else
						leComposant.setBackground(new Color(255,165,158));

				}

			}
			return leComposant;
		}
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RecettesRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_TOTAL_GENERAL = new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_TOTAL_GENERAL = new Color(0,0,0);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++) {
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
			}
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			leComposant.setBackground(COULEUR_FOND_TOTAL_GENERAL);
			leComposant.setForeground(new Color(0,0,0));

			if (column == 5 || column == 0) {
				leComposant.setFont(new Font("Arial", Font.BOLD, 12));
			}

			if (column == 1 || column == 3) {
				leComposant.setForeground(new Color(150,150,150));
			}

			// ligne recap
			if (row == typesCreditRecette.count()) {
				leComposant.setFont(new Font("Arial", Font.BOLD, 12));

				if (column == 5)	{
					leComposant.setForeground(Color.BLACK);
					BigDecimal difference = (BigDecimal) ((NSDictionary) eodSyntheseRecette.displayedObjects().objectAtIndex(row)).objectForKey("CUMUL_DIFF");

					if (difference.floatValue() == 0.0) {
						leComposant.setBackground(new Color(153,255,155));
					} else {
						leComposant.setBackground(new Color(255,165,158));
					}
				}
			}

			return leComposant;
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }

        public void actionPerformed(ActionEvent e) {
        	window.hide();
        }
	}

}
