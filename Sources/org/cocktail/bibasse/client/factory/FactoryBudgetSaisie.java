/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *
 *  CETTE FACTORY PERMET DE GERER :
 *  LA CREATION D'UNE SAISIE : ETAT VALIDE
 *  LA SUPPRESSION D'UNE SAISIE  : ETAT ANNULE
 *  LA CLOTURE D'UNE SAISIE : ETAT CLOT
 *
 */


package org.cocktail.bibasse.client.factory;

import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;



public class FactoryBudgetSaisie extends FactoryBibasse {



    public FactoryBudgetSaisie() {
        super();
    }

    public FactoryBudgetSaisie(boolean withLog) {
        super(withLog);
    }

    /** Methode qui permet de creer un object de la classe BudgetSaisie.
     * Obligation de creer une saisie hors convention.
     * @param monEOEditingContext editingcontext de travail
     * @param setUtilisateur utilisateur qui gere la saisie
     * @param setTypeSaisie type de saisie voir classe TypeSaisie
     * @param setTypeEtat etat de la saisie voir classe TypeEtat
     * @param setExercice exercice concerné par cette saisie
     * @param setBdsaLibelle libelle utilisateur de la saisie
     * @param setBdsaDateCreation deta de creation de la saisie
     * @throws Exception message si incoherence des ETATS
     * @return un objet utiliser uniquement pour une saisie liees a une peration hors convention
     */
    public EOBudgetSaisie creerEOBudgetSaisieHorsConvention(
            EOEditingContext monEOEditingContext,
            EOUtilisateur setUtilisateur,
            EOTypeSaisie setTypeSaisie,
            EOTypeEtat setTypeEtatValide,
            EOExercice setExercice,
            String setBdsaLibelle,
            NSTimestamp setBdsaDateCreation
    ) throws Exception {

        EOBudgetSaisie newEOBudgetSaisie = this.creerEOBudgetSaisie(
                monEOEditingContext,
                setUtilisateur,
                setTypeSaisie,
                setTypeEtatValide,
                setExercice,
                setBdsaLibelle,
                setBdsaDateCreation);

        newEOBudgetSaisie.verifierTypeHorsConvention();

        return  newEOBudgetSaisie;
    }


    /** Methode qui permet de creer un object de la classe BudgetSaisie.
     * Obligation de creer une saisie pour une convention
     * @param monEOEditingContext editingcontext de travail
     * @param setUtilisateur utilisateur qui gere la saisie
     * @param setTypeSaisie type de saisie voir classe TypeSaisie
     * @param setTypeEtat etat de la saisie voir classe TypeEtat
     * @param setExercice exercice concerné par cette saisie
     * @param setBdsaLibelle libelle utilisateur de la saisie
     * @param setBdsaDateCreation deta de creation de la saisie
     * @throws Exception message si incoherence des ETATS
     * @return un objet utiliser uniquement pour une saisie liees a une convention
     */
    public EOBudgetSaisie creerEOBudgetSaisiePourConvention(
            EOEditingContext monEOEditingContext,
            EOUtilisateur setUtilisateur,
            EOTypeSaisie setTypeSaisie,
            EOTypeEtat setTypeEtatValide,
            EOExercice setExercice,
            String setBdsaLibelle,
            NSTimestamp setBdsaDateCreation
    ) throws Exception
    {
        EOBudgetSaisie newEOBudgetSaisie= this.creerEOBudgetSaisie(
                monEOEditingContext,
                setUtilisateur,
                setTypeSaisie,
                setTypeEtatValide,
                setExercice,
                setBdsaLibelle,
                setBdsaDateCreation);

        newEOBudgetSaisie.verifierTypeConvention();

        return  newEOBudgetSaisie;
    }




    /** Methode qui permet d'annuler une saisie
     * @param monEOEditingContext editingContext de travail
     * @param monEOBudgetSaisie object a manipuler
     * @param setUtilisateur utilisateur qui annule la saisie
     * @param setTypeEtatAnnule etat ANNULE
     * @throws Exception message si incoherence des ETATS
     */
    public void annulerEOBudgetSaisie(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisie monEOBudgetSaisie,
            EOUtilisateur setUtilisateur,
            EOTypeEtat setTypeEtatAnnule
    ) throws Exception
    {
        trace("Methode supprimerEOBudgetSaisie");
        trace("EOEditingContext monEOEditingContext");
        trace("EOBudgetSaisie monEOBudgetSaisie");
        trace("EOUtilisateur setUtilisateur");
        trace("EOTypeEtat setTypeEtatAnnule");

        trace(monEOEditingContext);
        trace(monEOBudgetSaisie);
        trace(setUtilisateur);
        trace(setTypeEtatAnnule);

        try {
            verifierEtat_VALIDE_ANNULE ((EOTypeEtat)monEOBudgetSaisie.typeEtat(),setTypeEtatAnnule);

            monEOBudgetSaisie.addUtilisateur(monEOBudgetSaisie,setUtilisateur);
            monEOBudgetSaisie.addTypeEtat(monEOBudgetSaisie,setTypeEtatAnnule);

        } catch (Exception e) {
            throw e;
        }
    }



    /** Methode qui permet de cloturer une saisie.
     * @param monEOEditingContext editingContext de travail
     * @param monEOBudgetSaisie object en cours de manipulation
     * @param setUtilisateurValidation utilisateur qui clot la saisie
     * @param setBdsaDateValidation date de cloturation de la saisie
     * @param setTypeEtatClot etat CLOT de la saisie
     * @throws Exception problemes de coherence des ETATS
     */
    public void cloturerEOBudgetSaisie(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisie monEOBudgetSaisie,
            EOUtilisateur setUtilisateurValidation,
            NSTimestamp setBdsaDateValidation,
            EOTypeEtat setTypeEtatClot
    ) throws Exception {

        trace("Methode cloturerEOBudgetSaisie");
        trace("EOEditingContext monEOEditingContext");
        trace("EOBudgetSaisie monEOBudgetSaisie");
        trace("EOUtilisateur setUtilisateurValidation");
        trace("ENSTimestamp setBdsaDateValidation");

        trace(monEOEditingContext);
        trace(monEOBudgetSaisie);
        trace(setUtilisateurValidation);
        trace(setBdsaDateValidation);

        try {
            verifierEtat_VALIDE_CLOT((EOTypeEtat) monEOBudgetSaisie.typeEtat(), setTypeEtatClot);

            monEOBudgetSaisie.addUtilisateurValidation(monEOBudgetSaisie,setUtilisateurValidation);
            monEOBudgetSaisie.addTypeEtat(monEOBudgetSaisie,setTypeEtatClot);
            monEOBudgetSaisie.setBdsaDateValidation(setBdsaDateValidation);

        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Methode private qui permet de creer un object de la classe BudgetSaisie.
     * @param monEOEditingContext editingcontext de travail
     * @param setUtilisateur utilisateur qui gere la saisie
     * @param setTypeSaisie type de saisie voir classe TypeSaisie
     * @param setTypeEtat etat de la saisie voir classe TypeEtat
     * @param setExercice exercice concerné par cette saisie
     * @param setBdsaLibelle libelle utilisateur de la saisie
     * @param setBdsaDateCreation deta de creation de la saisie
     * @return
     */
    private EOBudgetSaisie creerEOBudgetSaisie(
            EOEditingContext monEOEditingContext,
            EOUtilisateur setUtilisateur,
            EOTypeSaisie setTypeSaisie,
            EOTypeEtat setTypeEtatValide,
            EOExercice setExercice,
            String setBdsaLibelle,
            NSTimestamp setBdsaDateCreation
    ) throws Exception {
        EOBudgetSaisie newEOBudgetSaisie = null;
        try {
            newEOBudgetSaisie = (EOBudgetSaisie) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetSaisie.ENTITY_NAME);

            newEOBudgetSaisie.addUtilisateurValidation(newEOBudgetSaisie, null);
            newEOBudgetSaisie.addUtilisateur(newEOBudgetSaisie, setUtilisateur);
            newEOBudgetSaisie.addTypeSaisie(newEOBudgetSaisie, setTypeSaisie);
            newEOBudgetSaisie.addTypeEtat(newEOBudgetSaisie, setTypeEtatValide);
            newEOBudgetSaisie.addExercice(newEOBudgetSaisie, setExercice);
            newEOBudgetSaisie.setBdsaLibelle(setBdsaLibelle);
            newEOBudgetSaisie.setBdsaDateCreation(setBdsaDateCreation);

            System.out.println("FactoryBudgetSaisie.creerEOBudgetSaisie() ==> " + newEOBudgetSaisie);

            monEOEditingContext.insertObject(newEOBudgetSaisie);

            return newEOBudgetSaisie;
        } catch (Exception e) {
            throw e;
        }
    }
}
