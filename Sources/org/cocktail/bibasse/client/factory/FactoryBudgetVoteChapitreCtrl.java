/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import org.cocktail.bibasse.client.metier.EOBudgetVoteChapitreCtrl;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryBudgetVoteChapitreCtrl extends FactoryBibasse {
    
    public FactoryBudgetVoteChapitreCtrl() {
        super();
    }
    
    public FactoryBudgetVoteChapitreCtrl(boolean withLog) {
        super(withLog);
    }
    
    
/**
 * 
 * @param monEOEditingContext
 * @param exercice
 * @param planComptable
 * @param typeCredit
 * @param typeEtat
 * @return
 * @throws Exception
 */
    public EOBudgetVoteChapitreCtrl creerBudgetVoteChapitreControle	(
            EOEditingContext monEOEditingContext,
            EOExercice exercice,
            EOPlanComptable planComptable,
            EOTypeCredit typeCredit,
            EOTypeEtat typeEtat,
            EOUtilisateur utilisateur
    ) throws Exception
    {
        
        try {
            
        	EOBudgetVoteChapitreCtrl newRecord = null;
            
            newRecord= (EOBudgetVoteChapitreCtrl) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetVoteChapitreCtrl.ENTITY_NAME);
            
            newRecord.addObjectToBothSidesOfRelationshipWithKey( planComptable , EOBudgetVoteChapitreCtrl.PLAN_COMPTABLE_KEY);
            newRecord.addObjectToBothSidesOfRelationshipWithKey( exercice , EOBudgetVoteChapitreCtrl.EXERCICE_KEY);
            newRecord.addObjectToBothSidesOfRelationshipWithKey( typeCredit , EOBudgetVoteChapitreCtrl.TYPE_CREDIT_KEY);
            newRecord.addObjectToBothSidesOfRelationshipWithKey( typeEtat , EOBudgetVoteChapitreCtrl.TYPE_ETAT_KEY);
            newRecord.addObjectToBothSidesOfRelationshipWithKey( utilisateur , EOBudgetVoteChapitreCtrl.UTILISATEUR_KEY);
            
            newRecord.setBvccDate(new NSTimestamp());
            
            monEOEditingContext.insertObject( newRecord );
            
            return newRecord;
            
            
        } catch (Exception e) {
            throw e;
        }
    }   
}
