/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryBudgetMouvements extends FactoryBibasse {
    
    public FactoryBudgetMouvements() {
        super();
    }
    
    public FactoryBudgetMouvements(boolean withLog) {
        super(withLog);
    }
    
    
    /** cette methode permet de creer un mouvement budgetaire
     * 
     * @param monEOEditingContext editing context de travail
     * @param setUtilisateur utilisateur qui a creee le mouvement
     * @param setTypeMouvementBudgetaire type de mouvement 
     * @param setExercice exercice de ce mouvement
     * @param setTypeEtatValide etat VALIDE
     * @param setBmouLibelle libelle de cet mouvement
     * @param setBmouDateCreation date de creation de ce mouvement
     * @return objetde la classe EOBudgetMouvements
     * @throws Exception probleme d'etat 
     */
    public EOBudgetMouvements creerEOBudgetMouvements
    (
            EOEditingContext monEOEditingContext,
            EOUtilisateur setUtilisateur,
            EOTypeMouvementBudgetaire setTypeMouvementBudgetaire,
            EOExercice setExercice,
            EOTypeEtat setTypeEtatValide,
            String setBmouLibelle,
            NSTimestamp setBmouDateCreation
    ) throws Exception
    {
        
//        trace("creerEOBudgetMouvements");
//        trace("EOEditingContext monEOEditingContext");
//        trace("EOUtilisateur setUtilisateur");
//        trace("EOTypeMouvementBudgetaire setTypeMouvementBudgetaire");
//        trace("EOExercice setExercice");
//        trace("EOTypeEtat setTypeEtatValide");
//        trace("String setBmouLibelle");
//        trace("NSTimestamp setBmouDateCreation");
//        
//        trace( monEOEditingContext );
//        trace( setUtilisateur );
//        trace( setTypeMouvementBudgetaire );
//        trace( setExercice );
//        trace( setTypeEtatValide );
//        trace( setBmouLibelle );
//        trace( setBmouDateCreation );
        
        
        try {
            //verifierEtat_VALIDE(setTypeEtatValide,"setTypeEtatValide");
            
            EOBudgetMouvements newEOBudgetMouvements = null;
            
            newEOBudgetMouvements= (EOBudgetMouvements) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMouvements.ENTITY_NAME);
            
            newEOBudgetMouvements.addUtilisateur( newEOBudgetMouvements , setUtilisateur );
            newEOBudgetMouvements.addTypeMouvementBudgetaire( newEOBudgetMouvements , setTypeMouvementBudgetaire );
            newEOBudgetMouvements.addExercice( newEOBudgetMouvements , setExercice );
            newEOBudgetMouvements.addTypeEtat( newEOBudgetMouvements , setTypeEtatValide );
            
            newEOBudgetMouvements.setBmouLibelle( setBmouLibelle );
            newEOBudgetMouvements.setBmouDateCreation( setBmouDateCreation );
            
            monEOEditingContext.insertObject( newEOBudgetMouvements );
            
            return newEOBudgetMouvements;
            
            
        } catch (Exception e) {
            throw e;
        }
    }
    
    
    /** Cette methode permet de d'annuler la saisie d'un mouvement budgetaire
     *  IMPOSSIBLE DE REVENIR SUR CETTE MISE A JOUR
     * @param monEOEditingContext editing context de travail
     * @param monEOBudgetMouvements mouvment budgetaire de travail
     * @param setUtilisateur utilisateur qui annule ce mouvent
     * @param setTypeEtatAnnuler etat ANNULE
     * @throws Exception problemes d'etat
     */
    public void annulerEOBudgetMouvements  (
            EOEditingContext monEOEditingContext,
            EOBudgetMouvements monEOBudgetMouvements,
            EOUtilisateur setUtilisateur,
            EOTypeEtat setTypeEtatAnnuler
    ) throws Exception
    {
        trace("creerEOBudgetMouvements");
        trace("EOEditingContext monEOEditingContext");
        trace("EOUtilisateur setUtilisateur");
        trace("EOTypeEtat setTypeEtatAnnuler");
        
        trace( monEOEditingContext );
        trace( setUtilisateur );
        trace( setTypeEtatAnnuler );
        
        try {
            verifierEtat_ANNULE(setTypeEtatAnnuler,"setTypeEtatAnnuler");
            verifierEtat_VALIDE_ANNULE((EOTypeEtat)monEOBudgetMouvements.typeEtat(),setTypeEtatAnnuler);
            
            monEOBudgetMouvements.addUtilisateur( monEOBudgetMouvements , setUtilisateur );
            monEOBudgetMouvements.addTypeEtat( monEOBudgetMouvements , setTypeEtatAnnuler );     
            
        } catch (Exception e) {
            throw e;
        }  
        
    }
    
    /** Cette methode permet de cloturer la saisie d'un mouvement budgetaire
     *  IMOPSSIBLE DE MODIFIER LA SAISIE APRES CETTE MISE A JOUR
     * @param monEOEditingContext editing context de travail
     * @param monEOBudgetMouvements mouvement budgetaire en modification
     * @param setUtilisateur utilisateur qui clot la saisie
     * @param setTypeEtatCloturer  etat CLOT
     * @throws Exception probleme d'etat
     */
    public void cloturerEOBudgetMouvements (
            EOEditingContext monEOEditingContext,
            EOBudgetMouvements monEOBudgetMouvements,
            EOUtilisateur setUtilisateur,
            EOTypeEtat setTypeEtatCloturer
    ) throws Exception
    {
        trace("creerEOBudgetMouvements");
        trace("EOEditingContext monEOEditingContext");
        trace("EOUtilisateur setUtilisateur");
        trace("EOTypeEtat setTypeEtatValider");
        
        trace( monEOEditingContext );
        trace( setUtilisateur );
        trace( setTypeEtatCloturer );
        
        try {
            verifierEtat_CLOT(setTypeEtatCloturer,"setTypeEtatAnnuler");
            verifierEtat_VALIDE_CLOT((EOTypeEtat)monEOBudgetMouvements.typeEtat(),setTypeEtatCloturer);
       
            monEOBudgetMouvements.addUtilisateur( monEOBudgetMouvements , setUtilisateur );
            monEOBudgetMouvements.addTypeEtat( monEOBudgetMouvements , setTypeEtatCloturer );     
            
        } catch (Exception e) {
            throw e;
        }  
        
    }
    
    
    
    
}
