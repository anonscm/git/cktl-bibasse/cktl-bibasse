/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *
 */
package org.cocktail.bibasse.client.factory;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryBudgetSaisieNature extends FactoryBibasse {

    private static final String ERROR_MESS_VERIF_ORGAN ="IMPOSSIBLE DE CREER L OBJET : PROBLEME ORGAN";

    public FactoryBudgetSaisieNature() {
        super();
    }

    public FactoryBudgetSaisieNature(boolean withLog) {
        super(withLog);
    }



    /** Cette methode permet de creer une ligne de budget par nature
     *
     * @param monEOEditingContext editingContext de travail
     * @param setTypeEtatValide etat VALIDE
     * @param setTypeCredit type de credit
     * @param setPlanComptable compte d imputation
     * @param setOrgan ligne budgetaire
     * @param setExercice exerice
     * @param setBudgetSaisie liens vers la saisie
     * @param setBdsnMontant montant de la saisie
     * @return une ligne EOBudgetSaisieNature
     * @throws Exception probleme sur les etats
     */
    public EOBudgetSaisieNature creerEOBudgetSaisieNature(
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatValide,
            EOTypeCredit setTypeCredit,
            EOPlanComptable setPlanComptable,
            EOOrgan setOrgan,
            EOExercice setExercice,
            EOBudgetSaisie setBudgetSaisie,
            BigDecimal setBdsnVote,
            BigDecimal setBdsnSaisi,
            BigDecimal setBdsnMontant
    ) throws Exception {

        EOBudgetSaisieNature newBudgetSaisieNature = null;

        try {

            if (this.verifierOrgan(setBudgetSaisie, setOrgan)) {

                newBudgetSaisieNature = (EOBudgetSaisieNature) FactoryBibasse.instanceForEntity(
                        monEOEditingContext,
                        EOBudgetSaisieNature.ENTITY_NAME);

                monEOEditingContext.insertObject( newBudgetSaisieNature);

                newBudgetSaisieNature.addTypeEtat( newBudgetSaisieNature,setTypeEtatValide);
                newBudgetSaisieNature.addTypeCredit( newBudgetSaisieNature,setTypeCredit);
                newBudgetSaisieNature.addPlanComptable( newBudgetSaisieNature,setPlanComptable);
                newBudgetSaisieNature.addOrgan( newBudgetSaisieNature,setOrgan);
                newBudgetSaisieNature.addExercice( newBudgetSaisieNature,setExercice);
                newBudgetSaisieNature.addBudgetSaisie( newBudgetSaisieNature,setBudgetSaisie);

                newBudgetSaisieNature.setBdsnMontant( setBdsnMontant);
                newBudgetSaisieNature.setBdsnVote( setBdsnVote);
                newBudgetSaisieNature.setBdsnSaisi( setBdsnSaisi);
            } else {
                throw new Exception( "creerEOBudgetSaisieGestion : "+FactoryBudgetSaisieNature.ERROR_MESS_VERIF_ORGAN);
            }

            return newBudgetSaisieNature;

        } catch (Exception e) {
            throw e;
        }
    }


    /**Cette Methode permet d'annuler une ligne de saisie du budget par nature
     * Impossible de modifier celle ci apres cette mise a jour
     * @param monEOEditingContext editing context de travail
     * @param monBudgetSaisieNature object en cours de manipulation
     * @param setTypeEtatAnnule etat ANNULE
     * @throws Exception probleme d etat
     */
    public void annulerEOBudgetSaisieNature(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisieNature monBudgetSaisieNature,
            EOTypeEtat setTypeEtatAnnule
    ) throws Exception
    {

        trace(" annulerEOBudgetSaisieNature");
        trace("EOEditingContext monEOEditingContext");
        trace("EOBudgetSaisie monBudgetSaisieNature");
        trace("EOTypeEtat setTypeEtatAnnule");

        trace(monEOEditingContext);
        trace(monBudgetSaisieNature);
        trace(setTypeEtatAnnule);

        try {
            verifierEtat_VALIDE_ANNULE ((EOTypeEtat)monBudgetSaisieNature.typeEtat(),setTypeEtatAnnule);

            monBudgetSaisieNature.addTypeEtat( monBudgetSaisieNature,setTypeEtatAnnule);

        } catch (Exception e) {
            throw e;
        }

    }



    /**Cette Methode permet de cloturer la saisie d'une ligne du budget par nature
     * Impossible de modifier celle ci apres cette mise a jour
     * @param monEOEditingContext editing context de travail
     * @param monBudgetSaisieNature object en cours de manipulation
     * @param setTypeEtatClot etat CLOT
     * @throws Exception probleme d'etat
     */
    public void cloturerEOBudgetSaisieNature(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisieNature monBudgetSaisieNature,
            EOTypeEtat setTypeEtatClot
    ) throws Exception
    {

        trace(" cloturerEOBudgetSaisieNature");
        trace("EOEditingContext monEOEditingContext");
        trace("EOBudgetSaisie monBudgetSaisieNature");
        trace("EOTypeEtat setTypeEtatClot");

        trace(monEOEditingContext);
        trace(monBudgetSaisieNature);
        trace(setTypeEtatClot);

        try {

            verifierEtat_VALIDE_CLOT ((EOTypeEtat)monBudgetSaisieNature.typeEtat(),setTypeEtatClot);

            monBudgetSaisieNature.addTypeEtat( monBudgetSaisieNature,setTypeEtatClot);

        } catch (Exception e) {
            throw e;
        }
    }




    /*PRIVATE
     * PRIVATE
     * PRIVATE
     */


    private boolean verifierOrgan ( EOBudgetSaisie setBudgetSaisie,EOOrgan setOrgan)
    {

            return true;
    }

}
