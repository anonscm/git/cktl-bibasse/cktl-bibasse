
package org.cocktail.bibasse.client.factory;

import org.cocktail.bibasse.client.metier.EOBudgetEvenements;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOTypeEvenement;
import org.cocktail.bibasse.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryBudgetEvenements extends FactoryBibasse {

    public FactoryBudgetEvenements() {
        super();
    }

    public FactoryBudgetEvenements(boolean withLog) {
        super(withLog);
    }


    /**
     *
     * @param monEOEditingContext
     * @param exercice
     * @param typeAction
     * @param typeEtat
     * @return
     * @throws Exception
     */
    public EOBudgetEvenements creerBudgetEvenement (EOEditingContext monEOEditingContext, EOBudgetSaisie budgetSaisie,
            EOUtilisateur utilisateur, EOTypeEvenement typeEvenement) throws Exception
    {

        try {

            EOBudgetEvenements newEvenement = null;

            newEvenement= (EOBudgetEvenements) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetEvenements.ENTITY_NAME);

            newEvenement.addObjectToBothSidesOfRelationshipWithKey( budgetSaisie , EOBudgetEvenements.BUDGET_SAISIE_KEY);
            newEvenement.addObjectToBothSidesOfRelationshipWithKey( utilisateur , EOBudgetEvenements.UTILISATEUR_KEY);
            newEvenement.addObjectToBothSidesOfRelationshipWithKey( typeEvenement , EOBudgetEvenements.TYPE_EVENEMENT_KEY);

            newEvenement.setBeveDate(new NSTimestamp());

            monEOEditingContext.insertObject( newEvenement );

            return newEvenement;


        } catch (Exception e) {
            throw e;
        }
    }



}
