/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNatureLolf;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryBudgetSaisieNatureLolf extends FactoryBibasse {

	private static final String ERROR_MESS_VERIF_ORGAN ="IMPOSSIBLE DE CREER L OBJET : PROBLEME ORGAN";

	public FactoryBudgetSaisieNatureLolf() {
		super();
	}

	public FactoryBudgetSaisieNatureLolf(boolean withLog) {
		super(withLog);
	}



	/** Cette methode permet de creer une ligne de budget par nature
	 * 
	 * @param monEOEditingContext editingContext de travail
	 * @param setTypeEtatValide etat VALIDE
	 * @param setTypeCredit type de credit
	 * @param setPlanComptable compte d imputation
	 * @param setTypeAction action lolf
	 * @param setOrgan ligne budgetaire
	 * @param setExercice exerice
	 * @param setBudgetSaisie liens vers la saisie
	 * @param setBdslMontant montant de la saisie
	 * @return une ligne EOBudgetSaisieNature
	 * @throws Exception probleme sur les etats
	 */
	public EOBudgetSaisieNatureLolf creerEOBudgetSaisieNatureLolf(
			EOEditingContext monEOEditingContext,
			EOTypeEtat setTypeEtatValide,
			EOTypeCredit setTypeCredit,
			EOPlanComptable setPlanComptable,
			EOTypeAction setTypeAction,
			EOOrgan setOrgan,
			EOExercice setExercice,
			EOBudgetSaisie setBudgetSaisie,
			BigDecimal setBdslVote,
			BigDecimal setBdslSaisi,
			BigDecimal setBdslMontant
	) throws Exception
	{

		EOBudgetSaisieNatureLolf newBudgetSaisieNatureLolf=null;

		try {

			//verifierEtat_VALIDE(setTypeEtatValide,"setTypeEtatValide");

			if ( this. verifierOrgan (  setBudgetSaisie, setOrgan)){

				newBudgetSaisieNatureLolf = (EOBudgetSaisieNatureLolf)FactoryBibasse.instanceForEntity(monEOEditingContext,
						EOBudgetSaisieNatureLolf.ENTITY_NAME);

				monEOEditingContext.insertObject(newBudgetSaisieNatureLolf);

				newBudgetSaisieNatureLolf.addTypeEtat(newBudgetSaisieNatureLolf,setTypeEtatValide);
				newBudgetSaisieNatureLolf.addTypeCredit(newBudgetSaisieNatureLolf,setTypeCredit);
				newBudgetSaisieNatureLolf.addTypeAction(newBudgetSaisieNatureLolf,setTypeAction);
				newBudgetSaisieNatureLolf.addPlanComptable(newBudgetSaisieNatureLolf,setPlanComptable);
				newBudgetSaisieNatureLolf.addOrgan(newBudgetSaisieNatureLolf,setOrgan);
				newBudgetSaisieNatureLolf.addExercice(newBudgetSaisieNatureLolf,setExercice);
				newBudgetSaisieNatureLolf.addBudgetSaisie(newBudgetSaisieNatureLolf,setBudgetSaisie);

				newBudgetSaisieNatureLolf.setBdslMontant(setBdslMontant);
				newBudgetSaisieNatureLolf.setBdslVote(setBdslVote);                
				newBudgetSaisieNatureLolf.setBdslSaisi(setBdslSaisi);
			}
			else
				throw new Exception( "creerEOBudgetSaisieNatureLolf : "+FactoryBudgetSaisieNatureLolf.ERROR_MESS_VERIF_ORGAN);

			return newBudgetSaisieNatureLolf;

		} catch (Exception e) {
			throw e;
		}     


	}


	/**Cette Methode permet d'annuler une ligne de saisie du budget par nature
	 * Impossible de modifier celle ci apres cette mise a jour
	 * @param monEOEditingContext editing context de travail
	 * @param monBudgetSaisieNature object en cours de manipulation
	 * @param setTypeEtatAnnule etat ANNULE
	 * @throws Exception probleme d etat
	 */
	public void annulerEOBudgetSaisieNatureLolf(EOEditingContext monEOEditingContext, EOBudgetSaisieNatureLolf monBudgetSaisieNatureLolf,
			EOTypeEtat setTypeEtatAnnule) throws Exception {
		try {
			verifierEtat_VALIDE_ANNULE ((EOTypeEtat)monBudgetSaisieNatureLolf.typeEtat(),setTypeEtatAnnule);
			monBudgetSaisieNatureLolf.addTypeEtat(monBudgetSaisieNatureLolf,setTypeEtatAnnule);  

		} catch (Exception e) {
			throw e;
		}

	}



	/**Cette Methode permet de cloturer la saisie d'une ligne du budget par nature
	 * Impossible de modifier celle ci apres cette mise a jour
	 * @param monEOEditingContext editing context de travail
	 * @param monBudgetSaisieNature object en cours de manipulation
	 * @param setTypeEtatClot etat CLOT
	 * @throws Exception probleme d'etat
	 */
	public void cloturerEOBudgetSaisieNatureLolf(EOEditingContext monEOEditingContext, EOBudgetSaisieNatureLolf monBudgetSaisieNatureLolf,
			EOTypeEtat setTypeEtatClot) throws Exception { 
		try {
			verifierEtat_VALIDE_CLOT ((EOTypeEtat)monBudgetSaisieNatureLolf.typeEtat(),setTypeEtatClot);
			monBudgetSaisieNatureLolf.addTypeEtat(monBudgetSaisieNatureLolf,setTypeEtatClot);  
		} catch (Exception e) {
			throw e;
		}      
	}




	/*PRIVATE
	 * PRIVATE
	 * PRIVATE
	 */


	private boolean verifierOrgan ( EOBudgetSaisie setBudgetSaisie,EOOrgan setOrgan) 
	{
		return true;
	}

}
