/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetMouvGestion;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSens;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryBudgetMouvementsGestion extends FactoryBibasse {

    public FactoryBudgetMouvementsGestion() {
        super();

    }

    public FactoryBudgetMouvementsGestion(boolean withLog) {
        super(withLog);

    }

    
    public EOBudgetMouvGestion creerEOBudgetMouvementsGestion
    (
            EOEditingContext monEOEditingContext,
            EOTypeSens setTypeSens,
            EOTypeCredit setTypeCredit,
            EOTypeAction setTypeAction,
            EOOrgan setOrgan,
            EOBudgetMouvements setBudgetMouvements,
            EOExercice setExercice,
            EOTypeEtat setTypeEtat,
            BigDecimal setBdmgMontant
    ) throws Exception
    {
        

        try {

            
            EOBudgetMouvGestion newEOBudgetMouvGestion = null;
            
            newEOBudgetMouvGestion= (EOBudgetMouvGestion) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMouvGestion.ENTITY_NAME);
            
            
            newEOBudgetMouvGestion.addTypeSens(newEOBudgetMouvGestion,setTypeSens);
            newEOBudgetMouvGestion.addTypeCredit(newEOBudgetMouvGestion,setTypeCredit);
            newEOBudgetMouvGestion.addTypeAction(newEOBudgetMouvGestion,setTypeAction);
            newEOBudgetMouvGestion.addOrgan(newEOBudgetMouvGestion,setOrgan);
            newEOBudgetMouvGestion.addBudgetMouvements(newEOBudgetMouvGestion,setBudgetMouvements);
            newEOBudgetMouvGestion.addExercice(newEOBudgetMouvGestion,setExercice);
            
            newEOBudgetMouvGestion.setBdmgMontant (setBdmgMontant);
            
            newEOBudgetMouvGestion.setTypeEtatRelationship(setTypeEtat);
            
            monEOEditingContext.insertObject( newEOBudgetMouvGestion );
            
            return newEOBudgetMouvGestion;
            
        } catch (Exception e) {
            throw e;
        }
    }
    
       
}
