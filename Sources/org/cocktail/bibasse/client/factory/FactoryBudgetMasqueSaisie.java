/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *
 */
package org.cocktail.bibasse.client.factory;

import org.cocktail.bibasse.client.finder.FinderPlanComptable;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueGestion;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryBudgetMasqueSaisie extends FactoryBibasse {

    public FactoryBudgetMasqueSaisie() {
        super();
    }

    public FactoryBudgetMasqueSaisie(boolean withLog) {
        super(withLog);
    }


    /**
     *
     * @param monEOEditingContext
     * @param exercice
     * @param typeAction
     * @param typeEtat
     * @return
     * @throws Exception
     */
    public EOBudgetMasqueCredit creerMasqueSaisieCredit(
            EOEditingContext monEOEditingContext,
            EOExercice exercice,
            EOTypeCredit typeCredit,
            EOTypeEtat typeEtat
    ) throws Exception
    {

        try {

            EOBudgetMasqueCredit newMasque = null;

            newMasque= (EOBudgetMasqueCredit) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMasqueCredit.ENTITY_NAME);

            newMasque.addObjectToBothSidesOfRelationshipWithKey( exercice , "exercice");
            newMasque.addObjectToBothSidesOfRelationshipWithKey( typeCredit , "typeCredit");
            newMasque.addObjectToBothSidesOfRelationshipWithKey( typeEtat , "typeEtat");

            monEOEditingContext.insertObject( newMasque );

            return newMasque;


        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *
     * @param monEOEditingContext
     * @param exercice
     * @param typeAction
     * @param typeEtat
     * @return
     * @throws Exception
     */
    public EOBudgetMasqueGestion creerMasqueSaisieGestion
    (
            EOEditingContext monEOEditingContext,
            EOExercice exercice,
            EOTypeAction typeAction,
            EOTypeEtat typeEtat
    ) throws Exception
    {

        try {

            EOBudgetMasqueGestion newMasqueGestion = null;

            newMasqueGestion= (EOBudgetMasqueGestion) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMasqueGestion.ENTITY_NAME);

            newMasqueGestion.addObjectToBothSidesOfRelationshipWithKey( exercice , "exercice");
            newMasqueGestion.addObjectToBothSidesOfRelationshipWithKey( typeAction , "typeAction");
            newMasqueGestion.addObjectToBothSidesOfRelationshipWithKey( typeEtat , "typeEtat");

            monEOEditingContext.insertObject( newMasqueGestion );

            return newMasqueGestion;


        } catch (Exception e) {
            throw e;
        }
    }

    /**
     *
     * @param monEOEditingContext
     * @param exercice
     * @param typeAction
     * @param typeEtat
     * @return
     * @throws Exception
     */
    public EOBudgetMasqueNature creerMasqueSaisieNature
    (
            EOEditingContext monEOEditingContext,
            EOExercice exercice,
            EOTypeCredit typeCredit,
            EOPlanComptable planco,
            EOTypeEtat typeEtat
    ) throws Exception
    {

        try {

            EOBudgetMasqueNature newMasqueNature = null;

            newMasqueNature= (EOBudgetMasqueNature) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMasqueNature.ENTITY_NAME);

            newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( exercice , "exercice");
            newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( typeCredit , "typeCredit");
            newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( planco , "planComptable");
            if (typeCredit.tcdSect().equals("1"))
            	newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( FinderPlanComptable.findCompte(monEOEditingContext,
            			planco.pcoNum().substring(0,2), exercice) , "planComptableVote");
            else
                if (typeCredit.tcdSect().equals("2"))	{
                	String compte = planco.pcoNum();
                	if (compte.length() > 2)
                		newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( FinderPlanComptable.findCompte(monEOEditingContext, compte.substring(0,3),
                				exercice) , "planComptableVote");
                	else
                		newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( FinderPlanComptable.findCompte(monEOEditingContext, compte,
                				exercice) , "planComptableVote");
                }
            newMasqueNature.addObjectToBothSidesOfRelationshipWithKey( typeEtat , "typeEtat");

            monEOEditingContext.insertObject( newMasqueNature );

            return newMasqueNature;


        } catch (Exception e) {
            throw e;
        }
    }

}
