/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;



public class FactoryBudgetSaisieGestion  extends FactoryBibasse {
    
    private static final String ERROR_MESS_VERIF_ORGAN ="IMPOSSIBLE DE CREER L OBJET : PROBLEME ORGAN";
    
    public FactoryBudgetSaisieGestion()  {
        super();
        
    }
    
    public FactoryBudgetSaisieGestion(boolean withLog)  {
        super(withLog);
    }
    
    
    /** Cette methode permet de creer une ligne de saisie du budget de gestion
     * 
     * @param monEOEditingContext editingcontexte de travail
     * @param setTypeEtatValide etat VALIDE
     * @param setTypeCredit type de credit
     * @param setTypeAction action LOLF
     * @param setOrgan ligne de l'organigramme
     * @param setExercice exercice de la saisie
     * @param setBudgetSaisie lien avec la saisie en cours (1 saisie par convention,une saisie pour le reste)
     * @param setBdsgMontant montant de la saisie.
     * @return object cree
     * @throws Exception probleme organ,etat,etc....
     */
    public  EOBudgetSaisieGestion creerEOBudgetSaisieGestion(
            EOEditingContext monEOEditingContext,
            EOTypeEtat setTypeEtatValide,
            EOTypeCredit setTypeCredit,
            EOTypeAction setTypeAction,
            EOOrgan setOrgan,
            EOExercice setExercice,
            EOBudgetSaisie setBudgetSaisie,
            BigDecimal setBdsgVote,
            BigDecimal setBdsgSaisi,
            BigDecimal setBdsgMontant
    ) throws Exception
    {
    	        
//        trace(" creerEOBudgetSaisieGestion");
//        trace("EOEditingContext monEOEditingContext");
//        trace("EOTypeEtat setTypeEtatValide");
//        trace("EOTypeCredit setTypeCredit");
//        trace("EOTypeAction setTypeAction");
//        trace(" EOOrgan setOrgan");
//        trace(" EOExercice setExercice");
//        trace(" EOBudgetSaisie setBudgetSaisie");
//        trace(" BigDecimal setBdsgMontant");
//        
//        trace( monEOEditingContext);
//        trace( setTypeEtatValide);
//        trace( setTypeCredit);
//        trace( setTypeAction);
//        trace(setOrgan);
//        trace( setExercice);
//        trace( setBudgetSaisie);
//        trace( setBdsgMontant);
        
        EOBudgetSaisieGestion newEOBudgetSaisieGestion=null;
        
        try {
            //verifierEtat_VALIDE(setTypeEtatValide,"setTypeEtatValide");
            
            if (this. verifierOrgan ( setBudgetSaisie, setOrgan)){
                
                newEOBudgetSaisieGestion= (EOBudgetSaisieGestion) FactoryBibasse.instanceForEntity(
                        monEOEditingContext,
                        EOBudgetSaisieGestion.ENTITY_NAME);
                
                monEOEditingContext.insertObject(newEOBudgetSaisieGestion);
                
                newEOBudgetSaisieGestion.addTypeEtat(newEOBudgetSaisieGestion,setTypeEtatValide);
                newEOBudgetSaisieGestion.addTypeCredit(newEOBudgetSaisieGestion,setTypeCredit);
                newEOBudgetSaisieGestion.addTypeAction(newEOBudgetSaisieGestion,setTypeAction);
                newEOBudgetSaisieGestion.addOrgan(newEOBudgetSaisieGestion,setOrgan);
                newEOBudgetSaisieGestion.addExercice(newEOBudgetSaisieGestion,setExercice);
                newEOBudgetSaisieGestion.addBudgetSaisie(newEOBudgetSaisieGestion,setBudgetSaisie);
                
                newEOBudgetSaisieGestion.setBdsgVote(setBdsgVote);
                newEOBudgetSaisieGestion.setBdsgSaisi(setBdsgSaisi);
                newEOBudgetSaisieGestion.setBdsgMontant(setBdsgMontant);
                
            }
            else
                throw new Exception("creerEOBudgetSaisieGestion : "+FactoryBudgetSaisieGestion.ERROR_MESS_VERIF_ORGAN);
                        
            return newEOBudgetSaisieGestion;
            
        } catch (Exception e) {
            throw e;
        }
    }
    
    
    
    
    /**Permet d annuler un lignede saisie pour le budget de gestion
     * impossible de modifier la ligne apres cette mise a jour
     * @param monEOEditingContext editing context de travail
     * @param monEOBudgetSaisieGestion object en cours de manipulation
     * @param setTypeEtatAnnule etat ANNULE 
     * @throws Exception probleme d'etat
     */
    public  void annulerEOBudgetSaisieGestion(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisieGestion monEOBudgetSaisieGestion,
            EOTypeEtat setTypeEtatAnnule
    )  throws Exception
    {
        
        trace("Methode annulerEOBudgetSaisieGestion");
        trace("EOEditingContext monEOEditingContext");
        trace("EOBudgetSaisie monEOBudgetSaisieGestion");
        trace("EOTypeEtat setTypeEtatAnnule");
        
        trace(monEOEditingContext);
        trace(monEOBudgetSaisieGestion);
        trace(setTypeEtatAnnule);
        
        
        try {
            verifierEtat_VALIDE_ANNULE ((EOTypeEtat)monEOBudgetSaisieGestion.typeEtat(),setTypeEtatAnnule);
            
            monEOBudgetSaisieGestion.addTypeEtat( monEOBudgetSaisieGestion,setTypeEtatAnnule);  
            
        } catch (Exception e) {
            throw e;
        }
        
    }
    
    
    /** Permet de valider la saisie dune ligne de budgetde gestion
     *  impossible de la modifier apres quelle soit cloture
     * @param monEOEditingContext editing contest de travail
     * @param monEOBudgetSaisieGestion object en cours de manipulation
     * @param setTypeEtatClot etat CLOT
     * @param setUtilisateurValidation utilisateur qui clot la ligne
     * @throws Exception problemes d'etat
     */
    
    public  void cloturerEOBudgetSaisieGestion(
            EOEditingContext monEOEditingContext,
            EOBudgetSaisieGestion monEOBudgetSaisieGestion,
            EOTypeEtat setTypeEtatClot
    )  throws Exception
    {
        
        trace("Methode cloturerEOBudgetSaisieGestion");
        trace("EOEditingContext monEOEditingContext");
        trace("EOBudgetSaisie monEOBudgetSaisieGestion");
        trace("EOTypeEtat setTypeEtatClot");
        
        trace(monEOEditingContext);
        trace(monEOBudgetSaisieGestion);
        trace(setTypeEtatClot);
        
        try {
            
            verifierEtat_VALIDE_CLOT ((EOTypeEtat)monEOBudgetSaisieGestion.typeEtat(),setTypeEtatClot);
            
            monEOBudgetSaisieGestion.addTypeEtat( monEOBudgetSaisieGestion,setTypeEtatClot);
        } catch (Exception e) {
            throw e;
        }      
    }
    
    
    
    
    /*PRIVATE
     * PRIVATE
     * PRIVATE
     */
    
    
    private boolean verifierOrgan (EOBudgetSaisie setBudgetSaisie,EOOrgan setOrgan)  {
        
            return true;
    }
    
}
