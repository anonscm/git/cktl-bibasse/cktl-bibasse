/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSens;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryBudgetMouvementsCredit extends FactoryBibasse {

    public FactoryBudgetMouvementsCredit() {
        super();

    }

    public FactoryBudgetMouvementsCredit(boolean withLog) {
        super(withLog);

    }


    public EOBudgetMouvCredit creerEOBudgetMouvementsCredit
    (
            EOEditingContext monEOEditingContext,
            EOTypeSens setTypeSens,
            EOTypeCredit setTypeCredit,
            EOOrgan setOrgan,
            EOBudgetMouvements setBudgetMouvements,
            EOExercice setExercice,
            EOTypeEtat setTypeEtat,
            BigDecimal setBdmcMontant
    ) throws Exception
    {
        
        try {

            
            EOBudgetMouvCredit newEOBudgetMouvCredit = null;
            
            newEOBudgetMouvCredit= (EOBudgetMouvCredit) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMouvCredit.ENTITY_NAME);
            
            newEOBudgetMouvCredit.addTypeSens(newEOBudgetMouvCredit,setTypeSens);
            newEOBudgetMouvCredit.addTypeCredit(newEOBudgetMouvCredit,setTypeCredit);
            newEOBudgetMouvCredit.addOrgan(newEOBudgetMouvCredit,setOrgan);
            newEOBudgetMouvCredit.addBudgetMouvements(newEOBudgetMouvCredit,setBudgetMouvements);
            newEOBudgetMouvCredit.addExercice(newEOBudgetMouvCredit,setExercice);
            newEOBudgetMouvCredit.addTypeEtat(newEOBudgetMouvCredit,setTypeEtat);
            
            newEOBudgetMouvCredit.setBdmcMontant (setBdmcMontant);
    
            monEOEditingContext.insertObject( newEOBudgetMouvCredit );
            
            return newEOBudgetMouvCredit;
            
        } catch (Exception e) {
            throw e;
        }
    }
    
       
}
