/*
 * Created on 8 juil. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.bibasse.client.factory;

import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueCredit;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;




/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 * 
 */

public  abstract class FactoryBibasse extends Factory{



    public FactoryBibasse() {
        super();
        this.setWithLogs(false);
    }

    public FactoryBibasse(boolean withLog) {
        super();
        this.setWithLogs(withLog);
    }


    
    
    
    
    
    
    public  void  verifierEtat_VALIDE_CLOT (EOTypeEtat ancien,EOTypeEtat nouveau) throws Exception
    {
        verifierEtat_NON_TRAITE(ancien,"ancien");
        verifierEtat_NON_TRAITE(nouveau,"nouveau");
        
        verifierEtat_VALIDE(ancien,"ancien");
        verifierEtat_CLOT(nouveau,"nouveau");
        
    }
    
    public  void  verifierEtat_VALIDE_ANNULE (EOTypeEtat ancien,EOTypeEtat nouveau) throws Exception
    {
        verifierEtat_NON_TRAITE(ancien,"ancien");
        verifierEtat_NON_TRAITE(nouveau,"nouveau");
        
        verifierEtat_VALIDE(ancien,"ancien");
        verifierEtat_ANNULE(nouveau,"nouveau");  
    }
    
        
    public  void  verifierEtat_VALIDE (EOTypeEtat nouveau,String mess) throws Exception
    {
             
        if (! nouveau.tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE) ) 
            throw new Exception(mess + ". " + EOTypeEtat.ERROR_MESS_ETAT_VALIDE);
        
    }
    
    public  void  verifierEtat_ANNULE (EOTypeEtat nouveau,String mess) throws Exception
    {
             
        if (! nouveau.tyetLibelle().equals(EOTypeEtat.ETAT_ANNULE) ) 
            throw new Exception(mess+EOTypeEtat.ERROR_MESS_ETAT_ANNULE);
        
    }
    
    public  void  verifierEtat_CLOT (EOTypeEtat nouveau,String mess) throws Exception
    {
             
        if (! nouveau.tyetLibelle().equals(EOTypeEtat.ETAT_CLOT) ) 
            throw new Exception(mess+EOTypeEtat.ERROR_MESS_ETAT_CLOT);
        
    }
    
    public  void  verifierEtat_NON_TRAITE (EOTypeEtat nouveau,String mess) throws Exception
    {
        
        if (nouveau.tyetLibelle().equals(EOTypeEtat.ETAT_TRAITE) ) 
            throw new Exception(mess+EOTypeEtat.ERROR_MESS_ETAT_NON_TRAITE);
        
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    public static boolean controlerOuvertureBudget(EOEditingContext ec, EOExercice exercice) throws Exception	{
    	
       	NSArray masquesCredit = FinderBudgetMasqueCredit.findMasqueCredit(ec, exercice);
    	
    	NSArray masquesGestion = FinderBudgetMasqueGestion.findMasqueGestion(ec, exercice);

    	NSArray masquesNature = FinderBudgetMasqueNature.findMasquesNatureForExercice(ec, exercice);

    	String errorMessage = "";

    	if (masquesCredit.count() == 0)
    		errorMessage = errorMessage.concat("  - Types de crédit\n");

    	if (masquesGestion.count() == 0)
    		errorMessage = errorMessage.concat("  - Budget de Gestion\n");

    	if (masquesNature.count() == 0)
    		errorMessage = errorMessage.concat("  - Budget par Nature\n");
    	
    	if (!"".equals(errorMessage))	{
    		throw new Exception("Les masques de saisie suivant n'ont pas été initialisés :\n\n"
    				+ errorMessage + "\n\nVeuillez les mettre à jour dans le menu 'Paramétrages / Masques de saisie");
    	}

    	return true;
    }
    

    /**
     * 
     * Validation de la saisie d'un CR ou d'une UB
     * 
     * @param ec
     * @param organ
     * @param exercice
     * @throws Exception
     */
    public static void validerSaisieOrgan(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ, EOExercice exercice) throws Exception	{
    	
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey((EOEnterpriseObject)budgetSaisie, "EOBudgetSaisie");
			parametres.setObjectForKey(organ, "EOOrgan");
			
			ServerProxy.clientSideRequestValiderLigneBudgetaire(ec, parametres);		
    }
    
}
