/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.bibasse.client.factory;

import java.math.BigDecimal;

import org.cocktail.bibasse.client.metier.EOBudgetMouvNature;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSens;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryBudgetMouvementsNature extends FactoryBibasse {

    public FactoryBudgetMouvementsNature() {
        super();

    }

    public FactoryBudgetMouvementsNature(boolean withLog) {
        super(withLog);

    }

    
    public EOBudgetMouvNature creerEOBudgetMouvementsNature
    (
            EOEditingContext monEOEditingContext,
            EOTypeSens setTypeSens,
            EOTypeCredit setTypeCredit,
            EOPlanComptable setPlanComptable,
            EOOrgan setOrgan,
            EOBudgetMouvements setBudgetMouvements,
            EOExercice setExercice,
            EOTypeEtat setTypeEtat,
            BigDecimal setBdmnMontant
    ) throws Exception
    {
        

        try {
            
            EOBudgetMouvNature newEOBudgetMouvNature = null;
            
            newEOBudgetMouvNature= (EOBudgetMouvNature) FactoryBibasse.instanceForEntity(
                    monEOEditingContext,
                    EOBudgetMouvNature.ENTITY_NAME);
            System.out
					.println("FactoryBudgetMouvementsNature.creerEOBudgetMouvementsNature() TYPE SENS : "+ setTypeSens);
            newEOBudgetMouvNature.addTypeSens(newEOBudgetMouvNature,setTypeSens);
            newEOBudgetMouvNature.addTypeCredit(newEOBudgetMouvNature,setTypeCredit);
            newEOBudgetMouvNature.addPlanComptable(newEOBudgetMouvNature,setPlanComptable);
            newEOBudgetMouvNature.addOrgan(newEOBudgetMouvNature,setOrgan);
            newEOBudgetMouvNature.addBudgetMouvements(newEOBudgetMouvNature,setBudgetMouvements);
            newEOBudgetMouvNature.addExercice(newEOBudgetMouvNature,setExercice);
            
            newEOBudgetMouvNature.setTypeEtatRelationship(setTypeEtat);
            
            newEOBudgetMouvNature.setBdmnMontant (setBdmnMontant);
            
            monEOEditingContext.insertObject( newEOBudgetMouvNature );
            
            return newEOBudgetMouvNature;
            
        } catch (Exception e) {
            throw e;
        }
    }
    

}
