package org.cocktail.bibasse.client.masques;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.process.budget.ProcessBudgetFactory;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionRecettes;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureRecettes;
import org.cocktail.bibasse.client.utils.XWaitingDialog;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class MasqueSaisie {

	private static MasqueSaisie sharedInstance;
	private EOEditingContext ec;

	protected   ApplicationClient NSApp;
	protected	JFrame mainFrame;
	protected	JFrame window;

	protected 	ButtonGroup matriceTypeBudget;
	protected	JRadioButton radioButtonCredit;
	protected	JRadioButton radioButtonGestion;
	protected	JRadioButton radioButtonNature;

	protected	JPanel mainView, swapViewMasques, swapViewTop;

	protected ActionValidate 		actionValidate = new ActionValidate();
	protected ActionSave 			actionSave = new ActionSave();
	protected ActionCancel 			actionCancel = new ActionCancel();
	protected ActionClose 			actionClose = new ActionClose();

	protected 	EOBudgetSaisie			currentBudgetSaisie = null;
	protected 	EOBudgetSaisieGestion 	currentBudgetGestion = null;
	protected 	EOBudgetSaisieNature 	currentBudgetNature = null;
	protected	EOTypeSaisie 			currentTypeSaisie = null;
	protected	EOOrgan 				currentOrgan;
	protected	EOTypeEtat				currentTypeEtatOrgan = null;

	protected XWaitingDialog waitingFrame;

	public JTextField typeSaisieBudget;
	public JTextField informations;
	public JTextField etatSaisieBudget;

	protected	NSArray budgetsGestion = new NSArray();
	protected	NSArray budgetsNature = new NSArray();

	protected ProcessBudgetFactory myProcessBudgetFactory;

	/**
	 *
	 *
	 */
	public MasqueSaisie(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		initView();
	}

	/**
	 *
	 *
	 */
	public void initView()	{

		swapViewTop = new JPanel(new BorderLayout());
		swapViewMasques = new JPanel(new CardLayout());

        window = new JFrame();
        window.setTitle("Saisie du Budget");
        window.setSize(925, 550);

        Box topBox = Box.createHorizontalBox();
        topBox.add(Box.createHorizontalGlue());
        JPanel panelOrgan = new JPanel(new BorderLayout());
        panelOrgan.add(topBox, BorderLayout.CENTER);

        matriceTypeBudget = new ButtonGroup();

        radioButtonCredit = new JRadioButton("Masque TYPE CREDIT", true);
        radioButtonCredit.setForeground(Color.YELLOW);
        radioButtonCredit.setBackground(Color.BLACK);
        radioButtonCredit.setFont(new Font("Arial", Font.PLAIN, 12));
        radioButtonCredit.addItemListener(new typeBudgetListener());
        matriceTypeBudget.add(radioButtonCredit);
        radioButtonCredit.setSelected(true);

        radioButtonGestion = new JRadioButton("Masque Budget GESTION", true);
        radioButtonGestion.setForeground(Color.YELLOW);
        radioButtonGestion.setBackground(Color.BLACK);
        radioButtonGestion.setFont(new Font("Arial", Font.PLAIN, 12));
        radioButtonGestion.addItemListener(new typeBudgetListener());
        matriceTypeBudget.add(radioButtonGestion);

        radioButtonNature = new JRadioButton("Masque Budget NATURE", true);
        radioButtonNature.setForeground(Color.YELLOW);
        radioButtonNature.setBackground(Color.BLACK);
        radioButtonNature.setFont(new Font("Arial", Font.PLAIN, 12));
        radioButtonNature.addItemListener(new typeBudgetListener());
        matriceTypeBudget.add(radioButtonNature);


        Box boxSaisie1 = Box.createHorizontalBox();
        Box boxSaisie2 = Box.createHorizontalBox();
        Box boxSaisie3 = Box.createHorizontalBox();
        boxSaisie1.add(Box.createHorizontalGlue());
        boxSaisie2.add(Box.createHorizontalGlue());
        boxSaisie3.add(Box.createHorizontalGlue());
		boxSaisie1.add(MasqueSaisieCredit.sharedInstance(ec).getPanel());
		boxSaisie2.add(MasqueSaisieGestion.sharedInstance(ec).getPanel());
		boxSaisie3.add(MasqueSaisieNature.sharedInstance(ec).getPanel());

		swapViewMasques.add("credit", boxSaisie1);
		swapViewMasques.add("gestion", boxSaisie2);
		swapViewMasques.add("nature", boxSaisie3);

        JPanel panelRadioBoutons = new JPanel(new FlowLayout());
        panelRadioBoutons.add(radioButtonCredit);
        panelRadioBoutons.add(radioButtonGestion);
        panelRadioBoutons.add(radioButtonNature);

		JPanel panelSelectbudget = new JPanel(new BorderLayout());
		panelSelectbudget.add(panelRadioBoutons, BorderLayout.CENTER);
        panelSelectbudget.setPreferredSize(new Dimension(panelSelectbudget.getWidth(), 35));

        swapViewTop.add(panelSelectbudget, BorderLayout.CENTER);

        informations = new JTextField("");
        informations.setBackground(new Color(236, 234, 149));
        informations.setForeground(new Color(0, 0, 0));
        informations.setBorder(BorderFactory.createEmptyBorder());
        informations.setHorizontalAlignment(0);
        informations.setFont(Configuration.instance().informationLabelFont(ec));
        informations.setHorizontalAlignment(JTextField.CENTER);
        informations.setEditable(false);
        informations.setFocusable(false);
		informations.setText("Masque de saisie du Budget de GESTION " + NSApp.getExerciceBudgetaire().exeExercice());

        JPanel panelSouth = new JPanel(new BorderLayout());
        panelSouth.add(informations, BorderLayout.CENTER);

        swapViewTop.add(panelSouth, BorderLayout.SOUTH);
        swapViewTop.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        // Boutons
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 110, 22));

		JPanel swapViewButtons = new JPanel(new BorderLayout());
		swapViewButtons.setBorder(BorderFactory.createEmptyBorder(5, 4, 2, 4));
		swapViewButtons.add(new JSeparator(), BorderLayout.NORTH);
		swapViewButtons.add(panelButtons, BorderLayout.EAST);

        mainView = new JPanel(new BorderLayout());
        mainView.add(swapViewTop, BorderLayout.NORTH);
        mainView.add(swapViewMasques, BorderLayout.CENTER);
        mainView.add(swapViewButtons, BorderLayout.SOUTH);

        window.setContentPane(mainView);
	}

	/**
	 *
	 * @param frame
	 */
	public void setMainFrame(JFrame frame)	{
		mainFrame = frame;
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static MasqueSaisie sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new MasqueSaisie(editingContext);
		return sharedInstance;
	}



	/**
	 *
	 *
	 */
	public void updateUI()	{

	}



	/**
	 *
	 *
	 */
	public void open()	{

		NSApp.setWaitCursor(window);

		String title = ConstantesCocktail.APPLICATION_NAME+" - Gestion des masques de saisie " + NSApp.getExerciceBudgetaire().exeExercice();
		window.setTitle(title);

		MasqueSaisieCredit.sharedInstance(ec).load();
		MasqueSaisieGestion.sharedInstance(ec).load();
		MasqueSaisieNature.sharedInstance(ec).load();

		if (!NSApp.hasFonction(ConstantesCocktail.ID_FCT_MASQUE))	{
			actionValidate.setEnabled(false);
			actionSave.setEnabled(false);
			actionCancel.setEnabled(false);
		}

		// Si la saisie budgetaire est commencee, on ne peut plus modifier les masques
		if (EOBudgetSaisie.saisieBudgetaireEnCours(ec, NSApp.getExerciceBudgetaire()))	{

			MasqueSaisieCredit.sharedInstance(ec).setUpdateEnable(false);
			MasqueSaisieGestion.sharedInstance(ec).setUpdateEnable(false);
			MasqueSaisieNature.sharedInstance(ec).setUpdateEnable(false);

		}
		else	{

			MasqueSaisieCredit.sharedInstance(ec).setUpdateEnable(true);
			MasqueSaisieGestion.sharedInstance(ec).setUpdateEnable(true);
			MasqueSaisieNature.sharedInstance(ec).setUpdateEnable(true);

		}

		ZUiUtil.centerWindow(window);
		window.show();

		updateUI();

		NSApp.setDefaultCursor(window);

	}


	/**
	 *
	 *
	 */
	public void clean()	{

		MasqueSaisieCredit.sharedInstance(ec).clean();
		MasqueSaisieGestion.sharedInstance(ec).clean();
		MasqueSaisieNature.sharedInstance(ec).clean();

	}


	/**
	 *
	 *
	 */
	public void close()	{
		window.dispose();
	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	public class typeBudgetListener implements ItemListener 	{

		public void itemStateChanged(ItemEvent e)	{

			if (e.getStateChange() == ItemEvent.SELECTED)	{

				if (radioButtonCredit.isSelected())	{
					((CardLayout)swapViewMasques.getLayout()).show(swapViewMasques,"credit");
					informations.setText("Masque de saisie des TYPES DE CREDIT " + NSApp.getExerciceBudgetaire().exeExercice());
				}
				else	{
					if (radioButtonGestion.isSelected())	{
						((CardLayout)swapViewMasques.getLayout()).show(swapViewMasques,"gestion");
						informations.setText("Masque de saisie du Budget de GESTION " + NSApp.getExerciceBudgetaire().exeExercice());
					}
					else	{
						((CardLayout)swapViewMasques.getLayout()).show(swapViewMasques,"nature");
						MasqueSaisieNature.sharedInstance(ec).load();
						informations.setText("Masque de saisie du Budget par NATURE " + NSApp.getExerciceBudgetaire().exeExercice());
					}

				}
			}
		}

	}


	/**
	 *
	 *
	 */
	public void initData()	{

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionValidate extends AbstractAction {

	    public ActionValidate() {
            super("Valider Saisie");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
        }


	    public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(mainView);

	    	try {

	    	}
	    	catch (Exception ex)	{
	    		EODialogs.runErrorDialog("ERREUR", ex.getMessage());
	    	}
	    	NSApp.setDefaultCursor(mainView);
	    }

	}






	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }

        public void actionPerformed(ActionEvent e) {

        	// On reinitialise tous les budget_saisie

        	SaisieBudgetGestionDepenses.sharedInstance(ec).initData();
        	SaisieBudgetGestionRecettes.sharedInstance(ec).initData();
        	SaisieBudgetNatureDepenses.sharedInstance(ec).initData();
        	SaisieBudgetNatureRecettes.sharedInstance(ec).initData();

        	window.hide();
        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionSave extends AbstractAction {

	    public ActionSave() {
            super("Enregistrer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SAVE);
        }


        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

        	try {
        	}
        	catch (Exception ex)	{
        		EODialogs.runErrorDialog("ERREUR", ex.getMessage());
        	}

        	NSApp.setDefaultCursor(mainView);
        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
        }

        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

        	ec.revert();

        	NSApp.setDefaultCursor(mainView);

        }
	}


}
