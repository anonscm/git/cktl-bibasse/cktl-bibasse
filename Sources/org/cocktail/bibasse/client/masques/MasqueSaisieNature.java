
package org.cocktail.bibasse.client.masques;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueCredit;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueNature;
import org.cocktail.bibasse.client.finder.FinderPlanComptable;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.selectors.PlancoSelectCtrl;
import org.cocktail.bibasse.client.utils.CocktailUtilities;
import org.cocktail.bibasse.client.utils.MsgPanel;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class MasqueSaisieNature {

	private static MasqueSaisieNature sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;
	
	protected JPanel mainPanel, viewTableTypeCredit, viewTablePlanco, viewTableMasque;
	protected	JTextField plancoVote;
	
	private EODisplayGroup eodTypeCredit, eodPlanco, eodMasque;
	private ZEOTable myEOTableTypeCredit, myEOTablePlanco, myEOTableMasque;
	private ZEOTableModel myTableModelTypeCredit, myTableModelPlanco, myTableModelMasque;
	private TableSorter myTableSorterTypeCredit, myTableSorterPlanco, myTableSorterMasque;
		
	protected ActionAdd 				actionAdd = new ActionAdd();
	protected ActionDelete 				actionDelete = new ActionDelete();
	protected ActionGetPlanComptable 	actionGetPlanComptable = new ActionGetPlanComptable();
	protected ActionDuplicate 			actionDuplicate = new ActionDuplicate();

	private RendererTypeCredit	rendererTypeCredit = new RendererTypeCredit();
	private RendererMasque	rendererMasque = new RendererMasque();
	
	private EOTypeCredit currentTypeCredit;
	private EOBudgetMasqueNature currentMasque;
		
	/**
	 * 
	 *
	 */
	public MasqueSaisieNature(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		initData();
		initView();
		initGUI();
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MasqueSaisieNature sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new MasqueSaisieNature(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	private void initView()	{
		
		JPanel panelLeft = new JPanel(new BorderLayout());
		JPanel panelCenter = new JPanel(new FlowLayout());
		
		mainPanel = new JPanel(new BorderLayout());

		viewTableTypeCredit = new JPanel(new BorderLayout());
		viewTablePlanco = new JPanel(new BorderLayout());
		viewTableMasque = new JPanel(new BorderLayout());
		
		JPanel panelTypeCredit = new JPanel(new BorderLayout());
		panelTypeCredit.setBorder(BorderFactory.createEmptyBorder(0,10,0,4));
		panelTypeCredit.add(viewTableTypeCredit);
		
		JButton btnAddAction = new JButton(actionAdd);
		btnAddAction.setPreferredSize(new Dimension(50,23));
		btnAddAction.setHorizontalAlignment(0);
		btnAddAction.setVerticalAlignment(0);
		btnAddAction.setHorizontalTextPosition(0);
		btnAddAction.setVerticalTextPosition(0);

		JButton btnDelAction = new JButton(actionDelete);
		btnDelAction.setPreferredSize(new Dimension(50,23));
		btnDelAction.setHorizontalAlignment(0);
		btnDelAction.setVerticalAlignment(0);
		btnDelAction.setHorizontalTextPosition(0);
		btnDelAction.setVerticalTextPosition(0);
				
		JButton btnDuplicate = new JButton(actionDuplicate);
		btnDuplicate.setToolTipText("Dupliquer le masque de l'annee " + (NSApp.getExerciceBudgetaire().exeExercice().intValue() - 1));
		btnDuplicate.setPreferredSize(new Dimension(50,45));
		btnDuplicate.setHorizontalAlignment(0);
		btnDuplicate.setVerticalAlignment(0);
		btnDuplicate.setHorizontalTextPosition(0);
		btnDuplicate.setVerticalTextPosition(0);

		
		ArrayList arrayList = new ArrayList();
		arrayList.add(btnAddAction);
		arrayList.add(btnDelAction);
		arrayList.add(btnDuplicate);
      
		Component panelTemp = ZUiUtil.buildBoxColumn(arrayList);

		panelCenter.setBorder(BorderFactory.createEmptyBorder(60, 10,0,10));
		panelCenter.add(panelTemp, BorderLayout.NORTH);
		
		JSplitPane splitPaneLeft = ZUiUtil.buildVerticalSplitPane(viewTableTypeCredit, viewTablePlanco, 0.3, 0.3);

		panelLeft.add(splitPaneLeft, BorderLayout.CENTER);
		
		plancoVote = new JTextField("");
//		plancoVote.setPreferredSize(new Dimension(300,22));
		plancoVote.setEditable(false);
		plancoVote.setHorizontalAlignment(JTextField.LEFT);
		plancoVote.setFocusable(false);
		plancoVote.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		
		JButton btnGetPlanco = new JButton(actionGetPlanComptable);
		btnGetPlanco.setPreferredSize(new Dimension(22,22));
		btnGetPlanco.setHorizontalAlignment(0);
		btnGetPlanco.setVerticalAlignment(0);
		btnGetPlanco.setHorizontalTextPosition(0);
		btnGetPlanco.setVerticalTextPosition(0);

		JPanel panelSelectPlanco = new JPanel(new BorderLayout());
		panelSelectPlanco.setBorder(BorderFactory.createEmptyBorder(5,0,5,0));
		panelSelectPlanco.add(new JLabel("Compte Voté : "), BorderLayout.WEST);
		panelSelectPlanco.add(plancoVote, BorderLayout.CENTER);
		panelSelectPlanco.add(btnGetPlanco, BorderLayout.EAST);

		JPanel containerPlancoVote = new JPanel(new BorderLayout());
		containerPlancoVote.add(panelSelectPlanco, BorderLayout.WEST);
		
		JPanel panelRight = new JPanel(new BorderLayout());
		panelRight.add(viewTableMasque, BorderLayout.CENTER);
		panelRight.add(panelSelectPlanco, BorderLayout.SOUTH);
		
		arrayList = new ArrayList();
		arrayList.add(panelLeft);
		arrayList.add(panelCenter);
		arrayList.add(panelRight);
		
		JPanel panelGestion = new JPanel(new BorderLayout());
		panelGestion.add(ZUiUtil.buildBoxLine(arrayList), BorderLayout.CENTER);

		if (!NSApp.hasFonction(ConstantesCocktail.ID_FCT_MASQUE))	{
			actionAdd.setEnabled(false);
			actionDelete.setEnabled(false);
		}

		mainPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));		
		mainPanel.add(panelGestion, BorderLayout.CENTER);
	}

	/**
	 * 
	 *
	 */
	public void initData()	{
	}
	
	/**
	 * 
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		eodTypeCredit = new EODisplayGroup();
		eodPlanco = new EODisplayGroup();
		eodMasque = new EODisplayGroup();
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOTypeCredit.TCD_TYPE_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOTypeCredit.TCD_SECT_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareAscending));

		eodTypeCredit.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending));
		eodPlanco.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOBudgetMasqueNature.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOBudgetMasqueNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareAscending));
		eodMasque.setSortOrderings(mySort);

		
		initTableModel();
		initTable();
		
		myEOTableTypeCredit.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableTypeCredit.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableTypeCredit.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTableTypeCredit.setBorder(BorderFactory.createEmptyBorder());
		viewTableTypeCredit.removeAll();
		viewTableTypeCredit.setLayout(new BorderLayout());
		viewTableTypeCredit.add(new JScrollPane(myEOTableTypeCredit), BorderLayout.CENTER);

		
		myEOTablePlanco.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTablePlanco.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTablePlanco.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTablePlanco.setBorder(BorderFactory.createEmptyBorder());
		viewTablePlanco.removeAll();
		viewTablePlanco.setLayout(new BorderLayout());
		viewTablePlanco.add(new JScrollPane(myEOTablePlanco), BorderLayout.CENTER);

		
		
		myEOTableMasque.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableMasque.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableMasque.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableMasque.setBorder(BorderFactory.createEmptyBorder());
		viewTableMasque.removeAll();
		viewTableMasque.setLayout(new BorderLayout());
		viewTableMasque.add(new JScrollPane(myEOTableMasque), BorderLayout.CENTER);
	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{
		
		myEOTableTypeCredit = new ZEOTable(myTableSorterTypeCredit);
		myEOTableTypeCredit.addListener(new ListenerTypeCredit());
					
		myTableSorterTypeCredit.setTableHeader(myEOTableTypeCredit.getTableHeader());		

		myEOTablePlanco = new ZEOTable(myTableSorterPlanco);
		myTableModelPlanco.addTableModelListener(new ListenerPlanco());
		myTableSorterPlanco.setTableHeader(myEOTablePlanco.getTableHeader());		
		
		myEOTableMasque = new ZEOTable(myTableSorterMasque);
		myTableModelMasque.addTableModelListener(new ListenerMasque());
		myTableSorterMasque.setTableHeader(myEOTableMasque.getTableHeader());		
		myEOTableMasque.addListener(new TableListenerMasque());
	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
						
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eodTypeCredit, EOTypeCredit.TCD_SECT_KEY, "Section", 10);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setTableCellRenderer(rendererTypeCredit);
		myCols.add(col1);

		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodTypeCredit, EOTypeCredit.TCD_CODE_KEY, "Code", 10);
		col2.setAlignment(SwingConstants.CENTER);
		col2.setTableCellRenderer(rendererTypeCredit);
		myCols.add(col2);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(eodTypeCredit, EOTypeCredit.TCD_LIBELLE_KEY, "Libellé", 100);
		col3.setAlignment(SwingConstants.LEFT);
		col3.setTableCellRenderer(rendererTypeCredit);
		myCols.add(col3);

		myTableModelTypeCredit = new ZEOTableModel(eodTypeCredit, myCols);
		myTableSorterTypeCredit = new TableSorter(myTableModelTypeCredit);

		// PLANCO
		
		myCols = new Vector();
		
		col1 = new ZEOTableModelColumn(eodPlanco, EOPlanComptable.PCO_NUM_KEY, "Code", 20);
		col1.setAlignment(SwingConstants.LEFT);
		myCols.add(col1);

		col2 = new ZEOTableModelColumn(eodPlanco, EOPlanComptable.PCO_LIBELLE_KEY, "Libellé", 160);
		col2.setAlignment(SwingConstants.LEFT);
		myCols.add(col2);

		col3 = new ZEOTableModelColumn(eodPlanco, "validite", "V", 20);
		col3.setAlignment(SwingConstants.LEFT);
		myCols.add(col3);

		myTableModelPlanco = new ZEOTableModel(eodPlanco, myCols);
		myTableSorterPlanco = new TableSorter(myTableModelPlanco);

		
		// MASQUE
		
		myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodMasque, EOBudgetMasqueNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, "Tcd", 30);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, EOBudgetMasqueNature.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, "Cpt Saisie", 20);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, EOBudgetMasqueNature.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_LIBELLE_KEY, "Libellé", 120);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, EOBudgetMasqueNature.PLAN_COMPTABLE_VOTE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, "Planco Voté", 20);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);
		
		myTableModelMasque = new ZEOTableModel(eodMasque, myCols);
		myTableSorterMasque = new TableSorter(myTableModelMasque);
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerTypeCredit implements ZEOTable.ZEOTableListener 	{
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			clean();
			currentTypeCredit = (EOTypeCredit)eodTypeCredit.selectedObject();
			if (currentTypeCredit != null)	{
				refreshMasque();
				refreshPlanco();
			}
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPlanco implements TableModelListener 	{
		public void tableChanged(TableModelEvent e) {
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerMasque implements TableModelListener 	{
		public void tableChanged(TableModelEvent e) {
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class TableListenerMasque implements ZEOTable.ZEOTableListener  	{
		public void onDbClick() {
			actionGetPlanComptable.actionPerformed(null);
		}
		public void onSelectionChanged() {
			currentMasque = (EOBudgetMasqueNature)eodMasque.selectedObject();
			if (currentMasque != null)
				CocktailUtilities.setTextTextField(plancoVote, currentMasque.planComptableVote().pcoNum()+" - " + currentMasque.planComptableVote().pcoLibelle());
			else
				plancoVote.setText("");
		}
	}

	/**
	 * 
	 *
	 */
	public void updateUI()	{
	}
	
	/**
	 * 
	 *
	 */
	public void clean()	{
		eodMasque.setObjectArray(new NSArray());
		myEOTableMasque.updateData();
	}

	/**
	 * 
	 *
	 */
	public void refreshPlanco()	{
		NSMutableArray listePlancos = new NSMutableArray(FinderPlanComptable.findComptesForTypeCreditViaPlancoCredit(ec, currentTypeCredit));
		NSMutableArray listeMasques = new NSMutableArray((NSArray)eodMasque.displayedObjects().valueForKey("planComptable"));

		listePlancos.removeObjectsInArray(listeMasques);

		eodPlanco.setObjectArray(listePlancos);
		myEOTablePlanco.updateData();		
	}
	
	/**
	 * 
	 *
	 */
	public void refreshMasque()	{
		eodMasque.setObjectArray(FinderBudgetMasqueNature.findMasquesNatureForTypeCredit(ec, NSApp.getExerciceBudgetaire(), currentTypeCredit));
		myEOTableMasque.updateData();
		myTableModelMasque.fireTableDataChanged();
	}
	
	/**
	 *
	 */
	public void setUpdateEnable(boolean enable)	{
		actionDuplicate.setEnabled(enable);
	}
	
	/**
	 * 
	 *
	 */
	public void load()	{
		NSApp.setWaitCursor(mainPanel);

		NSMutableArray types = new NSMutableArray((NSArray)FinderBudgetMasqueCredit.findMasqueCredit(ec, NSApp.getExerciceBudgetaire()).valueForKey("typeCredit"));
		eodTypeCredit.setObjectArray(types);
		myEOTableTypeCredit.updateData();
		
		NSApp.setDefaultCursor(mainPanel);
	}
	
		
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	private class RendererMasque extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
						
			String type = ((EOBudgetMasqueNature)eodMasque.displayedObjects().objectAtIndex(row)).typeCredit().tcdType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			}	
			else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}
			
			return leComposant;
		}
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	private class RendererTypeCredit extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			String type = ((EOTypeCredit)eodTypeCredit.displayedObjects().objectAtIndex(row)).tcdType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			}	
			else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionAdd extends AbstractAction {
	    public ActionAdd() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_DROITE);
        }
        public void actionPerformed(ActionEvent e) {
    		NSApp.setWaitCursor(mainPanel);

        	try {        		
    			currentTypeCredit = (EOTypeCredit)eodTypeCredit.selectedObject();
        		for (int i=0;i<eodPlanco.selectedObjects().count();i++)	{
        			
        			EOPlanComptable planco = (EOPlanComptable)eodPlanco.selectedObjects().objectAtIndex(i);

            		NSMutableDictionary parametres = new NSMutableDictionary();
            		parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
            		parametres.setObjectForKey((EOEnterpriseObject)planco, "EOPlanComptable");
            		parametres.setObjectForKey((EOEnterpriseObject)currentTypeCredit, "EOTypeCredit");
            		
            		ServerProxy.clientSideRequestAddMasqueNature(ec, parametres);
        		}
    			    			
    			eodMasque.setObjectArray(FinderBudgetMasqueNature.findMasquesNatureForTypeCredit(ec, NSApp.getExerciceBudgetaire(), currentTypeCredit));
    			myEOTableMasque.updateData();

    			eodPlanco.deleteSelection();
    			myEOTablePlanco.updateData();
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        	}
        	
    		NSApp.setDefaultCursor(mainPanel);
        }  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionDelete extends AbstractAction {

	    public ActionDelete() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_GAUCHE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
    		NSApp.setWaitCursor(mainPanel);

        	try {
        		
    			NSMutableArray listePlancos = new NSMutableArray(eodPlanco.displayedObjects());

    			for (int i=0;i<eodMasque.selectedObjects().count();i++)	{
        			
        			EOBudgetMasqueNature masque = (EOBudgetMasqueNature)eodMasque.selectedObjects().objectAtIndex(i);

        			EOPlanComptable planco =  masque.planComptable();

            		NSMutableDictionary parametres = new NSMutableDictionary();
            		parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
            		parametres.setObjectForKey((EOEnterpriseObject)planco, "EOPlanComptable");
            		parametres.setObjectForKey((EOEnterpriseObject)masque.typeCredit(), "EOTypeCredit");
            		
            		ServerProxy.clientSideRequestDelMasqueNature(ec, parametres);

        			listePlancos.addObject(masque.planComptable());

        		}

    			eodPlanco.setObjectArray(listePlancos);
    			myEOTablePlanco.updateData();
    			
    			eodMasque.deleteSelection();
       			myEOTableMasque.updateData();       		    			

        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        		MsgPanel.sharedInstance().runErrorDialog("ERREUR",NSApp.getErrorDialog(ex));
        	}
        	
    		NSApp.setDefaultCursor(mainPanel);

        }  
	} 

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionGetPlanComptable extends AbstractAction {

	    public ActionGetPlanComptable() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_INTERROGER);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	System.out.println("ActionGetPlanComptable.actionPerformed()");
        	try {
        		
        		EOPlanComptable newPlancoVote = PlancoSelectCtrl.sharedInstance(ec).getPlanComptable(
        				currentMasque.planComptableVote().pcoNum().substring(0,2), NSApp.getExerciceBudgetaire());
        		
        		if (newPlancoVote != null)	{
        			currentMasque.addObjectToBothSidesOfRelationshipWithKey(newPlancoVote, "planComptableVote");
        			ec.saveChanges();
        			refreshMasque();
        		}        		 
        		
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        	}
        	
        }  
	} 
	
	
	/**
	 * 
	 * Recuperation des masques de l'annee n-1
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionDuplicate extends AbstractAction {

	    public ActionDuplicate() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_PILOTAGE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	try {

        		NSMutableDictionary parametres = new NSMutableDictionary();
        		parametres.setObjectForKey("NATURE", "typeMasque");
        		parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
        		ServerProxy.clientSideRequestDupliquerMasque(ec, parametres);

        		load();
        		
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
        	}
        	
        }  
	} 
}
