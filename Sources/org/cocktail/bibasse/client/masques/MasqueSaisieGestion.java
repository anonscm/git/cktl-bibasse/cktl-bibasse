package org.cocktail.bibasse.client.masques;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.finder.FinderTypeAction;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueGestion;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.utils.MsgPanel;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class MasqueSaisieGestion {

	private static MasqueSaisieGestion sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;
	private EOTypeEtat typeEtatValide;

	protected JPanel mainPanel, viewTableActions, viewTableSousActions, viewTableMasque, swapViewActions;

	protected 	ButtonGroup matriceTypeAction;
	protected	JRadioButton radioButtonAction;
	protected	JRadioButton radioButtonSousAction;

	private EODisplayGroup eodActions, eodSousActions, eodMasque;
	private ZEOTable myEOTableActions, myEOTableSousActions, myEOTableMasque;
	private ZEOTableModel myTableModelActions,myTableModelSousActions, myTableModelMasque;
	private TableSorter myTableSorterActions, myTableSorterSousActions, myTableSorterMasque;

	private ActionRenderer	rendererActions = new ActionRenderer();
	private SousActionRenderer	rendererSousActions = new SousActionRenderer();
	private MasqueRenderer	rendererMasque = new MasqueRenderer();

	protected ActionAdd 			actionAdd = new ActionAdd();
	protected ActionDelete 			actionDelete = new ActionDelete();
	protected ActionDuplicate 		actionDuplicate = new ActionDuplicate();

	/**
	 *
	 *
	 */
	public MasqueSaisieGestion(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
		typeEtatValide = FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE);
		initView();
		initGUI();
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static MasqueSaisieGestion sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null) {
			sharedInstance = new MasqueSaisieGestion(editingContext);
		}
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	private void initView()	{

		mainPanel = new JPanel(new BorderLayout());

		viewTableActions = new JPanel(new BorderLayout());
		viewTableSousActions = new JPanel(new BorderLayout());
		viewTableMasque = new JPanel(new BorderLayout());

		viewTableActions.setBorder(BorderFactory.createEmptyBorder(2,5,2,0));
		viewTableSousActions.setBorder(BorderFactory.createEmptyBorder(5,5,2,0));
		viewTableMasque.setBorder(BorderFactory.createEmptyBorder(2,0,2,5));

		matriceTypeAction = new ButtonGroup();

		radioButtonAction = new JRadioButton("ACTIONS", true);
		radioButtonAction.setForeground(Color.BLUE);
		radioButtonAction.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButtonAction.addItemListener(new TypeActionListener());
		matriceTypeAction.add(radioButtonAction);
		radioButtonAction.setSelected(true);

		radioButtonSousAction = new JRadioButton("SOUS-ACTIONS", true);
		radioButtonSousAction.setForeground(Color.BLUE);
		radioButtonSousAction.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButtonSousAction.addItemListener(new TypeActionListener());
		matriceTypeAction.add(radioButtonSousAction);

		Box boxSaisie1 = Box.createHorizontalBox();
		Box boxSaisie2 = Box.createHorizontalBox();
		boxSaisie1.add(Box.createHorizontalGlue());
		boxSaisie2.add(Box.createHorizontalGlue());
		boxSaisie1.add(viewTableActions);
		boxSaisie2.add(viewTableSousActions);

		swapViewActions = new JPanel(new CardLayout());
		swapViewActions.add("actions",boxSaisie1);
		swapViewActions.add("sousActions",boxSaisie2);

		JPanel panelRadioBoutons = new JPanel(new FlowLayout());
		panelRadioBoutons.add(radioButtonAction);
		panelRadioBoutons.add(radioButtonSousAction);

		// CENTER
		ArrayList arrayList = new ArrayList();

		JButton btnAddAction = new JButton(actionAdd);
		btnAddAction.setPreferredSize(new Dimension(50,23));
		btnAddAction.setHorizontalAlignment(0);
		btnAddAction.setVerticalAlignment(0);
		btnAddAction.setHorizontalTextPosition(0);
		btnAddAction.setVerticalTextPosition(0);

		JButton btnDelAction = new JButton(actionDelete);
		btnDelAction.setPreferredSize(new Dimension(50,23));
		btnDelAction.setHorizontalAlignment(0);
		btnDelAction.setVerticalAlignment(0);
		btnDelAction.setHorizontalTextPosition(0);
		btnDelAction.setVerticalTextPosition(0);

		JButton btnDuplicate = new JButton(actionDuplicate);
		btnDuplicate.setToolTipText("Dupliquer le masque de l'annee " + (NSApp.getExerciceBudgetaire().exeExercice().intValue() - 1));
		btnDuplicate.setPreferredSize(new Dimension(50,45));
		btnDuplicate.setHorizontalAlignment(0);
		btnDuplicate.setVerticalAlignment(0);
		btnDuplicate.setHorizontalTextPosition(0);
		btnDuplicate.setVerticalTextPosition(0);

		arrayList.add(btnAddAction);
		arrayList.add(btnDelAction);
		arrayList.add(btnDuplicate);

		Component panelTemp = ZUiUtil.buildBoxColumn(arrayList);

		JPanel panelCenter = new JPanel(new FlowLayout());
		panelCenter.setBorder(BorderFactory.createEmptyBorder(60, 10,0,10));
		panelCenter.add(panelTemp, BorderLayout.NORTH);

		arrayList = new ArrayList();
		arrayList.add(panelRadioBoutons);
		arrayList.add(swapViewActions);

		JPanel panelActionsSousActions = new JPanel(new BorderLayout());
		panelActionsSousActions.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);

		arrayList = new ArrayList();
		arrayList.add(panelActionsSousActions);
		arrayList.add(panelCenter);
		arrayList.add(viewTableMasque);

		JPanel panelGestion = new JPanel(new BorderLayout());
		panelGestion.add(ZUiUtil.buildBoxLine(arrayList), BorderLayout.CENTER);

		if (!NSApp.hasFonction(ConstantesCocktail.ID_FCT_MASQUE))	{
			actionAdd.setEnabled(false);
			actionDelete.setEnabled(false);
		}

		mainPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		mainPanel.add(panelGestion, BorderLayout.CENTER);

	}

	/**
	 *
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

		eodActions = new EODisplayGroup();
		eodSousActions = new EODisplayGroup();
		eodMasque = new EODisplayGroup();

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("tyacType", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("tyacCode", EOSortOrdering.CompareAscending));

		eodActions.setSortOrderings(mySort);
		eodSousActions.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("typeAction.tyacCode", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("typeAction.tyacNiveau", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("typeAction.tyacType", EOSortOrdering.CompareAscending));
		eodMasque.setSortOrderings(mySort);

		initTableModel();
		initTable();

		myEOTableActions.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableActions.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableActions.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		viewTableActions.setBorder(BorderFactory.createEmptyBorder());
		viewTableActions.removeAll();
		viewTableActions.setLayout(new BorderLayout());
		viewTableActions.add(new JScrollPane(myEOTableActions), BorderLayout.CENTER);


		myEOTableSousActions.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableSousActions.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableSousActions.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		viewTableSousActions.setBorder(BorderFactory.createEmptyBorder());
		viewTableSousActions.removeAll();
		viewTableSousActions.setLayout(new BorderLayout());
		viewTableSousActions.add(new JScrollPane(myEOTableSousActions), BorderLayout.CENTER);

		myEOTableMasque.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableMasque.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableMasque.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		viewTableMasque.setBorder(BorderFactory.createEmptyBorder());
		viewTableMasque.removeAll();
		viewTableMasque.setLayout(new BorderLayout());
		viewTableMasque.add(new JScrollPane(myEOTableMasque), BorderLayout.CENTER);

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTableActions = new ZEOTable(myTableSorterActions);
		myTableModelActions.addTableModelListener(new ListenerActions());

		myEOTableSousActions = new ZEOTable(myTableSorterSousActions);
		myTableModelSousActions.addTableModelListener(new ListenerSousActions());

		myTableSorterSousActions.setTableHeader(myEOTableSousActions.getTableHeader());

		myEOTableMasque = new ZEOTable(myTableSorterMasque);
		myTableModelMasque.addTableModelListener(new ListenerMasque());

		myTableSorterMasque.setTableHeader(myEOTableMasque.getTableHeader());
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		Vector myCols = new Vector();

		// ACTIONS
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodActions, "tyacCode", "Code", 30);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererActions);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodActions, "tyacLibelle", "Libellé", 200);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererActions);
		myCols.add(col);

		myTableModelActions = new ZEOTableModel(eodActions, myCols);
		myTableSorterActions = new TableSorter(myTableModelActions);

		// SOUS ACTIONS
		myCols = new Vector();

		col = new ZEOTableModelColumn(eodSousActions, "tyacCode", "Code", 30);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererSousActions);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodSousActions, "tyacLibelle", "Libellé", 120);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererSousActions);
		myCols.add(col);

		myTableModelSousActions = new ZEOTableModel(eodSousActions, myCols);
		myTableSorterSousActions = new TableSorter(myTableModelSousActions);

		// MASQUE
		myCols = new Vector();

		col = new ZEOTableModelColumn(eodMasque, "typeAction.tyacCode", "Code", 5);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, "typeAction.tyacLibelle", "Libellé", 120);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, "typeAction.tyacNiveau", "Niv", 5);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, "typeAction.toTypeEtat.tyetLibelle", "Etat", 30);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		myTableModelMasque = new ZEOTableModel(eodMasque, myCols);
		myTableSorterMasque = new TableSorter(myTableModelMasque);

	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private class ListenerActions implements TableModelListener 	{
		public void tableChanged(TableModelEvent e) {
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private class ListenerSousActions implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {

			eodActions.setSelectedObject(null);
			eodActions.setSelectionIndexes(new NSArray());
			myEOTableActions.updateData();
			myEOTableActions.updateUI();

		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private class ListenerMasque implements TableModelListener 	{
		public void tableChanged(TableModelEvent e) {
		}
	}

	/**
	 *
	 *
	 */
	public void updateUI()	{
	}

	/**
	 *
	 *
	 */
	public void clean()	{
		eodActions.setObjectArray(new NSArray());
		myEOTableActions.updateData();
		eodMasque.setObjectArray(new NSArray());
		myEOTableMasque.updateData();
	}


	/**
	 *
	 *
	 */
	public void refreshActions()	{

		Integer niveauAction = Integer.valueOf(1);
		Integer niveauSousAction = Integer.valueOf(2);
		try {
			String paramAction = FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "NIVEAU_ACTION");
			if (paramAction != null) {
				niveauAction = new Integer(paramAction);
			}
		} catch (Exception e) {
			niveauAction = Integer.valueOf(1);
		}
		try {
			String paramSousAction = FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "NIVEAU_SOUS_ACTION");
			if (paramSousAction != null) {
				niveauSousAction = new Integer(paramSousAction);
			}
		} catch (Exception e) {
			niveauSousAction = Integer.valueOf(2);
		}

		NSMutableArray actions = new NSMutableArray(
				FinderTypeAction.findDestinationsBudgetGestion(ec, NSApp.getExerciceBudgetaire(), niveauAction, typeEtatValide));
		NSMutableArray sousActions = new NSMutableArray(
				FinderTypeAction.findDestinationsBudgetGestion(ec, NSApp.getExerciceBudgetaire(), niveauSousAction, typeEtatValide));

		NSArray actionsMasque = (NSArray) ((eodMasque.displayedObjects()).valueForKey("typeAction"));

		actions.removeObjectsInArray(actionsMasque);
		sousActions.removeObjectsInArray(actionsMasque);

		eodActions.setObjectArray(actions);
		myEOTableActions.updateData();
		myTableModelActions.fireTableDataChanged();

		eodSousActions.setObjectArray(sousActions);
		myEOTableSousActions.updateData();
		myTableModelSousActions.fireTableDataChanged();

	}

	/**
	 *
	 */
	public void setUpdateEnable(boolean enable)	{
		actionDuplicate.setEnabled(enable);
	}


	/**
	 *
	 *
	 */
	public void load()	{

		NSApp.setWaitCursor(mainPanel);

		eodMasque.setObjectArray(FinderBudgetMasqueGestion.findMasqueGestion(ec, NSApp.getExerciceBudgetaire()));
		myEOTableMasque.updateData();
		myTableModelMasque.fireTableDataChanged();

		refreshActions();

		NSApp.setDefaultCursor(mainPanel);
	}



	private class ActionRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE = new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE = new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE = new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE = new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED = new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED = new Color(255,255,255);

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			EOTypeAction typeAction = (EOTypeAction) eodActions.displayedObjects().objectAtIndex(row);
			String type = typeAction.tyacType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			} else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}

			if (isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	private class SousActionRenderer extends ZEOTableCellRenderer {

		public final Color COULEUR_FOND_DEPENSE = new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE = new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE = new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE = new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED = new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED = new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			EOTypeAction sousAction = (EOTypeAction)eodSousActions.displayedObjects().objectAtIndex(row);
			String type = sousAction.tyacType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			} else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}

			if (isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 * Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	private class MasqueRenderer extends ZEOTableCellRenderer {

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			EOBudgetMasqueGestion masqueGestion = (EOBudgetMasqueGestion) eodMasque.displayedObjects().objectAtIndex(row);
			String type = masqueGestion.typeAction().tyacType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			} else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}

			if (isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}
			return leComposant;
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionAdd extends AbstractAction {

		public ActionAdd() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_DROITE);
		}

		public void actionPerformed(ActionEvent e) {


			try {

				if (radioButtonAction.isSelected())	{
					for (int i=0;i<eodActions.selectedObjects().count();i++)	{

						EOTypeAction typeAction = (EOTypeAction)eodActions.selectedObjects().objectAtIndex(i);

						NSMutableDictionary parametres = new NSMutableDictionary();
						parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
						parametres.setObjectForKey((EOEnterpriseObject)typeAction, "EOTypeAction");

						ServerProxy.clientSideRequestAddMasqueGestion(ec, parametres);

					}

					eodActions.deleteSelection();
					myEOTableActions.updateData();

				}
				else	{
					for (int i=0;i<eodSousActions.selectedObjects().count();i++)	{

						EOTypeAction typeAction = (EOTypeAction)eodSousActions.selectedObjects().objectAtIndex(i);

						NSMutableDictionary parametres = new NSMutableDictionary();
						parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
						parametres.setObjectForKey((EOEnterpriseObject)typeAction, "EOTypeAction");

						ServerProxy.clientSideRequestAddMasqueGestion(ec, parametres);

					}

					eodSousActions.deleteSelection();
					myEOTableSousActions.updateData();

				}

				eodMasque.setObjectArray(FinderBudgetMasqueGestion.findMasqueGestion(ec, NSApp.getExerciceBudgetaire()));
				myEOTableMasque.updateData();

			}
			catch (Exception ex)	{
				ex.printStackTrace();
			}

		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionDelete extends AbstractAction {

		public ActionDelete() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_GAUCHE);
		}

		public void actionPerformed(ActionEvent e) {

			NSApp.setWaitCursor(mainPanel);

			try {

				NSMutableArray listeActions = new NSMutableArray(eodActions.displayedObjects());
				NSMutableArray listeSousActions = new NSMutableArray(eodSousActions.displayedObjects());

				for (int i=0;i<eodMasque.selectedObjects().count();i++)	{

					EOBudgetMasqueGestion masque = (EOBudgetMasqueGestion)eodMasque.selectedObjects().objectAtIndex(i);
					EOTypeAction typeAction = masque.typeAction();

					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
					parametres.setObjectForKey((EOEnterpriseObject)masque.typeAction(), "EOTypeAction");

					ServerProxy.clientSideRequestDelMasqueGestion(ec, parametres);
				}
				load();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
				MsgPanel.sharedInstance().runErrorDialog("ERREUR",NSApp.getErrorDialog(ex));
			}

			NSApp.setDefaultCursor(mainPanel);

		}
	}


	/**
	 *
	 * Recuperation des masques de l'annee n-1
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionDuplicate extends AbstractAction {

		public ActionDuplicate() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_PILOTAGE);
		}

		public void actionPerformed(ActionEvent e) {

			try {

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey("GESTION", "typeMasque");
				parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
				ServerProxy.clientSideRequestDupliquerMasque(ec, parametres);

				load();
			}
			catch (Exception ex)	{
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
			}

		}
	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	private class TypeActionListener implements ItemListener 	{

		public void itemStateChanged(ItemEvent e)	{

			if (e.getStateChange() == ItemEvent.SELECTED)	{

				if (radioButtonAction.isSelected())
					((CardLayout)swapViewActions.getLayout()).show(swapViewActions,"actions");
				else
					((CardLayout)swapViewActions.getLayout()).show(swapViewActions,"sousActions");
			}
		}

	}
}
