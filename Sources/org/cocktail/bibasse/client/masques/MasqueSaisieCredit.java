package org.cocktail.bibasse.client.masques;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.factory.FactoryBudgetMasqueSaisie;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueCredit;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueNature;
import org.cocktail.bibasse.client.finder.FinderTypeCredit;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueCredit;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class MasqueSaisieCredit {

	private static MasqueSaisieCredit sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;
	
	protected JPanel mainPanel, viewTableTypeCredit, viewTableMasque;	

	private EODisplayGroup eodTypeCredit, eodMasque;
	private ZEOTable myEOTableTypeCredit, myEOTableMasque;
	private ZEOTableModel myTableModelTypeCredit, myTableModelMasque;
	private TableSorter myTableSorterTypeCredit, myTableSorterMasque;
		
	private TypeCreditRenderer	rendererTypeCredit = new TypeCreditRenderer();
	private MasqueRenderer	rendererMasque = new MasqueRenderer();
	
	protected ActionAdd 			actionAdd = new ActionAdd();
	protected ActionDelete 			actionDelete = new ActionDelete();
	protected	ActionDuplicate	actionDuplicate = new ActionDuplicate();
		
	/**
	 * 
	 *
	 */
	public MasqueSaisieCredit(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		initView();
		initGUI();
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static MasqueSaisieCredit sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new MasqueSaisieCredit(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	private void initView()	{
		
		mainPanel = new JPanel(new BorderLayout());

		viewTableTypeCredit = new JPanel(new BorderLayout());
		viewTableMasque = new JPanel(new BorderLayout());

		viewTableTypeCredit.setBorder(BorderFactory.createEmptyBorder(2,5,2,0));		
		viewTableMasque.setBorder(BorderFactory.createEmptyBorder(2,0,2,2));		

        // CENTER
        ArrayList arrayList = new ArrayList();
		
		JButton btnAddAction = new JButton(actionAdd);
		btnAddAction.setPreferredSize(new Dimension(50,23));
		btnAddAction.setHorizontalAlignment(0);
		btnAddAction.setVerticalAlignment(0);
		btnAddAction.setHorizontalTextPosition(0);
		btnAddAction.setVerticalTextPosition(0);

		JButton btnDelAction = new JButton(actionDelete);
		btnDelAction.setPreferredSize(new Dimension(50,23));
		btnDelAction.setHorizontalAlignment(0);
		btnDelAction.setVerticalAlignment(0);
		btnDelAction.setHorizontalTextPosition(0);
		btnDelAction.setVerticalTextPosition(0);

		JButton btnDuplicate = new JButton(actionDuplicate);
		btnDuplicate.setToolTipText("Dupliquer le masque de l'annee " + (NSApp.getExerciceBudgetaire().exeExercice().intValue() - 1));
		btnDuplicate.setPreferredSize(new Dimension(50,45));
		btnDuplicate.setHorizontalAlignment(0);
		btnDuplicate.setVerticalAlignment(0);
		btnDuplicate.setHorizontalTextPosition(0);
		btnDuplicate.setVerticalTextPosition(0);

		arrayList.add(btnAddAction);
		arrayList.add(btnDelAction);
		arrayList.add(btnDuplicate);
        
		Component panelTemp = ZUiUtil.buildBoxColumn(arrayList);

		JPanel panelCenter = new JPanel(new FlowLayout());
		panelCenter.setBorder(BorderFactory.createEmptyBorder(60, 10,0,10));
		panelCenter.add(panelTemp, BorderLayout.NORTH);
		
		arrayList = new ArrayList();
				
		arrayList.add(viewTableTypeCredit);
		arrayList.add(panelCenter);
		arrayList.add(viewTableMasque);
		
		JPanel panelGestion = new JPanel(new BorderLayout());
		panelGestion.add(ZUiUtil.buildBoxLine(arrayList), BorderLayout.CENTER);
		
		if (!NSApp.hasFonction(ConstantesCocktail.ID_FCT_MASQUE))	{
			actionAdd.setEnabled(false);
			actionDelete.setEnabled(false);
		}

		mainPanel.setBorder(BorderFactory.createEmptyBorder(40,40,40,40));		
        mainPanel.add(panelGestion, BorderLayout.CENTER);

	}
	
	/**
	 * 
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}
		
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void initGUI() {
		
		eodTypeCredit = new EODisplayGroup();
		eodMasque = new EODisplayGroup();
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("tcdType", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("tcdCode", EOSortOrdering.CompareAscending));

		eodTypeCredit.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("typeCredit.tcdType", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareAscending));
		eodMasque.setSortOrderings(mySort);
		
		initTableModel();
		initTable();
		
		myEOTableTypeCredit.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableTypeCredit.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableTypeCredit.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableTypeCredit.setBorder(BorderFactory.createEmptyBorder());
		viewTableTypeCredit.removeAll();
		viewTableTypeCredit.setLayout(new BorderLayout());
		viewTableTypeCredit.add(new JScrollPane(myEOTableTypeCredit), BorderLayout.CENTER);

			
		myEOTableMasque.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableMasque.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableMasque.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableMasque.setBorder(BorderFactory.createEmptyBorder());
		viewTableMasque.removeAll();
		viewTableMasque.setLayout(new BorderLayout());
		viewTableMasque.add(new JScrollPane(myEOTableMasque), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{
				
		myEOTableTypeCredit = new ZEOTable(myTableSorterTypeCredit);
		myTableModelTypeCredit.addTableModelListener(new ListenerTypeCredit());
					
		myTableSorterTypeCredit.setTableHeader(myEOTableTypeCredit.getTableHeader());		
		
		myEOTableMasque = new ZEOTable(myTableSorterMasque);
		myTableModelMasque.addTableModelListener(new ListenerMasque());

		myTableSorterMasque.setTableHeader(myEOTableMasque.getTableHeader());		

	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
						
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodTypeCredit, "tcdCode", "Code", 20);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(rendererTypeCredit);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodTypeCredit, "tcdLibelle", "Libellé", 60);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererTypeCredit);
		myCols.add(col);

		myTableModelTypeCredit = new ZEOTableModel(eodTypeCredit, myCols);
		myTableSorterTypeCredit = new TableSorter(myTableModelTypeCredit);

		// MASQUE
		
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodMasque, "typeCredit.tcdCode", "Code", 5);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMasque, "typeCredit.tcdLibelle", "Libellé", 120);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		myTableModelMasque = new ZEOTableModel(eodMasque, myCols);
		myTableSorterMasque = new TableSorter(myTableModelMasque);
	
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ListenerTypeCredit implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {
			
		}
	}
			
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ListenerMasque implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {
			
		}
	}
	
	/**
	 * 
	 *
	 */
	public void clean()	{
		
		eodTypeCredit.setObjectArray(new NSArray());
		myEOTableTypeCredit.updateData();
		eodMasque.setObjectArray(new NSArray());
		myEOTableMasque.updateData();
	}
		
	
	/**
	 *
	 */
	public void setUpdateEnable(boolean enable)	{
		
		actionAdd.setEnabled(enable);
		actionDelete.setEnabled(enable);
		actionDuplicate.setEnabled(enable);

	}
	
	/**
	 * 
	 *
	 */
	public void load()	{

		NSApp.setWaitCursor(mainPanel);

		eodMasque.setObjectArray(FinderBudgetMasqueCredit.findMasqueCredit(ec, NSApp.getExerciceBudgetaire()));
		myEOTableMasque.updateData();
		myTableModelMasque.fireTableDataChanged();

		NSMutableArray typesCredit = new NSMutableArray(FinderTypeCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), null, null));
		
		NSArray typesCreditMasque = (NSArray)((eodMasque.displayedObjects()).valueForKey("typeCredit"));
		
		typesCredit.removeObjectsInArray(typesCreditMasque);

		eodTypeCredit.setObjectArray(typesCredit);		

		myEOTableTypeCredit.updateData();
		myTableModelTypeCredit.fireTableDataChanged();

		NSApp.setDefaultCursor(mainPanel);
	}
	
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class TypeCreditRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			String type = ((EOTypeCredit)eodTypeCredit.displayedObjects().objectAtIndex(row)).tcdType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			}	
			else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}
	
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class MasqueRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
						
			String type = ((EOBudgetMasqueCredit)eodMasque.displayedObjects().objectAtIndex(row)).typeCredit().tcdType();

			if ("DEPENSE".equals(type))	{
				leComposant.setBackground(COULEUR_FOND_DEPENSE);
				leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			}	
			else	{
				leComposant.setBackground(COULEUR_FOND_RECETTE);
				leComposant.setForeground(COULEUR_TEXTE_RECETTE);
			}
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}
			
			return leComposant;
		}
	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionAdd extends AbstractAction {

	    public ActionAdd() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_DROITE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	

        	try {
        		
        		FactoryBudgetMasqueSaisie myFactory = new FactoryBudgetMasqueSaisie();
        		
        		for (int i=0;i<eodTypeCredit.selectedObjects().count();i++)	{
        			
        			EOTypeCredit typeCredit = (EOTypeCredit)eodTypeCredit.selectedObjects().objectAtIndex(i);
        			
        			myFactory.creerMasqueSaisieCredit(
        					ec, 
        					NSApp.getExerciceBudgetaire(),
        					typeCredit, 
        					FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE));        			
        		}
        		
        		ec.saveChanges();
        		eodTypeCredit.deleteSelection();        			
        		myEOTableTypeCredit.updateData();
        		
        		eodMasque.setObjectArray(FinderBudgetMasqueCredit.findMasqueCredit(ec, NSApp.getExerciceBudgetaire()));
        		myEOTableMasque.updateData();
        		
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        	}        	
        }  
	} 
	
	/**
	 * 
	 * Recuperation des masques de l'annee n-1
	 * 
	 * @author cpinsard
	 *
	 */
	private final class ActionDuplicate extends AbstractAction {

	    public ActionDuplicate() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_PILOTAGE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	try {

        		NSMutableDictionary parametres = new NSMutableDictionary();
        		parametres.setObjectForKey("CREDIT", "typeMasque");
        		parametres.setObjectForKey((EOEnterpriseObject)NSApp.getExerciceBudgetaire(), "EOExercice");
        		ServerProxy.clientSideRequestDupliquerMasque(ec, parametres);

        		load();
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
        	}
        	
        }  
	} 
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionDelete extends AbstractAction {

	    public ActionDelete() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_GAUCHE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	

        	try {
        		        		
    			NSMutableArray listeTypesCredit= new NSMutableArray(eodTypeCredit.displayedObjects());

    			for (int i=0;i<eodMasque.selectedObjects().count();i++)	{
        			
    				EOBudgetMasqueCredit masque = (EOBudgetMasqueCredit)eodMasque.selectedObjects().objectAtIndex(i);
        			EOTypeCredit typeCredit = masque.typeCredit();
        			
            		// On teste si le type de credit n'est pas utilié dans le masque NATURE ==> Sinon ERREUR
            		if (FinderBudgetMasqueNature.findMasquesNatureForTypeCredit(ec, NSApp.getExerciceBudgetaire(), typeCredit).count() == 0)	{
            			listeTypesCredit.addObject(typeCredit);
                		ec.deleteObject(masque);            			
            		}
            		else	{
            			EODialogs.runErrorDialog("ERREUR","Erreur de suppression !\nLe type de crédit " + typeCredit.tcdCode()+
            					" est utilisé dans le masque du budget par nature. Veuillez modifier ce dernier avant cette suppression.");
            			ec.revert();
            			return;
            		}
        		}        		

        		ec.saveChanges();
    			
    			eodTypeCredit.setObjectArray(listeTypesCredit);
    			myEOTableTypeCredit.updateData();
    			
    			eodMasque.deleteSelection();
       			myEOTableMasque.updateData();       		    			

        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        	}
        	
        }  
	} 
	
	
}
