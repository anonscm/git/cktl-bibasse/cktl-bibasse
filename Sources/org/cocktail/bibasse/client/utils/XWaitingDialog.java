/**
WaitingFrame.java

*/
package org.cocktail.bibasse.client.utils;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.webobjects.eoapplication.client.EOClientResourceBundle;


/**
Classe destinee a afficher de facon simple des messages d'attente pour les utilisateurs.
Merci a Titou pour avoir trouve la bonne solution pour l'affichage.
*/
public class XWaitingDialog extends JDialog {
   
   JComponent myContentPane;
   
   String title, message1, message2;
   JLabel texte1, texte2, image;
   
   ImageIcon	icone;
   
   private JProgressBar myProgressBar;
   private int progressValue;

   /**
    Cree une nouvelle fenetre qui permet d'afficher un message d'attente. Bien entendu cette classe peut etre utilisee pour tous types de messages a afficher. Les parametres anIntro et aMessage peuvent etre nuls. 
    @param aTitle : titre de la fenetre.
    @param anIntro : message d'introduction. Ex : "Traitement en cours : "
    @param aMessage : message d'information. Ex : "Chargement des valeurs..."
    */
   public XWaitingDialog(String aTitle, String aMessage,String aMessage2, boolean progressBar) {
       
       super();//((ApplicationClient)EOApplication.sharedApplication()).superviseur().mainWindow, aTitle, false);
       setSize(400,150);
       progressValue=0;
               
       // Initialisations
       title = aTitle;
       message1 = aMessage;
       message2 = aMessage2;
       this.setDefaultCloseOperation(EXIT_ON_CLOSE);
       
       myProgressBar = new JProgressBar(0, 100);
       myProgressBar.setIndeterminate(false);
       myProgressBar.setStringPainted(true);
       myProgressBar.setBorder(BorderFactory.createEmptyBorder());
       
       // Recuperer les dimensions de l'ecran
       int x = (int)this.getGraphicsConfiguration().getBounds().getWidth();
       int y = (int)this.getGraphicsConfiguration().getBounds().getHeight();
       
       myContentPane = createUI(progressBar);
       myContentPane.setOpaque(true);
       
       this.setContentPane(myContentPane);
       this.setLocation((x/2)-((int)this.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)this.getContentPane().getMinimumSize().getHeight()/2)));
       // prevenir la fermeture de l'application en cas de souci
       this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
       
       //Display the window.
//       this.pack();
//       this.setVisible(true);
   }
      
   // Creation du panneau et des composants a afficher
   private JPanel createUI(boolean progressbar) {
       JPanel container=new JPanel(new BorderLayout());
       
       texte1 = new JLabel(message1);
       texte2 = new JLabel(message2);

       icone = (ImageIcon)new EOClientResourceBundle().getObject("sablier");
       image = new JLabel(icone);
       icone.setImageObserver(image);

       container.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
               
       JPanel panel = new JPanel();
       panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
       panel.setBorder(BorderFactory.createEmptyBorder(20,20,10,20));
       texte1.setAlignmentX(JComponent.CENTER_ALIGNMENT);
       texte2.setAlignmentX(JComponent.CENTER_ALIGNMENT);
       
       panel.add(Box.createVerticalStrut(5)); //extra space
       panel.add(texte1);
       panel.add(texte2);
        
       if (progressbar)
           panel.add(myProgressBar);
       
       panel.add(Box.createRigidArea(new Dimension(200, 40)));
       
       container.add(panel, BorderLayout.CENTER);
       container.add(image, BorderLayout.WEST);
       
       return container;
   }
   
   /**
    * 
    * @param yn
    */
   public void setProgressBarVisible(boolean yn)	{
      myProgressBar.setVisible(yn); 
   }
   
   /**
    Modification du titre de la fenetre. Prend en compte la modification immediatement.
    */
   public void setTitle(String aTitle) {
       title = aTitle;
       this.setTitle(title);
       this.paintAll(this.getGraphics());
   }
   
   public void setStringProgress(String texte)	{
       myProgressBar.setString(texte);
       //this.paintAll(this.getGraphics());
   }
   
   public void setMaxProgress(int value)	{
       myProgressBar.setMaximum(value);
       //this.paintAll(this.getGraphics());
   }
   
   public void setMinProgress(int value)	{
       myProgressBar.setMinimum(value);
       //this.paintAll(this.getGraphics());
   }
      
   public void startProgress() {
       progressValue=0;
   }
   
   public void setCurrentProgress(int value)	{
       progressValue=value;
       myProgressBar.setValue(value);
       myProgressBar.setString(value+"/"+myProgressBar.getMaximum());
   }
   
   public void setIndeterminateProgress(boolean yn){
       myProgressBar.setIndeterminate(yn);
       this.paintAll(this.getGraphics());
   }
   
   /**
    Modification du message d'information de la fenetre. Prend en compte la modification immediatement.
    */
   public void setMessages(String m1, String m2) {
       message1 = m1;
       message2 = m2;
       texte1.setText(message1);
       texte2.setText(message2);
       //this.paintAll(this.getGraphics());
   }
       
   /**
    Fermeture de la fenetre d'attente.
    */
   public void close() {
       this.setVisible(false);
       this.dispose();
   }
   
   
   
}
