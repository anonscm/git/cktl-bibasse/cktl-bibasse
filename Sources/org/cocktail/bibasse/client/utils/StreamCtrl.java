package org.cocktail.bibasse.client.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Cette classe fournit les methodes pour la gestion des flots
 * de donnees. Elle ne contient que des methodes statiques et
 * aucune instance de cette classe ne doit pas etre creee. 
 */
public class StreamCtrl {
  /**
   * La taille du tampont utilise pour le transfert de document.
   */
  public static final int BUFFER_SIZE = 1024;

  /**
   * <i>Il n'est pas necessaire de creer un objet de la classe
   * <code>StreamCtrl</code>, car toutes ses methodes sont statiques !</i>
   */
  public StreamCtrl() {
    // Ce constructeur est laisse juste pour pouvoir
    // ajouter un commentaire JavaDoc.
  }

  /**
   * Lit une chaine de caractere a partir d'un flux. Le le flux commence
   * par la taille de la chaine (un entier) suivi des caracteres de la
   * chaine.
   * 
   * @param dataIn Le flux a partir duquel la chaine est lue. 
   * @exception IOException 
   */
  public static String readStringFromStream(DataInputStream dataIn)
    throws IOException
  {
    byte[] bytes;
    int i = dataIn.readInt();
    if (i > 0) {
      bytes = new byte[i];
      dataIn.read(bytes);
      return new String(bytes);
    } else {
      return "";
    }
  }

  /**
   * Ecrit une chaine de caracteres dans un flux. On ecrit
   * la longeur de la chaine (un entier) suivi des ses caracteres.
   *   
   * @param aString La chaine a enregistrer dans un flux. 
   * @param dataOut Le flux.
   * @exception IOException 
   */
  public static void writeStringToStream(String aString, DataOutputStream dataOut)
    throws IOException
  {
    if ((aString == null) || (aString.length() == 0)) {
      dataOut.writeInt(0);
    } else {
      byte[] bytes = aString.getBytes();
      dataOut.writeInt(bytes.length);
      dataOut.write(bytes, 0, bytes.length);
    }
  }

  /**
   * Enregistre les donnees envoyees via le flux <code>content</code>
   * dans le fichier avec le chemin <code>filePath</code>. L'enregistrement
   * est termine lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee.
   * 
   * @throws IOException
   */
  public static void saveContentToFile(InputStream content,
                                       String filePath)
    throws IOException
  {
    saveContentToFile(content, filePath, -1);
  }

  /**
   * Enregistre les donnees envoyees via le flux <code>content</code>
   * dans le fichier avec le chemin <code>filePath</code>. L'enregistrement
   * est termine lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee ou lorsque la nombre total des octets
   * envoyes via le flux atteint <code>contentSize</code>.
   * 
   * @throws IOException
   */
  public static void saveContentToFile(InputStream content,
                                       String filePath,
                                       long contentSize)
    throws IOException
  {
    FileOutputStream out = null;
    if (filePath != null) out = new FileOutputStream(filePath);
    writeContentToStream(content, out, contentSize);
    if (out != null) {
      out.flush();
      forceClose(out);
    }
  }
  
  /**
   * Lit les donnees a partir de flux <code>content</code> et les
   * ecrit dans le flux <code>out</code>. L'ecriture est termine
   * lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee ou lorsque la nombre total des octets
   * envoyes via le flux <code>out</code> atteint <code>contentSize</code>.
   * 
   * @throws IOException
   */
  public static void writeContentToStream(InputStream content,
                                          OutputStream out,
                                          long contentSize)
    throws IOException
  {
    long bytesTotal;
    int bytesRead;
    byte bBuffer[] = new byte[BUFFER_SIZE];
    bytesTotal = 0;
    do {
      bytesRead = content.read(bBuffer, 0, BUFFER_SIZE);
      if (bytesRead > 0) {
        if (out != null) out.write(bBuffer, 0, bytesRead);
        bytesTotal += (long)bytesRead;
      }
      // Si on connait la taille des donnees, on peut s'arreter explicitement
      if ((contentSize >= 0) && (bytesTotal >= contentSize))
        break;
    } while(bytesRead > 0);
    if (out != null) out.flush();
  }
  
  /**
   * Lit les donnees a partir de flux <code>content</code> et les
   * ecrit dans le flux <code>out</code>. L'ecriture est termine
   * lorsque la lecture a partir du flux <code>content</code> ne
   * retourne plus aucune donnee.
   * 
   * @throws IOException
   */
  public static void writeContentToStream(InputStream content,
                                          OutputStream out)
    throws IOException
  {
    writeContentToStream(content, out, -1);
  }
  
  /**
   * Ferme le flux d'entree <code>stream</code> et ignore les erreurs
   * si elles se produissent.
   */
  public static void forceClose(InputStream stream) {
    if (stream != null)
      try { stream.close(); } catch(Throwable e) { }
  }

  /**
   * Ferme le flux de sortie <code>stream</code> et ignore les erreurs
   * si elles se produissent.
   */
  public static void forceClose(OutputStream stream) {
    if (stream != null)
      try { stream.close(); } catch(Throwable e) { }
  }

  /**
   * Ferme le socket de commnuication <code>socket</code> et ignore
   * les erreurs si elles se produissent.
   */
  public static void forceClose(Socket socket) {
    if (socket != null)
      try { socket.close(); } catch(Throwable e) { }
  }
  
  /**
   * Decompresse le contenu d'une archive <code>zipFilePath</code> dans
   * le repertoire <code>destDir</code>. Le contenue de l'archive est
   * decompressee en recreant l'arborescence des fichiers enregistree
   * dans l'archive.
   * 
   * @param zipFilePath Le chemin d'acces au fichier compresse. Le format
   *   de fichier doit etre compatible avec le format des fichiers Zip.
   * @param destDir Le repertoire ou le contenu de fichier sera decompresse.
   *   Ce repertoire doit exister.
   * 
   * @throws IOException
   */
  public static void unzipFile(String zipFilePath, String destDir)
    throws IOException
  {
    if (!FileCtrl.existsFile(destDir))
      throw new IOException("Le repertoire n'existe pas : "+destDir);
    ZipFile zFile = new ZipFile(zipFilePath);
    ZipEntry zEntry;
    String destFileName, destFileDir;
    Enumeration zEntries = zFile.entries();
//    System.out.println("File : "+zipFilePath);
//    System.out.println("Destination Directory : "+destDir);
    destDir = FileCtrl.normalizeDirName(destDir);
    while(zEntries.hasMoreElements()) {
      zEntry = (ZipEntry)zEntries.nextElement();
      destFileName = destDir + zEntry.getName();
      if (zEntry.isDirectory())
        destFileDir = destFileName;
      else
        destFileDir = FileCtrl.getParentDirectory(destFileName);
//      System.out.print("unzip : "+destFileName+" ...");
      if (!FileCtrl.existsFile(destFileDir)) {
        FileCtrl.makeDirs(destFileDir, destDir);
      }
      if (!zEntry.isDirectory())
        StreamCtrl.saveContentToFile(zFile.getInputStream(zEntry), destFileName, zEntry.getSize());
//      System.out.println("OK");
    }
  }
}
