//  ULRUtilities.java
package org.cocktail.bibasse.client.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Enumeration;

import javax.swing.BoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.EOMasterDetailAssociation;
import com.webobjects.eointerface.EOTableAssociation;
import com.webobjects.eointerface.swing.EOImageView;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;

public class CocktailUtilities 
{
private static final	Color COULEUR_FOND_ACTIF = Color.white;
private static final	Color COULEUR_FOND_INACTIF = new Color(222,222,222);
//	private static final	Color COULEUR_FOND_INACTIF = new Color(255,0,0);

private static final	Color COULEUR_TEXTE_ACTIF = Color.black;
private static final	Color COULEUR_TEXTE_INACTIF = Color.black;

private static final 	EOClientResourceBundle lesRessources = new EOClientResourceBundle(); 

/** Rendre une table (EOTable) non editable : a utiliser partout ou il y a des tables */
public static void setNonEditableTable(EOTable aTable)
{
JTable actualTable = aTable.table();

for(Enumeration liste = actualTable.getColumnModel().getColumns() ; liste.hasMoreElements() ; ) 
((TableColumn)liste.nextElement()).setCellEditor(null);
}

/** 
*	Affecte une image a un bouton. 
*	L'image est prise dans les Web Server Resources de l'application. 
*	
*	@param sender Controleur parent 
* @param nomImage Nom de l'image 
* @param text Text du bouton 
* @param leBouton Bouton a modifier 
*/ 
public static void affecterImageEtTextAuBouton(EOInterfaceController sender, String nomImage, String text, JButton leBouton, String message) 
{
if (nomImage != null)
{
ImageIcon icone = (ImageIcon)lesRessources.getObject(nomImage);
leBouton.setIcon(icone); 
}
else
leBouton.setIcon(null); 

leBouton.setText(text);
leBouton.setSelected(false);
leBouton.setFocusPainted(false);
leBouton.setToolTipText(message);
leBouton.setDefaultCapable(false);

//leBouton.setSize(new Dimension(icone.getIconWidth() + 3, icone.getIconHeight()+3));

// leBouton.setVerticalAlignment(SwingConstants.CENTER); 
//leBouton.setHorizontalTextPosition(SwingConstants.LEADING); 
//leBouton.setHorizontalAlignment(SwingConstants.TRAILING); 
}

public static void afficheTextField(EOView view, JTextField textfield)
{
	view.removeAll();
	view.add(textfield);
	view.validate();
} 

	/** Affecte du texte à un JTextField. Aligne le texte à gauche et ajoute un tooltip si tout le texte est trop long pour etre complètement affiché.. */
	public static void setTextTextField(JTextField textfield, String texte) 
	{
		textfield.setText(texte);
		textfield.moveCaretPosition(0);
		BoundedRangeModel tmpBoundedRangeModel = textfield.getHorizontalVisibility();       
		if (tmpBoundedRangeModel.getMaximum()>textfield.getWidth() ) {
			textfield.setToolTipText(texte);
		}
		else {
			textfield.setToolTipText(null);
		}
	} 

	/** */ 
	public static void setImageForImageView(String nomImage, EOImageView laVue)
	{
		laVue.setImageScaling(EOImageView.ScaleToFit);
		
		if (nomImage != null)
			laVue.setImage(((ImageIcon)lesRessources.getObject(nomImage)).getImage());
		else
			laVue.setImage(null);
	}

/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
public static void initTextField(JTextField leChamp, boolean clean, boolean actif)
{
if (actif)
{
leChamp.setForeground(COULEUR_TEXTE_ACTIF);
leChamp.setBackground(COULEUR_FOND_ACTIF);
}
else
{
leChamp.setForeground(COULEUR_TEXTE_INACTIF);
leChamp.setBackground(COULEUR_FOND_INACTIF);
}

if (clean)
leChamp.setText(null);

leChamp.setEditable(actif);
}

/** Informe les EOTableAssociation et les EOMasterDetailAssociation que le DisplayGroup peut avoir change
// Il arrive que les associations ne soient pas correctement mises a jour */
public static void informObservingAssociations(EODisplayGroup aTable)
{
	NSArray observingAssociations = aTable.observingAssociations();

	// Informer les EOTableAssociation et EOMasterDetailAssociation
	for(int i = 0 ; i < (observingAssociations.count()) ; i++)
	{
		if(observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOTableAssociation"))
			((EOTableAssociation)observingAssociations.objectAtIndex(i)).subjectChanged();

		if(observingAssociations.objectAtIndex(i).getClass().getName().equals("com.webobjects.eointerface.EOMasterDetailAssociation"))	
		{
			EOMasterDetailAssociation anAssociation = (EOMasterDetailAssociation)observingAssociations.objectAtIndex(i);
			anAssociation.subjectChanged();
		// Informer les observers de l'objet detail
			informObservingAssociations((EODisplayGroup)anAssociation.object());
		}
	}
}

/** Place une vue dans une autre. La swapView et la vue sont passes en parametres	*/
public static void putView(EOView maSwapView, EOView maView)
{
	maSwapView.getComponent(0).setVisible(false);
	maSwapView.removeAll();
	maSwapView.setLayout(new BorderLayout());
	maSwapView.add(maView,BorderLayout.CENTER);
	maView.setVisible(true);
	maSwapView.validate();
}

/** Refresh Display Group */
public static  void refreshDisplayGroup(EODisplayGroup eod, EOGenericRecord currentRecord)
{
	if (currentRecord != null)
	{	
		eod.setSelectedObject(null);
		CocktailUtilities.informObservingAssociations(eod);
		eod.setSelectedObject(currentRecord);
	}
	CocktailUtilities.informObservingAssociations(eod);
}

/** */
public static void refreshDisplayGroupAndTable(EODisplayGroup eod, EOGenericRecord currentRecord, EOTable table)
{
    refreshDisplayGroup(eod, currentRecord);
    if (eod.selectionIndexes().count()>0)
        table.table().changeSelection(((Number)eod.selectionIndexes().objectAtIndex(0)).intValue(), 0, false, false);
}


}
