/**
WaitingFrame.java

*/
package org.cocktail.bibasse.client.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;


/**
Classe destinee a afficher de facon simple des messages d'attente pour les utilisateurs.
Merci a Titou pour avoir trouve la bonne solution pour l'affichage.
*/
public class XLogs extends JFrame {
   
   private	ApplicationClient	myApp;
   private	JComponent 	myContentPane;

   private	JTextArea	logs;
   private 	JLabel		labelTailleLogs;
   private	JButton		btnMail,btnClean, btnRafraichir, btnFermer;
   
   private	ButtonGroup		types;
   private	JRadioButton	typeClient, typeServeur;
   
   String title;   

   /**
    * 
    * @param aTitle
    * @param clientErrLog
    * @param clientOutLog
    * @param serverErrLog
    * @param serverOutLog
    */
   public XLogs(String aTitle) {
       
       super(aTitle);

       myApp = (ApplicationClient)ApplicationClient.sharedApplication();
       
       title = aTitle;
       
       myContentPane = createUI();
       myContentPane.setOpaque(true);
       
       this.setContentPane(myContentPane);
       this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
       
       rafraichir();

       this.pack();
       centerWindow();

   }
      
/**
 * 
 * @return
 */
   private JPanel createUI() {
       
       JPanel container=new JPanel(new BorderLayout());
       
       JPanel panelRadio=new JPanel(new GridLayout(1,1,5,5));
       JPanel containerRadio=new JPanel(new BorderLayout());

       JPanel panelMessage=new JPanel(new GridLayout(1,1,5,5));       
       JPanel panelLabel=new JPanel(new GridLayout(1,1,5,5));

       JPanel containerCenter=new JPanel(new BorderLayout());

       JPanel panelBouton=new JPanel(new GridLayout(1,2,5,5));
       
       //Types de logs : Radio boutons Client et Serveur
       types = new ButtonGroup();
       typeClient = new JRadioButton(new ActionListenerClient());
       typeServeur = new JRadioButton(new ActionListenerServeur());

       types.add(typeClient);
       types.add(typeServeur);
       types.setSelected(typeClient.getModel(), true);

       panelRadio.add(typeClient);
       panelRadio.add(typeServeur);

       containerRadio.add(panelRadio, BorderLayout.WEST);
       
       // Text Area
       logs = new JTextArea();
       logs.setBackground(new Color(220,220,220));
       panelMessage.add(new JScrollPane(logs));

       // Label pour la taille des logs
       labelTailleLogs = new JLabel("");
       labelTailleLogs.setForeground(new Color(0,0,255));
       panelLabel.add(labelTailleLogs);

       containerCenter.add(panelMessage,BorderLayout.CENTER);
       containerCenter.add(panelLabel,BorderLayout.SOUTH);

       // Boutons
       btnMail = new JButton(new ActionListenerMail());
       btnClean = new JButton(new ActionListenerClean());
       btnRafraichir = new JButton(new ActionListenerRafraichir());
       btnFermer = new JButton(new ActionListenerFermer());
       
       panelBouton.add(btnRafraichir);
       panelBouton.add(btnClean);
       panelBouton.add(btnMail);
       panelBouton.add(btnFermer);

       // Panel principal
       container.add(containerRadio, BorderLayout.NORTH);
       container.add(containerCenter, BorderLayout.CENTER);
       container.add(panelBouton, BorderLayout.SOUTH);
       
       container.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
       container.setPreferredSize(new Dimension(600,400));
       
       return container;
   }
   
   public final void centerWindow() {
    int screenWidth = (int)this.getGraphicsConfiguration().getBounds().getWidth();
    int screenHeight = (int)this.getGraphicsConfiguration().getBounds().getHeight();
    this.setLocation((screenWidth/2)-((int)this.getSize().getWidth()/2), ((screenHeight/2)-((int)this.getSize().getHeight()/2)));
      } 
/**
 * 
 */
   public void setTitle(String aTitle) {
       title = aTitle;
       this.setTitle(title);
       this.paintAll(this.getGraphics());
   }
   
   /**
    * 
    * @author cpinsard
    *
    * To change the template for this generated type comment go to
    * Window - Preferences - Java - Code Style - Code Templates
    */
      private final class ActionListenerClient extends AbstractAction	{
          public ActionListenerClient() {
              super("Messages Client");
              putValue(AbstractAction.SHORT_DESCRIPTION , "Afficher les logs client");
          }
          
          public void actionPerformed(ActionEvent e)	{
              rafraichir();
          }
      }
      
      /**
       * 
       * @author cpinsard
       *
       * To change the template for this generated type comment go to
       * Window - Preferences - Java - Code Style - Code Templates
       */
         private final class ActionListenerServeur extends AbstractAction	{
             public ActionListenerServeur() {
                 super("Messages Serveur");
                 putValue(AbstractAction.SHORT_DESCRIPTION , "Afficher les logs serveur");
             }
             
             public void actionPerformed(ActionEvent e)	{
                 rafraichir();
             }
         }
         
/**
 * 
 * @author cpinsard
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
   private final class ActionListenerMail extends AbstractAction	{
       public ActionListenerMail() {
           super("Mail");
           putValue(AbstractAction.SMALL_ICON, (ImageIcon)new EOClientResourceBundle().getObject("enveloppe"));
           putValue(AbstractAction.SHORT_DESCRIPTION , "Envoyer le log Client ou Serveur");
       }
       
       public void actionPerformed(ActionEvent e)	{
           mail();
       }
   }
 
/**
 * 
 * @author cpinsard
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
   public class ActionListenerClean extends AbstractAction	{
       public ActionListenerClean() {
           super("Nettoyer");
           putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_DELETE);
           putValue(AbstractAction.SHORT_DESCRIPTION , "Nettoyer");
       }
       public void actionPerformed(ActionEvent e)	{
           clean();
       }
   }

/**
 * 
 * @author cpinsard
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
   public class ActionListenerRafraichir extends AbstractAction	{
       public ActionListenerRafraichir() {
           super("Rafraichir");
           putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_UPDATE);
           putValue(AbstractAction.SHORT_DESCRIPTION , "Rafraichir");
       }
       public void actionPerformed(ActionEvent e)	{
           rafraichir();
       }
   }
   
/**
 * 
 * @author cpinsard
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
   public class ActionListenerFermer extends AbstractAction	{
       public ActionListenerFermer() {
           super("Fermer");
           putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
           putValue(AbstractAction.SHORT_DESCRIPTION , "Fermer");
       }
       public void actionPerformed(ActionEvent e)	{
           hide();
       }
   }
   
   /**
    * 
    *
    */
   public void mail()	{
   	if (typeClient.getModel() == types.getSelection())
   		((ApplicationClient)ApplicationClient.sharedApplication()).sendLog("CLIENT");       
   	else
   		((ApplicationClient)ApplicationClient.sharedApplication()).sendLog("SERVEUR");       
   }

   /**
    * 
    *
    */
   public void clean()	{
   	if (typeClient.getModel() == types.getSelection())
   		((ApplicationClient)ApplicationClient.sharedApplication()).cleanLogs("CLIENT");       
   	else
   		((ApplicationClient)ApplicationClient.sharedApplication()).cleanLogs("SERVER");      

	labelTailleLogs.setText("TAILLE DES LOGS : OUT (0) , ERREUR (0)");

   	logs.setText("");
   }
   
   public void rafraichir()	{
   	String message = "";
   	   	
   	if (typeClient.getModel() == types.getSelection())	{
   		message = ">>>> OUPUT LOGS :\n";
   		message = message.concat(myApp.outLogs());       
   		message = message.concat("\n\n>>>> ERROR LOGS :\n");
   		message = message.concat(myApp.errLogs());   
   		labelTailleLogs.setText("TAILLE DES LOGS : OUT ("+String.valueOf(myApp.outLogs().length()) 
						+ " octets) , ERREUR ("+myApp.errLogs().length()+" octets)");
   	}
   	else	{
   		EOEditingContext ec = myApp.editingContext();
	    String outLog = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestOutLog", new Class[] {}, new Object[] {}, false);
	    String errLog = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestErrLog", new Class[] {}, new Object[] {}, false);
   		message = message.concat(">>>> OUTPUT LOGS :\n");
   		message = message.concat(outLog);       
   		message = message.concat("\n\n>>>> ERROR LOGS :\n");
   		message = message.concat(errLog);          		

   		labelTailleLogs.setText("TAILLE DES LOGS : OUT ("+String.valueOf(outLog.length()) 
				+ " octets) , ERREUR ("+errLog.length()+" octets)");
   	}

   	logs.setText(message);
   }


}
