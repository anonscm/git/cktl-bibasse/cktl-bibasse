/* Logs.java created by cpinsard on Sun 21-Sep-2003 */
package org.cocktail.bibasse.client.utils;

import com.webobjects.eocontrol.EOCustomObject;
import com.webobjects.foundation.NSTimestamp;

public class Logs extends EOCustomObject {

	public static void trace(String message) {
		System.out.println(message);
	}

	public static void trace(String message, int decalage, boolean addDate) {
		String chaineDecalage = "";
		for (int i=0;i<decalage;i++)
			chaineDecalage = chaineDecalage+"\t";

		if (addDate)
			System.out.println(chaineDecalage + message + " (" + DateCtrl.dateToString(new NSTimestamp(),"%H:%M:%S:%F") + ")");
		else
			System.out.println(chaineDecalage + message);
	}



}
