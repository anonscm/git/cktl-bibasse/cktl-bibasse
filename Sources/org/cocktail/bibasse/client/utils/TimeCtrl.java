package org.cocktail.bibasse.client.utils;

import com.webobjects.foundation.NSArray;

public class TimeCtrl 
{

// GET MINUTES : Retourne le nombre de minutes correspondant à la chaine string au format %H:%M (l'inverse de stringFor: )
	public static int getMinutes(String chaine)
	{
		NSArray			str =  NSArray.componentsSeparatedByString(chaine,":");
		int				nombre=0;

		if ( ("00:00".equals(chaine)) || ("".equals(chaine)) || (" ".equals(chaine)) || ("..:..".equals(chaine)) ) return 0;

		if (chaine.length() == 0) return 0;
	
		if (str.count() == 1)
			nombre = ((Number)str.objectAtIndex(0)).intValue() * 60;
		else
		{
			if ((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) < 0)			
				nombre = ( -((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) * 60) + ((((Number)new Integer((String)str.objectAtIndex(1))).intValue())) );
			else
				nombre = ( ((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) * 60) + ((((Number)new Integer((String)str.objectAtIndex(1))).intValue())) );			
		}		

		if ((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) < 0) nombre = -nombre;	// on a passe une valeur negative

		return nombre;
	}

// STRING FOR MINUTES
// Formatte le nombre de minutes en une chaine au format %H:%M (l'inverse de numberOfMinutesFor: )
	public static String stringForMinutes(int numberOfMinutes)
	{
		String	formattedString;
		int		hours, minutes;

		if (numberOfMinutes == 0) return "00:00";

		if (numberOfMinutes < 0) numberOfMinutes = -numberOfMinutes;

		hours = numberOfMinutes / 60;
		minutes = numberOfMinutes % 60;

		if (hours < 10)
			formattedString = "0" + hours;
		else	
			formattedString = "" + hours;

		if (minutes < 10)
			formattedString  = formattedString + ":0" + minutes;
		else 
			formattedString  = formattedString + ":" + minutes;

		return formattedString;
	}

}
