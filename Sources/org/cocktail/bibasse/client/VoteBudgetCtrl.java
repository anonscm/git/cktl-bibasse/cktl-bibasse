package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.editions.EditionsCtrl;
import org.cocktail.bibasse.client.factory.FactoryBudgetEvenements;
import org.cocktail.bibasse.client.finder.FinderTypeEvenement;
import org.cocktail.bibasse.client.metier.EOBudgetEvenements;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation.ValidationException;

public class VoteBudgetCtrl {

	private static VoteBudgetCtrl sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	private JPanel mainPanel;
	private	JDialog mainWindow;

    private ActionBudgetVote	actionBudgetVote = new ActionBudgetVote();
    private ActionBudgetNonVote	actionBudgetNonVote = new ActionBudgetNonVote();

    private ActionBudgetPrintCtrl	actionBudgetPrintCtrl = new ActionBudgetPrintCtrl();

    private ActionCancel	actionCancel = new ActionCancel();

    private JButton buttonBudgetVote, buttonBudgetNonVote;

    private JTextField messageBudgetVote, messageBudgetNonVote, titre;

    private EOBudgetSaisie currentBudgetSaisie;

	/**
	 *
	 *
	 */
	public VoteBudgetCtrl(EOEditingContext editingContext)	{

		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		initView();
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static VoteBudgetCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new VoteBudgetCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 *
	 *
	 */
	private void initTextFields()	{

		messageBudgetVote = new JTextField();
		messageBudgetVote.setColumns(35);

		messageBudgetNonVote = new JTextField();
		messageBudgetNonVote.setColumns(35);

        titre = new JTextField("");
        titre.setBackground(new Color(236,234,149));
        titre.setForeground(new Color(0,0,0));
        titre.setFont(Configuration.instance().informationTitreMenuFont(ec));
        titre.setHorizontalAlignment(JTextField.CENTER);
        titre.setEditable(false);
        titre.setFocusable(false);

	}


	/**
	 *
	 *
	 */
	private void initButtons()	{

		buttonBudgetVote = new JButton(actionBudgetVote);
		buttonBudgetVote.setPreferredSize(new Dimension(200, 25));
		buttonBudgetVote.setToolTipText("Budget VOTE");

		buttonBudgetNonVote = new JButton(actionBudgetNonVote);
		buttonBudgetNonVote.setPreferredSize(new Dimension(200, 25));
		buttonBudgetNonVote.setToolTipText("Budget NON VOTE");

	}


	/**
	 *
	 *
	 */
	private void initView()	{

    	mainWindow = new JDialog(NSApp.superviseur().mainFrame(), "Vote du budget", true);
		mainWindow.setTitle("BIBASSE - VOTE du BUDGET");

    	mainPanel = new JPanel(new BorderLayout());
    	mainPanel.setPreferredSize(new Dimension(650, 300));
    	mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

    	initButtons();

    	initTextFields();

    	mainPanel.add(buildNorthPanel(), BorderLayout.NORTH);

    	mainPanel.add(buildCenterPanel(), BorderLayout.CENTER);

    	mainPanel.add(buildSouthPanel(), BorderLayout.SOUTH);

    	mainWindow.setContentPane(mainPanel);
    	mainWindow.pack();

	}

	/**
	 *
	 * @return
	 */
	private JPanel buildPanelBudgetVote()	{

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,5,15,5));

		ArrayList arrayList = new ArrayList();

		JLabel label1 = new JLabel("* VOTE VALIDE : ");
		label1.setForeground(ConstantesCocktail.BG_COLOR_BLUE);

		JLabel label2 = new JLabel("VALIDATION DU BUDGET");
		label2.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JPanel panelLabel1= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{label1},
				BorderLayout.WEST);
		panelLabel1.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelLabel2= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{label2},
				BorderLayout.WEST);
		panelLabel2.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelMessage= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{new JLabel("Commentaires : "), messageBudgetVote},
				BorderLayout.WEST);
		panelMessage.setBorder(BorderFactory.createEmptyBorder(10,2,5,2));

		arrayList.add(panelLabel1);
		//arrayList.add(panelLabel2);
		arrayList.add(panelMessage);

		JPanel panelBudgetVote= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{buttonBudgetVote},
				BorderLayout.EAST);
		panelBudgetVote.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelBudgetVote, BorderLayout.EAST);
		panel.add(new JSeparator(), BorderLayout.SOUTH);
		panel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.CENTER);

		return panel;

	}

	/**
	 *
	 * @return
	 */
	private JPanel buildPanelBudgetNonVote()	{

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

		JPanel panelBudgetVote= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{buttonBudgetNonVote},
				BorderLayout.EAST);
		panelBudgetVote.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		ArrayList arrayList = new ArrayList();

		JLabel label1 = new JLabel("* VOTE REFUSE : ");
		label1.setForeground(ConstantesCocktail.BG_COLOR_RED);

		JLabel label2 = new JLabel("REFUS DU BUDGET");
		label2.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JPanel panelLabel1= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{label1},
				BorderLayout.WEST);
		panelLabel1.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelLabel2= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{label2},
				BorderLayout.WEST);
		panelLabel2.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelMessage= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{new JLabel("Commentaires : "), messageBudgetNonVote},
				BorderLayout.WEST);
		panelMessage.setBorder(BorderFactory.createEmptyBorder(10,2,5,2));

		arrayList.add(panelLabel1);
//		arrayList.add(panelLabel2);
		arrayList.add(panelMessage);


		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelBudgetVote, BorderLayout.EAST);
		panel.add(new JSeparator(), BorderLayout.SOUTH);
		panel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.CENTER);

		return panel;

	}


	private JPanel buildNorthPanel()	{

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(titre, BorderLayout.CENTER);


        return panel;
	}


	/**
	 *
	 * @return
	 */
	private JPanel buildCenterPanel()	{

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder());

		ArrayList arrayListPanels = new ArrayList();

		arrayListPanels.add(buildPanelBudgetVote());
		arrayListPanels.add(buildPanelBudgetNonVote());

		panel.add(ZUiUtil.buildBoxColumn(arrayListPanels), BorderLayout.NORTH);
		panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return panel;
	}


	/**
	 *
	 * @return
	 */
	private JPanel buildSouthPanel()	{

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		JPanel panelEastButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));
		panelEastButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		arrayList = new ArrayList();
		arrayList.add(actionBudgetPrintCtrl);
		JPanel panelWestButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 150, 23));
		panelWestButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		panel.add(panelEastButtons, BorderLayout.EAST);
		panel.add(panelWestButtons, BorderLayout.WEST);

		return panel;

	}


	/**
	 *
	 *
	 */
	public void open(EOBudgetSaisie budgetSaisie)	{

		currentBudgetSaisie = budgetSaisie;

		messageBudgetVote.setText("");
		messageBudgetNonVote.setText("");

		titre.setText("VOTE du BUDGET " + budgetSaisie.typeSaisie().tysaLibelle() + " " + budgetSaisie.exercice().exeExercice());

		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();

	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	private  final class ActionBudgetVote extends AbstractAction {

		public ActionBudgetVote() {
			super("VOTE DU BUDGET");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous le VOTE du Budget " + currentBudgetSaisie.typeSaisie().tysaLibelle()
					+ " " + NSApp.getExerciceBudgetaire().exeExercice()+ " ?",
					"OUI", "NON"))	{

				NSApp.setWaitCursor(mainPanel);

				try {

					if (currentBudgetSaisie.canVote(ec))	{
						NSMutableDictionary parametres = new NSMutableDictionary();

						parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");

						ServerProxy.clientSideRequestVoterBudget(ec, parametres);

						ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentBudgetSaisie)));

						currentBudgetSaisie.addObjectToBothSidesOfRelationshipWithKey(NSApp.getUtilisateur(), EOBudgetSaisie.UTILISATEUR_VALIDATION_KEY);

						// Creation d'un evenement
						// FLA le 22.10.2013 (Geode 5100) : erreur non reproductible systematiquement d'ou problème a identifier la cause.
						// Pour le moment on suspecte le code de génération de l'evenement et celui-ci n'etant pas "utile" au reste de l'appli on le desactive.
						/*
						FactoryBudgetEvenements myFactoryEvenements = new FactoryBudgetEvenements();
						EOBudgetEvenements newEvenement = myFactoryEvenements.creerBudgetEvenement(ec, currentBudgetSaisie,
								NSApp.getUtilisateur(), FinderTypeEvenement.findTypeEvenement(ec, "VOTE"));

						newEvenement.setBeveInfos("Budget " + currentBudgetSaisie.typeSaisie().tysaLibelle() +
								" VOTE");

						if (!StringCtrl.chaineVide(messageBudgetNonVote.getText())) {
							newEvenement.setBeveInfos2(messageBudgetNonVote.getText());
						}
						*/

						ec.saveChanges();

						EODialogs.runInformationDialog("BUDGET VOTE","Le budget " +
								currentBudgetSaisie.typeSaisie().tysaLibelle() + " " + NSApp.getExerciceBudgetaire().exeExercice() + " est maintenant VOTE !");

						mainWindow.dispose();

						NSApp.superviseur().refreshMenu();
					}

				} catch (ValidationException ex)	{
					ec.revert();
					EODialogs.runErrorDialog("ERREUR", "Erreur lors du VOTE du budget !\n\n" + NSApp.getErrorDialog(ex));
				} catch (Exception ex)	{
					ec.revert();
					EODialogs.runErrorDialog("ERREUR", "Erreur lors du VOTE du budget !\n\n" + NSApp.getErrorDialog(ex));
				}

				NSApp.setDefaultCursor(mainPanel);

			}

		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private  final class ActionBudgetNonVote extends AbstractAction {

		public ActionBudgetNonVote() {
			super("   VOTE REFUSE  ");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
		}

		public void actionPerformed(ActionEvent e) {

			NSApp.setWaitCursor(mainPanel);

			try {

				if (currentBudgetSaisie.canVote(ec))	{

				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");

				ServerProxy.clientSideRequestRefuserBudget(ec, parametres);

				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentBudgetSaisie)));

				currentBudgetSaisie.addObjectToBothSidesOfRelationshipWithKey(NSApp.getUtilisateur(), EOBudgetSaisie.UTILISATEUR_VALIDATION_KEY);

				// Creation d'un evenement
				// FLA le 22.10.2013 (Geode 5100) : erreur non reproductible systematiquement d'ou problème a identifier la cause.
				// Pour le moment on suspecte le code de génération de l'evenement et celui-ci n'etant pas "utile" au reste de l'appli on le desactive.
				/*
				FactoryBudgetEvenements myFactoryEvenements = new FactoryBudgetEvenements();
				EOBudgetEvenements newEvenement = myFactoryEvenements.creerBudgetEvenement(ec, currentBudgetSaisie,
						NSApp.getUtilisateur(), FinderTypeEvenement.findTypeEvenement(ec, "VOTE"));

				newEvenement.setBeveInfos("Vote du budget " + currentBudgetSaisie.typeSaisie().tysaLibelle() +
						" REFUSE");

				if (!StringCtrl.chaineVide(messageBudgetNonVote.getText()))
					newEvenement.setBeveInfos2(messageBudgetNonVote.getText());
				*/

				ec.saveChanges();

				EODialogs.runInformationDialog("BUDGET NON VOTE","Le refus du budget " +
						currentBudgetSaisie.typeSaisie().tysaLibelle() + " " + NSApp.getExerciceBudgetaire().exeExercice()
						+ " a bien été enregistré !");

				mainWindow.dispose();

				NSApp.superviseur().refreshMenu();
				}

			}
			catch (ValidationException ex)	{
				ec.revert();
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", "(Echec de la procédure de refus du budget (ValidationException) !\n\n" + ex.getMessage());
			}
			catch (Exception ex)	{
				ec.revert();
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", "Erreur lors du VOTE du budget (Exception) !\n\n" + ex.getMessage());
			}

			NSApp.setDefaultCursor(mainPanel);

		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private  final class ActionBudgetPrintCtrl extends AbstractAction {

		public ActionBudgetPrintCtrl() {
			super(" contrôle Saisie ");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}

		public void actionPerformed(ActionEvent e) {

			EditionsCtrl.sharedInstance(ec).printCtrlSaisie(currentBudgetSaisie);

		}

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private  final class ActionCancel extends AbstractAction {

		public ActionCancel() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
		}

		public void actionPerformed(ActionEvent e) {

        	mainWindow.dispose();

		}
	}

}
