 package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.factory.FactoryBibasse;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieNature;
import org.cocktail.bibasse.client.finder.FinderOrgan;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.process.budget.ProcessBudgetFactory;
import org.cocktail.bibasse.client.saisie.OrganPopupCtrl;
import org.cocktail.bibasse.client.saisie.SaisieBudget;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionRecettes;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureRecettes;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class PilotageCtrl {

	private static PilotageCtrl sharedInstance;
	private 	EOEditingContext 	ec;
	private 	ApplicationClient 	NSApp;

	protected	JFrame 		window;
	protected 	JPanel 		viewTable;

	private		JButton		buttonDelOrgan;
	private 	JComboBox 	popupEtab, popupUb, popupCr, popupEtat;
	private		JTextField 	informations;

	private 	EODisplayGroup 	eodCr;
	private 	ZEOTable 		myEOTable;
	private 	ZEOTableModel 	myTableModel;
	private 	TableSorter 	myTableSorter;

	protected 	ListenerFiltreEtab 	myListenerEtab = new ListenerFiltreEtab();
	protected 	ListenerFiltreUb 	myListenerUb = new ListenerFiltreUb();
	protected 	ListenerFiltreCr 	myListenerCr = new ListenerFiltreCr();
	protected 	ListenerFiltreEtat 	myListenerEtat = new ListenerFiltreEtat();

	private 	PilotageRenderer	monRendererColor = new PilotageRenderer();

	// Actions
	protected 	ActionClose 	actionClose = new ActionClose();
	protected 	ActionInit 		actionInit = new ActionInit();
	protected 	ActionValidate 	actionValidate = new ActionValidate();
	protected 	ActionControle 	actionControle = new ActionControle();
	protected 	ActionCloture 	actionCloture = new ActionCloture();
	protected 	ActionDelOrgan 	actionDelOrgan = new ActionDelOrgan();

	protected	NSArray 	budgetsGestion  = new NSArray();
	protected	NSArray 	budgetsNature  = new NSArray();

	private EOBudgetSaisie 	currentBudgetSaisie;
	public EOOrgan 		currentOrgan;
	private EOOrgan 		filterOrgan;

	/**
	 *
	 *
	 */
	public PilotageCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		gui_init();
		gui_initView();
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static PilotageCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new PilotageCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 * Panel de boutons a droite de l'ecran.
	 * Modification d'une ligne + Suppression d'un CR.
	 *
	 * @return
	 */
	private JPanel gui_buildNorthPanel()	{

		JPanel northPanel = new JPanel(new BorderLayout());

		popupEtab = new JComboBox();
		popupUb = new JComboBox();
		popupCr = new JComboBox();
		popupEtat = new JComboBox();

		popupEtab.setBackground(new Color(220,220,220));
		popupEtab.setForeground(new Color(0,0,0));

		popupUb.setBackground(new Color(220,220,220));
		popupUb.setForeground(new Color(0,0,0));

		popupCr.setBackground(new Color(220,220,220));
		popupCr.setForeground(new Color(0,0,0));

		popupEtat.setBackground(new Color(220,220,220));
		popupEtat.setForeground(new Color(0,0,0));

		fillPopupEtabs();
		fillPopupEtat();

		popupEtab.setSelectedIndex(0);
		etabHasChanged();

		JPanel panelPopup = new JPanel(new FlowLayout());

		JPanel panelEtab = new JPanel(new BorderLayout());
		panelEtab.setPreferredSize(new Dimension(75,21));
		panelEtab.add(popupEtab,BorderLayout.CENTER);

		JPanel panelUb = new JPanel(new BorderLayout());
		panelUb.setPreferredSize(new Dimension(75,21));
		panelUb.add(popupUb,BorderLayout.CENTER);

		JPanel panelCr = new JPanel(new BorderLayout());
		panelCr.setPreferredSize(new Dimension(110,21));
		panelCr.add(popupCr,BorderLayout.CENTER);

		JPanel panelEtat = new JPanel(new BorderLayout());
		panelEtat.setPreferredSize(new Dimension(110,21));
		panelEtat.add(popupEtat,BorderLayout.CENTER);

		panelPopup.add(panelEtab);
		panelPopup.add(panelUb);
		if (NSApp.isSaisieCr())
			panelPopup.add(panelCr);
		panelPopup.add(panelEtat);

		popupEtab.addActionListener(myListenerEtab);

		northPanel.add(panelPopup, BorderLayout.CENTER);

		popupEtat.addActionListener(myListenerEtat);

		return northPanel;
	}

	/**
	 *
	 * Panel de boutons a droite de l'ecran.
	 * Modification d'une ligne + Suppression d'un CR.
	 *
	 * @return
	 */
	private JPanel gui_buildCenterPanel()	{

		JPanel centerPanel = new JPanel(new BorderLayout());
		JPanel eastPanel = new JPanel(new BorderLayout());

		ArrayList arrayList = new ArrayList();

		arrayList.add(buttonDelOrgan);

		eastPanel.setBorder(BorderFactory.createEmptyBorder(2,3,0,1));
		eastPanel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		eastPanel.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);

		centerPanel.add(viewTable, BorderLayout.CENTER);
		centerPanel.add(eastPanel, BorderLayout.EAST);
		centerPanel.setBorder(BorderFactory.createEmptyBorder(1,4,1,4));

		return centerPanel;
	}


	/**
	 *
	 * Panel de boutons a droite de l'ecran.
	 * Modification d'une ligne + Suppression d'un CR.
	 *
	 * @return
	 */
	private JPanel gui_buildSouthPanel()	{

		JPanel southPanel = new JPanel(new BorderLayout());

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelClose = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 21));

		arrayList = new ArrayList();
		arrayList.add(actionInit);
		arrayList.add(actionValidate);
		arrayList.add(actionControle);
		arrayList.add(actionCloture);

		JPanel panelClotureBudget = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 21));

		JPanel panelInfos = new JPanel(new BorderLayout());
		panelInfos.add(new JSeparator(), BorderLayout.NORTH);
		panelInfos.add(informations, BorderLayout.CENTER);
		panelInfos.add(new JSeparator(), BorderLayout.SOUTH);

		southPanel.add(panelInfos, BorderLayout.NORTH);
		southPanel.add(panelClotureBudget, BorderLayout.WEST);
		southPanel.add(panelClose, BorderLayout.EAST);

		return southPanel;
	}

	/**
	 *
	 *
	 */
	private void gui_init_textFields()	{
		informations = new JTextField("");
		informations.setFont(Configuration.instance().informationLabelSmallFont(ec));
		informations.setEditable(false);
		informations.setBorder(BorderFactory.createEmptyBorder(2,5,4,5));
		informations.setBackground(new Color(100, 100, 100));
		informations.setForeground(new Color(117,255,232));
		informations.setHorizontalAlignment(JTextField.LEFT);
	}

	/**
	 *
	 *
	 */
	private void gui_init_buttons()	{
		buttonDelOrgan = new JButton(actionDelOrgan);
		buttonDelOrgan.setPreferredSize(new Dimension(22,22));
	}

	/**
	 *
	 *
	 */
	private void gui_initView()	{
        window = new JFrame();
        window.setTitle("Pilotage Budget " + NSApp.getExerciceBudgetaire().exeExercice());
        window.setSize(650, 720);

        gui_init_buttons();
        gui_init_textFields();

		JPanel mainView = new JPanel(new BorderLayout());
//        if (NSApp.isSaisieCr())
        	mainView.add(gui_buildNorthPanel(), BorderLayout.NORTH);
		mainView.add(gui_buildCenterPanel(), BorderLayout.CENTER);
		mainView.add(gui_buildSouthPanel(), BorderLayout.SOUTH);

		window.setContentPane(mainView);
	}

	/**
	 *
	 *
	 */
	public void fillPopupEtat()	{
		popupEtat.addItem("****");
		popupEtat.addItem("NON INITIALISE");
		popupEtat.addItem(EOTypeEtat.ETAT_EN_COURS);
		popupEtat.addItem(EOTypeEtat.ETAT_VALIDE);
		popupEtat.addItem(EOTypeEtat.ETAT_CONTROLE);
		popupEtat.addItem(EOTypeEtat.ETAT_CLOT);
		popupEtat.addItem(EOTypeEtat.ETAT_VOTE);
	}

	/**
	 *
	 *
	 */
	public void fillPopupEtabs()	{
		NSArray listeEtabs = FinderOrgan.rechercherLignesBudgetairesNiveau(ec, NSApp.getExerciceBudgetaire(), new Integer(1));

		//popupEtab.addItem("****");
		popupEtab.removeAllItems();
		for (int i=0;i<listeEtabs.count();i++)
			popupEtab.addItem(listeEtabs.objectAtIndex(i));
	}

	/**
	 *
	 * @param budget
	 */
	public void setBudgetSaisie(EOBudgetSaisie budget)	{
		currentBudgetSaisie = budget;
		budgetsGestion = new NSArray();

		if (currentBudgetSaisie != null)
			window.setTitle("Pilotage " + currentBudgetSaisie.bdsaLibelle() + " " + currentBudgetSaisie.exercice().exeExercice());
	}


	/**
	 *
	 * @return
	 */
	public String getSqlQualifier() {

		String sqlQualifier = "";

		Number utlOrdre = (Number)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre"));
		Number bdsaid = (Number)(ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentBudgetSaisie).objectForKey("bdsaId"));
		Number niveau = (NSApp.isSaisieCr())?new Integer(3):new Integer(2);

		sqlQualifier = "select o.org_id ORG_ID, org_etab ETAB, org_ub UB, org_cr CR, "
				+ " jefy_budget.budget_moteur.get_etat_saisie("+ bdsaid +" , o.org_id) ETAT, "
				+ " (select nvl(sum(bdsg_saisi), 0) from jefy_budget.budget_saisie_gestion b, jefy_admin.type_credit t where bdsa_id = " + bdsaid
				+ " and b.tcd_ordre = t.tcd_ordre and t.tcd_type = 'DEPENSE' and org_id = o.org_id) MONTANT_GES_SAISI, "
				+ " (select nvl(sum(bdsg_montant), 0) from jefy_budget.budget_saisie_gestion b, jefy_admin.type_credit t where bdsa_id = " + bdsaid
				+ " and b.tcd_ordre = t.tcd_ordre and t.tcd_type = 'DEPENSE' and org_id = o.org_id) MONTANT_GES, "
				+ " (select nvl(sum(bdsn_saisi), 0) from jefy_budget.budget_saisie_nature b, jefy_admin.type_credit t where bdsa_id = " + bdsaid
				+ " and b.tcd_ordre = t.tcd_ordre and t.tcd_type = 'DEPENSE' and org_id = o.org_id) MONTANT_NAT_SAISI, "
				+ " (select nvl(sum(bdsn_montant), 0) from jefy_budget.budget_saisie_nature b, jefy_admin.type_credit t where bdsa_id = " + bdsaid
				+ " and b.tcd_ordre = t.tcd_ordre and t.tcd_type = 'DEPENSE' and org_id = o.org_id) MONTANT_NAT " +
		" FROM jefy_admin.v_organ o, jefy_admin.utilisateur_organ u" +
		" WHERE o.org_id = u.org_id and u.utl_ordre = " + utlOrdre + " and org_niv = " + niveau + " and exe_ordre = " + NSApp.getExerciceBudgetaire().exeExercice() +
		" ORDER BY org_etab, org_ub, org_cr ";

		System.out.println("PilotageCtrl.getSqlQualifier() QUALIF SQL : " + sqlQualifier);

		return sqlQualifier;
	}



	/**
	 *
	 *
	 */
	public void refresh()	 {

		NSDictionary selectedObject = (NSDictionary)eodCr.selectedObject();

		NSArray myOrgans = ServerProxy.clientSideRequestSqlQuery(ec, getSqlQualifier());
		eodCr.setObjectArray(myOrgans);
		myEOTable.updateData();

		eodCr.setSelectedObject(selectedObject);
		myEOTable.forceNewSelection(eodCr.selectionIndexes());

 	   informations.setText(eodCr.displayedObjects().count() + " CRs");

		updateUI();

	}

	/**
	 *
	 * @return
	 */
	public boolean isVisible()	{
		return window.isVisible();
	}


	/**
	 *
	 *
	 */
	public void open()	{

		refresh();

		updateUI();
		window.show();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void gui_init() {

		eodCr = new EODisplayGroup();

		viewTable = new JPanel();

		Vector myCols = new Vector();

		ZEOTableModelColumn col = new ZEOTableModelColumn(eodCr, "ETAB", "Etab", 50);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodCr, "UB", "Ub", 50);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);

		if (NSApp.isSaisieCr())	{
			col = new ZEOTableModelColumn(eodCr, "CR", "Cr", 80);
			col.setAlignment(SwingConstants.LEFT);
			col.setTableCellRenderer(monRendererColor);
			myCols.add(col);
		}

		col = new ZEOTableModelColumn(eodCr, "ETAT", "Etat", 75);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodCr, "MONTANT_GES_SAISI", "Ges.Saisi", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodCr, "MONTANT_GES", "Gestion", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodCr, "MONTANT_NAT_SAISI", "Nat.Saisi", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodCr, "MONTANT_NAT", "Nature", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col.setTableCellRenderer(monRendererColor);
		myCols.add(col);


		myTableModel = new ZEOTableModel(eodCr, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerCr());
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTable.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}


	/**
	 * Listener sur le deuxieme niveau de l'arborescence budgetaire
	 * Mise à jour du troisieme niveau si deuxieme niveau selectionne
	 */
	   private class ListenerCr implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		   public void onDbClick() {

			   NSApp.setWaitCursor(window);

			   if (currentOrgan != null)
				   OrganPopupCtrl.sharedInstance(ec).setSelectedOrgan(currentOrgan);
			   SaisieBudget.sharedInstance(ec).open();

			   NSApp.setDefaultCursor(window);

		   }

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			NSDictionary selectedDico = (NSDictionary)eodCr.selectedObject();
			currentOrgan = null;

			if (selectedDico != null)	{

				currentOrgan = FinderOrgan.findOrganForKey(ec, (Number)selectedDico.objectForKey("ORG_ID"));

				String etat = (String)((NSDictionary)eodCr.selectedObject()).objectForKey("ETAT");

				actionInit.setEnabled(NSApp.fonctionSaisie() && currentBudgetSaisie != null && !currentBudgetSaisie.isVote() && "NON INITIALISE".equals(etat));
				actionValidate.setEnabled(NSApp.fonctionSaisie() && EOTypeEtat.ETAT_EN_COURS.equals(etat));
				actionControle.setEnabled(NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE) && EOTypeEtat.ETAT_VALIDE.equals(etat));
				actionCloture.setEnabled(NSApp.fonctionCloture() && EOTypeEtat.ETAT_CONTROLE.equals(etat));
			}

			updateUI();
		}
	   }


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	 private class PilotageRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_NON_INITIALISE=new Color(255,136,124);
		public final Color COULEUR_EN_COURS=new Color(255,165,158);
		public final Color COULEUR_VALIDE=new Color(153,255,155);
		public final Color COULEUR_CONTROLE=new Color(113,255,113);
		public final Color COULEUR_CLOTURE=new Color(180,180,180);
		public final Color COULEUR_VOTE=new Color(200,200,200);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);

			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, mdlRow, column);

			//Tester la valeur
			String etat = (String)((NSDictionary)eodCr.displayedObjects().objectAtIndex(mdlRow)).objectForKey("ETAT");

			leComposant.setForeground(Color.BLACK);

			if(isSelected)
				leComposant.setBackground(ConstantesCocktail.COLOR_SELECTION_TABLES);
			else	{
				if( "NON INITIALISE".equals(etat) )
					leComposant.setBackground(COULEUR_NON_INITIALISE);
				else
					if( EOTypeEtat.ETAT_EN_COURS.equals(etat) )
						leComposant.setBackground(COULEUR_EN_COURS);
					else
						if( EOTypeEtat.ETAT_VALIDE.equals(etat) )
							leComposant.setBackground(COULEUR_VALIDE);
						else
							if( EOTypeEtat.ETAT_CONTROLE.equals(etat) )
								leComposant.setBackground(COULEUR_CONTROLE);
							else
								if( EOTypeEtat.ETAT_CLOT.equals(etat) )
									leComposant.setBackground(COULEUR_CLOTURE);
								else
									if( EOTypeEtat.ETAT_VOTE.equals(etat) )
										leComposant.setBackground(COULEUR_VOTE);
			}

			return leComposant;
		}
	}


	/**
	 *
	 *
	 */
	public void cleanBudgetsNature()	{

		budgetsNature = new NSArray();

	}


	/**
	 *
	 * Désinitialisation d'une ligne budgetaire (CR ou UB)
	 * Appel a une procedure stockee
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionDelOrgan extends AbstractAction {

	    public ActionDelOrgan() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_DELETE);
        }

        public void actionPerformed(ActionEvent e) {

			boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...",
					"Confirmez-vous la suppression de la saisie du budget pour cette ligne budgétaire ?","OUI","NON");

			if (dialog)	{
				try {

					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey(currentBudgetSaisie,"EOBudgetSaisie");
					parametres.setObjectForKey(currentOrgan,"EOOrgan");

					ServerProxy.clientSideRequestSupprimerSaisieBudget(ec, parametres);

					refresh();

				}
				catch (Exception ex)	{
					EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
				}
			}

        }
	}

	public void close() {

		window.hide();

	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }

        public void actionPerformed(ActionEvent e) {
        	window.hide();
        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionValidate extends AbstractAction {

	    public ActionValidate() {
            super("Verrouiller");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALIDE_BUDGET);
        }


	    public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(window);

	    	try {

	    		for (int i=0;i<eodCr.selectedObjects().count();i++)	{

	    			NSMutableDictionary myRow = new NSMutableDictionary((NSDictionary)eodCr.selectedObjects().objectAtIndex(i));
	    			EOEnterpriseObject organ = (EOEnterpriseObject)FinderOrgan.findOrganForKey(ec, (Number)myRow.objectForKey("ORG_ID"));

	    			FactoryBibasse.validerSaisieOrgan(ec, currentBudgetSaisie, (EOOrgan)organ, NSApp.getExerciceBudgetaire());

	    			budgetsNature = FinderBudgetSaisieNature.findBudgetsNature(ec, currentBudgetSaisie, (EOOrgan)organ, NSApp.getExerciceBudgetaire());
	    			for (int j=0;j<budgetsNature.count();j++)
	    				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOBudgetSaisieNature)budgetsNature.objectAtIndex(j))));
	    		}

	       		refresh();

	    		myEOTable.updateData();

	    	}
	    	catch (Exception ex)	{
	    		ex.printStackTrace();
	    		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
	    	}
	    	NSApp.setDefaultCursor(window);


        }

	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionInit extends AbstractAction {

	    public ActionInit() {
            super("Initialiser");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_INIT_BUDGET);
        }


	    public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(window);

	    	try {

	    		ProcessBudgetFactory myProcessBudgetFactory = new ProcessBudgetFactory(true, ProcessBudgetFactory.PROCESS_PROVISOIRE);

	    		NSMutableArray plancosCredit = new NSMutableArray(SaisieBudgetNatureDepenses.sharedInstance(ec).getMasqueNature());
	    		plancosCredit.addObjectsFromArray(SaisieBudgetNatureRecettes.sharedInstance(ec).getMasqueNature());

	    		for (int i=0;i<eodCr.selectedObjects().count();i++)	{

	    			NSMutableDictionary dico = new NSMutableDictionary((NSDictionary)eodCr.selectedObjects().objectAtIndex(i));

	    			if ("NON INITIALISE".equals(dico.objectForKey("ETAT")))	{

	    				EOOrgan organ = FinderOrgan.findOrganForKey(ec, (Number)dico.objectForKey("ORG_ID"));

	    				myProcessBudgetFactory.initialiserBudgetGestion(
	    						ec,
	    						FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
	    						SaisieBudgetGestionDepenses.sharedInstance(ec).getTypesCredit(),
	    						SaisieBudgetGestionDepenses.sharedInstance(ec).getActions(),
	    						SaisieBudgetGestionRecettes.sharedInstance(ec).getTypesCredit(),
	    						SaisieBudgetGestionRecettes.sharedInstance(ec).getActions(),
	    						organ,
	    						NSApp.getExerciceBudgetaire(),
	    						NSApp.getUtilisateur(),
	    						currentBudgetSaisie
	    				);

	    				myProcessBudgetFactory.initialiserBudgetNature(
	    						ec,
	    						FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
	    						plancosCredit,
	    						organ,
	    						NSApp.getExerciceBudgetaire(),
	    						NSApp.getUtilisateur(),
	    						currentBudgetSaisie
	    				);

	    				ec.saveChanges();
	    			}

	    		}
	    	}
	    	catch (Exception ex)	{
	    		ex.printStackTrace();
	    		EODialogs.runErrorDialog("ERREUR", ex.getMessage());
	    	}

	    	refresh();

	    	NSApp.setDefaultCursor(window);
	    }

	}


	/**
	 *
	 *
	 */
	private void updateUI()	{

		NSDictionary selectedRow = (NSDictionary)eodCr.selectedObject();

		actionDelOrgan.setEnabled(selectedRow != null && !selectedRow.objectForKey("ETAT").equals("NON INITIALISE"));

		if (currentBudgetSaisie != null && currentBudgetSaisie.isVote())	{
			actionCloture.setEnabled(false);
			actionControle.setEnabled(false);
			actionValidate.setEnabled(false);
			actionDelOrgan.setEnabled(false);
		}

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionControle extends AbstractAction {

	    public ActionControle() {
            super("contrôler");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CONTROLE_BUDGET);
        }

        public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(window);

	    	try {

	    		for (int i=0;i<eodCr.selectedObjects().count();i++)	{

	    			NSMutableDictionary myRow = new NSMutableDictionary((NSDictionary)eodCr.selectedObjects().objectAtIndex(i));

	    			EOEnterpriseObject organ = (EOEnterpriseObject)FinderOrgan.findOrganForKey(ec, (Number)myRow.objectForKey("ORG_ID"));

	    			NSMutableDictionary parametres = new NSMutableDictionary();
	    			parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
	    			parametres.setObjectForKey(organ, "EOOrgan");

		    		ServerProxy.clientSideRequestControlerLigneBudgetaire(ec, parametres);

		    		budgetsNature = FinderBudgetSaisieNature.findBudgetsNature(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire());
		    		for (int j=0;j<budgetsNature.count();j++)
		    			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOBudgetSaisieNature)budgetsNature.objectAtIndex(j))));
	    		}

	       		refresh();

	       		// Si l'organ traite est celui qui est affiche, on actualise.
	       		EOOrgan organSaisie = SaisieBudget.sharedInstance(ec).getCurrentOrgan();
	       		if (organSaisie != null && organSaisie.equals(currentOrgan))
	       			SaisieBudget.sharedInstance(ec).organHasChanged(organSaisie);
	    	}
	    	catch (Exception ex)	{
	    		ex.printStackTrace();
	    		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
	    	}
	    	NSApp.setDefaultCursor(window);


        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionCloture extends AbstractAction {

	    public ActionCloture() {
            super("Clôturer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOTURE_BUDGET);
        }

        public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(window);

	    	try {

//	    		if (eodCr.selectedObjects().count() > 1)	{
//	    			EODialogs.runInformationDialog("ERREUR","1 seul CR doit être sélectionné pour l'étape de clôture !");
//					return;
//	    		}

	    		for (int i=0;i<eodCr.selectedObjects().count();i++)	{

	    			NSMutableDictionary myRow = new NSMutableDictionary((NSDictionary)eodCr.selectedObjects().objectAtIndex(i));

	    			EOEnterpriseObject organ = (EOEnterpriseObject)FinderOrgan.findOrganForKey(ec, (Number)myRow.objectForKey("ORG_ID"));
	    			NSMutableDictionary parametres = new NSMutableDictionary();
	    			parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
	    			parametres.setObjectForKey(organ, "EOOrgan");
	    			ServerProxy.clientSideRequestCloturerLigneBudgetaire(ec, parametres);

		    		budgetsNature = FinderBudgetSaisieNature.findBudgetsNature(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire());
		    		for (int j=0;j<budgetsNature.count();j++)
		    			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOBudgetSaisieNature)budgetsNature.objectAtIndex(j))));
	    		}

				OrganPopupCtrl.sharedInstance(ec).setSelectedOrgan(currentOrgan);
	       		refresh();

	    	}
	    	catch (Exception ex)	{
	    		ex.printStackTrace();
	    		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
	    	}
	    	NSApp.setDefaultCursor(window);


        }
	}



	/**
	 *
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (popupEtat.getSelectedIndex() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ETAT = %@",new NSArray((String)popupEtat.getSelectedItem())));

		//if (popupEtab.getSelectedIndex() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ETAB = %@",new NSArray(filterOrgan.orgEtab())));

		if (popupUb.getSelectedIndex() > 0)	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("UB = %@",new NSArray(filterOrgan.orgUb())));

			if (popupCr.getSelectedIndex() > 0)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("CR = %@",new NSArray(filterOrgan.orgCr())));
		}

		return new EOAndQualifier(mesQualifiers);
	}


	/**
	 *
	 *
	 */
	public void filter()	{


	       eodCr.setQualifier(getFilterQualifier());
	       eodCr.updateDisplayedObjects();


	       if (informations != null && eodCr != null)
	    	   informations.setText(eodCr.displayedObjects().count() + " CRs");

	       myEOTable.updateData();

	}

	/**
	 *
	 *
	 */
	public void etabHasChanged()	{

		popupUb.removeActionListener(myListenerUb);
		popupCr.removeActionListener(myListenerCr);
		popupUb.removeAllItems();
		popupUb.addItem("****");

		popupCr.removeAllItems();
		popupCr.addItem("****");

//		if (popupEtab.getSelectedIndex() == 0)	{
//			currentOrgan = null;
//			return;
//		}

		if (popupEtab.getSelectedItem()==null) {
			currentOrgan = null;
			return;
		}
		filterOrgan = (EOOrgan)popupEtab.getSelectedItem();
		popupEtab.removeActionListener(myListenerEtab);

		NSMutableArray listeUbs = new NSMutableArray(filterOrgan.organFils());
		EOSortOrdering.sortArrayUsingKeyOrderArray(listeUbs, new NSArray(new EOSortOrdering("orgUb", EOSortOrdering.CompareAscending)));

		for (int i=0;i<listeUbs.count();i++)
			popupUb.addItem(listeUbs.objectAtIndex(i));

		popupUb.addActionListener(myListenerUb);
		popupCr.addActionListener(myListenerCr);

		popupEtab.addActionListener(myListenerEtab);

		filter();
	}

	public void ubHasChanged()	{

		popupCr.removeActionListener(myListenerCr);

		popupCr.removeAllItems();
		popupCr.addItem("****");

		if (popupUb.getSelectedIndex() == 0)
			filterOrgan = (EOOrgan)popupEtab.getSelectedItem();
		else	{
			filterOrgan = (EOOrgan)popupUb.getSelectedItem();

			NSMutableArray listeCrs = new NSMutableArray(filterOrgan.organFils());
			EOSortOrdering.sortArrayUsingKeyOrderArray(listeCrs, new NSArray(new EOSortOrdering("orgCr", EOSortOrdering.CompareAscending)));
			for (int i=0;i<listeCrs.count();i++)
				popupCr.addItem(listeCrs.objectAtIndex(i));
		}

		popupCr.addActionListener(myListenerCr);

		filter();
	}

	public void crHasChanged()	{

		if (popupCr.getSelectedIndex() == 0)	{
			if (popupUb.getSelectedIndex() == 0)
				filterOrgan = (EOOrgan)popupEtab.getSelectedItem();
			else
				filterOrgan = (EOOrgan)popupUb.getSelectedItem();
		}
		else
			filterOrgan = (EOOrgan)popupCr.getSelectedItem();

		filter();
	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerFiltreEtab implements ActionListener {

		public ListenerFiltreEtab() {super();}
		public void actionPerformed(ActionEvent anAction) {
			etabHasChanged();
		}

	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerFiltreUb implements ActionListener {

		public ListenerFiltreUb() {super();}
		public void actionPerformed(ActionEvent anAction) {
			ubHasChanged();
		}

	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerFiltreCr implements ActionListener {

		public ListenerFiltreCr() {super();}
		public void actionPerformed(ActionEvent anAction) {
			crHasChanged();
		}

	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerFiltreEtat implements ActionListener {

		public ListenerFiltreEtat() {super();}
		public void actionPerformed(ActionEvent anAction) {
			filter();
		}

	}
}
