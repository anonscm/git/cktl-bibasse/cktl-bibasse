package org.cocktail.bibasse.client;

import java.awt.Color;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.ImageIcon;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class ConstantesCocktail extends EOModalDialogController
{

	private	static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();

	public final static String APPLICATION_NAME = "BIBASSE";

	// COULEURS

	public static final Color BG_COLOR_WHITE = new Color(255,255,255);
	public static final Color BG_COLOR_BLACK = new Color(0,0,0);
	public static final Color BG_COLOR_RED = new Color(255,0,0);
	public static final Color BG_COLOR_GREEN = new Color(0,255,0);
	public static final Color BG_COLOR_BLUE = new Color(0,0,255);
	public static final Color BG_COLOR_YELLOW = new Color(255,255,0);

	public static final Color BG_COLOR_DEPENSES = new Color(218,221,255);
	public static final Color BG_COLOR_RECETTES = new Color(255, 207, 213);

	public static final Color COLOR_SELECTION_NOMENCLATURES = new Color(100,100,100);
	public static final Color COLOR_FOND_NOMENCLATURES = new Color(220,220,220);
	public static final Color COLOR_FILTRES_NOMENCLATURES = new Color(240,240,240);
	public static final Color COLOR_SELECTION_TABLES = new Color(127,155,165);

	// FORMATS
	public static final Format FORMAT_EUROS = NumberFormat.getCurrencyInstance(Locale.FRANCE);
	public static final Format FORMAT_NUMBER = NumberFormat.getNumberInstance();
	public static final Format FORMAT_DECIMAL = new DecimalFormat("#,##0.00");
	public static final Format FORMAT_DATESHORT = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);

	// FONCTIONS
	public static final String ID_FCT_OUVERTURE = "OPENBUD";
	public static final String ID_FCT_SAISIE = "SAISBUD";
	public static final String ID_FCT_CONTROLE = "CTRLBUD";
	public static final String ID_FCT_CLOTURE = "CLOSEBUD";
	public static final String ID_FCT_VOTE = "VOTEBUD";
	public static final String ID_FCT_REINIT = "REINITBD";
	public static final String ID_FCT_MASQUE = "MASQUEBD";
	public static final String ID_FCT_PARAM = "PARAMBUD";

	public static final String ID_FCT_VIREMENT = "VIREMENT";
	public static final String ID_FCT_VENTILATION = "VENTIL";

	public static final String ID_FCT_PILOTAGE = "PILOTBUD";

	public static final String ID_FCT_CONSULTATION = "CONSBUD";
	public static final String ID_FCT_PRINT = "IMPBUD";

	public static final String ID_FCT_GENERATION_COFISUP = "GCOFISUP";

	// IMAGES

	public static final String IMAGE_APP_LOGO = "logoAppliBibasse";
    public static final ImageIcon ICON_APP_LOGO = (ImageIcon)new EOClientResourceBundle().getObject(IMAGE_APP_LOGO);

	public static final String IMAGE_ADD = "add";
	public static final String IMAGE_UPDATE= "update";
	public static final String IMAGE_DELETE = "delete";
	public static final String IMAGE_VALID = "valid";
	public static final String IMAGE_CANCEL = "cancel";
	public static final String IMAGE_CLOSE = "close";
	public static final String IMAGE_SAVE = "save";

	public static final String IMAGE_LOGIN = "login";
	public static final String IMAGE_QUIT = "exit";

	public static final String IMAGE_LOCK = "lock";
	public static final String IMAGE_UNLOCK = "unlock";

	public static final String IMAGE_ROOT = "root";

	public static final String IMAGE_PARAMETRES = "params";

	public static final String IMAGE_FLECHE_DROITE = "FlecheDroite";
	public static final String IMAGE_FLECHE_GAUCHE = "FlecheGauche";

	public static final String IMAGE_MENU_ACTIF = "FlecheDroite";
	public static final String IMAGE_MENU_INACTIF = "FlecheDroiteGrise";

	public static final String IMAGE_MAIN_MENU = "valid";
	public static final String IMAGE_OUVERTURE_BUDGET = "open_budget";
	public static final String IMAGE_SAISIE_BUDGET = "saisie_budget";
	public static final String IMAGE_PILOTAGE = "pilotage";
	public static final String IMAGE_VOTE_BUDGET = "vote";
	public static final String IMAGE_CALCUL_BUDGET = "calculatrice";

	public static final String IMAGE_REINIT = "reinit_budget";

	public static final String IMAGE_INIT_BUDGET = "valid";
	public static final String IMAGE_VALIDE_BUDGET = "organ_valide";
	public static final String IMAGE_CONTROLE_BUDGET = "organ_controle";
	public static final String IMAGE_CLOTURE_BUDGET = "organ_cloture";

	public static final String IMAGE_CALENDAR = "Calendar";

	public static final String IMAGE_IMPRIMER_16 = "printer_16";
	public static final String IMAGE_IMPRIMER_32 = "printer_32";

	public static final String IMAGE_LOUPE = "loupe";
	public static final String IMAGE_INTERROGER = "why";
	public static final String IMAGE_SELECT = "select";

	public static final String IMAGE_CALCULER = "calculatrice";

	public static final String IMAGE_ALERT_INFO = "alert-exclam";
	public static final String IMAGE_ALERT_ERROR = "alert-error";
	public static final String IMAGE_ALERT_OK = "alert-message";

	public static final String IMAGE_GLYPH_OK = "glyphicons-ok";
	public static final String IMAGE_GLYPH_BAN = "glyphicons-ban";


	// ICONS
	public static final ImageIcon ICON_LOGIN = (ImageIcon) resourceBundle.getObject(IMAGE_LOGIN);
	public static final ImageIcon ICON_QUIT = (ImageIcon) resourceBundle.getObject(IMAGE_QUIT);

	public static final ImageIcon ICON_MENU_ACTIF = (ImageIcon) resourceBundle.getObject(IMAGE_MENU_ACTIF);
	public static final ImageIcon ICON_MENU_INACTIF = (ImageIcon) resourceBundle.getObject(IMAGE_MENU_INACTIF);

	public static final ImageIcon ICON_PARAMETRES = (ImageIcon) resourceBundle.getObject(IMAGE_PARAMETRES);

	public static final ImageIcon ICON_FLECHE_DROITE = (ImageIcon) resourceBundle.getObject(IMAGE_FLECHE_DROITE);
	public static final ImageIcon ICON_FLECHE_GAUCHE = (ImageIcon) resourceBundle.getObject(IMAGE_FLECHE_GAUCHE);

	public static final ImageIcon ICON_ADD = (ImageIcon) resourceBundle.getObject(IMAGE_ADD);
	public static final ImageIcon ICON_UPDATE = (ImageIcon) resourceBundle.getObject(IMAGE_UPDATE);
	public static final ImageIcon ICON_DELETE = (ImageIcon) resourceBundle.getObject(IMAGE_DELETE);
	public static final ImageIcon ICON_VALID = (ImageIcon) resourceBundle.getObject(IMAGE_VALID);
	public static final ImageIcon ICON_CANCEL = (ImageIcon) resourceBundle.getObject(IMAGE_CANCEL);
	public static final ImageIcon ICON_CLOSE = (ImageIcon) resourceBundle.getObject(IMAGE_CLOSE);
	public static final ImageIcon ICON_SAVE = (ImageIcon) resourceBundle.getObject(IMAGE_SAVE);

	public static final ImageIcon ICON_CALENDAR = (ImageIcon) resourceBundle.getObject(IMAGE_CALENDAR);

	public static final ImageIcon ICON_REINIT = (ImageIcon) resourceBundle.getObject(IMAGE_REINIT);
	public static final ImageIcon ICON_LOCK = (ImageIcon) resourceBundle.getObject(IMAGE_LOCK);
	public static final ImageIcon ICON_UNLOCK = (ImageIcon) resourceBundle.getObject(IMAGE_UNLOCK);
	public static final ImageIcon ICON_ROOT = (ImageIcon) resourceBundle.getObject(IMAGE_ROOT);

	public static final ImageIcon ICON_MAIN_MENU = (ImageIcon)resourceBundle.getObject(IMAGE_MAIN_MENU);
	public static final ImageIcon ICON_SAISIE_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_SAISIE_BUDGET);
	public static final ImageIcon ICON_OUVERTURE_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_OUVERTURE_BUDGET);
	public static final ImageIcon ICON_INIT_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_INIT_BUDGET);
	public static final ImageIcon ICON_PILOTAGE = (ImageIcon)resourceBundle.getObject(IMAGE_PILOTAGE);
	public static final ImageIcon ICON_CALCUL_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_CALCUL_BUDGET);
	public static final ImageIcon ICON_VOTE_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_VOTE_BUDGET);

	public static final ImageIcon ICON_VALIDE_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_VALIDE_BUDGET);
	public static final ImageIcon ICON_CONTROLE_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_CONTROLE_BUDGET);
	public static final ImageIcon ICON_CLOTURE_BUDGET = (ImageIcon)resourceBundle.getObject(IMAGE_CLOTURE_BUDGET);

	public static final ImageIcon ICON_IMPRIMER_16  = (ImageIcon)resourceBundle.getObject(IMAGE_IMPRIMER_16);
	public static final ImageIcon ICON_IMPRIMER_32  = (ImageIcon)resourceBundle.getObject(IMAGE_IMPRIMER_32);

	public static final ImageIcon ICON_LOUPE  = (ImageIcon)resourceBundle.getObject(IMAGE_LOUPE);
	public static final ImageIcon ICON_INTERROGER  = (ImageIcon)resourceBundle.getObject(IMAGE_INTERROGER);
	public static final ImageIcon ICON_SELECT  = (ImageIcon)resourceBundle.getObject(IMAGE_SELECT);

	public static final ImageIcon ICON_ALERT_INFO  = (ImageIcon)resourceBundle.getObject(IMAGE_ALERT_INFO);
	public static final ImageIcon ICON_ALERT_ERROR  = (ImageIcon)resourceBundle.getObject(IMAGE_ALERT_ERROR);
	public static final ImageIcon ICON_ALERT_OK = (ImageIcon)resourceBundle.getObject(IMAGE_ALERT_OK);

	public static final ImageIcon ICON_GLYPH_OK = (ImageIcon) resourceBundle.getObject(IMAGE_GLYPH_OK);
	public static final ImageIcon ICON_GLYP_BAN = (ImageIcon) resourceBundle.getObject(IMAGE_GLYPH_BAN);

}