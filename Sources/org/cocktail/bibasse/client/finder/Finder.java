/*
 * Created on 7 oct. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.bibasse.client.finder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Finder {

	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{

		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
			fs.setRefreshesRefetchedObjects(true);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}

	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sortOrderings, boolean refresh)	{

		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}

	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String stringQualifier, NSArray sortOrderings, boolean refresh)	{

		try {

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);

			EOFetchSpecification fs = new EOFetchSpecification(entityName, myQualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}

	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{

		EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);

	}

	public static NSArray tableauTrie(NSArray donnees, NSArray sort) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(donnees, sort);
	}
}
