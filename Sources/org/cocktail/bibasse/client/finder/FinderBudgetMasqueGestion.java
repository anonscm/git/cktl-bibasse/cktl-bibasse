package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMasqueGestion;
import org.cocktail.bibasse.client.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMasqueGestion {

	/**
	 * @return
	 */
	public static NSArray findMasqueGestion(EOEditingContext ec, EOExercice exercice)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueGestion.EXERCICE_KEY+"=%@", new NSArray(exercice));
		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueGestion.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	
	/**
	 * @return
	 */
	public static NSArray findMasqueActions(EOEditingContext ec, EOExercice exercice, String type)	{
		NSArray sort = new NSArray(new EOSortOrdering("typeAction.tyacCode", EOSortOrdering.CompareAscending));
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		if (type != null)	
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction.tyacType = %@", new NSArray(type)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueGestion.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort);
		fs.setRefreshesRefetchedObjects(true);
		NSArray masques =  ec.objectsWithFetchSpecification(fs);
		
		return (NSArray)masques.valueForKey("typeAction");
	}
}
