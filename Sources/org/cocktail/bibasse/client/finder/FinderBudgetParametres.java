package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetParametres;
import org.cocktail.bibasse.client.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetParametres {

	public static final String COMPTE_DEFICIT_FONC = "COMPTE_DEFICIT_FONC";
	public static final String COMPTE_CAF = "COMPTE_CAF";
	public static final String COMPTE_IAF = "COMPTE_IAF";

	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static String getValue(EOEditingContext ec, EOExercice exercice, String key)	{
		String parametreResult = null;
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetParametres.EXERCICE_KEY + " = %@", new NSArray(exercice)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetParametres.BPAR_KEY_KEY + " = '" + key + "'", null));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetParametres.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);

			NSArray params = ec.objectsWithFetchSpecification(fs);

			if (params.count() == 0) {
				System.out.println("Parametre manquant : " + key + ", exercice : " + exercice);
				parametreResult = null;
			} else {
				parametreResult = ((EOBudgetParametres) params.objectAtIndex(0)).bparValue();
			}

			return parametreResult;
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Parametre manquant : "+key+", exercice : "+exercice);
			return null;
		}
	}

	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static EOBudgetParametres findParametre(EOEditingContext ec, EOExercice exercice, String key)	{

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetParametres.BPAR_KEY_KEY + " = %@", new NSArray(key)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetParametres.EXERCICE_KEY + " = %@", new NSArray(exercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetParametres.ENTITY_NAME,new EOAndQualifier(mesQualifiers),null);

			return (EOBudgetParametres)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			return null;
		}

	}

	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
*/
	public static NSArray getParametres(EOEditingContext ec, EOExercice exercice)	{

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("bparUpdate = 'N' and exercice = %@", new NSArray (exercice));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetParametres.ENTITY_NAME,qual,null);

		NSArray params = ec.objectsWithFetchSpecification(fs);

		return params;
	}

}
