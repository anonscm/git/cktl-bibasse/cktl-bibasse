package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetExecCredit;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetExecCredit {

	/**
	 * 
	 * @param ec
	 * @param organ
	 * @param typeCredit
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsExecution(EOEditingContext ec, EOOrgan organ, EOTypeCredit typeCredit, EOExercice exercice)  {
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetExecCredit.ORGAN_KEY+"=%@", new NSArray(organ)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetExecCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			e.printStackTrace();
			return new NSArray();
		}
	}
}
