package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetVoteNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetVoteNature {
	
	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsVote(EOEditingContext ec, EOOrgan organ, EOTypeCredit typeCredit, EOExercice exercice)  {
		try { 
			NSArray mySort = new NSArray(new EOSortOrdering(EOBudgetVoteNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareAscending));
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.ORGAN_KEY+"=%@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.TYPE_CREDIT_KEY+"!=%@", 
					new NSArray(FinderTypeCredit.findTypesCreditReserve(ec, exercice))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.BDVN_OUVERTS_KEY+">0", null));

			if (typeCredit != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetVoteNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsVoteForVirement(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {
		
		try { 
			
			NSMutableArray mySort = new NSMutableArray();
			mySort.addObject(new EOSortOrdering(EOBudgetVoteNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_TYPE_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(EOBudgetVoteNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_SECT_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(EOBudgetVoteNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering(EOBudgetVoteNature.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending));
			
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.ORGAN_KEY+"=%@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.TYPE_CREDIT_KEY+"!=%@", 
					new NSArray(FinderTypeCredit.findTypesCreditReserve(ec, exercice))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetVoteNature.BDVN_OUVERTS_KEY+">0 or "+
					EOBudgetVoteNature.BDVN_VOTES_KEY+">0", null));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetVoteNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
}
