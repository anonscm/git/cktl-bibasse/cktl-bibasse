package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetVoteChapitreCtrl;
import org.cocktail.bibasse.client.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderBudgetVoteChapitreCtrl {

	
	/**
	 * 
	 * @return
	 */
	public static NSArray findChapitresControles(EOEditingContext ec, EOExercice exercice)	{
				
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetVoteChapitreCtrl.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}
	

}
