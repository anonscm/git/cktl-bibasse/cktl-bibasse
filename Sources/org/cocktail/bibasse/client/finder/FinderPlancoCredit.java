package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOPlancoCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPlancoCredit {
	
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptesBudgetairesForBudgetNature(EOEditingContext ec, String classe, NSArray typesCredit, EOExercice exercice) {
		
		NSArray mySort = new NSArray(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareAscending));
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();

			if (typesCredit != null && typesCredit.count() > 0)	{
				
				NSMutableArray qualifsTypeCredit = new NSMutableArray();
				for (int i=0;i<typesCredit.count();i++)
					qualifsTypeCredit.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit.tcdCode = %@", new NSArray(typesCredit.objectAtIndex(i))));
				mesQualifiers.addObject(new EOOrQualifier(qualifsTypeCredit));
			}
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable.pcoBudgetaire = 'O'", null));
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum like %@", new NSArray(classe+"*")));
						
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit.exercice = %@", new NSArray(exercice)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOPlancoCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return comptes;
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
		
}
