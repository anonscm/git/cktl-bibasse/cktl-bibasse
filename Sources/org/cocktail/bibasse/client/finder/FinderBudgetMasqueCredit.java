package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMasqueCredit;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMasqueCredit {
	
	/**
	 * @return
	 */
	public static NSArray findMasqueCredit(EOEditingContext ec, EOExercice exercice)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice));
		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueCredit.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	public static NSArray findMasqueCreditTrieTypeCredit(EOEditingContext ec, EOExercice exercice)	{
		return Finder.tableauTrie(findMasqueCredit(ec, exercice), sort());
	}

	/**
	 * @return
	 */
	public static NSArray findTypesCredit(EOEditingContext ec, EOExercice exercice, String section, String type)	{
		
		NSArray mySort = new NSArray(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareAscending));

		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit.tcdBudget != %@", new NSArray(EOTypeCredit.TYPE_BUDGET_RESERVE)));
		if (section != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit.tcdSect = %@", new NSArray(section)));
		if (type != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit.tcdType = %@", new NSArray(type)));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);
		NSArray masques = ec.objectsWithFetchSpecification(fs);
		return (NSArray)masques.valueForKey(EOBudgetMasqueCredit.TYPE_CREDIT_KEY);
	}

    private static NSArray sort() {
    	NSMutableArray array=new NSMutableArray();
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetMasqueCredit.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, 
    			EOSortOrdering.CompareCaseInsensitiveAscending));
    	return array;
    }
}
