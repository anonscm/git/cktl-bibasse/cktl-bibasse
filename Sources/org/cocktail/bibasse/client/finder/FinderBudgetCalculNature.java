package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetCalculNature;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetCalculNature {

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static EOBudgetSaisieNature findBudgetNatureForExercice(EOEditingContext ec, EOExercice exercice)  {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetCalculNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOBudgetSaisieNature)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e ) {
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static EOBudgetSaisieNature findBudgetNature(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ, EOPlanComptable planComptable, EOTypeCredit typeCredit)  {
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetSaisie = %@", new NSArray(budgetSaisie)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable = %@", new NSArray(planComptable)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(typeCredit)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetCalculNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return (EOBudgetSaisieNature)budgets.objectAtIndex(0);
		}
		catch (Exception e )	{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsNature(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ, EOExercice exercice)  {
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetSaisie = %@", new NSArray(budgetSaisie)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat != %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetCalculNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			e.printStackTrace();
			return new NSArray();
		}
	}
}
