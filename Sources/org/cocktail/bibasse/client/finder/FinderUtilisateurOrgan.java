package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOUtilisateur;
import org.cocktail.bibasse.client.metier.EOUtilisateurOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderUtilisateurOrgan {


	/**
	 *
	 *
	 * @param ec
	 * @return
	 */
	public static NSArray findOrgansForUtilisateur(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice)	{

		NSMutableArray args = new NSMutableArray();
		args.addObject(utilisateur);
		args.addObject(exercice);

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateur = %@ and organ.exercice = %@", args);

		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateurOrgan.ENTITY_NAME, myQualifier, null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray("organ.organPere"));

		return ec.objectsWithFetchSpecification(fs);

	}

}
