package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOTypeEvenement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeEvenement {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeEvenement findTypeEvenement(EOEditingContext ec, String type)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeEvenement.TYEV_LIBELLE_KEY + " = %@", new NSArray(type));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeEvenement.ENTITY_NAME, myQualifier, null);
		
		try {return (EOTypeEvenement)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
		
	}
	
}
