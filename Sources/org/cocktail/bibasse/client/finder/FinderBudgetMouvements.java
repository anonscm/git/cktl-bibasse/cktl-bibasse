package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMouvGestion;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMouvements {

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsVirements(EOEditingContext ec, EOExercice exercice, EOOrgan organ)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.TYPE_MOUVEMENT_BUDGETAIRE_KEY+"=%@", 
				new NSArray(FinderTypeMouvementBudgetaire.findTypeMouvement(ec, EOTypeMouvementBudgetaire.MOUVEMENT_VIREMENT))));

		if (organ.orgNiveau().intValue() == 3)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.BUDGET_MOUVEMENTS_GESTIONS_KEY+"."+
					EOBudgetMouvGestion.ORGAN_KEY+"=%@", new NSArray(organ)));
		if (organ.orgNiveau().intValue() == 2)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.BUDGET_MOUVEMENTS_GESTIONS_KEY+"."+
					EOBudgetMouvGestion.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY+"=%@", new NSArray(organ.orgUb())));
		if (organ.orgNiveau().intValue() == 1)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.BUDGET_MOUVEMENTS_GESTIONS_KEY+"."+
					EOBudgetMouvGestion.ORGAN_KEY+"."+EOOrgan.ORG_ETAB_KEY+"=%@", new NSArray(organ.orgEtab())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.TYPE_ETAT_KEY+"=%@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_TRAITE))));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvements.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setUsesDistinct(true);
		return ec.objectsWithFetchSpecification(fs);
	}
}
