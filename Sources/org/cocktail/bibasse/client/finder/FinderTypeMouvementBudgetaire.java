package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeMouvementBudgetaire {


	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeMouvementBudgetaire findTypeMouvement(EOEditingContext ec, String etat)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("tymbLibelle = %@", new NSArray(etat));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeMouvementBudgetaire.ENTITY_NAME, myQualifier, null);
		
		try {return (EOTypeMouvementBudgetaire)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
		
	}
	
	
}
