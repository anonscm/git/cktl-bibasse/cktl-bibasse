package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetCalculGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetCalculGestion {

	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsGestion(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ, EOExercice exercice)  {
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetSaisie = %@", new NSArray(budgetSaisie)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat != %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetCalculGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param action
	 * @param typeCredit
	 * @return
	 */
	public static EOBudgetSaisieGestion findBudgetGestion(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ, EOTypeAction action, EOTypeCredit typeCredit)  {
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetSaisie = %@", new NSArray(budgetSaisie)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction = %@", new NSArray(action)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(typeCredit)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetSaisieGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return (EOBudgetSaisieGestion)budgets.objectAtIndex(0);
		}
		catch (Exception e )	{
			//			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static EOBudgetSaisieGestion findBudgetGestionForOrganAndExercice(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetSaisieGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return (EOBudgetSaisieGestion)budgets.objectAtIndex(0);
		}
		catch (Exception e )	{
			return null;
		}
	}
}
