package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOTypeSaisie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeSaisie {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeSaisie findTypeSaisie(EOEditingContext ec, String type)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("tysaLibelle = %@", new NSArray(type));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeSaisie.ENTITY_NAME, myQualifier, null);
		
		try {return (EOTypeSaisie)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
		
	}
	
}
