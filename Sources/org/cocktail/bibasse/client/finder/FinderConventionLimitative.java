package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOConventionLimitative;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderConventionLimitative {


	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
		public static NSArray findConventionsForOrgan(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {
			try { 
				NSMutableArray mesQualifiers = new NSMutableArray();
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConventionLimitative.ORGAN_KEY+" = %@", new NSArray(organ)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConventionLimitative.EXERCICE_KEY + " = %@", new NSArray(exercice)));
								
				EOFetchSpecification fs = new EOFetchSpecification(EOConventionLimitative.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				fs.setRefreshesRefetchedObjects(true);
				
				return ec.objectsWithFetchSpecification(fs);
			}
			catch (Exception e )	{
				e.printStackTrace();
				return new NSArray();
			}
		}
}
