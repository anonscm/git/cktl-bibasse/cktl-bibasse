package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOTypeOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeOrgan {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeOrgan findTypeOrgan(EOEditingContext ec, String type)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeOrgan.TYOR_LIBELLE_KEY+" = %@", new NSArray(EOTypeOrgan.TYPE_CONVENTION));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeOrgan.ENTITY_NAME, myQualifier, null);

		try {return (EOTypeOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{e.printStackTrace();return null;}
		
	}
	
}
