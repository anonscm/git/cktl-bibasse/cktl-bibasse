package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvGestion;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMouvGestion {

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsGestion(EOEditingContext ec, EOExercice exercice, EOOrgan organ)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		//		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeSens = %@", new NSArray(FinderTypeSens.findTypeSens(ec, EOTypeSens.TYPE_SENS_DEBIT))));

		if (organ.orgNiveau().intValue() >= 2)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY+"=%@", 
					new NSArray(organ.orgUb())));
		if (organ.orgNiveau().intValue() == 1)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_ETAB_KEY+"=%@", 
					new NSArray(organ.orgEtab())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.TYPE_ETAT_KEY+"=%@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_TRAITE))));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsGestionForMouvement(EOEditingContext ec, EOBudgetMouvements mouvement)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		try {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvGestion.MOUVEMENT_KEY+"=%@", new NSArray(mouvement)));
			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
}
