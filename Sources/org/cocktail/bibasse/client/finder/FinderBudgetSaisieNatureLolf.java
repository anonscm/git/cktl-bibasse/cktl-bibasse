package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNatureLolf;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class FinderBudgetSaisieNatureLolf {
	
	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param typeCredit
	 * @return
	 */
	public static NSArray findBudgetsNatureLolf(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOTypeCredit typeCredit)  {
		
		try { 
			return Finder.fetchArray(ec, EOBudgetSaisieNatureLolf.ENTITY_NAME, 
					EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNatureLolf.BUDGET_SAISIE_KEY+"=%@ and "+ EOBudgetSaisieNatureLolf.TYPE_CREDIT_KEY+"=%@", 
					new NSArray(new Object[] {budgetSaisie, typeCredit})), 
					new NSArray(new Object [] {
						EOSortOrdering.sortOrderingWithKey(EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending),
						EOSortOrdering.sortOrderingWithKey(EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY+"."+EOTypeAction.TYAC_CODE_KEY, EOSortOrdering.CompareAscending),
						EOSortOrdering.sortOrderingWithKey(EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY+"."+EOTypeAction.TYAC_LIBELLE_KEY, EOSortOrdering.CompareAscending)}));
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
}
