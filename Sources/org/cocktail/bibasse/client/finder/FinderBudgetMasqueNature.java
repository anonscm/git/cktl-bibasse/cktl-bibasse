package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMasqueNature {

	/**
	 * 
	 * @return
	 */
	public static NSArray findMasqueForInit(EOEditingContext ec, String section, EOExercice exercice, String type)	{
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_SECT_KEY+"=%@",
					new NSArray(section)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_TYPE_KEY+"=%@", 
					new NSArray(type)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray masques = ec.objectsWithFetchSpecification(fs);
			return masques;
		}
		catch (Exception e)	{
			return null;
		}
	}	

	/**
	 * 
	 * @return
	 */
	public static NSArray findMasquesNatureForTypeCredit(EOEditingContext ec, EOExercice exercice, EOTypeCredit typeCredit)	{
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return null;
		}
	}	

	/**
	 * 
	 * @return
	 */
	public static NSArray findMasqueNatureDepense(EOEditingContext ec, EOExercice exercice)	{
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.TYPE_CREDIT_KEY+"."+
					EOTypeCredit.TCD_TYPE_KEY+"=%@", new NSArray("DEPENSE")));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			return null;
		}
	}	

	/**
	 * 
	 * @return
	 */
	public static NSArray findMasquesNatureForExercice(EOEditingContext ec, EOExercice exercice)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.EXERCICE_KEY+"=%@", new NSArray(exercice));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}	

	/**
	 * 
	 * @return
	 */
	public static EOBudgetMasqueNature findMasqueNature(EOEditingContext ec, EOExercice exercice, EOTypeCredit typeCredit, EOPlanComptable planComptable)	{
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.PLAN_COMPTABLE_KEY+"=%@", new NSArray(planComptable)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.EXERCICE_KEY+"=%@", new NSArray(exercice)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			return (EOBudgetMasqueNature)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			return null;
		}
	}
}
