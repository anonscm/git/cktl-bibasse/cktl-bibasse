package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOUtilisateur;
import org.cocktail.bibasse.client.metier.EOUtilisateurFonction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderUtilisateurFonction {

	private static Number TYAP_ID_BUDGET = new Integer(2);
	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findFonctionsForUtilisateur(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice)	{
		
		NSMutableArray args = new NSMutableArray();
		args.addObject(utilisateur);
		args.addObject(TYAP_ID_BUDGET);
		args.addObject(exercice);
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("utilisateur = %@ and fonction.tyapId = %@" +
				" and utilisateurFonctionExercices.exercice = %@", args);
		
		EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateurFonction.ENTITY_NAME, myQualifier, null);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
}
