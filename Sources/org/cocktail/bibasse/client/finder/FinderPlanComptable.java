package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOPlancoCredit;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPlanComptable {
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static EOPlanComptable findCompte(EOEditingContext ec, String numero, EOExercice exercice) {
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY+"=%@", new NSArray(numero)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.EXERCICE_KEY+"=%@", new NSArray(exercice)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return (EOPlanComptable)comptes.objectAtIndex(0);
		}
		catch (Exception e )	{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptesForTypeCredit(EOEditingContext ec, EOTypeCredit typeCredit) {
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_BUDGETAIRE_KEY + " = 'O'", null));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.EXERCICE_KEY+"=%@", new NSArray(typeCredit.exercice())));

			//mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_VALIDITE_KEY + " = 'VALIDE'", null));

			if (EOTypeCredit.TYPE_DEPENSE.equals(typeCredit.tcdType()))	{
				
				if ( "1".equals(typeCredit.tcdSect()) || "3".equals(typeCredit.tcdSect()) )	{
					
					// Pour les types de credit 10,30, 40 ==> Classe 6
					if ("10".equals(typeCredit.tcdCode()) || "30".equals(typeCredit.tcdCode()))
						mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("6*")));
					else {	// Pour les autres 6 et 2
						NSMutableArray mesOrQualifiers = new NSMutableArray();
						mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("6*")));
						mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("2*")));
						mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));
					}	
					
				}
				else	{

					NSMutableArray mesOrQualifiers = new NSMutableArray();
					mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("2*")));
					mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("1*")));
					mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));
					
				}
			}
			else	{
				if ("1".equals(typeCredit.tcdSect()))
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("7*")));							
				else
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY + " like %@", new NSArray("1*")));							
			}
			
			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return comptes;
		}
		catch (Exception e )	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static NSArray findComptesForTypeCreditViaPlancoCredit(EOEditingContext ec, EOTypeCredit typeCredit) {
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();
			
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_BUDGETAIRE_KEY+"='O'", null));
			// On filtre les comptes de niveau 1 (classe)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NIVEAU_KEY+">1", null));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.TYPE_CREDIT_KEY+"=%@", new NSArray(typeCredit)));

			EOFetchSpecification fs = new EOFetchSpecification(EOPlancoCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return (NSArray)comptes.valueForKey(EOPlancoCredit.PLAN_COMPTABLE_KEY);
		}
		catch (Exception e )	{
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray findComptesForSelection(EOEditingContext ec, String classe, EOExercice exercice) {
		
		try { 
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.EXERCICE_KEY+"=%@", new NSArray(exercice)));

			if (classe != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NUM_KEY+" like %@", new NSArray(classe+"*")));
			

			NSMutableArray myOrQualifier = new NSMutableArray();
			myOrQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_VALIDITE_KEY+" = %@", new NSArray("VALIDE")));
			myOrQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlanComptable.PCO_NIVEAU_KEY  + " = 2", null));

			mesQualifiers.addObject(new EOOrQualifier(myOrQualifier));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			fs.setRefreshesRefetchedObjects(true);
			
			NSArray comptes = ec.objectsWithFetchSpecification(fs);
			
			return comptes;
		}
		catch (Exception e )	{
			//e.printStackTrace();
			return new NSArray();
		}
	}
}
