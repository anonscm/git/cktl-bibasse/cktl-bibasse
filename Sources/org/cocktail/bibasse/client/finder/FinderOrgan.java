package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOJefyAdminTypeNatureBudget;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.utils.DateCtrl;
import org.cocktail.common.cofisup.ETypeNatureBudget;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderOrgan {

	/**
	 *
	 * @param ec
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findOrgansFilsForOrgan(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (organ.orgNiveau().intValue() == 1)	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY+"=%@", new NSArray(organ.orgEtab())));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+">1", null));
		}
		else
			if (organ.orgNiveau().intValue() == 2)	{
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY+"=%@", new NSArray(organ.orgUb())));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+">2", null));
			}
			else
				if (organ.orgNiveau().intValue() == 3)	{

					NSMutableArray args = new NSMutableArray();
					args.addObject(organ.orgUb());
					args.addObject(organ.orgCr());

					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY+"=%@ and "+EOOrgan.ORG_CR_KEY+"=%@", args));
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+">3", null));
				}

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.EXERCICE_KEY + " = %@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @param ec
	 * @param ub
	 * @param exercice
	 * @return
	 */
	public static NSArray findOrgansFilsForUb(EOEditingContext ec, String ub, EOExercice exercice)  {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+">2", null));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY+"=%@", new NSArray(ub)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.EXERCICE_KEY + " = %@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @param ec
	 * @param niveau
	 * @return
	 */
	public static NSArray findOrgansForOrganPere(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORGAN_PERE_KEY+"=%@", new NSArray(organ)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.EXERCICE_KEY+"=%@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @param ec
	 * @param exercice
	 * @param niveau
	 * @return
	 */
	public static NSArray rechercherLignesBudgetairesNiveau(EOEditingContext ec, EOExercice exercice, Number niveau)  {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+"=%@", new NSArray(niveau)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.EXERCICE_KEY+"=%@", new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @param ec
	 * @param niveau
	 * @param libelle
	 * @return
	 */
	public static EOOrgan findOrganForNiveauAndLibelle(EOEditingContext ec,Number niveau, String libelle) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+"=%@", new NSArray(niveau)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY+"=%@", new NSArray(libelle)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());

		try {
			return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 *
	 * @param ec
	 * @return
	 */
	public static EOOrgan findOrganRoot(EOEditingContext ec) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+"=0", null));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		try {
			return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 *
	 * @param ec
	 * @param orgId
	 * @return
	 */
	public static EOOrgan findOrganForKey(EOEditingContext ec, Number orgId) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("orgId = %@", new NSArray(orgId)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		try {return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}
	}

	/**
	 *
	 * @param ec
	 * @param niveau
	 * @param organ
	 * @param exercice
	 * @param agent
	 * @return
	 */
	public static EOOrgan findOrganAvecNiveau(EOEditingContext ec, int niveau, EOOrgan organ) {
		NSMutableArray mesQualifiers = new NSMutableArray();

		//       mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("orgDateCloture = nil", null));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+"=%@", new NSArray(new Integer(niveau))));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY+"=%@", new NSArray(organ.orgEtab())));

		if (niveau > 1)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY+"=%@",new NSArray(organ.orgUb())));
		if (niveau > 2)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY+"=%@",new NSArray(organ.orgCr())));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		try {return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e) {return null;}
	}

	/**
	 *
	 * @param ec
	 * @param niveau
	 * @return
	 */
	public static NSArray findOrgansForPilotageBudget(EOEditingContext ec, EOExercice exercice, Number niveau)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+"=%@", new NSArray(niveau)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.EXERCICE_KEY+"=%@",new NSArray(exercice)));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @param ec
	 * @param niveau
	 * @return
	 */
	public static NSArray findOrgansForPilotageBudget(EOEditingContext ec, EOExercice exercice, String etab, String ub, String cr)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY+"=3", null));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_DATE_OUVERTURE_KEY+"<=%@",
				new NSArray(DateCtrl.stringToDate("01/01/"+exercice.exeExercice().intValue()))));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_DATE_CLOTURE_KEY+"=nil or "+EOOrgan.ORG_DATE_CLOTURE_KEY+">=%@",
				new NSArray(DateCtrl.stringToDate("01/01/"+exercice.exeExercice().intValue()))));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	private static NSArray sort() {
		NSMutableArray mySort=new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_ETAB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

		return mySort;
	}
}
