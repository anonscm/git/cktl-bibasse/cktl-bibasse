package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public final class FinderTypeAction {

	/**
	 * Constructeur privé.
	 */
	private FinderTypeAction() {
	}

	/**
	 *
	 * @return
	 */
	public static NSArray findDestinationsBudgetGestion(EOEditingContext ec, EOExercice exercice, Number niveau, EOTypeEtat typeEtat)	{

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("tyacCode", EOSortOrdering.CompareAscending));

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tyacNiveau = %@", new NSArray(niveau)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));

		if (typeEtat != null) {
			mesQualifiers.addObject((EOQualifier.qualifierWithQualifierFormat("toTypeEtat = %@", new NSArray(typeEtat))));
		}

		EOFetchSpecification fs = new EOFetchSpecification(EOTypeAction.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @return
	 */
	public static NSArray findDestinationsBudgetGestionDepense(EOEditingContext ec)	{

		NSArray mySort = new NSArray(new EOSortOrdering("tyacCode", EOSortOrdering.CompareAscending));

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("tyacNiveau = 0  and tyacType = 'DEPENSE'", null);

		EOFetchSpecification fs = new EOFetchSpecification(EOTypeAction.ENTITY_NAME, myQualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 *
	 * @return
	 */
	public static NSArray findDestinationsBudgetGestionRecette(EOEditingContext ec)	{

		NSArray mySort = new NSArray(new EOSortOrdering("tyacCode", EOSortOrdering.CompareAscending));

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("tyacNiveau = 0 and tyacType = 'RECETTE'", null);

		EOFetchSpecification fs = new EOFetchSpecification(EOTypeAction.ENTITY_NAME, myQualifier, mySort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

}
