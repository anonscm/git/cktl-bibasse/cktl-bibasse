package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetVoteGestion;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetVoteGestion {
	
	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsVote(EOEditingContext ec, EOOrgan organ, EOTypeCredit typeCredit, EOTypeAction typeAction, EOExercice exercice) {
		try { 
			NSArray mySort = new NSArray(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareAscending));

			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));
			if (typeAction != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction = %@", new NSArray(typeAction)));
			if (typeCredit == null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit != %@", new NSArray(FinderTypeCredit.findTypesCreditReserve(ec, exercice))));
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(typeCredit)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("bdvgOuverts > 0", null));

			if (typeCredit != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(typeCredit)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetVoteGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param budgetSaisie
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray findBudgetsVoteForVirement(EOEditingContext ec, EOOrgan organ, EOExercice exercice)  {
		try { 
			NSMutableArray mySort = new NSMutableArray();
			mySort.addObject(new EOSortOrdering("typeCredit.tcdType", EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering("typeCredit.tcdSect", EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareAscending));
			mySort.addObject(new EOSortOrdering("typeAction.tyacCode", EOSortOrdering.CompareAscending));

			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ = %@", new NSArray(organ)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit != %@", new NSArray(FinderTypeCredit.findTypesCreditReserve(ec, exercice))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("bdvgOuverts>0 or bdvgVotes>0", null));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetVoteGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
			fs.setRefreshesRefetchedObjects(true);
			NSArray budgets = ec.objectsWithFetchSpecification(fs);
			return budgets;
		}
		catch (Exception e )	{
			return new NSArray();
		}
	}
}
