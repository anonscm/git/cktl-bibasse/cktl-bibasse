package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderTypeCredit {

	
	/**
	 * 
	 * @return
	 */
	public static EOTypeCredit findTypeCreditForCode(EOEditingContext ec, EOExercice exercice, String code)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));

		if (code != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdCode = %@", new NSArray(code)));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		try {return (EOTypeCredit)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
	}
	
	/**
	 * 
	 * @return
	 */
	public static NSArray findTypesCredit(EOEditingContext ec, EOExercice exercice, String section, String type)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		NSArray mySort = new NSArray(new EOSortOrdering("tcdCode", EOSortOrdering.CompareAscending));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdBudget != %@", new NSArray(EOTypeCredit.TYPE_BUDGET_RESERVE)));

		//mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat != %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));

		if (section != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdSect = %@", new NSArray(section)));

		if (type != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdType = %@", new NSArray(type)));

		EOFetchSpecification fs = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	
	/**
	 * 
	 * @return
	 */
	public static NSArray findTypesCreditReserve(EOEditingContext ec, EOExercice exercice)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		NSArray mySort = new NSArray(new EOSortOrdering("tcdCode", EOSortOrdering.CompareAscending));
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdBudget = %@", new NSArray(EOTypeCredit.TYPE_BUDGET_RESERVE)));

		EOFetchSpecification fs = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	
}
