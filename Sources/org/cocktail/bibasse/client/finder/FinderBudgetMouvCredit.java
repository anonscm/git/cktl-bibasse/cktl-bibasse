package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMouvCredit {

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsCredit(EOEditingContext ec,EOOrgan organ, EOExercice exercice)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		if (organ.orgNiveau().intValue() >= 2)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY+"=%@",
					new NSArray(organ.orgUb())));
		if (organ.orgNiveau().intValue() == 1)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_ETAB_KEY+"=%@", 
					new NSArray(organ.orgEtab())));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.TYPE_ETAT_KEY+"=%@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_TRAITE))));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findVentilations(EOEditingContext ec,EOOrgan organ, EOExercice exercice)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (organ.orgNiveau().intValue() == 3)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY+"=%@", 
					new NSArray(organ.orgCr())));
		if (organ.orgNiveau().intValue() == 2)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY+"=%@", 
					new NSArray(organ.orgUb())));
		if (organ.orgNiveau().intValue() == 1)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_ETAB_KEY+"=%@",
					new NSArray(organ.orgEtab())));
		if (organ.orgNiveau().intValue() == 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.ORG_UNIV_KEY+"=%@", 
					new NSArray(organ.orgUniv())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.ORGAN_KEY+"."+EOOrgan.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.MOUVEMENT_KEY+"."+EOBudgetMouvements.TYPE_MOUVEMENT_BUDGETAIRE_KEY+
				"=%@", new NSArray(FinderTypeMouvementBudgetaire.findTypeMouvement(ec, EOTypeMouvementBudgetaire.MOUVEMENT_VENTILATION))));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.TYPE_ETAT_KEY+"=%@", 
				new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_TRAITE))));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		return ec.objectsWithFetchSpecification(fs);
	}

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsCreditForMouvement(EOEditingContext ec, EOBudgetMouvements mouvement)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		try {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.TYPE_ETAT_KEY+"!=%@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvCredit.MOUVEMENT_KEY+"=%@", new NSArray(mouvement)));

			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
}
