package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvNature;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMouvNature {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsNatureForMouvement(EOEditingContext ec, EOBudgetMouvements mouvement)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
						
		try {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("mouvement = %@", new NSArray(mouvement)));
			
			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
			
			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
		
	}

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findMouvementsNature(EOEditingContext ec,EOExercice exercice, EOOrgan organ)	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		if (organ.orgNiveau().intValue() >= 2)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ.orgUb = %@", new NSArray(organ.orgUb())));
		else
			if (organ.orgNiveau().intValue() == 1)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("organ.orgEtab = %@", new NSArray(organ.orgEtab())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_TRAITE))));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
}
