package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOTypeSens;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeSens {

	
	/**
	 * 
	 * 
	 * @param ec
	 * @return
	 */
	public static EOTypeSens findTypeSens(EOEditingContext ec, String sens)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeSens.TYSE_LIBELLE_KEY + " = %@", new NSArray(sens));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeSens.ENTITY_NAME, myQualifier, null);
		
		try {return (EOTypeSens)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);}
		catch (Exception e)	{return null;}
		
	}
	
}
