package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOBudgetCalculGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetSaisie {


	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
		public static EOBudgetSaisie findBudgetSaisieForExercice(EOEditingContext ec, EOExercice exercice, EOTypeSaisie typeSaisie)  {
						
			try { 
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat != %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeSaisie = %@", new NSArray(typeSaisie)));

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
								
				EOFetchSpecification fs = new EOFetchSpecification(EOBudgetSaisie.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				fs.setRefreshesRefetchedObjects(true);
				
				NSArray budgets = ec.objectsWithFetchSpecification(fs);

				return (EOBudgetSaisie)budgets.objectAtIndex(0);
			}
			catch (Exception e )	{
				//e.printStackTrace();
				return null;
			}
		}
		
		
		/**
		 * 
		 * @param ec
		 * @param exercice
		 * @return
		 */
			public static NSArray findBudgetsSaisieForExercice(EOEditingContext ec, EOExercice exercice, EOTypeSaisie typeSaisie)  {
							
				try { 
					
					NSArray sort = new NSArray(new EOSortOrdering(EOBudgetSaisie.BDSA_DATE_CREATION_KEY, EOSortOrdering.CompareDescending));
					
					NSMutableArray mesQualifiers = new NSMutableArray();

					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat != %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));

					if (typeSaisie != null)
						mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeSaisie = %@", new NSArray(typeSaisie)));
					
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
									
					EOFetchSpecification fs = new EOFetchSpecification(EOBudgetSaisie.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort);
					fs.setRefreshesRefetchedObjects(true);
					
					return ec.objectsWithFetchSpecification(fs);
				}
				catch (Exception e )	{
					//e.printStackTrace();
					return new NSArray();
				}
			}
			
			/**
			 * 
			 * @param ec
			 * @param exercice
			 * @return
			 */
				public static EOTypeEtat findTypeEtatForOrgan(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ, boolean niveauSaisieUb)  {
								
					try { 

						if (organ.orgNiveau().intValue() > 2 || 
								(niveauSaisieUb && organ.orgNiveau().intValue()==2) )	{							
							NSArray budgetsGestion = FinderBudgetSaisieGestion.findBudgetsGestion(ec, budgetSaisie, organ, budgetSaisie.exercice());
							return ((EOBudgetSaisieGestion)budgetsGestion.objectAtIndex(0)).typeEtat();
						}
						else	{

							NSArray budgetsCalculGestion = FinderBudgetCalculGestion.findBudgetsGestion(ec, budgetSaisie, organ, budgetSaisie.exercice());

							if (budgetsCalculGestion.count() > 0)
								return ((EOBudgetCalculGestion)budgetsCalculGestion.objectAtIndex(0)).typeEtat();

						}

						return null;
					}
					catch (Exception e )	{
						//e.printStackTrace();
						return null;
					}
				}	
}
