package org.cocktail.bibasse.client.finder;

import org.cocktail.bibasse.client.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class FinderExercice {

	/**
	 * @param ec
	 * @return
	 */
	public static EOExercice findExerciceCourant(EOEditingContext ec)	{
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_STAT_KEY+"='P'", null);
		EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, myQualifier, null);

		try {
			NSArray exercices = ec.objectsWithFetchSpecification(fs);

			if (exercices.count() == 0)	{
				myQualifier = EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_STAT_KEY + "='O'", null);
				fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, myQualifier, null);
				return (EOExercice) ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			}

			return (EOExercice) exercices.objectAtIndex(0);
		} catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @param ec
	 * @return
	 */
	public static NSArray findExercices(EOEditingContext ec)	{
		NSArray sort = new NSArray(new EOSortOrdering(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, null, sort);
		return ec.objectsWithFetchSpecification(fs);
	}

	public static EOExercice exercicePrecedent(EOEditingContext ec, EOExercice exercice) {
		if (exercice == null) {
			return null;
		}

		EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(
				EOExercice.EXE_EXERCICE_KEY + " = %@",
				new NSArray(Integer.valueOf(exercice.exeExercice().intValue() - 1)));
		EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, qualExercice, null);

		NSArray exercices = ec.objectsWithFetchSpecification(fs);
		if (exercices.count() == 0) {
			return null;
		}

		return (EOExercice) exercices.objectAtIndex(0);
	}
}
