package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.factory.FactoryBudgetSaisieNatureLolf;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueCredit;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieNature;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieNatureLolf;
import org.cocktail.bibasse.client.finder.FinderOrgan;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueCredit;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNatureLolf;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Gestion répartition Nature / LOLF.
 */
public class SaisieNatureLolf {

	/**
	 * Singleton instance.
	 */
	private static SaisieNatureLolf sharedInstance;

	private JComboBox    popupTypeCredit;
	private EODisplayGroup 	eodImputation, eodAction, eodTotaux;
	private ZEOTable 		myEOTableImputation, myEOTableAction, myEOTableTotaux;
	private ZEOTableModel 	myTableModelImputation, myTableModelAction, myTableModelTotaux;
	private JScrollPane scrollPaneImputation, scrollPaneAction, scrollPaneTotaux;
	private CellEditor myCellEditor = new CellEditor(new JTextField());

	private ActionInitialiser actionInitialiser=new ActionInitialiser();
	private ActionEnregistrer actionEnregistrer=new ActionEnregistrer();
	private ActionCloturer actionCloturer=new ActionCloturer();

	private NSMutableArray arrayColumnAction;
	private NSMutableArray arrayNatureLolf;

	private	EOBudgetSaisie currentBudgetSaisie;
	private EOOrgan currentOrgan;
	private EOTypeCredit currentTypeCredit;

	private EOEditingContext ec;
	private	ApplicationClient NSApp;
	protected JPanel mainPanel;

	private boolean saisieEnCours;

	/**
	 *
	 *
	 */
	public SaisieNatureLolf(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		eodAction=new EODisplayGroup();
		eodAction.setObjectArray(new NSArray());
		eodImputation=new EODisplayGroup();
		eodImputation.setObjectArray(new NSArray());
		eodTotaux=new EODisplayGroup();
		eodTotaux.setObjectArray(new NSArray());

		currentOrgan=FinderOrgan.findOrganRoot(ec);

		initView();
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static SaisieNatureLolf sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SaisieNatureLolf(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	public void initView()	{
		mainPanel = new JPanel(new BorderLayout());
		JPanel saisiePanel=new JPanel(new BorderLayout());

		popupTypeCredit = new JComboBox();
		popupTypeCredit.setFocusable(false);
		popupTypeCredit.setBackground(new Color(100, 100, 100));
		popupTypeCredit.setForeground(new Color(0,0,0));
		popupTypeCredit.addActionListener(new ListenerPopupTypeCredit());

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eodImputation, "EO_PLAN_COMPTABLE", "Imputation", 100);
		col1.setAlignment(SwingConstants.LEFT);
		col1.setEditable(false);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodImputation, "TOTALNAT", "Total Nature", 30);
		col2.setAlignment(SwingConstants.RIGHT);
		col2.setEditable(false);
		col2.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col2.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(eodImputation, "TOTAL", "Total Saisie", 30);
		col3.setAlignment(SwingConstants.RIGHT);
		col3.setEditable(false);
		col3.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col3.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col3.setTableCellRenderer(new ImputationRenderer());

		Vector myCols = new Vector();
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);

		myTableModelImputation = new ZEOTableModel(eodImputation, myCols);

		myEOTableImputation = new ZEOTable(myTableModelImputation);

		saisiePanel.add(popupTypeCredit, BorderLayout.NORTH);
		scrollPaneImputation=new JScrollPane(myEOTableImputation, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPaneImputation.setPreferredSize(new Dimension(300,20));
		scrollPaneImputation.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListenerImputation());
		saisiePanel.add(scrollPaneImputation, BorderLayout.WEST);

		myTableModelAction = new ZEOTableModel(eodAction, new Vector());
		myTableModelAction.addTableModelListener(new ListenerAction());
		myEOTableAction = new ZEOTable(myTableModelAction);
		myEOTableAction.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		myEOTableAction.getTableHeader().setReorderingAllowed(false);
		myEOTableAction.getTableHeader().setResizingAllowed(false);

		scrollPaneAction=new JScrollPane(myEOTableAction, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPaneAction.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListenerActionVertical());
		scrollPaneAction.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListenerActionHorizontal());
		saisiePanel.add(scrollPaneAction, BorderLayout.CENTER);


		myTableModelTotaux = new ZEOTableModel(eodTotaux, new Vector());
		myEOTableTotaux= new ZEOTable(myTableModelTotaux);
		myEOTableTotaux.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		myEOTableTotaux.getTableHeader().setReorderingAllowed(false);
		myEOTableTotaux.getTableHeader().setResizingAllowed(false);

		scrollPaneTotaux=new JScrollPane(myEOTableTotaux, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPaneTotaux.setPreferredSize(new Dimension(10,70));
		scrollPaneTotaux.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListenerTotaux());

		JPanel viewAjust=new JPanel(), panelSouth=new JPanel(new BorderLayout());
		viewAjust.setPreferredSize(new Dimension((int)scrollPaneImputation.getPreferredSize().getWidth(), 0));
		saisiePanel.remove(panelSouth);
		panelSouth.add(viewAjust, BorderLayout.WEST);
		panelSouth.add(scrollPaneTotaux, BorderLayout.CENTER);
		saisiePanel.add(panelSouth, BorderLayout.SOUTH);

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionInitialiser);
		arrayList.add(actionEnregistrer);
		arrayList.add(actionCloturer);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 110, 23));
		JPanel south=new JPanel(new BorderLayout());
		south.add(new JPanel(), BorderLayout.CENTER);
		south.add(panelButtons, BorderLayout.EAST);

		mainPanel.add(saisiePanel, BorderLayout.CENTER);
		mainPanel.add(south, BorderLayout.SOUTH);
	}

	private class AdjustmentListenerActionVertical implements AdjustmentListener {
		AdjustmentListenerActionVertical() { super(); }
		public void adjustmentValueChanged(AdjustmentEvent e) {
			scrollPaneImputation.getVerticalScrollBar().setValue(e.getValue());
		}
	}
	private class AdjustmentListenerActionHorizontal implements AdjustmentListener {
		AdjustmentListenerActionHorizontal() { super(); }
		public void adjustmentValueChanged(AdjustmentEvent e) {
			scrollPaneTotaux.getHorizontalScrollBar().setValue(e.getValue());
		}
	}
	private class AdjustmentListenerTotaux implements AdjustmentListener {
		AdjustmentListenerTotaux() { super(); }
		public void adjustmentValueChanged(AdjustmentEvent e) {
			scrollPaneAction.getHorizontalScrollBar().setValue(e.getValue());
		}
	}
	private class AdjustmentListenerImputation implements AdjustmentListener {
		AdjustmentListenerImputation() { super(); }
		public void adjustmentValueChanged(AdjustmentEvent e) {
			scrollPaneAction.getVerticalScrollBar().setValue(e.getValue());
		}
	}

	public class ListenerAction implements TableModelListener 	{
		public void tableChanged(TableModelEvent e) {
			updateTotal(e.getFirstRow(), e.getColumn());
		}
	}

	private final class ActionInitialiser extends AbstractAction {
		public ActionInitialiser() {
			super("Initialiser");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_INIT_BUDGET);
		}
		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(mainPanel);
			initialiser();
			NSApp.setDefaultCursor(mainPanel);
		}
	}

	private final class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SAVE);
		}
		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(mainPanel);
			enregistrer();
			NSApp.setDefaultCursor(mainPanel);
		}
	}

	private final class ActionCloturer extends AbstractAction {
		public ActionCloturer() {
			super("Cloturer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CONTROLE_BUDGET);
		}
		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(mainPanel);
			cloturer();
			NSApp.setDefaultCursor(mainPanel);
		}
	}

	/**
	 *
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}

	public void initialiser() {
		System.out.println("currentBudgetSaisie:"+currentBudgetSaisie);
		if (currentBudgetSaisie==null)
			return;

		try {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
			ServerProxy.clientSideRequestInitialiserNatureLolf(ec, parametres);
			changeTypeCredit(currentTypeCredit);
		}
		catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR", NSApp.getMessageFormatte(ex.getMessage()));
		}
	}

	public void enregistrer() {

		try {

			for (int i=0; i<eodAction.displayedObjects().count(); i++) {
				NSDictionary row=(NSDictionary)eodAction.displayedObjects().objectAtIndex(i);

				for (int j=0; j<arrayColumnAction.count(); j++) {
					EOTypeAction typeAction=(EOTypeAction)arrayColumnAction.objectAtIndex(j);
					EOTypeCredit typeCredit=(EOTypeCredit)row.objectForKey("EO_TYPE_CREDIT");
					EOPlanComptable planComptable=(EOPlanComptable)row.objectForKey("EO_PLAN_COMPTABLE");

					NSArray filtres=EOQualifier.filteredArrayWithQualifier(arrayNatureLolf,
							EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY+"=%@ and "+
									EOBudgetSaisieNatureLolf.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY+"=%@",
									new NSArray(new Object[]{typeAction, typeCredit, planComptable})));

					EOBudgetSaisieNatureLolf budget;
					BigDecimal saisie=new BigDecimal(StringCtrl.replace(row.objectForKey(((EOTypeAction)arrayColumnAction.objectAtIndex(j)).tyacCode()+" "+
							((EOTypeAction)arrayColumnAction.objectAtIndex(j)).tyacLibelle()).toString(),
							",", "."));

					if (filtres!=null && filtres.count()>0) {
						budget=(EOBudgetSaisieNatureLolf)filtres.objectAtIndex(0);
						if (saisie.floatValue()!=budget.bdslSaisi().floatValue()) {
							budget.setBdslSaisi(saisie);
							budget.setBdslMontant(budget.bdslVote().add(budget.bdslSaisi()));
						}
					} else {
						if (saisie.floatValue()!=0.0) {
							budget=new FactoryBudgetSaisieNatureLolf().creerEOBudgetSaisieNatureLolf(ec, FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
									typeCredit, planComptable, typeAction, currentOrgan, typeCredit.exercice(), currentBudgetSaisie, new BigDecimal(0.0), saisie, saisie);
							arrayNatureLolf.addObject(budget);
						}
					}
				}
			}

			ec.saveChanges();
		}
		catch (Exception e)	{
			EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement du budget !");
			e.printStackTrace();
		}

	}

	public void cloturer() {
		if (currentTypeCredit==null || currentBudgetSaisie==null) {
		    System.out.println("CLOTURER NATURE LOLF, ERREUR : bdsaid "+currentBudgetSaisie+", tcdordre "+currentTypeCredit);
			EODialogs.runErrorDialog("ERREUR", "Probleme d'application l'un des parametres est null.");
			return;
		}

		enregistrer();

		try {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
			parametres.setObjectForKey((EOEnterpriseObject)currentTypeCredit, "EOTypeCredit");
			ServerProxy.clientSideRequestCloturerNatureLolf(ec, parametres);

			NSMutableArray array=new NSMutableArray();
			for (int i=0; i<arrayNatureLolf.count(); i++)
				array.addObject(ec.globalIDForObject((EOBudgetSaisieNatureLolf)arrayNatureLolf.objectAtIndex(0)));
			ec.invalidateObjectsWithGlobalIDs(array);
			changeTypeCredit(currentTypeCredit);
		}
		catch (Exception ex) {
			EODialogs.runErrorDialog("ERREUR", NSApp.getMessageFormatte(ex.getMessage()));
		}
	}


	/**
	 *
	 *
	 */
	public void clean()	{
		setImputations(new NSArray());
		setActions(new NSArray());
		arrayNatureLolf=new NSMutableArray();
	}

	private void setImputations(NSArray array) {
		eodImputation.setObjectArray(array);
		myEOTableImputation.updateData();
		myEOTableImputation.updateUI();
		myTableModelImputation.fireTableDataChanged();
	}

	private void setActions(NSArray array) {
		eodAction.setObjectArray(array);
		if (myEOTableAction!=null) {
			myEOTableAction.updateData();
			myEOTableAction.updateUI();
		}
	}
	private void setTotaux(NSArray array) {
		eodTotaux.setObjectArray(array);
		if (myEOTableTotaux!=null) {
			myEOTableTotaux.updateData();
			myEOTableTotaux.updateUI();
		}
	}

	public void setBudgetSaisie(EOBudgetSaisie budget)	{
		currentBudgetSaisie = budget;
		if (currentBudgetSaisie==null) {
			clean();
			return;
		}

		NSArray arrayTypeCredit=FinderBudgetMasqueCredit.findMasqueCreditTrieTypeCredit(ec, currentBudgetSaisie.exercice());
		popupTypeCredit.removeAllItems();
		for (int i=0; i<arrayTypeCredit.count(); i++)
			popupTypeCredit.addItem(((EOBudgetMasqueCredit)arrayTypeCredit.objectAtIndex(i)).typeCredit());
	}

	private class ListenerPopupTypeCredit implements ActionListener {
		public ListenerPopupTypeCredit() {super();}

		public void actionPerformed(ActionEvent anAction) {
			if (ec.hasChanges()) {
				if (EODialogs.runConfirmOperationDialog("Attention",
						"Vous avez saisi des montants, souhaitez vous les enregistrer avant de changer de type de crédit ?","OUI", "NON"))
					enregistrer();
			}

			if (popupTypeCredit.getSelectedIndex()>= 0)
				changeTypeCredit((EOTypeCredit)popupTypeCredit.getSelectedItem());
			else
				changeTypeCredit(null);
		}
	}

	private void changeTypeCredit(EOTypeCredit typeCredit) {
		currentTypeCredit = typeCredit;
		if (typeCredit == null || currentBudgetSaisie == null) {
			clean();
			return;
		}

		arrayNatureLolf = new NSMutableArray(FinderBudgetSaisieNatureLolf.findBudgetsNatureLolf(ec, currentBudgetSaisie, typeCredit));

		if (arrayNatureLolf!=null && arrayNatureLolf.count()>0 &&
				((EOBudgetSaisieNatureLolf)arrayNatureLolf.objectAtIndex(0)).typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_EN_COURS) &&
				currentBudgetSaisie.typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_EN_COURS))
			saisieEnCours=true;
		else
			saisieEnCours=false;

		actionEnregistrer.setEnabled(saisieEnCours);
		actionCloturer.setEnabled(saisieEnCours);

		if (currentBudgetSaisie.typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_EN_COURS) && !saisieEnCours && arrayNatureLolf.count()==0)
			actionInitialiser.setEnabled(true);
		else
			actionInitialiser.setEnabled(false);

		NSMutableArray arraySansDoublonsImputation=new NSMutableArray();
		NSMutableArray arraySansDoublonsAction=new NSMutableArray();
		for (int i=0; i<arrayNatureLolf.count(); i++) {
			EOBudgetSaisieNatureLolf item=(EOBudgetSaisieNatureLolf)arrayNatureLolf.objectAtIndex(i);

			NSArray filtres=EOQualifier.filteredArrayWithQualifier(arraySansDoublonsImputation,
					EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY+"=%@", new NSArray(item.planComptable())));
			if (filtres.count()==0)
				arraySansDoublonsImputation.addObject(item);

			filtres=EOQualifier.filteredArrayWithQualifier(arraySansDoublonsAction,
					EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY+"=%@", new NSArray(item.typeAction())));
			if (filtres.count()==0)
				arraySansDoublonsAction.addObject(item);
		}


		NSMutableArray larray = new NSMutableArray();
		NSMutableDictionary myRecord;
		NSArray arrayTotal = FinderBudgetSaisieNature.findBudgetsNature(ec, currentBudgetSaisie, typeCredit);
		BigDecimal totalnat = new BigDecimal(0.0), totalgest = new BigDecimal(0.0);

		for (int i = 0; i < arraySansDoublonsImputation.count(); i++) {
			EOBudgetSaisieNatureLolf budgetNatureLolf = (EOBudgetSaisieNatureLolf) arraySansDoublonsImputation.objectAtIndex(i);

			myRecord = new NSMutableDictionary();

			myRecord.setObjectForKey(budgetNatureLolf.typeCredit(), "EO_TYPE_CREDIT");
			myRecord.setObjectForKey(budgetNatureLolf.planComptable(), "EO_PLAN_COMPTABLE");

			myRecord.setObjectForKey(budgetNatureLolf.bdslVote(), "VOTE");
			myRecord.setObjectForKey(budgetNatureLolf.bdslSaisi(), "SAISI");

			myRecord.setObjectForKey(computeSumForKey(EOQualifier.filteredArrayWithQualifier(arrayTotal,
					EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.PCO_NUM_VOTE_KEY+"=%@",
							new NSArray(budgetNatureLolf.planComptable().pcoNum()))), EOBudgetSaisieNature.BDSN_SAISI_KEY),
			"TOTALNAT");
			myRecord.setObjectForKey(new BigDecimal(0.0), "TOTAL");

			totalnat = totalnat.add((BigDecimal) myRecord.objectForKey("TOTALNAT"));
			larray.addObject(myRecord);
		}
		setImputations(larray);


		Vector myCols = new Vector(), myColsTotaux=new Vector();
		NSMutableDictionary recordTotauxSaisie=new NSMutableDictionary();
		NSMutableDictionary recordTotauxVote=new NSMutableDictionary();
		arrayColumnAction=new NSMutableArray();
		arrayTotal=FinderBudgetSaisieGestion.findBudgetsGestion(ec, currentBudgetSaisie, typeCredit);

		for(int i=0; i<arraySansDoublonsAction.count(); i++) {
			EOBudgetSaisieNatureLolf budgetNatureLolf = (EOBudgetSaisieNatureLolf)arraySansDoublonsAction.objectAtIndex(i);

			ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodAction, budgetNatureLolf.typeAction().tyacCode()+" "+budgetNatureLolf.typeAction().tyacLibelle(),
					budgetNatureLolf.typeAction().tyacCode()+" "+budgetNatureLolf.typeAction().tyacLibelle(), 70);
			arrayColumnAction.addObject(budgetNatureLolf.typeAction());
			col2.setAlignment(SwingConstants.RIGHT);
			col2.setColumnClass(BigDecimal.class);
			col2.setEditable(saisieEnCours);
			col2.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
			col2.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
			col2.setPreferredWidth(100);
			if (saisieEnCours)
				col2.setTableCellEditor(myCellEditor);

			myCols.add(col2);

			ZEOTableModelColumn colTotaux = new ZEOTableModelColumn(eodTotaux, budgetNatureLolf.typeAction().tyacCode()+" "+
					budgetNatureLolf.typeAction().tyacLibelle(), null, 70);
			colTotaux.setAlignment(SwingConstants.RIGHT);
			colTotaux.setColumnClass(BigDecimal.class);
			colTotaux.setEditable(false);
			colTotaux.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
			colTotaux.setPreferredWidth(100);
			colTotaux.setTableCellRenderer(new TotauxRenderer());

			recordTotauxSaisie.setObjectForKey(new BigDecimal(0.0), budgetNatureLolf.typeAction().tyacCode()+" "+budgetNatureLolf.typeAction().tyacLibelle());
			recordTotauxVote.setObjectForKey(computeSumForKey(EOQualifier.filteredArrayWithQualifier(arrayTotal,
					EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieGestion.TYPE_ACTION_KEY+"=%@",
							new NSArray(budgetNatureLolf.typeAction()))),EOBudgetSaisieGestion.BDSG_SAISI_KEY),
							budgetNatureLolf.typeAction().tyacCode()+" "+budgetNatureLolf.typeAction().tyacLibelle());

			totalgest=totalgest.add((BigDecimal)recordTotauxVote.objectForKey(budgetNatureLolf.typeAction().tyacCode()+" "+budgetNatureLolf.typeAction().tyacLibelle()));

			myColsTotaux.add(colTotaux);
		}

		System.out.println("total nat  : "+totalnat);
		System.out.println("total gest : "+totalgest);

		myTableModelAction = new ZEOTableModel(eodAction, myCols);
		myTableModelAction.addTableModelListener(new ListenerAction());
		myEOTableAction.setModel(myTableModelAction);

		eodTotaux.setObjectArray(new NSArray(new Object[]{recordTotauxSaisie, recordTotauxVote}));
		myTableModelTotaux = new ZEOTableModel(eodTotaux, myColsTotaux);
		myEOTableTotaux.setModel(myTableModelTotaux);


		larray=new NSMutableArray();
		for (int i=0; i<arraySansDoublonsImputation.count(); i++) {
			myRecord = new NSMutableDictionary();
			EOBudgetSaisieNatureLolf imputation=(EOBudgetSaisieNatureLolf)arraySansDoublonsImputation.objectAtIndex(i);

			for (int j=0; j<arraySansDoublonsAction.count(); j++) {
				EOBudgetSaisieNatureLolf action=(EOBudgetSaisieNatureLolf)arraySansDoublonsAction.objectAtIndex(j);

				NSArray filtres=EOQualifier.filteredArrayWithQualifier(arrayNatureLolf,
						EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY+"=%@ and "+
								EOBudgetSaisieNatureLolf.TYPE_CREDIT_KEY+"=%@ and "+
								EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY+"=%@",
								new NSArray(new Object[] {imputation.planComptable(), typeCredit, action.typeAction()})));

				myRecord.setObjectForKey(typeCredit, "EO_TYPE_CREDIT");
				myRecord.setObjectForKey(imputation.planComptable(), "EO_PLAN_COMPTABLE");
				if (filtres!=null && filtres.count()>0)
					myRecord.setObjectForKey(((EOBudgetSaisieNatureLolf)filtres.objectAtIndex(0)).bdslSaisi(), action.typeAction().tyacCode()+
							" "+action.typeAction().tyacLibelle());
				else
					myRecord.setObjectForKey(new BigDecimal(0.0), action.typeAction().tyacCode()+" "+action.typeAction().tyacLibelle());
			}

			larray.addObject(myRecord);
		}
		setActions(larray);

		for (int i=0; i<arraySansDoublonsImputation.count(); i++)
			updateTotal(i, 0);
		for (int j=0; j<arraySansDoublonsAction.count(); j++)
			updateTotal(0, j);
	}

	public void updateTotal(int row, int column) {
		if (eodAction==null || arrayColumnAction==null || row>=eodAction.displayedObjects().count() || row<0 || column<0 || column>=arrayColumnAction.count())
			return;

		BigDecimal montant=new BigDecimal(0.0);
		try {
			for (int i=0; i<arrayColumnAction.count(); i++)
				montant=montant.add(new BigDecimal(((NSDictionary)eodAction.displayedObjects().objectAtIndex(row)).objectForKey(((EOTypeAction)arrayColumnAction.objectAtIndex(i)).tyacCode()+
						" "+((EOTypeAction)arrayColumnAction.objectAtIndex(i)).tyacLibelle()).toString()));
			eodImputation.setValueForObjectAtIndex(montant, row, "TOTAL");
			setImputations(eodImputation.displayedObjects());
		}
		catch (Exception e) {}

		montant=new BigDecimal(0.0);
		try {
			for (int i=0; i<eodAction.displayedObjects().count(); i++)
				montant=montant.add(new BigDecimal(((NSDictionary)eodAction.displayedObjects().objectAtIndex(i)).objectForKey(((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacCode()+
						" "+((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacLibelle()).toString()));
			eodTotaux.setValueForObjectAtIndex(montant, 0, ((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacCode()+" "+
					((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacLibelle());
			setTotaux(eodTotaux.displayedObjects());
		}
		catch (Exception e) {}
	}

	private final class CellEditor extends ZEOTableModelColumn.ZEONumFieldTableCellEditor {
		private  JTextField myTextField;

		public CellEditor(JTextField textField) {
			super(textField, ConstantesCocktail.FORMAT_DECIMAL);
		}
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			myTextField = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);

			/*if (currentBudgetSaisie.isVote()
					|| (currentTypeEtatSaisie != null && currentTypeEtatSaisie.isCloture())
					|| !NSApp.fonctionSaisie()
					|| (currentTypeEtatSaisie.isValide() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
					|| (currentTypeEtatSaisie.isControle() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
					|| (currentOrgan.orgNiveau().intValue() < 3  && NSApp.isSaisieCr())
					|| (currentOrgan.orgNiveau().intValue() < 2  && NSApp.isSaisieUb())
					)
				return null;
			else	{
				myTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
				myTextField.setEditable(NSApp.fonctionSaisie());
			}   */
			return myTextField;
		}
	}

	private class ImputationRenderer extends ZEOTableCellRenderer {
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			//Tester la valeur
			BigDecimal montantTotalNat= (BigDecimal)((NSDictionary)eodImputation.displayedObjects().objectAtIndex(row)).objectForKey("TOTALNAT");
			BigDecimal montantTotal= (BigDecimal)((NSDictionary)eodImputation.displayedObjects().objectAtIndex(row)).objectForKey("TOTAL");

			leComposant.setForeground(Color.BLACK);
			if (montantTotalNat.floatValue()==montantTotal.floatValue())
				leComposant.setBackground(Color.GREEN);
			else
				leComposant.setBackground(Color.RED);
			return leComposant;
		}
	}

	private class TotauxRenderer extends ZEOTableCellRenderer {
		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			leComposant.setForeground(Color.BLACK);

			try {
				//Tester la valeur
				BigDecimal montantTotalAct= (BigDecimal)((NSDictionary)eodTotaux.displayedObjects().objectAtIndex(0)).objectForKey(((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacCode()+
						" "+((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacLibelle());
				BigDecimal montantTotal= (BigDecimal)((NSDictionary)eodTotaux.displayedObjects().objectAtIndex(1)).objectForKey(((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacCode()+
						" "+((EOTypeAction)arrayColumnAction.objectAtIndex(column)).tyacLibelle());

				if (montantTotalAct.floatValue()==montantTotal.floatValue())
					leComposant.setBackground(Color.GREEN);
				else
					leComposant.setBackground(Color.RED);
			} catch (Exception e) {
				leComposant.setBackground(Color.GREEN);
			}
			return leComposant;
		}
	}

	protected BigDecimal computeSumForKey(NSArray eo, String key) {
		if (eo==null || eo.count()==0)
			return new BigDecimal(0.0);
		return (BigDecimal)eo.valueForKeyPath("@sum."+key);
	}
}
