package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.BudgetSyntheseCtrl;
import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.factory.FactoryBudgetSaisieNature;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueNature;
import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetCalculNature;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel.IZEOTableModelProvider;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SaisieBudgetNatureDepenses {

	private static SaisieBudgetNatureDepenses sharedInstance;
	private 	EOEditingContext ec;
	private		ApplicationClient NSApp;
	protected 	JPanel 	mainPanel;

	protected	JPanel 	viewTableS1, viewTableS2, viewTableSousS1, viewTableSousS2;

	private EODisplayGroup 	eodSection1;
	private ZEOTable 		myEOTableS1;
	private ZEOTableModel 	myTableModelS1;
	private TableSorter 	myTableSorterS1;

	private	EODisplayGroup	eodSection2;
	private ZEOTable 		myEOTableS2;
	private ZEOTableModel 	myTableModelS2;
	private TableSorter 	myTableSorterS2;

	private	EODisplayGroup	eodSousS1;
	private ZEOTable 		myEOTableSousS1;
	private ZEOTableModel 	myTableModelSousS1;
	private TableSorter 	myTableSorterSousS1;

	private	EODisplayGroup	eodSousS2;
	private ZEOTable 		myEOTableSousS2;
	private ZEOTableModel 	myTableModelSousS2;
	private TableSorter 	myTableSorterSousS2;

	private RendererS1		myRendererS1 = new RendererS1();
	private RendererS2		myRendererS2 = new RendererS2();
	private RendererSousSection	myRendererSousSection = new RendererSousSection();

	private ListenerS1 myListenerS1 = new ListenerS1();
	private ListenerS2 myListenerS2 = new ListenerS2();

	private CellEditor myCellEditor = new CellEditor(new JTextField());

	private EOOrgan currentOrgan;
	private EOTypeEtat currentTypeEtatSaisie;
	private	EOBudgetSaisie currentBudgetSaisie;

	private NSMutableArray budgetsNature = new NSMutableArray();

	private BigDecimal total;

	private JTextField	totalTf;
	private	JLabel sousTotalS1, sousTotalS2, sousTotalSaisiS1, sousTotalSaisiS2, totalS1, totalS2;

	protected	NSMutableArray comptesSection1, comptesSection2;

	/**
	 *
	 *
	 */
	public SaisieBudgetNatureDepenses(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		initData();
		initView();
		initGUI();
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static SaisieBudgetNatureDepenses sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SaisieBudgetNatureDepenses(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	public void initView()	{

		mainPanel = new JPanel(new BorderLayout());

		// Init Views
		viewTableS1 = new JPanel();
		viewTableS2 = new JPanel();
		viewTableSousS1 = new JPanel();
		viewTableSousS2 = new JPanel();

		// Init Text Fields
		sousTotalS1 = new JLabel("0,00");
		sousTotalS1.setForeground(new Color(0,0,255));

		totalS1 = new JLabel("0,00");
		totalS1.setForeground(new Color(0,0,255));
		totalS1.setFont(new Font("Arial", Font.BOLD, 12));

		sousTotalS2 = new JLabel("0,00");
		sousTotalS2.setForeground(new Color(0,0,255));

		totalS2 = new JLabel("0,00");
		totalS2.setForeground(new Color(0,0,255));
		totalS2.setFont(new Font("Arial", Font.BOLD, 12));

		sousTotalSaisiS1 = new JLabel("0,00");
		sousTotalSaisiS1.setForeground(new Color(100,100,100));

		sousTotalSaisiS2 = new JLabel("0,00");
		sousTotalSaisiS2.setForeground(new Color(100,100,100));

		totalTf = new JTextField();
		totalTf.setFont(Configuration.instance().informationTitreTableauFont(ec));
		totalTf.setEditable(false);
		totalTf.setBorder(BorderFactory.createEmptyBorder());
		totalTf.setBackground(new Color(80,130,145));
		totalTf.setForeground(new Color(255,255,255));
		totalTf.setText("TOTAL DEPENSES : 0,00 \u20ac");
		totalTf.setHorizontalAlignment(0);


		// SECTION 1

		// Panel SOUS TOTAL SECTION 1 : ...
		JLabel labelSousTotalS1 = new JLabel("SOUS TOTAL 1ère SECTION : ");
		labelSousTotalS1.setForeground(new Color(0,0,255));
		JLabel labelEuro1 = new JLabel("\u20ac");
		JLabel labelEuro1Saisi = new JLabel("\u20ac");
		labelEuro1Saisi.setForeground(new Color(100,100,100));
		labelEuro1.setForeground(new Color(0,0,255));


		JPanel panelLabelSousTotalS1 = new JPanel(new FlowLayout());
		panelLabelSousTotalS1.add(labelSousTotalS1);
		panelLabelSousTotalS1.add(sousTotalSaisiS1);
		panelLabelSousTotalS1.add(labelEuro1Saisi);
		panelLabelSousTotalS1.add(new JLabel(" | "));
		panelLabelSousTotalS1.add(sousTotalS1);
		panelLabelSousTotalS1.add(labelEuro1);
		panelLabelSousTotalS1.setBorder(BorderFactory.createEmptyBorder());

		JPanel panelSousTotalS1 = new JPanel(new BorderLayout());
		panelSousTotalS1.add(panelLabelSousTotalS1, BorderLayout.EAST);

		// PANEL SOUS SECTION 1
		JPanel panelSousSection1 = new JPanel(new BorderLayout());
		panelSousSection1.setPreferredSize(new Dimension(viewTableS1.getWidth(), 18));
		panelSousSection1.add(viewTableSousS1, BorderLayout.CENTER);

		// Panel TOTAL SECTION 1 : ...
		JLabel labelTotalS1 = new JLabel("TOTAL 1ère SECTION : ");
		labelTotalS1.setFont(new Font("Arial", Font.BOLD, 12));
		JLabel labelEuroTotalS1 = new JLabel(" \u20ac");
		labelEuroTotalS1.setForeground(new Color(0,0,255));
		labelEuroTotalS1.setFont(new Font("Arial", Font.BOLD, 12));

		labelTotalS1.setForeground(new Color(0,0,255));

		JPanel panelLabelTotalS1 = new JPanel(new FlowLayout());
		panelLabelTotalS1.add(labelTotalS1);
		panelLabelTotalS1.add(totalS1);
		panelLabelTotalS1.add(labelEuroTotalS1);

		JPanel panelTotalS1 = new JPanel(new BorderLayout());
		panelTotalS1.add(panelLabelTotalS1, BorderLayout.EAST);

		// BILAN S1 : Sous Total + Excedent + TOTAL S1

		ArrayList arrayList = new ArrayList();
		arrayList.add(panelSousTotalS1);
		arrayList.add(panelSousSection1);
		arrayList.add(panelTotalS1);
		Component panelBilanS1 = ZUiUtil.buildBoxColumn(arrayList);

		// PANEL SECTION 1
		JPanel panelSection1 = new JPanel(new BorderLayout());
		panelSection1.add(viewTableS1, BorderLayout.CENTER);
		panelSection1.add(panelBilanS1, BorderLayout.SOUTH);


		// SECTION 2

		// Panel SOUS TOTAL SECTION 2 : ...
		JLabel labelSousTotalS2 = new JLabel("SOUS TOTAL 2ème SECTION : ");
		labelSousTotalS2.setForeground(new Color(0,0,255));
		JLabel labelEuro2Saisi = new JLabel("\u20ac");
		labelEuro2Saisi.setForeground(new Color(100,100,100));
		JLabel labelEuro2 = new JLabel("\u20ac");
		labelEuro2.setForeground(new Color(0,0,255));


		JPanel panelLabelSousTotalS2 = new JPanel(new FlowLayout());
		panelLabelSousTotalS2.add(labelSousTotalS2);
		panelLabelSousTotalS2.add(sousTotalSaisiS2);
		panelLabelSousTotalS2.add(labelEuro2Saisi);
		panelLabelSousTotalS2.add(new JLabel(" | "));
		panelLabelSousTotalS2.add(sousTotalS2);
		panelLabelSousTotalS2.add(labelEuro2);

		JPanel panelSousTotalS2 = new JPanel(new BorderLayout());
		panelSousTotalS2.add(panelLabelSousTotalS2, BorderLayout.EAST);

		// PANEL SOUS SECTION 2
		JPanel panelSousSection2 = new JPanel(new BorderLayout());
		panelSousSection2.setPreferredSize(new Dimension(viewTableS2.getWidth(), 18));
		panelSousSection2.add(viewTableSousS2, BorderLayout.CENTER);

		// Panel TOTAL SECTION 2 : ...

		JLabel labelTotalS2 = new JLabel("TOTAL 2ème SECTION : ");
		labelTotalS2.setFont(new Font("Arial", Font.BOLD, 12));
		JLabel labelEuroTotalS2 = new JLabel(" \u20ac");
		labelEuroTotalS2.setForeground(new Color(0,0,255));
		labelEuroTotalS2.setFont(new Font("Arial", Font.BOLD, 12));

		labelTotalS2.setForeground(new Color(0,0,255));

		JPanel panelLabelTotalS2 = new JPanel(new FlowLayout());
		panelLabelTotalS2.add(labelTotalS2);
		panelLabelTotalS2.add(totalS2);
		panelLabelTotalS2.add(labelEuroTotalS2);

		JPanel panelTotalS2 = new JPanel(new BorderLayout());
		panelTotalS2.add(panelLabelTotalS2, BorderLayout.EAST);

		// SPLIT PANE BILAN S2 : Sous Total + Excedent + TOTAL S2
		arrayList = new ArrayList();
		arrayList.add(panelSousTotalS2);
		arrayList.add(panelSousSection2);
		arrayList.add(panelTotalS2);
		Component panelBilanS2 = ZUiUtil.buildBoxColumn(arrayList);

		// PANEL SECTION 2
		JPanel panelSection2 = new JPanel(new BorderLayout());
		panelSection2.add(viewTableS2, BorderLayout.CENTER);
		panelSection2.add(panelBilanS2, BorderLayout.SOUTH);


		// SPLIT PANE SETION 1 + SECTION 2
		JSplitPane splitPane = ZUiUtil.buildVerticalSplitPane(panelSection1, panelSection2, 0.65, 0.5);
		mainPanel.add(splitPane, BorderLayout.CENTER);

		// TOTAL DEPENSES
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(totalTf, BorderLayout.CENTER);

		mainPanel.add(panelSouth, BorderLayout.SOUTH);
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	}

	/**
	 *
	 *
	 */
	public void initData()	{
		comptesSection1 = new NSMutableArray(FinderBudgetMasqueNature.findMasqueForInit(ec, "1", NSApp.getExerciceBudgetaire(), "DEPENSE"));
		comptesSection1.addObjectsFromArray(FinderBudgetMasqueNature.findMasqueForInit(ec, "3", NSApp.getExerciceBudgetaire(), "DEPENSE"));
		comptesSection2 = new NSMutableArray(FinderBudgetMasqueNature.findMasqueForInit(ec, "2", NSApp.getExerciceBudgetaire(), "DEPENSE"));
	}

	/**
	 *
	 * @return
	 */
	public NSArray getMasqueNature()	{
		NSMutableArray masques = new NSMutableArray(comptesSection1);
		masques.addObjectsFromArray(comptesSection2);

		return masques;
	}

	/**
	 *
	 *
	 */
	public void clean()	{
		eodSection1.setObjectArray(new NSArray());
		eodSection2.setObjectArray(new NSArray());
		eodSousS1.setObjectArray(new NSArray());
		eodSousS2.setObjectArray(new NSArray());

		myEOTableS1.updateData();
		myEOTableS2.updateData();
		myEOTableSousS1.updateData();
		myEOTableSousS2.updateData();
	}

	/**
	 *
	 * @param organ
	 */
	public void setOrgan(EOOrgan organ)	{
		currentOrgan = organ;
	}

	/**
	 *
	 * @param budget
	 */
	public void setBudgetSaisie(EOBudgetSaisie budget)	{
		currentBudgetSaisie = budget;
	}

	public void setTypeEtatSaisie(EOTypeEtat etat)	{
		currentTypeEtatSaisie = etat;
	}

	/**
	 *
	 *
	 */
	public void loadSection(EODisplayGroup eod, int section)	{

		NSMutableArray myRecordArray = new NSMutableArray();
		NSMutableDictionary myRecord = new NSMutableDictionary();

		NSArray filtres = new NSArray(), filtresMasque=new NSArray();
		EOQualifier myQualifier = null;

		switch (section)	{
		case 1 :
			myQualifier = EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum like '6*'", null);
			filtresMasque = EOQualifier.filteredArrayWithQualifier(comptesSection1,myQualifier);
			break;
		case 2 :
//			myQualifier = EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum like '2*' " +
//					" or planComptable.pcoNum like '1*'" +
//					" or planComptable.pcoNum = %@",
//					new NSArray(FinderBudgetParametres.getValue(ec, NSApp.exerciceBudgetaire(), "COMPTE_IAF")));
			myQualifier = EOQualifier.qualifierWithQualifierFormat("(typeCredit.tcdType='DEPENSE' and typeCredit.tcdSect='2') or planComptable.pcoNum=%@",
					new NSArray(FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), FinderBudgetParametres.COMPTE_IAF)));
			filtresMasque = EOQualifier.filteredArrayWithQualifier(comptesSection2,myQualifier);
			break;
		case 11 :
			myQualifier = EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum = %@",
					new NSArray(FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_EXCEDENT_FONC")));
			break;
		case 22 :
			myQualifier = EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum = %@",
					new NSArray(FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_AUGMENTATION_FDR")));
			break;
		}

		filtres = EOQualifier.filteredArrayWithQualifier(budgetsNature,myQualifier);

		for (int i=0;i<filtres.count();i++)	{

			if (currentOrgan.orgNiveau().intValue() < 2 ||
					(currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieCr() ) )	{

				EOBudgetCalculNature budget = (EOBudgetCalculNature)filtres.objectAtIndex(i);

				myRecord = new NSMutableDictionary();

				myRecord.setObjectForKey(budget, "EO_BUDGET_CALCUL_NATURE");

				myRecord.setObjectForKey(budget.typeCredit(), "EO_TYPE_CREDIT");
				myRecord.setObjectForKey(budget.planComptable(), "EO_PLAN_COMPTABLE");

				myRecord.setObjectForKey(budget.typeCredit().tcdCode(),"TCD");
				myRecord.setObjectForKey(budget.planComptable().pcoNum(),"COMPTE");
				myRecord.setObjectForKey(org.cocktail.bibasse.client.utils.StringCtrl.capitalizedString(budget.planComptable().pcoLibelle()),"LIBELLE");

				if (budget.bdcnCalcul() != null)	{
					myRecord.setObjectForKey(budget.bdcnSaisi(),"SAISI");
					myRecord.setObjectForKey(budget.bdcnCalcul(),"MONTANT");
					myRecord.setObjectForKey(budget.bdcnVote(),"VOTE");
				}
				else	{
					System.out.println("SaisieBudgetNatureDepenses.loadSection() ELSE ==> budget.bdcncalcul null");
					myRecord.setObjectForKey(new BigDecimal(0.0),"SAISI");
					myRecord.setObjectForKey(new BigDecimal(0.0),"MONTANT");
					myRecord.setObjectForKey(new BigDecimal(0.0),"VOTE");
				}
			}
			else {

				EOBudgetSaisieNature budget = (EOBudgetSaisieNature)filtres.objectAtIndex(i);

				myRecord = new NSMutableDictionary();

				myRecord.setObjectForKey(budget, "EO_BUDGET_SAISIE_NATURE");

				myRecord.setObjectForKey(budget.typeCredit(), "EO_TYPE_CREDIT");
				myRecord.setObjectForKey(budget.planComptable(), "EO_PLAN_COMPTABLE");

				myRecord.setObjectForKey(budget.typeCredit().tcdCode(),"TCD");
				myRecord.setObjectForKey(budget.planComptable().pcoNum(),"COMPTE");
				myRecord.setObjectForKey(org.cocktail.bibasse.client.utils.StringCtrl.capitalizedString(budget.planComptable().pcoLibelle()),"LIBELLE");

				myRecord.setObjectForKey(budget.bdsnVote(),"VOTE");
				myRecord.setObjectForKey(budget.bdsnSaisi(),"SAISI");
				myRecord.setObjectForKey(budget.bdsnMontant(),"MONTANT");
			}

			myRecordArray.addObject(myRecord);
		}

		for (int i=0; i<filtresMasque.count(); i++) {
			EOBudgetMasqueNature budget=(EOBudgetMasqueNature) filtresMasque.objectAtIndex(i);

			NSMutableArray arrayQualifier=new NSMutableArray();
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit=%@", new NSArray(budget.typeCredit())));
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable=%@", new NSArray(budget.planComptable())));
			NSArray existants = EOQualifier.filteredArrayWithQualifier(filtres,new EOAndQualifier(arrayQualifier));

			if (existants.count()==0) {
				myRecord = new NSMutableDictionary();

				myRecord.setObjectForKey(budget.typeCredit(), "EO_TYPE_CREDIT");
				myRecord.setObjectForKey(budget.planComptable(), "EO_PLAN_COMPTABLE");

				myRecord.setObjectForKey(budget.typeCredit().tcdCode(),"TCD");
				myRecord.setObjectForKey(budget.planComptable().pcoNum(),"COMPTE");
				myRecord.setObjectForKey(org.cocktail.bibasse.client.utils.StringCtrl.capitalizedString(budget.planComptable().pcoLibelle()),"LIBELLE");

				myRecord.setObjectForKey(new BigDecimal(0.0),"VOTE");
				myRecord.setObjectForKey(new BigDecimal(0.0),"SAISI");
				myRecord.setObjectForKey(new BigDecimal(0.0),"MONTANT");

				myRecordArray.addObject(myRecord);
			}
		}

		eod.setObjectArray(myRecordArray);
	}

	/**
	 *
	 *
	 */
	public void load(NSMutableArray budgets)	{
		budgetsNature = budgets;

		loadSection(eodSection1, 1);
		loadSection(eodSection2, 2);
		loadSection(eodSousS1, 11);
		loadSection(eodSousS2, 22);

		myEOTableS1.updateData();
		myTableModelS1.fireTableDataChanged();

		myEOTableS2.updateData();
		myTableModelS2.fireTableDataChanged();

		myEOTableSousS1.updateData();
		myTableModelSousS1.fireTableDataChanged();

		myEOTableSousS2.updateData();
		myTableModelSousS2.fireTableDataChanged();

		updateCumuls();
	}

	/**
	 *
	 *
	 */
	public void updateCumuls()	{

		BigDecimal sumS1 = NSApp.computeSumForKey(eodSection1, "MONTANT");
		BigDecimal sumS2 = NSApp.computeSumForKey(eodSection2, "MONTANT");

		BigDecimal sumSaisiS1 = NSApp.computeSumForKey(eodSection1, "SAISI");
		BigDecimal sumSaisiS2 = NSApp.computeSumForKey(eodSection2, "SAISI");

		sousTotalS1.setText(ConstantesCocktail.FORMAT_DECIMAL.format(sumS1));
		sousTotalS2.setText(ConstantesCocktail.FORMAT_DECIMAL.format(sumS2));

		sousTotalSaisiS1.setText(ConstantesCocktail.FORMAT_DECIMAL.format(sumSaisiS1));
		sousTotalSaisiS2.setText(ConstantesCocktail.FORMAT_DECIMAL.format(sumSaisiS2));

		BigDecimal fonc = new BigDecimal(0.0);
		if (eodSousS1.displayedObjects().count() == 1)
			fonc = new BigDecimal(((NSDictionary)eodSousS1.displayedObjects().objectAtIndex(0)).objectForKey("MONTANT").toString());

		BigDecimal fdr = new BigDecimal(0.0);
		if (eodSousS2.displayedObjects().count() == 1)
			fdr = new BigDecimal(((NSDictionary)eodSousS2.displayedObjects().objectAtIndex(0)).objectForKey("MONTANT").toString());

		totalS1.setText( ConstantesCocktail.FORMAT_DECIMAL.format(sumS1.add(fonc)));
		totalS2.setText( ConstantesCocktail.FORMAT_DECIMAL.format(sumS2.add(fdr)));

		total =sumS1.add(sumS2).add(fdr).add(fonc);
		totalTf.setText("TOTAL DEPENSES : " + ConstantesCocktail.FORMAT_DECIMAL.format(total) + " \u20ac");
	}

	/**
	 *
	 *
	 */
	public void save()	{

		NSApp.setWaitCursor(mainPanel);

		try {
			for (int i=0;i<eodSection1.displayedObjects().count();i++)	{

				NSMutableDictionary dico = (NSMutableDictionary)eodSection1.displayedObjects().objectAtIndex(i);

				BigDecimal vote = new BigDecimal(StringCtrl.replace(dico.objectForKey("VOTE").toString(), ",","."));
				BigDecimal saisi = new BigDecimal(StringCtrl.replace(dico.objectForKey("SAISI").toString(), ",","."));
				BigDecimal montant = vote.add(saisi);

				EOBudgetSaisieNature budget=(EOBudgetSaisieNature)dico.objectForKey("EO_BUDGET_SAISIE_NATURE");
				if (budget==null && (vote.floatValue()!=0.0 || saisi.floatValue()!=0.0 || montant.floatValue()!=0.0)) {
					EOTypeCredit typeCredit=(EOTypeCredit)dico.objectForKey("EO_TYPE_CREDIT");
					budget=new FactoryBudgetSaisieNature().creerEOBudgetSaisieNature(ec, FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
							typeCredit, (EOPlanComptable)dico.objectForKey("EO_PLAN_COMPTABLE"), currentOrgan,
							typeCredit.exercice(), currentBudgetSaisie, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0));
					dico.setObjectForKey(budget, "EO_BUDGET_SAISIE_NATURE");
					budgetsNature.addObject(budget);
				}

				if (budget!=null) {
					if (budget.bdsnSaisi().floatValue()!=saisi.floatValue()) {
						budget.setBdsnVote(vote);
						budget.setBdsnSaisi(saisi);
						budget.setBdsnMontant(montant);
					}
				}
			}

			for (int i=1;i<eodSection2.displayedObjects().count();i++)	{

				NSMutableDictionary dico = (NSMutableDictionary)eodSection2.displayedObjects().objectAtIndex(i);

				BigDecimal vote = new BigDecimal(StringCtrl.replace(dico.objectForKey("VOTE").toString(), ",","."));
				BigDecimal saisi = new BigDecimal(StringCtrl.replace(dico.objectForKey("SAISI").toString(), ",","."));
				BigDecimal montant = vote.add(saisi);

				EOBudgetSaisieNature budget=(EOBudgetSaisieNature)dico.objectForKey("EO_BUDGET_SAISIE_NATURE");
				if (budget==null && (vote.floatValue()!=0.0 || saisi.floatValue()!=0.0 || montant.floatValue()!=0.0)) {
					EOTypeCredit typeCredit=(EOTypeCredit)dico.objectForKey("EO_TYPE_CREDIT");
					budget=new FactoryBudgetSaisieNature().creerEOBudgetSaisieNature(ec, FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
							typeCredit, (EOPlanComptable)dico.objectForKey("EO_PLAN_COMPTABLE"), currentOrgan,
							typeCredit.exercice(), currentBudgetSaisie, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0));
					dico.setObjectForKey(budget, "EO_BUDGET_SAISIE_NATURE");
					budgetsNature.addObject(budget);
				}

				if (budget!=null) {
					if (budget.bdsnSaisi().floatValue()!=saisi.floatValue()) {
						budget.setBdsnVote(vote);
						budget.setBdsnSaisi(saisi);
						budget.setBdsnMontant(montant);
					}
				}
			}

			ec.saveChanges();
		}
		catch (Exception e)	{
			EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement du budget !");
			e.printStackTrace();
		}

		NSApp.setDefaultCursor(mainPanel);
	}

	/**
	 *
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

		eodSection1 = new EODisplayGroup();
		eodSection2 = new EODisplayGroup();
		eodSousS1 = new EODisplayGroup();
		eodSousS2 = new EODisplayGroup();

		eodSection1.setSortOrderings(new NSArray(new EOSortOrdering("COMPTE",EOSortOrdering.CompareAscending )));
		eodSection2.setSortOrderings(new NSArray(new EOSortOrdering("COMPTE",EOSortOrdering.CompareAscending )));

		eodSousS1.setSortOrderings(new NSArray(new EOSortOrdering("LIBELLE",EOSortOrdering.CompareAscending )));
		eodSousS2.setSortOrderings(new NSArray(new EOSortOrdering("LIBELLE",EOSortOrdering.CompareAscending )));

		initTableModel();
		initTable();

		myEOTableS1.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableS1.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableS1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableS1.setBorder(BorderFactory.createEmptyBorder());
		viewTableS1.removeAll();
		viewTableS1.setLayout(new BorderLayout());
		viewTableS1.add(new JScrollPane(myEOTableS1), BorderLayout.CENTER);

		myEOTableS2.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableS2.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableS2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableS2.setBorder(BorderFactory.createEmptyBorder());
		viewTableS2.removeAll();
		viewTableS2.setLayout(new BorderLayout());
		viewTableS2.add(new JScrollPane(myEOTableS2), BorderLayout.CENTER);

		myEOTableSousS1.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableSousS1.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableSousS1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableSousS1.setBorder(BorderFactory.createEmptyBorder());
		viewTableSousS1.removeAll();
		viewTableSousS1.setLayout(new BorderLayout());
		viewTableSousS1.add(new JScrollPane(myEOTableSousS1), BorderLayout.CENTER);

		myEOTableSousS2.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableSousS2.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableSousS2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableSousS2.setBorder(BorderFactory.createEmptyBorder());
		viewTableSousS2.removeAll();
		viewTableSousS2.setLayout(new BorderLayout());
		viewTableSousS2.add(new JScrollPane(myEOTableSousS2), BorderLayout.CENTER);
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTableS1 = new ZEOTable(myTableSorterS1, myRendererS1);
		myTableModelS1.addTableModelListener(myListenerS1);
//		myTableSorterS1.setTableHeader(myEOTableS1.getTableHeader());

		myEOTableS2 = new ZEOTable(myTableSorterS2, myRendererS2);
		myTableModelS2.addTableModelListener(myListenerS2);
		//myTableSorterS2.setTableHeader(myEOTableS2.getTableHeader());
//		myEOTableS2.setTableHeader(myEOTableS2.getTableHeader());

		myEOTableSousS1 = new ZEOTable(myTableSorterSousS1 , myRendererSousSection);
//		myTableSorterSousS1.setTableHeader(myEOTableSousS1.getTableHeader());
		myEOTableSousS1.setTableHeader(null);

		myEOTableSousS2 = new ZEOTable(myTableSorterSousS2 , myRendererSousSection);
//		myTableSorterSousS2.setTableHeader(myEOTableSousS2.getTableHeader());
		myEOTableSousS2.setTableHeader(null);
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eodSection1, "TCD", "Tcd", 30);
		col1.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodSection1, "COMPTE", "Compte", 50);
		col2.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(eodSection1, "LIBELLE", "Libellé Plan Comptable", 120);
		col3.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(eodSection1, "VOTE", "Voté", 85);
		col4.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col4.setAlignment(SwingConstants.RIGHT);

		ZEOTableModelColumn col5 = new ZEOTableModelColumn(eodSection1, "SAISI", "Saisi", 85);
		col5.setEditable(true);
		col5.setAlignment(SwingConstants.RIGHT);
		col5.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col5.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col5.setTableCellEditor(myCellEditor);

		ZEOTableModelColumn col6 = new ZEOTableModelColumn(eodSection1, "MONTANT", "Résultat", 85);
		col6.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col6.setAlignment(SwingConstants.RIGHT);

		Vector myCols = new Vector();
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);
		myCols.add(col4);
		myCols.add(col5);
		myCols.add(col6);

		myTableModelS1 = new ZEOTableModel(eodSection1, myCols);
		myTableSorterS1 = new TableSorter(myTableModelS1);

		// SECTION 2

		col1 = new ZEOTableModelColumn(eodSection2, "TCD", "Tcd", 30);
		col1.setAlignment(SwingConstants.CENTER);

		col2 = new ZEOTableModelColumn(eodSection2, "COMPTE", "Compte", 50);
		col2.setAlignment(SwingConstants.LEFT);

		col3 = new ZEOTableModelColumn(eodSection2, "LIBELLE", "Libellé Plan Comptable", 150);
		col3.setAlignment(SwingConstants.LEFT);

		col4 = new ZEOTableModelColumn(eodSection2, "VOTE", "Voté", 85);
		col4.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col4.setAlignment(SwingConstants.RIGHT);

		col5 = new ZEOTableModelColumn(eodSection2, "SAISI", "Saisi", 85);
		col5.setEditable(true);
		col5.setAlignment(SwingConstants.RIGHT);
		col5.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col5.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col5.setTableCellEditor(myCellEditor);

		col6 = new ZEOTableModelColumn(eodSection2, "MONTANT", "Résultat", 85);
		col6.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col6.setAlignment(SwingConstants.RIGHT);

		myCols = new Vector();
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);
		myCols.add(col4);
		myCols.add(col5);
		myCols.add(col6);

		myTableModelS2 = new ZEOTableModel(eodSection2, myCols);
		myTableModelS2.setMyProvider(buildReadOnlyTableModelProvider(ec, myTableModelS2));

		myTableSorterS2 = new TableSorter(myTableModelS2);

		// SOUS S1
		col1 = new ZEOTableModelColumn(eodSousS1, "COMPTE", "Compte", 50);
		col1.setAlignment(SwingConstants.LEFT);

		col2 = new ZEOTableModelColumn(eodSousS1, "LIBELLE", "Libellé Plan Comptable", 230);
		col2.setAlignment(SwingConstants.LEFT);

		col3 = new ZEOTableModelColumn(eodSousS1, "SAISI", "Saisi", 75);
		col3.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col3.setAlignment(SwingConstants.RIGHT);

		col4 = new ZEOTableModelColumn(eodSousS1, "MONTANT", "Montant", 75);
		col4.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col4.setAlignment(SwingConstants.RIGHT);

		myCols = new Vector();
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);
		myCols.add(col4);

		myTableModelSousS1 = new ZEOTableModel(eodSousS1, myCols);
		myTableSorterSousS1 = new TableSorter(myTableModelSousS1);


		// SOUS S2
		col1 = new ZEOTableModelColumn(eodSousS2, "COMPTE", "Compte", 50);
		col1.setAlignment(SwingConstants.LEFT);

		col2 = new ZEOTableModelColumn(eodSousS2, "LIBELLE", "Libellé Plan Comptable", 230);
		col2.setAlignment(SwingConstants.LEFT);

		col3 = new ZEOTableModelColumn(eodSousS2, "SAISI", "Dispo", 75);
		col3.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col3.setAlignment(SwingConstants.RIGHT);

		col4 = new ZEOTableModelColumn(eodSousS2, "MONTANT", "Montant", 75);
		col4.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col4.setAlignment(SwingConstants.RIGHT);

		myCols = new Vector();
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);
		myCols.add(col4);

		myTableModelSousS2 = new ZEOTableModel(eodSousS2, myCols);
		myTableSorterSousS2 = new TableSorter(myTableModelSousS2);
	}

	protected IZEOTableModelProvider buildReadOnlyTableModelProvider(EOEditingContext edc, ZEOTableModel model) {
		String compteDeficitFonctionnement = FinderBudgetParametres.getValue(
				edc, NSApp.getExerciceBudgetaire(), FinderBudgetParametres.COMPTE_IAF);
		CompteReadOnlyTableModelProvider provider = new CompteReadOnlyTableModelProvider();
		provider.setTableModel(model);
		provider.setCompteReadOnly(compteDeficitFonctionnement);
		return provider;
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public class ListenerS2 implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {

			if (eodSection2.selectedObject() != null)
				updateRowS2((NSMutableDictionary)eodSection2.selectedObject());
			updateCumuls();
			if (BudgetSyntheseCtrl.sharedInstance(ec).isVisible())	{
				BudgetSyntheseCtrl.sharedInstance(ec).setCurrentOrgan(currentOrgan);
				BudgetSyntheseCtrl.sharedInstance(ec).updateSynthese();
			}

		}
	}


	/**
	 *
	 *
	 */
	public void updateRowS1(NSMutableDictionary myRow)	{

		myTableModelS1.removeTableModelListener(myListenerS1);
		try {
			NSMutableDictionary selectedRow = new NSMutableDictionary(myRow);

			BigDecimal total = new BigDecimal(0.0);

			String saisi = StringCtrl.replace(selectedRow.objectForKey("SAISI").toString(), ",",".");
			String vote = StringCtrl.replace(selectedRow.objectForKey("VOTE").toString(), ",",".");

			total = new BigDecimal(saisi).add(new BigDecimal(vote));

			myRow.setObjectForKey(total, "MONTANT");
		}
		catch (Exception e)	{
		}
		myTableModelS1.addTableModelListener(myListenerS1);
	}

	/**
	 *
	 *
	 */
	public void updateRowS2(NSMutableDictionary myRow)	{

		myTableModelS2.removeTableModelListener(myListenerS2);
		try {

			NSMutableDictionary selectedRow = new NSMutableDictionary(myRow);

			if (!selectedRow.objectForKey("COMPTE").equals("010")) {

				BigDecimal total = new BigDecimal(0.0);

				String saisi = StringCtrl.replace(selectedRow.objectForKey("SAISI").toString(), ",",".");

				String vote = StringCtrl.replace(selectedRow.objectForKey("VOTE").toString(), ",",".");

				total = new BigDecimal(saisi).add(new BigDecimal(vote));

				myRow.setObjectForKey(total, "MONTANT");

			}
		}
		catch (Exception e)	{
		}
		myTableModelS2.addTableModelListener(myListenerS2);
	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	public class ListenerS1 implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {
			if (eodSection1.selectedObject() != null)
				updateRowS1((NSMutableDictionary)eodSection1.selectedObject());
			updateCumuls();
			if (BudgetSyntheseCtrl.sharedInstance(ec).isVisible())	{
				BudgetSyntheseCtrl.sharedInstance(ec).setCurrentOrgan(currentOrgan);
				BudgetSyntheseCtrl.sharedInstance(ec).updateSynthese();
			}
		}
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererS1 extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_TEXTE_SAISIE=new Color(0,0,0);

		public final Color COULEUR_FOND_PLANCO = new Color(218,221,255);
		public final Color COULEUR_TEXTE_PLANCO =new Color(0,0,0);

		public final Color COULEUR_FOND_DISPO =new Color(220,220,220);
		public final Color COULEUR_TEXTE_DISPO =new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			switch (column)	{

			case 3 :
				leComposant.setBackground(COULEUR_FOND_DISPO);
				leComposant.setForeground(COULEUR_TEXTE_DISPO);break;
			case 4 :
				if (currentBudgetSaisie.isVote()
						|| (currentTypeEtatSaisie != null && currentTypeEtatSaisie.isCloture())
						|| !NSApp.fonctionSaisie()
						|| (currentTypeEtatSaisie.isValide() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
						|| (currentTypeEtatSaisie.isControle() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
						|| (currentOrgan.orgNiveau().intValue() < 3  && NSApp.isSaisieCr())
						|| (currentOrgan.orgNiveau().intValue() < 2  && NSApp.isSaisieUb())
				)
				{
					leComposant.setBackground(COULEUR_FOND_DISPO);
					leComposant.setForeground(COULEUR_TEXTE_DISPO);	break;
				}
				else	{
					leComposant.setBackground(COULEUR_FOND_SAISIE);
					leComposant.setForeground(COULEUR_TEXTE_SAISIE);break;
				}
			case 5 :
				leComposant.setBackground(COULEUR_FOND_DISPO);
				leComposant.setForeground(COULEUR_TEXTE_DISPO);break;
			default :
				leComposant.setBackground(COULEUR_FOND_PLANCO);
			leComposant.setForeground(COULEUR_TEXTE_PLANCO);
			}

			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererS2 extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_TEXTE_SAISIE=new Color(0,0,0);

		public final Color COULEUR_FOND_PLANCO = new Color(218,221,255);
		public final Color COULEUR_TEXTE_PLANCO =new Color(0,0,0);

		public final Color COULEUR_FOND_DISPO =new Color(220,220,220);
		public final Color COULEUR_TEXTE_DISPO =new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (row == 0 && column == 4)	{

				leComposant.setBackground(COULEUR_FOND_DISPO);
				leComposant.setForeground(new Color(100,100,100));
				return leComposant;

			}

			switch (column)	{

			case 3 :
				leComposant.setBackground(COULEUR_FOND_DISPO);
				leComposant.setForeground(COULEUR_TEXTE_DISPO);break;
			case 4 :
				if (currentBudgetSaisie.isVote()
						|| (currentTypeEtatSaisie != null && currentTypeEtatSaisie.isCloture())
						|| !NSApp.fonctionSaisie()
						|| (currentTypeEtatSaisie.isValide() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
						|| (currentTypeEtatSaisie.isControle() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
						|| (currentOrgan.orgNiveau().intValue() < 3  && NSApp.isSaisieCr())
						|| (currentOrgan.orgNiveau().intValue() < 2  && NSApp.isSaisieUb())
						)
				{
					leComposant.setBackground(COULEUR_FOND_DISPO);
					leComposant.setForeground(COULEUR_TEXTE_DISPO);	break;
				}
				else	{
					leComposant.setBackground(COULEUR_FOND_SAISIE);
					leComposant.setForeground(COULEUR_TEXTE_SAISIE);break;
				}
			case 5 :
				leComposant.setBackground(COULEUR_FOND_DISPO);
				leComposant.setForeground(COULEUR_TEXTE_DISPO);break;
			default :
				leComposant.setBackground(COULEUR_FOND_PLANCO);
			leComposant.setForeground(COULEUR_TEXTE_PLANCO);
			}

			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}

	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererSousS1 extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_PLANCO = new Color(218,221,255);
		public final Color COULEUR_TEXTE_PLANCO =new Color(0,0,0);

		public final Color COULEUR_FOND_DISPO =new Color(220,220,220);
		public final Color COULEUR_TEXTE_DISPO =new Color(0,0,0);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			leComposant.setForeground(COULEUR_TEXTE_PLANCO);

			if (column == 2)
				leComposant.setForeground(new Color(100,100,100));

			if (column <= 1)
				leComposant.setBackground(COULEUR_FOND_PLANCO);
			else
				leComposant.setBackground(COULEUR_FOND_DISPO);

			return leComposant;
		}
	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererSousSection extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_PLANCO = new Color(218,221,255);
		public final Color COULEUR_TEXTE_PLANCO =new Color(0,0,0);

		public final Color COULEUR_FOND_DISPO =new Color(220,220,220);
		public final Color COULEUR_TEXTE_DISPO =new Color(0,0,0);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			leComposant.setForeground(COULEUR_TEXTE_PLANCO);

			if (column == 2)
				leComposant.setForeground(new Color(100,100,100));

			if (column <= 1)
				leComposant.setBackground(COULEUR_FOND_PLANCO);
			else
				leComposant.setBackground(COULEUR_FOND_DISPO);

			return leComposant;
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class CellEditor extends ZEOTableModelColumn.ZEONumFieldTableCellEditor {

		private  JTextField myTextField;

		public CellEditor(JTextField textField) {
            super(textField, ConstantesCocktail.FORMAT_DECIMAL);
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			myTextField = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);

			if (currentBudgetSaisie.isVote()
					|| (currentTypeEtatSaisie != null && currentTypeEtatSaisie.isCloture())
					|| !NSApp.fonctionSaisie()
					|| (currentTypeEtatSaisie.isValide() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
					|| (currentTypeEtatSaisie.isControle() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
					|| (currentOrgan.orgNiveau().intValue() < 3  && NSApp.isSaisieCr())
					|| (currentOrgan.orgNiveau().intValue() < 2  && NSApp.isSaisieUb())
					)
				return null;
			else	{
				myTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
				myTextField.setEditable(NSApp.fonctionSaisie());
			}
			return myTextField;
		}
	}


	/**
	 *
	 * @return
	 */
	public BigDecimal getCumulVoteForTypeCredit(EOTypeCredit tcd)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("TCD = %@", new NSArray(tcd.tcdCode())));

		NSMutableArray budgetsDepenses = new NSMutableArray(eodSection1.displayedObjects());
		budgetsDepenses.addObjectsFromArray(eodSection2.displayedObjects());

		NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsDepenses,  new EOAndQualifier(mesQualifiers));

		EODisplayGroup myEod = new EODisplayGroup();
		myEod.setObjectArray(filtres);

		return NSApp.computeSumForKey(myEod, "VOTE");
	}

	/**
	 *
	 * @return
	 */
	public BigDecimal getCumulSaisiForTypeCredit(EOTypeCredit tcd)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("TCD = %@", new NSArray(tcd.tcdCode())));

		NSMutableArray budgetsDepenses = new NSMutableArray(eodSection1.displayedObjects());
		budgetsDepenses.addObjectsFromArray(eodSection2.displayedObjects());

		NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsDepenses,  new EOAndQualifier(mesQualifiers));

		EODisplayGroup myEod = new EODisplayGroup();
		myEod.setObjectArray(filtres);

		return NSApp.computeSumForKey(myEod, "SAISI");
	}


	/**
	 *
	 * @return
	 */
	public BigDecimal getCumulForTypeCredit(EOTypeCredit tcd)	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("TCD = %@", new NSArray(tcd.tcdCode())));

		NSMutableArray budgetsDepenses = new NSMutableArray(eodSection1.displayedObjects());
		budgetsDepenses.addObjectsFromArray(eodSection2.displayedObjects());

		NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsDepenses,  new EOAndQualifier(mesQualifiers));

		EODisplayGroup myEod = new EODisplayGroup();
		myEod.setObjectArray(filtres);

		return NSApp.computeSumForKey(myEod, "MONTANT");
	}
}

