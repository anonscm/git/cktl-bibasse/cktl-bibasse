package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.BudgetSyntheseCtrl;
import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.PilotageCtrl;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.editions.EditionsCtrl;
import org.cocktail.bibasse.client.factory.FactoryBibasse;
import org.cocktail.bibasse.client.finder.FinderBudgetCalculGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetCalculNature;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisie;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieNature;
import org.cocktail.bibasse.client.finder.FinderConventionLimitative;
import org.cocktail.bibasse.client.finder.FinderOrgan;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.finder.FinderTypeSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetCalculNature;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.process.budget.ProcessBudgetFactory;
import org.cocktail.bibasse.client.utils.DateCtrl;
import org.cocktail.bibasse.client.utils.XWaitingDialog;
import org.cocktail.bibasse.client.zutil.ui.ZCommentPanel;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class SaisieBudget {

	private static SaisieBudget sharedInstance;
	private EOEditingContext ec;

	protected   ApplicationClient NSApp;
	protected	JFrame mainFrame;
	protected	JDialog mainWindow;

	protected 	ButtonGroup matriceTypeBudget;
	protected	JRadioButton radioButtonGestion;
	protected	JRadioButton radioButtonNature;

	protected	JPanel mainView, swapViewSaisie, swapViewOrgan;

	protected ActionInit			actionInit = new ActionInit();
	protected ActionValidate 		actionValidate = new ActionValidate();
	protected ActionControle 		actionControle = new ActionControle();
	protected ActionSave 			actionSave = new ActionSave();
	protected ActionCancel 			actionCancel = new ActionCancel();
	protected ActionDetailOrgan		actionDetailOrgan = new ActionDetailOrgan();
	protected ActionClose 			actionClose = new ActionClose();
    protected ActionPilotageBudget 	actionPilotageBudget = new ActionPilotageBudget();
    protected ActionCalculBudget 	actionCalculBudget = new ActionCalculBudget();
    protected ActionPrint 			actionPrint = new ActionPrint();

    protected	ActionGetOrganRoot	actionGetOrganRoot = new ActionGetOrganRoot();

	protected 	EOBudgetSaisie			currentBudgetSaisie = null;
	protected 	EOBudgetSaisieGestion 	currentBudgetGestion = null;
	protected 	EOBudgetSaisieNature 	currentBudgetNature = null;
	protected	EOTypeSaisie 			currentTypeSaisie = null;
	protected	EOOrgan 				currentOrgan;
	protected	EOTypeEtat				currentTypeEtatOrgan = null;

	protected XWaitingDialog waitingFrame;

	public JTextField typeSaisieBudget;
	public JTextField informations;

	public ZCommentPanel infosPanel ;

	protected	NSMutableArray budgetsGestion = new NSMutableArray();
	protected	NSMutableArray budgetsNature = new NSMutableArray();

	protected ProcessBudgetFactory myProcessBudgetFactory;

	/**
	 *
	 *
	 */
	public SaisieBudget(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		gui_initView();
	}


	/**
	 *
	 *
	 */
	public void gui_initView()	{

        swapViewOrgan = new JPanel(new BorderLayout());
        swapViewSaisie = new JPanel(new CardLayout());

        mainWindow = new JDialog(mainFrame, "Saisie du budget", false);
//        mainWindow.setTitle("Saisie du Budget");
        mainWindow.setSize(900, 720);

		JButton buttonGetOrganRoot = new JButton(actionGetOrganRoot);
		buttonGetOrganRoot.setPreferredSize(new Dimension(21,21));
		buttonGetOrganRoot.setToolTipText("Affichage de la racine de l'organigramme budgétaire");
		buttonGetOrganRoot.setHorizontalAlignment(0);
		buttonGetOrganRoot.setVerticalAlignment(0);
		buttonGetOrganRoot.setHorizontalTextPosition(0);
		buttonGetOrganRoot.setVerticalTextPosition(0);

		JPanel westFlowPanel = new JPanel(new FlowLayout());
		westFlowPanel.add(buttonGetOrganRoot);

		Box topBox = Box.createHorizontalBox();
        topBox.add(Box.createHorizontalGlue());
        topBox.add(OrganPopupCtrl.sharedInstance(ec).getPanel());
        JPanel panelOrgan = new JPanel(new BorderLayout());
        panelOrgan.add(westFlowPanel, BorderLayout.WEST);
        panelOrgan.add(topBox, BorderLayout.CENTER);

		JButton buttonSynthese = new JButton(actionDetailOrgan);
		buttonSynthese.setPreferredSize(new Dimension(50,23));
		buttonSynthese.setHorizontalAlignment(0);
		buttonSynthese.setVerticalAlignment(0);
		buttonSynthese.setHorizontalTextPosition(0);
		buttonSynthese.setVerticalTextPosition(0);

		JButton buttonPilotage = new JButton(actionPilotageBudget);
		buttonPilotage.setPreferredSize(new Dimension(50,23));
		buttonPilotage.setHorizontalAlignment(0);
		buttonPilotage.setVerticalAlignment(0);
		buttonPilotage.setHorizontalTextPosition(0);
		buttonPilotage.setVerticalTextPosition(0);

		JButton buttonCalcul = new JButton(actionCalculBudget);
		buttonCalcul.setPreferredSize(new Dimension(50,23));
		buttonCalcul.setHorizontalAlignment(0);
		buttonCalcul.setVerticalAlignment(0);
		buttonCalcul.setHorizontalTextPosition(0);
		buttonCalcul.setVerticalTextPosition(0);

		JButton buttonPrint = new JButton(actionPrint);
		buttonPrint.setPreferredSize(new Dimension(50,23));
		buttonPrint.setHorizontalAlignment(0);
		buttonPrint.setVerticalAlignment(0);
		buttonPrint.setHorizontalTextPosition(0);
		buttonPrint.setVerticalTextPosition(0);

		JPanel panelToolBar = new JPanel(new FlowLayout());
		panelToolBar.add(buttonSynthese);
		panelToolBar.add(buttonCalcul);
		panelToolBar.add(buttonPilotage);
		panelToolBar.add(buttonPrint);

       // panelOrgan.add(panelToolBar, BorderLayout.EAST);

        matriceTypeBudget = new ButtonGroup();

        radioButtonGestion = new JRadioButton("BUDGET GESTION", true);
        radioButtonGestion.setForeground(Color.YELLOW);
        radioButtonGestion.setBackground(Color.BLACK);
        radioButtonGestion.setFont(new Font("Arial", Font.PLAIN, 12));
        radioButtonGestion.addItemListener(new TypeBudgetListener());
        matriceTypeBudget.add(radioButtonGestion);
        radioButtonGestion.setSelected(true);

        radioButtonNature = new JRadioButton("BUDGET NATURE", true);
        radioButtonNature.setForeground(Color.YELLOW);
        radioButtonNature.setBackground(Color.BLACK);
        radioButtonNature.setFont(new Font("Arial", Font.PLAIN, 12));
        radioButtonNature.addItemListener(new TypeBudgetListener());
        matriceTypeBudget.add(radioButtonNature);

        Box boxSaisie1 = Box.createHorizontalBox();
        Box boxSaisie2 = Box.createHorizontalBox();
        boxSaisie1.add(Box.createHorizontalGlue());
        boxSaisie2.add(Box.createHorizontalGlue());
		boxSaisie1.add(SaisieBudgetGestion.sharedInstance(ec).getPanel());
		boxSaisie2.add(SaisieBudgetNature.sharedInstance(ec).getPanel());

		swapViewSaisie.add("gestion",boxSaisie1);
		swapViewSaisie.add("nature",boxSaisie2);

        JPanel panelRadioBoutons = new JPanel(new FlowLayout());
        panelRadioBoutons.add(radioButtonGestion);
        panelRadioBoutons.add(radioButtonNature);

		JPanel panelSelectbudget = new JPanel(new BorderLayout());
		panelSelectbudget.add(panelOrgan, BorderLayout.WEST);
		panelSelectbudget.add(panelToolBar, BorderLayout.CENTER);
		panelSelectbudget.add(panelRadioBoutons, BorderLayout.EAST);
        panelSelectbudget.setPreferredSize(new Dimension(panelSelectbudget.getWidth(), 35));

        swapViewOrgan.add(panelSelectbudget, BorderLayout.CENTER);

        infosPanel = new ZCommentPanel(null, "INFOS", null, new Color(0,0,0), new Color (0,255,0), null);

        typeSaisieBudget = new JTextField("BUDGET PROVISOIRE");
        typeSaisieBudget.setBackground(new Color(236,234,149));
        typeSaisieBudget.setForeground(new Color(0,0,0));
        typeSaisieBudget.setBorder(BorderFactory.createEmptyBorder());
        typeSaisieBudget.setHorizontalAlignment(JTextField.LEFT);
        typeSaisieBudget.setFont(new Font("Arial", Font.PLAIN, 12));
        typeSaisieBudget.setPreferredSize(new Dimension(175, 19));
        typeSaisieBudget.setEditable(false);
        typeSaisieBudget.setFocusable(false);

        informations = new JTextField("Sélectionnez une ligne budgétaire");
        informations.setBackground(new Color(236, 234, 149));
        informations.setForeground(new Color(0, 0, 0));
        informations.setBorder(BorderFactory.createEmptyBorder());
        informations.setFont(Configuration.instance().informationLabelFont(ec));
        informations.setHorizontalAlignment(JTextField.LEFT);
        informations.setEditable(false);
        informations.setFocusable(false);

        JPanel panelSouth = new JPanel(new BorderLayout());
        panelSouth.add(typeSaisieBudget, BorderLayout.WEST);
        panelSouth.add(informations, BorderLayout.CENTER);
        //panelSouth.add(etatSaisieBudget, BorderLayout.EAST);

        swapViewOrgan.add(panelSouth, BorderLayout.SOUTH);
        swapViewOrgan.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));

        // swapViewOrgan.add(new JSeparator(), BorderLayout.SOUTH);

        // Boutons
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionInit);
		arrayList.add(actionCancel);
		arrayList.add(actionSave);
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 110, 23));

		arrayList = new ArrayList();

		arrayList.add(actionValidate);

		arrayList.add(actionControle);

		JPanel panelButtonValidate = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 130, 23));

		JPanel swapViewButtons = new JPanel(new BorderLayout());
		swapViewButtons.setBorder(BorderFactory.createEmptyBorder(5,4,2,4));
		swapViewButtons.add(new JSeparator(), BorderLayout.NORTH);
		swapViewButtons.add(panelButtonValidate, BorderLayout.WEST);
		swapViewButtons.add(panelButtons, BorderLayout.EAST);

        mainView = new JPanel(new BorderLayout());
        mainView.add(swapViewOrgan, BorderLayout.NORTH);
        mainView.add(swapViewSaisie, BorderLayout.CENTER);
        mainView.add(swapViewButtons, BorderLayout.SOUTH);

		actionInit.setEnabled(false);
		actionValidate.setEnabled(false);
		actionPrint.setEnabled(false);
		actionSave.setEnabled(false);
		actionCancel.setEnabled(false);

        mainWindow.setContentPane(mainView);
	}

	/**
	 *
	 * @return
	 */
	public boolean isOpen()	{
		return mainWindow.isVisible();
	}

	/**
	 *
	 * @param frame
	 */
	public void setMainFrame(JFrame frame)	{
		mainFrame = frame;
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static SaisieBudget sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SaisieBudget(editingContext);
		return sharedInstance;
	}



	/**
	 *
	 *
	 */
	public void updateUI()	{

		actionPilotageBudget.setEnabled(NSApp.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));

		if (/*currentTypeEtatOrgan==null ||*/ currentOrgan == null)	{

			actionInit.setEnabled(false);
			actionValidate.setEnabled(false);
			actionControle.setEnabled(false);
			actionSave.setEnabled(false);
			actionCancel.setEnabled(false);
			actionDetailOrgan.setEnabled(false);
			actionCalculBudget.setEnabled(false);
			actionDetailOrgan.setEnabled(false);
			actionPrint.setEnabled(false);

		} else {

			// On regarde si un budget de gestion existe pour cet ligne budgetaire

			if (budgetsGestion.count() > 0)	{

				actionPrint.setEnabled(true);
				actionDetailOrgan.setEnabled(true);

				if (currentBudgetSaisie.isVote())	{
					actionInit.setEnabled(false);
					actionValidate.setEnabled(false);
					actionControle.setEnabled(false);
					actionSave.setEnabled(false);
					actionCancel.setEnabled(false);

					actionCalculBudget.setEnabled(false);
				}
				else	{		// Budget Non Vote

					if (currentOrgan.orgNiveau().intValue() < 2 || (currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieCr() ) )
						actionCalculBudget.setEnabled(false);
					else
						actionCalculBudget.setEnabled(true);


					actionInit.setEnabled(NSApp.hasFonction(ConstantesCocktail.ID_FCT_REINIT)
							&& !currentTypeEtatOrgan.isCloture()
							&& !currentTypeEtatOrgan.isControle()
							&& !currentTypeEtatOrgan.isValide()
							&& ( currentOrgan.orgNiveau().intValue() > 2
									|| (currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieUb()) ));
					actionInit.putValue(AbstractAction.NAME, "Réinitialiser");
					actionInit.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_REINIT);

					if (currentTypeEtatOrgan.isEnCours())	{
						actionValidate.setEnabled(NSApp.fonctionSaisie()
							&& ( currentOrgan.orgNiveau().intValue() > 2
							|| (currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieUb()) ));
					}
					else	{
						actionValidate.setEnabled(NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE)
								&& ( currentOrgan.orgNiveau().intValue() > 2
								|| (currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieUb()) ));
					}

					actionControle.setEnabled(NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE) && !currentTypeEtatOrgan.isEnCours()
							&& ( currentOrgan.orgNiveau().intValue() > 2
							|| (currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieUb()) ));

					actionSave.setEnabled(NSApp.fonctionSaisie() &&
							!NSApp.fonctionSaisie()
							|| (NSApp.isSaisieCr() && currentOrgan.orgNiveau().intValue() > 2)
							|| (NSApp.isSaisieUb() && currentOrgan.orgNiveau().intValue() > 1));

					actionCancel.setEnabled(NSApp.fonctionSaisie() &&
							(NSApp.isSaisieCr() && currentOrgan.orgNiveau().intValue() > 2)
							|| (NSApp.isSaisieUb() && currentOrgan.orgNiveau().intValue() > 1));
				}


				//
				if (currentTypeEtatOrgan != null)	 {

					if (currentTypeEtatOrgan.isEnCours())	{
						actionValidate.putValue(AbstractAction.NAME, "Verrouiller " + NSApp.niveauSaisie());
						actionValidate.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOCK);
					}
					else	{
						actionValidate.putValue(AbstractAction.NAME, "Déverrouiller " + NSApp.niveauSaisie());
						actionValidate.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_UNLOCK);
					}

					if (currentTypeEtatOrgan.isValide())	{
						actionControle.putValue(AbstractAction.NAME, "contrôler " + NSApp.niveauSaisie());
						actionControle.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CONTROLE_BUDGET);
					}
					else
						if (currentTypeEtatOrgan.isControle())	{
							actionControle.setEnabled(NSApp.fonctionCloture()
									&& ( currentOrgan.orgNiveau().intValue() > 2
									|| (currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieUb()) ));
							actionControle.putValue(AbstractAction.NAME, "Clôturer " + NSApp.niveauSaisie());
							actionControle.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOTURE_BUDGET);
						}
						else
							if (currentTypeEtatOrgan.isCloture())	{
								actionValidate.putValue(AbstractAction.NAME, NSApp.niveauSaisie() + " CLOTURE");
								actionValidate.putValue(AbstractAction.SMALL_ICON, null);
								actionControle.putValue(AbstractAction.NAME, NSApp.niveauSaisie() + " CLOTURE");
								actionControle.putValue(AbstractAction.SMALL_ICON, null);
								actionValidate.setEnabled(false);
								actionControle.setEnabled(false);
								actionSave.setEnabled(false);
								actionCancel.setEnabled(false);
							}

				}

			}
			else	{	// CR ou UB non initialises

				actionInit.putValue(AbstractAction.NAME, "Initialiser");

				if (currentBudgetSaisie != null &&
						!currentBudgetSaisie.isVote() &&
						((currentOrgan.orgNiveau().intValue() > 2)
						|| (NSApp.isSaisieUb() && currentOrgan.orgNiveau().intValue() == 2))
					)
					actionInit.setEnabled(NSApp.fonctionSaisie());
				else
					actionInit.setEnabled(false);

				actionSave.setEnabled(false);
				actionCancel.setEnabled(false);
				actionValidate.setEnabled(false);
				actionControle.setEnabled(false);

				actionCalculBudget.setEnabled(false);
				actionDetailOrgan.setEnabled(false);

			}
		}

		SaisieBudgetGestionDepenses.sharedInstance(ec).updateUI();
		SaisieBudgetGestionRecettes.sharedInstance(ec).updateUI();

	}



	/**
	 *
	 *
	 */
	public void open()	{

		String title = ConstantesCocktail.APPLICATION_NAME+" - Budget " + currentTypeSaisie.tysaLibelle() + " "  + NSApp.getExerciceBudgetaire().exeExercice() ;
		mainWindow.setTitle(title);

		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();

		updateUI();

	}

	public void clean()	{

		SaisieBudgetGestion.sharedInstance(ec).clean();
		SaisieBudgetNature.sharedInstance(ec).clean();

	}

	/**
	 *
	 *
	 */
	public void close()	{
		mainWindow.dispose();
	}

	/**
	 *
	 * @param typeBudget
	 */
		public void setBudgetSaisie(EOBudgetSaisie budget)	{

			currentBudgetSaisie = budget;

			SaisieBudgetGestionDepenses.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
			SaisieBudgetGestionRecettes.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
			SaisieBudgetNatureDepenses.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
			SaisieBudgetNatureRecettes.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);

		}


/**
 *
 * @param typeBudget
 */
	public void setTypeSaisie(EOTypeSaisie typeSaisie)	{

		currentTypeSaisie = typeSaisie;

		if (currentBudgetSaisie != null)
			typeSaisieBudget.setText(currentBudgetSaisie.bdsaLibelle() + " " + NSApp.getExerciceBudgetaire().exeExercice());
		else
			typeSaisieBudget.setText(typeSaisie.tysaLibelle());

	}

	/**
	 * Suppression d'un budget.
	 *
	 */
	public void deleteBudget()	{
		if (currentBudgetSaisie != null)	{
			try {
				System.out.println("Supression budget " + currentBudgetSaisie.bdsaLibelle());
				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey((EOEnterpriseObject) currentBudgetSaisie, "EOBudgetSaisie");
				ServerProxy.clientSideRequestSupprimerBudget(ec, parametres);
			} catch (Exception ex)	{
				ex.printStackTrace();
				EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			}
		}
	}




	/**
	 * Initialisation des budgets de Gestion et par Nature
	 *
	 */
	public EOBudgetSaisie initBudgetSaisie()	{

		ProcessBudgetFactory myProcessBudgetFactory = null;

		if (!currentTypeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_DBM)) {
			currentBudgetSaisie = FinderBudgetSaisie.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(), currentTypeSaisie);
		} else {
			// S'il s'agit d'une DBM, on cree un nouveau budget_saisie
			currentBudgetSaisie = null;
		}

		if (currentBudgetSaisie == null)	{

			myProcessBudgetFactory = new ProcessBudgetFactory(true, ProcessBudgetFactory.PROCESS_PROVISOIRE);

			try {

				// On cree soit un budget Convention ou Hors Convention
				if (currentTypeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_CONVENTION))	{
					currentBudgetSaisie = myProcessBudgetFactory.creerUnEOBudgetSaisiePourConvention(
							ec,
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
							currentTypeSaisie,
							NSApp.getExerciceBudgetaire(),
							"BUDGET " + currentTypeSaisie.tysaLibelle(),
							NSApp.getUtilisateur()
					);
				} else {
					// Si c'est une DBM, on construit le titre avec le libelle de la DBM
					String titreBudget = "BUDGET " + currentTypeSaisie.tysaLibelle();
					if (currentTypeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_DBM)) {
						NSArray dbms = FinderBudgetSaisie.findBudgetsSaisieForExercice(ec,NSApp.getExerciceBudgetaire(), FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_DBM));
						titreBudget = "DBM " + (dbms.count() + 1);
					}

					currentBudgetSaisie = myProcessBudgetFactory.creerUnEOBudgetSaisieHorsConvention(
							ec,
							FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
							currentTypeSaisie,
							NSApp.getExerciceBudgetaire(),
							titreBudget,
							NSApp.getUtilisateur()
					);
				}

				ec.saveChanges();

				// Initialisation d'un budget RELIQUAT ou DBM
				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(currentBudgetSaisie, "EOBudgetSaisie");

				if (currentTypeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_INITIAL))	{
					EOTypeSaisie typeSaisieProvisoire = FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_PROVISOIRE);
					EOBudgetSaisie budgetSaisie = FinderBudgetSaisie.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(), typeSaisieProvisoire);
					if (budgetSaisie != null) {
						ServerProxy.clientSideRequestInitialiserBudgetInitial(ec, parametres);
					}
				}

				if (currentTypeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_RELIQUAT))	{
					ServerProxy.clientSideRequestInitialiserBudgetReliquat(ec, parametres);
				}

				if (currentTypeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_DBM))	{
					ServerProxy.clientSideRequestInitialiserBudgetDbm(ec, parametres);
				}
			} catch (Exception e)	{
				EODialogs.runInformationDialog("ERREUR", "Erreur d'initialisation du budget de gestion  \n" + e.getMessage());
				e.printStackTrace();
				return null;
			}
		}

		SaisieBudgetGestionDepenses.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
		SaisieBudgetGestionRecettes.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
		SaisieBudgetNatureDepenses.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
		SaisieBudgetNatureRecettes.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
		PilotageCtrl.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);

		return currentBudgetSaisie;

	}

	 /**
	  *
	  * @author cpinsard
	  *
	  */
	 protected class ThreadInitCr extends Thread {
		 public void run() {
			 initialiserCrs();
		 }
	 }

	/**
	 *
	 *
	 */
	public void initialiserCrs()	{

		myProcessBudgetFactory = new ProcessBudgetFactory(true, ProcessBudgetFactory.PROCESS_PROVISOIRE);

		try {

			NSArray organs = FinderOrgan.findOrgansForPilotageBudget(ec, NSApp.getExerciceBudgetaire(), (NSApp.isSaisieCr())?new Integer(3):new Integer(2));

			for (int i=0;i<organs.count();i++)	{

				 waitingFrame.setMessages("Initialisation des CRS : ",i+" / " + organs.count());

				EOOrgan organ = (EOOrgan)organs.objectAtIndex(i);

				initBudgetGestion(organ);
				initBudgetNature(organ);

				 ec.saveChanges();
			}

			 waitingFrame.setMessages("Initialisation des CRS : ","Enregistrement des données ...");
		}
		catch (Exception e)	{
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur d'initialisation du budget de gestion !");
		}

		waitingFrame.close();
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public class TypeBudgetListener implements ItemListener 	{

		public void itemStateChanged(ItemEvent e)	{

			if (e.getStateChange() == ItemEvent.SELECTED)	{
				if (radioButtonGestion.isSelected()) {
					((CardLayout) swapViewSaisie.getLayout()).show(swapViewSaisie, "gestion");
				}
				if (radioButtonNature.isSelected()) {
					((CardLayout) swapViewSaisie.getLayout()).show(swapViewSaisie, "nature");
				}
			}
		}
	}

	/**
	 *
	 */
	public void reload()	{
		organHasChanged(currentOrgan);
	}

	public EOOrgan getCurrentOrgan() {
		return currentOrgan;
	}


	/**
	 *
	 *
	 */
	public void organHasChanged(EOOrgan organ)	{

		if (organ == null)
			return;

		NSApp.setWaitCursor(swapViewOrgan);

		currentOrgan = organ;
		System.out.println(">>> SaisieBudget.organHasChanged() : " + currentOrgan + "  " + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

		if (currentOrgan.orgNiveau().intValue() < 2 ||
				(currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieCr() ) )	{

			budgetsGestion = new NSMutableArray(FinderBudgetCalculGestion.findBudgetsGestion(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire()));
			budgetsNature = new NSMutableArray(FinderBudgetCalculNature.findBudgetsNature(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire()));

		}
		else	{
			budgetsGestion = new NSMutableArray(FinderBudgetSaisieGestion.findBudgetsGestion(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire()));
			budgetsNature = new NSMutableArray(FinderBudgetSaisieNature.findBudgetsNature(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire()));

			System.out.println("SaisieBudget.organHasChanged() BUDGETS GESTION COUNT : " + budgetsGestion.count());
			System.out.println("SaisieBudget.organHasChanged() BUDGETS NATURE COUNT : " + budgetsNature.count());
		}

		System.out.println("\tBudgets Gestion et Nature Charges (Gestion : " + budgetsGestion.count()+" , Nature : " + budgetsNature.count()+")  " + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

		informations.setText(currentOrgan.libelleExtraLong());

		if (currentOrgan.typeOrgan().isConvention())	{

			NSArray conventions = FinderConventionLimitative.findConventionsForOrgan(ec, currentOrgan, NSApp.getExerciceBudgetaire());
			if (conventions != null && conventions.count() > 0)
				informations.setText(informations.getText() + " - CONVENTION RA. Montant : " + NSApp.computeSumForKey(conventions, "clMontant").toString()+ " \u20ac ");
		}

		SaisieBudgetGestionDepenses.sharedInstance(ec).setOrgan(currentOrgan);
		SaisieBudgetGestionRecettes.sharedInstance(ec).setOrgan(currentOrgan);
		SaisieBudgetNatureDepenses.sharedInstance(ec).setOrgan(currentOrgan);
		SaisieBudgetNatureRecettes.sharedInstance(ec).setOrgan(currentOrgan);

		SaisieBudgetGestionDepenses.sharedInstance(ec).setArrayBudgetsGestion(budgetsGestion);
		SaisieBudgetGestionRecettes.sharedInstance(ec).setArrayBudgetsGestion(budgetsGestion);

		System.out.println("budgetsGestion.count() "+budgetsGestion.count());
		if (budgetsGestion.count() > 0 && budgetsNature.count() > 0)	{

			try {
				currentTypeEtatOrgan = ((EOBudgetSaisieNature)budgetsNature.objectAtIndex(0)).typeEtat();
			}
			catch (Exception e)	{
				currentTypeEtatOrgan = ((EOBudgetCalculNature)budgetsNature.objectAtIndex(0)).typeEtat();
			}

System.out.println(currentTypeEtatOrgan);
			SaisieBudgetGestionDepenses.sharedInstance(ec).setTypeEtatSaisie(currentTypeEtatOrgan);
			SaisieBudgetGestionRecettes.sharedInstance(ec).setTypeEtatSaisie(currentTypeEtatOrgan);
			SaisieBudgetNatureDepenses.sharedInstance(ec).setTypeEtatSaisie(currentTypeEtatOrgan);
			SaisieBudgetNatureRecettes.sharedInstance(ec).setTypeEtatSaisie(currentTypeEtatOrgan);

			informations.setText(informations.getText()+" ( "+ currentTypeEtatOrgan.tyetLibelle() + " )");

			SaisieBudgetGestionDepenses.sharedInstance(ec).load(budgetsGestion);
			System.out.println("\tGestion Depenses Chargees" + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));
			SaisieBudgetGestionRecettes.sharedInstance(ec).load(budgetsGestion);
			System.out.println("\tGestion Recettes Chargees" + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));
			SaisieBudgetNatureDepenses.sharedInstance(ec).load(budgetsNature);
			System.out.println("\tNature Depenses Chargees" + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));
			SaisieBudgetNatureRecettes.sharedInstance(ec).load(budgetsNature);
			System.out.println("\tNature Recettes Chargees" + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

			SaisieBudgetGestionDepenses.sharedInstance(ec).updateCumuls();
			System.out.println("\tGestion Depenses Cumuls" + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));
			SaisieBudgetGestionRecettes.sharedInstance(ec).updateCumuls();
			System.out.println("\tGestion Recettes Cumuls" + DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));
		}
		else	{
			SaisieBudgetGestionDepenses.sharedInstance(ec).clean();
			SaisieBudgetGestionRecettes.sharedInstance(ec).clean();
			SaisieBudgetNatureDepenses.sharedInstance(ec).clean();
			SaisieBudgetNatureRecettes.sharedInstance(ec).clean();
		}

		updateUI();

		NSApp.setDefaultCursor(swapViewOrgan);
	}


	/**
	 *
	 *
	 */
	public void initBudgetGestion(EOOrgan organ)	{

		try {

			// TODO : init pour virer zero
			//for (int i=0; i<)

			myProcessBudgetFactory.initialiserBudgetGestion(
					ec,
					FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
					SaisieBudgetGestionDepenses.sharedInstance(ec).getTypesCredit(),
					SaisieBudgetGestionDepenses.sharedInstance(ec).getActions(),
					SaisieBudgetGestionRecettes.sharedInstance(ec).getTypesCredit(),
					SaisieBudgetGestionRecettes.sharedInstance(ec).getActions(),
					organ,
					NSApp.getExerciceBudgetaire(),
					NSApp.getUtilisateur(),
					currentBudgetSaisie
			);
		}
		catch (Exception e)	{
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur d'initialisation du budget de gestion !");
		}
	}

	/**
	 *
	 *
	 */
	private void initBudgetNature(EOOrgan organ)	{

		try {

			NSMutableArray plancosCredit = new NSMutableArray(SaisieBudgetNatureDepenses.sharedInstance(ec).getMasqueNature());
			plancosCredit.addObjectsFromArray(SaisieBudgetNatureRecettes.sharedInstance(ec).getMasqueNature());

			myProcessBudgetFactory.initialiserBudgetNature(
					ec,
					FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
					plancosCredit,
					organ,
					NSApp.getExerciceBudgetaire(),
					NSApp.getUtilisateur(),
					currentBudgetSaisie
			);

		}
		catch (Exception e)	{
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur d'initialisation du budget de gestion !");
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionInit extends AbstractAction {

	    public ActionInit() {
            super("Initialiser");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_INIT_BUDGET);
        }

        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

    		myProcessBudgetFactory = new ProcessBudgetFactory(true, ProcessBudgetFactory.PROCESS_PROVISOIRE);

    		try {

    			// Si on avait deja des donnees, on vide tout pour reinitialiser
    			if (budgetsGestion.count() > 0)	{

    				if (!EODialogs.runConfirmOperationDialog("Attention",
    						"Vous allez réinitialiser le budget saisi pour cette ligne budgétaire !\nSouhaitez-vous Poursuivre ?",
    						"OUI", "NON"))	{
    		        	NSApp.setDefaultCursor(mainView);
    					return;
    				}

    	        	try {
    	        		System.out.println("ActionSave.actionPerformed() CLEAN budget");
    	        		NSMutableDictionary parametres = new NSMutableDictionary();
    	        		parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
    	        		parametres.setObjectForKey((EOEnterpriseObject)currentOrgan, "EOOrgan");
    	        		ServerProxy.clientSideRequestViderBudgetOrgan(ec, parametres);
    	        	}
    	        	catch (Exception ex)	{
    	        		EODialogs.runErrorDialog("ERREUR", ex.getMessage());
    	        	}

    			}

//
//
//
//
//
//    			a remplacer par le meme genre de proc a l'ouverture d'une dbm
//
//
//
//
    			initBudgetGestion(currentOrgan);
    			initBudgetNature(currentOrgan);




    			ec.saveChanges();

    			// On lance la procedure de mise a jour des budgets votes
    			if (currentBudgetSaisie.isBudgetInitial() || currentBudgetSaisie.isBudgetReliquat() ||
    					currentBudgetSaisie.isBudgetDbm())	{

	    			NSMutableDictionary parametres = new NSMutableDictionary();
	    			parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
	    			parametres.setObjectForKey(currentOrgan, "EOOrgan");

		    		ServerProxy.clientSideRequestSetBudgetsVotesOrgan(ec, parametres);
    			}

	    		organHasChanged(currentOrgan);

	    		for (int i=0;i<budgetsNature.count();i++)
	    			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOBudgetSaisieNature)budgetsNature.objectAtIndex(i))));

    		}
    		catch (Exception ex)	{

    			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
    			ex.printStackTrace();

    		}

        	organHasChanged(currentOrgan);

        	PilotageCtrl myPilotage = PilotageCtrl.sharedInstance(ec);
        	if (myPilotage.isVisible()) {
        		myPilotage.refresh();
        	}

        	NSApp.setDefaultCursor(mainView);

        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionValidate extends AbstractAction {

	    public ActionValidate() {
            super("Valider Saisie");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOCK);
        }


	    public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(mainView);

	    	try {

    			actionSave.actionPerformed(null);

    			NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
				parametres.setObjectForKey((EOEnterpriseObject)currentOrgan, "EOOrgan");

				// Deverouillage du CR ou de l'UB
	    		if (currentTypeEtatOrgan.isValide()) {
    				ServerProxy.clientSideRequestDeverouillerLigneBudgetaire(ec, parametres);
	    		} else	{ // Validation du CR ou de l'UB
	    			FactoryBibasse.validerSaisieOrgan(ec, currentBudgetSaisie, currentOrgan, NSApp.getExerciceBudgetaire());
	    		}

	    		// Rafraichissement des donnees
	    		for (int i=0; i < budgetsNature.count(); i++) {
	    			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOBudgetSaisieNature) budgetsNature.objectAtIndex(i))));
	    		}

	    		organHasChanged(currentOrgan);

	    		PilotageCtrl myPilotage = PilotageCtrl.sharedInstance(ec);
	    		if (myPilotage.isVisible()) {
	    			myPilotage.refresh();
	    		}

   				updateUI();
	    	} catch (Exception ex)	{
	    		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
	    		updateUI();
	    	}
	    	NSApp.setDefaultCursor(mainView);

	    }

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionControle extends AbstractAction {

	    public ActionControle() {
            super("contrôler CR");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CONTROLE_BUDGET);
        }


	    public void actionPerformed(ActionEvent e) {

	    	NSApp.setWaitCursor(mainView);

	    	try {

	    		// On sauvegarde les dernieres saisies
	    		actionSave.actionPerformed(null);

	    		NSMutableDictionary parametres = new NSMutableDictionary();
	    		parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
	    		parametres.setObjectForKey((EOEnterpriseObject)currentOrgan, "EOOrgan");

	    		if (currentTypeEtatOrgan.isValide()) {
	    			ServerProxy.clientSideRequestControlerLigneBudgetaire(ec, parametres);
	    		} else {
	            	if (EODialogs.runConfirmOperationDialog("Attention",
	        				"Confirmez-vous la clôture de cette saisie ?",
	        				"OUI", "NON"))	{
	            		if (currentTypeEtatOrgan.isControle()) {
	            			ServerProxy.clientSideRequestCloturerLigneBudgetaire(ec, parametres);
	            		}
	            	} else {
	            		return;
	            	}
	    		}

	    		// Rafraichissement des donnees
	    		for (int i=0;i<budgetsNature.count();i++)
	    			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject((EOBudgetSaisieNature)budgetsNature.objectAtIndex(i))));

	    		organHasChanged(currentOrgan);

	    		PilotageCtrl myPilotage = PilotageCtrl.sharedInstance(ec);
	    		if (myPilotage.isVisible()) {
	    			myPilotage.refresh();
	    		}

	    		updateUI();

	    	}
	    	catch (Exception ex)	{
	    		EODialogs.runErrorDialog("ERREUR", NSApp.getErrorDialog(ex));
	    		updateUI();
	    	}
	    	NSApp.setDefaultCursor(mainView);

	    }

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPrint extends AbstractAction {

	    public ActionPrint() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
        }

        public void actionPerformed(ActionEvent e) {

        	if (radioButtonNature.isSelected())	{
        		EditionsCtrl.sharedInstance(ec).printBudgetNature(currentOrgan, NSApp.getExerciceBudgetaire(), currentBudgetSaisie,
        				"SAISIE", currentOrgan.orgNiveau().intValue(), currentOrgan.orgNiveau().intValue());
        	}

        	if (radioButtonGestion.isSelected()) {
        		EditionsCtrl.sharedInstance(ec).printBudgetGestionDepense(currentOrgan, NSApp.getExerciceBudgetaire(), currentBudgetSaisie,
        				"SAISIE", currentOrgan.orgNiveau().intValue(), currentOrgan.orgNiveau().intValue());
        		EditionsCtrl.sharedInstance(ec).printBudgetGestionRecette(currentOrgan, NSApp.getExerciceBudgetaire(), currentBudgetSaisie,
        				"SAISIE", currentOrgan.orgNiveau().intValue(), currentOrgan.orgNiveau().intValue());
        	}
        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }

        public void actionPerformed(ActionEvent e) {
        	mainWindow.hide();
        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionSave extends AbstractAction {

	    public ActionSave() {
            super("Enregistrer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SAVE);
        }


        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

        	SaisieBudgetGestionDepenses.sharedInstance(ec).save();
        	SaisieBudgetGestionRecettes.sharedInstance(ec).save();

        	SaisieBudgetNatureDepenses.sharedInstance(ec).save();
        	SaisieBudgetNatureRecettes.sharedInstance(ec).save();

        	// ajout calcul CAF/IAF/DFR ...
        	calculerBudget(budgetsNature);
			SaisieBudgetNatureDepenses.sharedInstance(ec).load(budgetsNature);
			SaisieBudgetNatureRecettes.sharedInstance(ec).load(budgetsNature);
        	///////

        	// Consolidation du budget
        	try {
        		System.out.println("ActionSave.actionPerformed() CONSOLIDATION du budget");
        		NSMutableDictionary parametres = new NSMutableDictionary();
        		parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");
        		ServerProxy.clientSideRequestConsoliderBudget(ec, parametres);
        	}
        	catch (Exception ex)	{
        		EODialogs.runErrorDialog("ERREUR", ex.getMessage());
        	}

        	NSApp.setDefaultCursor(mainView);
        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionGetOrganRoot extends AbstractAction {

	    public ActionGetOrganRoot() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_ROOT);
        }

        public void actionPerformed(ActionEvent e) {
        	EOOrgan organRoot = FinderOrgan.findOrganRoot(ec);
        	OrganPopupCtrl.sharedInstance(ec).cleanPopups();
        	organHasChanged(organRoot);
        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionDetailOrgan extends AbstractAction {

	    public ActionDetailOrgan() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
        }

        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

        	BudgetSyntheseCtrl.sharedInstance(ec).setCurrentOrgan(currentOrgan);
        	System.out.println("ActionDetailOrgan.actionPerformed()");
        	BudgetSyntheseCtrl.sharedInstance(ec).updateSynthese();
        	BudgetSyntheseCtrl.sharedInstance(ec).open();

        	NSApp.setDefaultCursor(mainView);

        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
        }

        public void actionPerformed(ActionEvent e) {
        	boolean actionConfirmed = EODialogs.runConfirmOperationDialog(
        			"Attention",
        			"Confirmez-vous l'annulation de la saisie du budget pour cette ligne budgétaire ?", "OUI", "NON");

        	if (actionConfirmed) {
            	NSApp.setWaitCursor(mainView);

            	ec.revert();
            	SaisieBudgetGestionDepenses.sharedInstance(ec).load(budgetsGestion);
            	SaisieBudgetGestionRecettes.sharedInstance(ec).load(budgetsGestion);
            	SaisieBudgetNatureDepenses.sharedInstance(ec).load(budgetsNature);
            	SaisieBudgetNatureRecettes.sharedInstance(ec).load(budgetsNature);

            	SaisieBudgetGestionDepenses.sharedInstance(ec).updateCumuls();
            	SaisieBudgetGestionRecettes.sharedInstance(ec).updateCumuls();

            	NSApp.setDefaultCursor(mainView);
        	}
        }
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPilotageBudget extends AbstractAction {

	    public ActionPilotageBudget() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_PILOTAGE);
        }


        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

        	PilotageCtrl.sharedInstance(ec).open();

        	NSApp.setDefaultCursor(mainView);
        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionCalculBudget extends AbstractAction {

	    public ActionCalculBudget() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CALCUL_BUDGET);
        }


        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainView);

        	SaisieBudgetNatureDepenses.sharedInstance(ec).save();
        	SaisieBudgetNatureRecettes.sharedInstance(ec).save();

        	calculerBudget(budgetsNature);

			SaisieBudgetNatureDepenses.sharedInstance(ec).load(budgetsNature);
			SaisieBudgetNatureRecettes.sharedInstance(ec).load(budgetsNature);

        	NSApp.setDefaultCursor(mainView);
        }
	}


	/**
	 *
	 *
	 */
	private void calculerBudget(NSArray arrayBudgetsNature)	{

    	SaisieBudgetNature.sharedInstance(ec).calculerBudget(budgetsNature);

	}


}
