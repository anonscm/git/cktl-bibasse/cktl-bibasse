package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.BudgetSyntheseCtrl;
import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.factory.FactoryBudgetSaisieGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueCredit;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetVoteGestion;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetCalculGestion;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeAction;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SaisieBudgetGestionRecettes {

	private static SaisieBudgetGestionRecettes sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	protected JPanel mainPanel, viewTable, viewTableTotaux;
	protected	JTextField	totalTf;

	private EODisplayGroup eodGestion, eodTotaux;
	private ZEOTable myEOTable, myEOTableTotaux;
	private ZEOTableModel myTableModel, myTableModelTotaux;
	private TableSorter myTableSorter, myTableSorterTotaux;

	private OptionTableCellRenderer	monRendererColor = new OptionTableCellRenderer();
	private TotauxRenderer	rendererTotaux = new TotauxRenderer();

	private CellEditor myCellEditor = new CellEditor(new JTextField());

	private NSMutableArray arrayBudgetsGestion = new NSMutableArray();

	private NSArray	actions;
	NSArray typesCreditSection1 = new NSArray();
	NSArray typesCreditSection2 = new NSArray();

	protected NSArray budgetsGestion = new NSArray();

	private EOOrgan currentOrgan;
	private EOTypeEtat currentTypeEtatSaisie;
	private	EOBudgetSaisie currentBudgetSaisie;

	ListenerGestion myListenerGestion = new ListenerGestion();

	/**
	 *
	 *
	 */
	public SaisieBudgetGestionRecettes(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		initData();
		initView();
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static SaisieBudgetGestionRecettes sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SaisieBudgetGestionRecettes(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	public void clean()	{

		eodGestion.setObjectArray(new NSArray());
		myEOTable.updateData();
		myEOTable.updateUI();
	}

	/**
	 *
	 *
	 */
	public void initView()	{

		mainPanel = new JPanel(new BorderLayout());

		viewTable = new JPanel(new BorderLayout());
		viewTableTotaux = new JPanel(new BorderLayout());

		viewTableTotaux.setBorder(BorderFactory.createEmptyBorder(2,0,2,0));
		viewTableTotaux.setPreferredSize(new Dimension(viewTable.getWidth(), 35));

		JPanel panelGestion = new JPanel(new BorderLayout());
		panelGestion.add(viewTable, BorderLayout.CENTER);
		panelGestion.add(viewTableTotaux, BorderLayout.SOUTH);

		mainPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		mainPanel.add(panelGestion, BorderLayout.CENTER);

		totalTf = new JTextField();
		totalTf.setFont(Configuration.instance().informationTitreTableauFont(ec));
		totalTf.setEditable(false);
		totalTf.setBorder(BorderFactory.createEmptyBorder());
		totalTf.setBackground(new Color(255,105,98));
		totalTf.setForeground(new Color(255,255,255));
		totalTf.setText("TOTAL RECETTES : 0,00 \u20ac");
		totalTf.setHorizontalAlignment(0);

		mainPanel.add(totalTf, BorderLayout.SOUTH);

	}

	/**
	 *
	 *
	 */
	public void initData()	{

		actions = FinderBudgetMasqueGestion.findMasqueActions(ec, NSApp.getExerciceBudgetaire(), "RECETTE");

		typesCreditSection1 = new NSArray(FinderBudgetMasqueCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), "1", EOTypeCredit.TYPE_RECETTE));
		typesCreditSection2 = new NSArray(FinderBudgetMasqueCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), "2", EOTypeCredit.TYPE_RECETTE));

	}

	/**
	 *
	 * @return
	 */
	public NSArray getActions()	{

		return actions;

	}

	/**
	 *
	 * @return
	 */
	public NSArray getTypesCredit()	{

		NSMutableArray typesCreditRecette = new NSMutableArray(typesCreditSection1);
		typesCreditRecette.addObjectsFromArray(typesCreditSection2);

		return typesCreditRecette;
	}

	/**
	 *
	 * @return
	 */
	public BigDecimal getTotalRecettes()	{

		try {
			return new BigDecimal(((NSDictionary)eodTotaux.displayedObjects().objectAtIndex(0)).objectForKey("TOTAL").toString());
		}
		catch (Exception e)	{
			return new BigDecimal(0.0);
		}

	}

	/**
	 *
	 * @return
	 */
	public BigDecimal getCumulForTypeCredit(EOTypeCredit tcd)	{
		return NSApp.computeSumForKey(eodTotaux, tcd.tcdCode());
	}

	/**
	 *
	 * @return
	 */
	public BigDecimal getCumulVoteForTypeCredit(EOTypeCredit tcd)	{
		try {
			return NSApp.computeSumForKey(eodTotaux, tcd.tcdCode()+"VOTE");
		} catch (Exception e)	{
			return new BigDecimal(0.0);
		}
	}

	/**
	 *
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

		eodGestion = new EODisplayGroup();
		eodTotaux = new EODisplayGroup();

		initTableModel();
		initTable();

		myEOTable.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTable.setBorder(BorderFactory.createEmptyBorder());
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

		myEOTableTotaux.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableTotaux.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableTotaux.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableTotaux.setBorder(BorderFactory.createEmptyBorder());
		viewTableTotaux.removeAll();
		viewTableTotaux.setLayout(new BorderLayout());
		viewTableTotaux.add(new JScrollPane(myEOTableTotaux), BorderLayout.CENTER);

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{
		myEOTable = new ZEOTable(myTableSorter, monRendererColor);
		myTableModel.addTableModelListener(myListenerGestion);
		myEOTable.addListener(new ListenerBudgetGestion());
		//myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTableTotaux = new ZEOTable(myTableSorterTotaux, rendererTotaux);
		//myTableSorterTotaux.setTableHeader(myEOTableTotaux.getTableHeader());
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		Vector myCols = new Vector();

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eodGestion, "DESTIN", "RECETTES", 160);
		col1.setAlignment(SwingConstants.LEFT);
		myCols.add(col1);

		for (int i=0;i<typesCreditSection1.count();i++)	{

			EOTypeCredit typeCredit = (EOTypeCredit)typesCreditSection1.objectAtIndex(i);

//			if (currentBudgetSaisie != null &&
//					(currentBudgetSaisie.isBudgetReliquat() || currentBudgetSaisie.isBudgetDbm()) ){
//				ZEOTableModelColumn col = new ZEOTableModelColumn(eodGestion,
//						typeCredit.tcdCode(),
//						typeCredit.tcdCode()+ " (Voté)", 70);
//				col.setAlignment(SwingConstants.RIGHT);
//
//				col.setFormatEdit(Constantes.FORMAT_DECIMAL);
//				col.setFormatDisplay(Constantes.FORMAT_DECIMAL);
//
//				col.setTableCellEditor(myCellEditor);
//				col.setEditable(true);
//
//				myCols.add(col);
//			}

			ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodGestion,
					typeCredit.tcdCode(),
					typeCredit.tcdCode(), 70);
			col2.setAlignment(SwingConstants.RIGHT);

			col2.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
			col2.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);

			col2.setTableCellEditor(myCellEditor);
			col2.setEditable(true);

			myCols.add(col2);

		}

		ZEOTableModelColumn colTotal1= new ZEOTableModelColumn(eodGestion, "TOTAL_1", "Total 1", 70);
		colTotal1.setAlignment(SwingConstants.RIGHT);
		colTotal1.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		myCols.add(colTotal1);

		for (int i=0;i<typesCreditSection2.count();i++)	{

			EOTypeCredit typeCredit = (EOTypeCredit)typesCreditSection2.objectAtIndex(i);

			ZEOTableModelColumn col = new ZEOTableModelColumn(eodGestion,
					typeCredit.tcdCode(),
					typeCredit.tcdCode(), 70);
			col.setAlignment(SwingConstants.RIGHT);
			col.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
			col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
			col.setTableCellEditor(myCellEditor);

			col.setEditable(true);

			myCols.add(col);
		}

		ZEOTableModelColumn colTotal2= new ZEOTableModelColumn(eodGestion, "TOTAL_2", "Total 2", 70);
		colTotal2.setAlignment(SwingConstants.RIGHT);
		colTotal2.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		myCols.add(colTotal2);

		ZEOTableModelColumn colTotal= new ZEOTableModelColumn(eodGestion, "TOTAL", "TOTAL", 70);
		colTotal.setAlignment(SwingConstants.RIGHT);
		colTotal.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		myCols.add(colTotal);

		myTableModel = new ZEOTableModel(eodGestion, myCols);
		myTableSorter = new TableSorter(myTableModel);


		// TOTAUX

		myCols = new Vector();

		col1 = new ZEOTableModelColumn(eodTotaux, "DESTIN", "", 160);
		col1.setAlignment(SwingConstants.CENTER);
		myCols.add(col1);

		for (int i=0;i<typesCreditSection1.count();i++)	{

			EOTypeCredit typeCredit = (EOTypeCredit)typesCreditSection1.objectAtIndex(i);

			ZEOTableModelColumn col = new ZEOTableModelColumn(eodTotaux,
					typeCredit.tcdCode(),
					typeCredit.tcdCode(), 70);
			col.setAlignment(SwingConstants.RIGHT);
			col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
			myCols.add(col);

		}

		colTotal1= new ZEOTableModelColumn(eodTotaux, "TOTAL_1", "Total 1", 70);
		colTotal1.setAlignment(SwingConstants.RIGHT);
		colTotal1.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		myCols.add(colTotal1);

		for (int i=0;i<typesCreditSection2.count();i++)	{

			EOTypeCredit typeCredit = (EOTypeCredit)typesCreditSection2.objectAtIndex(i);

			ZEOTableModelColumn col = new ZEOTableModelColumn(eodTotaux,
					typeCredit.tcdCode(),
					typeCredit.tcdCode(), 70);
			col.setAlignment(SwingConstants.RIGHT);
			col.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
			myCols.add(col);
		}

		colTotal2= new ZEOTableModelColumn(eodTotaux, "TOTAL_2", "Total 2", 70);
		colTotal2.setAlignment(SwingConstants.RIGHT);
		colTotal2.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		myCols.add(colTotal2);

		colTotal= new ZEOTableModelColumn(eodTotaux, "TOTAL", "TOTAL", 70);
		colTotal.setAlignment(SwingConstants.RIGHT);
		colTotal.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		myCols.add(colTotal);

		myTableModelTotaux = new ZEOTableModel(eodTotaux, myCols);
		myTableSorterTotaux = new TableSorter(myTableModelTotaux);

	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public class ListenerGestion implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {

			updateCumuls();
			if (BudgetSyntheseCtrl.sharedInstance(ec).isVisible())	{
				BudgetSyntheseCtrl.sharedInstance(ec).setCurrentOrgan(currentOrgan);
				BudgetSyntheseCtrl.sharedInstance(ec).updateSynthese();
			}

		}
	}

	/**
	 * Listener sur le deuxieme niveau de l'arborescence budgetaire
	 * Mise à jour du troisieme niveau si deuxieme niveau selectionne
	 */
	private class ListenerBudgetGestion implements ZEOTable.ZEOTableListener {

		public void onDbClick() {

			if (currentBudgetSaisie.isBudgetDbm())	{
				SaisieDbmCtrl.sharedInstance(ec).open();

				NSDictionary dico = (NSDictionary) eodGestion.selectedObject();
				EOTypeAction typeAction = null;
				if (dico != null) {
					typeAction = (EOTypeAction) dico.objectForKey("EO_TYPE_ACTION");
				}

				NSArray budgets = FinderBudgetVoteGestion.findBudgetsVote(ec, currentOrgan, null,
						typeAction, NSApp.getExerciceBudgetaire());

				SaisieDbmCtrl.sharedInstance(ec).actualiser(budgets);
			}

		}

		public void onSelectionChanged() {

			if (currentBudgetSaisie != null && currentBudgetSaisie.isBudgetDbm())	{
				if (SaisieDbmCtrl.sharedInstance(ec).isVisible())	{

					NSDictionary dico = (NSDictionary)eodGestion.selectedObject();
					EOTypeAction typeAction = null;
					if (dico != null) {
						typeAction = (EOTypeAction) dico.objectForKey("EO_TYPE_ACTION");
					}

					NSArray budgets = FinderBudgetSaisieGestion.findBudgetsGestion(
							ec,	currentBudgetSaisie, currentOrgan, typeAction);

					SaisieDbmCtrl.sharedInstance(ec).actualiser(budgets);
				}
			}
		}


	}


	/**
	 *
	 *
	 */
	public void updateRow(NSMutableDictionary myRow)	{

		myTableModel.removeTableModelListener(myListenerGestion);

		try {
			NSMutableDictionary selectedRow = new NSMutableDictionary(myRow);

			BigDecimal totalFonctionnement = new BigDecimal(0.0);
			BigDecimal totalEquipement = new BigDecimal(0.0);

			for (int i=0;i<typesCreditSection1.count();i++)	{
				String tcdCode = ((EOTypeCredit)typesCreditSection1.objectAtIndex(i)).tcdCode();
				String montant = NSArray.componentsSeparatedByString(selectedRow.objectForKey(tcdCode).toString(),",").componentsJoinedByString(".");
				totalEquipement = totalEquipement.add(new BigDecimal(montant));
			}

			for (int i=0;i<typesCreditSection2.count();i++)	{
				String tcdCode = ((EOTypeCredit)typesCreditSection2.objectAtIndex(i)).tcdCode();
				String montant = NSArray.componentsSeparatedByString(selectedRow.objectForKey(tcdCode).toString(),",").componentsJoinedByString(".");
				totalFonctionnement = totalFonctionnement.add(new BigDecimal(montant));
			}

			myRow.setObjectForKey(totalEquipement, "TOTAL_1");
			myRow.setObjectForKey(totalFonctionnement, "TOTAL_2");
			myRow.setObjectForKey(totalFonctionnement.add(totalEquipement), "TOTAL");
		}
		catch (Exception e)	{
			EODialogs.runErrorDialog("ERREUR","Veuillez vérifier le format du montant saisi !");
		}

		myTableModel.addTableModelListener(myListenerGestion);
	}


	/**
	 *
	 * @param organ
	 */
	public void setOrgan(EOOrgan organ)	{
		currentOrgan = organ;
		initTableModel();
	}

	public void setTypeEtatSaisie(EOTypeEtat etat)	{
		currentTypeEtatSaisie = etat;
	}

	/**
	 *
	 * @param budget
	 */
	public void setBudgetSaisie(EOBudgetSaisie budget)	{
		currentBudgetSaisie = budget;
	}

	/**
	 *
	 * @param budgets
	 */
	public void setArrayBudgetsGestion(NSArray budgets)	{
		arrayBudgetsGestion = new NSMutableArray(budgets);
	}

	/**
	 *
	 *
	 */
	public void load(NSArray budgets)	{

		budgetsGestion = budgets;

		NSMutableArray arrayGestion = new NSMutableArray();
		NSMutableArray arrayTotaux = new NSMutableArray();
		NSMutableDictionary recordGestion = new NSMutableDictionary();
		NSMutableDictionary recordTotaux = new NSMutableDictionary();

		for (int i=0;i<actions.count();i++)	{

			EOTypeAction action = (EOTypeAction)actions.objectAtIndex(i);

			recordGestion = new NSMutableDictionary();
			recordTotaux = new NSMutableDictionary();

			recordGestion.setObjectForKey(action,"EO_TYPE_ACTION");

			recordGestion.setObjectForKey(action.tyacCode()+" - "+action.tyacAbrege(),"DESTIN");
			recordTotaux.setObjectForKey("TOTAL","DESTIN");

			for (int j=0;j<typesCreditSection1.count();j++)	{
				EOTypeCredit tcd = (EOTypeCredit)typesCreditSection1.objectAtIndex(j);

				try {
					NSMutableArray mesQualifiers = new NSMutableArray();
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction = %@", new NSArray(action)));
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(tcd)));

					NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsGestion, new EOAndQualifier(mesQualifiers));

					if (filtres.count()!=0) {
						if (currentOrgan.orgNiveau().intValue() < 2 ||
								(currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieCr() ) )	{
							EOBudgetCalculGestion budget = (EOBudgetCalculGestion)filtres.objectAtIndex(0);
							recordGestion.setObjectForKey(budget.bdcgCalcul(), tcd.tcdCode());
							recordGestion.setObjectForKey(budget.bdcgCalculVote(), tcd.tcdCode()+"VOTE");
						} else	{
							EOBudgetSaisieGestion budget = (EOBudgetSaisieGestion)filtres.objectAtIndex(0);
							recordGestion.setObjectForKey(budget.bdsgSaisi(), tcd.tcdCode());
							recordGestion.setObjectForKey(budget.bdsgVote(), tcd.tcdCode()+"VOTE");
						}
					} else {
						recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode());
						recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
					}
				}
				catch (Exception e)	{
					System.out.println("SaisieBudgetGestionRecettes.load() ACTION : " + action);
					System.out.println("SaisieBudgetGestionRecettes.load() TYPE CREDIT : " + tcd);

					e.printStackTrace();
					recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode());
					recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
				}

				recordTotaux.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode());
				recordTotaux.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
			}

			recordGestion.setObjectForKey("0","TOTAL_1");

			for (int j=0;j<typesCreditSection2.count();j++)	{
				EOTypeCredit tcd = (EOTypeCredit)typesCreditSection2.objectAtIndex(j);

				try {
					NSMutableArray mesQualifiers = new NSMutableArray();
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction = %@", new NSArray(action)));
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(tcd)));

					NSArray filtres = EOQualifier.filteredArrayWithQualifier(budgetsGestion, new EOAndQualifier(mesQualifiers));

					if (filtres.count() != 0) {
						if (currentOrgan.orgNiveau().intValue() < 2 ||
								(currentOrgan.orgNiveau().intValue() == 2 && NSApp.isSaisieCr() ) )	{
							EOBudgetCalculGestion budget = (EOBudgetCalculGestion)filtres.objectAtIndex(0);
							recordGestion.setObjectForKey(budget.bdcgCalcul(), tcd.tcdCode());
							recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
						} else {
							EOBudgetSaisieGestion budget = (EOBudgetSaisieGestion)filtres.objectAtIndex(0);
							recordGestion.setObjectForKey(budget.bdsgSaisi(), tcd.tcdCode());
							recordGestion.setObjectForKey(budget.bdsgVote(), tcd.tcdCode()+"VOTE");
						}
					} else {
						recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode());
						recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
					}
				} catch (Exception e)	{
					recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode());
					recordGestion.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
				}

				recordTotaux.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode());
				recordTotaux.setObjectForKey(new BigDecimal(0.0), tcd.tcdCode()+"VOTE");
			}

			recordGestion.setObjectForKey("0","TOTAL_1");
			recordGestion.setObjectForKey("0","TOTAL");

			recordTotaux.setObjectForKey("0.00","TOTAL_2");
			recordTotaux.setObjectForKey("0.00","TOTAL");

			arrayGestion.addObject(recordGestion);
		}

		arrayTotaux.addObject(recordTotaux);

		eodGestion.setObjectArray(arrayGestion);
		eodTotaux.setObjectArray(arrayTotaux);

		updateCumuls();

		myEOTable.updateData();
		myEOTableTotaux.updateData();

		myTableModel.fireTableDataChanged();
		myTableModelTotaux.fireTableDataChanged();

		myEOTable.updateUI();
	}


	/**
	 *
	 *
	 */
	public void updateUI()	{

	}

	/**
	 *
	 *
	 */
	public void updateCumuls()	{

		try {
			for (int i=0;i<eodGestion.displayedObjects().count();i++)
				updateRow((NSMutableDictionary)eodGestion.displayedObjects().objectAtIndex(i));

			// Mise a jour des totaux en colonne
			NSMutableDictionary dicoTotal = (NSMutableDictionary)eodTotaux.displayedObjects().objectAtIndex(0);
			for (int j=0;j<typesCreditSection1.count();j++)	{
				EOTypeCredit tcd = (EOTypeCredit)typesCreditSection1.objectAtIndex(j);
				dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, tcd.tcdCode()), tcd.tcdCode());
				dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, tcd.tcdCode()+"VOTE"), tcd.tcdCode()+"VOTE");
			}

			dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, "TOTAL_1"), "TOTAL_1");

			for (int j=0;j<typesCreditSection2.count();j++)	{
				EOTypeCredit tcd = (EOTypeCredit)typesCreditSection2.objectAtIndex(j);
				dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, tcd.tcdCode()), tcd.tcdCode());
				dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, tcd.tcdCode()+"VOTE"), tcd.tcdCode()+"VOTE");
			}

			dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, "TOTAL_2"), "TOTAL_2");
			dicoTotal.setObjectForKey(NSApp.computeSumForKey(eodGestion, "TOTAL"), "TOTAL");

			myEOTable.updateUI();
			myEOTableTotaux.updateUI();

			BigDecimal total = new BigDecimal(((NSDictionary)eodTotaux.displayedObjects().objectAtIndex(0)).objectForKey("TOTAL").toString());

			totalTf.setText("TOTAL RECETTES : " + ConstantesCocktail.FORMAT_DECIMAL.format(total) + " \u20ac");
		}
		catch (Exception e)	{

		}
	}


	/**
	 *
	 *
	 */
	public void save()	{

		try {

			for (int i=0;i<eodGestion.displayedObjects().count();i++)	{

				NSDictionary dico = (NSDictionary)eodGestion.displayedObjects().objectAtIndex(i);

				EOTypeAction action = (EOTypeAction)dico.objectForKey("EO_TYPE_ACTION");

				for (int j=0;j<typesCreditSection1.count();j++)	{

					EOTypeCredit tcd = (EOTypeCredit)typesCreditSection1.objectAtIndex(j);
					String montant = StringCtrl.replace(dico.objectForKey(tcd.tcdCode()).toString(), ",", ".");

					NSMutableArray mesQualifiers = new NSMutableArray();
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction = %@", new NSArray(action)));
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(tcd)));

					NSArray filtres = EOQualifier.filteredArrayWithQualifier(arrayBudgetsGestion, new EOAndQualifier(mesQualifiers));
					EOBudgetSaisieGestion budget=null;
					if (filtres.count() == 0)	{
						if (new BigDecimal(montant).floatValue()!=0.0) {
							budget=new FactoryBudgetSaisieGestion().creerEOBudgetSaisieGestion(ec, FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
								tcd, action, currentOrgan, tcd.exercice(), currentBudgetSaisie, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0));
							arrayBudgetsGestion.addObject(budget);
						}
					} else
						budget = (EOBudgetSaisieGestion)filtres.objectAtIndex(0);

//					EOBudgetSaisieGestion budget = FinderBudgetSaisieGestion.findBudgetGestion(
//							ec, currentBudgetSaisie, currentOrgan, action, tcd);

					if (budget!=null) {
						if (budget.bdsgSaisi().floatValue()!=new BigDecimal(montant).floatValue()) {
							budget.setBdsgMontant(budget.bdsgVote().add(new BigDecimal(montant)));
							budget.setBdsgSaisi(new BigDecimal(montant));
						}
					}
				}

				for (int j=0;j<typesCreditSection2.count();j++)	{

					EOTypeCredit tcd = (EOTypeCredit)typesCreditSection2.objectAtIndex(j);
					String montant = StringCtrl.replace(dico.objectForKey(tcd.tcdCode()).toString(), ",", ".");

					NSMutableArray mesQualifiers = new NSMutableArray();
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeAction = %@", new NSArray(action)));
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeCredit = %@", new NSArray(tcd)));

					NSArray filtres = EOQualifier.filteredArrayWithQualifier(arrayBudgetsGestion, new EOAndQualifier(mesQualifiers));
					EOBudgetSaisieGestion budget=null;
					if (filtres.count() == 0)	{
						if (new BigDecimal(montant).floatValue()!=0.0) {
							budget=new FactoryBudgetSaisieGestion().creerEOBudgetSaisieGestion(ec, FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS),
								tcd, action, currentOrgan, tcd.exercice(), currentBudgetSaisie, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0));
							arrayBudgetsGestion.addObject(budget);
						}
					} else
						budget = (EOBudgetSaisieGestion)filtres.objectAtIndex(0);

					if (budget!=null) {
						budget.setBdsgMontant(budget.bdsgVote().add(new BigDecimal(montant)));
						budget.setBdsgSaisi(new BigDecimal(montant));
					}
				}
			}
			ec.saveChanges();

		}
		catch (Exception e)	{
			EODialogs.runErrorDialog("ERREUR","Erreur d'enregistrement du budget !");
			e.printStackTrace();
		}
	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class OptionTableCellRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_SAISIE=new Color(255,255,255);
		public final Color COULEUR_TEXTE_SAISIE=new Color(0,0,0);

		public final Color COULEUR_FOND_ACTIONS = new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_ACTIONS =new Color(0,0, 0);

		public final Color COULEUR_FOND_UB=new Color(230,230,230);
		public final Color COULEUR_TEXTE_UB=new Color(100,100,100);

		public final Color COULEUR_FOND_TOTAUX=new Color(220,220,220);
		public final Color COULEUR_TEXTE_TOTAUX=new Color(100,100,100);

		public final Color COULEUR_FOND_TOTAL_GENERAL=new Color(200,200,200);
		public final Color COULEUR_TEXTE_TOTAL_GENERAL=new Color(0,0,0);

		public final Color COULEUR_FOND_TOTAUX_NEG=new Color(0,0,0);
		public final Color COULEUR_TEXTE_TOTAUX_NEG=new Color(255,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (column == 0)	{
				leComposant.setBackground(COULEUR_FOND_ACTIONS);
				leComposant.setForeground(COULEUR_TEXTE_ACTIONS);
			}
			else	{
				if (column == typesCreditSection1.count()+1)	{
					leComposant.setBackground(COULEUR_FOND_TOTAUX);
					leComposant.setForeground(COULEUR_TEXTE_TOTAUX);
				}
				else	{
					if (column == typesCreditSection1.count()+typesCreditSection2.count()+2) {
						leComposant.setBackground(COULEUR_FOND_TOTAUX);
						leComposant.setForeground(COULEUR_TEXTE_TOTAUX);
					}
					else	{
						if (column == typesCreditSection1.count()+typesCreditSection2.count()+3) {
							leComposant.setBackground(COULEUR_FOND_TOTAL_GENERAL);
							leComposant.setForeground(COULEUR_TEXTE_TOTAL_GENERAL);
							leComposant.setFont(new Font("Arial", Font.BOLD, 12));
						}
						else	{
							if (currentBudgetSaisie.isVote()
									|| (currentTypeEtatSaisie != null && currentTypeEtatSaisie.isCloture())
									|| !NSApp.fonctionSaisie()
									|| (currentTypeEtatSaisie.isValide() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
									|| (currentTypeEtatSaisie.isControle() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
									|| (currentOrgan.orgNiveau().intValue() < 3  && NSApp.isSaisieCr())
									|| (currentOrgan.orgNiveau().intValue() < 2  && NSApp.isSaisieUb())
									)
							{
								leComposant.setBackground(COULEUR_FOND_UB);
								leComposant.setForeground(COULEUR_TEXTE_UB);
							}
							else	{
								leComposant.setBackground(COULEUR_FOND_SAISIE);
								leComposant.setForeground(COULEUR_TEXTE_SAISIE);
							}
						}
					}
				}
			}

			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class TotauxRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_TOTAL_GENERAL=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_TOTAL_GENERAL=new Color(0,0,0);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			leComposant.setBackground(COULEUR_FOND_TOTAL_GENERAL);
			leComposant.setForeground(COULEUR_TEXTE_TOTAL_GENERAL);
			leComposant.setFont(new Font("Arial", Font.BOLD, 12));

			return leComposant;
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class CellEditor extends ZEOTableModelColumn.ZEONumFieldTableCellEditor {

		private  JTextField myTextField;

	    public CellEditor(JTextField textField) {
            super(textField, ConstantesCocktail.FORMAT_DECIMAL);
        }

        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            myTextField = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);

            if (currentBudgetSaisie.isVote()
					|| (currentTypeEtatSaisie != null && currentTypeEtatSaisie.isCloture())
					|| !NSApp.fonctionSaisie()
					|| (currentTypeEtatSaisie.isValide() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
					|| (currentTypeEtatSaisie.isControle() && !NSApp.hasFonction(ConstantesCocktail.ID_FCT_CONTROLE))
					|| (currentOrgan.orgNiveau().intValue() < 3  && NSApp.isSaisieCr())
					|| (currentOrgan.orgNiveau().intValue() < 2  && NSApp.isSaisieUb())
					)
             	return null;
            else	{
            	myTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            	myTextField.setEditable(true);
            }
			return myTextField;
        }
    }

}
