package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eocontrol.EOEditingContext;

public class SaisieBudgetGestion {

	private static SaisieBudgetGestion sharedInstance;
	private EOEditingContext ec;
	protected JPanel mainPanel;
		
	/**
	 * 
	 *
	 */
	public SaisieBudgetGestion(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		initView();	
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieBudgetGestion sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieBudgetGestion(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
		mainPanel = new JPanel(new BorderLayout());

		JSplitPane splitPane = ZUiUtil.buildVerticalSplitPane(
				SaisieBudgetGestionDepenses.sharedInstance(ec).getPanel(),
				SaisieBudgetGestionRecettes.sharedInstance(ec).getPanel(),
				0.5, 0.5);
				
        mainPanel.add(splitPane, BorderLayout.CENTER);        
        
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}

	/**
	 * 
	 *
	 */
	public void clean()	{
		SaisieBudgetGestionDepenses.sharedInstance(ec).clean();
		SaisieBudgetGestionRecettes.sharedInstance(ec).clean();
	}
}
