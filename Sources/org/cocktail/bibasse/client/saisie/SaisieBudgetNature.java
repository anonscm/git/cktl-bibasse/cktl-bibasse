package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.finder.FinderPlanComptable;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetParametres;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNature;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.process.budget.ProcessBudgetFactory;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SaisieBudgetNature {

	private static SaisieBudgetNature sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;
	protected JPanel mainPanel;
		
	/**
	 * 
	 *
	 */
	public SaisieBudgetNature(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		initView();	
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieBudgetNature sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieBudgetNature(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
		mainPanel = new JPanel(new BorderLayout());

		JSplitPane splitPane = ZUiUtil.buildHorizontalSplitPane(
				SaisieBudgetNatureDepenses.sharedInstance(ec).getPanel(),
				SaisieBudgetNatureRecettes.sharedInstance(ec).getPanel(), 0.5, 0.5);
				
        mainPanel.add(splitPane, BorderLayout.CENTER);        
	}

	/**
	 * 
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}
	
	
	/**
	 * 
	 *
	 */
	public void clean()	{
		SaisieBudgetNatureDepenses.sharedInstance(ec).clean();
		SaisieBudgetNatureRecettes.sharedInstance(ec).clean();
	}
	
	/**
	 * 
	 *
	 */
	public void calculerBudget(NSMutableArray budgets)	{
				
		ProcessBudgetFactory myProcessBudgetFactory = null;

		EOOrgan organ=null;
		EOExercice exercice=null;
		EOBudgetSaisie budgetSaisie=null;
		
		if (budgets.count()>0) {
			EOBudgetSaisieNature temp=(EOBudgetSaisieNature)budgets.objectAtIndex(0);
			organ=temp.organ();
			exercice=temp.exercice();
			budgetSaisie=temp.budgetSaisie();
		}
		
		try {

			NSMutableArray mesQualifiers = new NSMutableArray();
			NSArray filtres = new NSArray();
			
			myProcessBudgetFactory = new ProcessBudgetFactory(true, ProcessBudgetFactory.PROCESS_PROVISOIRE);

			EOTypeEtat etatEnCours=FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_EN_COURS);
			
			// CAF
			EOPlanComptable planco=FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_CAF"),
					NSApp.getExerciceBudgetaire());
			mesQualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("planComptable = %@", new NSArray(planco)));
			filtres = EOQualifier.filteredArrayWithQualifier(budgets, new EOAndQualifier(mesQualifiers));
			EOBudgetSaisieNature caf=null;
			if (filtres.count()>0)
				caf = (EOBudgetSaisieNature)filtres.objectAtIndex(0);
			else {
				caf=myProcessBudgetFactory.creerUneLigneNatureReserve(ec, etatEnCours, planco, organ, exercice, budgetSaisie);
				budgets.addObject(caf);
			}

			// IAF
			planco=FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_IAF"),
					NSApp.getExerciceBudgetaire());
			mesQualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("planComptable = %@", new NSArray(planco)));
			filtres = EOQualifier.filteredArrayWithQualifier(budgets, new EOAndQualifier(mesQualifiers));
			EOBudgetSaisieNature iaf=null;
			if (filtres.count()>0)
				iaf = (EOBudgetSaisieNature)filtres.objectAtIndex(0);
			else {
				iaf=myProcessBudgetFactory.creerUneLigneNatureReserve(ec, etatEnCours, planco, organ, exercice, budgetSaisie);
				budgets.addObject(iaf);
			}
			
			// DEPENSE FONC
			planco=FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_EXCEDENT_FONC"),
					NSApp.getExerciceBudgetaire());
			mesQualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("planComptable = %@",
					new NSArray(FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_EXCEDENT_FONC"),
							NSApp.getExerciceBudgetaire()))));
			filtres = EOQualifier.filteredArrayWithQualifier(budgets, new EOAndQualifier(mesQualifiers));
			EOBudgetSaisieNature resultatBrutDepense=null;
			if (filtres.count()>0)
				resultatBrutDepense = (EOBudgetSaisieNature)filtres.objectAtIndex(0);
			else {
				resultatBrutDepense=myProcessBudgetFactory.creerUneLigneNatureReserve(ec, etatEnCours, planco, organ, exercice, budgetSaisie);
				budgets.addObject(resultatBrutDepense);
			}
			
			// RECETTE FONC
			planco=FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_DEFICIT_FONC"),
					NSApp.getExerciceBudgetaire());
			mesQualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("planComptable = %@",
					new NSArray(FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_DEFICIT_FONC"),
							NSApp.getExerciceBudgetaire()))));
			filtres = EOQualifier.filteredArrayWithQualifier(budgets, new EOAndQualifier(mesQualifiers));
			EOBudgetSaisieNature resultatBrutRecette= null;
			if (filtres.count()>0)
				resultatBrutRecette = (EOBudgetSaisieNature)filtres.objectAtIndex(0);
			else {
				resultatBrutRecette=myProcessBudgetFactory.creerUneLigneNatureReserve(ec, etatEnCours, planco, organ, exercice, budgetSaisie);
				budgets.addObject(resultatBrutRecette);
			}
			
			// AUGMENTATION FDR
			planco=FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_AUGMENTATION_FDR"),
					NSApp.getExerciceBudgetaire());
			mesQualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("planComptable = %@",
					new NSArray(FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_AUGMENTATION_FDR"),
							NSApp.getExerciceBudgetaire()))));
			filtres = EOQualifier.filteredArrayWithQualifier(budgets, new EOAndQualifier(mesQualifiers));
			EOBudgetSaisieNature augmentationFdr = null;
			if (filtres.count()>0)
				augmentationFdr = (EOBudgetSaisieNature)filtres.objectAtIndex(0);
			else {
				augmentationFdr=myProcessBudgetFactory.creerUneLigneNatureReserve(ec, etatEnCours, planco, organ, exercice, budgetSaisie);
				budgets.addObject(augmentationFdr);
			}
			
			// DIMINUTION FDR
			planco=FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_DIMINUTION_FDR"),
					NSApp.getExerciceBudgetaire());
			mesQualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("planComptable = %@",
					new NSArray(FinderPlanComptable.findCompte(ec, FinderBudgetParametres.getValue(ec, NSApp.getExerciceBudgetaire(), "COMPTE_DIMINUTION_FDR"),
							NSApp.getExerciceBudgetaire()))));
			filtres = EOQualifier.filteredArrayWithQualifier(budgets, new EOAndQualifier(mesQualifiers));
			EOBudgetSaisieNature diminutionFdr= null;
			if (filtres.count()>0)
				diminutionFdr = (EOBudgetSaisieNature)filtres.objectAtIndex(0);
			else {
				diminutionFdr=myProcessBudgetFactory.creerUneLigneNatureReserve(ec, etatEnCours, planco, organ, exercice, budgetSaisie);
				budgets.addObject(diminutionFdr);
			}
			
			NSMutableArray plancosCredit = new NSMutableArray(SaisieBudgetNatureDepenses.sharedInstance(ec).getMasqueNature());
			plancosCredit.addObjectsFromArray(SaisieBudgetNatureRecettes.sharedInstance(ec).getMasqueNature());
			
	        boolean compte777EnProduit=false;
	        String param=FinderBudgetParametres.getValue(ec, exercice, EOBudgetParametres.param_777_EN_PRODUIT);
	        if (param!=null && param.equals("OUI"))
	        	compte777EnProduit=true;

	        boolean compte775FDR=false;
	        param=FinderBudgetParametres.getValue(ec, exercice, EOBudgetParametres.param_775_FDR);
	        if (param!=null && param.equals("OUI"))
	        	compte775FDR=true;

			myProcessBudgetFactory.calculBudgetNatureComptesBudgetaires(ec, budgets, resultatBrutDepense, resultatBrutRecette,
					caf, iaf, augmentationFdr, diminutionFdr, compte777EnProduit, compte775FDR);

			myProcessBudgetFactory.calculBudgetNatureSaisiComptesBudgetaires(ec, budgets, resultatBrutDepense, resultatBrutRecette,
					caf, iaf, augmentationFdr, diminutionFdr, compte777EnProduit, compte775FDR);
			
			ec.saveChanges();
		}
		catch (Exception e)	{
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur de calcul du budget par nature : " + e.getMessage());
		}
	}

}
