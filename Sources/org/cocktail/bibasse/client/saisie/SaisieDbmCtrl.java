package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieGestion;
import org.cocktail.bibasse.client.metier.EOBudgetVoteGestion;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SaisieDbmCtrl {


	private static SaisieDbmCtrl sharedInstance;

	protected	JFrame window;

	protected JPanel viewTable;
	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	protected ActionClose 		actionClose = new ActionClose();

	protected	EOBudgetSaisieGestion currentBudgetGestion;

	/**
	 *
	 *
	 */
	public SaisieDbmCtrl(EOEditingContext editingContext)	{

		super();

		initGUI();

		initView();

	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static SaisieDbmCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new SaisieDbmCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	public void initView()	{

        window = new JFrame();//Dialog(mainFrame);
        window.setSize(500, 200);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

		JPanel panelDepenses = new JPanel(new BorderLayout());

		JTextField titreDepenses = new JTextField("Détail du budget voté");
		titreDepenses.setForeground(Color.WHITE);
		titreDepenses.setBackground(new Color(80,130,145));
		titreDepenses.setEditable(false);
		titreDepenses.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		titreDepenses.setHorizontalAlignment(0);

		panelDepenses.add(titreDepenses, BorderLayout.NORTH);
		panelDepenses.add(viewTable, BorderLayout.CENTER);

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 25));
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.add(panelDepenses, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);


		window.setContentPane(mainView);
	}



	/**
	 *
	 *
	 */
	public void open()	{

        window.show();

	}

	/**
	 *
	 *
	 */
	public void close()	{
		window.hide();
	}


	/**
	 *
	 * @return
	 */
	public boolean isVisible()	 {
		return window.isVisible();
	}

	/**
	 *
	 *
	 */
	public void actualiser(NSArray budgets)	{

		NSMutableArray myArray = new NSMutableArray();
		NSMutableDictionary myRecord = new NSMutableDictionary();

		for (int i = 0; i < budgets.count(); i++)	{
			// FLA : systeme a revoir. On doit être capable de savoir quel type de budgets on a en entrée.
			// ou adapter le code ci-dessous pour afficher les données associées à un Budget en cours de saisie.
			if (!(budgets.objectAtIndex(i) instanceof EOBudgetVoteGestion)) {
				break;
			}

			myRecord = new NSMutableDictionary();

			EOBudgetVoteGestion budget = (EOBudgetVoteGestion)budgets.objectAtIndex(i);

			myRecord.setObjectForKey(budget.typeAction().tyacLibelle(), "ACTION");
			myRecord.setObjectForKey(budget.typeCredit().tcdCode(), "TCD");
			myRecord.setObjectForKey(budget.bdvgVotes(), "VOTE");
			myRecord.setObjectForKey(budget.bdvgOuverts(), "OUVERT");

			myArray.addObject(myRecord);
		}

		eod.setObjectArray(myArray);
		myEOTable.updateData();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	private void initGUI() {

		eod = new EODisplayGroup();

        viewTable = new JPanel();

		initTableModel();
		initTable();

		myEOTable.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
//		myTableSorter.setTableHeader(myEOTable.getTableHeader());

	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		Vector myCols = new Vector();

		ZEOTableModelColumn col = new ZEOTableModelColumn(eod, "ACTION", "Action", 100);
		col.setAlignment(SwingConstants.LEFT);
		col.setEditable(false);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "TCD", "TCD", 80);
		col.setAlignment(SwingConstants.CENTER);
		col.setEditable(false);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "VOTE", "Voté", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		myCols.add(col);

		col = new ZEOTableModelColumn(eod, "OUVERT", "Ouvert", 80);
		col.setAlignment(SwingConstants.RIGHT);
		col.setEditable(false);
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }

        public void actionPerformed(ActionEvent e) {
        	window.hide();
        }
	}

}
