package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.BudgetSyntheseCtrl;
import org.cocktail.bibasse.client.finder.FinderOrgan;
import org.cocktail.bibasse.client.finder.FinderTypeOrgan;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeOrgan;
import org.cocktail.bibasse.client.utils.DateCtrl;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class OrganPopupCtrl extends EODialogController {
	
	private static OrganPopupCtrl sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;
	
	public JPanel mainPanel;
	
	public JComboBox popupEtab, popupUb, popupCr;
	
	protected	JFrame window;
	
	protected ListenerEtab myListenerEtab = new ListenerEtab();
	protected ListenerUb myListenerUb = new ListenerUb();
	protected ListenerCr myListenerCr = new ListenerCr();
	
	private org.cocktail.bibasse.client.metier.EOOrgan currentOrgan;
	private NSArray privateOrgans;
	private EOBudgetSaisie currentBudgetSaisie;
	
	/**
	 * 
	 *
	 */
	public OrganPopupCtrl(EOEditingContext editingContext)	{

		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
				
		initView();
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OrganPopupCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new OrganPopupCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
		popupEtab = new JComboBox();
		popupEtab.setSize(100,21);
		popupUb = new JComboBox();
		popupCr = new JComboBox();
		
		popupEtab.setBackground(new Color(100,100,100));
		popupEtab.setForeground(new Color(0,0,0));

		popupUb.setBackground(new Color(100,100,100));
		popupUb.setForeground(new Color(0,0,0));

		popupCr.setBackground(new Color(100,100,100));
		popupCr.setForeground(new Color(0,0,0));

		fillPopupEtabs();
		popupEtab.setSelectedIndex(0);
		etabHasChanged();
				
		JPanel panelPopup = new JPanel(new FlowLayout());
		
		JPanel panelEtab = new JPanel(new BorderLayout());
		panelEtab.setPreferredSize(new Dimension(80,23));
		panelEtab.add(popupEtab,BorderLayout.CENTER);

		JPanel panelUb = new JPanel(new BorderLayout());
		panelUb.setPreferredSize(new Dimension(80,23));
		panelUb.add(popupUb,BorderLayout.CENTER);

		JPanel panelCr = new JPanel(new BorderLayout());
		panelCr.setPreferredSize(new Dimension(80,23));
		panelCr.add(popupCr,BorderLayout.CENTER);
					
		panelPopup.add(panelEtab);
		panelPopup.add(panelUb);
		panelPopup.add(panelCr);		
		
		System.out.println("OrganPopupCtrl.initView()");
		popupCr.setEnabled(NSApp.isSaisieCr());

		popupEtab.addActionListener(myListenerEtab);
		
		mainPanel = new JPanel(new BorderLayout());		

		mainPanel.add(panelPopup, BorderLayout.CENTER);
	}	
	
	
	/**
	 * 
	 *
	 */
	public void cleanPopups()	{
		
		popupEtab.removeActionListener(myListenerEtab);
		popupUb.removeActionListener(myListenerUb);
		popupCr.removeActionListener(myListenerCr);

		popupEtab.setSelectedIndex(0);
		popupUb.setSelectedIndex(0);
		popupCr.setSelectedIndex(0);

		popupEtab.addActionListener(myListenerEtab);
		popupUb.addActionListener(myListenerUb);
		popupCr.addActionListener(myListenerCr);

	}
	
	
	/**
	 * 
	 * @param organs
	 */
	public void setPrivateOrgans(NSArray organs)	{
		privateOrgans = organs;
		
		fillPopupEtabs();
		popupEtab.setSelectedIndex(0);
		etabHasChanged();

	}
	
	
	/**
	 * 
	 *
	 */
	public void fillPopupEtabs()	{
		
		NSArray etabs = (NSArray)NSApp.getUserOrgans().valueForKeyPath("organ.orgEtab");
		
		NSMutableArray listeEtabs = new NSMutableArray(filterDoublons(new NSMutableArray(etabs)));

		popupEtab.removeAllItems();
		popupEtab.addItem("****");
		
		for (int i=0;i<listeEtabs.count();i++)	{
			System.out.println("organ For : ");
			System.out.println((String)listeEtabs.objectAtIndex(i));
			popupEtab.addItem(FinderOrgan.findOrganForNiveauAndLibelle(ec, new Integer(1), (String)listeEtabs.objectAtIndex(i)));
		}
	}
	
	/**
	 * 
	 * @param organPere
	 */
	public void fillPopUbs(EOOrgan organPere)	{
		
		popupUb.removeAllItems();
		popupUb.addItem("****");

		NSMutableArray listeUbs = new NSMutableArray(organPere.organFils());
		EOSortOrdering.sortArrayUsingKeyOrderArray(listeUbs, new NSArray(new EOSortOrdering("orgUb", EOSortOrdering.CompareAscending)));
		
		for (int i=0;i<listeUbs.count();i++)
			popupUb.addItem(listeUbs.objectAtIndex(i));

	}
	
	/**
	 * 
	 * @return
	 */
	public JPanel getPanel()	{
		return mainPanel;
	}
	
	/**
	 * 
	 * @param organ
	 */
	public void setSelectedOrgan(EOOrgan organ)	{
		
		System.out.println("OrganPopupCtrl.setSelectedOrgan() CURRENT ORGAN : "+ currentOrgan);
		
		EOOrgan organEtab = FinderOrgan.findOrganAvecNiveau(ec, 1, organ);
		popupEtab.setSelectedItem(organEtab);

		EOOrgan organUb = FinderOrgan.findOrganAvecNiveau(ec, 2, organ);
		popupUb.setSelectedItem(organUb);
		
		if (NSApp.isSaisieCr())	{
			EOOrgan organCr = FinderOrgan.findOrganAvecNiveau(ec, 3, organ);
			popupCr.setSelectedItem(organCr);
		}
		
	}

	
	/**
	 * 
	 */
	public void updateNiveauSaisie() {
		
		popupCr.setEnabled(NSApp.isSaisieCr());
		
	}
	
	/**
	 * 
	 *
	 */
	public void setBudgetSaisie(EOBudgetSaisie budgetSaisie)	{
		
		currentBudgetSaisie = budgetSaisie;
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	public EOOrgan getCurrentOrgan()	{
		return currentOrgan;
	}
	
	
	
	/**
	 * 
	 *
	 */
	public void etabHasChanged()	{
		
//        CRICursor.setWaitCursor(mainPanel, true);
		
		popupUb.removeActionListener(myListenerUb);
		popupCr.removeActionListener(myListenerCr);
		popupUb.removeAllItems();
		popupUb.addItem("****");

		popupCr.removeAllItems();
		popupCr.addItem("****");

		if (popupEtab.getSelectedIndex() == 0)	{
			currentOrgan = null;
			return;
		}

		currentOrgan = (EOOrgan)popupEtab.getSelectedItem();
		if (currentOrgan==null)
			return;
		
		popupEtab.removeActionListener(myListenerEtab);

		NSMutableArray listeUbs = new NSMutableArray((NSArray)privateOrgans.valueForKeyPath("organ"));
		System.out.println("\tRecup des ubs : " + listeUbs.count() +" - "+ DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat("organPere = %@", new NSArray(currentOrgan));				
		EOQualifier.filterArrayWithQualifier(listeUbs,myQualifier);

		System.out.println("\tFin du filtre - Count " + listeUbs.count()+ " - " +DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

		listeUbs = new NSMutableArray(filterDoublons(listeUbs));

		System.out.println("\tListeUbs count : " + listeUbs.count() +" - "+ DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

		EOSortOrdering.sortArrayUsingKeyOrderArray(listeUbs, new NSArray(new EOSortOrdering("orgUb", EOSortOrdering.CompareAscending)));
		
		System.out.println("\tListeUbs sort - "+ DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

		for (int i=0;i<listeUbs.count();i++)
			popupUb.addItem(listeUbs.objectAtIndex(i));

		popupUb.addActionListener(myListenerUb);
		popupCr.addActionListener(myListenerCr);

		System.out.println("OrganPopupCtrl.etabHasChanged()");
		SaisieBudget.sharedInstance(ec).organHasChanged(currentOrgan);

		popupEtab.addActionListener(myListenerEtab);
		BudgetSyntheseCtrl.sharedInstance(ec).close();	
		
		System.out.println("***** FIN EtabHasChanged "+ DateCtrl.dateToString(new NSTimestamp(), "%H:%M:%S:%F"));

		//   CRICursor.setWaitCursor(mainPanel, false);

	}
	
	/**
	 * 
	 * @param liste
	 */
	public NSArray filterDoublons(NSMutableArray liste)	{
		
		NSMutableArray retour = new NSMutableArray();
		
		for (int i=0;i<liste.count();i++)	{
			if (!retour.containsObject(liste.objectAtIndex(i)))
				retour.addObject(liste.objectAtIndex(i));
		}
		
		return retour;
	}
	
	/**
	 * 
	 *
	 */
	public void ubHasChanged()	{
		
		if (popupUb.getSelectedIndex() == 0)	{
			if (popupEtab.getSelectedIndex() == 0)
				currentOrgan = null;
			else
				currentOrgan = (EOOrgan)popupEtab.getSelectedItem();
		}
		else	{
			currentOrgan = (EOOrgan)popupUb.getSelectedItem();
			
			if (NSApp.isSaisieUb())	{
				
				
			}
			else	{
				
				popupCr.removeActionListener(myListenerCr);
				
				popupCr.removeAllItems();
				popupCr.addItem("****");
				
				NSMutableArray listeCrs = new NSMutableArray((NSArray)privateOrgans.valueForKey("organ"));
				
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORGAN_PERE_KEY+" = %@", new NSArray(currentOrgan)));
				
				if (currentBudgetSaisie != null && currentBudgetSaisie.isBudgetConvention())
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.TYPE_ORGAN_KEY+" = %@", new NSArray(FinderTypeOrgan.findTypeOrgan(ec, EOTypeOrgan.TYPE_CONVENTION))));

				EOQualifier.filterArrayWithQualifier(listeCrs, new EOAndQualifier(mesQualifiers));
				
				EOSortOrdering.sortArrayUsingKeyOrderArray(listeCrs, new NSArray(new EOSortOrdering(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending)));
				for (int i=0;i<listeCrs.count();i++)
					popupCr.addItem(listeCrs.objectAtIndex(i));
			}
			
			popupCr.addActionListener(myListenerCr);		
		}
		
		BudgetSyntheseCtrl.sharedInstance(ec).close();
		SaisieBudget.sharedInstance(ec).organHasChanged(currentOrgan);
	}
	
	
	
	/**
	 * 
	 *
	 */
	public void crHasChanged()	{
		
		if (popupCr.getSelectedIndex() == 0)	{
			if (popupUb.getSelectedIndex() == 0)
				if (popupEtab.getSelectedIndex() == 0)
					currentOrgan = null;
				else
					currentOrgan = (EOOrgan)popupEtab.getSelectedItem();
			else
				currentOrgan = (EOOrgan)popupUb.getSelectedItem();
		}
		else	{
			currentOrgan = (EOOrgan)popupCr.getSelectedItem();
		}
		
		BudgetSyntheseCtrl myCtrlOrgan = BudgetSyntheseCtrl.sharedInstance(ec);
		
		if (myCtrlOrgan.isVisible())
			myCtrlOrgan.setCurrentOrgan(currentOrgan);

		SaisieBudget.sharedInstance(ec).organHasChanged(currentOrgan);
	}
	
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerEtab implements ActionListener {
		
		public ListenerEtab() {super();}
		public void actionPerformed(ActionEvent anAction) {
			etabHasChanged();
		}
	}
	
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerUb implements ActionListener {
		
		public ListenerUb() {super();}
		public void actionPerformed(ActionEvent anAction) {
			ubHasChanged();
		}
	}
	
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerCr implements ActionListener {
		
		public ListenerCr() {super();}
		public void actionPerformed(ActionEvent anAction) {
			crHasChanged();
		}
	}
	
	
}
