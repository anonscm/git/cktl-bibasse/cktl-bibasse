package org.cocktail.bibasse.client.saisie;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisieNatureLolf;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.process.budget.ProcessBudgetFactory;
import org.cocktail.bibasse.client.utils.XWaitingDialog;
import org.cocktail.bibasse.client.zutil.ui.ZCommentPanel;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableArray;

public class SaisieNatureLolfCtrl {

	private static SaisieNatureLolfCtrl sharedInstance;
	private EOEditingContext ec;

	protected   ApplicationClient NSApp;
	protected	JFrame mainFrame;
	protected	JDialog mainWindow;

	protected	JPanel mainView;


	protected 	EOBudgetSaisie			currentBudgetSaisie = null;
	protected 	EOBudgetSaisieNatureLolf currentBudgetNatureLolf = null;
	protected	EOTypeSaisie 			currentTypeSaisie = null;
	protected	EOOrgan 				currentOrgan;
	protected	EOTypeEtat				currentTypeEtatOrgan = null;

	protected XWaitingDialog waitingFrame;

	public JTextField typeSaisieBudget;
	public JTextField informations;

	public ZCommentPanel infosPanel ;

	protected	NSMutableArray budgetsGestion = new NSMutableArray();
	protected	NSMutableArray budgetsNature = new NSMutableArray();

	protected ProcessBudgetFactory myProcessBudgetFactory;

	/**
	 * 
	 *
	 */
	public SaisieNatureLolfCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		gui_initView();	
	}


	/**
	 * 
	 *
	 */
	public void gui_initView()	{

		mainWindow = new JDialog(mainFrame, "Saisie du budget", false);
		mainWindow.setSize(900, 720);

		mainView=new JPanel(new BorderLayout());
		mainView.add(SaisieNatureLolf.sharedInstance(ec).getPanel(), BorderLayout.CENTER);

		mainWindow.setContentPane(mainView);
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieNatureLolfCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieNatureLolfCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 *
	 */
	public void open()	{
		if (!NSApp.repartitionNatureLolf())
			return;
		
		String title = ConstantesCocktail.APPLICATION_NAME;//+" - Budget " + currentTypeSaisie.tysaLibelle() + " "  + NSApp.getExerciceBudgetaire().exeExercice() ;
		mainWindow.setTitle(title);				

		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();
	}

	/**
	 * 
	 *
	 */
	public void close()	{
		mainWindow.dispose();
	}

	/**
	 * 
	 * @param typeBudget
	 */
	public void setBudgetSaisie(EOBudgetSaisie budget)	{
		currentBudgetSaisie = budget;
		SaisieNatureLolf.sharedInstance(ec).setBudgetSaisie(currentBudgetSaisie);
	}
}
