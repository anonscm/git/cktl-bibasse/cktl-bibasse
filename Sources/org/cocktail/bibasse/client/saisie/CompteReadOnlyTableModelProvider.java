package org.cocktail.bibasse.client.saisie;

import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel.IZEOTableModelProvider;

import com.webobjects.foundation.NSDictionary;

public class CompteReadOnlyTableModelProvider implements IZEOTableModelProvider {

	public static final String COMPTE_KEY = "COMPTE";

	private ZEOTableModel tableModel;
	private String compteReadOnly;

	public boolean isRowEditable(int row) {
		// non bloquant
		if (tableModel == null || compteReadOnly == null) {
			return true;
		}

		NSDictionary ligneDeSaisie = (NSDictionary) tableModel.getMyDg().displayedObjects().objectAtIndex(row);
		String compte = (String) ligneDeSaisie.objectForKey(COMPTE_KEY);
		return !compteReadOnly.equalsIgnoreCase(compte);
	}

	public ZEOTableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(ZEOTableModel tableModel) {
		this.tableModel = tableModel;
	}

	public String getCompteReadOnly() {
		return compteReadOnly;
	}

	public void setCompteReadOnly(String compteReadOnly) {
		this.compteReadOnly = compteReadOnly;
	}

}
