package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.cofisup.CofisupExtractionSelectCtrl;
import org.cocktail.bibasse.client.editions.EditionsCtrl;
import org.cocktail.bibasse.client.factory.FactoryBibasse;
import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisie;
import org.cocktail.bibasse.client.finder.FinderTypeSaisie;
import org.cocktail.bibasse.client.menus.MainMenu;
import org.cocktail.bibasse.client.metier.EOBudgetParametres;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.saisie.OrganPopupCtrl;
import org.cocktail.bibasse.client.saisie.SaisieBudget;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetGestionRecettes;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureDepenses;
import org.cocktail.bibasse.client.saisie.SaisieBudgetNatureRecettes;
import org.cocktail.bibasse.client.saisie.SaisieNatureLolfCtrl;
import org.cocktail.bibasse.client.selectors.SelectExercice;
import org.cocktail.bibasse.client.utils.DateCtrl;
import org.cocktail.bibasse.client.ventilations.Ventilations;
import org.cocktail.bibasse.client.virements.Virements;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.EOSimpleWindowController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class Superviseur extends EOInterfaceController {

	public static final String PARAM_CTRL_SAISIE_RECETTE = "CTRL_SAISIE_RECETTE";

	private static final String BTN_LABEL_COFISUP = "Extractions Cofisup Budget";
	private static final String BTN_LABEL_ETABLISSEMENT_RCE = "Etablissement RCE";
	private static final String BTN_LABEL_ETABLISSEMENT_NON_RCE = "Etablissement NON RCE";


	protected ApplicationClient NSApp;
	protected EOEditingContext ec;

	protected JFrame window;

	public JComboBox popupExercices, popupDbms;
	private JTextField tfExercice;

	// protected MainMenu leMenu;

	protected JPanel viewProvisoire, viewInitial, viewDbm, viewReliquats,
			viewConvention, viewVentilations, viewVirements, viewMio,
			viewImpressions;
	protected JPanel viewCofisup;
	protected JPanel swapViewSousMenus;

	protected JTextField titreMenuProvisoire, titreMenuInitial,
			titreMenuReliquats;
	protected JTextField titreMenuVentilations, titreMenuDbm,
			titreMenuConvention, titreMenuVirements, titreMenuImpressions,
			titreMenuMio;
	protected JTextField titreMenuCofisup;

	private EOBudgetSaisie currentBudgetSaisie;
	private EOTypeSaisie currentTypeSaisie;

	// Budget Provisoire
	private ActionBudgetProvisoire actionBudgetProvisoire = new ActionBudgetProvisoire();

	private ActionOuvertureProvisoire actionOuvertureProvisoire = new ActionOuvertureProvisoire();
	private ActionSaisieProvisoire actionSaisieProvisoire = new ActionSaisieProvisoire();
	private ActionPilotageProvisoire actionPilotageProvisoire = new ActionPilotageProvisoire();
	private ActionNatureLolfProvisoire actionNatureLolfProvisoire = new ActionNatureLolfProvisoire();
	private ActionVoteProvisoire actionVoteProvisoire = new ActionVoteProvisoire();
	private ActionDeleteBudget actionDeleteBudgetProvisoire = new ActionDeleteBudget();

	// Budget Initial
	private ActionBudgetInitial actionBudgetInitial = new ActionBudgetInitial();

	private ActionOuvertureInitial actionOuvertureInitial = new ActionOuvertureInitial();
	private ActionSaisieInitial actionSaisieInitial = new ActionSaisieInitial();
	private ActionPilotageInitial actionPilotageInitial = new ActionPilotageInitial();
	private ActionNatureLolfInitial actionNatureLolfInitial = new ActionNatureLolfInitial();
	private ActionVoteInitial actionVoteInitial = new ActionVoteInitial();
	private ActionDeleteBudget actionDeleteBudgetInitial = new ActionDeleteBudget();

	// Reliquats
	private ActionBudgetReliquats actionBudgetReliquats = new ActionBudgetReliquats();

	private ActionOuvertureReliquats actionOuvertureReliquats = new ActionOuvertureReliquats();
	private ActionSaisieReliquats actionSaisieReliquats = new ActionSaisieReliquats();
	private ActionPilotageReliquats actionPilotageReliquats = new ActionPilotageReliquats();
	private ActionNatureLolfReliquats actionNatureLolfReliquats = new ActionNatureLolfReliquats();
	private ActionVoteReliquats actionVoteReliquats = new ActionVoteReliquats();
	private ActionDeleteBudget actionDeleteBudgetReliquats = new ActionDeleteBudget();

	// DBM
	private ActionBudgetDbm actionBudgetDbm = new ActionBudgetDbm();

	private ActionOuvertureDbm actionOuvertureDbm = new ActionOuvertureDbm();
	private ActionSaisieDbm actionSaisieDbm = new ActionSaisieDbm();
	private ActionPilotageDbm actionPilotageDbm = new ActionPilotageDbm();
	private ActionCtrlSaisieDbm actionCtrlSaisieDbm = new ActionCtrlSaisieDbm();
	private ActionNatureLolfDbm actionNatureLolfDbm = new ActionNatureLolfDbm();
	private ActionVoteDbm actionVoteDbm = new ActionVoteDbm();
	private ActionDeleteBudget actionDeleteBudgetDbm = new ActionDeleteBudget();

	private ActionCancelDbm actionCancelDbm = new ActionCancelDbm();

	private ListenerDbms listenerDbms = new ListenerDbms();

	// Conventions
	private ActionBudgetConvention actionBudgetConvention = new ActionBudgetConvention();

	private ActionOuvertureConvention actionOuvertureConvention = new ActionOuvertureConvention();
	private ActionSaisieConvention actionSaisieConvention = new ActionSaisieConvention();
	private ActionPilotageConvention actionPilotageConvention = new ActionPilotageConvention();
	private ActionNatureLolfConvention actionNatureLolfConvention = new ActionNatureLolfConvention();
	private ActionVoteConvention actionVoteConvention = new ActionVoteConvention();

	// Ventilations
	private ActionVentilations actionVentilations = new ActionVentilations();

	// Virements
	private ActionVirements actionVirements = new ActionVirements();

	// MIO
	private ActionMio actionMio = new ActionMio();

	// Impressions
	private ActionImpressions actionImpressions = new ActionImpressions();

	// Cofisup
	private Action actionCofisup = new ActionCofisup();
	private Action actionCofisupRCE = new ActionCofisupRCE();
	private Action actionCofisupNonRCE = new ActionCofisupNonRCE();

	private ActionSelectExercice actionSelectExercice = new ActionSelectExercice();
	private ActionQuit actionQuit = new ActionQuit();

	/**
	 *
	 */
	public Superviseur() {
		super();
		NSApp = (ApplicationClient) EOApplication.sharedApplication();
		ec = NSApp.editingContext();
	}

	/**
	 *
	 */
	public Superviseur(EOEditingContext substitutionEditingContext) {
		super(substitutionEditingContext);
	}

	/**
	 *
	 */
	protected void connectionWasEstablished() {
	}

	/**
	 *
	 */
	public void componentDidBecomeVisible() {
	}

	/**
	 *
	 * @return
	 */
	public JFrame mainFrame() {
		return window;
	}

	private void gui_initTextFields() {
		tfExercice = new JTextField("EXERCICE "
				+ NSApp.getExerciceBudgetaire().exeExercice());
		tfExercice.setEditable(false);
		tfExercice.setPreferredSize(new Dimension(250, 21));
		tfExercice.setHorizontalAlignment(0);
		tfExercice.setBackground(Color.BLACK);
		tfExercice.setForeground(Color.YELLOW);

		titreMenuProvisoire = buildTitreMenu("BUDGET PROVISOIRE");
		titreMenuInitial = buildTitreMenu("BUDGET INITIAL");
		titreMenuReliquats = buildTitreMenu("BUDGET RELIQUATS");
		titreMenuDbm = buildTitreMenu("BUDGET DBM");
		titreMenuConvention = buildTitreMenu("BUDGET CONVENTION");
		titreMenuVentilations = buildTitreMenu("VENTILATIONS");
		titreMenuVirements = buildTitreMenu("VIREMENTS");
		titreMenuMio = buildTitreMenu("M I O");
		titreMenuImpressions = buildTitreMenu("IMPRESSIONS");
		titreMenuCofisup = buildTitreMenu("COFISUP");
	}

	private JTextField buildTitreMenu(String title) {
		JTextField titreMenu = new JTextField(title);

		titreMenu.setFont(Configuration.instance().informationTitreMenuFont(ec));
		titreMenu.setEditable(false);
		titreMenu.setBorder(BorderFactory.createEmptyBorder());
		titreMenu.setBackground(new Color(100, 100, 100));
		titreMenu.setForeground(new Color(117, 255, 232));
		titreMenu.setHorizontalAlignment(JTextField.CENTER);

		return titreMenu;
	}

	/**
	 *
	 * @param titreWindow
	 */
	public void init(String titreWindow) {
		// L'utilisateur est ok et possede les droits pour lancer cette application.
		NSApp.waitingFrame.setMessages("Chargement de l'application ...", "");

		window = new JFrame(titreWindow);
		window.setSize(630, 475);
		window.setIconImage(ConstantesCocktail.ICON_APP_LOGO.getImage());

		gui_initView();

		MainMenu leMenu = new MainMenu(ec, NSApp);
		window.getRootPane().setJMenuBar(leMenu);
		leMenu.updateMenus();

		window.show();
	}

	private void gui_initView() {

		gui_initTextFields();
		gui_initSubMenus();

		swapViewSousMenus = new JPanel(new CardLayout());
		swapViewSousMenus.setBorder(BorderFactory.createEmptyBorder(10, 30, 10, 30));

		Box boxSaisie1 = Box.createHorizontalBox();
		boxSaisie1.add(Box.createHorizontalGlue());
		Box boxSaisie2 = Box.createHorizontalBox();
		boxSaisie2.add(Box.createHorizontalGlue());
		Box boxSaisie3 = Box.createHorizontalBox();
		boxSaisie3.add(Box.createHorizontalGlue());
		Box boxSaisie4 = Box.createHorizontalBox();
		boxSaisie4.add(Box.createHorizontalGlue());
		Box boxSaisie5 = Box.createHorizontalBox();
		boxSaisie5.add(Box.createHorizontalGlue());
		Box boxSaisie6 = Box.createHorizontalBox();
		boxSaisie6.add(Box.createHorizontalGlue());
		Box boxSaisie7 = Box.createHorizontalBox();
		boxSaisie7.add(Box.createHorizontalGlue());
		Box boxSaisie8 = Box.createHorizontalBox();
		boxSaisie8.add(Box.createHorizontalGlue());
		Box boxSaisie9 = Box.createHorizontalBox();
		boxSaisie9.add(Box.createHorizontalGlue());
		Box boxSaisie10 = Box.createHorizontalBox();
		boxSaisie10.add(Box.createHorizontalGlue());

		Box boxCofisup = Box.createHorizontalBox();
		boxCofisup.add(Box.createHorizontalGlue());

		boxSaisie1.add(viewProvisoire);
		boxSaisie2.add(viewInitial);
		boxSaisie3.add(viewReliquats);
		boxSaisie4.add(viewDbm);
		boxSaisie5.add(viewConvention);
		boxSaisie6.add(viewVentilations);
		boxSaisie7.add(viewVirements);
		boxSaisie8.add(viewMio);
		boxSaisie9.add(viewImpressions);
		boxCofisup.add(viewCofisup);

		swapViewSousMenus.add("vide", Box.createHorizontalBox());
		swapViewSousMenus.add("provisoire", boxSaisie1);
		swapViewSousMenus.add("initial", boxSaisie2);
		swapViewSousMenus.add("reliquat", boxSaisie3);
		swapViewSousMenus.add("dbm", boxSaisie4);
		swapViewSousMenus.add("convention", boxSaisie5);
		swapViewSousMenus.add("ventilations", boxSaisie6);
		swapViewSousMenus.add("virements", boxSaisie7);
		swapViewSousMenus.add("mio", boxSaisie8);
		swapViewSousMenus.add("impressions", boxSaisie9);
		swapViewSousMenus.add("cofisup", boxCofisup);

		// Vue principale
		JPanel view = new JPanel(new BorderLayout());
		view.setBorder(BorderFactory.createEmptyBorder());

		view.add(gui_buildNorthPanel(), BorderLayout.NORTH);
		view.add(swapViewSousMenus, BorderLayout.CENTER);
		view.add(gui_buildLeftPanel(), BorderLayout.WEST);
		view.add(gui_buildSouthPanel(), BorderLayout.SOUTH);

		window.setContentPane(view);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		window.setLocation(GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getCenterPoint().x - window.getWidth() / 2,
				GraphicsEnvironment.getLocalGraphicsEnvironment()
						.getCenterPoint().y - window.getHeight() / 2);

		SaisieBudget.sharedInstance(ec).setMainFrame(window);

		NSApp.waitingFrame.close();
		NSApp.setQuitsOnLastWindowClose(true);

		// On active le premier menu par defaut
		actionBudgetInitial.actionPerformed(null);

		// Mise a jour des boutons (Actifs / Inactifs)
		updateUI();
		updateUISousMenus();
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildLeftPanel() {

		JPanel leftPanel = new JPanel(new BorderLayout());

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionBudgetProvisoire);
		arrayList.add(actionBudgetInitial);
		arrayList.add(actionBudgetReliquats);
		arrayList.add(actionBudgetDbm);
		arrayList.add(actionVentilations);
		arrayList.add(actionVirements);
		arrayList.add(actionImpressions);
		arrayList.add(actionCofisup);

		JPanel panelBoutons = ZUiUtil.buildGridColumn(
				ZUiUtil.getButtonListFromActionList(arrayList), 10);

		leftPanel.setBorder(BorderFactory.createEmptyBorder(15, 40, 15, 40));
		leftPanel.add(panelBoutons, BorderLayout.NORTH);
		leftPanel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		leftPanel.setPreferredSize(new Dimension(310, 80));

		return leftPanel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildNorthPanel() {

		JPanel northPanel = new JPanel(new BorderLayout());

		JButton buttonGetExercice = new JButton(actionSelectExercice);
		buttonGetExercice.setPreferredSize(new Dimension(23, 23));

		JPanel panelExercice = new JPanel(new FlowLayout());
		panelExercice.add(tfExercice);
		panelExercice.add(buttonGetExercice);

		northPanel.add(panelExercice, BorderLayout.CENTER);

		return northPanel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildSouthPanel() {

		JPanel southPanel = new JPanel(new BorderLayout());

		southPanel.add(new JSeparator(), BorderLayout.NORTH);

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionQuit);

		southPanel.add(ZUiUtil.buildGridLine(ZUiUtil
				.getButtonListFromActionList(arrayList, 110, 23)),
				BorderLayout.EAST);

		JLabel version = new JLabel(NSApp.version()
				+ " - " + NSApp.getConnectionName() + " (JRE "
				+ System.getProperty("java.version") + ")");
		version.setForeground(new Color(100, 100, 100));
		version.setBorder(BorderFactory.createEmptyBorder());
		version.setFont(Configuration.instance().informationLabelSmallFont(ec));
		version.setHorizontalAlignment(JTextField.LEFT);
		version.setFocusable(false);

		southPanel.add(version, BorderLayout.WEST);

		return southPanel;
	}

	/**
	 *
	 *
	 */
	public void updateIcons(Action action) {

		actionBudgetProvisoire.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionBudgetInitial.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionBudgetReliquats.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionBudgetDbm.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionBudgetConvention.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionVentilations.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionVirements.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionMio.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionImpressions.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);
		actionCofisup.putValue(AbstractAction.SMALL_ICON,
				ConstantesCocktail.ICON_MENU_INACTIF);

		if (action != null) {
			action.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_MENU_ACTIF);
		}
	}

	/**
	 *
	 *
	 */
	private void gui_initSubMenus() {

		gui_initSubMenusProvisoire();
		gui_initSubMenusInitial();
		gui_initSubMenusReliquats();
		gui_initSubMenusDbms();
		gui_initSubMenusConventions();
		gui_initSubMenusMio();
		gui_initSubMenusVentilations();
		gui_initSubMenusVirements();
		gui_initSubMenusImpressions();
		gui_initSubMenusCofisup();
	}

	private void gui_initSubMenusProvisoire() {
		viewProvisoire = new JPanel(new BorderLayout());

		List<Action> arrayListProvisoire = new ArrayList<Action>();

		arrayListProvisoire.add(actionOuvertureProvisoire);
		arrayListProvisoire.add(actionSaisieProvisoire);
		arrayListProvisoire.add(actionPilotageProvisoire);
		arrayListProvisoire.add(actionNatureLolfProvisoire);
		arrayListProvisoire.add(actionVoteProvisoire);
		arrayListProvisoire.add(actionDeleteBudgetProvisoire);

		JPanel panelBoutonsProvisoire = ZUiUtil.buildGridColumn(ZUiUtil
				.getButtonListFromActionList(arrayListProvisoire, 150, 23));
		panelBoutonsProvisoire.setBorder(BorderFactory.createEmptyBorder(15,
				30, 15, 30));

		JPanel viewBoutonsProvisoire = new JPanel(new BorderLayout());
		viewBoutonsProvisoire.add(panelBoutonsProvisoire);
		viewBoutonsProvisoire.setBorder(BorderFactory.createLineBorder(
				Color.BLACK, 1));

		JPanel tempView = new JPanel(new BorderLayout());
		tempView.add(titreMenuProvisoire, BorderLayout.NORTH);
		tempView.add(viewBoutonsProvisoire, BorderLayout.CENTER);

		viewProvisoire.add(tempView, BorderLayout.NORTH);
	}

	private void gui_initSubMenusInitial() {
		viewInitial = new JPanel(new BorderLayout());

		List<Action> arrayListInitial = new ArrayList<Action>();

		arrayListInitial.add(actionOuvertureInitial);
		arrayListInitial.add(actionSaisieInitial);
		arrayListInitial.add(actionPilotageInitial);
		arrayListInitial.add(actionNatureLolfInitial);
		arrayListInitial.add(actionVoteInitial);
		arrayListInitial.add(actionDeleteBudgetInitial);

		JPanel panelBoutonsInitial = ZUiUtil.buildGridColumn(ZUiUtil
				.getButtonListFromActionList(arrayListInitial, 150, 23));
		panelBoutonsInitial.setBorder(BorderFactory.createEmptyBorder(15, 30,
				15, 30));

		JPanel viewBoutonsInitial = new JPanel(new BorderLayout());
		viewBoutonsInitial.add(panelBoutonsInitial);
		viewBoutonsInitial.setBorder(BorderFactory.createLineBorder(
				Color.BLACK, 1));

		JPanel tempViewInitial = new JPanel(new BorderLayout());
		tempViewInitial.add(titreMenuInitial, BorderLayout.NORTH);
		tempViewInitial.add(viewBoutonsInitial, BorderLayout.CENTER);

		viewInitial.add(tempViewInitial, BorderLayout.NORTH);
	}

	private void gui_initSubMenusReliquats() {
		viewReliquats = new JPanel(new BorderLayout());

		List<Action> arrayListReliquats = new ArrayList<Action>();

		arrayListReliquats.add(actionOuvertureReliquats);
		arrayListReliquats.add(actionSaisieReliquats);
		arrayListReliquats.add(actionPilotageReliquats);
		arrayListReliquats.add(actionNatureLolfReliquats);
		arrayListReliquats.add(actionVoteReliquats);
		arrayListReliquats.add(actionDeleteBudgetReliquats);

		JPanel panelBoutonsReliquats = ZUiUtil.buildGridColumn(ZUiUtil
				.getButtonListFromActionList(arrayListReliquats));
		panelBoutonsReliquats.setBorder(BorderFactory.createEmptyBorder(15, 30,
				15, 30));

		JPanel viewBoutonsReliquats = new JPanel(new BorderLayout());
		viewBoutonsReliquats.add(panelBoutonsReliquats);
		viewBoutonsReliquats.setBorder(BorderFactory.createLineBorder(
				Color.BLACK, 1));

		JPanel tempViewReliquats = new JPanel(new BorderLayout());
		tempViewReliquats.add(titreMenuReliquats, BorderLayout.NORTH);
		tempViewReliquats.add(viewBoutonsReliquats, BorderLayout.CENTER);

		viewReliquats.add(tempViewReliquats, BorderLayout.NORTH);
	}

	private void gui_initSubMenusDbms() {
		viewDbm = new JPanel(new BorderLayout());

		popupDbms = new JComboBox();
		popupDbms.setFocusable(false);
		popupDbms.setBackground(new Color(100, 100, 100));
		popupDbms.setForeground(new Color(0, 0, 0));
		popupDbms.addActionListener(listenerDbms);

		List<Action> arrayListDbm = new ArrayList<Action>();

		arrayListDbm.add(actionOuvertureDbm);
		arrayListDbm.add(actionSaisieDbm);
		arrayListDbm.add(actionPilotageDbm);
		arrayListDbm.add(actionCtrlSaisieDbm);
		arrayListDbm.add(actionNatureLolfDbm);
		arrayListDbm.add(actionVoteDbm);
		arrayListDbm.add(actionCancelDbm);

		JPanel panelBoutonsDbm = ZUiUtil.buildGridColumn(ZUiUtil
				.getButtonListFromActionList(arrayListDbm));
		panelBoutonsDbm
				.setBorder(BorderFactory.createEmptyBorder(2, 30, 2, 30));

		JPanel viewBoutonsDbm = new JPanel(new BorderLayout());
		viewBoutonsDbm.add(panelBoutonsDbm);
		viewBoutonsDbm
				.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));

		JPanel tempViewDbm = new JPanel(new BorderLayout());
		tempViewDbm.add(popupDbms, BorderLayout.NORTH);
		tempViewDbm.add(viewBoutonsDbm, BorderLayout.CENTER);

		viewDbm.add(tempViewDbm, BorderLayout.NORTH);
	}

	private void gui_initSubMenusConventions() {
		viewConvention = new JPanel(new BorderLayout());

		List<Action> arrayListConvention = new ArrayList<Action>();

		arrayListConvention.add(actionOuvertureConvention);
		arrayListConvention.add(actionSaisieConvention);
		arrayListConvention.add(actionPilotageConvention);
		arrayListConvention.add(actionNatureLolfConvention);
		arrayListConvention.add(actionVoteConvention);

		JPanel panelBoutonsConvention = ZUiUtil.buildGridColumn(ZUiUtil
				.getButtonListFromActionList(arrayListConvention));
		panelBoutonsConvention.setBorder(BorderFactory.createEmptyBorder(15,
				30, 15, 30));

		JPanel viewBoutonsConvention = new JPanel(new BorderLayout());
		viewBoutonsConvention.add(panelBoutonsConvention);
		viewBoutonsConvention.setBorder(BorderFactory.createLineBorder(
				Color.BLACK, 1));

		JPanel tempViewConvention = new JPanel(new BorderLayout());
		tempViewConvention.add(titreMenuConvention, BorderLayout.NORTH);
		tempViewConvention.add(viewBoutonsConvention, BorderLayout.CENTER);

		viewConvention.add(tempViewConvention, BorderLayout.NORTH);
	}

	private void gui_initSubMenusMio() {
		viewMio = new JPanel(new BorderLayout());

		JPanel tempViewMio = new JPanel(new BorderLayout());
		tempViewMio.add(titreMenuMio, BorderLayout.NORTH);

		viewMio.add(tempViewMio, BorderLayout.NORTH);
	}

	private void gui_initSubMenusVentilations() {
		viewVentilations = new JPanel(new BorderLayout());

		JPanel tempViewVentilation = new JPanel(new BorderLayout());
		tempViewVentilation.add(titreMenuVentilations, BorderLayout.NORTH);

		viewVentilations.add(tempViewVentilation, BorderLayout.NORTH);
	}

	private void gui_initSubMenusVirements() {
		viewVirements = new JPanel(new BorderLayout());

		JPanel tempViewVirements = new JPanel(new BorderLayout());
		tempViewVirements.add(titreMenuVirements, BorderLayout.NORTH);

		viewVirements.add(tempViewVirements, BorderLayout.NORTH);
	}

	private void gui_initSubMenusImpressions() {
		viewImpressions = new JPanel(new BorderLayout());

		JPanel tempViewImpressions = new JPanel(new BorderLayout());
		tempViewImpressions.add(titreMenuImpressions, BorderLayout.NORTH);

		viewImpressions.add(tempViewImpressions, BorderLayout.NORTH);
	}

	private void gui_initSubMenusCofisup() {
		viewCofisup = new JPanel(new BorderLayout());

		List<Action> actionsCofisup = new ArrayList<Action>();
		actionsCofisup.add(actionCofisupRCE);
		actionsCofisup.add(actionCofisupNonRCE);

		JPanel panelBoutonsCofisup = ZUiUtil.buildGridColumn(ZUiUtil
				.getButtonListFromActionList(actionsCofisup, 150, 23));
		panelBoutonsCofisup.setBorder(BorderFactory.createEmptyBorder(15, 30,
				15, 30));

		JPanel viewBoutonsCofisup = new JPanel(new BorderLayout());
		viewBoutonsCofisup.add(panelBoutonsCofisup);
		viewBoutonsCofisup.setBorder(BorderFactory.createLineBorder(
				Color.BLACK, 1));

		JPanel viewCofisupFinale = new JPanel(new BorderLayout());
		viewCofisupFinale.add(viewBoutonsCofisup, BorderLayout.CENTER);

		viewCofisup.add(viewCofisupFinale, BorderLayout.PAGE_START);
	}

	/**
	 *
	 *
	 */
	public void initDbms() {

		popupDbms.removeActionListener(listenerDbms);
		NSArray dbms = FinderBudgetSaisie.findBudgetsSaisieForExercice(ec,
				NSApp.getExerciceBudgetaire(), currentTypeSaisie);
		popupDbms.removeAllItems();

		for (int i = 0; i < dbms.count(); i++)
			popupDbms.addItem((EOBudgetSaisie) dbms.objectAtIndex(i));

		if (dbms.count() > 0) {
			popupDbms.setSelectedIndex(0);
			listenerDbms.actionPerformed(null);
			popupDbms.addActionListener(listenerDbms);
		}
	}

	/**
	 *
	 */
	public void setWaitCursor(boolean wait) {
		if (wait)
			component().setCursor(
					Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		else
			component().setCursor(
					Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void setWaitCursor() {
		NSApp.setWaitCursor(mainFrame());
	}

	/**
	 * Quitte l'application
	 */
	public void quitterApplication() {
		NSApp.quit();
	}

	/**
	 *
	 *
	 */
	public void consoliderBudget() {

		if (EODialogs.runConfirmOperationDialog("Attention",
				"Voulez vous lancer la consolidation du budget : "
						+ currentBudgetSaisie.bdsaLibelle() + " ?", "OUI",
				"NON")) {

			NSApp.setWaitCursor(window);
			try {
				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(
						(EOEnterpriseObject) currentBudgetSaisie,
						"EOBudgetSaisie");
				ServerProxy.clientSideRequestConsoliderBudget(ec, parametres);
			} catch (Exception ex) {
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", ex.getMessage());
			}

			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 *
	 */
	public void consoliderBudgetsVotes() {

		if (EODialogs
				.runConfirmOperationDialog(
						"Attention",
						"Voulez vous lancer la consolidation de TOUS les budgets VOTES ?",
						"OUI", "NON")) {
			NSApp.setWaitCursor(window);
			try {
				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey(NSApp.getExerciceBudgetaire()
						.exeExercice(), "exercice");
				ServerProxy.clientSideRequestConsoliderTousBudgets(ec,
						parametres);
			} catch (Exception ex) {
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", ex.getMessage());
			}

			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 */
	public static NSDictionary serverPrimaryKeyForObject(EOEditingContext ed,
			EOEnterpriseObject eo) {
		return (NSDictionary) ((EODistributedObjectStore) ed
				.parentObjectStore()).invokeRemoteMethodWithKeyPath(ed,
				"session", "clientSideRequestPrimaryKeyForObject",
				new Class[] { EOEnterpriseObject.class }, new Object[] { eo },
				false);
	}

	/**
	 *
	 */
	public int abcisseMainWindow() {
		Window mainWindow = ((EOSimpleWindowController) supercontroller())
				.window();
		return mainWindow.getBounds().x;
	}

	/**
	 *
	 */
	public int ordonneeMainWindow() {
		Window mainWindow = ((EOSimpleWindowController) supercontroller())
				.window();
		return mainWindow.getBounds().y;
	}

	/**
	 *
	 */
	public void windowClosing(WindowEvent e) {
		boolean dialog = EODialogs.runConfirmOperationDialog(
				"Fermeture de l'application ...",
				"Etes-vous sur de vouloir quitter l'application ?", "OUI",
				"NON");

		if (dialog) {
			if (ec.updatedObjects().count() > 0) {
				try {
					ec.lock();
					ec.saveChanges();
				} catch (Exception ex) {
				} finally {
					ec.unlock();
				}
			}
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionBudgetProvisoire extends AbstractAction {

		public ActionBudgetProvisoire() {
			super("Budget PROVISOIRE");
			setToolTip("Gestion du budget PROVISOIRE");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			updateIcons(actionBudgetProvisoire);

			currentTypeSaisie = FinderTypeSaisie.findTypeSaisie(ec,
					EOTypeSaisie.SAISIE_PROVISOIRE);
			currentBudgetSaisie = FinderBudgetSaisie
					.findBudgetSaisieForExercice(ec,
							NSApp.getExerciceBudgetaire(), currentTypeSaisie);

			if (currentBudgetSaisie != null)
				titreMenuProvisoire.setText("BUDGET PROVISOIRE ( "
						+ currentBudgetSaisie.typeEtat().tyetLibelle() + " )");
			else
				titreMenuProvisoire.setText("BUDGET PROVISOIRE");

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "provisoire");

			updateUISousMenus();

			SaisieBudget.sharedInstance(ec).close();
			SaisieBudgetGestionDepenses.sharedInstance(ec).initGUI();
			SaisieBudgetGestionRecettes.sharedInstance(ec).initGUI();

			PilotageCtrl.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			OrganPopupCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec).setTypeSaisie(currentTypeSaisie);
			SaisieNatureLolfCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);

			BudgetSyntheseCtrl.sharedInstance(ec).close();
			SaisieBudget.sharedInstance(ec).reload();
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionBudgetInitial extends AbstractAction {

		public ActionBudgetInitial() {
			super("Budget INITIAL");
			setToolTip("Saisie du budget INITIAL");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			updateIcons(actionBudgetInitial);

			currentTypeSaisie = FinderTypeSaisie.findTypeSaisie(ec,
					EOTypeSaisie.SAISIE_INITIAL);
			currentBudgetSaisie = FinderBudgetSaisie
					.findBudgetSaisieForExercice(ec,
							NSApp.getExerciceBudgetaire(), currentTypeSaisie);

			if (currentBudgetSaisie != null) {
				titreMenuInitial.setText("BUDGET INITIAL ( "
						+ currentBudgetSaisie.typeEtat().tyetLibelle() + " )");
			} else {
				titreMenuInitial.setText("BUDGET INITIAL");
			}

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "initial");
			updateUISousMenus();

			SaisieBudget.sharedInstance(ec).close();
			SaisieBudgetGestionDepenses.sharedInstance(ec).initGUI();
			SaisieBudgetGestionRecettes.sharedInstance(ec).initGUI();

			PilotageCtrl.sharedInstance(ec).cleanBudgetsNature();
			PilotageCtrl.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			OrganPopupCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec).setTypeSaisie(currentTypeSaisie);
			SaisieNatureLolfCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);

			BudgetSyntheseCtrl.sharedInstance(ec).close();
			SaisieBudget.sharedInstance(ec).reload();

		}
	}

	private final class ActionBudgetReliquats extends AbstractAction {

		public ActionBudgetReliquats() {
			super("Budget RELIQUATS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			updateIcons(actionBudgetReliquats);

			currentTypeSaisie = FinderTypeSaisie.findTypeSaisie(ec,
					EOTypeSaisie.SAISIE_RELIQUAT);
			currentBudgetSaisie = FinderBudgetSaisie
					.findBudgetSaisieForExercice(ec,
							NSApp.getExerciceBudgetaire(), currentTypeSaisie);

			if (currentBudgetSaisie != null)
				titreMenuInitial.setText("BUDGET RELIQUATS ( "
						+ currentBudgetSaisie.typeEtat().tyetLibelle() + " )");
			else
				titreMenuInitial.setText("BUDGET RELIQUATS");

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "reliquat");
			updateUISousMenus();

			SaisieBudget.sharedInstance(ec).close();
			SaisieBudgetGestionDepenses.sharedInstance(ec).initGUI();
			SaisieBudgetGestionRecettes.sharedInstance(ec).initGUI();

			PilotageCtrl.sharedInstance(ec).cleanBudgetsNature();
			PilotageCtrl.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			OrganPopupCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec).setTypeSaisie(currentTypeSaisie);
			SaisieNatureLolfCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);

			SaisieBudget.sharedInstance(ec).clean();

		}
	}

	private final class ActionBudgetDbm extends AbstractAction {

		public ActionBudgetDbm() {
			super("Saisie DBM");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			updateIcons(actionBudgetDbm);

			currentTypeSaisie = FinderTypeSaisie.findTypeSaisie(ec,
					EOTypeSaisie.SAISIE_DBM);
			currentBudgetSaisie = null;

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "dbm");

			initDbms();
			updateUISousMenus();

			SaisieBudget.sharedInstance(ec).setTypeSaisie(currentTypeSaisie);
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionBudgetConvention extends AbstractAction {

		public ActionBudgetConvention() {
			super("Budget CONVENTIONS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			updateIcons(actionBudgetConvention);

			currentTypeSaisie = FinderTypeSaisie.findTypeSaisie(ec,
					EOTypeSaisie.SAISIE_CONVENTION);
			currentBudgetSaisie = FinderBudgetSaisie
					.findBudgetSaisieForExercice(ec,
							NSApp.getExerciceBudgetaire(), currentTypeSaisie);

			if (currentBudgetSaisie != null)
				titreMenuConvention.setText("BUDGET CONVENTION ( "
						+ currentBudgetSaisie.typeEtat().tyetLibelle() + " )");
			else
				titreMenuConvention.setText("BUDGET CONVENTION");

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "convention");
			updateUISousMenus();

			PilotageCtrl.sharedInstance(ec).cleanBudgetsNature();
			PilotageCtrl.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			OrganPopupCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec).setTypeSaisie(currentTypeSaisie);
			SaisieNatureLolfCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);

			SaisieBudget.sharedInstance(ec).clean();
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionSelectExercice extends AbstractAction {
		public ActionSelectExercice() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SELECT);
		}

		public void actionPerformed(ActionEvent e) {
			EOExercice exercice = SelectExercice.sharedInstance(ec, window)
					.getExercice();

			if (exercice != null) {

				tfExercice.setText("EXERCICE " + exercice.exeExercice());
				NSApp.setExerciceBudgetaire(exercice);

				window.setTitle(ConstantesCocktail.APPLICATION_NAME
						+ " - Gestion du budget " + exercice.exeExercice());

				SaisieBudget.sharedInstance(ec).clean();
				SaisieBudget.sharedInstance(ec).close();

				PilotageCtrl.sharedInstance(ec).close();
				PilotageCtrl.sharedInstance(ec).fillPopupEtabs();

				// on rafraichit les parametres pour cet exercice.
				EOBudgetParametres paramNiveauSaisie = FinderBudgetParametres
						.findParametre(ec, exercice, "NIVEAU_SAISIE");
				NSApp.setNiveauSaisie(paramNiveauSaisie.bparValue());

				EOBudgetParametres paramNatureLolf = FinderBudgetParametres
						.findParametre(ec, exercice, "REPART_NATURE_LOLF");
				if (paramNatureLolf == null
						|| paramNatureLolf.bparValue() == null
						|| !paramNatureLolf.bparValue().equals("OUI")) {
					NSApp.setRepartitionNatureLolf(false);
					System.out.println("SAISIE NATURE LOLF : NON");
				} else {
					NSApp.setRepartitionNatureLolf(true);
					System.out.println("SAISIE NATURE LOLF : OUI");
				}

				NSApp.refreshParametres(exercice);

				// On raffraichit tous les modules dependant des types de
				// credit.

				OrganPopupCtrl.sharedInstance(ec).updateNiveauSaisie();

				NSApp.setUserOrgans(exercice);

				SaisieBudgetGestionDepenses.sharedInstance(ec).initData();
				SaisieBudgetGestionDepenses.sharedInstance(ec).initGUI();

				SaisieBudgetGestionRecettes.sharedInstance(ec).initData();
				SaisieBudgetGestionRecettes.sharedInstance(ec).initGUI();

				SaisieBudgetNatureDepenses.sharedInstance(ec).initData();

				SaisieBudgetNatureRecettes.sharedInstance(ec).initData();

				Ventilations.sharedInstance(ec).exerciceHasChanged();
				EditionsCtrl.sharedInstance(ec).exerciceHasChanged();
				updateUI();

				updateIcons(null);

				currentBudgetSaisie = null;

				((CardLayout) swapViewSousMenus.getLayout()).show(
						swapViewSousMenus, "vide");

			}
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionVirements extends AbstractAction {
		public ActionVirements() {
			super("Virements");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			NSApp.setWaitCursor(mainFrame());

			updateIcons(actionVirements);

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "virements");
			updateUISousMenus();

			Virements.sharedInstance(ec).open();

			NSApp.setDefaultCursor(mainFrame());
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionMio extends AbstractAction {

		public ActionMio() {
			super("MIO");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {

			updateIcons(actionMio);

			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "mio");
			updateUISousMenus();
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionVentilations extends AbstractAction {

		public ActionVentilations() {
			super("Ventilations");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(mainFrame());

			updateIcons(actionVentilations);
			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "ventilations");
			updateUISousMenus();

			Ventilations.sharedInstance(ec).open();

			NSApp.setDefaultCursor(mainFrame());
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionImpressions extends AbstractAction {

		public ActionImpressions() {
			super("Impressions");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {
			updateIcons(actionImpressions);
			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "impressions");
			EditionsCtrl.sharedInstance(ec).open();
		}
	}

	private final class ActionCofisup extends AbstractAction {
		public ActionCofisup() {
			super(BTN_LABEL_COFISUP);
			setToolTip(BTN_LABEL_COFISUP);
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent actionEvent) {
			updateIcons(actionCofisup);
			((CardLayout) swapViewSousMenus.getLayout()).show(
					swapViewSousMenus, "cofisup");
		}
	}

	private final class ActionCofisupRCE extends AbstractAction {
		public ActionCofisupRCE() {
			super(BTN_LABEL_ETABLISSEMENT_RCE);
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent actionEvent) {
			CofisupExtractionSelectCtrl.instance(ec).openRce();
		}
	}

	private final class ActionCofisupNonRCE extends AbstractAction {
		public ActionCofisupNonRCE() {
			super(BTN_LABEL_ETABLISSEMENT_NON_RCE);
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent actionEvent) {
			CofisupExtractionSelectCtrl.instance(ec).openNonRce();
		}
	}

	/**
	 *
	 *
	 */
	private void updateUISousMenus() {
		// Budget Provisoire
		if (EOTypeSaisie.SAISIE_PROVISOIRE.equals(currentTypeSaisie
				.tysaLibelle())) {

			actionOuvertureProvisoire.setEnabled(false);
			actionVoteProvisoire.setEnabled(false);
			actionSaisieProvisoire.setEnabled(false);
			actionPilotageProvisoire.setEnabled(false);
			actionDeleteBudgetProvisoire.setEnabled(false);

			if (currentBudgetSaisie == null) // Bubdget PROVISOIRE non
												// Initialise
				actionOuvertureProvisoire.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_OUVERTURE));
			else {
				actionSaisieProvisoire.setEnabled(true);
				actionPilotageProvisoire.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));

				if (currentBudgetSaisie.isVote() || !NSApp.fonctionSaisie()) {
					actionSaisieProvisoire.putValue(AbstractAction.NAME,
							"Consultation");
					if (currentBudgetSaisie.bdsaDateValidation() != null)
						actionVoteProvisoire.putValue(
								AbstractAction.NAME,
								"Voté le "
										+ DateCtrl.dateToString(
												currentBudgetSaisie
														.bdsaDateValidation(),
												"%d/%m/%Y"));
				} else {
					actionSaisieProvisoire.putValue(AbstractAction.NAME,
							"Saisie");
					actionVoteProvisoire.putValue(AbstractAction.NAME, "Vote");
					actionVoteProvisoire.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
					actionDeleteBudgetProvisoire.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
				}
			}
		}

		// Budget Initial
		if (EOTypeSaisie.SAISIE_INITIAL.equals(currentTypeSaisie.tysaLibelle())) {

			actionOuvertureInitial.setEnabled(false);
			actionVoteInitial.setEnabled(false);
			actionSaisieInitial.setEnabled(false);
			actionPilotageInitial.setEnabled(false);
			actionDeleteBudgetInitial.setEnabled(false);

			if (currentBudgetSaisie == null)
				actionOuvertureInitial.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_OUVERTURE));
			else {

				actionSaisieInitial.setEnabled(true);
				actionPilotageInitial.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));

				if (currentBudgetSaisie.isVote()) {
					actionSaisieInitial.putValue(AbstractAction.NAME,
							"Consultation");
					actionVoteInitial
							.putValue(
									AbstractAction.NAME,
									"Voté le "
											+ org.cocktail.bibasse.client.utils.DateCtrl.dateToString(
													currentBudgetSaisie
															.bdsaDateValidation(),
													"%d/%m/%Y"));
				} else {
					actionSaisieInitial.putValue(AbstractAction.NAME, "Saisie");
					actionVoteInitial.putValue(AbstractAction.NAME, "Vote");
					actionVoteInitial.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
					actionDeleteBudgetInitial.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
				}

			}
		}

		// Budget Reliquats
		if (EOTypeSaisie.SAISIE_RELIQUAT
				.equals(currentTypeSaisie.tysaLibelle())) {

			actionOuvertureReliquats.setEnabled(false);
			actionVoteReliquats.setEnabled(false);
			actionSaisieReliquats.setEnabled(false);
			actionPilotageReliquats.setEnabled(false);
			actionDeleteBudgetReliquats.setEnabled(false);

			if (currentBudgetSaisie == null)
				actionOuvertureReliquats.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_OUVERTURE));
			else {

				actionSaisieReliquats.setEnabled(true);
				actionPilotageReliquats.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));

				if (currentBudgetSaisie.isVote())
					actionSaisieReliquats.putValue(AbstractAction.NAME,
							"Consultation");
				else {
					actionSaisieReliquats.putValue(AbstractAction.NAME,
							"Saisie");
					// if (currentBudgetSaisie.isCloture())
					actionVoteReliquats.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
					actionDeleteBudgetReliquats.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
				}
			}
		}

		// Budget DBM
		if (EOTypeSaisie.SAISIE_DBM.equals(currentTypeSaisie.tysaLibelle())) {

			actionOuvertureDbm.setEnabled(false);
			actionVoteDbm.setEnabled(false);
			actionSaisieDbm.setEnabled(false);
			actionPilotageDbm.setEnabled(false);
			actionCtrlSaisieDbm.setEnabled(false);
			actionCancelDbm.setEnabled(false);
			actionDeleteBudgetDbm.setEnabled(false);

			if (currentBudgetSaisie == null || currentBudgetSaisie.isVote()) {

				// Nouvelle DBM à ouvrir.
				actionOuvertureDbm.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_OUVERTURE));
				actionOuvertureDbm.putValue(AbstractAction.NAME,
						"Ouverture Nouvelle DBM");

				if (currentBudgetSaisie != null && currentBudgetSaisie.isVote()) {

					if (currentBudgetSaisie.bdsaDateValidation() != null)
						actionVoteDbm.putValue(
								AbstractAction.NAME,
								"Votée le "
										+ DateCtrl.dateToString(
												currentBudgetSaisie
														.bdsaDateValidation(),
												"%d/%m/%Y"));

					actionSaisieDbm.putValue(AbstractAction.NAME,
							"Consultation");
					actionSaisieDbm.setEnabled(true);
					actionPilotageDbm.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));
				}
			} else { // DBM xxx ouverte.
				actionOuvertureDbm.putValue(AbstractAction.NAME, "Ouverture");
				actionSaisieDbm.setEnabled(true);
				actionPilotageDbm.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));

				actionCtrlSaisieDbm.setEnabled(true);

				actionSaisieDbm.putValue(AbstractAction.NAME, "Saisie");
				actionVoteDbm.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
				actionVoteDbm.putValue(AbstractAction.NAME, "Vote");

				actionCancelDbm.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
				actionDeleteBudgetDbm.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
			}
		}

		// Budget CONVENTION
		if (EOTypeSaisie.SAISIE_CONVENTION.equals(currentTypeSaisie
				.tysaLibelle())) {

			actionOuvertureConvention.setEnabled(false);
			actionVoteConvention.setEnabled(false);
			actionSaisieConvention.setEnabled(false);
			actionPilotageConvention.setEnabled(false);

			if (currentBudgetSaisie == null)
				actionOuvertureConvention.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_OUVERTURE));
			else {

				actionSaisieConvention.setEnabled(true);
				actionPilotageConvention.setEnabled(NSApp
						.hasFonction(ConstantesCocktail.ID_FCT_PILOTAGE));

				if (currentBudgetSaisie.isVote())
					actionSaisieConvention.putValue(AbstractAction.NAME,
							"Consultation");
				else {
					actionSaisieConvention.putValue(AbstractAction.NAME,
							"Saisie");
					// if (currentBudgetSaisie.isCloture())
					actionVoteConvention.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VOTE));
				}
			}
		}
	}

	/**
	 *
	 *
	 */
	public void changerTypeSaisieBudget() {
		TypeSaisieCtrl.sharedInstance(ec).open(currentBudgetSaisie);
	}

	/**
	 *
	 * @param budgetSaisie
	 */
	public void refreshMenu() {
		NSApp.superviseur().updateUI();

		EOBudgetSaisie budgetInitial = FinderBudgetSaisie
				.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(),
						FinderTypeSaisie.findTypeSaisie(ec,
								EOTypeSaisie.SAISIE_INITIAL));
		if (budgetInitial != null)
			actionBudgetInitial.actionPerformed(null);
		else
			actionBudgetProvisoire.actionPerformed(null);

		NSApp.superviseur().updateUISousMenus();
	}

	/**
	 * Mise a jour de l'interface. On active ou non les differents menus en
	 * fonction de l'etat du budget en cours
	 *
	 */
	private void updateUI() {

		actionBudgetProvisoire.setEnabled(false);
		actionBudgetInitial.setEnabled(false);
		actionBudgetReliquats.setEnabled(false);
		actionBudgetConvention.setEnabled(false);
		actionBudgetDbm.setEnabled(false);
		actionVirements.setEnabled(false);
		actionMio.setEnabled(false);
		actionVentilations.setEnabled(false);
		actionImpressions.setEnabled(true);
		actionCofisup.setEnabled(NSApp.hasFonction(ConstantesCocktail.ID_FCT_GENERATION_COFISUP));

		// On recherche le budget provisoire
		EOBudgetSaisie budgetProvisoire = FinderBudgetSaisie
				.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(),
						FinderTypeSaisie.findTypeSaisie(ec,
								EOTypeSaisie.SAISIE_PROVISOIRE));
		EOBudgetSaisie budgetInitial = FinderBudgetSaisie
				.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(),
						FinderTypeSaisie.findTypeSaisie(ec,
								EOTypeSaisie.SAISIE_INITIAL));
		EOBudgetSaisie budgetDbm = FinderBudgetSaisie
				.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(),
						FinderTypeSaisie.findTypeSaisie(ec,
								EOTypeSaisie.SAISIE_DBM));
		EOBudgetSaisie budgetReliquat = FinderBudgetSaisie
				.findBudgetSaisieForExercice(ec, NSApp.getExerciceBudgetaire(),
						FinderTypeSaisie.findTypeSaisie(ec,
								EOTypeSaisie.SAISIE_RELIQUAT));

		if (budgetProvisoire == null) {

			actionBudgetProvisoire.putValue(AbstractAction.NAME,
					"Budget PROVISOIRE");
			actionBudgetInitial.setEnabled(true);

			if (budgetInitial == null) {
				actionBudgetInitial.putValue(AbstractAction.NAME,
						"Budget INITIAL");
				actionBudgetProvisoire.setEnabled(false);
			} else {
				if (budgetInitial.isVote()) {

					actionBudgetInitial.putValue(AbstractAction.NAME,
							"Budget INITIAL (VOTE)");

					actionBudgetReliquats.setEnabled(true);
					actionBudgetDbm.setEnabled(true);
					actionVentilations
							.setEnabled(NSApp
									.hasFonction(ConstantesCocktail.ID_FCT_VENTILATION));
					actionVirements.setEnabled(NSApp
							.hasFonction(ConstantesCocktail.ID_FCT_VIREMENT));
					// actionMio.setEnabled(true);
				} else {
					actionBudgetInitial.putValue(AbstractAction.NAME, "Budget INITIAL");
				}
			}
		} else {

			actionBudgetProvisoire.setEnabled(true);

			if (budgetProvisoire.isVote()) {

				actionBudgetProvisoire.putValue(AbstractAction.NAME,
						"Budget PROVISOIRE (VOTE)");
				actionBudgetInitial.setEnabled(true);
				actionVentilations.setEnabled(true);
				actionVirements.setEnabled(true);

				if (budgetInitial != null) {

					if (budgetInitial.isVote()) {

						actionBudgetInitial.putValue(AbstractAction.NAME,
								"Budget INITIAL (VOTE)");
						actionBudgetReliquats.setEnabled(true);
						actionBudgetDbm.setEnabled(true);
						actionVentilations
								.setEnabled(NSApp
										.hasFonction(ConstantesCocktail.ID_FCT_VENTILATION));
						actionVirements
								.setEnabled(NSApp
										.hasFonction(ConstantesCocktail.ID_FCT_VIREMENT));
						// actionMio.setEnabled(true);
					} else
						actionBudgetInitial.putValue(AbstractAction.NAME,
								"Budget INITIAL");
				}
			} else {
				actionBudgetProvisoire.putValue(AbstractAction.NAME,
						"Budget PROVISOIRE");
			}
		}

		if (budgetReliquat != null && budgetReliquat.isVote())
			actionBudgetReliquats.putValue(AbstractAction.NAME,
					"Budget RELIQUATS (VOTE)");

		if (budgetReliquat != null && !budgetReliquat.isVote())
			actionBudgetDbm.setEnabled(false);

		// if (budgetDbm != null && budgetDbm.isVote() && budgetReliquat ==
		// null)
		// actionBudgetReliquats.setEnabled(false);
		if (budgetDbm != null && !budgetDbm.isVote()) {
			if (budgetReliquat != null && budgetReliquat.isVote())
				actionBudgetReliquats.setEnabled(true);
			else
				actionBudgetReliquats.setEnabled(false);
		}
	}

	// SOUS MENUS

	/**
	 *
	 * Ouverture du budget Provisoire
	 *
	 */
	public final class ActionOuvertureProvisoire extends AbstractAction {
		public ActionOuvertureProvisoire() {
			super("Ouverture");
			setToolTip("Ouverture du budget PROVISOIRE");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_OUVERTURE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous l'OUVERTURE du Budget PROVISOIRE "
							+ NSApp.getExerciceBudgetaire().exeExercice()
							+ " ?", "OUI", "NON")) {
				try {
					if (FactoryBibasse.controlerOuvertureBudget(ec,
							NSApp.getExerciceBudgetaire()))
						currentBudgetSaisie = SaisieBudget.sharedInstance(ec)
								.initBudgetSaisie();
				} catch (Exception ex) {
					EODialogs
							.runInformationDialog("ATTENTION", ex.getMessage());
				}
				updateUI();
				updateUISousMenus();
			}
		}
	}

	/**
	 *
	 * Saisie du budget Provisoire
	 *
	 */
	private final class ActionSaisieProvisoire extends AbstractAction {
		public ActionSaisieProvisoire() {
			super("Saisie");
			setToolTip("Saisie du budget PROVISOIRE");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			SaisieBudget.sharedInstance(ec).open();
		}
	}

	/**
	 *
	 * Pilotage du budget Provisoire
	 *
	 */
	private final class ActionPilotageProvisoire extends AbstractAction {
		public ActionPilotageProvisoire() {
			super("Pilotage");
			setToolTip("Pilotage du budget PROVISOIRE");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_PILOTAGE);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			PilotageCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Repartition nature/lolf du budget Provisoire
	 *
	 */
	private final class ActionNatureLolfProvisoire extends AbstractAction {
		public ActionNatureLolfProvisoire() {
			super("Nature / Lolf");
			setToolTip("Repartition nature/lolf du budget PROVISOIRE");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			SaisieNatureLolfCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Vote du budget Provisoire
	 *
	 */
	private final class ActionVoteProvisoire extends AbstractAction {
		public ActionVoteProvisoire() {
			super("Vote");
			setToolTip("Vote du budget PROVISOIRE");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VOTE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			VoteBudgetCtrl.sharedInstance(ec).open(currentBudgetSaisie);
			updateUI();
			updateUISousMenus();
		}
	}

	/**
	 *
	 * Ouverture du budget Initial
	 *
	 */
	private final class ActionOuvertureInitial extends AbstractAction {
		public ActionOuvertureInitial() {
			super("Ouverture");
			setToolTip("Ouverture du budget INITIAL");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_OUVERTURE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous l'OUVERTURE du Budget INITIAL "
							+ NSApp.getExerciceBudgetaire().exeExercice()
							+ " ?", "OUI", "NON")) {
				try {
					if (FactoryBibasse.controlerOuvertureBudget(ec,
							NSApp.getExerciceBudgetaire()))
						currentBudgetSaisie = SaisieBudget.sharedInstance(ec)
								.initBudgetSaisie();
				} catch (Exception ex) {
					EODialogs
							.runInformationDialog("ATTENTION", ex.getMessage());
				}
				updateUI();
				updateUISousMenus();
			}
		}
	}

	/**
	 *
	 * Saisie du budget Initial
	 *
	 */
	private final class ActionSaisieInitial extends AbstractAction {
		public ActionSaisieInitial() {
			super("Saisie");
			setToolTip("Saisie du budget INITIAL");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			SaisieBudget.sharedInstance(ec).open();
		}
	}

	/**
	 *
	 * Pilotage du budget Initial
	 *
	 */
	private final class ActionPilotageInitial extends AbstractAction {
		public ActionPilotageInitial() {
			super("Pilotage");
			setToolTip("Pilotage du budget INITIAL");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_PILOTAGE);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			PilotageCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Repartition nature/lolf du budget Initial
	 *
	 */
	private final class ActionNatureLolfInitial extends AbstractAction {
		public ActionNatureLolfInitial() {
			super("Nature / Lolf");
			setToolTip("Repartition nature/lolf du budget INITIAL");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			SaisieNatureLolfCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Vote du budget Initial
	 *
	 */
	private final class ActionVoteInitial extends AbstractAction {
		public ActionVoteInitial() {
			super("Vote");
			setToolTip("Vote du budget INITIAL");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VOTE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			VoteBudgetCtrl.sharedInstance(ec).open(currentBudgetSaisie);
			updateUI();
			updateUISousMenus();
		}
	}

	/**
	 *
	 * Ouverture du budget RELIQUATS
	 *
	 */
	private final class ActionOuvertureReliquats extends AbstractAction {
		public ActionOuvertureReliquats() {
			super("Ouverture");
			setToolTip("Ouverture du budget RELIQUATS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_OUVERTURE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous l'OUVERTURE d'un budget RELIQUATS pour l'exercice "
							+ NSApp.getExerciceBudgetaire().exeExercice()
							+ " ?", "OUI", "NON")) {
				currentBudgetSaisie = SaisieBudget.sharedInstance(ec)
						.initBudgetSaisie();
				updateUISousMenus();
			}
		}
	}

	/**
	 *
	 * Saisie du budget RELIQUATS
	 *
	 */
	private final class ActionSaisieReliquats extends AbstractAction {
		public ActionSaisieReliquats() {
			super("Saisie");
			setToolTip("Saisie du budget RELIQUATS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			SaisieBudget.sharedInstance(ec).open();
		}
	}

	/**
	 *
	 * Pilotage du budget RELIQUATS
	 *
	 */
	private final class ActionPilotageReliquats extends AbstractAction {
		public ActionPilotageReliquats() {
			super("Pilotage");
			setToolTip("Pilotage du budget RELIQUATS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_PILOTAGE);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			PilotageCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Repartition nature/lolf du budget RELIQUATS
	 *
	 */
	private final class ActionNatureLolfReliquats extends AbstractAction {
		public ActionNatureLolfReliquats() {
			super("Nature / Lolf");
			setToolTip("Repartition nature/lolf du budget RELIQUATS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			SaisieNatureLolfCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Vote du budget RELIQUATS
	 *
	 */
	private final class ActionVoteReliquats extends AbstractAction {
		public ActionVoteReliquats() {
			super("Vote");
			setToolTip("Vote du budget RELIQUATS");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VOTE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			VoteBudgetCtrl.sharedInstance(ec).open(currentBudgetSaisie);
			updateUI();
			updateUISousMenus();
		}
	}

	/**
	 *
	 * Suppression (si possible) de la DBM selectionnee
	 *
	 */
	private final class ActionCancelDbm extends AbstractAction {
		public ActionCancelDbm() {
			super("Annuler DBM");
			setToolTip("Suppression de la DBM en cours");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_CANCEL);
		}

		public void actionPerformed(ActionEvent e) {
			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous la suppression du bugdet : "
							+ currentBudgetSaisie.bdsaLibelle() + " ?", "OUI",
					"NON")) {
				SaisieBudget.sharedInstance(ec).deleteBudget();
				actionBudgetDbm.actionPerformed(null);
			}
		}
	}

	/**
	 *
	 * Suppression (si possible) du budget selectionne
	 *
	 */
	private final class ActionDeleteBudget extends AbstractAction {
		public ActionDeleteBudget() {
			super("Annuler budget");
			setToolTip("Suppression du budget en cours");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_CANCEL);
		}

		public void actionPerformed(ActionEvent e) {
			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous la suppression du bugdet : "
							+ currentBudgetSaisie.bdsaLibelle() + " ?", "OUI", "NON")) {
				SaisieBudget.sharedInstance(ec).deleteBudget();
				updateIcons(null);
				updateUI();
				updateUISousMenus();
				((CardLayout) swapViewSousMenus.getLayout()).show(swapViewSousMenus, "vide");
			}
		}
	}

	/**
	 *
	 * Ouverture du budget DBM
	 *
	 */
	private final class ActionOuvertureDbm extends AbstractAction {
		public ActionOuvertureDbm() {
			super("Ouverture");
			setToolTip("Ouverture du budget DBM");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_OUVERTURE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			if (EODialogs.runConfirmOperationDialog("Attention",
					"Confirmez-vous l'OUVERTURE d'une nouvelle DBM pour l'exercice "
							+ NSApp.getExerciceBudgetaire().exeExercice()
							+ " ?", "OUI", "NON")) {
				currentBudgetSaisie = SaisieBudget.sharedInstance(ec)
						.initBudgetSaisie();
				actionBudgetDbm.actionPerformed(null);
			}
		}
	}

	/**
	 * Saisie du budget DBM.
	 */
	private final class ActionSaisieDbm extends AbstractAction {
		public ActionSaisieDbm() {
			super("Saisie");
			setToolTip("Saisie du budget DBM");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			SaisieBudget.sharedInstance(ec).open();
		}
	}

	/**
	 *
	 * Pilotage du budget DBM
	 *
	 */
	private final class ActionPilotageDbm extends AbstractAction {
		public ActionPilotageDbm() {
			super("Pilotage");
			setToolTip("Pilotage du budget DBM");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_PILOTAGE);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			PilotageCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Repartition nature/lolf du budget DBM
	 *
	 */
	private final class ActionNatureLolfDbm extends AbstractAction {
		public ActionNatureLolfDbm() {
			super("Nature / Lolf");
			setToolTip("Repartition nature/lolf du budget DBM");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			SaisieNatureLolfCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Controle saisie du budget DBM
	 *
	 */
	private final class ActionCtrlSaisieDbm extends AbstractAction {
		public ActionCtrlSaisieDbm() {
			super(" contrôle Saisie ");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_IMPRIMER_16);
		}

		public void actionPerformed(ActionEvent e) {
			EditionsCtrl.sharedInstance(ec)
					.printCtrlSaisie(currentBudgetSaisie);
		}
	}

	/**
	 *
	 * Vote du budget DBM
	 *
	 */
	private final class ActionVoteDbm extends AbstractAction {
		public ActionVoteDbm() {
			super("Vote");
			setToolTip("Vote du budget DBM");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VOTE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			VoteBudgetCtrl.sharedInstance(ec).open(currentBudgetSaisie);
			updateUI();
			updateUISousMenus();
		}
	}

	/**
	 *
	 * Ouverture du budget Convention
	 *
	 */
	private final class ActionOuvertureConvention extends AbstractAction {
		public ActionOuvertureConvention() {
			super("Ouverture");
			setToolTip("Ouverture du budget CONVENTION");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_OUVERTURE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			currentBudgetSaisie = SaisieBudget.sharedInstance(ec)
					.initBudgetSaisie();
			updateUISousMenus();
		}
	}

	/**
	 *
	 * Saisie du budget Convention
	 *
	 */
	private final class ActionSaisieConvention extends AbstractAction {
		public ActionSaisieConvention() {
			super("Saisie");
			setToolTip("Saisie du budget CONVENTION");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			SaisieBudget.sharedInstance(ec).open();
		}
	}

	/**
	 *
	 * Pilotage du budget Convention
	 *
	 */
	private final class ActionPilotageConvention extends AbstractAction {
		public ActionPilotageConvention() {
			super("Pilotage");
			setToolTip("Pilotage du budget CONVENTION");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_PILOTAGE);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			PilotageCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Repartition nature/lolf du budget Convention
	 *
	 */
	private final class ActionNatureLolfConvention extends AbstractAction {
		public ActionNatureLolfConvention() {
			super("Nature / Lolf");
			setToolTip("Repartition nature/lolf du budget CONVENTION");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_SAISIE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			NSApp.setWaitCursor(window);
			SaisieNatureLolfCtrl.sharedInstance(ec).open();
			NSApp.setDefaultCursor(window);
		}
	}

	/**
	 *
	 * Vote du budget Convention
	 *
	 */
	private final class ActionVoteConvention extends AbstractAction {
		public ActionVoteConvention() {
			super("Vote");
			setToolTip("Vote du budget CONVENTION");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_VOTE_BUDGET);
		}

		public void actionPerformed(ActionEvent e) {
			VoteBudgetCtrl.sharedInstance(ec).open(currentBudgetSaisie);
			updateUI();
			updateUISousMenus();
		}
	}

	/**
	 *
	 * Quitter l'application
	 *
	 */
	private final class ActionQuit extends AbstractAction {

		public ActionQuit() {
			super("Quitter");
			setToolTip("Quitter l'application");
			this.putValue(AbstractAction.SMALL_ICON,
					ConstantesCocktail.ICON_QUIT);
		}

		public void actionPerformed(ActionEvent e) {

			NSApp.quit();

		}
	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire Mise à jour
	 * du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerDbms implements ActionListener {

		public ListenerDbms() {
			super();
		}

		public void actionPerformed(ActionEvent anAction) {

			if (popupDbms.getSelectedIndex() >= 0) {
				currentBudgetSaisie = (EOBudgetSaisie) popupDbms
						.getSelectedItem();
			} else {
				currentBudgetSaisie = null;
			}

			PilotageCtrl.sharedInstance(ec).cleanBudgetsNature();
			PilotageCtrl.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			OrganPopupCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec)
					.setBudgetSaisie(currentBudgetSaisie);
			SaisieBudget.sharedInstance(ec).setTypeSaisie(currentTypeSaisie);
			SaisieNatureLolfCtrl.sharedInstance(ec).setBudgetSaisie(
					currentBudgetSaisie);

			SaisieBudget.sharedInstance(ec).clean();

			updateUISousMenus();
		}
	}
}