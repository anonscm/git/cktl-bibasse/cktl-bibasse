/*
 * Created on 7 juil. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.bibasse.client.exception;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 *  
 */
public class ExceptionBibasse extends RuntimeException {

    public static String objetNull   = " NULL";

    public static String tableauVide = " TABLEAU VIDE";

    private String       message;

    public ExceptionBibasse() {
        super();

    }

    public ExceptionBibasse(String s) {
        super();
        this.setMessage(s);
    }

    /**
     * renvoie une cause message pour l execepeion
     * 
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * affecte une cause message pour l execepeion
     * 
     * @param string
     */
    public void setMessage(
            String string) {
        message = string;
    }

}
