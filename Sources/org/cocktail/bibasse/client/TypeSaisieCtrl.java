package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.finder.FinderTypeSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOTypeSaisie;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation.ValidationException;

public class TypeSaisieCtrl {

	private static TypeSaisieCtrl sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	private JPanel mainPanel;
	private	JDialog mainWindow;

    private ActionValidate	actionValidate = new ActionValidate();
    private ActionClose	actionClose = new ActionClose();

    private JTextField titre;

    private EOBudgetSaisie currentBudgetSaisie;

    private	JLabel	labelTypeSaisieOrigine;

    private JComboBox newTypeSaisie;

	/**
	 *
	 *
	 */
	public TypeSaisieCtrl(EOEditingContext editingContext)	{

		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		initView();
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static TypeSaisieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new TypeSaisieCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 *
	 *
	 */
	private void initTextFields()	{

		labelTypeSaisieOrigine = new JLabel("");
		labelTypeSaisieOrigine.setForeground(ConstantesCocktail.BG_COLOR_BLUE);
		labelTypeSaisieOrigine.setFont(Configuration.instance().informationTitreFont(ec));

        titre = new JTextField("");
        titre.setBackground(new Color(236,234,149));
        titre.setForeground(new Color(0,0,0));
        titre.setBorder(BorderFactory.createEmptyBorder());
        titre.setFont(Configuration.instance().informationTitreMenuFont(ec));
        titre.setHorizontalAlignment(JTextField.CENTER);
        titre.setEditable(false);
        titre.setFocusable(false);

	}




	/**
	 *
	 * @return
	 */
	public JPanel buildSouthPanel()	{

        // Boutons
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		arrayList.add(actionValidate);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 110, 22));

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,4,2,4));
		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelButtons, BorderLayout.EAST);

		return panel;
	}

	/**
	 *
	 *
	 */
	private void initComboBoxs()	{

		newTypeSaisie = new JComboBox();
		newTypeSaisie.setPreferredSize(new Dimension(125, 22));

	}

	/**
	 *
	 *
	 */
	private void initView()	{


    	mainWindow = new JDialog(NSApp.superviseur().mainFrame(), "Vote du budget", true);
		mainWindow.setTitle("BIBASSE - VOTE du BUDGET");

    	mainPanel = new JPanel(new BorderLayout());
    	mainPanel.setPreferredSize(new Dimension(500, 125));
    	mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		initComboBoxs();

    	initTextFields();

    	mainPanel.add(buildNorthPanel(), BorderLayout.NORTH);

    	mainPanel.add(buildCenterPanel(), BorderLayout.CENTER);

    	mainPanel.add(buildSouthPanel(), BorderLayout.SOUTH);

    	mainWindow.setContentPane(mainPanel);
    	mainWindow.pack();

	}


	private JPanel buildNorthPanel()	{

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(titre, BorderLayout.CENTER);


        return panel;
	}


	/**
	 *
	 * @return
	 */
	private JPanel buildCenterPanel()	{

		JPanel centerPanel = new JPanel(new BorderLayout());

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(5,5,15,5));

		JLabel label1 = new JLabel("Modifier le BUDGET ");

		JLabel label2 = new JLabel(" en BUDGET ");
		label2.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JPanel panelLabel1= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{label1},
				BorderLayout.WEST);
		panelLabel1.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelTypeOrigine= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{labelTypeSaisieOrigine},
				BorderLayout.WEST);
		panelTypeOrigine.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelLabel2= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{label2},
				BorderLayout.WEST);
		panelLabel2.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		JPanel panelNewTypeSaisie= (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{newTypeSaisie},
				BorderLayout.WEST);
		panelNewTypeSaisie.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		panel = (JPanel)ZUiUtil.buildBoxLine(
				new Component[]{panelLabel1, panelTypeOrigine, panelLabel2, panelNewTypeSaisie},
				BorderLayout.WEST);
		panel.setBorder(BorderFactory.createEmptyBorder(5,2,5,2));

		centerPanel.add(panel, BorderLayout.NORTH);

		return centerPanel;
	}



	/**
	 *
	 *
	 */
	public void open(EOBudgetSaisie budgetSaisie)	{

		currentBudgetSaisie = budgetSaisie;

		if (budgetSaisie != null)	{
			labelTypeSaisieOrigine.setText(currentBudgetSaisie.typeSaisie().tysaLibelle());

			updateNewTypeSaisie();

			titre.setText("Exercice " + budgetSaisie.exercice().exeExercice() + " - Changement de type de BUDGET");

			ZUiUtil.centerWindow(mainWindow);
			mainWindow.show();
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private  final class ActionValidate extends AbstractAction {

		public ActionValidate() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
		}

		public void actionPerformed(ActionEvent e) {


			try {
				NSMutableDictionary parametres = new NSMutableDictionary();

				if (newTypeSaisie.getSelectedIndex() == 0)	{
					EODialogs.runErrorDialog("ERREUR","Veuillez saisir le nouveau type de budget");
					return;
				}

				NSApp.setWaitCursor(mainPanel);

				EOTypeSaisie typeDestination = (EOTypeSaisie)newTypeSaisie.getSelectedItem();

				parametres.setObjectForKey((EOEnterpriseObject)currentBudgetSaisie, "EOBudgetSaisie");

				parametres.setObjectForKey((EOEnterpriseObject)typeDestination, "EOTypeSaisie");

				ServerProxy.clientSideRequestChangerSaisieBudget(ec, parametres);

				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentBudgetSaisie)));

				ec.saveChanges();

				EODialogs.runInformationDialog("","Le budget " + currentBudgetSaisie.typeSaisie().tysaLibelle()
						+ " " + NSApp.getExerciceBudgetaire().exeExercice()
						+ " a bien été modifié en budget " + typeDestination.tysaLibelle() + " !");

				mainWindow.dispose();
				NSApp.superviseur().refreshMenu();

			}
			catch (ValidationException ex)	{
				ec.revert();
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", "(Echec de la procédure de refus du budget (ValidationException) !\n\n" + ex.getMessage());
			}
			catch (Exception ex)	{
				ec.revert();
				ex.printStackTrace();
				EODialogs.runErrorDialog("ERREUR", "Erreur lors du VOTE du budget (Exception) !\n\n" + ex.getMessage());
			}

			NSApp.setDefaultCursor(mainPanel);
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private  final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
		}

		public void actionPerformed(ActionEvent e) {

        	mainWindow.dispose();

		}
	}


	/**
	 *
	 *
	 */
	private void updateNewTypeSaisie()	{

		newTypeSaisie.removeAllItems();

		newTypeSaisie.addItem("*");

		if (currentBudgetSaisie.isBudgetProvisoire())
			newTypeSaisie.addItem(FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_INITIAL));
		else
			if (currentBudgetSaisie.isBudgetInitial())
				newTypeSaisie.addItem(FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_PROVISOIRE));

	}

}
