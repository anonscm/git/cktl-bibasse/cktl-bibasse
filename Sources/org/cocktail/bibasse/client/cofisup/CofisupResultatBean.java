package org.cocktail.bibasse.client.cofisup;

import java.io.File;

/**
 * Bean representant un resultat d'extraction Cofisup.
 */
public class CofisupResultatBean {

	/** Identifiant du resultat cote serveur. */
	private String remoteId;

	/** Titre. */
	private String title;

	/** Marqueur de succes. */
	private boolean success;

	/** Message. */
	private String statusMessage;

	/** Fichier resultat. */
	private File resultFile;

	/**
	 * Constructeur.
	 * @param remoteId identifiant resultat serveur.
	 * @param title titre.
	 */
	public CofisupResultatBean(String remoteId, String title) {
		this(remoteId, title, true, "", null);
	}

	/**
	 * Constructeur.
	 * @param remoteId identifiant resultat serveur.
	 * @param title titre.
	 * @param resultFile fichier resultat.
	 */
	public CofisupResultatBean(String remoteId, String title, File resultFile) {
		this(remoteId, title, true, "", resultFile);
	}

	/**
	 * Constructeur.
	 * @param remoteId identifiant resultat serveur.
	 * @param title titre.
	 * @param message message.
	 */
	public CofisupResultatBean(String remoteId, String title, String message) {
		this(remoteId, title, false, message, null);
	}

	/**
	 * Constructeur.
	 * @param remoteId identifiant resultat serveur.
	 * @param title titre.
	 * @param status marqueur.
	 * @param statusMessage message.
	 * @param resultFile fichier resultat.
	 */
	private CofisupResultatBean(String remoteId, String title, boolean status, String statusMessage, File resultFile) {
		this.remoteId = remoteId;
		this.title = title;
		this.success = status;
		this.statusMessage = statusMessage;
		this.resultFile = resultFile;
	}

	public String getRemoteId() {
		return remoteId;
	}

	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public File getResultFile() {
		return resultFile;
	}

	public void setResultFile(File resultFile) {
		this.resultFile = resultFile;
	}
}
