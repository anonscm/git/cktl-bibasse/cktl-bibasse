package org.cocktail.bibasse.client.cofisup;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JDialog;

import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.common.cofisup.CofisupActionConstants;
import org.cocktail.common.cofisup.ETypeBudget;
import org.cocktail.common.cofisup.ETypeFichier;
import org.cocktail.common.cofisup.ETypeNatureBudget;
import org.cocktail.common.cofisup.JobParameters;
import org.jdesktop.swingworker.SwingWorker;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 *
 * Tache de fond Cofisup : génère les fichiers et rafraichit l'interface principale au fur et a mesure.
 * @author flagoueyte.
 *
 */
public class CofisupTask extends SwingWorker<Void, CofisupResultatBean> {

	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final int EOF = -1;
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
    private static final long WAIT_TIMER = 500L;
    private static final String EXCEPTION_MARKER = "CofisupException";
    private static final String INFO_NO_FILE = "Aucun fichier généré";
    private static final String RESULT_TITLE = "{0} - {1} - {2}";
    private static final String EXTENSION = ".csv";

	private EOEditingContext ec;
    private String tempDir;
	private CofisupResultatsModel resultsModel;
	private List<CofisupExtractionBean> extractions;
	private EOExercice exerciceCourant;
	private EOExercice exercicePrecedent;
	private JDialog waitingDialog;

	/**
	 * Constructeur.
	 * @param ec editing context.
	 * @param tempDir chemin repertoire temporaire.
	 * @param resultsModel model pour stocker les resultats.
	 * @param extractions liste extractions demandées.
	 * @param exerciceCourant exercice budgetaire en cours.
	 * @param exercicePrecedent exercice budgetaire précédent.
	 * @param waitingDialog boite de dialogue d'attente.
	 */
	public CofisupTask(EOEditingContext ec, String tempDir, CofisupResultatsModel resultsModel,
			List<CofisupExtractionBean> extractions, EOExercice exerciceCourant, EOExercice exercicePrecedent,
			JDialog waitingDialog) {
		this.ec = ec;
		this.tempDir = tempDir;
		this.resultsModel = resultsModel;
		this.extractions = extractions;
		this.exerciceCourant = exerciceCourant;
		this.exercicePrecedent = exercicePrecedent;
		this.waitingDialog = waitingDialog;
	}

	@Override
	protected Void doInBackground()  {
		Iterator<CofisupResultatBean> iteExpectedResults = launch(getExtractions()).iterator();
		while (iteExpectedResults.hasNext()) {
			publish(retrieveNextFile(iteExpectedResults.next()));
		}

		return null;
	}

	@Override
    protected void process(List<CofisupResultatBean> chunks) {
        for (CofisupResultatBean currentResult : chunks) {
        	resultsModel.add(currentResult);
        }
    }

	@Override
	protected void done() {
		super.done();
		hideWaitingWindow();
		if (hasError()) {
			EODialogs.runErrorDialog("Fin traitement", "Des anomalies ont eu lieu pendant le traitement.");
		} else {
			EODialogs.runInformationDialog("Fin traitement", "Extractions Cofisup terminées avec succès.");
		}
	}

	private void hideWaitingWindow() {
		this.waitingDialog.setVisible(false);
	}

	private boolean hasError() {
		boolean hasError = false;
		Iterator<CofisupResultatBean> iteResultats = resultsModel.getData().iterator();
		while (iteResultats.hasNext() && !hasError) {
			hasError |= !iteResultats.next().isSuccess();
		}
		return hasError;
	}

	private List<CofisupResultatBean> launch(List<CofisupExtractionBean> extractions) {
		List<CofisupResultatBean> launchedJobs = new ArrayList<CofisupResultatBean>();
		for (CofisupExtractionBean currentBean : extractions) {
			if (Boolean.TRUE.equals(currentBean.isRdBp())) {
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BP, ETypeFichier.RD));
			}
			if (Boolean.TRUE.equals(currentBean.isRdBm())) {
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BM, ETypeFichier.RD));
			}
			if (Boolean.TRUE.equals(currentBean.isRdEb())) {
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exercicePrecedent, currentBean, ETypeBudget.EB, ETypeFichier.RD));
			}
			if (Boolean.TRUE.equals(currentBean.isIcBp())) {
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BP, ETypeFichier.CR));
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BP, ETypeFichier.TF));
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BP, ETypeFichier.BPI));
			}
			if (Boolean.TRUE.equals(currentBean.isIcBm())) {
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BM, ETypeFichier.CR));
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BM, ETypeFichier.TF));
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exerciceCourant, currentBean, ETypeBudget.BM, ETypeFichier.BPI));
			}
			if (Boolean.TRUE.equals(currentBean.isIcEb())) {
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exercicePrecedent, currentBean, ETypeBudget.EB, ETypeFichier.CR));
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exercicePrecedent, currentBean, ETypeBudget.EB, ETypeFichier.TF));
				launchedJobs.addAll(decorateLaunchCofisup(exerciceCourant, exercicePrecedent, currentBean, ETypeBudget.EB, ETypeFichier.BPI));
			}
		}

		return launchedJobs;
	}

	private List<CofisupResultatBean> decorateLaunchCofisup(EOExercice exerciceEnCours, EOExercice exercice, CofisupExtractionBean extractionInfos, ETypeBudget typeBudget, ETypeFichier typeFichier) {
		List<CofisupResultatBean> results = new ArrayList<CofisupResultatBean>();

		ETypeNatureBudget typeNatureBudget = extractionInfos.getNature();

		if (incoherenceNatureBpi(typeNatureBudget, typeFichier) || incoherenceFichierBpi(typeNatureBudget, typeFichier)) {
			return new ArrayList<CofisupResultatBean>();
		}

		String libelleTypeFichier = libelleTypeFichier(typeFichier);
		NSArray jobsId = launchCofisup(exerciceEnCours, exercice, extractionInfos, typeBudget, typeFichier);
		if (jobsId.isEmpty()) {
			publish(new CofisupResultatBean(
						"",
						MessageFormat.format(RESULT_TITLE, extractionInfos.getLabel(), libelleTypeFichier, typeBudget),
						INFO_NO_FILE));
		}

		for (int idx = 0; idx < jobsId.count(); idx++) {
			results.add(new CofisupResultatBean(
					(String) jobsId.objectAtIndex(idx),
					MessageFormat.format(RESULT_TITLE, extractionInfos.getLabel(), libelleTypeFichier, typeBudget)));
		}

		return results;
	}

	private NSArray launchCofisup(EOExercice exerciceEnCours, EOExercice exercice, CofisupExtractionBean extractionInfos, ETypeBudget typeBudget, ETypeFichier typeFichier) {
		ETypeNatureBudget typeNatureBudget = extractionInfos.getNature();
		Set<ETypeNatureBudget> typesNatureBudget = null;
		switch(typeNatureBudget) {
		case BPI :
			typesNatureBudget = EnumSet.of(ETypeNatureBudget.BPI_IUT, ETypeNatureBudget.BPI_ESPE);
			break;
		default :
			typesNatureBudget = EnumSet.of(typeNatureBudget);
		}

		NSMutableArray futurJobIds = new NSMutableArray();
		for (ETypeNatureBudget curNature : typesNatureBudget) {
			final NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.takeValueForKey(exerciceEnCours.exeOrdre(), JobParameters.PARAM_EXERCICE_EN_COURS);
			parametres.takeValueForKey(exercice.exeOrdre(), JobParameters.PARAM_EXERCICE);
			parametres.takeValueForKey(extractionInfos.getTypeEtablissement().toString(), JobParameters.PARAM_TYPE_ETABLISSEMENT);
			parametres.takeValueForKey(curNature.toString(), JobParameters.PARAM_NATURE_BUDGET);
			parametres.takeValueForKey(typeBudget.toString(), JobParameters.PARAM_TYPE_BUDGET);
			parametres.takeValueForKey(typeFichier.toString(), JobParameters.PARAM_TYPE_FICHIER);
			parametres.takeValueForKey(extractionInfos.getLabel(), JobParameters.PARAM_LIBELLE);

			futurJobIds.addObjectsFromArray(CofisupProxy.instance().clientStartJob(ec, parametres));
		}

		return futurJobIds;
	}

	private CofisupResultatBean retrieveNextFile(CofisupResultatBean resultat) {
		String status = null;
		String jobIdCompleted = null;
		String jobFilenameCompleted = null;
		String msg = null;
		boolean isDone = false;

		try {
			while (!isDone) {
				Thread.sleep(WAIT_TIMER);
				NSDictionary infos = CofisupProxy.instance().clientInfosJob(ec);
				status = (String) infos.objectForKey(CofisupActionConstants.INFO_STATUS);
				jobIdCompleted = (String) infos.objectForKey(CofisupActionConstants.INFO_JOB_ID);
				jobFilenameCompleted = (String) infos.objectForKey(CofisupActionConstants.INFO_JOB_FILE);
				msg = (String) infos.objectForKey(CofisupActionConstants.INFO_MSG);

				isDone = !CofisupActionConstants.STATUS_PENDING.equalsIgnoreCase(status);
			}

			if (CofisupActionConstants.STATUS_ERROR.equals(status)) {
				resultat.setSuccess(false);
				resultat.setStatusMessage(msg);
			} else {
				resultat.setSuccess(true);
				resultat.setResultFile(copyFileFromServer(jobIdCompleted, jobFilenameCompleted));
			}

		} catch (InterruptedException ie) {
			resultat = handleException(resultat, ie);
		} catch (IOException ioe) {
			resultat = handleException(resultat, ioe);
		} catch (Exception e) {
			resultat = handleException(resultat, e);
		}

		return resultat;
	}

	private CofisupResultatBean handleException(CofisupResultatBean resultat, Throwable t) {
		resultat.setSuccess(false);

		if (t == null) {
			return resultat;
		}
		String message = t.getMessage();
		if (message == null) {
			return resultat;
		}
		int idxMarker = message.lastIndexOf(EXCEPTION_MARKER);
		if (idxMarker > -1) {
			message = message.substring(idxMarker + EXCEPTION_MARKER.length() + 2);
		}

		resultat.setStatusMessage(message);
		return resultat;
	}

	private String libelleTypeFichier(ETypeFichier typeFichier) {
		String libelleTypeFichier = null;
		switch (typeFichier) {
		case RD :
			libelleTypeFichier = CofisupExtractionSelectDialog.RD_LABEL;
			break;
		default :
			libelleTypeFichier = CofisupExtractionSelectDialog.IC_LABEL + " - " + typeFichier.getLibelle();
		}
		return libelleTypeFichier;
	}

	private boolean incoherenceNatureBpi(ETypeNatureBudget typeNatureBudget, ETypeFichier typeFichier) {
		return typeNatureBudget.isBPI() && EnumSet.of(ETypeFichier.CR, ETypeFichier.TF).contains(typeFichier);
	}

	private boolean incoherenceFichierBpi(ETypeNatureBudget typeNatureBudget, ETypeFichier typeFichier) {
		return !typeNatureBudget.isBPI() && typeFichier.isBPI();
	}

	private File copyFileFromServer(String jobId, String jobFilename) throws IOException {
		NSData serverData = CofisupProxy.instance().clientRetrieveFile(ec, jobId);

		File destinationFile = outputFile(jobFilename);
		OutputStream destinationStream = new BufferedOutputStream(new FileOutputStream(destinationFile));
		OutputStreamWriter writer = new OutputStreamWriter(destinationStream, DEFAULT_ENCODING);

		InputStream fromStream = serverData.stream();
		Reader reader = new InputStreamReader(fromStream, DEFAULT_ENCODING);

		copy(reader, writer);

		writer.close();
		fromStream.close();

		return destinationFile;
	}

	private long copy(Reader input, Writer output) throws IOException {
		char[] buffer = new char[DEFAULT_BUFFER_SIZE];
        long count = 0;
        int n = 0;
        while (EOF != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
	}

	private File outputFile(String jobId) throws IOException {
		return new File(tempDir, jobId + EXTENSION);
	}

	public List<CofisupExtractionBean> getExtractions() {
		return extractions;
	}
}
