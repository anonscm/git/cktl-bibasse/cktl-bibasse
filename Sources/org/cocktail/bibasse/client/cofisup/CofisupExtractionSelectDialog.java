package org.cocktail.bibasse.client.cofisup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.UIResource;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.zutil.ui.ZCommentPanel;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ColumnGroup;
import org.cocktail.bibasse.client.zutil.wo.table.GroupableTableColumnModel;
import org.cocktail.bibasse.client.zutil.wo.table.GroupableTableHeader;

public class CofisupExtractionSelectDialog extends JDialog implements TableModelListener {

	public static final String RD_LABEL = "Recettes / Dépenses";
	public static final String IC_LABEL = "Informations complémentaires";

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	/** UI Text. */
	private static final String WINDOW_TITLE = "Extractions Cofisup Budget";
	private static final String COMMENT =
			"<html>"
			+ "<p>Extractions des établissements (non) soumis à l'instruction M9-3 RCE.</p>"
			+ "<p>Extractions basées sur la nature de budget qui caractérise les lignes de l'organigramme budgétaire dans JefyAdmin (Principal, Annexe EPRD, Annexe non EPRD, SIE, IUT / BPI, IUT/ESPE …).</p>"
			+ "<p>BP = Budget primitif ; BM = Budget modifié (Budget rectificatif [DBM], Reports, Ressources affectées, Virements ; EB = Exécution budgétaire de l'exercice précédent</p>"
			+ "</html>";
	private static final String TITLE_RESULTATS = "Fichiers résultats";

	private JPanel mainPanel;
	private JTable selectionTable;

	private JButton extractButton;
	private JButton cancelButton;
	private JButton saveAsButton;

    private ActionExtract extractAction = new ActionExtract();
    private ActionCancel cancelAction = new ActionCancel();
    private ActionSaveAs saveAsAction = new ActionSaveAs();

    private CofisupExtractionModel extractionModel;
    private CofisupResultatsModel resultatsModel;

    private CofisupExtractionSelectCtrl controller;

	public CofisupExtractionSelectDialog(Frame owner, CofisupExtractionModel extractModel, CofisupResultatsModel resultsModel, CofisupExtractionSelectCtrl controller) {
		super(owner, WINDOW_TITLE, true);
		this.extractionModel = extractModel;
		this.resultatsModel = resultsModel;
		this.controller = controller;
		init();
	}

	public void tableChanged(TableModelEvent e) {
		TableModel model = (TableModel) e.getSource();
		if (model instanceof CofisupResultatsModel) {
			CofisupResultatsModel localResultatsModel = (CofisupResultatsModel) model;
			saveAsButton.setEnabled(!localResultatsModel.getData().isEmpty());
		}
	}

	public void resetHeaders() {
		TableColumnModel columnModel = this.selectionTable.getColumnModel();
		Enumeration<TableColumn> columnsEnum = columnModel.getColumns();
		while (columnsEnum.hasMoreElements()) {
			TableColumn column = columnsEnum.nextElement();
			TableCellRenderer columnHeaderRenderer = column.getHeaderRenderer();
			if (columnHeaderRenderer instanceof CheckBoxHeaderRenderer) {
				CheckBoxHeaderRenderer columnCBoxHeaderRenderer = (CheckBoxHeaderRenderer) columnHeaderRenderer;
				columnCBoxHeaderRenderer.getCheckBox().setSelected(false);
			}
		}
	}

	private void init() {
		mainPanel = new JPanel(new BorderLayout());
    	mainPanel.setPreferredSize(new Dimension(1000, 550));
    	mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

    	mainPanel.add(buildDescriptionPanel(), BorderLayout.PAGE_START);
    	mainPanel.add(buildGenerationPanel(), BorderLayout.CENTER);
    	mainPanel.add(buildActionsPanel(), BorderLayout.PAGE_END);

    	setContentPane(mainPanel);
    	pack();
	}

	private JPanel buildDescriptionPanel() {
		return new ZCommentPanel(null, COMMENT, null, Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.WEST);

	}

	private JPanel buildGenerationPanel()	{
		BorderLayout panelLayout = new BorderLayout();
		panelLayout.setHgap(8);
		JPanel panel = new JPanel(panelLayout);
		panel.setBorder(BorderFactory.createEmptyBorder());

		JPanel selectPanel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder());
		this.selectionTable = setupTable();
		selectPanel.add(selectionTable.getTableHeader(), BorderLayout.PAGE_START);
		selectPanel.add(selectionTable, BorderLayout.CENTER);

		panel.add(selectPanel, BorderLayout.PAGE_START);
		panel.add(setupResultats());

		return panel;
	}

	private JTable setupTable() {
		ItemListener headerListener = new CheckboxHeaderItemListener();
		JTable table = new JTable();
		table.setRowSelectionAllowed(false);
		table.setDefaultRenderer(Boolean.class, new CofisupCheckBoxCellRenderer());
        table.setColumnModel(new GroupableTableColumnModel());
        table.setTableHeader(new GroupableTableHeader((GroupableTableColumnModel) table.getColumnModel()));
       	table.setModel(getExtractionModel());

        GroupableTableColumnModel columnModel = (GroupableTableColumnModel) table.getColumnModel();

        ColumnGroup groupeRecettesDepenses = new ColumnGroup(RD_LABEL);
        TableColumn columnRDBP = columnModel.getColumn(1);
        TableColumn columnRDBM = columnModel.getColumn(2);
        TableColumn columnRDEB = columnModel.getColumn(3);

        columnRDBP.setHeaderRenderer(new CheckBoxHeaderRenderer(headerListener));
        columnRDBM.setHeaderRenderer(new CheckBoxHeaderRenderer(headerListener));
        columnRDEB.setHeaderRenderer(new CheckBoxHeaderRenderer(headerListener));

        groupeRecettesDepenses.add(columnRDBP);
        groupeRecettesDepenses.add(columnRDBM);
        groupeRecettesDepenses.add(columnRDEB);

        ColumnGroup groupeInfosCplt = new ColumnGroup(IC_LABEL);

        TableColumn columnICBP = columnModel.getColumn(4);
        TableColumn columnICBM = columnModel.getColumn(5);
        TableColumn columnICEB = columnModel.getColumn(6);

        columnICBP.setHeaderRenderer(new CheckBoxHeaderRenderer(headerListener));
        columnICBM.setHeaderRenderer(new CheckBoxHeaderRenderer(headerListener));
        columnICEB.setHeaderRenderer(new CheckBoxHeaderRenderer(headerListener));

        groupeInfosCplt.add(columnICBP);
        groupeInfosCplt.add(columnICBM);
        groupeInfosCplt.add(columnICEB);

        columnModel.addColumnGroup(groupeRecettesDepenses);
        columnModel.addColumnGroup(groupeInfosCplt);

		TableColumn column = null;
		for (int i = 0; i < table.getColumnCount(); i++) {
		    column = table.getColumnModel().getColumn(i);
		    if (i == 0) {
		        column.setPreferredWidth(350);
		    } else {
		        column.setPreferredWidth(5);
		    }
		}

		return table;
	}

	private JComponent setupResultats() {
		JPanel resultatsPanel = new JPanel(new BorderLayout());
		resultatsPanel.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                TITLE_RESULTATS, TitledBorder.CENTER, TitledBorder.TOP));

		JTable resultatsTable = new JTable();
		resultatsTable.setRowSelectionAllowed(false);
		resultatsTable.setTableHeader(null);
		resultatsTable.setModel(getResultatsModel());
		resultatsTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		getResultatsModel().addTableModelListener(this);

		TableColumn column = null;
		for (int i = 0; i < resultatsTable.getColumnCount(); i++) {
		    column = resultatsTable.getColumnModel().getColumn(i);
		    if (i == 1) {
		        column.setMinWidth(30);
		        column.setMaxWidth(30);
		    } else if (i == 2) {
		    	column.setPreferredWidth(200);
		    	column.setMaxWidth(250);
		    }
		}

		JScrollPane listScroller = new JScrollPane(resultatsTable);
		resultatsPanel.add(listScroller);

		return resultatsPanel;
	}

	private JPanel buildActionsPanel()	{
		initActionButtons();

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));

		List<JButton> actions = new ArrayList<JButton>();
		actions.add(saveAsButton);
		actions.add(extractButton);
		actions.add(cancelButton);
		actions.add(extractButton);
		JPanel panelEastButtons = ZUiUtil.buildGridLine(actions);
		panelEastButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		panel.add(panelEastButtons, BorderLayout.EAST);

		return panel;
	}

	private void initActionButtons()	{
		extractButton = new JButton(extractAction);
		extractButton.setMinimumSize(new Dimension(100, 25));
		extractButton.setPreferredSize(new Dimension(100, 25));
		extractButton.setToolTipText("Générer");

		cancelButton = new JButton(cancelAction);
		cancelButton.setMinimumSize(new Dimension(100, 25));
		cancelButton.setPreferredSize(new Dimension(100, 25));
		cancelButton.setToolTipText("Fermer");

		saveAsButton = new JButton(saveAsAction);
		saveAsButton.setMinimumSize(new Dimension(150, 25));
		saveAsButton.setPreferredSize(new Dimension(150, 25));
		saveAsButton.setToolTipText("Sauvegarder");
		saveAsButton.setEnabled(false);
	}

	private void enregistrerFichiers() {
		File dir = null;
		if (controller.homeDir() != null) {
			dir = new File(controller.homeDir());
		}

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setDialogTitle("Sélectionnez le répertoire de destination ...");
		fileChooser.setCurrentDirectory(dir);
		if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			controller.onSaveAs(fileChooser.getSelectedFile());
		}
	}

	public CofisupExtractionModel getExtractionModel() {
		return extractionModel;
	}

	public CofisupResultatsModel getResultatsModel() {
		return resultatsModel;
	}

	/**
	 * Action declenchant les extractions.
	 */
	final class ActionExtract extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public ActionExtract() {
			super("Générer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SAVE);
		}

		public void actionPerformed(ActionEvent e) {
			controller.onExtract();
		}
	}

	/**
	 * Action declenchant la sauvegarde des fichiers.
	 */
	final class ActionSaveAs extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public ActionSaveAs() {
			super("Sauvegarder...");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
		}

		public void actionPerformed(ActionEvent e) {
			enregistrerFichiers();
		}
	}

	/**
	 * Action fermant la fenetre actuelle, annulant ainsi le traitement.
	 */
	final class ActionCancel extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public ActionCancel() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
		}

		public void actionPerformed(ActionEvent e) {
        	dispose();
        	controller.onCancel();
		}
	}

	/**
	 *
	 */
	static class CofisupCheckBoxCellRenderer extends JCheckBox implements TableCellRenderer, UIResource {
	    /** Serial version Id. */
		private static final long serialVersionUID = 1L;

		private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

		public CofisupCheckBoxCellRenderer() {
		    super();
		    setHorizontalAlignment(JLabel.CENTER);
	            setBorderPainted(true);
		}

        public Component getTableCellRendererComponent(JTable table, Object value,
        		boolean isSelected, boolean hasFocus, int row, int column) {

        	setEnabled(table.isCellEditable(row, column));
		    if (isSelected) {
		        setForeground(table.getSelectionForeground());
		        super.setBackground(table.getSelectionBackground());
		    } else {
		        setForeground(table.getForeground());
		        setBackground(table.getBackground());
		    }

            setSelected((value != null && ((Boolean) value).booleanValue()));

            if (hasFocus) {
                setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                setBorder(NO_FOCUS_BORDER);
            }

            return this;
        }
	}

	/**
	 * Listener des cases a cocher "Tous / Aucun".
	 */
	class CheckboxHeaderItemListener implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			Object source = e.getSource();
			if (!(source instanceof AbstractButton)) {
				return;
			}

			CheckBoxHeader sourceCBox = (CheckBoxHeader) source;
			boolean checked = e.getStateChange() == ItemEvent.SELECTED;
			int column = sourceCBox.getColumn();
			for (int x = 0, y = selectionTable.getRowCount(); x < y; x++) {
				selectionTable.setValueAt(checked, x, column);
			}
		}
	}

	/**
	 * Surcharge les checkbox en ajoutant un index de la colonne de la table.
	 */
	static class CheckBoxHeader extends JCheckBox implements MouseListener {
		private static final long serialVersionUID = 1L;

		private int column;
		private boolean mousePressed = false;

		public int getColumn() {
			return column;
		}

		public void setColumn(int column) {
			this.column = column;
		}

		protected void handleClickEvent(MouseEvent e) {
			if (mousePressed) {
				mousePressed = false;
				JTableHeader header = (JTableHeader) (e.getSource());
				JTable tableView = header.getTable();
				TableColumnModel columnModel = tableView.getColumnModel();
				int viewColumn = columnModel.getColumnIndexAtX(e.getX());
				int localColumn = tableView.convertColumnIndexToModel(viewColumn);

				if (viewColumn == this.column && e.getClickCount() == 1
						&& localColumn != -1) {
					doClick();
				}
			}
		}

		public void mouseClicked(MouseEvent e) {
			handleClickEvent(e);
			((JTableHeader) e.getSource()).repaint();
		}

		public void mousePressed(MouseEvent e) {
			mousePressed = true;
		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}
	}

	/**
	 *	Gere l'affichage de l'entete de la table.
	 */
	static class CheckBoxHeaderRenderer extends JPanel implements TableCellRenderer {
		private static final long serialVersionUID = 1L;

		private CheckBoxHeader checkBox = null;

		public CheckBoxHeaderRenderer(ItemListener itemListener) {
			super(new BorderLayout());
			checkBox = new CheckBoxHeader();
			checkBox.addItemListener(itemListener);
			checkBox.setHorizontalTextPosition(SwingConstants.LEFT);
			add(checkBox);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {

			String columnTitle = (String) value;
			if (table != null) {
				JTableHeader header = table.getTableHeader();
				if (header != null) {
					setForeground(header.getForeground());
					setBackground(header.getBackground());
					setFont(header.getFont());
					Color back = header.getBackground();
					setOpaque(back != null && back.equals(table.getBackground()));
					header.addMouseListener(checkBox);
				}
			}

			checkBox.setText(columnTitle);
			checkBox.setColumn(column);

			Border border = null;
			if (hasFocus) {
				if (isSelected) {
					border = UIManager.getBorder("Table.focusSelectedCellHighlightBorder");
				}
				if (border == null) {
					border = UIManager.getBorder("Table.focusCellHighlightBorder");
				}
			} else {
				border = UIManager.getBorder("TableHeader.cellBorder");
			}

			setBorder(border);
			return this;
		}

		public CheckBoxHeader getCheckBox() {
			return checkBox;
		}

		public void setCheckBox(CheckBoxHeader checkBox) {
			this.checkBox = checkBox;
		}
	}
}
