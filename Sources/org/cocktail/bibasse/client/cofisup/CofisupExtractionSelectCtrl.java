package org.cocktail.bibasse.client.cofisup;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.Superviseur;
import org.cocktail.bibasse.client.finder.FinderExercice;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.zutil.ZFileUtil;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.ui.ZWaitingPanelDialog;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Controlleur de l'interface de generation des fichiers Cofisup.
 * @author flagoueyte
 */
public final class CofisupExtractionSelectCtrl {

	private static final String MSG_VEUILLEZ_PATIENTER = "Veuillez patienter...";

	private static CofisupExtractionSelectCtrl sharedInstance;

	/**
	 * Singleton instance.
	 * @param ec editingContext.
	 * @return controller singleton instance.
	 */
	public static CofisupExtractionSelectCtrl instance(EOEditingContext ec) {
		if (sharedInstance == null) {
			sharedInstance = new CofisupExtractionSelectCtrl(ec);
		}
		return sharedInstance;
	}

	private EOEditingContext editingContext;
	private CofisupExtractionSelectDialog view;

	private CofisupResultatsModel modelResultsFiles;
	private CofisupExtractionModel modelTableSelection;
	private List<CofisupExtractionBean> rceExtractBean;
	private List<CofisupExtractionBean> nonRceExtractBean;

	/**
	 * Private constructor.
	 * @param ec editingContext.
	 */
	private CofisupExtractionSelectCtrl(EOEditingContext ec) {
		this.editingContext = ec;
		this.rceExtractBean = FactoryCofisupExtractionModel.modeleRce();
		this.nonRceExtractBean = FactoryCofisupExtractionModel.modeleNonRce();
		this.modelResultsFiles = new CofisupResultatsModel();
		this.modelTableSelection = new CofisupExtractionModel();
		this.view = new CofisupExtractionSelectDialog(superviseur().mainFrame(), modelTableSelection, modelResultsFiles, this);
	}

	/**
	 * Ouvre la fenetre de generation pour les etablissements RCE.
	 */
	public void openRce()	{
		open(rceExtractBean);
	}

	/**
	 * Ouvre la fenetre de generation pour les etablissements non soumis RCE.
	 */
	public void openNonRce() {
		open(nonRceExtractBean);
	}

	/**
	 * Action réalisée lors de la fermeture de la fenêtre.
	 */
	public void onCancel() {
	}

	/**
	 * Action réalisée lors d'une demande d'extraction.
	 */
	public void onExtract() {
		modelResultsFiles.clear();
		genererFichiers();
	}

	/**
	 * Enregistre l'ensemble des fichiers générés dans le répertoire sélectionné.
	 * @param destinationDir répertoire destination.
	 */
	public void onSaveAs(File destinationDir) {
		try {
			Iterator<CofisupResultatBean> iteResultats = modelResultsFiles.iterator();
			while (iteResultats.hasNext()) {
				CofisupResultatBean element = iteResultats.next();
				if (element.isSuccess()) {
					File file = element.getResultFile();
					ZFileUtil.fileCopy(file, new File(destinationDir, file.getName()));
				}
			}
			EODialogs.runInformationDialog("Informations",
					"Les fichiers ont été enregistrés dans le répertoire " + destinationDir.getAbsolutePath());
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", e.getMessage());
		}
	}

	/**
	 * Répertoire Home du user.
	 * @return le home de l'utilisateur connecté.
	 */
	public String homeDir() {
		return appClient().getHomeDir();
	}

	private void genererFichiers() {
		ZWaitingPanelDialog waitingDialog = new ZWaitingPanelDialog(null, view, ConstantesCocktail.ICON_VALID);
		waitingDialog.setTitle(MSG_VEUILLEZ_PATIENTER);
		waitingDialog.setTopText(MSG_VEUILLEZ_PATIENTER);
		waitingDialog.setBottomText("Génération des fichiers en cours");
		waitingDialog.setModal(true);

		EOExercice exerciceCourant = appClient().getExerciceBudgetaire();
		EOExercice exercicePrecedent = FinderExercice.exercicePrecedent(getEditingContext(), exerciceCourant);

		CofisupTask monitor = new CofisupTask(getEditingContext(), appClient().temporaryDir,
				modelResultsFiles, modelTableSelection.data(), exerciceCourant, exercicePrecedent, waitingDialog);
		monitor.execute();
		waitingDialog.setVisible(true);
	}

	private void open(List<CofisupExtractionBean> data) {
		modelResultsFiles.clear();
		modelTableSelection.populate(data);
		view.resetHeaders();
		ZUiUtil.centerWindow(view);
		view.setVisible(true);
	}

	private ApplicationClient appClient() {
		return (ApplicationClient) ApplicationClient.sharedApplication();
	}

	private Superviseur superviseur() {
		return appClient().superviseur();
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}
}
