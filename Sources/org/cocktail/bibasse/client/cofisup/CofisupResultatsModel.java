package org.cocktail.bibasse.client.cofisup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import org.cocktail.bibasse.client.ConstantesCocktail;

/**
 * Table Cofisup pour afficher les résultats d'extraction.
 */
public class CofisupResultatsModel extends AbstractTableModel {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	/** Nombre de colonne. */
	private static final int NB_COLS = 3;

	/** Donnees. */
	private List<CofisupResultatBean> data;

	/**
	 * Constructeur par défaut.
	 */
	public CofisupResultatsModel() {
		this.data = new ArrayList<CofisupResultatBean>();
	}

	public int getRowCount() {
		return data.size();
	}

	public int getColumnCount() {
		return NB_COLS;
	}

	public void clear() {
		this.data.clear();
    	fireTableDataChanged();
	}

	public void add(CofisupResultatBean newResult) {
		this.data.add(newResult);
    	fireTableDataChanged();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		CofisupResultatBean resultat = data.get(rowIndex);
		Object valueAt = null;
		switch (columnIndex) {
		case 0 :
			valueAt = resultat.getTitle();
			break;
		case 1 :
			valueAt = convertToImage(resultat.isSuccess());
			break;
		case 2 :
			valueAt = resultat.getStatusMessage();
			break;
		default :
		}

		return valueAt;
	}

	public Class getColumnClass(int c) {
		if (c == 1) {
			return ImageIcon.class;
		}
        return getValueAt(0, c).getClass();
    }

	public Iterator<CofisupResultatBean> iterator() {
		return this.data.iterator();
	}

	public List<CofisupResultatBean> getData() {
		return data;
	}

	private ImageIcon convertToImage(boolean isSuccess) {
		return isSuccess ? ConstantesCocktail.ICON_GLYPH_OK : ConstantesCocktail.ICON_GLYP_BAN;
	}
}
