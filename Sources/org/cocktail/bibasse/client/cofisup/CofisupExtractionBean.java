package org.cocktail.bibasse.client.cofisup;

import java.io.Serializable;

import org.cocktail.common.cofisup.ETypeNatureBudget;
import org.cocktail.common.cofisup.ETypeEtablissement;

public class CofisupExtractionBean implements Serializable {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	private ETypeEtablissement typeEtablissement;
	private ETypeNatureBudget nature;
	private String label;
	private Boolean rdBp;
	private Boolean rdBm;
	private Boolean rdEb;
	private Boolean icBp;
	private Boolean icBm;
	private Boolean icEb;

	public CofisupExtractionBean(ETypeEtablissement typeEtablissement, ETypeNatureBudget nature, String label) {
		this.typeEtablissement = typeEtablissement;
		this.nature = nature;
		this.label = label;
		init();
	}

	public void reset() {
		init();
	}

	private void init() {
		this.rdBp = Boolean.FALSE;
		this.rdBm = Boolean.FALSE;
		this.rdEb = Boolean.FALSE;
		this.icBp = Boolean.FALSE;
		this.icBm = Boolean.FALSE;
		this.icEb = Boolean.FALSE;
	}

	public ETypeEtablissement getTypeEtablissement() {
		return typeEtablissement;
	}

	public void setTypeEtablissement(ETypeEtablissement typeEtablissement) {
		this.typeEtablissement = typeEtablissement;
	}

	public ETypeNatureBudget getNature() {
		return nature;
	}
	public void setNature(ETypeNatureBudget nature) {
		this.nature = nature;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Boolean isRdBp() {
		return rdBp;
	}
	public void setRdBp(Boolean rdBp) {
		this.rdBp = rdBp;
	}
	public Boolean isRdBm() {
		return rdBm;
	}
	public void setRdBm(Boolean rdBm) {
		this.rdBm = rdBm;
	}
	public Boolean isRdEb() {
		return rdEb;
	}
	public void setRdEb(Boolean rdEb) {
		this.rdEb = rdEb;
	}
	public Boolean isIcBp() {
		return icBp;
	}
	public void setIcBp(Boolean icBp) {
		this.icBp = icBp;
	}
	public Boolean isIcBm() {
		return icBm;
	}
	public void setIcBm(Boolean icBm) {
		this.icBm = icBm;
	}
	public Boolean isIcEb() {
		return icEb;
	}
	public void setIcEb(Boolean icEb) {
		this.icEb = icEb;
	}
}
