package org.cocktail.bibasse.client.cofisup;

import static org.cocktail.common.cofisup.CofisupActionConstants.ACTION_START_JOB;
import static org.cocktail.common.cofisup.CofisupActionConstants.ACTION_INFOS_JOB;
import static org.cocktail.common.cofisup.CofisupActionConstants.ACTION_GET_FILE;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/**
 * Interfacage Client / Serveur spécifique à Cofisup.
 */
public class CofisupProxy {

	/**
	 * Singleton.
	 */
	private static final CofisupProxy INSTANCE = new CofisupProxy();

	/**
	 * @return singleton.
	 */
	public static CofisupProxy instance() {
		return INSTANCE;
	}


	public NSArray clientStartJob(EOEditingContext ec, NSDictionary parametres) {
		return (NSArray) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", ACTION_START_JOB,
				new Class[] { NSDictionary.class },	new Object[] { parametres }, false);
	}

	public NSDictionary clientInfosJob(EOEditingContext ec) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec,	"session", ACTION_INFOS_JOB, null, null, false);
	}

	public NSData clientRetrieveFile(EOEditingContext ec, String jobId) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", ACTION_GET_FILE,
				new Class[] { String.class }, new Object[] { jobId }, false);
	}
}
