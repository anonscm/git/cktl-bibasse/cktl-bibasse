package org.cocktail.bibasse.client.cofisup;

import static org.cocktail.common.cofisup.ETypeEtablissement.RCE;
import static org.cocktail.common.cofisup.ETypeEtablissement.NON_RCE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.AGREGE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.EPRD;
import static org.cocktail.common.cofisup.ETypeNatureBudget.EPRD_NATURE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.EPRD_GESTION;
import static org.cocktail.common.cofisup.ETypeNatureBudget.BPI;
import static org.cocktail.common.cofisup.ETypeNatureBudget.ETAB_GESTION;
import static org.cocktail.common.cofisup.ETypeNatureBudget.ETAB_NATURE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.PRINC_GESTION;
import static org.cocktail.common.cofisup.ETypeNatureBudget.PRINC_NATURE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.NON_EPRD;
import static org.cocktail.common.cofisup.ETypeNatureBudget.NON_EPRD_NATURE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.NON_EPRD_GESTION;
import static org.cocktail.common.cofisup.ETypeNatureBudget.PRINCIPAL;
import static org.cocktail.common.cofisup.ETypeNatureBudget.SIE;
import static org.cocktail.common.cofisup.ETypeNatureBudget.SIE_GESTION;
import static org.cocktail.common.cofisup.ETypeNatureBudget.SIE_NATURE;
import java.util.ArrayList;
import java.util.List;

public class FactoryCofisupExtractionModel {

	public static List<CofisupExtractionBean> modeleRce() {
		List<CofisupExtractionBean> modele = new ArrayList<CofisupExtractionBean>();

		modele.add(new CofisupExtractionBean(RCE, AGREGE, "Budget agrégé de l’établissement"));
		modele.add(new CofisupExtractionBean(RCE, PRINCIPAL, "Budget principal"));
		modele.add(new CofisupExtractionBean(RCE, NON_EPRD, "Budgets annexes non EPRD"));
		modele.add(new CofisupExtractionBean(RCE, EPRD, "Budgets annexes des EPRD"));
		modele.add(new CofisupExtractionBean(RCE, BPI, "Budgets propres intégrés du budget principal (IUT/ESPE)"));
		modele.add(new CofisupExtractionBean(RCE, SIE, "Budgets des services inter-établissement"));

		return modele;
	}

	public static List<CofisupExtractionBean> modeleNonRce() {
		List<CofisupExtractionBean> modele = new ArrayList<CofisupExtractionBean>();

		modele.add(new CofisupExtractionBean(NON_RCE, ETAB_NATURE,      "Budget de l’établissement par nature"));
		modele.add(new CofisupExtractionBean(NON_RCE, ETAB_GESTION,     "Budget de gestion de l’établissement"));
		modele.add(new CofisupExtractionBean(NON_RCE, PRINC_NATURE,     "Budget principal de l'établissement par nature"));
		modele.add(new CofisupExtractionBean(NON_RCE, PRINC_GESTION,    "Budget principal de l'établissement de gestion"));
		modele.add(new CofisupExtractionBean(NON_RCE, EPRD_NATURE,      "Budgets annexes EPRD par nature"));
		modele.add(new CofisupExtractionBean(NON_RCE, NON_EPRD_NATURE,  "Budgets annexes non EPRD par nature"));
		modele.add(new CofisupExtractionBean(NON_RCE, EPRD_GESTION,     "Budgets annexes EPRD de gestion"));
		modele.add(new CofisupExtractionBean(NON_RCE, NON_EPRD_GESTION, "Budgets annexes non EPRD de gestion"));
		modele.add(new CofisupExtractionBean(NON_RCE, SIE_NATURE,       "Budgets des services inter-établissement par nature"));
		modele.add(new CofisupExtractionBean(NON_RCE, SIE_GESTION,      "Budgets de gestion des services inter-établissement"));

		return modele;
	}
}
