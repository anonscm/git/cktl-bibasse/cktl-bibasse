package org.cocktail.bibasse.client.cofisup;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Modele de table Cofisup dédié à la sélection.
 */
public class CofisupExtractionModel extends AbstractTableModel {

    /** Serial version Id. */
	private static final long serialVersionUID = 1L;

	/** Nom des colonnes. */
	private static final String[] COLUMN_NAMES  = {
    		"Type fichier",
            "BP", "BM", "EB",
            "BP", "BM", "EB"
       };

	/** Données. */
    private List<CofisupExtractionBean> data;

    /**
     * Constructeur par défaut.
     */
    public CofisupExtractionModel() {
    	this(new ArrayList<CofisupExtractionBean>());
    }

    /**
     * Constructeur.
     * @param initData donnees d'initialisation.
     */
    public CofisupExtractionModel(List<CofisupExtractionBean> initData) {
    	this.data = initData;
    }

    /**
     * Remplir la table avec les donnees transmises.
     * @param newData donnees a afficher.
     */
    public final void populate(List<CofisupExtractionBean> newData) {
    	try {
    		if (newData == null) {
    			return;
    		}

    		this.data = newData;
    		reset();
    	} finally {
    		fireTableDataChanged();
    	}
    }

    /**
     * Réinitialise les donnees.
     */
    public final void reset() {
    	for (CofisupExtractionBean bean : data) {
    		bean.reset();
    	}
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public String getColumnName(int column) {
		return COLUMN_NAMES[column];
	}

	public int getRowCount() {
		return data.size();
	}

	public int getColumnCount() {
        return COLUMN_NAMES.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		CofisupExtractionBean bean = data.get(rowIndex);
		Object result = null;
		switch (columnIndex) {
		case 0 :
			result = bean.getLabel();
			break;
		case 1 :
			result = bean.isRdBp();
			break;
		case 2 :
			result = bean.isRdBm();
			break;
		case 3 :
			result = bean.isRdEb();
			break;
		case 4 :
			result = bean.isIcBp();
			break;
		case 5 :
			result = bean.isIcBm();
			break;
		case 6 :
			result = bean.isIcEb();
			break;
		default :
		}

		return result;
	}

    public void setValueAt(Object value, int row, int col) {
    	if (!isCellEditable(row, col)) {
    		return;
    	}
    	CofisupExtractionBean bean = data.get(row);
    	Boolean booleanValue = null;
    	if (col > 0) {
    		booleanValue = (Boolean) value;
    	}
		switch (col) {
	    case 1 :
			bean.setRdBp(booleanValue);
			break;
		case 2 :
			bean.setRdBm(booleanValue);
			break;
		case 3 :
			bean.setRdEb(booleanValue);
			break;
		case 4 :
			bean.setIcBp(booleanValue);
			break;
		case 5 :
			bean.setIcBm(booleanValue);
			break;
		case 6 :
			bean.setIcEb(booleanValue);
			break;
		default :
		}
        fireTableCellUpdated(row, col);
    }

	public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
    	CofisupExtractionBean bean = data.get(row);
    	boolean isGestion = bean.getNature().isGestion();
    	boolean isRce = bean.getTypeEtablissement().isRce();

    	return isRce || !isGestion || col <= 3;
    }

    /**
     * @return donnees.
     */
    public List<CofisupExtractionBean> data() {
    	return data;
    }
}
