package org.cocktail.bibasse.client;

import static org.cocktail.common.SessionConstants.KEY_ERRNO;
import static org.cocktail.common.SessionConstants.KEY_MSG;
import static org.cocktail.common.SessionConstants.KEY_OK_CODE;
import static org.cocktail.common.SessionConstants.KEY_PERS_ID;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.UnknownHostException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOUtilisateur;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.fwkcktlwebapp.common.CktlDockClient;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class ApplicationConnect {

	private static final String PARAM_SHOW_LOGIN_PANEL = "SHOW_LOGIN_PANEL";

	private static final String PARAM_SAUT_USER = "SAUT_USER";

	private static final String GLOBAL_PASSWORD = "wxc";

	private static ApplicationConnect sharedInstance;

	private ApplicationClient NSApp;
	private EOEditingContext ec;
	private EODistributedObjectStore objectStore;
	private EOUtilisateur currentUtilisateur;
	private String casUserName;
	private String sautUser;
	private String showLoginPanel;

	/**
	 * Constructeur.
	 */
	public ApplicationConnect(EOEditingContext editingContext) {
		super();

		ec = editingContext;
		objectStore = (EODistributedObjectStore)ec.parentObjectStore();
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		casUserName = getCASUserName(NSApp);
		sautUser = getParam(PARAM_SAUT_USER);
		showLoginPanel = getParam(PARAM_SHOW_LOGIN_PANEL);
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static ApplicationConnect sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null) {
			sharedInstance = new ApplicationConnect(editingContext);
		}
		return sharedInstance;
	}

	/**
	 *
	 * @return  PERSID de la personne connectee ou NULL si ERREUR
	 */
	public EOUtilisateur connect(String applicationName) {

    	if (casUserName == null)	{

    		// Si on ne demande pas le panel de connection, on se connecte avec le login System
    		if (showLoginPanel != null && !showLoginPanel.equals("1") && !showLoginPanel.equals("O") && !showLoginPanel.equals("YES") && !showLoginPanel.equals("OUI") ) {
           	 	testConnection(ec, System.getProperty("user.name"), GLOBAL_PASSWORD);
    		} else {

    			LoginDialog myLoginDialog = new LoginDialog(new JFrame(), applicationName + " - Identification");
    			myLoginDialog.setLogin(System.getProperty("user.name"));

    			boolean connectionOk = false;

    			// On laisse affiche le panel de connection tant que les controles de connection ne sont pas valides
    			while (!connectionOk)	{

    				myLoginDialog.open();

    				if (myLoginDialog.getModalResult() == 1) {
    					if (GLOBAL_PASSWORD.equals(myLoginDialog.getPassword())) {
    						sautUser = "NO";
    						connectionOk = testConnection(ec, myLoginDialog.getLogin(), GLOBAL_PASSWORD);
    					} else {
    						connectionOk = testConnection(ec, myLoginDialog.getLogin(), myLoginDialog.getPassword());
    					}
    				} else {
    					NSApp.quit();
    				}
    			}

    			ServerProxy.clientSideRequestSetLoginParametres(ec,  myLoginDialog.getLogin(),getIpAdress());
    		}

    	} else {
       	 	testConnection(ec, casUserName, GLOBAL_PASSWORD);
			ServerProxy.clientSideRequestSetLoginParametres(ec,  casUserName, getIpAdress());
    	}

		System.out.println(" ==> UTILISATEUR connecté : " + StringCtrl.capitalizedString(currentUtilisateur.individu().prenom()) + " " + currentUtilisateur.individu().nomUsuel());

		System.out.println("     - PersId : " + currentUtilisateur.persId()
				+ " , UtlOrdre : " + ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentUtilisateur).objectForKey("utlOrdre")
				+ " , NoIndividu : " + ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentUtilisateur.individu()).objectForKey("noIndividu"));

    	return currentUtilisateur;

    }

    /**
     *
     * @param login
     * @param password
     */
    private boolean testConnection(EOEditingContext ec, String login, String password)	{
    	System.out.println("ApplicationConnect.testConnection() ... Login : " + login);

    	if (StringCtrl.chaineVide(login) || StringCtrl.chaineVide(password))	{
    		EODialogs.runInformationDialog("ERREUR", "Le login ET le mot de passe doivent être renseignés !");
    		return false;
    	}

    	if (isCASUser(casUserName)) {
	    	NSDictionary connectDico = ServerProxy.clientSideRequestCheckPasswordCAS(ec, login);
	    	if (connectDico == null || !KEY_OK_CODE.equals(connectDico.objectForKey(KEY_ERRNO).toString())) {
				EODialogs.runInformationDialog("ERREUR", (String) connectDico.objectForKey(KEY_MSG));
				return false;
			}

			currentUtilisateur = findUtilisateurForPersId(ec, Integer.valueOf(connectDico.objectForKey(KEY_PERS_ID).toString()));
    	} else {
    		if (sautUser != null && isSAUTUser(sautUser))	{
    			NSDictionary connectDico = ServerProxy.clientSideRequestCheckPassword(ec, login, password);
    			if (connectDico == null || !KEY_OK_CODE.equals(connectDico.objectForKey(KEY_ERRNO).toString())) {
    				EODialogs.runInformationDialog("ERREUR", (String) connectDico.objectForKey(KEY_MSG));
    				return false;
    			}
    			currentUtilisateur = findUtilisateurForPersId(ec, new Integer(connectDico.objectForKey(KEY_PERS_ID).toString()));
    		} else {
    	    	System.out.println("ApplicationConnect.testConnection() ... SAUT non installé");
    	    	NSDictionary connectDico = ServerProxy.clientSideRequestCheckPasswordDirectConnect(ec, login, password);
    	    	if (connectDico == null || !KEY_OK_CODE.equals(connectDico.objectForKey(KEY_ERRNO).toString())) {
    				EODialogs.runInformationDialog("ERREUR", (String) connectDico.objectForKey(KEY_MSG));
    				return false;
    			}

    			currentUtilisateur = findUtilisateurForPersId(ec, Integer.valueOf(connectDico.objectForKey(KEY_PERS_ID).toString()));
    		}
    	}

    	// Verification des droits de l'utilisateur
		if (currentUtilisateur == null)	{
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits pour utiliser cette application !");
    		return false;
		}

		return true;
    }

    private boolean isCASUser(String casUserName) {
    	return casUserName != null;
    }

    private boolean isSAUTUser(String sautUser) {
    	return "YES".equals(sautUser) || "OUI".equals(sautUser) || "1".equals(sautUser);
    }

    /**
     *
     * @return
     */
	private final String getIpAdress() {
	       try {
	           final String s = java.net.InetAddress.getLocalHost().getHostAddress();
	           final String s2 = java.net.InetAddress.getLocalHost().getHostName() ;
	           return s +" / " + s2;
	       } catch (UnknownHostException e) {
	          return "Machine inconnue";
	       }
	   }

	/**
	 *
	 * @param application
	 * @return
	 */
	private String getCASUserName(EOApplication application) {
		NSDictionary arguments = application.arguments();
		String dockPort = (String) arguments.objectForKey("LRAppDockPort");
		String dockTicket = (String) arguments.objectForKey("LRAppDockTicket");
		String uname = null;
		if ((dockTicket != null) && (dockPort != null)) {
			uname = CktlDockClient.getNetID(null, dockPort, dockTicket);
		}
		return uname;
	}

	/**
	 *
	 * @param paramKey
	 * @return
	 */
	private String getParam(String paramKey) {
		return ServerProxy.clientSideRequestGetParam(ec, paramKey);
	}

/**
 *
 * @author cpinsard
 *
 */
	private class LoginDialog  {

		protected JDialog  mainFrame;

		public static final int MROK=1;
		public static final int MRCANCEL=0;

		protected int modalResult;

	    private JTextField identifiantField;
	    private JPasswordField pwdField;
	    private JButton btOk;

	    /**
	     * @param owner
	     * @param title
	     * @param modal
	     * @throws HeadlessException
	     */
	    public LoginDialog(Frame owner, String title) throws HeadlessException {

	        super();

			mainFrame=new JDialog(owner, title, true);
			mainFrame.setSize(630, 475);
			//mainFrame.setIconImage(ConstantesCocktail.ICON_APP_LOGO.getImage());

	        mainFrame.addWindowListener(new LocalWindowListener());

	        initGUI();
	    }


		public int  open()  {
			setModalResult(MRCANCEL);
			centerWindow();
			mainFrame.show();
			//en modal la fenetre reste active jusqu'au closeWindow...
			return getModalResult();
		}


		public final void centerWindow() {
			int screenWidth = (int)mainFrame.getGraphicsConfiguration().getBounds().getWidth();
			int screenHeight = (int)mainFrame.getGraphicsConfiguration().getBounds().getHeight();
			mainFrame.setLocation((screenWidth/2)-((int)mainFrame.getSize().getWidth()/2), ((screenHeight/2)-((int)mainFrame.getSize().getHeight()/2)));

		}

		private void initGUI() {


			JPanel myPanel = new JPanel(new BorderLayout());
			JPanel leftPanel = new JPanel(new BorderLayout());

			JLabel iconLabel = new JLabel((ImageIcon)new EOClientResourceBundle().getObject("login"));
			leftPanel.add(iconLabel);
			leftPanel.setPreferredSize(new Dimension(80,80));
			leftPanel.setMaximumSize(leftPanel.getPreferredSize());

			JPanel rightPanel = new JPanel(new BorderLayout());

			//Premiere ligne
			Box box1 = Box.createHorizontalBox();
			JLabel labelId = new JLabel("Identifiant");
			labelId.setPreferredSize(new Dimension(100,0));
			identifiantField = new JTextField();
			identifiantField.setColumns(25);
			identifiantField.setBackground(Color.WHITE);
			box1.setBorder(BorderFactory.createEmptyBorder(2,4,2,4));
			box1.add(labelId);
			box1.add(identifiantField);
			box1.add(new JPanel());

			Box box2 = Box.createHorizontalBox();
			JLabel labelPwd = new JLabel("Mot de passe");
			labelPwd.setPreferredSize(new Dimension(100,0));
			pwdField = new JPasswordField();
			pwdField.setColumns(25);
			pwdField.setBackground(Color.WHITE);
//			box2.add(Box.createRigidArea(new Dimension(1,15)));
			box2.setBorder(BorderFactory.createEmptyBorder(2,4,2,4));
			box2.add(labelPwd);
			box2.add(pwdField);
			box2.add(new JPanel());

			Box box = Box.createVerticalBox();
			box.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
			box.add(new JPanel());
			box.add(box1);
			box.add(box2);
			box.add(buildLeftBottomPanel());
			box.add(new JPanel());

			rightPanel.add(box);
//			rightPanel.add(new JPanel(), BorderLayout.CENTER);

			myPanel.add(leftPanel, BorderLayout.LINE_START);
			myPanel.add(rightPanel, BorderLayout.CENTER);
			myPanel.add(buildButtonsPanel(), BorderLayout.PAGE_END);

			mainFrame.setContentPane(myPanel );
			myPanel.getRootPane().setDefaultButton(btOk);
			mainFrame.pack();
		}

		private final JPanel buildLeftBottomPanel() {
	        ApplicationClient myApp = (ApplicationClient)EOApplication.sharedApplication();

		    JPanel p = new JPanel(new BorderLayout());
		    JLabel l = new JLabel(myApp.version() + " - " + myApp.getConnectionName());
		    l.setFont(l.getFont().deriveFont(Font.ITALIC));
		    l.setHorizontalAlignment(SwingConstants.LEFT);
		    p.add(l);
		    return p;
		}

		public void onOkClick() {
			setModalResult(MROK);
			mainFrame.hide();
		}


		public void onCancelClick() {
			setModalResult(MRCANCEL);
			mainFrame.hide();
		}

		public void onCloseClick() {
			setModalResult(MRCANCEL);
			mainFrame.hide();
		}


		/**
		 *
		 */
		public int getModalResult() {
			return modalResult;
		}


		/**
		 * @param i
		 */
		public void setModalResult(int i) {
			modalResult = i;
		}

		protected JPanel buildButtonsPanel() {
			Action actionOk = new AbstractAction("Ok") {
				public void actionPerformed(ActionEvent e) {
					onOkClick();
				}
			};
			actionOk.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);

			Action actionCancel = new AbstractAction("Annuler") {
				public void actionPerformed(ActionEvent e) {
					onCancelClick();
				}
			};
			actionCancel.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);

			btOk = new JButton(actionOk);
			JButton btCancel = new JButton(actionCancel);
			Dimension btSize = new Dimension(95,24);
			btOk.setMinimumSize(btSize);
			btCancel.setMinimumSize(btSize);
			btOk.setPreferredSize(btSize);
			btCancel.setPreferredSize(btSize);

			JPanel buttonPanel2 = new JPanel(new BorderLayout());
			buttonPanel2.add( new JSeparator(), BorderLayout.PAGE_START );

			JPanel buttonPanel = new JPanel();
			buttonPanel.add(btOk);
			buttonPanel.add(btCancel);
			buttonPanel.setBorder(BorderFactory.createEmptyBorder(8,0,8,0));

			buttonPanel2.add(buttonPanel, BorderLayout.CENTER);

			return buttonPanel2;
		}

	    /**
	     * @param errMsg The errMsg to set.
	     */
	    public void setErrMsg(String errMsg) {
//	        errMsgLabel.setText(errMsg);
	    }

	    public String getLogin() {
	        return identifiantField.getText();
	    }

	    public void setLogin(String login) {
	        identifiantField.setText(login);
	    }

	    public String getPassword() {
	        return new String(pwdField.getPassword());
	    }

	    public class LocalWindowListener implements WindowListener {
	        public void windowOpened(WindowEvent arg0) {
	        }

	        public void windowClosed(WindowEvent arg0) {
	        }

	        public void windowIconified(WindowEvent arg0) {
	        }

	        public void windowDeiconified(WindowEvent arg0) {
	        }

	        public void windowActivated(WindowEvent arg0) {
	            pwdField.setRequestFocusEnabled(true);
	            pwdField.requestFocus();
	        }

	        public void windowDeactivated(WindowEvent arg0) {
	        }

	        public void windowClosing(WindowEvent arg0) {
	        }
	    }
	}

	/**
	 *
	 * @param ec
	 * @param persId
	 * @return
	 */
		private static EOUtilisateur findUtilisateurForPersId(EOEditingContext ec, Number persId)	{
			try {
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOUtilisateur.PERS_ID_KEY + " = %@", new NSArray(persId)));
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOUtilisateur.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + " = %@", new NSArray(EOTypeEtat.ETAT_VALIDE)));

				EOFetchSpecification fs = new EOFetchSpecification(EOUtilisateur.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

				return (EOUtilisateur) ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

}
