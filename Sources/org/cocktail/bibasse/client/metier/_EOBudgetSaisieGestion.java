// _EOBudgetSaisieGestion.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetSaisieGestion.java instead.
package org.cocktail.bibasse.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBudgetSaisieGestion extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "BudgetSaisieGestion";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Saisie_Gestion";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdsgId";

	public static final String BDSG_MONTANT_KEY = "bdsgMontant";
	public static final String BDSG_SAISI_KEY = "bdsgSaisi";
	public static final String BDSG_VOTE_KEY = "bdsgVote";

// Attributs non visibles
	public static final String BDSA_ID_KEY = "bdsaId";
	public static final String BDSG_ID_KEY = "bdsgId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAC_ID_KEY = "tyacId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String BDSG_MONTANT_COLKEY = "BDSG_MONTANT";
	public static final String BDSG_SAISI_COLKEY = "BDSG_SAISI";
	public static final String BDSG_VOTE_COLKEY = "BDSG_VOTE";

	public static final String BDSA_ID_COLKEY = "BDSA_ID";
	public static final String BDSG_ID_COLKEY = "BDSG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String BUDGET_SAISIE_KEY = "budgetSaisie";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public java.math.BigDecimal bdsgMontant() {
    return (java.math.BigDecimal) storedValueForKey(BDSG_MONTANT_KEY);
  }

  public void setBdsgMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDSG_MONTANT_KEY);
  }

  public java.math.BigDecimal bdsgSaisi() {
    return (java.math.BigDecimal) storedValueForKey(BDSG_SAISI_KEY);
  }

  public void setBdsgSaisi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDSG_SAISI_KEY);
  }

  public java.math.BigDecimal bdsgVote() {
    return (java.math.BigDecimal) storedValueForKey(BDSG_VOTE_KEY);
  }

  public void setBdsgVote(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDSG_VOTE_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOBudgetSaisie budgetSaisie() {
    return (org.cocktail.bibasse.client.metier.EOBudgetSaisie)storedValueForKey(BUDGET_SAISIE_KEY);
  }

  public void setBudgetSaisieRelationship(org.cocktail.bibasse.client.metier.EOBudgetSaisie value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOBudgetSaisie oldValue = budgetSaisie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_SAISIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_SAISIE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOExercice exercice() {
    return (org.cocktail.bibasse.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.bibasse.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOOrgan organ() {
    return (org.cocktail.bibasse.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.bibasse.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeAction typeAction() {
    return (org.cocktail.bibasse.client.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.bibasse.client.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.bibasse.client.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.bibasse.client.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.bibasse.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.bibasse.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }


  public static EOBudgetSaisieGestion createBudgetSaisieGestion(EOEditingContext editingContext, java.math.BigDecimal bdsgMontant
, java.math.BigDecimal bdsgSaisi
, java.math.BigDecimal bdsgVote
, org.cocktail.bibasse.client.metier.EOBudgetSaisie budgetSaisie, org.cocktail.bibasse.client.metier.EOExercice exercice, org.cocktail.bibasse.client.metier.EOOrgan organ, org.cocktail.bibasse.client.metier.EOTypeAction typeAction, org.cocktail.bibasse.client.metier.EOTypeCredit typeCredit, org.cocktail.bibasse.client.metier.EOTypeEtat typeEtat) {
    EOBudgetSaisieGestion eo = (EOBudgetSaisieGestion) createAndInsertInstance(editingContext, _EOBudgetSaisieGestion.ENTITY_NAME);
		eo.setBdsgMontant(bdsgMontant);
		eo.setBdsgSaisi(bdsgSaisi);
		eo.setBdsgVote(bdsgVote);
    eo.setBudgetSaisieRelationship(budgetSaisie);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeCreditRelationship(typeCredit);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBudgetSaisieGestion.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBudgetSaisieGestion.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOBudgetSaisieGestion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetSaisieGestion)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOBudgetSaisieGestion localInstanceIn(EOEditingContext editingContext, EOBudgetSaisieGestion eo) {
    EOBudgetSaisieGestion localInstance = (eo == null) ? null : (EOBudgetSaisieGestion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBudgetSaisieGestion#localInstanceIn a la place.
   */
	public static EOBudgetSaisieGestion localInstanceOf(EOEditingContext editingContext, EOBudgetSaisieGestion eo) {
		return EOBudgetSaisieGestion.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOBudgetSaisieGestion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBudgetSaisieGestion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetSaisieGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetSaisieGestion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetSaisieGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetSaisieGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetSaisieGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetSaisieGestion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃ©.
	   */
	  public static EOBudgetSaisieGestion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetSaisieGestion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetSaisieGestion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetSaisieGestion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}