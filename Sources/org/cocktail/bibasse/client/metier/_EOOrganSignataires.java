// _EOOrganSignataires.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrganSignataires.java instead.
package org.cocktail.bibasse.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrganSignataires extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "OrganSignataires";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.Organ_Signataire";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "orsiId";

	public static final String ORSI_DATE_CLOTURE_KEY = "orsiDateCloture";
	public static final String ORSI_DATE_OUVERTRURE_KEY = "orsiDateOuvertrure";
	public static final String ORSI_LIBELLE_SIGNATAIRE_KEY = "orsiLibelleSignataire";

// Attributs non visibles
	public static final String ORSI_ID_KEY = "orsiId";
	public static final String TYSI_ID_KEY = "tysiId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ORSI_DATE_CLOTURE_COLKEY = "ORSI_DATE_CLOTURE";
	public static final String ORSI_DATE_OUVERTRURE_COLKEY = "ORSI_DATE_OUVERTURE";
	public static final String ORSI_LIBELLE_SIGNATAIRE_COLKEY = "ORSI_LIBELLE_SIGNATAIRE";

	public static final String ORSI_ID_COLKEY = "ORSI_ID";
	public static final String TYSI_ID_COLKEY = "TYSI_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TYPE_SIGNATURES_KEY = "typeSignatures";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp orsiDateCloture() {
    return (NSTimestamp) storedValueForKey(ORSI_DATE_CLOTURE_KEY);
  }

  public void setOrsiDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, ORSI_DATE_CLOTURE_KEY);
  }

  public NSTimestamp orsiDateOuvertrure() {
    return (NSTimestamp) storedValueForKey(ORSI_DATE_OUVERTRURE_KEY);
  }

  public void setOrsiDateOuvertrure(NSTimestamp value) {
    takeStoredValueForKey(value, ORSI_DATE_OUVERTRURE_KEY);
  }

  public String orsiLibelleSignataire() {
    return (String) storedValueForKey(ORSI_LIBELLE_SIGNATAIRE_KEY);
  }

  public void setOrsiLibelleSignataire(String value) {
    takeStoredValueForKey(value, ORSI_LIBELLE_SIGNATAIRE_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOTypeSignatures typeSignatures() {
    return (org.cocktail.bibasse.client.metier.EOTypeSignatures)storedValueForKey(TYPE_SIGNATURES_KEY);
  }

  public void setTypeSignaturesRelationship(org.cocktail.bibasse.client.metier.EOTypeSignatures value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeSignatures oldValue = typeSignatures();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SIGNATURES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SIGNATURES_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.bibasse.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.bibasse.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }


  public static EOOrganSignataires createOrganSignataires(EOEditingContext editingContext, String orsiLibelleSignataire
, org.cocktail.bibasse.client.metier.EOTypeSignatures typeSignatures, org.cocktail.bibasse.client.metier.EOUtilisateur utilisateur) {
    EOOrganSignataires eo = (EOOrganSignataires) createAndInsertInstance(editingContext, _EOOrganSignataires.ENTITY_NAME);
		eo.setOrsiLibelleSignataire(orsiLibelleSignataire);
    eo.setTypeSignaturesRelationship(typeSignatures);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOOrganSignataires.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOOrganSignataires.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOOrganSignataires localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrganSignataires)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOOrganSignataires localInstanceIn(EOEditingContext editingContext, EOOrganSignataires eo) {
    EOOrganSignataires localInstance = (eo == null) ? null : (EOOrganSignataires)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOOrganSignataires#localInstanceIn a la place.
   */
	public static EOOrganSignataires localInstanceOf(EOEditingContext editingContext, EOOrganSignataires eo) {
		return EOOrganSignataires.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOOrganSignataires fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOOrganSignataires fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrganSignataires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrganSignataires)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOOrganSignataires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOOrganSignataires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrganSignataires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrganSignataires)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃ©.
	   */
	  public static EOOrganSignataires fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrganSignataires eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrganSignataires ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOOrganSignataires fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}