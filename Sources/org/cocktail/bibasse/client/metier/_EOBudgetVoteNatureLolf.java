// _EOBudgetVoteNatureLolf.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetVoteNatureLolf.java instead.
package org.cocktail.bibasse.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBudgetVoteNatureLolf extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "BudgetVoteNatureLolf";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Vote_Nature_Lolf";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdvlId";

	public static final String BDVL_CONVENTIONS_KEY = "bdvlConventions";
	public static final String BDVL_CREDITS_KEY = "bdvlCredits";
	public static final String BDVL_DBMS_KEY = "bdvlDbms";
	public static final String BDVL_DEBITS_KEY = "bdvlDebits";
	public static final String BDVL_OUVERTS_KEY = "bdvlOuverts";
	public static final String BDVL_PRIMITIFS_KEY = "bdvlPrimitifs";
	public static final String BDVL_PROVISOIRES_KEY = "bdvlProvisoires";
	public static final String BDVL_RELIQUATS_KEY = "bdvlReliquats";
	public static final String BDVL_VOTES_KEY = "bdvlVotes";

// Attributs non visibles
	public static final String BDVL_ID_KEY = "bdvlId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAC_ID_KEY = "tyacId";

//Colonnes dans la base de donnees
	public static final String BDVL_CONVENTIONS_COLKEY = "BDVL_CONVENTIONS";
	public static final String BDVL_CREDITS_COLKEY = "BDVL_CREDITS";
	public static final String BDVL_DBMS_COLKEY = "BDVL_DBMS";
	public static final String BDVL_DEBITS_COLKEY = "BDVL_DEBITS";
	public static final String BDVL_OUVERTS_COLKEY = "BDVL_OUVERTS";
	public static final String BDVL_PRIMITIFS_COLKEY = "BDVL_PRIMITIFS";
	public static final String BDVL_PROVISOIRES_COLKEY = "BDVL_PROVISOIRES";
	public static final String BDVL_RELIQUATS_COLKEY = "BDVL_RELIQUATS";
	public static final String BDVL_VOTES_COLKEY = "BDVL_VOTES";

	public static final String BDVL_ID_COLKEY = "BDVL_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal bdvlConventions() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_CONVENTIONS_KEY);
  }

  public void setBdvlConventions(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_CONVENTIONS_KEY);
  }

  public java.math.BigDecimal bdvlCredits() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_CREDITS_KEY);
  }

  public void setBdvlCredits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_CREDITS_KEY);
  }

  public java.math.BigDecimal bdvlDbms() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_DBMS_KEY);
  }

  public void setBdvlDbms(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_DBMS_KEY);
  }

  public java.math.BigDecimal bdvlDebits() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_DEBITS_KEY);
  }

  public void setBdvlDebits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_DEBITS_KEY);
  }

  public java.math.BigDecimal bdvlOuverts() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_OUVERTS_KEY);
  }

  public void setBdvlOuverts(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_OUVERTS_KEY);
  }

  public java.math.BigDecimal bdvlPrimitifs() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_PRIMITIFS_KEY);
  }

  public void setBdvlPrimitifs(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_PRIMITIFS_KEY);
  }

  public java.math.BigDecimal bdvlProvisoires() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_PROVISOIRES_KEY);
  }

  public void setBdvlProvisoires(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_PROVISOIRES_KEY);
  }

  public java.math.BigDecimal bdvlReliquats() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_RELIQUATS_KEY);
  }

  public void setBdvlReliquats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_RELIQUATS_KEY);
  }

  public java.math.BigDecimal bdvlVotes() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_VOTES_KEY);
  }

  public void setBdvlVotes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_VOTES_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOExercice exercice() {
    return (org.cocktail.bibasse.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.bibasse.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOOrgan organ() {
    return (org.cocktail.bibasse.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.bibasse.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.bibasse.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.bibasse.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeAction typeAction() {
    return (org.cocktail.bibasse.client.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }

  public void setTypeActionRelationship(org.cocktail.bibasse.client.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.bibasse.client.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.bibasse.client.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }


  public static EOBudgetVoteNatureLolf createBudgetVoteNatureLolf(EOEditingContext editingContext, java.math.BigDecimal bdvlConventions
, java.math.BigDecimal bdvlCredits
, java.math.BigDecimal bdvlDbms
, java.math.BigDecimal bdvlDebits
, java.math.BigDecimal bdvlOuverts
, java.math.BigDecimal bdvlPrimitifs
, java.math.BigDecimal bdvlProvisoires
, java.math.BigDecimal bdvlReliquats
, java.math.BigDecimal bdvlVotes
, org.cocktail.bibasse.client.metier.EOExercice exercice, org.cocktail.bibasse.client.metier.EOOrgan organ, org.cocktail.bibasse.client.metier.EOPlanComptable planComptable, org.cocktail.bibasse.client.metier.EOTypeAction typeAction, org.cocktail.bibasse.client.metier.EOTypeCredit typeCredit) {
    EOBudgetVoteNatureLolf eo = (EOBudgetVoteNatureLolf) createAndInsertInstance(editingContext, _EOBudgetVoteNatureLolf.ENTITY_NAME);
		eo.setBdvlConventions(bdvlConventions);
		eo.setBdvlCredits(bdvlCredits);
		eo.setBdvlDbms(bdvlDbms);
		eo.setBdvlDebits(bdvlDebits);
		eo.setBdvlOuverts(bdvlOuverts);
		eo.setBdvlPrimitifs(bdvlPrimitifs);
		eo.setBdvlProvisoires(bdvlProvisoires);
		eo.setBdvlReliquats(bdvlReliquats);
		eo.setBdvlVotes(bdvlVotes);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBudgetVoteNatureLolf.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBudgetVoteNatureLolf.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOBudgetVoteNatureLolf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetVoteNatureLolf)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOBudgetVoteNatureLolf localInstanceIn(EOEditingContext editingContext, EOBudgetVoteNatureLolf eo) {
    EOBudgetVoteNatureLolf localInstance = (eo == null) ? null : (EOBudgetVoteNatureLolf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBudgetVoteNatureLolf#localInstanceIn a la place.
   */
	public static EOBudgetVoteNatureLolf localInstanceOf(EOEditingContext editingContext, EOBudgetVoteNatureLolf eo) {
		return EOBudgetVoteNatureLolf.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOBudgetVoteNatureLolf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBudgetVoteNatureLolf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetVoteNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetVoteNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetVoteNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetVoteNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetVoteNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetVoteNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃ©.
	   */
	  public static EOBudgetVoteNatureLolf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetVoteNatureLolf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetVoteNatureLolf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetVoteNatureLolf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}