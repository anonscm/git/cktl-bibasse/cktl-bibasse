// _EOBudgetVoteNature.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetVoteNature.java instead.
package org.cocktail.bibasse.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBudgetVoteNature extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "BudgetVoteNature";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Vote_Nature";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdvnId";

	public static final String BDVN_CONVENTIONS_KEY = "bdvnConventions";
	public static final String BDVN_CREDITS_KEY = "bdvnCredits";
	public static final String BDVN_DBMS_KEY = "bdvnDbms";
	public static final String BDVN_DEBITS_KEY = "bdvnDebits";
	public static final String BDVN_OUVERTS_KEY = "bdvnOuverts";
	public static final String BDVN_PRIMITIFS_KEY = "bdvnPrimitifs";
	public static final String BDVN_PROVISOIRES_KEY = "bdvnProvisoires";
	public static final String BDVN_RELIQUATS_KEY = "bdvnReliquats";
	public static final String BDVN_VOTES_KEY = "bdvnVotes";

// Attributs non visibles
	public static final String BDVN_ID_KEY = "bdvnId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String BDVN_CONVENTIONS_COLKEY = "BDVN_CONVENTIONS";
	public static final String BDVN_CREDITS_COLKEY = "BDVN_CREDITS";
	public static final String BDVN_DBMS_COLKEY = "BDVN_DBMS";
	public static final String BDVN_DEBITS_COLKEY = "BDVN_DEBITS";
	public static final String BDVN_OUVERTS_COLKEY = "BDVN_OUVERTS";
	public static final String BDVN_PRIMITIFS_COLKEY = "BDVN_PRIMITIFS";
	public static final String BDVN_PROVISOIRES_COLKEY = "BDVN_PROVISOIRES";
	public static final String BDVN_RELIQUATS_COLKEY = "BDVN_RELIQUATS";
	public static final String BDVN_VOTES_COLKEY = "BDVN_VOTES";

	public static final String BDVN_ID_COLKEY = "BDVN_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal bdvnConventions() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_CONVENTIONS_KEY);
  }

  public void setBdvnConventions(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_CONVENTIONS_KEY);
  }

  public java.math.BigDecimal bdvnCredits() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_CREDITS_KEY);
  }

  public void setBdvnCredits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_CREDITS_KEY);
  }

  public java.math.BigDecimal bdvnDbms() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_DBMS_KEY);
  }

  public void setBdvnDbms(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_DBMS_KEY);
  }

  public java.math.BigDecimal bdvnDebits() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_DEBITS_KEY);
  }

  public void setBdvnDebits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_DEBITS_KEY);
  }

  public java.math.BigDecimal bdvnOuverts() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_OUVERTS_KEY);
  }

  public void setBdvnOuverts(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_OUVERTS_KEY);
  }

  public java.math.BigDecimal bdvnPrimitifs() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_PRIMITIFS_KEY);
  }

  public void setBdvnPrimitifs(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_PRIMITIFS_KEY);
  }

  public java.math.BigDecimal bdvnProvisoires() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_PROVISOIRES_KEY);
  }

  public void setBdvnProvisoires(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_PROVISOIRES_KEY);
  }

  public java.math.BigDecimal bdvnReliquats() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_RELIQUATS_KEY);
  }

  public void setBdvnReliquats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_RELIQUATS_KEY);
  }

  public java.math.BigDecimal bdvnVotes() {
    return (java.math.BigDecimal) storedValueForKey(BDVN_VOTES_KEY);
  }

  public void setBdvnVotes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVN_VOTES_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOExercice exercice() {
    return (org.cocktail.bibasse.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.bibasse.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOOrgan organ() {
    return (org.cocktail.bibasse.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.bibasse.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.bibasse.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.bibasse.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.bibasse.client.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.bibasse.client.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }


  public static EOBudgetVoteNature createBudgetVoteNature(EOEditingContext editingContext, java.math.BigDecimal bdvnConventions
, java.math.BigDecimal bdvnCredits
, java.math.BigDecimal bdvnDbms
, java.math.BigDecimal bdvnDebits
, java.math.BigDecimal bdvnOuverts
, java.math.BigDecimal bdvnPrimitifs
, java.math.BigDecimal bdvnProvisoires
, java.math.BigDecimal bdvnReliquats
, java.math.BigDecimal bdvnVotes
, org.cocktail.bibasse.client.metier.EOExercice exercice, org.cocktail.bibasse.client.metier.EOOrgan organ, org.cocktail.bibasse.client.metier.EOPlanComptable planComptable, org.cocktail.bibasse.client.metier.EOTypeCredit typeCredit) {
    EOBudgetVoteNature eo = (EOBudgetVoteNature) createAndInsertInstance(editingContext, _EOBudgetVoteNature.ENTITY_NAME);
		eo.setBdvnConventions(bdvnConventions);
		eo.setBdvnCredits(bdvnCredits);
		eo.setBdvnDbms(bdvnDbms);
		eo.setBdvnDebits(bdvnDebits);
		eo.setBdvnOuverts(bdvnOuverts);
		eo.setBdvnPrimitifs(bdvnPrimitifs);
		eo.setBdvnProvisoires(bdvnProvisoires);
		eo.setBdvnReliquats(bdvnReliquats);
		eo.setBdvnVotes(bdvnVotes);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBudgetVoteNature.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBudgetVoteNature.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOBudgetVoteNature localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetVoteNature)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOBudgetVoteNature localInstanceIn(EOEditingContext editingContext, EOBudgetVoteNature eo) {
    EOBudgetVoteNature localInstance = (eo == null) ? null : (EOBudgetVoteNature)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBudgetVoteNature#localInstanceIn a la place.
   */
	public static EOBudgetVoteNature localInstanceOf(EOEditingContext editingContext, EOBudgetVoteNature eo) {
		return EOBudgetVoteNature.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOBudgetVoteNature fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBudgetVoteNature fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetVoteNature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetVoteNature)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetVoteNature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetVoteNature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetVoteNature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetVoteNature)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃ©.
	   */
	  public static EOBudgetVoteNature fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetVoteNature eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetVoteNature ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetVoteNature fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}