

// EOBudgetMouvGestion.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetMouvGestion extends _EOBudgetMouvGestion {

    public EOBudgetMouvGestion() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
    public void addBudgetMouvements(EOBudgetMouvGestion eog,EOBudgetMouvements eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "mouvement");
    }
    
    
    public void addTypeSens(EOBudgetMouvGestion eog,EOTypeSens eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeSens");
    }
    
    public void addExercice(EOBudgetMouvGestion eog,EOExercice eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");

    }
    
    public void addTypeCredit(EOBudgetMouvGestion eog,EOTypeCredit eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeCredit");

    }
    
    
    public void addTypeAction(EOBudgetMouvGestion eog,EOTypeAction eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeAction");

    }
    public void addOrgan(EOBudgetMouvGestion eog,EOOrgan eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "organ");

    }


}
