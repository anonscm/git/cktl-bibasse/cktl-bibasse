

// EOBudgetMouvements.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetMouvements extends _EOBudgetMouvements {

    public static  final String ERROR_MESS_ORGAN_CONVENTION="TOUTES LES LIGNES DE CETTE SAISIE DOIVENT ETRE DE LA MEME CONVENTION";
    public static  final String ERROR_MESS_TYPE_CONVENTION="CETTE SAISIE DOIT ETRE DE TYPE CONVENTION";
    public static  final String ERROR_MESS_TYPE_HORS_CONVENTION="CETTE SAISIE NE DOIT PAS ETRE DE TYPE CONVENTION";

    public EOBudgetMouvements() {
        super();
    }

    public String toString()	{
    	
    	return  "MOUV : " + typeMouvementBudgetaire().tymbLibelle();
    	
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
      
    public void addUtilisateur(EOBudgetMouvements eog,EOUtilisateur eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "utilisateur");
    }
    
    public void addTypeMouvementBudgetaire(EOBudgetMouvements eog,EOTypeMouvementBudgetaire eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeMouvementBudgetaire");
    }
    
    public void addExercice(EOBudgetMouvements eog,EOExercice eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");
    }
    
    public void addTypeEtat(EOBudgetMouvements eog,EOTypeEtat eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeEtat");
    }

}
