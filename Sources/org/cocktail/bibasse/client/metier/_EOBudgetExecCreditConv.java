// _EOBudgetExecCreditConv.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetExecCreditConv.java instead.
package org.cocktail.bibasse.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBudgetExecCreditConv extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "BudgetExecCreditConv";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.BUDGET_EXEC_CREDIT_CONV";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "beccId";

	public static final String BECC_CONVENTIONS_KEY = "beccConventions";
	public static final String BECC_CREDITS_KEY = "beccCredits";
	public static final String BECC_DBMS_KEY = "beccDbms";
	public static final String BECC_DEBITS_KEY = "beccDebits";
	public static final String BECC_DISPONIBLE_KEY = "beccDisponible";
	public static final String BECC_ENGAGEMENTS_KEY = "beccEngagements";
	public static final String BECC_MANDATS_KEY = "beccMandats";
	public static final String BECC_OUVERTS_KEY = "beccOuverts";
	public static final String BECC_PRIMITIFS_KEY = "beccPrimitifs";
	public static final String BECC_PROVISOIRES_KEY = "beccProvisoires";
	public static final String BECC_RELIQUATS_KEY = "beccReliquats";
	public static final String BECC_RESERVE_KEY = "beccReserve";
	public static final String BECC_VOTES_KEY = "beccVotes";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

// Attributs non visibles
	public static final String BECC_ID_KEY = "beccId";

//Colonnes dans la base de donnees
	public static final String BECC_CONVENTIONS_COLKEY = "BECC_CONVENTIONS";
	public static final String BECC_CREDITS_COLKEY = "BECC_CREDITS";
	public static final String BECC_DBMS_COLKEY = "BECC_DBMS";
	public static final String BECC_DEBITS_COLKEY = "BECC_DEBITS";
	public static final String BECC_DISPONIBLE_COLKEY = "BECC_DISPONIBLE";
	public static final String BECC_ENGAGEMENTS_COLKEY = "BECC_ENGAGEMENTS";
	public static final String BECC_MANDATS_COLKEY = "BECC_MANDATS";
	public static final String BECC_OUVERTS_COLKEY = "BECC_OUVERTS";
	public static final String BECC_PRIMITIFS_COLKEY = "BECC_PRIMITIFS";
	public static final String BECC_PROVISOIRES_COLKEY = "BECC_PROVISOIRES";
	public static final String BECC_RELIQUATS_COLKEY = "BECC_RELIQUATS";
	public static final String BECC_RESERVE_COLKEY = "BECC_RESERVE";
	public static final String BECC_VOTES_COLKEY = "BECC_VOTES";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";

	public static final String BECC_ID_COLKEY = "BECC_ID";


	// Relationships



	// Accessors methods
  public java.math.BigDecimal beccConventions() {
    return (java.math.BigDecimal) storedValueForKey(BECC_CONVENTIONS_KEY);
  }

  public void setBeccConventions(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_CONVENTIONS_KEY);
  }

  public java.math.BigDecimal beccCredits() {
    return (java.math.BigDecimal) storedValueForKey(BECC_CREDITS_KEY);
  }

  public void setBeccCredits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_CREDITS_KEY);
  }

  public java.math.BigDecimal beccDbms() {
    return (java.math.BigDecimal) storedValueForKey(BECC_DBMS_KEY);
  }

  public void setBeccDbms(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_DBMS_KEY);
  }

  public java.math.BigDecimal beccDebits() {
    return (java.math.BigDecimal) storedValueForKey(BECC_DEBITS_KEY);
  }

  public void setBeccDebits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_DEBITS_KEY);
  }

  public java.math.BigDecimal beccDisponible() {
    return (java.math.BigDecimal) storedValueForKey(BECC_DISPONIBLE_KEY);
  }

  public void setBeccDisponible(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_DISPONIBLE_KEY);
  }

  public java.math.BigDecimal beccEngagements() {
    return (java.math.BigDecimal) storedValueForKey(BECC_ENGAGEMENTS_KEY);
  }

  public void setBeccEngagements(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_ENGAGEMENTS_KEY);
  }

  public java.math.BigDecimal beccMandats() {
    return (java.math.BigDecimal) storedValueForKey(BECC_MANDATS_KEY);
  }

  public void setBeccMandats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_MANDATS_KEY);
  }

  public java.math.BigDecimal beccOuverts() {
    return (java.math.BigDecimal) storedValueForKey(BECC_OUVERTS_KEY);
  }

  public void setBeccOuverts(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_OUVERTS_KEY);
  }

  public java.math.BigDecimal beccPrimitifs() {
    return (java.math.BigDecimal) storedValueForKey(BECC_PRIMITIFS_KEY);
  }

  public void setBeccPrimitifs(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_PRIMITIFS_KEY);
  }

  public java.math.BigDecimal beccProvisoires() {
    return (java.math.BigDecimal) storedValueForKey(BECC_PROVISOIRES_KEY);
  }

  public void setBeccProvisoires(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_PROVISOIRES_KEY);
  }

  public java.math.BigDecimal beccReliquats() {
    return (java.math.BigDecimal) storedValueForKey(BECC_RELIQUATS_KEY);
  }

  public void setBeccReliquats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_RELIQUATS_KEY);
  }

  public java.math.BigDecimal beccReserve() {
    return (java.math.BigDecimal) storedValueForKey(BECC_RESERVE_KEY);
  }

  public void setBeccReserve(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_RESERVE_KEY);
  }

  public java.math.BigDecimal beccVotes() {
    return (java.math.BigDecimal) storedValueForKey(BECC_VOTES_KEY);
  }

  public void setBeccVotes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BECC_VOTES_KEY);
  }

  public Double exeOrdre() {
    return (Double) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Double value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Double orgId() {
    return (Double) storedValueForKey(ORG_ID_KEY);
  }

  public void setOrgId(Double value) {
    takeStoredValueForKey(value, ORG_ID_KEY);
  }

  public Double tcdOrdre() {
    return (Double) storedValueForKey(TCD_ORDRE_KEY);
  }

  public void setTcdOrdre(Double value) {
    takeStoredValueForKey(value, TCD_ORDRE_KEY);
  }


  public static EOBudgetExecCreditConv createBudgetExecCreditConv(EOEditingContext editingContext, java.math.BigDecimal beccConventions
, java.math.BigDecimal beccCredits
, java.math.BigDecimal beccDbms
, java.math.BigDecimal beccDebits
, java.math.BigDecimal beccDisponible
, java.math.BigDecimal beccEngagements
, java.math.BigDecimal beccMandats
, java.math.BigDecimal beccOuverts
, java.math.BigDecimal beccPrimitifs
, java.math.BigDecimal beccProvisoires
, java.math.BigDecimal beccReliquats
, java.math.BigDecimal beccVotes
, Double exeOrdre
, Double orgId
, Double tcdOrdre
) {
    EOBudgetExecCreditConv eo = (EOBudgetExecCreditConv) createAndInsertInstance(editingContext, _EOBudgetExecCreditConv.ENTITY_NAME);
		eo.setBeccConventions(beccConventions);
		eo.setBeccCredits(beccCredits);
		eo.setBeccDbms(beccDbms);
		eo.setBeccDebits(beccDebits);
		eo.setBeccDisponible(beccDisponible);
		eo.setBeccEngagements(beccEngagements);
		eo.setBeccMandats(beccMandats);
		eo.setBeccOuverts(beccOuverts);
		eo.setBeccPrimitifs(beccPrimitifs);
		eo.setBeccProvisoires(beccProvisoires);
		eo.setBeccReliquats(beccReliquats);
		eo.setBeccVotes(beccVotes);
		eo.setExeOrdre(exeOrdre);
		eo.setOrgId(orgId);
		eo.setTcdOrdre(tcdOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBudgetExecCreditConv.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBudgetExecCreditConv.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOBudgetExecCreditConv localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetExecCreditConv)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOBudgetExecCreditConv localInstanceIn(EOEditingContext editingContext, EOBudgetExecCreditConv eo) {
    EOBudgetExecCreditConv localInstance = (eo == null) ? null : (EOBudgetExecCreditConv)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBudgetExecCreditConv#localInstanceIn a la place.
   */
	public static EOBudgetExecCreditConv localInstanceOf(EOEditingContext editingContext, EOBudgetExecCreditConv eo) {
		return EOBudgetExecCreditConv.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOBudgetExecCreditConv fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBudgetExecCreditConv fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetExecCreditConv eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetExecCreditConv)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetExecCreditConv fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetExecCreditConv fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetExecCreditConv eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetExecCreditConv)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃ©.
	   */
	  public static EOBudgetExecCreditConv fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetExecCreditConv eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetExecCreditConv ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetExecCreditConv fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}