

//EOOrgan.java

package org.cocktail.bibasse.client.metier;

import java.util.HashMap;
import java.util.Map;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSValidation;

public class EOOrgan extends _EOOrgan {

	public static  final String ERROR_MESS_NIVEAU_SAISIE="LA SAISIE EST AUTORISEE POUR LES UB , CR ET CONVENTIONS";

	public static final Integer ORG_NIV_0 = new Integer(0);
	public static final Integer ORG_NIV_1 = new Integer(1);
	public static final Integer ORG_NIV_2 = new Integer(2);
	public static final Integer ORG_NIV_3 = new Integer(3);
	public static final Integer ORG_NIV_4 = new Integer(4);

	public static final int ORG_NIV_MAX = EOOrgan.ORG_NIV_3.intValue();    
	public static NSValidation.ValidationException EXCEPTION_DELETE_ORGAN_A_ENFANTS = new NSValidation.ValidationException("Impossible de supprimer une ligne budgétaire qui a des enfants.");
	public static NSValidation.ValidationException EXCEPTION_DELETE_ORGAN_A_UTILISATEURS = new NSValidation.ValidationException("Impossible de supprimer une ligne budgétaire qui a des utilisateurs affectés. Supprimez les autilisateurs affectés à cette ligne pour pouvoir la supprimer.");

	public static final String LONG_STRING_KEY  = "longString";

	public static final EOSortOrdering SORT_ORG_UNIV_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UNIV_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_ETAB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_ETAB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_UB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_CR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_CR_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_SOUSCR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending);


	public static final String ORG_NIV_0_LIB = "UNIVERSITE";
	public static final String ORG_NIV_1_LIB = "ETABLISSEMENT";
	public static final String ORG_NIV_2_LIB = "UB";
	public static final String ORG_NIV_3_LIB = "CR";
	public static final String ORG_NIV_4_LIB = "SOUS CR";


	public static final Object ERREUR_STRUCTURE_VIDE = "Une structure de l'annuaire devrait être associée.";
	public static final String ERREUR_SIGNATAIRE_PRINCIPAL="Un signataire principal valide est nécessaire au niveau le plus haut de l'organigramme budgétaire. Ce signataire principal doit être l'unique signataire.";
//	public static final String ERREUR_SIGNATAIRE_UB="Un signataire SECONDAIRE ou DE DROIT est nécessaire au niveau UB de l'organigramme.";

	/** Libellés des niveaux (par niveau)*/
	public static final Map NIV_LIB_MAP = new HashMap();

	static {
		NIV_LIB_MAP.put(ORG_NIV_0, ORG_NIV_0_LIB);
		NIV_LIB_MAP.put(ORG_NIV_1, ORG_NIV_1_LIB);
		NIV_LIB_MAP.put(ORG_NIV_2, ORG_NIV_2_LIB);
		NIV_LIB_MAP.put(ORG_NIV_3, ORG_NIV_3_LIB);
		NIV_LIB_MAP.put(ORG_NIV_4, ORG_NIV_4_LIB);
	};

	public EOOrgan() {
		super();
	}


	/**
	 * 
	 */
	public String toString()	{

		switch (orgNiveau().intValue())	{

			case 0 : return orgUniv();
			case 1 : return orgEtab();
			case 2 : return orgUb();
			case 3 : return orgCr();
			case 4 : return orgSouscr();

		}

		return "????";

	}


	/**
	 * Renvoie selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
	 * @param s
	 */
	public final String getShortString() {
		final int niv = orgNiveau().intValue();
		String res = null;
		switch (niv) {
			case 0 :  
				res = orgUniv();
				break;

			case 1 :  
				res = orgEtab();
				break;

			case 2 :  
				res = orgUb();
				break;

			case 3 :  
				res = orgCr();
				break;

			case 4 :  
				res = orgSouscr();
				break;

			default:
				break;
		}
		return res;
	}



	public final String getLongString() {
//		String tmp =  orgUniv();
		String tmp =  orgEtab();

//		if (orgEtab()!=null )
//		tmp = tmp + " / " + orgEtab();

		if (orgUb()!=null )
			tmp = tmp + " / " + orgUb();

		if (orgCr()!=null) {
			tmp = tmp + " / "+orgCr();
		}

		return tmp;
	}



	/**
	 * 
	 * @return
	 */
	public String libelleLong() {
		if (orgUniv() == null)
			return "ERREUR LIGNE";
		if (orgEtab() == null)
			return orgUniv();
		if (orgUb() == null)
			return orgEtab();
		if (orgCr() == null)
			return orgEtab()+" / "+orgUb();
		return orgEtab()+" / "+orgUb() + " / " + orgCr()+" / ";
	}

	public String libelleExtraLong() {
		if (orgUniv() == null)
			return "ERREUR LIGNE";

		if (orgEtab() == null)
			return orgUniv()+" : "+orgLibelle();
		if (orgUb() == null)
			return orgEtab()+" : "+orgLibelle();
		if (orgCr() == null)
			return orgEtab()+" / "+orgUb()+" : "+orgLibelle();
		return orgEtab()+" / "+orgUb() + " / " + orgCr()+" / "+" : "+orgLibelle();
	}


	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {


	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}
