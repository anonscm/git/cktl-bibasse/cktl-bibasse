

// EOTypeOrgan.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeOrgan extends _EOTypeOrgan {

    public static final String TYPE_LIGNE_BUDGETAIRE="LIGNE BUDGETAIRE";
    public static final String TYPE_CONVENTION="CONVENTION R.A.";

    public EOTypeOrgan() {
        super();
    }

    public boolean isConvention()	{
    	return TYPE_CONVENTION.equals(tyorLibelle());
    }
    

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
