

// EOBudgetMouvNature.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetMouvNature extends _EOBudgetMouvNature {

    public EOBudgetMouvNature() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
    public void addBudgetMouvements(EOBudgetMouvNature eog,EOBudgetMouvements eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "mouvement");
    }
    
    
    public void addTypeSens(EOBudgetMouvNature eog,EOTypeSens eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeSens");
    }
    
    public void addExercice(EOBudgetMouvNature eog,EOExercice eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");

    }
    
    public void addTypeCredit(EOBudgetMouvNature eog,EOTypeCredit eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeCredit");

    }
    
    
    public void addPlanComptable(EOBudgetMouvNature eog,EOPlanComptable eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "planComptable");

    }
    public void addOrgan(EOBudgetMouvNature eog,EOOrgan eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "organ");

    }


}
