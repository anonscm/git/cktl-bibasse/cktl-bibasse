

// EOBudgetSaisieGestion.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetSaisieGestion extends _EOBudgetSaisieGestion {

	
    // Liste des etats 
    public final static String ETAT_EN_COURS="EN COURS";
    public final static String ETAT_FERME_SAISIE="FERME";
    public final static String ETAT_CLOTURE="CLOTURE";

    
    
    public EOBudgetSaisieGestion() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
    public void addTypeEtat(EOBudgetSaisieGestion eog,EOTypeEtat eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeEtat");
    }
    
    public void addTypeCredit(EOBudgetSaisieGestion eog,EOTypeCredit eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeCredit");
    }
    public void addTypeAction(EOBudgetSaisieGestion eog,EOTypeAction eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeAction");
    }
    public void addOrgan(EOBudgetSaisieGestion eog,EOOrgan eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "organ");
    }
    public void addExercice(EOBudgetSaisieGestion eog,EOExercice eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");
    }
    public void addBudgetSaisie(EOBudgetSaisieGestion eog,EOBudgetSaisie eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "budgetSaisie");
    }


}
