

// EOTypeCredit.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeCredit extends _EOTypeCredit {

	
    public static  final String TYPE_DEPENSE="DEPENSE";

    public static  final String TYPE_RECETTE="RECETTE";

    public static  final String TYPE_BUDGET_RESERVE = "RESERVE";

    
    
    public EOTypeCredit() {
        super();
    }

    public boolean isDepense()	{
    	return tcdType().equals(TYPE_DEPENSE);
    }

    public boolean isRecette()	{
    	return tcdType().equals(TYPE_RECETTE);
    }

    /**
     * 
     */
    public String toString()	{
    
    	return tcdCode()+ " - " + tcdLibelle();
    	
    }

    

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
