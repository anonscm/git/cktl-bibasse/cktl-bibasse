

// EOPlanComptable.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOPlanComptable extends _EOPlanComptable {

    public static  final String TYPE_RECETTE ="R";
    public static  final String TYPE_DEPENSE ="D";
    
    
    public static  final String ETAT_VALIDE ="VALIDE";
    public static  final String ERROR_MESS_ETAT_VALIDE="CE COMPTE DIMPUTATION N EST PAS VALIDE";
    
    public static  final String ETAT_BUDGETAIRE ="O";
    public static  final String ERROR_MESS_ETAT_BUDGETAIRE="CE COMPTE DIMPUTATION N EST PAS BUDGETAIRE";
    
    
    public EOPlanComptable() {
        super();
    }

    public String toString() {
    	return pcoNum()+"-"+pcoLibelle();
    }
    
    public String validite() {
    	
    	if ("ANNULE".equals(pcoValidite()))
    		return "A";
    	
    	return "V";
    	
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
