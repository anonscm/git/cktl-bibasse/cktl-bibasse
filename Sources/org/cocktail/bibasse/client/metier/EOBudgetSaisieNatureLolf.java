

// EOBudgetSaisieNatureLolf.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetSaisieNatureLolf extends _EOBudgetSaisieNatureLolf {

    public EOBudgetSaisieNatureLolf() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    public void addTypeEtat(EOBudgetSaisieNatureLolf eog,EOTypeEtat eo) {
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.TYPE_ETAT_KEY);
    }
    
    public void addTypeCredit(EOBudgetSaisieNatureLolf eog,EOTypeCredit eo) {
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.TYPE_CREDIT_KEY);
    }
    
    public void addPlanComptable(EOBudgetSaisieNatureLolf eog,EOPlanComptable eo){
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY);
    }
    public void addTypeAction(EOBudgetSaisieNatureLolf eog,EOTypeAction eo){
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY);
    }
    
    public void addOrgan(EOBudgetSaisieNatureLolf eog,EOOrgan eo){
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.ORGAN_KEY);
    }
    
    public void addExercice(EOBudgetSaisieNatureLolf eog,EOExercice eo){
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.EXERCICE_KEY);
    }
    
    public void addBudgetSaisie(EOBudgetSaisieNatureLolf eog,EOBudgetSaisie eo){
        eog.addObjectToBothSidesOfRelationshipWithKey(eo,EOBudgetSaisieNatureLolf.BUDGET_SAISIE_KEY);
    }

}
