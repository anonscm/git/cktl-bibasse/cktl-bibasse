

// EOTypeSaisie.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeSaisie extends _EOTypeSaisie {

    // saisie de type convention : uniquement des lignes budgetaires representant des conventions.
    public final static String SAISIE_CONVENTION="CONVENTION";
    public static final String ERROR_MESS_SAISIE_CONVENTION="SAISIE DOIT ETRE BUDGET CONVENTION";
    
    public final static String SAISIE_PROVISOIRE="PROVISOIRE";
    public static final String ERROR_MESS_SAISIE_PROVISOIRE="SAISIE DOIT ETRE BUDGET PROVISOIRE";
    
    public final static String SAISIE_INITIAL="INITIAL";
    public static final String ERROR_MESS_SAISIE_INITIAL="SAISIE DOIT ETRE BUDGET INITIAL";
  
    public final static String SAISIE_RELIQUAT="RELIQUAT";
    public static final String ERROR_MESS_SAISIE_RELIQUAT="SAISIE DOIT ETRE BUDGET RELIQUAT";
    
    public final static String SAISIE_DBM="DBM";
    public static final String ERROR_MESS_SAISIE_DBM="SAISIE DOIT ETRE BUDGET DBM";
   
    public final static String SAISIE_MIO="MIO";
    public static final String ERROR_MESS_SAISIE_MIO="SAISIE DOIT ETRE BUDGET MIO";

    public EOTypeSaisie() {
        super();
    }
    
    public String toString()	{
    	return tysaLibelle();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
