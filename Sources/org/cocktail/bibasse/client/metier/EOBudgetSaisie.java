

// EOBudgetSaisie.java
// 
package org.cocktail.bibasse.client.metier;


import java.math.BigDecimal;

import org.cocktail.bibasse.client.finder.FinderBudgetSaisie;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieGestion;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisieNature;
import org.cocktail.bibasse.client.finder.FinderTypeCredit;
import org.cocktail.bibasse.client.utils.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOBudgetSaisie extends _EOBudgetSaisie {

    public static  final String ENTITY_NAME="BudgetSaisie";
    
    public static  final String ERROR_MESS_ORGAN_CONVENTION="TOUTES LES LIGNES DE CETTE SAISIE DOIVENT ETRE DE LA MEME CONVENTION";
    public static  final String ERROR_MESS_TYPE_CONVENTION="CETTE SAISIE DOIT ETRE DE TYPE CONVENTION";
    public static  final String ERROR_MESS_TYPE_HORS_CONVENTION="CETTE SAISIE NE DOIT PAS ETRE DE TYPE CONVENTION";
       
    public EOBudgetSaisie() {
        super();
    }

    
    /**
     * 
     */
    public String toString()	{
    	
    	return bdsaLibelle();
    	
    }
    
    
    /**
     * Le budget en cours est il VALIDE ?
     * @return
     */
    public boolean isAnnule()	{
    	return typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_ANNULE);
    }

    
    /**
     * Le budget en cours est il VALIDE ?
     * @return
     */
    public boolean isValide()	{
    	return typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE);
    }

    /**
     * Le budget en cours est il VOTE ?
     * @return
     */
    public boolean isVote()	{
    	return typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VOTE);
    }

    /**
     * Le budget en cours est il VOTE ?
     * @return
     */
    public boolean isCloture()	{
    	return typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_CLOT);
    }
    
    /**
     * Le budget est il un budget PROVISOIRE ?
     * @return
     */
    public boolean isBudgetProvisoire()	{
    	return typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_PROVISOIRE);
    }

    /**
     * Le budget est il un budget CONVENTION ?
     * @return
     */
    public boolean isBudgetConvention()	{
    	return typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_CONVENTION);
    }
    
    /**
     * Le budget est il un budget INITIAL ?
     * @return
     */
    public boolean isBudgetInitial()	{
    	return typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_INITIAL);
    }
    
    /**
     * Le budget est il un budget INITIAL ?
     * @return
     */
    public boolean isBudgetReliquat()	{
    	return typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_RELIQUAT);
    }
    
    /**
     * Le budget est il un budget INITIAL ?
     * @return
     */
    public boolean isBudgetDbm()	{
    	return typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_DBM);
    }

    
    
    /**
     * 
     *
     */
    public boolean  canVote(EOEditingContext ec) throws ValidationException	{
    	
//		NSArray budgetsGestion = FinderBudgetSaisieGestion.findBudgetsGestion(ec, this, null, this.exercice());
//
//		if (budgetsGestion.count() == 0)	{
//			throw new ValidationException("Aucun Budget n'a été saisi !");			
//		}
//
//		System.out.println("EOBudgetSaisie.canVote() : " + budgetsGestion.count());
//
//		for (int i=0;i<budgetsGestion.count();i++)	{
//			
//			EOBudgetSaisieGestion budget = (EOBudgetSaisieGestion)budgetsGestion.objectAtIndex(i);
//			
//			if (!budget.typeEtat().isCloture())	{
//				System.out.println("Budget KO : " + budget.organ().getLongString());
//				System.out.println("TYPE ETAT KO : " + budget.typeEtat());
//				throw new ValidationException("Toutes les saisies n'ont pas été cloturées.");
//			}
//			
//		}
//		
    	
    	return true;
    }
    

    /**
     * 
     * @param ec
     * @param exercice
     * @return
     */
    public static boolean saisieBudgetaireEnCours(EOEditingContext ec, EOExercice exercice)	{

		NSArray budgets = FinderBudgetSaisie.findBudgetsSaisieForExercice(ec ,exercice, null);
		
		if (budgets.count() > 0)
			return true;
		
//		// Si un budget initial est ouvert avec des saisies, on ne peut plus modifier le masque de saisie
//		for (int i=0;i<budgets.count();i++){
//			
//			EOBudgetSaisie budget = (EOBudgetSaisie)budgets.objectAtIndex(i);
//			
//			if (budget.isBudgetInitial())	{
//			
//				NSArray budgetsGestion = FinderBudgetSaisieGestion.findBudgetsGestion(ec, budget, null, exercice);
//				
//				if (budgetsGestion.count() > 0)	{
//					return true;
//				}				
//			}
//		}
//		
		return false;

    }
    
    

    /**
     * 
     * @return
     */
    public boolean isBudgetValide(EOEditingContext ec, EOOrgan organ, EOExercice exercice) throws ValidationException	{
    	
    	// Le montant global saisi ne peut etre égal à 0 !!!
		NSArray budgetsGestion = FinderBudgetSaisieGestion.findBudgetsGestion(ec, this, organ, exercice);
		BigDecimal totalGestion = computeSumForKey(budgetsGestion, "bdsgMontant");
		BigDecimal totalGestionVote = computeSumForKey(budgetsGestion, "bdsgVote");

    	if (totalGestion.floatValue() == 0 && totalGestionVote.floatValue() == 0)
			throw new ValidationException(
					"BUDGET SAISI INVALIDE !\n\nLe budget de GESTION est vide !");
		
		NSArray budgetsNature = FinderBudgetSaisieNature.findBudgetsNature(ec, this, organ, exercice);    	
		BigDecimal totalNature = computeSumForKey(budgetsNature, "bdsnMontant");
		BigDecimal totalNatureVote = computeSumForKey(budgetsNature, "bdsnVote");
    	if (totalNature.floatValue() == 0 && totalNatureVote.floatValue() == 0)
			throw new ValidationException(
					"BUDGET SAISI INVALIDE !\n\nLe budget par NATURE est vide !");
		
		
    	// On verifie que le montant du budget de gestion et nature en depense soit identique par type de credit
    	
    	// Recuperation des differents types de credit.
		NSArray typesCredit = FinderTypeCredit.findTypesCredit(ec, exercice, null, "DEPENSE");

		// Pour chaque type de credit on controle le budget de gestion et le budget par nature
    	for (int i=0;i<typesCredit.count();i++)	{
    		
    		EOTypeCredit typeCredit = (EOTypeCredit)typesCredit.objectAtIndex(i);
    		
    		// Recuperation du budget de gestion
    		NSArray gestions = FinderBudgetSaisieGestion.findBudgetsGestion(ec, this, organ, typeCredit, exercice);
    		
    		NSArray natures = FinderBudgetSaisieNature.findBudgetsNature(ec, this, organ, typeCredit, exercice);

    		BigDecimal cumulGestion = computeSumForKey(gestions, "bdsgMontant");
    		BigDecimal cumulNature = computeSumForKey(natures, "bdsnMontant");
    	
    		if (cumulGestion.floatValue() != cumulNature.floatValue())	{
    			throw new ValidationException(
    					"BUDGET SAISI INVALIDE !\n\nPour le type de credit " + typeCredit.tcdCode()+
    					", le budget de GESTION (" + cumulGestion.toString()
    					+" \u20ac) doit être égal au budget par NATURE ("+cumulNature.toString()+" \u20ac).");
    		}
    		
    	}
    	
    	return true;
    }
    
    
    

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      
    	
    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    
    public void addUtilisateurValidation(EOBudgetSaisie eog,EOUtilisateur eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "utilisateurValidation");
    }
    
    public void addUtilisateur(EOBudgetSaisie eog,EOUtilisateur eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "utilisateur");
    }
    
    public void addTypeSaisie(EOBudgetSaisie eog,EOTypeSaisie eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeSaisie");
    }
    
    public void addTypeEtat(EOBudgetSaisie eog,EOTypeEtat eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeEtat");
    }
    
    public void addExercice(EOBudgetSaisie eog,EOExercice eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");
    }
    
 
    
    
    public void verifierTypeConvention() throws Exception
    {

        // si le type de saisie est different de convention
        if ( ! this.typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_CONVENTION)) 
            throw new Exception("verifierTypeConvention : "+EOBudgetSaisie.ERROR_MESS_TYPE_CONVENTION);
             
    }
    
    public void verifierTypeHorsConvention() throws Exception	{
    	
        // si le type de saisie est   convention
        if (  this.typeSaisie().tysaLibelle().equals(EOTypeSaisie.SAISIE_CONVENTION)) 
            throw new Exception("verifierTypeHorsConvention : "+EOBudgetSaisie.ERROR_MESS_TYPE_HORS_CONVENTION);
             
    }
    

    /**
     * 
     * @throws Exception
     */
    public void verifierOrganConvention() throws Exception    {
        boolean probleme = false;
        
        this.verifierTypeConvention();
        
        // organ identiques dans BudgetSaisieGestion
        
        // organ identiques dans BudgetSaisieNature
        
        if ( probleme )
            throw new Exception("VerifierOrganconvention : "+EOBudgetSaisie.ERROR_MESS_ORGAN_CONVENTION);
        
    }


	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
		public BigDecimal computeSumForKey(NSArray myArray, String key) {
			
			BigDecimal total = new BigDecimal(0.0);

			int i = 0;
			while (i < myArray.count()) {
				try {
					if (NSDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
							|| NSMutableDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
					)	{
						NSDictionary dico = (NSDictionary) myArray.objectAtIndex(i);
						
						if (String.class.getName().equals(dico.objectForKey(key).getClass().getName()))
							total = total.add(new BigDecimal(StringCtrl.replace((String)dico.objectForKey(key), ",", ".")));
						else
							if (BigDecimal.class.getName().equals(dico.objectForKey(key).getClass().getName()))
								total = total.add((BigDecimal)dico.objectForKey(key));
					}
					else	{
						total = total.add((BigDecimal) ((EOEnterpriseObject) myArray.objectAtIndex(i)).valueForKey(key));
					}
				}
				catch (Exception e) {
					e.printStackTrace();
					total = new BigDecimal(0.0);
					break;
				}
				i = i + 1;
			}
			
			return total.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
}
