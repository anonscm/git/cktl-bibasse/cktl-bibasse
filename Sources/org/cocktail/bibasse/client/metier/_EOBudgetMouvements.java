// _EOBudgetMouvements.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetMouvements.java instead.
package org.cocktail.bibasse.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBudgetMouvements extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "BudgetMouvements";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Mouvements";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bmouId";

	public static final String BMOU_DATE_CREATION_KEY = "bmouDateCreation";
	public static final String BMOU_LIBELLE_KEY = "bmouLibelle";

// Attributs non visibles
	public static final String BMOU_ID_KEY = "bmouId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYMB_ID_KEY = "tymbId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String BMOU_DATE_CREATION_COLKEY = "BMOU_DATE_CREATION";
	public static final String BMOU_LIBELLE_COLKEY = "BMOU_LIBELLE";

	public static final String BMOU_ID_COLKEY = "BMOU_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYMB_ID_COLKEY = "TYMB_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String BUDGET_MOUVEMENTS_CREDITS_KEY = "budgetMouvementsCredits";
	public static final String BUDGET_MOUVEMENTS_GESTIONS_KEY = "budgetMouvementsGestions";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_MOUVEMENT_BUDGETAIRE_KEY = "typeMouvementBudgetaire";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp bmouDateCreation() {
    return (NSTimestamp) storedValueForKey(BMOU_DATE_CREATION_KEY);
  }

  public void setBmouDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, BMOU_DATE_CREATION_KEY);
  }

  public String bmouLibelle() {
    return (String) storedValueForKey(BMOU_LIBELLE_KEY);
  }

  public void setBmouLibelle(String value) {
    takeStoredValueForKey(value, BMOU_LIBELLE_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOExercice exercice() {
    return (org.cocktail.bibasse.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.bibasse.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.bibasse.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.bibasse.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire typeMouvementBudgetaire() {
    return (org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire)storedValueForKey(TYPE_MOUVEMENT_BUDGETAIRE_KEY);
  }

  public void setTypeMouvementBudgetaireRelationship(org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire oldValue = typeMouvementBudgetaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_MOUVEMENT_BUDGETAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_MOUVEMENT_BUDGETAIRE_KEY);
    }
  }

  public org.cocktail.bibasse.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.bibasse.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.bibasse.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.bibasse.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }

  public NSArray budgetMouvementsCredits() {
    return (NSArray)storedValueForKey(BUDGET_MOUVEMENTS_CREDITS_KEY);
  }

  public NSArray budgetMouvementsCredits(EOQualifier qualifier) {
    return budgetMouvementsCredits(qualifier, null, false);
  }

  public NSArray budgetMouvementsCredits(EOQualifier qualifier, boolean fetch) {
    return budgetMouvementsCredits(qualifier, null, fetch);
  }

  public NSArray budgetMouvementsCredits(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.bibasse.client.metier.EOBudgetMouvCredit.MOUVEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.bibasse.client.metier.EOBudgetMouvCredit.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = budgetMouvementsCredits();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToBudgetMouvementsCreditsRelationship(org.cocktail.bibasse.client.metier.EOBudgetMouvCredit object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BUDGET_MOUVEMENTS_CREDITS_KEY);
  }

  public void removeFromBudgetMouvementsCreditsRelationship(org.cocktail.bibasse.client.metier.EOBudgetMouvCredit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BUDGET_MOUVEMENTS_CREDITS_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOBudgetMouvCredit createBudgetMouvementsCreditsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BudgetMouvementsCredit");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BUDGET_MOUVEMENTS_CREDITS_KEY);
    return (org.cocktail.bibasse.client.metier.EOBudgetMouvCredit) eo;
  }

  public void deleteBudgetMouvementsCreditsRelationship(org.cocktail.bibasse.client.metier.EOBudgetMouvCredit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BUDGET_MOUVEMENTS_CREDITS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBudgetMouvementsCreditsRelationships() {
    Enumeration objects = budgetMouvementsCredits().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBudgetMouvementsCreditsRelationship((org.cocktail.bibasse.client.metier.EOBudgetMouvCredit)objects.nextElement());
    }
  }

  public NSArray budgetMouvementsGestions() {
    return (NSArray)storedValueForKey(BUDGET_MOUVEMENTS_GESTIONS_KEY);
  }

  public NSArray budgetMouvementsGestions(EOQualifier qualifier) {
    return budgetMouvementsGestions(qualifier, null, false);
  }

  public NSArray budgetMouvementsGestions(EOQualifier qualifier, boolean fetch) {
    return budgetMouvementsGestions(qualifier, null, fetch);
  }

  public NSArray budgetMouvementsGestions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.bibasse.client.metier.EOBudgetMouvGestion.MOUVEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.bibasse.client.metier.EOBudgetMouvGestion.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = budgetMouvementsGestions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToBudgetMouvementsGestionsRelationship(org.cocktail.bibasse.client.metier.EOBudgetMouvGestion object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BUDGET_MOUVEMENTS_GESTIONS_KEY);
  }

  public void removeFromBudgetMouvementsGestionsRelationship(org.cocktail.bibasse.client.metier.EOBudgetMouvGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BUDGET_MOUVEMENTS_GESTIONS_KEY);
  }

  public org.cocktail.bibasse.client.metier.EOBudgetMouvGestion createBudgetMouvementsGestionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BudgetMouvementsGestion");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BUDGET_MOUVEMENTS_GESTIONS_KEY);
    return (org.cocktail.bibasse.client.metier.EOBudgetMouvGestion) eo;
  }

  public void deleteBudgetMouvementsGestionsRelationship(org.cocktail.bibasse.client.metier.EOBudgetMouvGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BUDGET_MOUVEMENTS_GESTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBudgetMouvementsGestionsRelationships() {
    Enumeration objects = budgetMouvementsGestions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBudgetMouvementsGestionsRelationship((org.cocktail.bibasse.client.metier.EOBudgetMouvGestion)objects.nextElement());
    }
  }


  public static EOBudgetMouvements createBudgetMouvements(EOEditingContext editingContext, NSTimestamp bmouDateCreation
, String bmouLibelle
, org.cocktail.bibasse.client.metier.EOExercice exercice, org.cocktail.bibasse.client.metier.EOTypeEtat typeEtat, org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire typeMouvementBudgetaire, org.cocktail.bibasse.client.metier.EOUtilisateur utilisateur) {
    EOBudgetMouvements eo = (EOBudgetMouvements) createAndInsertInstance(editingContext, _EOBudgetMouvements.ENTITY_NAME);
		eo.setBmouDateCreation(bmouDateCreation);
		eo.setBmouLibelle(bmouLibelle);
    eo.setExerciceRelationship(exercice);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeMouvementBudgetaireRelationship(typeMouvementBudgetaire);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBudgetMouvements.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBudgetMouvements.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOBudgetMouvements localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetMouvements)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOBudgetMouvements localInstanceIn(EOEditingContext editingContext, EOBudgetMouvements eo) {
    EOBudgetMouvements localInstance = (eo == null) ? null : (EOBudgetMouvements)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBudgetMouvements#localInstanceIn a la place.
   */
	public static EOBudgetMouvements localInstanceOf(EOEditingContext editingContext, EOBudgetMouvements eo) {
		return EOBudgetMouvements.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOBudgetMouvements fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBudgetMouvements fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetMouvements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetMouvements)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetMouvements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetMouvements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetMouvements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetMouvements)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃ©.
	   */
	  public static EOBudgetMouvements fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetMouvements eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetMouvements ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetMouvements fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}