

// EOTypeMouvementBudgetaire.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeMouvementBudgetaire extends _EOTypeMouvementBudgetaire {

    public static  final String ENTITY_NAME="TypeMouvementBudgetaire";
    public static  final String MOUVEMENT_VENTILATION="VENTILATION";
    public static  final String MOUVEMENT_VIREMENT="VIREMENT";
    public static  final String MOUVEMENT_MIO="MIO";
    
    public static  final String PROBLEME_TYPE_VENTILATION="CECI N EST PAS UNE VENTILATION";
    public static  final String PROBLEME_TYPE_MIO="CECI N EST PAS UNE VENTILATION";
    public static  final String PROBLEME_TYPE_VIREMENT="CECI N EST PAS UNE VENTILATION";

    
    public EOTypeMouvementBudgetaire() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
