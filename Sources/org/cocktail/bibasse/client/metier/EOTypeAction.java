

// EOTypeAction.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeAction extends _EOTypeAction {

    public static  final String  ACTION_DEPENSE ="DEPENSE"; // a definir
    public static  final String  ACTION_RECETTE ="RECETTE"; // a definir
    
    public static  final String NIVEAU_ACTION = "3"; // voir table de grum nomenclature LOLF
    public static  final String ERROR_MESS_NIVEAU_ACTION="L EXERCICE EST FERME";

    public EOTypeAction() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
