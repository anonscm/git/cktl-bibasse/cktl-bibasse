

// EOBudgetMouvCredit.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetMouvCredit extends _EOBudgetMouvCredit {

    public EOBudgetMouvCredit() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {

    	System.out.println("EOBudgetMouvCredit.validateForInsert()");
    	
    	this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    public void addBudgetMouvements(EOBudgetMouvCredit eog,EOBudgetMouvements eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "mouvement");
    }
    
    public void addTypeEtat(EOBudgetMouvCredit eog,EOTypeEtat eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeEtat");
    }
    
    
//    public void addUtilisateur(EOBudgetMouvCredit eog,EOUtilisateur eo)
//    {
//        
//        eog.addObjectToBothSidesOfRelationshipWithKey(
//                eo,
//                "utilisateur");
//    }
    
    public void addTypeSens(EOBudgetMouvCredit eog,EOTypeSens eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeSens");
    }
    
    public void addExercice(EOBudgetMouvCredit eog,EOExercice eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");

    }
    
    public void addTypeCredit(EOBudgetMouvCredit eog,EOTypeCredit eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeCredit");

    }
    
    public void addOrgan(EOBudgetMouvCredit eog,EOOrgan eo)
    {
        
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "organ");

    }

}
