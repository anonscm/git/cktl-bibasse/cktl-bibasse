

// EOTypeEtat.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOTypeEtat extends _EOTypeEtat {

    /* A la creation etat VALIDE
     * autorisation :
     * VALIDE -> CLOT
     * VALIDE -> ANNULE
     * autres transitions impossibles.
     * 
     */
    // etat disponible a modification  MODIFIABLE
    public final static String ETAT_VALIDE="VALIDE";
    public static final String ERROR_MESS_ETAT_VALIDE="ETAT DOIT ETRE VALIDE";
    public static final String ERROR_MESS_ETAT_NON_VALIDE="ETAT NE DOIT PAS ETRE VALIDE";
    
    // etat annul? impossible de modifier al saisie NON MODIFIABLE
    public final static String ETAT_ANNULE="ANNULE";
    public static final String ERROR_MESS_ETAT_ANNULE="ETAT DOIT ETRE  ANNULE";
    public static final String ERROR_MESS_ETAT_NON_ANNULE="ETAT NE DOIT PAS ETRE  ANNULE";

    public final static String ETAT_CONTROLE = "CONTROLE";
    public final static String ETAT_VOTE = "VOTE";

    // etat clot , saisie ferm?e NON MODIDIABLE
    public final static String ETAT_CLOT="CLOTURE";
    public static final String ERROR_MESS_ETAT_CLOT="ETAT DOIT ETRE CLOT";
    public static final String ERROR_MESS_ETAT_NON_CLOT="ETAT NE DOIT PAS  ETRE  CLOT";
   
    
    // etat TRAITE , saisie ferm?e NON MODIDIABLE
    // etat gere par les packages PLSQL !!!!!!
    public final static String ETAT_TRAITE="TRAITE";
    public static final String ERROR_MESS_ETAT_TRAITE="ETAT DOIT ETRE TRAITE";
    public static final String ERROR_MESS_ETAT_NON_TRAITE="ETAT NE DOIT PAS  TRAITE";

    public final static String ETAT_EN_COURS = "EN COURS";

    public EOTypeEtat() {
        super();
    }

    /**
     * 
     * @return
     */
     public boolean isEnCours()	{    	
     	return ETAT_EN_COURS.equals(tyetLibelle());
     }

     /**
    * 
    * @return
    */
    public boolean isValide()	{    	
    	return ETAT_VALIDE.equals(tyetLibelle());
    }
    
    /**
     * 
     * @return
     */
     public boolean isControle()	{    	
     	return ETAT_CONTROLE.equals(tyetLibelle());
     }
     
    /**
     * 
     * @return
     */
     public boolean isAnnule()	{    	
     	return ETAT_ANNULE.equals(tyetLibelle());
     }

     /**
      * 
      * @return
      */
      public boolean isCloture()	{    	
      	return ETAT_CLOT.equals(tyetLibelle());
      }

      
      
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
