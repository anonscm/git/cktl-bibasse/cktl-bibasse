

// EOExercice.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOExercice extends _EOExercice {

    public static  final String ERROR_MESS_FERMETURE="L EXERCICE EST FERME";

    public EOExercice() {
        super();
    }

    
    public String etat_detaille()	{
   
    	if (exeStat().equals("P"))	return "Préparation";
    	
    	if (exeStat().equals("C"))	return "Clos";
    	
    	return "Ouvert";
    }

    
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
