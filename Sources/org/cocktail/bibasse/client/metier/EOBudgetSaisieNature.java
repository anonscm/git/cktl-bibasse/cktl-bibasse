

// EOBudgetSaisieNature.java
// 
package org.cocktail.bibasse.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOBudgetSaisieNature extends _EOBudgetSaisieNature {

    public EOBudgetSaisieNature() {
        super();
    }

    public String pcoNumVote() {
        if (super.pcoNumVote()==null && planComptable()!=null)
      	  setPcoNumVote(planComptable().pcoNum());
        return super.pcoNumVote();
    }
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
    
    public void addTypeEtat(EOBudgetSaisieNature eog,EOTypeEtat eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeEtat");
    }
    
    public void addTypeCredit(EOBudgetSaisieNature eog,EOTypeCredit eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "typeCredit");
    }
    public void addPlanComptable(EOBudgetSaisieNature eog,EOPlanComptable eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "planComptable");
    }
    public void addOrgan(EOBudgetSaisieNature eog,EOOrgan eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "organ");
    }
    public void addExercice(EOBudgetSaisieNature eog,EOExercice eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "exercice");
    }
    public void addBudgetSaisie(EOBudgetSaisieNature eog,EOBudgetSaisie eo)
    {
        eog.addObjectToBothSidesOfRelationshipWithKey(
                eo,
                "budgetSaisie");
    }
    

}
