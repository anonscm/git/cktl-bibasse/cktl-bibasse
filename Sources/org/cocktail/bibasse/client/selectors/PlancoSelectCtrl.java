package org.cocktail.bibasse.client.selectors;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.finder.FinderPlanComptable;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOPlanComptable;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class PlancoSelectCtrl  {


	private static PlancoSelectCtrl sharedInstance;
	private EOEditingContext ec;

	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private JTextField filtreCompte, filtreLibelle;

	private EOPlanComptable currentPlanComptable;

	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected JPanel viewTable, viewTableRecettes;


	/**
	 *
	 *
	 */
	public PlancoSelectCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;

		initGUI();
		initView();
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static PlancoSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new PlancoSelectCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	public void initView()	{

        mainWindow = new JDialog(mainFrame, "Plan Comptable", true);

        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

        filtreCompte = new JTextField("");
        filtreCompte.setPreferredSize(new Dimension(60,18));
        filtreCompte.setFont(Configuration.instance().informationFiltreFont(ec));
        filtreLibelle = new JTextField("");
        filtreLibelle.setPreferredSize(new Dimension(440,18));
        filtreLibelle.setFont(Configuration.instance().informationFiltreFont(ec));

        filtreLibelle.getDocument().addDocumentListener(new ADocumentListener());
        filtreCompte.getDocument().addDocumentListener(new ADocumentListener());

		JPanel panelNorth = new JPanel(new BorderLayout());
//		panelNorth.add(new JLabel("Compte : " ));
		panelNorth.add(filtreCompte, BorderLayout.WEST);
//		panelNorth.add(new JLabel("Libellé : " ));
		panelNorth.add(filtreLibelle, BorderLayout.CENTER);
		panelNorth.setBorder(BorderFactory.createEmptyBorder(0,2,0,2));

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 23));
		panelButtons.setBorder(BorderFactory.createEmptyBorder(2,0,0,0));

		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		mainView.setPreferredSize(new Dimension(500, 600));
		mainView.add(panelNorth, BorderLayout.NORTH);
		mainView.add(viewTable, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);

		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}


	/**
	 *
	 * @return
	 */
	public EOPlanComptable getPlanComptable(String classe, EOExercice exercice)	{

		eod.setObjectArray(FinderPlanComptable.findComptesForSelection(ec, classe, exercice));
		filter();

		ZUiUtil.centerWindow(mainWindow);
		mainWindow.show();

		if (currentPlanComptable == null)
			return null;

		return currentPlanComptable;

	}


	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

		eod = new EODisplayGroup();

//		NSMutableArray sort = new NSMutableArray();
//		sort.addObject(new NSArray(new EOSortOrdering("pcoNum", EOSortOrdering.CompareAscending)));
//		eod.setSortOrderings(sort);

        viewTable = new JPanel();
        viewTableRecettes = new JPanel();

		initTableModel();
		initTable();

		myEOTable.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerExercice());
//		myTableSorter.setTableHeader(myEOTable.getTableHeader());


	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		Vector myCols = new Vector();

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eod, "pcoNum", "Compte", 60);
		col1.setAlignment(SwingConstants.LEFT);
		myCols.add(col1);

		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eod, "pcoLibelle", "Libellé", 440);
		col2.setAlignment(SwingConstants.LEFT);
		myCols.add(col2);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super("Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
        }

        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }
	}


	/**
	 *
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();

        if (!StringCtrl.chaineVide(filtreLibelle.getText()))	{
            NSArray args = new NSArray("*"+filtreLibelle.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("pcoLibelle caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(filtreCompte.getText()))	{
            NSArray args = new NSArray("*"+filtreCompte.getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("pcoNum caseInsensitiveLike %@",args));
        }

        return new EOAndQualifier(mesQualifiers);
    }

    /**
    *
    */
   public void filter()	{

       eod.setQualifier(getFilterQualifier());
       eod.updateDisplayedObjects();
       myEOTable.updateData();

   }


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
        }

        public void actionPerformed(ActionEvent e) {
        	myTableModel.fireTableDataChanged();
        	mainWindow.dispose();
        }
	}


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerExercice implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			mainWindow.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentPlanComptable = (EOPlanComptable)eod.selectedObject();
		}
	   }


	   /**
	    * Permet de définir un listener sur le contenu du champ texte qui sert à filtrer la table.
	    * Dès que le contenu du champ change, on met à jour le filtre.
	    * Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	    *
	    * @author Rodolphe Prin
	    */
	   private class ADocumentListener implements DocumentListener {
	       public void changedUpdate(DocumentEvent e) {
	           filter();
	       }

	       public void insertUpdate(DocumentEvent e) {
	           filter();
	       }

	       public void removeUpdate(DocumentEvent e) {
	           filter();
	       }
	   }
}
