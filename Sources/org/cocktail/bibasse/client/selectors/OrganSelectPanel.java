package org.cocktail.bibasse.client.selectors;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class OrganSelectPanel  {

	
	private static OrganSelectPanel sharedInstance;
	private EOEditingContext ec;
	
	protected OrganSelectCtrl mySelector;
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;

	private EOOrgan currentOrgan;
	private EOExercice currentExercice;

	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSelect 		actionSelect = new ActionSelect();

	protected ActionAdd 		actionAdd = new ActionAdd();
	protected ActionUpdate 		actionUpdate = new ActionUpdate();

	protected JPanel viewTable, viewTableRecettes;
	

	/**
	 * 
	 *
	 */
	public OrganSelectPanel(EOEditingContext editingContext, JFrame frame)	{
		super();
		ec = editingContext;
		
		mainFrame = frame;
		
		initGUI();
		updateData();
	}

	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OrganSelectPanel sharedInstance(EOEditingContext editingContext, JFrame frame)	{
		if (sharedInstance == null)	
			sharedInstance = new OrganSelectPanel(editingContext, frame);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
        mainWindow = new JDialog(mainFrame, "Exercice", true);
        
        viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionSelect);
		JPanel panelSouthButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 50, 23));                
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelSouthButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setPreferredSize(new Dimension(550, 350));

		mainView.add(mySelector.mainPanel(), BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
				
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
		
	/**
	 * 
	 * @return
	 */
	public EOOrgan getOrgan(EOExercice exercice, NSArray privateOrgans)	{
		
		currentExercice = exercice;
		
		mySelector = new OrganSelectCtrl(ec, currentExercice, null, (NSArray)privateOrgans.valueForKey("organ"));
		
		initView();

		ZUiUtil.centerWindowInContainer(mainWindow);
		mainWindow.show();
		
		currentOrgan = mySelector.getSelectedOrgan();
		
		if (currentOrgan == null)	
			return null;
		
		return currentOrgan;
		
	}
	
	/**
	 * 
	 *
	 */
	public void updateData()	{
			
			
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eod = new EODisplayGroup();

        viewTable = new JPanel();
        viewTableRecettes = new JPanel();
		
		initTableModel();
		initTable();
		
		myEOTable.setBackground(ConstantesCocktail.COLOR_FILTRES_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(new ListenerExercice());
//		myTableSorter.setTableHeader(myEOTable.getTableHeader());		


	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eod, "exeExercice", "Exercice", 80);
		col1.setAlignment(SwingConstants.LEFT);
		col1.setEditable(false);
		myCols.add(col1);
		
		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

	}

	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionUpdate extends AbstractAction {

	    public ActionUpdate() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_UPDATE);
        }
	    
        public void actionPerformed(ActionEvent e) {

        }  
	} 

	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionAdd extends AbstractAction {

	    public ActionAdd() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_ADD);
        }
	    
        public void actionPerformed(ActionEvent e) {

        }  
	} 
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionSelect extends AbstractAction {

	    public ActionSelect() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	mainWindow.dispose();
        }  
	} 

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

	    public ActionCancel() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	myTableModel.fireTableDataChanged();
        	mainWindow.dispose();
        }  
	} 
	
	
	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	   private class ListenerExercice implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			mainWindow.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentExercice = (EOExercice)eod.selectedObject();
		}
	   }
	
}
