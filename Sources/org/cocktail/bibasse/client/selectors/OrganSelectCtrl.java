/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.client.selectors;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.finder.Finder;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.selectors.TreeSelectPanel.ITreeSelectMdl;
import org.cocktail.bibasse.client.zutil.logging.ZLogger;
import org.cocktail.bibasse.client.zutil.ui.IZDataCompModel;
import org.cocktail.bibasse.client.zutil.ui.ZAbstractPanel;
import org.cocktail.bibasse.client.zutil.ui.ZCommonDialog;
import org.cocktail.bibasse.client.zutil.ui.forms.ZTextField;
import org.cocktail.bibasse.client.zutil.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.bibasse.client.zutil.ui.forms.ZTextField.IZTextFieldModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

public class OrganSelectCtrl extends CommonCtrl implements OrganTreeNode.ILBudTreeNodeDelegate {
    private static final String TITLE = "Sélection d'une ligne budgétaire";
    private final Dimension SIZE = new Dimension(450, 500);

    private final TreeSelectPanel mainPanel;
    private final static String TREE_SRCH_STR_KEY = "srchStr";    
    private final ActionOk actionOk = new ActionOk();
    private final ActionCancel actionCancel = new ActionCancel();
    private final ActionSrchFilter actionSrchFilter = new ActionSrchFilter();
    
    private final LbudSelectMdl lbudSelectMdl;
    
    private final OrganTreeMdl lBudTreeMdl;
    private final LBudTreeCellRenderer lBudTreeCellRenderer ;
    
    private final ZTextField.DefaultTextFieldModel treeSrchFilterMdl;
    
    
    private final Map treeSrchFilterMap = new HashMap();
    private final ArrayList nodesToIgnoreInSrch = new ArrayList();
    private String lastSrchTxt = null;
    private EOQualifier qualDatesOrgan;    
    
    private final EOExercice currentExercice;
    private final LabelOrganMdl labelOrganMdl;
    
    public OrganSelectCtrl(EOEditingContext editingContext, EOExercice exer, final EOOrgan organ, NSArray privateOrgans) {
       
    	super();
        currentExercice = exer;
        treeSrchFilterMdl = new DefaultTextFieldModel(treeSrchFilterMap, TREE_SRCH_STR_KEY);
        labelOrganMdl = new LabelOrganMdl();
        
        lBudTreeMdl = new OrganTreeMdl(null, true);
        lBudTreeMdl.setPrivateOrgans(privateOrgans);
        
        lBudTreeCellRenderer = new LBudTreeCellRenderer();
        lbudSelectMdl = new LbudSelectMdl();
        mainPanel = new TreeSelectPanel(lbudSelectMdl);      
        
        mainPanel.getTree().addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                try {
                    onOrganSelectionChanged();
                } catch (Exception e1) {
                    //showErrorDialog(e1);
                }
            }
        });        
        
        updateQualsDateOrgan(currentExercice);
        updateTreeModelFull();        
        lBudTreeMdl.reload();
        expandPremierNiveau();
        
        //preselection
        if (organ != null) {
            TreePath path = lBudTreeMdl.findOrgan(organ);
            if (path != null) {
                mainPanel.getTree().setSelectionPath(path);
                mainPanel.getTree().scrollPathToVisible(path);
            }
        }        
        
        
    }
    
    private void onOrganSelectionChanged() {
        try {
            //ZLogger.verbose("onOrganSelectionChanged");
            mainPanel.updateData();
            
			NSNotificationCenter.defaultCenter().postNotification("selectedOrganHasChanged", getSelectedOrgan());

        }
        catch (Exception e) {
            //showErrorDialog(e);
        }
    }
    
    private void expandPremierNiveau() {
        mainPanel.getTree().expandAllObjectsAtLevel(1,true);
    }
    
    
    public void updateTreeModelFull() {
        //memoriser la selection
        TreePath oldPath = null;
        boolean expanded = false;
        if (mainPanel != null && mainPanel.getTree() != null) {
            oldPath = mainPanel.getTree().getSelectionPath();
            expanded = mainPanel.getTree().isExpanded(oldPath);
        }
        final EOOrgan selectedOrgan = getSelectedOrgan();
        //        ZLogger.debug("oldPath = " + oldPath);
        //        ZLogger.debug("expanded = " + expanded);        
        //        ZLogger.debug("organ = " + selectedOrgan);        

        final EOOrgan _organRoot = getOrganRoot();
        //ZLogger.verbose("organroot = " +_organRoot);
        
        final OrganTreeNode rootNode = lBudTreeMdl.createNode(null, _organRoot, this);
        lBudTreeMdl.setRoot(rootNode);
        lBudTreeMdl.invalidateNode(rootNode);      
        

        //expand 1er niveau
        expandPremierNiveau();
//        final NSArray organFils = _organRoot.organFils();
//        for (int i = 0; i < organFils.count(); i++) {
//            final EOOrgan element = (EOOrgan) organFils.objectAtIndex(i);
//            final TreePath path = lBudTreeMdl.findOrgan(element);
//            if (path != null) {
//                mainPanel.getTreeLbud().expandPath(path);
//            }
//        }
//        

        
        
        if (selectedOrgan != null) {
            TreePath path = lBudTreeMdl.findOrgan(selectedOrgan);
            if (path == null) {
                path = oldPath;
            }

            //ZLogger.debug("path = " + path);

            mainPanel.getTree().setSelectionPath(path);
            mainPanel.getTree().scrollPathToVisible(path);
            if (path.equals(oldPath) && expanded) {
                mainPanel.getTree().expandPath(path);
            }
            
            
            
        }

        lBudTreeMdl.setQualDatesOrgan(qualDatesOrgan);
    }
    
    
    private void updateQualsDateOrgan(EOExercice lastexer) {

        //ZLogger.debug("lastExer = " + lastexer);
        if (lastexer != null) {
//            final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(lastexer.exeExercice().intValue());
//            final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(lastexer.exeExercice().intValue()), 1, 0, 0, 0);

          //  ZLogger.debug("firstDayOfYear = " + firstDayOfYear);
          //  ZLogger.debug("lastDayOfYear = " + lastDayOfYear);
//            qualDatesOrgan = EOQualifier.qualifierWithQualifierFormat("(orgDateOuverture=nil or orgDateOuverture<%@) and (orgDateCloture=nil or orgDateCloture>=%@)", new NSArray(new Object[] {
//                    lastDayOfYear, firstDayOfYear }));
            
          qualDatesOrgan = EOQualifier.qualifierWithQualifierFormat(EOOrgan.EXERCICE_KEY + " = %@", new NSArray(lastexer));

            
        } else {
            qualDatesOrgan = null;
        }
        
        System.out.println("OrganSelectCtrl.updateQualsDateOrgan() " + qualDatesOrgan);
        
        lBudTreeMdl.setQualDatesOrgan(qualDatesOrgan);
    }    
    
    
    /**
     * 
     * @return
     */
    public final EOOrgan getOrganRoot() {
    	
    	EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + "=" + 0 + " and exercice = %@", new NSArray(currentExercice));

        final EOOrgan org = (EOOrgan) Finder.fetchObject(getEditingContext(), EOOrgan.ENTITY_NAME, myQualifier, null, false);

        return org;
        
    }

    
    /**
     */
    public EOOrgan getSelectedOrgan() {
        final OrganTreeNode node = getSelectedNode();
        if (node == null) {
            return null;
        }
        return node.getOrgan();
    }
    
    public OrganTreeNode getSelectedNode() {
        if (mainPanel == null || mainPanel.getTree() == null) {
            return null;
        }
        return (OrganTreeNode) mainPanel.getTree().getLastSelectedPathComponent();
    }
    
    
    private void onCancel() {
        getMyDialog().setModalResult(ZCommonDialog.MRCANCEL);
        getMyDialog().hide();
    }
    
    private void onOk() {
        getMyDialog().setModalResult(ZCommonDialog.MROK);
        getMyDialog().hide();
    }
    
    
    
    public Dimension defaultDimension() {
        return SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }

    public String title() {
        return TITLE;
    }

    
    private final class LbudSelectMdl implements ITreeSelectMdl {

        public Action actionCancel() {
            return actionCancel;
        }

        public Action actionOk() {
            return actionOk;
        }

        public AbstractAction actionSrchFilter() {
            return actionSrchFilter;
        }

        public TreeCellRenderer getLbudTreeCellRenderer() {
            return lBudTreeCellRenderer;
        }

        public TreeModel getSelectionTreeModel() {
            return lBudTreeMdl;
        }

        public IZTextFieldModel getTreeSrchModel() {
            return treeSrchFilterMdl;
        }

        public IZDataCompModel getLabelSelectedMdl() {
            return labelOrganMdl;
        }
        
    }

    
    private final class LabelOrganMdl implements IZDataCompModel {

        public Object getValue() {
            return (getSelectedOrgan()==null ? "" : getSelectedOrgan().getLongString());
        }

        public void setValue(Object value) {
            
        }
        
    }
    
    protected final class ActionCancel extends AbstractAction {
        public ActionCancel() {
            this.putValue(AbstractAction.NAME, "Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
        }
        public void actionPerformed(ActionEvent e) {
            onCancel();
        }

    }        
    
    
    protected final class ActionOk extends AbstractAction {
        public ActionOk() {
            this.putValue(AbstractAction.NAME, "Ok");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
        }
        public void actionPerformed(ActionEvent e) {
            onOk();
        }
        
    }   
    
    private final class ActionSrchFilter extends AbstractAction {
        public ActionSrchFilter() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher/suivant");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
        }

        public void actionPerformed(ActionEvent e) {
            onTreeFilterSrch();
        }

    }
    
    public void onTreeFilterSrch() {
        String str = (String) treeSrchFilterMap.get(TREE_SRCH_STR_KEY);
        
        if (str == null || !str.equals(lastSrchTxt)) {
            ZLogger.verbose("Nouvelle recherche = " + str);
            nodesToIgnoreInSrch.clear();
            lastSrchTxt = str;
        }

        ZLogger.verbose("recherche de = " + str);
        final OrganTreeNode node = lBudTreeMdl.find(null, str, nodesToIgnoreInSrch, true);

        if (node != null) {
            ZLogger.verbose("Trouve = " + node.getOrgan().getLongString());
            final TreePath path = new TreePath(lBudTreeMdl.getPathToRoot(node));
            mainPanel.getTree().setSelectionPath(path);
            mainPanel.getTree().scrollPathToVisible(path);
            nodesToIgnoreInSrch.add(node);
        }

    }    
 
    public class LBudTreeCellRenderer extends DefaultTreeCellRenderer {
//        private final Icon ICON_CLOSED_NORMAL=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL);
//        private final Icon ICON_CLOSED_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_WARN);
//        private final Icon ICON_OPEN_NORMAL=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL);
//        private final Icon ICON_OPEN_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_WARN);
//        private final Icon ICON_LEAF_NORMAL=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL);
//        private final Icon ICON_LEAF_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_WARN);

        public LBudTreeCellRenderer() {
            
        }

        public Component getTreeCellRendererComponent(
                            JTree tree,
                            Object value,
                            boolean sel,
                            boolean expanded,
                            boolean leaf,
                            int row,
                            boolean _hasFocus) {

            super.getTreeCellRendererComponent(
                            tree, value, sel,
                            expanded, leaf, row,
                            _hasFocus);
            
            
            final OrganTreeNode node = (OrganTreeNode)value;
            
//            if (!node.isMatchingQualifier() ) {
//                setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + node.getTitle() + ZHtmlUtil.STRIKE_PREFIX + ZHtmlUtil.HTML_SUFFIX);
//            }
//            else {
                setText(node.getTitle());
//            }
            
            
//            if (leaf) {
//                setIcon( ICON_LEAF_NORMAL );
//            }
//            else if (expanded) {
//                setIcon( ICON_OPEN_NORMAL );
//            }
//            else {
//                setIcon( ICON_CLOSED_NORMAL );
//            }
            
//            setDisabledIcon(getIcon());
            
            return this;
        }
    }

    public Object[] getOrganWarnings(EOOrgan organ) {
        return null;
    }

    public boolean accept(EOOrgan organ) {
        return true;
    }

	public EOExercice getExercice() {
		return currentExercice;
	}    
    
}
