/*
 * Copyright COCKTAIL, 1995-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.client.selectors;

import org.cocktail.bibasse.client.finder.FinderOrgan;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.zutil.tree.ZTreeNode2;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class OrganTreeNode extends ZTreeNode2 {

	private EOOrgan _organ;
    private final ILBudTreeNodeDelegate _delegate;
    
    private NSArray privateOrgans = new NSArray();

    public OrganTreeNode(final OrganTreeNode parentNode, final EOOrgan organ, ILBudTreeNodeDelegate delegate, NSArray organs) {
        super(parentNode);
        _delegate = delegate;
        _organ = organ;
        privateOrgans = new NSArray(organs);
    }


    public boolean getAllowsChildren() {
        return (_organ==null ? false :  _organ.orgNiveau().intValue() < EOOrgan.ORG_NIV_MAX);
    }
    
        
    /**
     * A appeler lorsqu'il est necessaire de rafraichir l'arbre à partir de ce noeud. 
     * Utiliser plutot invalidateNode().
     *
     */
    protected void refreshChilds() {
    	
    	childs.clear();
    	if (getAllowsChildren()) {
    		    		
    		NSArray organFils = FinderOrgan.findOrgansForOrganPere(getOrgan().editingContext(), getOrgan(), _delegate.getExercice());
    		
    		organFils = EOSortOrdering.sortedArrayUsingKeyOrderArray(organFils, new NSArray(new Object[]{EOOrgan.SORT_ORG_UNIV_ASC, EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC}));
    		    		    		
    		for (int i = 0; i < organFils.count(); i++) {
    			
    			final EOOrgan element = (EOOrgan) organFils.objectAtIndex(i);
    			
    			if (element.orgNiveau().intValue()<2 || privateOrgans.containsObject((element)))	{
    				if (_delegate.accept(element)) {
    					final OrganTreeNode tren = new OrganTreeNode(this, element, _delegate, privateOrgans);
    					tren.invalidateNode();
    				}
    			}
    		}
    	}        
    }
    
  
    public EOOrgan getOrgan() {
        return _organ;
    }
    
    public Object getAssociatedObject() {
        return getOrgan();
    }

    public String getTitle() {
        return _organ.getShortString();
    }

    
    public interface ILBudTreeNodeDelegate {
        public Object[] getOrganWarnings(final EOOrgan organ);
        
        public EOExercice getExercice(); 
        
        /** doit renvoyer false si pour une raison ou une autre il ne faut pas intégrer l'organ dans l'arbre*/
        public boolean accept(final EOOrgan organ);
    }
}
