package org.cocktail.bibasse.client;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.finder.FinderExercice;
import org.cocktail.bibasse.client.finder.FinderUtilisateurFonction;
import org.cocktail.bibasse.client.finder.FinderUtilisateurOrgan;
import org.cocktail.bibasse.client.metier.EOBudgetParametres;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOUtilisateur;
import org.cocktail.bibasse.client.saisie.OrganPopupCtrl;
import org.cocktail.bibasse.client.utils.CRICursor;
import org.cocktail.bibasse.client.utils.DateCtrl;
import org.cocktail.bibasse.client.utils.StreamCtrl;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.utils.XLogs;
import org.cocktail.bibasse.client.utils.XWaitingFrame;
import org.cocktail.bibasse.client.zutil.ui.ZDatePickerPanel;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppFromJar;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOWindowObserver;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Point d'entree de l'application client.
 *
 */
public class ApplicationClient extends EOApplication {

	/** Version. */
	//public static final String VERSION_APPLICATION = "Version " + VersionCommon.VERSIONNUM + " du " + VersionCommon.VERSIONDATE;

	//public static final String WINDOWS_OS_NAME = "Windows";
	public static final String WINDOWS_OS_NAME = "Win"; // A utiliser avec startsWith
	public static final String WINDOWS2000_OS_NAME = "Windows 2000";

	public static final String WINDOWS_FILE_PATH_SEPARATOR = "\\"+"\\";
	public static final String APPLICATION_FILE_PATH_SEPARATOR = "\\";

	public static final String MAC_OS_X_OPEN = "open ";

	// Chaines correspondant a System.getProperties().getProperty(os.name)
	public static final String MAC_OS_X_OS_NAME = "Mac OS X";
	//public static final String WINDOWS2000_OS_NAME = "Windows 2000";
	// Chaines correspondant aux commandes permettant de lancer une commande sur les différents systemes
	public static final String MAC_OS_X_EXEC = "open ";
	public static final String WINDOWS_EXEC = "launch.bat ";

	private JTextField myDateTextField;

	protected		Superviseur				toSuperviseur;

	public 	JFrame			panelAttente;
	public	JLabel			messageAttente;

	private MyByteArrayOutputStream	redirectedOutStream, redirectedErrStream; // Nouvelles sorties

	public String temporaryDir;
	public String osName;
	private String homeDir;


	public XWaitingFrame waitingFrame;

	protected String niveauSaisie;
	protected boolean repartitionNatureLolf;
	protected boolean ctrlSaisieRecette;
	protected Number niveauLolf;

	protected String etablissement;
	protected EOExercice currentExercice;
	protected EOUtilisateur currentUtilisateur;
	protected NSArray userOrgans, userFonctions;

	protected EOEditingContext editingContext;
	private String applicationVersion;

	/**
	 * Constructeur.
	 */
	public ApplicationClient()	{

		super();

		// redirection des logs
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));

		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);

		editingContext = new EOEditingContext();

	}

	/**
	 * Post initialisation hook.
	 */
	public void finishInitialization() {

		super.finishInitialization();
		setQuitsOnLastWindowClose(false);

		try {
			String serverVersion = ServerProxy.serverVersion(editingContext());
			compareJarVersionsClientAndServer();
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", e.getMessage());
			quit();
		}

		initApplication();

	}

	private void compareJarVersionsClientAndServer() throws Exception {
		Boolean isDevMode = CktlServerInvoke.clientSideRequestIsDevelopmentMode(editingContext());

		if (isDevMode) {
			System.out.println("Mode développement, pas de vérification de cohérence entre les versions des jars client et serveur.");
			return;
		}
		String serverJarVersion = CktlServerInvoke.clientSideRequestGetVersionFromJar(editingContext());
		String clientJarVersion = clientJarVersion();
		System.out.println("Serveur build : " + serverJarVersion);
		System.out.println("Client build : " + clientJarVersion);
		if (serverJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté serveur.");
		}
		if (clientJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté client.");
		}
		if (!serverJarVersion.equals(clientJarVersion)) {
			throw new Exception("Incoherence entre la version cliente et la version serveur. Les ressources web ne sont probablement pas à jour." + serverJarVersion + " / " + clientJarVersion);
		}
	}

	private String clientJarVersion() {
		String className = this.getClass().getName();
		VersionAppFromJar varAppFromJar = new VersionAppFromJar(className);
		return varAppFromJar.fullVersion();
	}

	/**
	 *
	 */
	public void initApplication() {
		// Recuperation de l'exercice courant et test de la connection a la base de donnees
		currentExercice = FinderExercice.findExerciceCourant(editingContext());
		if (currentExercice == null)	{
			EODialogs.runErrorDialog("ERREUR EXERCICE","Problème d'accés à la base de données !");
			return;
		}

		initParametres();

		// Login / Password et verification des droits
		currentUtilisateur = ApplicationConnect.sharedInstance(editingContext()).connect("BIBASSE");

		if (!checkDroitsUtilisateur())
				quit();

		waitingFrame = new XWaitingFrame("BIBASSE - Initialisation", "Chargement en cours ...","Veuillez patienter !",false);

		String requeteEtablissement = "select LL_STRUCTURE from grhum.structure_ulr where c_type_structure='E' and tem_valide='O'";
		NSArray retourRequete = ServerProxy.clientSideRequestSqlQuery(editingContext(), requeteEtablissement);
		etablissement = (String)((NSDictionary)retourRequete.objectAtIndex(0)).objectForKey("LL_STRUCTURE");

		System.out.println("     - Etablissement : " + etablissement);

		String titreWindow = "BIBASSE - Gestion du Budget " + currentExercice.exeExercice();

		toSuperviseur = new Superviseur();
		toSuperviseur.init(titreWindow);

		initForPlatform();

	}

	private void initParametres() {
		EOBudgetParametres paramNatureLolf = FinderBudgetParametres.findParametre(editingContext(), currentExercice, "REPART_NATURE_LOLF");
		if (paramNatureLolf==null || paramNatureLolf.bparValue()==null || !paramNatureLolf.bparValue().equals("OUI")) {
			setRepartitionNatureLolf(false);
			System.out.println("SAISIE NATURE LOLF : NON");
		} else {
			setRepartitionNatureLolf(true);
			System.out.println("SAISIE NATURE LOLF : OUI");
		}

		niveauSaisie = FinderBudgetParametres.getValue(editingContext(), currentExercice ,"NIVEAU_SAISIE");
		System.out.println("ApplicationClient.initApplication() NIVEAU SAISIE : : " + niveauSaisie);

		refreshParametres(currentExercice);
	}

	/**
	 * TODO FLA : fusionner initParametres + refreshParametre et modifier Superviseur.ActionSelectExercice.actionPerformed.
	 * Je ne l'ai aps réalisé car la reherche du niveau saisie n'est pas exactement fait de la meme maniere (faudrait modifier le finder également).
	 * @param exercice exercice
	 */
	public void refreshParametres(EOExercice exercice) {
		EOBudgetParametres paramCtrlSaisieRecette = FinderBudgetParametres.findParametre(
				editingContext(), exercice, Superviseur.PARAM_CTRL_SAISIE_RECETTE);
		this.ctrlSaisieRecette = (paramCtrlSaisieRecette != null && "OUI".equalsIgnoreCase(paramCtrlSaisieRecette.bparValue()));
		System.out.println("ApplicationClient.initApplication() CONTROLE SAISIE RECETTE : " + ctrlSaisieRecette);
	}

	/**
	 *
	 * @param e
	 * @return
	 */
	public String getErrorDialog(Exception e) {
		e.printStackTrace();

		String text = e.getMessage();

		if ((text == null) || (text.trim().length() == 0)) {
			System.out.println("ERREUR... Impossible de recuperer le message...");
			e.printStackTrace();
			text = "Une erreur est survenue. Impossible de récupérer le message, il doit etre accessible dans la console...";
		}
		else {
			String[] msgs = text.split("ORA-20001:");
			if (msgs.length > 1) {
				text = msgs[1].split("\n")[0];
			}
		}
		return text;
	}

	public String getMessageFormatte(String message) {
		if (message != null) {
			message = message.replaceAll("\n", " ");
			message = message.replaceAll("\"", "''");

			int index = message.indexOf("ORA-20001:");
			if (index > -1) {
				message = message.substring(index+10);
				index = message.indexOf("ORA-");
				if (index > -1) {
					message = message.substring(0, index);
				}
			}
		}
		return message;
	}

	public String getEtablissement()	{
		return etablissement;
	}

	public String niveauSaisie()	{
		return niveauSaisie;
	}

	public void setNiveauSaisie(String niveau)	{
		niveauSaisie = niveau;
	}

	public boolean repartitionNatureLolf()	{
		return repartitionNatureLolf;
	}

	public void setRepartitionNatureLolf(boolean param)	{
		repartitionNatureLolf = param;
	}

	public boolean isCtrlSaisieRecette() {
		return ctrlSaisieRecette;
	}

	public void setCtrlSaisieRecette(boolean ctrlSaisieRecette) {
		this.ctrlSaisieRecette = ctrlSaisieRecette;
	}

	/**
	 *
	 * @return
	 */
	public boolean isSaisieUb()	{
		return "UB".equals(niveauSaisie());
	}

	/**
	 *
	 * @return
	 */
	public Number niveauLolf()	{
		return niveauLolf;
	}

	/**
	 *
	 * @return
	 */
	public boolean isSaisieCr()	{
		return "CR".equals(niveauSaisie);
	}



	/**
	 *
	 * @param login
	 * @param password
	 */
	public boolean checkDroitsUtilisateur() {

		userFonctions = new NSArray();
		userOrgans = new NSArray();

		userFonctions = FinderUtilisateurFonction.findFonctionsForUtilisateur(editingContext(), currentUtilisateur, currentExercice);

		if (userFonctions.count() == 0)	{
			EODialogs.runInformationDialog("ERREUR","Vous n'etes autorisé(e) à aucune des fonctions BIBASSE !");
			return false;
		}

		userFonctions = new NSArray((NSArray)userFonctions.valueForKeyPath("fonction.fonIdInterne"));
		System.out.println("     - Fonctions (" + userFonctions.count() + ") : " + userFonctions);

		setUserOrgans(currentExercice);

		if (userOrgans.count() == 0)	{
			EODialogs.runInformationDialog("ERREUR","Vous n'avez de droits sur aucune ligne budgétaire.");
			return false;
		}
		System.out.println("     - Lignes budgétaires : " + userOrgans.count());

		OrganPopupCtrl.sharedInstance(editingContext()).setPrivateOrgans(getUserOrgans());

		return true;
	}


	/**
	 *
	 * @param exercice
	 */
	public void setUserOrgans(EOExercice exercice) {
		System.out.println("\n\n\n\n\n\n\n\nChangement exercice : ");
		System.out.println(exercice);
		userOrgans = new NSArray(FinderUtilisateurOrgan.findOrgansForUtilisateur(editingContext(), currentUtilisateur, exercice));
		OrganPopupCtrl.sharedInstance(editingContext()).setPrivateOrgans(userOrgans);

	}

	/**
	 *
	 * @return
	 */
	public EOUtilisateur getUtilisateur()	{
		return currentUtilisateur;
	}

	/**
	 *
	 * @return
	 */
	public NSArray userFonctions()	{
		return userFonctions;
	}

	/**
	 *
	 * @return
	 */
	public NSArray getUserOrgans()	{
		return userOrgans;
	}



	public String getConnectionName()	{
		String bdConnexionName = (String)((EODistributedObjectStore)new EOEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(editingContext(), "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);

		return bdConnexionName;
	}

	public String outLogs()	{
		return redirectedOutStream.toString();
	}

	public String errLogs()	{
		return redirectedErrStream.toString();
	}

	public String version()	{
		if (applicationVersion == null) {
			applicationVersion = ServerProxy.serverVersion(editingContext);
		}
		return applicationVersion;
	}


	/**
	 *
	 */
	public Superviseur superviseur() {
		return toSuperviseur;
	}

	/**
	 *
	 * @param win
	 * @param yn
	 */
	public void setWaitCursor(Component win)	{
		CRICursor.setWaitCursor(win, true);
	}

	/**
	 *
	 * @param win
	 * @param yn
	 */
	public void setDefaultCursor(Component win)	{
		CRICursor.setWaitCursor(win, false);
	}


	/**
	 *
	 */
	public void openURL (String URL)	{
		try {
			Runtime runTime = Runtime.getRuntime();//.exec(WINDOWS_EXEC+filePath);
			runTime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", URL} );
		}
		catch(Exception exception) {
			EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+URL+"\nMESSAGE : " + exception.getMessage());				exception.printStackTrace();
		}
	}

	/**
	 *
	 */
	public void openFile(String filePath)	{
		File	aFile = new File(filePath);

		if(osName.equals(MAC_OS_X_OS_NAME)) 	{
			try {
				Runtime.getRuntime().exec(MAC_OS_X_OPEN+aFile);
			}
			catch(Exception exception) {
				EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());				exception.printStackTrace();
			}
		}
		else	{
			try {
				Runtime runTime = Runtime.getRuntime();//.exec(WINDOWS_EXEC+filePath);
				runTime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\""} );
			}
			catch(Exception exception) {
				EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());				exception.printStackTrace();
			}
		}
	}

	/**
	 *	Ecriture d'une chaine dans un fichier et ouverture de ce fichier
	 */
	public boolean writeStringToFileAndOpen(String chaine, String filePath)	{
		File 				aFile = new File(filePath);
		FileOutputStream 	anOutputStream;

		try	{
			anOutputStream = new FileOutputStream(aFile);
			anOutputStream.write(chaine.getBytes());
			anOutputStream.close();
		} catch (Exception exception) {
			System.out.println("Erreur d'ecriture du fichier !");
			return false;
		}

		openFile(filePath);

		return true;
	}

	/**
	 * Creation d'un report en fonction d'un array de valeurs et d'un nom
	 */
	public void exportExcel(String chaine, String reportName)	{
		String 		fileName;
		File 		aFile;
		FileOutputStream	anOutputStream = null;

		if(chaine != null)	{
			fileName = temporaryDir.concat(reportName.concat(".csv"));
			aFile = new File(fileName);
			try	{
				anOutputStream = new FileOutputStream(aFile);
				anOutputStream.write(chaine.getBytes());
				anOutputStream.close();
			} catch (Exception exception) {
				System.out.println(this.getClass().getName()+".imprimer() - Exception : "+exception.getMessage());
				return;
			}

			openFile(fileName);
		}
	}

	/**
	 * Convertit un chemin type Unix avec separateur "\" par un chemin avec separateur "\\" de type Windows.
	 */
	protected String convertUnixPathSeparator(String unixPath)	{
		String token;
		StringTokenizer st = new StringTokenizer(unixPath,APPLICATION_FILE_PATH_SEPARATOR);
		StringBuffer winPath = new StringBuffer();
		while(st.hasMoreTokens()) {
			token = st.nextToken();
			// cas ou le repertoire contient un espace
			if (token.indexOf(' ') != -1)  {
				token = "\"" + token + "\"";
			}
			winPath.append(token + WINDOWS_FILE_PATH_SEPARATOR);
		}
		return winPath.toString();
	}

	/**
	 *
	 */
	public Window getMainWindow() {
		System.out.println("ApplicationClient.getMainWindow() TO SUPERVISEUR : " + toSuperviseur);
		return toSuperviseur.mainFrame();
	}


	/**
	 *
	 * @return
	 */
	public String getTemporaryDir()	{
		return temporaryDir;
	}

	/**
	 *
	 */
	public void closeAllWindowsAndBringMainToFront() 	{
		for (int i = windowObserver().visibleWindows().count()-1; i >= 0; i--) {
			if ((Window)windowObserver().visibleWindows().objectAtIndex(i)!= getMainWindow()) {
				((Window)windowObserver().visibleWindows().objectAtIndex(i)).dispose();
			} else {
				((Window)windowObserver().visibleWindows().objectAtIndex(i)).show();
			}
		}
	}

	/**
	 *
	 */
	public void closeAllWindowsAndMiniaturizeMain() {
		for (int i=windowObserver().visibleWindows().count()-1;i>=0;i--) 	{
			if ((Window)windowObserver().visibleWindows().objectAtIndex(i)!= getMainWindow()  )
				((Window)windowObserver().visibleWindows().objectAtIndex(i)).dispose();
			else
				toSuperviseur.makeInvisible();
		}
	}

	/**
	 *
	 */
	public void closeAllWindowsExceptMain()
	{
		for (int i=windowObserver().visibleWindows().count()-1;i>=0;i--) 	{
			if ((Window)windowObserver().visibleWindows().objectAtIndex(i)!= getMainWindow()  )
				((Window)windowObserver().visibleWindows().objectAtIndex(i)).dispose();
		}
	}

	/**
	 *
	 */
	public class MyEOWindowObserver extends EOWindowObserver 	{

		public void windowActivated(WindowEvent event) 	{
			super.windowActivated(event);
			//Si l'évènement se produit sur la fenetre principale
			if ( event.getWindow()==getMainWindow()) 	{
				//Récupérer toutes les fenetres modales ouvertes et les mettre en premier-plan
				for (int i=visibleWindows().count()-1;i>=0;i--)	{
					if ((Window)visibleWindows().objectAtIndex(i)!= getMainWindow()  ) 	{
						Window window = (Window)visibleWindows().objectAtIndex(i);
						window.toFront();
					}
				}
			}
		}

		public void windowClosing(WindowEvent event) 	{
			super.windowClosing(event);
			//Si l'évènement se produit sur la fenetre principale
			boolean dialog = EODialogs.runConfirmOperationDialog("Fermeture de l'application ...","Etes-vous sur de vouloir quitter l'application ?","OUI","NON");

			if (dialog)	{
				if (superviseur().editingContext().updatedObjects().count() > 0)	{
					try {
						superviseur().editingContext().lock();
						superviseur().editingContext().saveChanges();
					}
					catch (Exception ex){}
					finally {
						superviseur().editingContext().unlock();
					}
				}
			}
		}
	}

	/**
	 * Recuperation d'un repertoire temporaraire ou ecrire les fichiers crees
	 *
	 * @return Retourne le chemin complet du repertoire temporaire
	 */
	public void initForPlatform() 	{
		initHomeDir();

		osName = System.getProperties().getProperty("os.name");
		System.out.println("     - OS NAME : " + osName);

		try {
			temporaryDir = System.getProperty("java.io.tmpdir");

			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
		} catch (Exception e) {
			System.out.println("Impossible de recuperer le repertoire temporaire !");
		}

		if (osName.equals(MAC_OS_X_OS_NAME)) {
			if (temporaryDir == null) {
				temporaryDir = "/tmp/";
			}
		} else {
			// On regarde si un chemin a ete entre dans le fichier Properties
			if (temporaryDir == null)	{
				try {
					EODistributedObjectStore	store = (EODistributedObjectStore)superviseur().editingContext().parentObjectStore();
					temporaryDir = (String) store.invokeRemoteMethodWithKeyPath(superviseur().editingContext(), "session", "clientSideRequestCheminImpressions", null,null,true);
				} catch (Exception e) {
				}

				if (temporaryDir == null) {
					// Rien n'est entre dans Properties, on en choisit un par defaut selon le systeme d'exploitation
					temporaryDir = "c:/temp/";
				}
			}
		}

		File tmpDir = new File(temporaryDir);
		if (!tmpDir.exists()) {
			System.out.println("Tentative de creation du repertoire temporaire " + tmpDir);
			try {
				tmpDir.mkdirs();
				System.out.println("Repertoire " + tmpDir + " cree.");
			} catch (Exception e) {
				System.out.println("Impossible de creer le repertoire " + tmpDir);
			}
		} else {
			System.out.println("     - Répertoire Temporaire : " + tmpDir);
		}
	}

	private void initHomeDir() {
		homeDir = System.getProperty("user.home");
		if (!homeDir.endsWith(File.separator)) {
			homeDir = homeDir + File.separator;
		}
	}

	/**
	 * Test de l'existence d'un repertoire
	 *
	 * @param filePath Chemin du repertoire a tester
	 * @return Retourne un boolean designant si le repertoire designe en parametres existe ou non
	 */
	public boolean testDirectory(String filePath)
	{
		File file = new File(filePath);
		if (!file.isDirectory())	{
			EODialogs.runErrorDialog("ERREUR", "Veuillez créer le répertoire " + filePath + " pour pouvoir imprimer les documents demandés.");
			return false;
		}

		return true;
	}

	/**
	 *
	 */
	public boolean afficherPdf(NSData datas, String fileName)	{

		if (temporaryDir == null)
			return false;

		if (datas == null)	{
			EODialogs.runErrorDialog("ERREUR","Impression impossible !\nVos paramètres d'impressions doivent etre mal configurés.");
			return false;
		}

		byte b[] = datas.bytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(b);

		String filePath = temporaryDir + fileName + ".pdf";

		try {StreamCtrl.saveContentToFile (stream, filePath);}
		catch (Exception e) {}

		openFile(filePath);

		return true;
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		public MyByteArrayOutputStream() {
			this(System.out);
		}
		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}
		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}
		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}

	public EOEditingContext	editingContext()	{
		return editingContext;
	}
	/**
	 *
	 *
	 */
	public void showLogs() {
		XLogs messages = new XLogs("MESSAGES CLIENT ET SERVEUR");
		messages.setVisible(true);
	}

	public void cleanLogs(String type)	{

		if (type.equals("CLIENT"))	{
			redirectedOutStream.reset();
			redirectedErrStream.reset();
		}

		if (type.equals("SERVER"))	{
			((EODistributedObjectStore)editingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(editingContext(), "session", "clientSideRequestCleanLogs", new Class[] {}, new Object[] {}, false);;
		}

	}

	/**
	 *
	 * @param typeLog
	 */
	public void sendLog(String typeLog) {
		String 	destinataire = "cpinsard@univ-lr.fr";

		try {
			String sujet  = "BIBASSE - Logs (" + DateCtrl.dateToString(new NSTimestamp(),"%d/%m/%Y %H:%M")+")";

			String	message = "BIBASSE : LOGS CLIENT ET SERVEUR.";

			message = message + "\n\n************* LOGS CLIENT *****************";
			message = message + "\nOUTPUT log :\n\n" + redirectedOutStream.toString() + "\n\nERROR log :\n\n" + redirectedErrStream.toString();

			message = message + "\n\n************* LOGS SERVER *****************";
			message = message + "\n*****************************************";

			String outLog = (String)((EODistributedObjectStore)editingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(editingContext(), "session", "clientSideRequestOutLog", new Class[] {}, new Object[] {}, false);
			String errLog = (String)((EODistributedObjectStore)editingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(editingContext(), "session", "clientSideRequestErrLog", new Class[] {}, new Object[] {}, false);
			message = message + "\nOUTPUT log SERVER :\n\n" + outLog + "\n\nERROR log SERVER :\n\n" + errLog;

			StringBuffer mail = new StringBuffer();
			mail.append(message);

			((EODistributedObjectStore)editingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(editingContext(), "session", "clientSideRequestSendMail", new Class[] {String.class,String.class,String.class,String.class,String.class}, new Object[] { "cpinsard@univ-lr.fr", destinataire, null, sujet, mail.toString() }, false);
			EODialogs.runInformationDialog("ENVOI MAIL","Le mail a bien été envoyé.");
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
	}

	/**
	 *
	 * @return
	 */
	public void setExerciceBudgetaire(EOExercice exer)	{

		currentExercice = exer;
		//setUserOrgans(currentExercice);
	}

	/**
	 *
	 * @return
	 */
	public EOExercice getExerciceBudgetaire()	{

		return currentExercice;
	}

	/**
	 *
	 * @param matrice
	 * @return
	 */
	public int getSelectedRadioButton(EOMatrix matrice)	{

		NSArray components = new NSArray(matrice.getComponents());
		int btnSelectionne;

		for (btnSelectionne = 0; btnSelectionne <components.count(); btnSelectionne ++) {
			if (((JRadioButton)components.objectAtIndex(btnSelectionne)).isSelected())
				break;
		}

		return btnSelectionne;
	}

	/**
	 *
	 * @param eo
	 * @param key
	 * @return
	 */
	public BigDecimal computeSumForKey(EODisplayGroup eo, String key) {

		BigDecimal total = new BigDecimal(0.0);

		int i = 0;
		while (i < eo.allObjects().count()) {
			try {
				if (NSDictionary.class.getName().equals(eo.allObjects().objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(eo.allObjects().objectAtIndex(i).getClass().getName())
				)	{
					NSDictionary dico = (NSDictionary) eo.allObjects().objectAtIndex(i);

					if (String.class.getName().equals(dico.objectForKey(key).getClass().getName()))
						total = total.add(new BigDecimal(StringCtrl.replace((String)dico.objectForKey(key), ",", ".")));
					else
						if (BigDecimal.class.getName().equals(dico.objectForKey(key).getClass().getName()))
							total = total.add((BigDecimal)dico.objectForKey(key));
						else
							total = total.add(new BigDecimal(dico.objectForKey(key).toString()));
				}
				else	{
					total = total.add((BigDecimal) ((EOEnterpriseObject) eo.allObjects().objectAtIndex(i)).valueForKey(key));
				}
			}
			catch (Exception e) {
				//e.printStackTrace();
				total = new BigDecimal(0.0);
				break;
			}
			i = i + 1;
		}

		return total.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 *
	 * @param eo
	 * @param key
	 * @return
	 */
	public BigDecimal computeSumForKey(NSArray array, String key) {

		BigDecimal total = new BigDecimal(0.0);

		int i = 0;
		while (i < array.count()) {
			try {
				if (NSDictionary.class.getName().equals(array.objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(array.objectAtIndex(i).getClass().getName())
				)	{
					NSDictionary dico = (NSDictionary) array.objectAtIndex(i);

					if (String.class.getName().equals(dico.objectForKey(key).getClass().getName()))
						total = total.add(new BigDecimal(StringCtrl.replace((String)dico.objectForKey(key), ",", ".")));
					else
						if (BigDecimal.class.getName().equals(dico.objectForKey(key).getClass().getName()))
							total = total.add((BigDecimal)dico.objectForKey(key));
						else
							total = total.add(new BigDecimal(dico.objectForKey(key).toString()));

				}
				else	{
					total = total.add((BigDecimal) ((EOEnterpriseObject)array.objectAtIndex(i)).valueForKey(key));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new BigDecimal(0.0);
				break;
			}
			i = i + 1;
		}

		return total.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public boolean hasFonction(String idFonction)	{
		return userFonctions.containsObject(idFonction);
	}

	public boolean fonctionParametrage()	{
		return userFonctions.containsObject(ConstantesCocktail.ID_FCT_PARAM);
	}

	public boolean fonctionCloture()	{
		return userFonctions.containsObject(ConstantesCocktail.ID_FCT_CLOTURE);
	}

	public boolean fonctionSaisie()	{
		return userFonctions.containsObject(ConstantesCocktail.ID_FCT_SAISIE);
	}


	/**
	 *
	 * @param message
	 * @return
	 */
	public String traiterMessageOracle(String message)	{

		NSArray array = NSArray.componentsSeparatedByString(message, "-20001: ");

		if (array.count() == 1)
			return message;

		NSArray mess = NSArray.componentsSeparatedByString((String)array.objectAtIndex(1), "\n");

		return (String)mess.objectAtIndex(0);
	}


	/**
	 *
	 * @param textField
	 */
	public void setMyDateTextField(JTextField textField)	{
		myDateTextField = textField;
	}


	/**
	 *
	 * @return
	 */
	public JTextField getMyDateTextField()	{
		return myDateTextField;
	}


	/**
	 * Affiche un datePicker
	 * @param parentWindow
	 * @param dateText
	 */
	public void showDatePickerPanel(Dialog parentWindow) {
		Date ladate=null;

		final JDialog datePickerDialog = new JDialog(parentWindow);

		if ((getMyDateTextField().getText()!=null) && ((getMyDateTextField().getText().length()>0)) ) {
			try {
				ladate = ((DateFormat)new SimpleDateFormat("dd/MM/yyyy")).parse(getMyDateTextField().getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
		}

		ZDatePickerPanel datePickerPanel = new ZDatePickerPanel();

		ComponentAdapter myComponentAdapter = new ComponentAdapter() {
			public void componentHidden(final ComponentEvent evt) {
				final Date dt = ((ZDatePickerPanel) evt.getSource()).getDate();
				if (null != dt) {
					getMyDateTextField().setText( new SimpleDateFormat("dd/MM/yyyy").format(dt) );
				}
				datePickerDialog.dispose();
			}
		};
		datePickerPanel.addComponentListener(myComponentAdapter);

		datePickerPanel.open(ladate);
		final Point p = getMyDateTextField().getLocationOnScreen();
		p.setLocation(p.getX(), p.getY() - 1 + myDateTextField.getSize().getHeight());

		//Ajouter la gestion de la touche echap pour fermer la fenetre
		KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
		Action escapeAction = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				datePickerDialog.hide();
			}
		};
		datePickerDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");
		datePickerDialog.getRootPane().getActionMap().put("ESCAPE", escapeAction);
		datePickerDialog.getRootPane().setFocusable(true);


		datePickerDialog.setResizable(false);
		datePickerDialog.setUndecorated(true);
		datePickerDialog.getContentPane().add(datePickerPanel);
		datePickerDialog.setLocation(p);
		datePickerDialog.pack();
		datePickerDialog.show();
	}

	public final String getIpAdress() {
		try {
			final String s = java.net.InetAddress.getLocalHost().getHostAddress();
			final String s2 = java.net.InetAddress.getLocalHost().getHostName() ;
			return s+" / " + s2;
		} catch (UnknownHostException e) {
			return "Machine inconnue";

		}
	}

	public String getHomeDir() {
		return homeDir;
	}
}