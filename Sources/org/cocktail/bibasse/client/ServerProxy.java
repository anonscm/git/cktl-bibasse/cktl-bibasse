package org.cocktail.bibasse.client;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/*
 * Created on 13 dec. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author cpinsard
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ServerProxy {


	/**
	 * Verification du login/mot de passe cote serveur
	 */
	public static NSDictionary clientSideRequestCheckPassword(EOEditingContext ec, String login, String password) {
        return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
        		ec, "session", "clientSideRequestCheckPassword",
        		new Class[] {String.class, String.class}, new Object[] {login, password}, false);
	}

	public static NSDictionary clientSideRequestCheckPasswordDirectConnect(EOEditingContext ec, String login, String password) {
        return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
        		ec, "session", "clientSideRequestCheckPasswordDirectConnect",
        		new Class[] {String.class, String.class}, new Object[] {login, password}, false);

	}

	public static NSDictionary clientSideRequestCheckPasswordCAS(EOEditingContext ec, String login) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
        		ec, "session", "clientSideRequestCheckPasswordCAS",
        		new Class[] {String.class}, new Object[] {login}, false);
	}

	/**
	 * Verification du login/mot de passe cote serveur
	 */
	public static Boolean clientSideRequestSetLoginParametres(EOEditingContext ec, String login, String ip) {
        return (Boolean)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestSetLoginParametres", new Class[] {String.class, String.class}, new Object[] {login, ip}, false);
	}


    /**
     * Recupere le parametre paramKey, en cherchant dans l'ordre: la table parametresTableName(), le fichier configFileName(), la table configTableName()
     * @param ec
     * @param paramKey
     * @return La valeur du parametre trouve (String) ou null si rien trouve
     */
    public static String clientSideRequestGetParam(EOEditingContext ec, String paramKey)  {
        return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestGetParam", new Class[] {String.class}, new Object[] {paramKey}, false);
    }


	/**
	 *
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public static final NSArray clientSideRequestSqlQuery(EOEditingContext ec, final String sql) {
		return clientSideRequestSqlQuery(ec, sql, null);
	}

	/**
	 *
	 * @param sql
	 * @param keys
	 * @return
	 * @throws Exception
	 */
	public static final NSArray clientSideRequestSqlQuery(EOEditingContext ec, final String sql, final NSArray keys) {
       return (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestSqlQuery", new Class[] {String.class, NSArray.class}, new Object[] {sql, keys}, false);
	}

	/**
	 * Recuperation d'une cle primaire
	 *
	 * @param eo Objet pour lequel on veut recuperer la cle primaire
	 *
	 * @return Retourne un dictionnaire contenant les cles primaires et leur valeur
	 */
	public  static NSDictionary clientSideRequestPrimaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) 	{
        return (NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestPrimaryKeyForObject", new Class[] {EOEnterpriseObject.class}, new Object[] {eo}, false);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestAddMasqueNature(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestAddMasqueNature", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestAddMasqueGestion(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestAddMasqueGestion", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestDelMasqueGestion(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestDelMasqueGestion", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestDelMasqueNature(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestDelMasqueNature", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestDupliquerMasque(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session",
				"clientSideRequestDupliquerMasque", new Class[] {NSDictionary.class}, new Object[] {parametres}, false);
	}

	/**
	 * Initialiser la repartition nature/lolf
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestInitialiserNatureLolf(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session",
				"clientSideRequestInitialiserNatureLolf", new Class[] {NSDictionary.class}, new Object[] {parametres}, false);
	}

	/**
	 * Cloturer la repartition nature/lolf par type de credit
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void clientSideRequestCloturerNatureLolf(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session",
				"clientSideRequestCloturerNatureLolf", new Class[] {NSDictionary.class}, new Object[] {parametres}, false);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestConsoliderBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session",
				"clientSideRequestConsoliderBudget", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestConsoliderTousBudgets(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestConsoliderTousBudgets", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestInitialiserBudgetDbm(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestInitialiserBudgetDbm", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestInitialiserBudgetReliquat(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestInitialiserBudgetReliquat", new Class[] {NSDictionary.class},new Object[] {parametres}, false);
	}

	public static void clientSideRequestInitialiserBudgetInitial(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestInitialiserBudgetInitial", new Class[] {NSDictionary.class},new Object[] {parametres}, false);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestViderBudgetOrgan(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestViderBudgetOrgan", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestDeverouillerLigneBudgetaire(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		System.out.println("ServerProxy.clientSideRequestValiderLigneBudgetaire() VALIDATION DU CR - \nPARAMETRES : " + parametres);
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session",
				"clientSideRequestDeverouillerLbud", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestValiderLigneBudgetaire(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		System.out.println("ServerProxy.clientSideRequestValiderLigneBudgetaire() VALIDATION DU CR - \nPARAMETRES : " + parametres);
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestValiderLbud",new Class[] {NSDictionary.class}, new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestControlerLigneBudgetaire(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		System.out.println("ServerProxy.clientSideRequestControlerLigneBudgetaire() PARAMETRES CONTROLE : " + parametres);
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestControlerLbud", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestCloturerLigneBudgetaire(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestCloturerLbud",new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestVoterBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestVoterBudget", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestRefuserBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestRefuserBudget", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestChangerSaisieBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestChangerSaisieBudget", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestSupprimerSaisieBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestSupprimerSaisieBudget", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Suppression d'un budget saisi.
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestSupprimerBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestSupprimerBudget", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestTraiterMouvement(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestTraiterMouvement", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestAnnulerMouvement(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestAnnulerMouvement", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}

	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestUpdateMontantsVotes(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestUpdateMontantsVotes", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static void clientSideRequestSetBudgetsVotesOrgan(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		System.out.println("ServerProxy.clientSideRequestSetBudgetsVotesOrgan() SET BDS_VOTES_ORGAN - \nPARAMETRES : " + parametres);
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec,"session",
				"clientSideRequestSetBudgetsVotesOrgan", new Class[] {NSDictionary.class},new Object[] {parametres},false);
	}


	/**
	 * Vote du budget
	 * Vote du budget. Insertion dans les tables BUDGET_VOTE_****
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public static BigDecimal clientSideRequestGetDisponible(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		BigDecimal disponible = (BigDecimal)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session","clientSideRequestGetDisponible",new Class[] {NSDictionary.class},new Object[] {parametres},false);
		return disponible;
	}

	   public static NSData clientSideRequestPrintAndWait(EOEditingContext ec, String report, String sql, NSDictionary parametres) {
	       return (NSData) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestPrintAndWait", new Class[] {String.class, String.class, NSDictionary.class}, new Object[] {report, sql, parametres}, false);
	   }
	   public static void clientSideRequestPrintByThread(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId, final Boolean printIfEmpty) {
	       ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestPrintByThread", new Class[] {String.class, String.class, NSDictionary.class, String.class, Boolean.class}, new Object[] {report, sql, parametres, customJasperId, printIfEmpty}, false);
	   }
	   public static void clientSideRequestPrintByThreadXls(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId) {
	       ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestPrintByThreadXls", new Class[] {String.class, String.class, NSDictionary.class, String.class}, new Object[] {report, sql, parametres, customJasperId}, false);
	   }

	   public static NSDictionary clientSideRequestGetPrintProgression(EOEditingContext ec) {
	       return (NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestGetPrintProgression", new Class[] {}, new Object[] {}, false);
	   }

	   public static void clientSideRequestPrintKillCurrentTask(EOEditingContext ec) {
	       ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestPrintKillCurrentTask", new Class[] {}, new Object[] {}, false);

	   }
	   public static NSData clientSideRequestPrintDifferedGetPdf(EOEditingContext ec) {
	       System.out.println("ServerProxy.clientSideRequestPrintDifferedGetPdf()");
	       return (NSData)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestPrintDifferedGetPdf", new Class[] {}, new Object[] {}, false);
	   }


	   /**
	    * @param ec
	    * @return Un tableau de NSDictionary stockant les infos sur les utilisateurs connectes.
	    */
	   public static NSArray clientSideRequestGetConnectedUsers(EOEditingContext ec) {
	        return (NSArray) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestGetConnectedUsers", new Class[] {}, new Object[] {}, false);
	   }

	public static String serverVersion(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestVersion", new Class[] {}, new Object[] {}, false);
	}

}

