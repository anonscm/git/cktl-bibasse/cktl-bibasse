package org.cocktail.bibasse.client;

import java.awt.Font;
import java.io.Serializable;

import com.webobjects.eocontrol.EOEditingContext;

public class Configuration implements Serializable {

	// TODO evolution : mettre un dictionnaire de style pour enlever les duplications de tailles de caractères.
	// La configuration d'une font devrait cotnenir le triplet [FontFamily, style, size]

	/** Version UID. */
	private static final long serialVersionUID = 1L;

	public static final String INFORMATION_FONT_DEFAULT = "Times";
	public static final String INFORMATION_FONT_KEY = "information.font";
	public static final int INFO_FILTRE_SIZE = 11;
	public static final int INFO_LABEL_SMALL_SIZE = 11;
	public static final int INFO_LABEL_SIZE = 12;
	public static final int INFO_TITRE_MENU_SIZE = 13;
	public static final int INFO_TITRE_TABLEAU_SIZE = 14;
	public static final int INFO_TITRE_SIZE = 14;

	private static final Configuration INSTANCE = new Configuration();

	public static final Configuration instance() {
		return INSTANCE;
	}

	private String informationFontName = null;

	private Configuration() {
	}

	public Font buildFont(EOEditingContext ec, int style, int size) {
		return new Font(informationFontName(ec), style, size);
	}

	public Font informationLabelSmallFont(EOEditingContext ec) {
		return buildFont(ec, Font.PLAIN, INFO_LABEL_SMALL_SIZE);
	}

	public Font informationLabelFont(EOEditingContext ec) {
		return buildFont(ec, Font.PLAIN, INFO_LABEL_SIZE);
	}

	public Font informationTitreFont(EOEditingContext ec) {
		return buildFont(ec, Font.PLAIN, INFO_TITRE_SIZE);
	}

	public Font informationTitreMenuFont(EOEditingContext ec) {
		return buildFont(ec, Font.PLAIN, INFO_TITRE_MENU_SIZE);
	}

	public Font informationTitreTableauFont(EOEditingContext ec) {
		return buildFont(ec, Font.BOLD, INFO_TITRE_TABLEAU_SIZE);
	}

	public Font informationFiltreFont(EOEditingContext ec) {
		return buildFont(ec, Font.PLAIN, INFO_FILTRE_SIZE);
	}

	private String informationFontName(EOEditingContext ec) {
		if (informationFontName == null) {
			loadInformationFontName(ec);
		}
		return informationFontName;
	}

	private void loadInformationFontName(EOEditingContext ec) {
		String infoFontName = ServerProxy.clientSideRequestGetParam(ec, INFORMATION_FONT_KEY);
		if (infoFontName == null || "".equals(infoFontName)) {
			infoFontName = INFORMATION_FONT_DEFAULT;
		}

		this.informationFontName = infoFontName;
	}

}
