package org.cocktail.bibasse.client.ventilations;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.finder.FinderOrgan;
import org.cocktail.bibasse.client.finder.FinderTypeCredit;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.finder.FinderTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.finder.FinderTypeSens;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeCredit;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOTypeSens;
import org.cocktail.bibasse.client.process.mouvement.ProcessMouvements;
import org.cocktail.bibasse.client.selectors.OrganSelectCtrl;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class Ventilations {


	private static Ventilations sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	protected	JFrame window;

	public JPanel filterPanel;
	public JComboBox popupEtab, popupUb, popupCr, popupEtat;
	public	JTextField informations, libelleVentilation;

	protected OrganSelectCtrl myOrganSelector;

	private OptionTableCellRenderer	monRendererColor = new OptionTableCellRenderer();
	private CellEditor myCellEditor = new CellEditor(new JTextField());

	protected JPanel viewTableOrigine, viewTableDestination;

	private EODisplayGroup eodDestination;
	private ZEOTable myEOTableDestination;
	private ZEOTableModel myTableModelDestination;
	private TableSorter myTableSorterDestination;

	private	JLabel totalVentilations, resteAVentiler;

	protected 	ListenerDestination myListenerDestination = new ListenerDestination();
	private		ListenerSaisieVentilations listenerSaisieVentilations = new ListenerSaisieVentilations();
	private		ListenerTypeCredit listenerTypeCredit = new ListenerTypeCredit();

	protected	JTextField disponible, sommeVentilation;
	protected	JComboBox typesCredit;

	protected	JCheckBox checkAfficherSousCrs, checkAfficherConventions;

	protected ActionClose 		actionClose = new ActionClose();
	protected ActionValidate 		actionValidate = new ActionValidate();
	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionSaisieVentilation actionSaisieVentilation = new ActionSaisieVentilation();
	protected ActionHistoVentilations actionHistoVentilations = new ActionHistoVentilations();

	NSArray budgetsGestion  = new NSArray();
	NSArray budgetsNature  = new NSArray();

	private EOOrgan currentOrganOrigine;
	private EOTypeCredit currentTypeCredit;

	/**
	 *
	 *
	 */
	public Ventilations(EOEditingContext editingContext)	{

		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		System.out.println("Ventilations.Ventilations() CONSTRUCTEUR !!!!!!!");

		if (myOrganSelector == null)
			myOrganSelector = new OrganSelectCtrl(ec, NSApp.getExerciceBudgetaire(), null, (NSArray)NSApp.getUserOrgans().valueForKey("organ"));

		// Notification de changement de ligne budgetaire
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("selectedOrganHasChanged",new Class[] {NSNotification.class}), "selectedOrganHasChanged",null);

		initGUI();

		gui_initView();
	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static Ventilations sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new Ventilations(editingContext);
		return sharedInstance;
	}

	/**
	 *
	 *
	 */
	private void gui_initView()	{
		// Font Label
		Font infoLabelSmallFont = Configuration.instance().informationLabelSmallFont(ec);

		// Main Window
		window = new JFrame();
		window.setTitle("Ventilations Budget " + NSApp.getExerciceBudgetaire().exeExercice());
		window.setSize(800,500);

		// Types de credit
		typesCredit = new JComboBox();

		NSArray liste = FinderTypeCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), null, "DEPENSE");

		for (int i=0;i<liste.count();i++)
			typesCredit.addItem(liste.objectAtIndex(i));

		currentTypeCredit = (EOTypeCredit)typesCredit.getItemAt(0);

		typesCredit.addActionListener(listenerTypeCredit);

		// Text Fields ==> Disponible et somme a ventiler
		disponible = new JTextField("");
		disponible.setFont(infoLabelSmallFont);
		disponible.setEditable(false);
		disponible.setBorder(BorderFactory.createEmptyBorder(2,5,4,5));
		disponible.setBackground(new Color(100, 100, 100));
		disponible.setForeground(new Color(117,255,232));
		disponible.setHorizontalAlignment(JTextField.CENTER);
		disponible.addActionListener(new ListenerDisponible());

		sommeVentilation = new JTextField("");
		sommeVentilation.setFont(infoLabelSmallFont);
		sommeVentilation.setEditable(true);
		sommeVentilation.setBorder(BorderFactory.createLineBorder(ConstantesCocktail.BG_COLOR_RED));
		sommeVentilation.setBackground(ConstantesCocktail.BG_COLOR_WHITE);
		sommeVentilation.setForeground(ConstantesCocktail.BG_COLOR_BLACK);
		sommeVentilation.setHorizontalAlignment(JTextField.CENTER);

		JLabel lblLibelleVentilation = new JLabel("Libellé Ventilation ");

		libelleVentilation = new JTextField("");
		libelleVentilation.setFont(infoLabelSmallFont);
		libelleVentilation.setEditable(true);
		libelleVentilation.setBorder(BorderFactory.createLineBorder(ConstantesCocktail.BG_COLOR_BLACK));
		libelleVentilation.setBackground(ConstantesCocktail.BG_COLOR_WHITE);
		libelleVentilation.setForeground(ConstantesCocktail.BG_COLOR_BLACK);
		libelleVentilation.setHorizontalAlignment(JTextField.CENTER);

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionValidate);
		arrayList.add(actionClose);
		JPanel panelClose = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 25));

		// CENTER ==> Types de credit, disponible et somme a ventiler
		JPanel panelCenter = new JPanel(new BorderLayout());
		panelCenter.setBorder(BorderFactory.createEmptyBorder(1,4,1,4));

		arrayList = new ArrayList();

		JLabel lblTypeCredit = new JLabel("Type de Crédit");
		lblTypeCredit.setBorder(BorderFactory.createEmptyBorder());
		lblTypeCredit.setFocusable(false);
		lblTypeCredit.setHorizontalAlignment(JTextField.CENTER);

		JPanel panelTypeCredit = new JPanel(new BorderLayout());
		panelTypeCredit.setBorder(BorderFactory.createEmptyBorder(20,2,5,2));
		arrayList.add(lblTypeCredit);
		arrayList.add(typesCredit);
		panelTypeCredit.add(ZUiUtil.buildBoxColumn(arrayList));

		arrayList= new ArrayList();

		JTextField lblDispo = new JTextField("Disponible");
		lblDispo.setBorder(BorderFactory.createEmptyBorder());
		lblDispo.setEditable(false);
		lblDispo.setFocusable(false);
		lblDispo.setHorizontalAlignment(JTextField.CENTER);

		JPanel panelDisponible = new JPanel(new BorderLayout());
		panelDisponible.setBorder(BorderFactory.createEmptyBorder(5,2,10,2));

		arrayList.add(lblDispo);
		arrayList.add(disponible);

		panelDisponible.add(ZUiUtil.buildBoxColumn(arrayList));

		arrayList= new ArrayList();

		arrayList.add(panelTypeCredit);
		arrayList.add(panelDisponible);

		// Affichage des Sous Crs
		checkAfficherSousCrs = new JCheckBox("Sous-CRs  ");
		checkAfficherSousCrs.addActionListener(new CheckBoxVentilationRapideListener());
		JPanel panelVentilationRapide = new JPanel(new FlowLayout());
		panelVentilationRapide.add(checkAfficherSousCrs, BorderLayout.WEST);
		panelVentilationRapide.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		panelVentilationRapide.setBorder(BorderFactory.createEmptyBorder(20, 0,5,0));

		arrayList.add(panelVentilationRapide);

		checkAfficherConventions = new JCheckBox("Conventions");
		checkAfficherConventions.addActionListener(new CheckBoxConventionsListener());
		JPanel panelConvention = new JPanel(new FlowLayout());
		panelConvention.add(checkAfficherConventions, BorderLayout.WEST);
		panelConvention.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		panelConvention.setBorder(BorderFactory.createEmptyBorder(0, 0,5,0));

		arrayList.add(panelConvention);

		Component panelTemp = ZUiUtil.buildBoxColumn(arrayList);

		JPanel tempoPanel = new JPanel(new BorderLayout());
		tempoPanel.add(panelTemp, BorderLayout.NORTH);

		panelCenter.add(new JPanel(new BorderLayout()), BorderLayout.NORTH);
		panelCenter.add(tempoPanel, BorderLayout.CENTER);
		panelCenter.add(new JPanel(new BorderLayout()), BorderLayout.SOUTH);

		// SOUTH
		informations = new JTextField("Veuillez sélectionner une ligne budgétaire (Organigramme)");
		informations.setFont(infoLabelSmallFont);
		informations.setEditable(false);
		informations.setBorder(BorderFactory.createEmptyBorder(2,5,4,5));
		informations.setBackground(new Color(100, 100, 100));
		informations.setForeground(new Color(117,255,232));
		informations.setHorizontalAlignment(JTextField.CENTER);

		JPanel panelInfos = new JPanel(new BorderLayout());
		panelInfos.add(new JSeparator(), BorderLayout.NORTH);
		panelInfos.add(informations, BorderLayout.CENTER);
		panelInfos.add(new JSeparator(), BorderLayout.SOUTH);

		arrayList = new ArrayList();
		arrayList.add(actionHistoVentilations);

		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(panelInfos, BorderLayout.NORTH);
		panelSouth.add(panelClose, BorderLayout.EAST);
		//panelSouth.add(panelLeftButtons, BorderLayout.WEST);

		actionHistoVentilations.setEnabled(false);

		JPanel panelRight = new JPanel(new BorderLayout());
		totalVentilations = new JLabel("0,00");
		totalVentilations.setForeground(new Color(0,0,255));
		totalVentilations.setFocusable(false);
		totalVentilations.setBorder(BorderFactory.createEmptyBorder());

		resteAVentiler = new JLabel("0,00");
		resteAVentiler.setForeground(new Color(0,0,255));
		resteAVentiler.setFocusable(false);
		resteAVentiler.setBorder(BorderFactory.createEmptyBorder());

		JLabel labelTotalVentilations = new JLabel("TOTAL VENTILATIONS : ");
		JLabel labelEuro = new JLabel(" \u20ac");
		labelEuro.setForeground(new Color(0,0,0));

		JLabel labelEuro2 = new JLabel(" \u20ac");
		labelEuro2.setForeground(new Color(0,0,0));

		JLabel lblResteAVentiler = new JLabel("RESTE à ventiler : ");
		lblResteAVentiler.setBorder(BorderFactory.createEmptyBorder());
		lblResteAVentiler.setFocusable(false);
		lblResteAVentiler.setHorizontalAlignment(JTextField.CENTER);

		labelTotalVentilations.setForeground(new Color(0,0,0));

		JPanel panelLibelleVentilation = new JPanel(new BorderLayout());
		panelLibelleVentilation.setBorder(BorderFactory.createEmptyBorder());
		panelLibelleVentilation.add(lblLibelleVentilation, BorderLayout.WEST);
		panelLibelleVentilation.add(libelleVentilation, BorderLayout.CENTER);

		JPanel panelResteAVentiler = new JPanel(new FlowLayout());
		panelResteAVentiler.add(lblResteAVentiler);
		panelResteAVentiler.add(resteAVentiler);
		panelResteAVentiler.add(labelEuro2);
		panelResteAVentiler.setBorder(BorderFactory.createEmptyBorder());

		JPanel panelTotalVentilations = new JPanel(new FlowLayout());
		panelTotalVentilations.add(labelTotalVentilations);
		panelTotalVentilations.add(totalVentilations);
		panelTotalVentilations.add(labelEuro);
		panelTotalVentilations.setBorder(BorderFactory.createEmptyBorder());

		JPanel panelTempo = new JPanel(new BorderLayout());
		panelTempo.add(panelResteAVentiler, BorderLayout.WEST);
		panelTempo.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		panelTempo.add(panelTotalVentilations, BorderLayout.EAST);

		arrayList = new ArrayList();
		arrayList.add(panelLibelleVentilation);
		arrayList.add(panelTempo);

		panelRight.add(viewTableDestination, BorderLayout.CENTER);
		panelRight.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.SOUTH);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.add(myOrganSelector.mainPanel(), BorderLayout.WEST);
		mainView.add(panelCenter, BorderLayout.CENTER);
		mainView.add(panelRight, BorderLayout.EAST);
		mainView.add(panelSouth, BorderLayout.SOUTH);

		window.setContentPane(mainView);
		window.pack();

		ZUiUtil.centerWindow(window);
	}


	/**
	 *
	 */
	public void exerciceHasChanged() {

		System.out.println("Ventilations.exerciceHasChanged()");

		eodDestination.setObjectArray(new NSArray());
		myEOTableDestination.updateData();

		window.setTitle("Ventilations Budget " + NSApp.getExerciceBudgetaire().exeExercice());

		window.getContentPane().remove(myOrganSelector.mainPanel());
		window.getContentPane().repaint();
		window.repaint();

		myOrganSelector =
			new OrganSelectCtrl(ec, NSApp.getExerciceBudgetaire(), null, (NSArray)NSApp.getUserOrgans().valueForKey("organ"));

		window.getContentPane().add(myOrganSelector.mainPanel(), BorderLayout.WEST);
		window.getContentPane().repaint();
		window.repaint();

		refreshTypesCredit();

		if (checkAfficherSousCrs.isSelected())
			setModeVentilationRapide();
		else
			setModeSaisie();

		setDisponible();
	}

	/**
	 *
	 */
	public void refreshTypesCredit()	{

		NSArray liste = FinderTypeCredit.findTypesCredit(ec, NSApp.getExerciceBudgetaire(), null, "DEPENSE");

		typesCredit.removeActionListener(listenerTypeCredit);
		typesCredit.removeAllItems();

		for (int i=0;i<liste.count();i++)
			typesCredit.addItem(liste.objectAtIndex(i));

		currentTypeCredit = (EOTypeCredit)typesCredit.getItemAt(0);

		typesCredit.addActionListener(listenerTypeCredit);

	}


	/**
	 *
	 *
	 */
	public void open()	{

		libelleVentilation.setText("");
		updateUI();
		window.show();

		updateUI();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

		eodDestination = new EODisplayGroup();

		NSMutableArray sort = new NSMutableArray();
		sort.addObject(new EOSortOrdering("UB", EOSortOrdering.CompareAscending));
		sort.addObject(new EOSortOrdering("CR", EOSortOrdering.CompareAscending));
		sort.addObject(new EOSortOrdering("SOUS_CR", EOSortOrdering.CompareAscending));

		viewTableDestination = new JPanel();

		initTableModel();
		initTable();

		myEOTableDestination.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableDestination.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableDestination.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		viewTableDestination.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableDestination.removeAll();
		viewTableDestination.setLayout(new BorderLayout());
		viewTableDestination.add(new JScrollPane(myEOTableDestination), BorderLayout.CENTER);

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTableDestination = new ZEOTable(myTableSorterDestination);
		myEOTableDestination.addListener(new ListenerDestination());
		myTableModelDestination.addTableModelListener(listenerSaisieVentilations);
//		myTableSorterDestination.setTableHeader(myEOTableDestination.getTableHeader());

	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		Vector myCols = new Vector();

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eodDestination, "UB", "UB", 30);
		col1.setAlignment(SwingConstants.LEFT);
		col1.setTableCellRenderer(monRendererColor);
		myCols.add(col1);

		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eodDestination, "CR", "CR", 60);
		col2.setAlignment(SwingConstants.LEFT);
		col2.setTableCellRenderer(monRendererColor);
		myCols.add(col2);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(eodDestination, "SOUS_CR", "SCR", 60);
		col3.setAlignment(SwingConstants.LEFT);
		col3.setTableCellRenderer(monRendererColor);
		myCols.add(col3);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(eodDestination, "DISPO", "Dispo", 50);
		col4.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setTableCellRenderer(monRendererColor);
		myCols.add(col4);

		ZEOTableModelColumn col5 = new ZEOTableModelColumn(eodDestination, "MONTANT", "Montant", 60);
		col5.setAlignment(SwingConstants.RIGHT);
		col5.setFormatEdit(ConstantesCocktail.FORMAT_DECIMAL);
		col5.setFormatDisplay(ConstantesCocktail.FORMAT_DECIMAL);
		col5.setColumnClass(BigDecimal.class);
		col5.setTableCellEditor(myCellEditor);
		col5.setEditable(true);
		col5.setTableCellRenderer(monRendererColor);
		myCols.add(col5);

		myTableModelDestination = new ZEOTableModel(eodDestination, myCols);
		myTableSorterDestination = new TableSorter(myTableModelDestination);


	}

	/**
	 * Classe d'ecoute pour les doubleclics dans la table pays
	 */
	public class ListenerDisponible implements ActionListener 	{
		public void actionPerformed(ActionEvent e) 	{
			setModeSaisie();
		}
	}

	/**
	 * Listener sur le deuxieme niveau de l'arborescence budgetaire
	 * Mise à jour du troisieme niveau si deuxieme niveau selectionne
	 */
	private class ListenerDestination implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

		}
	}

	/**
	 *
	 *
	 */
	public void setResteAVentiler()	{

		BigDecimal total = new BigDecimal(0.0);

		for (int i=0;i<eodDestination.displayedObjects().count();i++)	{

			NSDictionary dico = (NSDictionary)eodDestination.displayedObjects().objectAtIndex(i);
			total = total.add(new BigDecimal(dico.objectForKey("MONTANT").toString()));
		}

		totalVentilations.setText(total.toString());

		try {
			BigDecimal dispo = new BigDecimal(disponible.getText());
			resteAVentiler.setText((dispo.subtract(total)).toString());
		}
		catch (Exception ex)	{
			resteAVentiler.setText("0");
		}

	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public class ListenerSaisieVentilations implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {

			setResteAVentiler();

		}
	}




	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionHistoVentilations extends AbstractAction {

		public ActionHistoVentilations() {
			super("Historique des ventilations");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
		}

		public void actionPerformed(ActionEvent e) {

			VentilationsHistorique.sharedInstance(ec, window).openHistorique(currentOrganOrigine);

		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {

		public ActionCancel() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
		}

		public void actionPerformed(ActionEvent e) {
			setModeSaisie();
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
		}

		public void actionPerformed(ActionEvent e) {
			window.hide();
		}
	}


	/**
	 *
	 *
	 */
	public void setModeVentilationRapide()	{

		NSApp.setWaitCursor(window);

		if (currentOrganOrigine == null)
			return;

		String sqlQualifier = "";
		Number utlOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
		Number tcdOrdre = (Number) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentTypeCredit).objectForKey("tcdOrdre");

		if (currentOrganOrigine.orgNiveau().intValue() == 2)	{
			sqlQualifier =
				" SELECT o.org_id ORG_ID, o.org_ub UB, o.org_cr CR, nvl(o.org_souscr, ' ') SOUS_CR, " +
				" jefy_budget.budget_utilitaires.get_disponible(o.org_id, "+tcdOrdre+" , "+NSApp.getExerciceBudgetaire().exeExercice()+") DISPO, 0 MONTANT " +
				" FROM jefy_admin.v_organ o, jefy_admin.utilisateur_organ u " +
				" WHERE o.exe_ordre = " + NSApp.getExerciceBudgetaire().exeExercice() +
				" AND o.org_ub = '" + currentOrganOrigine.orgUb() + "'" +
				" AND o.org_id = u.org_id " +
				" AND u.utl_ordre = " + utlOrdre.intValue() + " and o.org_niv > 2" +
				" ORDER BY o.org_ub, o.org_cr, o.org_niv, o.org_souscr";
		}
		else	{
			sqlQualifier =
				" SELECT o.org_id ORG_ID, o.org_ub UB, o.org_cr CR, nvl(o.org_souscr, ' ') SOUS_CR, " +
				" jefy_budget.budget_utilitaires.get_disponible(o.org_id, "+tcdOrdre+", "+NSApp.getExerciceBudgetaire().exeExercice()+") DISPO, 0 MONTANT " +
				" FROM jefy_admin.v_organ o, jefy_admin.utilisateur_organ u " +
				" WHERE o.exe_ordre = " + NSApp.getExerciceBudgetaire().exeExercice() +
				" AND o.org_ub = '" + currentOrganOrigine.orgUb() + "'" +
				" AND o.org_cr = '" + currentOrganOrigine.orgCr() + "'" +
				" AND o.org_id = u.org_id " +
				" AND u.utl_ordre = " + utlOrdre.intValue() + " and o.org_niv > 2" +
				" ORDER BY o.org_ub, o.org_cr, o.org_niv, o.org_souscr";
		}

		System.out.println("Ventilations.setModeVentilationRapide() SQLQUALIFER :" + sqlQualifier);

		NSMutableArray arrayOrgans = new NSMutableArray();
		NSMutableDictionary dicoOrgan = new NSMutableDictionary();
		NSArray organs = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);

		for (int i=0;i<organs.count();i++)	{

			NSDictionary dico = (NSDictionary)organs.objectAtIndex(i);

			dicoOrgan = new NSMutableDictionary();
			dicoOrgan.setObjectForKey(dico.objectForKey("ORG_ID"),"ORG_ID");
			dicoOrgan.setObjectForKey(dico.objectForKey("UB").toString(),"UB");
			dicoOrgan.setObjectForKey(dico.objectForKey("CR").toString(),"CR");
			dicoOrgan.setObjectForKey(dico.objectForKey("SOUS_CR").toString(),"SOUS_CR");
			dicoOrgan.setObjectForKey(new BigDecimal(dico.objectForKey("DISPO").toString()), "DISPO");
			dicoOrgan.setObjectForKey(new BigDecimal(0.0),"MONTANT");

			arrayOrgans.addObject(dicoOrgan);
		}

		eodDestination.setObjectArray(arrayOrgans);
		myEOTableDestination.updateData();

		NSApp.setDefaultCursor(window);

	}

	/**
	 *
	 *
	 */
	public void setModeSaisie()	{

		NSApp.setWaitCursor(window);

		if (currentOrganOrigine == null)
			return;

		String sqlQualifier = "";
		Number orgId = (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentOrganOrigine).objectForKey("orgId");
		Number utlOrdre = (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, NSApp.getUtilisateur()).objectForKey("utlOrdre");
		Number tcdOrdre = (Number)ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentTypeCredit).objectForKey("tcdOrdre");

		sqlQualifier =
			" SELECT o.org_id ORG_ID, o.org_ub UB, o.org_cr CR, nvl(o.org_souscr, ' ') SOUS_CR, " +
			" jefy_budget.budget_utilitaires.get_disponible(o.org_id, "+tcdOrdre+" , "+NSApp.getExerciceBudgetaire().exeExercice() + ") DISPO, 0 MONTANT " +
			" FROM jefy_admin.v_organ o, jefy_admin.utilisateur_organ u " +
			" WHERE o.exe_ordre = " + NSApp.getExerciceBudgetaire().exeExercice() +
			" AND o.org_pere = '" + orgId + "'" +
			" AND o.org_id = u.org_id " +
			" AND u.utl_ordre = " + utlOrdre.intValue() + " and o.org_niv > 2" +
			" ORDER BY o.org_ub, o.org_cr, o.org_niv, o.org_souscr";

		NSMutableArray arrayOrgans = new NSMutableArray();
		NSMutableDictionary dicoOrgan = new NSMutableDictionary();
		NSArray organs = ServerProxy.clientSideRequestSqlQuery(ec, sqlQualifier);

		for (int i=0;i<organs.count();i++)	{

			NSDictionary dico = (NSDictionary)organs.objectAtIndex(i);

			dicoOrgan = new NSMutableDictionary();
			dicoOrgan.setObjectForKey(dico.objectForKey("ORG_ID"),"ORG_ID");
			dicoOrgan.setObjectForKey(dico.objectForKey("UB").toString(),"UB");
			dicoOrgan.setObjectForKey(dico.objectForKey("CR").toString(),"CR");
			dicoOrgan.setObjectForKey(dico.objectForKey("SOUS_CR").toString(),"SOUS_CR");
			dicoOrgan.setObjectForKey(new BigDecimal(dico.objectForKey("DISPO").toString()), "DISPO");
			dicoOrgan.setObjectForKey(new BigDecimal(0.0),"MONTANT");

			arrayOrgans.addObject(dicoOrgan);
		}

		eodDestination.setObjectArray(arrayOrgans);
		myEOTableDestination.updateData();

		NSApp.setDefaultCursor(window);

	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionSaisieVentilation extends AbstractAction {

		public ActionSaisieVentilation() {
			super(null);
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
		}


		public void actionPerformed(ActionEvent e) {

			setModeSaisie();

		}

	}


	/**
	 *
	 * @return
	 */
	public boolean testSaisieValide()	{

		BigDecimal montantAVentiler = new BigDecimal(totalVentilations.getText());

		if (StringCtrl.chaineVide(libelleVentilation.getText()))	{

			EODialogs.runInformationDialog("ERREUR","Vous devez saisir un libellé pour cette ventilation !");
			return false;

		}

		// Si le budget a ete saisi au niveau 3 (CR) pas de remontees de credits possibles
		if (NSApp.isSaisieCr() && currentOrganOrigine.orgNiveau().intValue() < 3)	{

			if (montantAVentiler.floatValue() < 0 || montantAVentiler.floatValue() > 0)	{

				EODialogs.runInformationDialog("ERREUR", "Le budget a été saisi au niveau 3 (CR), la somme des ventilations à effectuer doit donc être nulle !");
				return false;

			}
		}
		else	{

			// Total des Ventilations > 0, on debite le disponible de l'UB ou du CR
			// On teste alors si le dispo est suffisant
			BigDecimal montantDisponible = new BigDecimal(disponible.getText());
			if (montantAVentiler.floatValue() > 0)	{
				if (montantDisponible.floatValue() < montantAVentiler.floatValue())	{

					EODialogs.runInformationDialog("ERREUR", "Disponible insuffisant au niveau de l'UB ou du CR !");
					return false;
				}
			}
		}

		return true;
	}

	/**
	 *
	 *
	 */
	public void ventilerCredits()	{

		EOBudgetMouvements currentMouvement = null;

		// Verifier que toutes les sommes ne soient pas a 0
		if (!testSaisieValide())
			return;

		try {

			System.out.println("Ventilations.ventilerCredits() CREATION MOUVEMENT");

			EOTypeSens typeSensDebit = FinderTypeSens.findTypeSens(ec, EOTypeSens.TYPE_SENS_DEBIT);
			EOTypeSens typeSensCredit = FinderTypeSens.findTypeSens(ec, EOTypeSens.TYPE_SENS_CREDIT);

			// Creation d'un mouvement
			ProcessMouvements myProcessMouvements = null;

			myProcessMouvements = new ProcessMouvements(false);

			currentMouvement = myProcessMouvements.creerMouvement(ec,
					NSApp.getUtilisateur(),
					FinderTypeMouvementBudgetaire.findTypeMouvement(ec, EOTypeMouvementBudgetaire.MOUVEMENT_VENTILATION),
					NSApp.getExerciceBudgetaire(),
					FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
					StringCtrl.chaineVide(libelleVentilation.getText())?"VENTILATION":libelleVentilation.getText(),
							new NSTimestamp());

			// Creation des lignes de ventilation

			for (int i=0;i<eodDestination.displayedObjects().count();i++)	{

				NSDictionary dico = (NSDictionary)eodDestination.displayedObjects().objectAtIndex(i);

//				EOOrgan organ = (EOOrgan)dico.objectForKey("EO_ORGAN");

				BigDecimal montant = new BigDecimal(dico.objectForKey("MONTANT").toString());
				BigDecimal dispo = new BigDecimal(dico.objectForKey("DISPO").toString());

				if (!montant.equals(new BigDecimal(0.0)))	{

					EOOrgan organ = FinderOrgan.findOrganForKey(ec, (Number)dico.objectForKey("ORG_ID"));

					// On regarde qu'il y ait bien assez de dispo sur le CR pour effectuer la ventilation
					if (montant.floatValue() < 0 && (montant.multiply(new BigDecimal(-1))).floatValue() > dispo.floatValue())
						throw new ValidationException("Manque de disponible sur le CR " + organ.orgCr() + " !");

					myProcessMouvements.creerUneLigneEOBudgetMouvCredit(
							ec,
							(montant.floatValue() > 0)?typeSensCredit:typeSensDebit,
									currentTypeCredit,
									organ,
									currentMouvement,
									NSApp.getExerciceBudgetaire(),
									NSApp.getUtilisateur(),
									FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
									montant
					);
				}
			}

			// Suivant le signe du total des ventilations, on debite ou on credite le disponible de l'UB
			if (new BigDecimal(totalVentilations.getText()).floatValue() < 0 ||
					new BigDecimal(totalVentilations.getText()).floatValue() > 0)	{

				BigDecimal montant = new BigDecimal(totalVentilations.getText()).multiply(new BigDecimal(-1));

				myProcessMouvements.creerUneLigneEOBudgetMouvCredit(
						ec,
						(montant.floatValue() > 0)?typeSensCredit:typeSensDebit,
								currentTypeCredit,
								currentOrganOrigine,
								currentMouvement,
								NSApp.getExerciceBudgetaire(),
								NSApp.getUtilisateur(),
								FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
								montant
				);
			}

			ec.saveChanges();

			// Lancement de la procedure stockee
			NSMutableDictionary parametres = new NSMutableDictionary();

			parametres.setObjectForKey((EOEnterpriseObject)currentMouvement, "EOBudgetMouvement");
			ServerProxy.clientSideRequestTraiterMouvement(ec, parametres);

		}
		catch (ValidationException e)	{
			ec.revert();
			EODialogs.runErrorDialog("ERREUR","Opération annulée ! \n" + e.getMessage());
			return;
		}
		catch (Exception e)	{

			// Lancement de la procedure stockee
			try {
				System.out.println("Ventilations.ventilerCredits() ANNULATION DES MOUVEMENTS CREES !!!!!");
				NSMutableDictionary parametres = new NSMutableDictionary();

				parametres.setObjectForKey((EOEnterpriseObject)currentMouvement, "EOBudgetMouvement");
				ServerProxy.clientSideRequestAnnulerMouvement(ec, parametres);
			}
			catch (Exception ex)	{
				e.printStackTrace();
			}

			EODialogs.runErrorDialog("ERREUR","Opération annulée ! \n" + NSApp.traiterMessageOracle(e.getMessage()));
			ec.revert();
			return;
		}

		if (checkAfficherSousCrs.isSelected())
			setModeVentilationRapide();
		else
			setModeSaisie();

		setDisponible();

		setResteAVentiler();

		libelleVentilation.setText("");

		EODialogs.runInformationDialog("VENTILATION", "Les ventilations ont bien été effectuées !");

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionValidate extends AbstractAction {

		public ActionValidate() {

			super("Ventiler");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALIDE_BUDGET);

		}

		public void actionPerformed(ActionEvent e) {

			ventilerCredits();

		}

	}


	/**
	 *
	 * @param notif
	 */
	public void selectedOrganHasChanged(NSNotification notif)	{

		NSApp.setWaitCursor(window);

		currentOrganOrigine = (EOOrgan)notif.object();

		if (currentOrganOrigine != null)	{

			if (currentOrganOrigine.orgNiveau().intValue() < 2)	{

				eodDestination.setObjectArray(new NSArray());
				myEOTableDestination.updateData();
				checkAfficherSousCrs.setEnabled(false);

			}
			else	{

				checkAfficherSousCrs.setEnabled(true);

				if (checkAfficherSousCrs.isSelected())
					setModeVentilationRapide();
				else
					setModeSaisie();
			}

			actionSaisieVentilation.setEnabled(true);
			setDisponible();
			informations.setText("");
		}
		else	{
			actionSaisieVentilation.setEnabled(false);
			eodDestination.setObjectArray(new NSArray());
			myEOTableDestination.updateData();
		}

		listenerSaisieVentilations.tableChanged(null);

		updateUI();

		NSApp.setDefaultCursor(window);

	}


	/**
	 *
	 *
	 */
	public void typeCreditHasChanged()	{

		currentTypeCredit = (EOTypeCredit)typesCredit.getSelectedItem();

		if (checkAfficherSousCrs.isSelected())
			setModeVentilationRapide();
		else
			setModeSaisie();

		setDisponible();

		setResteAVentiler();
	}


	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param typeCredit
	 * @return
	 */
	public BigDecimal getDisponible(EOOrgan organ,  EOTypeCredit typeCredit, EOExercice exercice)	{

		try {
			NSMutableDictionary parametres = new NSMutableDictionary();
			parametres.setObjectForKey((EOEnterpriseObject)organ,"EOOrgan");
			parametres.setObjectForKey((EOEnterpriseObject)typeCredit,"EOTypeCredit");
			parametres.setObjectForKey((EOEnterpriseObject)exercice,"EOExercice");

			return ServerProxy.clientSideRequestGetDisponible(ec, parametres);

		}
		catch (Exception e) {

			return new BigDecimal(0.0);
		}

	}

	/**
	 *
	 *
	 */
	public void setDisponible()	{

		try {

			System.out.println("Ventilations.setDisponible()DISPO PROC : " + getDisponible(currentOrganOrigine, currentTypeCredit, NSApp.getExerciceBudgetaire()));

			BigDecimal dispo = getDisponible(currentOrganOrigine, currentTypeCredit, NSApp.getExerciceBudgetaire()).setScale(2, BigDecimal.ROUND_HALF_UP);
			System.out.println("Ventilations.setDisponible() DISPO : " + dispo);
			disponible.setText(dispo.toString());

		}
		catch (Exception e) {

			disponible.setText("ERREUR");
		}

	}

	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise à jour du deuxieme niveau si premier niveau selectionne
	 */
	private class ListenerTypeCredit implements ActionListener {

		public ListenerTypeCredit() {super();}
		public void actionPerformed(ActionEvent anAction) {
			typeCreditHasChanged();
		}
	}


	/**
	 *
	 *
	 */
	public void updateUI()	{

		actionHistoVentilations.setEnabled(currentOrganOrigine != null);

	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class OptionTableCellRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_SAISIE=new Color(240,240,240);
		public final Color COULEUR_TEXTE_SAISIE=new Color(0,0,0);

		public final Color COULEUR_FOND_INACTIF = new Color(200,200,200);
		public final Color COULEUR_TEXTE_INACTIF =new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (currentOrganOrigine != null && (column < 4
					|| currentOrganOrigine.orgNiveau().intValue() < 2
					|| (NSApp.isSaisieCr() && currentOrganOrigine.orgNiveau().intValue() == 2)))	{
				leComposant.setBackground(COULEUR_FOND_INACTIF);
				leComposant.setForeground(COULEUR_TEXTE_INACTIF);
			}
			else	{
				leComposant.setBackground(COULEUR_FOND_SAISIE);
				leComposant.setForeground(COULEUR_TEXTE_SAISIE);
			}

			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class CellEditor extends ZEOTableModelColumn.ZEONumFieldTableCellEditor {

		private  JTextField myTextField;

		public CellEditor(JTextField textField) {
			super(textField, ConstantesCocktail.FORMAT_DECIMAL);
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			myTextField = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);

			myTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
			myTextField.setEditable(true);

			return myTextField;
		}
	}


	/**
	 * Class listener du checkBox temVerfie. Mise a jour du currentPrepa pour dire si la paye est verifiee ou non
	 */
	private class CheckBoxVentilationRapideListener extends AbstractAction {
		public CheckBoxVentilationRapideListener () 	{super();}

		public void actionPerformed(ActionEvent anAction)	{

			if (checkAfficherSousCrs.isSelected())
				setModeVentilationRapide();
			else
				setModeSaisie();
		}
	}


	/**
	 * Class listener du checkBox temVerfie. Mise a jour du currentPrepa pour dire si la paye est verifiee ou non
	 */
	private class CheckBoxConventionsListener extends AbstractAction {
		public CheckBoxConventionsListener () 	{super();}

		public void actionPerformed(ActionEvent anAction)	{

			if (checkAfficherConventions.isSelected())	{

			}
			else	{

			}

		}

	}
}
