package org.cocktail.bibasse.client.ventilations;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.Configuration;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.editions.EditionsCtrl;
import org.cocktail.bibasse.client.finder.FinderBudgetMouvCredit;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.finder.FinderTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.metier.EOBudgetMouvCredit;
import org.cocktail.bibasse.client.metier.EOBudgetMouvements;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.metier.EOTypeMouvementBudgetaire;
import org.cocktail.bibasse.client.utils.CocktailUtilities;
import org.cocktail.bibasse.client.utils.DateCtrl;
import org.cocktail.bibasse.client.utils.StringCtrl;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

public class VentilationsHistorique  {

	private static VentilationsHistorique sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	private EODisplayGroup eodMouvements, eodVentilations;
	private ZEOTable myEOTableMouvements, myEOTableVentilations;
	private ZEOTableModel myTableModelMouvements, myTableModelVentilations;
	private TableSorter myTableSorterMouvements, myTableSorterVentilations;

	protected ActionClose actionClose = new ActionClose();
	protected ActionPrint actionPrint = new ActionPrint();

	protected ActionFind actionFind = new ActionFind();
	protected ActionSelectAll actionSelectAll = new ActionSelectAll();

	protected ActionGetDateDebut actionGetDateDebut = new ActionGetDateDebut();
	protected ActionGetDateFin actionGetDateFin = new ActionGetDateFin();

	protected ActionDelDateDebut actionDelDateDebut = new ActionDelDateDebut();
	protected ActionDelDateFin actionDelDateFin = new ActionDelDateFin();

	//	protected ActionFind actionFind = new ActionFind();

	private JTextField	filtreDateDebut, filtreDateFin, filtreLibelle;
	private JButton		buttonGetDateDebut, buttonGetDateFin, buttonFind, buttonDelDateDebut, buttonDelDateFin, buttonSelectAll;

	protected JPanel viewTableMouvements, viewTableVentilations;

	private	EOOrgan currentOrgan;
	private	EOBudgetMouvements currentMouvement;

	/**
	 *
	 *
	 */
	public VentilationsHistorique(EOEditingContext editingContext, JFrame frame)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		mainFrame = frame;

		initGUI();
		initView();

	}


	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static VentilationsHistorique sharedInstance(EOEditingContext editingContext, JFrame frame)	{
		if (sharedInstance == null)
			sharedInstance = new VentilationsHistorique(editingContext, frame);
		return sharedInstance;
	}


	/**
	 *
	 *
	 */
	private void initTextFields()	{

		filtreDateDebut = new JTextField("");
        filtreDateDebut.setColumns(8);
        filtreDateDebut.setHorizontalAlignment(JTextField.CENTER);
        CocktailUtilities.initTextField(filtreDateDebut, false, true);
        filtreDateDebut.addFocusListener(new ListenerTextFieldDebut());
        filtreDateDebut.addActionListener(new ActionListenerDebut());

        filtreDateFin = new JTextField("");
        filtreDateFin.setColumns(8);
        filtreDateFin.setHorizontalAlignment(JTextField.CENTER);
        CocktailUtilities.initTextField(filtreDateFin, false, true);
        filtreDateFin.addFocusListener(new ListenerTextFieldFin());
        filtreDateFin.addActionListener(new ActionListenerFin());

        filtreLibelle = new JTextField("");
        filtreLibelle.setColumns(15);

	}


	/**
	 *
	 * @return
	 */
	private JPanel buildSouthPanel()	{

		JPanel panel = new JPanel(new BorderLayout());

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionPrint);
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 22));

		panel.add(new JSeparator(), BorderLayout.NORTH);
		panel.add(panelButtons, BorderLayout.EAST);

		return panel;
	}

	/**
	 *
	 *
	 */
	public void initView()	{

        mainWindow = new JDialog(mainFrame, "Historique des Ventilations", true);

        viewTableMouvements.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

        initButtons();

        initTextFields();

		ArrayList arrayList = new ArrayList();

		arrayList = new ArrayList();
		arrayList.add(viewTableVentilations);

		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.setBorder(BorderFactory.createEmptyBorder(0,0,0,2));
		centerPanel.add(new JLabel("Détail Mouvement"), BorderLayout.NORTH);
		centerPanel.add(ZUiUtil.buildBoxColumn(arrayList));

		JPanel westPanel = new JPanel(new BorderLayout());
		westPanel.setBorder(BorderFactory.createEmptyBorder(0,2,0,2));
		westPanel.add(new JLabel("Mouvements"), BorderLayout.NORTH);
		westPanel.add(viewTableMouvements, BorderLayout.CENTER);
		westPanel.add(buttonSelectAll, BorderLayout.SOUTH);
		westPanel.setPreferredSize(new Dimension(450,500));

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setPreferredSize(new Dimension(850, 600));
		mainView.add(buildNorhtPanel(), BorderLayout.NORTH);
		mainView.add(westPanel, BorderLayout.WEST);
		mainView.add(centerPanel, BorderLayout.CENTER);
		mainView.add(buildSouthPanel(), BorderLayout.SOUTH);

		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}




	/**
	 *
	 * @return
	 */
	private JPanel buildNorhtPanel()	{

		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

		// Titre Filtres de recherche
		JPanel panelTitle = new JPanel(new BorderLayout());
		JTextField title = new JTextField("Filtres de recherche");
		title.setFont(Configuration.instance().informationLabelFont(ec));
		title.setEditable(false);
		title.setBorder(BorderFactory.createEmptyBorder(2,0,2,0));
		title.setBackground(new Color(80,130,145));
		title.setForeground(new Color(255,255,255));
		title.setHorizontalAlignment(JTextField.LEFT);
		panelTitle.add(title, BorderLayout.CENTER);

		JPanel panelFiltres = new JPanel(new BorderLayout());
		panelFiltres.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		JPanel panelRecherche = new JPanel(new FlowLayout());
		panelRecherche.add(new JLabel("Ventilations du  "));
		panelRecherche.add(filtreDateDebut);
		//panelRecherche.add(buttonGetDateDebut);
		//panelRecherche.add(buttonDelDateDebut);
		panelRecherche.add(new JLabel(" au "));
		panelRecherche.add(filtreDateFin);
		//panelRecherche.add(buttonGetDateFin);
		//panelRecherche.add(buttonDelDateFin);
		panelRecherche.add(new JLabel("    Libellé : "));
		panelRecherche.add(filtreLibelle);

		panelFiltres.add(panelTitle, BorderLayout.NORTH);
		panelFiltres.add(panelRecherche, BorderLayout.CENTER);
		panelFiltres.add(buttonFind, BorderLayout.EAST);

		ArrayList arrayList = new ArrayList();
		arrayList.add(ZUiUtil.buildBoxLine(new Component[] {panelFiltres}));
		Component panelTemp = ZUiUtil.buildBoxColumn(arrayList);

		panel.add(panelTemp, BorderLayout.CENTER);

		return panel;
	}



	/**
	 *
	 *
	 */
	private void initButtons()	{

		buttonGetDateDebut = new JButton(actionGetDateDebut);
		buttonGetDateDebut.setPreferredSize(new Dimension(20,20));

		buttonGetDateFin = new JButton(actionGetDateFin);
		buttonGetDateFin.setPreferredSize(new Dimension(20,20));
		buttonGetDateFin.setFocusable(false);

		buttonDelDateDebut = new JButton(actionDelDateDebut);
		buttonDelDateDebut.setPreferredSize(new Dimension(20,20));

		buttonDelDateFin = new JButton(actionDelDateFin);
		buttonDelDateFin.setPreferredSize(new Dimension(20,20));
		buttonDelDateFin.setFocusable(false);

		buttonFind = new JButton(actionFind);
		buttonFind.setPreferredSize(new Dimension(150,20));

		buttonSelectAll = new JButton(actionSelectAll);
		buttonSelectAll.setPreferredSize(new Dimension(150,18));

	}



	/**
	 *
	 * @return
	 */
	public void openHistorique(EOOrgan organOrigine)	{

		currentOrgan = organOrigine;

		filter();

		ZUiUtil.centerWindowInContainer(mainWindow);
		mainWindow.show();

	}

	/**
	 *
	 *
	 */
	public void updateData()	{



	}

	public void updateUI()	{


	}

	/*
	 * (non-Javadoc)
	 *
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

		eodMouvements = new EODisplayGroup();
		eodVentilations = new EODisplayGroup();

		NSMutableArray mySortMouvements = new NSMutableArray();
		mySortMouvements.addObject(new EOSortOrdering("bmouDateCreation", EOSortOrdering.CompareDescending));
		eodMouvements.setSortOrderings(mySortMouvements);

		NSMutableArray mySortGestion = new NSMutableArray();
		mySortGestion.addObject(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareDescending));
		mySortGestion.addObject(new EOSortOrdering("bdmcMontant", EOSortOrdering.CompareAscending));
		eodVentilations.setSortOrderings(mySortGestion);

		viewTableMouvements = new JPanel();
        viewTableVentilations = new JPanel();

		initTableModel();
		initTable();

		myEOTableMouvements.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableMouvements.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableMouvements.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		viewTableMouvements.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableMouvements.removeAll();
		viewTableMouvements.setLayout(new BorderLayout());
		viewTableMouvements.add(new JScrollPane(myEOTableMouvements), BorderLayout.CENTER);


		myEOTableVentilations.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableVentilations.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableVentilations.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		viewTableVentilations.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableVentilations.removeAll();
		viewTableVentilations.setLayout(new BorderLayout());
		viewTableVentilations.add(new JScrollPane(myEOTableVentilations), BorderLayout.CENTER);
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{

		myEOTableMouvements = new ZEOTable(myTableSorterMouvements);
		myEOTableMouvements.addListener(new ListenerMouvement());
		myTableSorterMouvements.setTableHeader(myEOTableMouvements.getTableHeader());

		myEOTableVentilations = new ZEOTable(myTableSorterVentilations, new RendererVentilations());
//		myEOTable.addListener(new ListenerGestion());
		myTableSorterVentilations.setTableHeader(myEOTableVentilations.getTableHeader());

	}

	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {

		Vector myCols = new Vector();

		ZEOTableModelColumn col = new ZEOTableModelColumn(eodMouvements, "bmouDateCreation", "Date", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay((DateFormat)new SimpleDateFormat("dd/MM/yyyy"));
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMouvements, "bmouLibelle", "Libellé", 250);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodMouvements, "utilisateur.individu.nomUsuel", "Utilisateur", 100);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		myTableModelMouvements = new ZEOTableModel(eodMouvements, myCols);
		myTableSorterMouvements = new TableSorter(myTableModelMouvements);


		// VENTILATIONS
		myCols = new Vector();

		col = new ZEOTableModelColumn(eodVentilations, "organ.orgUb", "UB", 30);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodVentilations, "organ.orgCr", "CR", 60);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodVentilations, "organ.orgSouscr", "SOUS CR", 80);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodVentilations, "typeCredit.tcdCode", "Tcd", 30);
		col.setAlignment(SwingConstants.CENTER);
		myCols.add(col);

		col = new ZEOTableModelColumn(eodVentilations, "bdmcMontant", "Montant", 80);
		col.setAlignment(SwingConstants.RIGHT);
		myCols.add(col);

		myTableModelVentilations = new ZEOTableModel(eodVentilations, myCols);
		myTableSorterVentilations = new TableSorter(myTableModelVentilations);


}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }

        public void actionPerformed(ActionEvent e) {
        	myTableModelMouvements.fireTableDataChanged();
        	mainWindow.dispose();
        }
	}



	/**
	 *
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeMouvementBudgetaire = %@", new NSArray(FinderTypeMouvementBudgetaire.findTypeMouvement(ec, EOTypeMouvementBudgetaire.MOUVEMENT_VENTILATION))));

		if (currentOrgan.orgNiveau().intValue() == 4)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetMouvementsCredits.organ = %@", new NSArray(currentOrgan)));
		else
		if (currentOrgan.orgNiveau().intValue() == 3)	{
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetMouvementsCredits.organ.orgUb = %@ ", new NSArray(currentOrgan.orgUb())));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetMouvementsCredits.organ.orgCr = %@ ", new NSArray(currentOrgan.orgCr())));
		}
		else
			if (currentOrgan.orgNiveau().intValue() == 2)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetMouvementsCredits.organ.orgUb = %@", new NSArray(currentOrgan.orgUb())));
			else
				if (currentOrgan.orgNiveau().intValue() == 1)
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("budgetMouvementsCredits.organ.orgEtab = %@", new NSArray(currentOrgan.orgEtab())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.EXERCICE_KEY + " = %@", new NSArray(NSApp.getExerciceBudgetaire())));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.TYPE_ETAT_KEY + " = %@", new NSArray(FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_TRAITE))));

		if (!StringCtrl.chaineVide(filtreLibelle.getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("bmouLibelle like %@", new NSArray("*"+filtreLibelle.getText()+"*")));

		if (!StringCtrl.chaineVide(filtreDateDebut.getText()))	{
			NSTimestamp dateReference = new NSTimestamp(DateCtrl.stringToDate(filtreDateDebut.getText()).getTime(), NSTimeZone.defaultTimeZone());
			dateReference = dateReference.timestampByAddingGregorianUnits(0,0,-1,+11,0,0);
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.BMOU_DATE_CREATION_KEY + " > %@", new NSArray(dateReference)));
		}

		if (!StringCtrl.chaineVide(filtreDateFin.getText()))	{
			NSTimestamp dateReference = new NSTimestamp(DateCtrl.stringToDate(filtreDateFin.getText()).getTime(), NSTimeZone.defaultTimeZone());
			dateReference = dateReference.timestampByAddingGregorianUnits(0,0,1,-11,0,0);
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMouvements.BMOU_DATE_CREATION_KEY + " < %@", new NSArray(dateReference)));
		}
		System.out.println("VentilationsHistorique.getFilterQualifier() QUAIF VENTILATIONS : " + new EOAndQualifier(mesQualifiers));

		return new EOAndQualifier(mesQualifiers);

	}

	/**
	 *
	 *
	 */
	public void filter()	{

		EOQualifier myQualifier = getFilterQualifier();

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMouvements.ENTITY_NAME, myQualifier, null);

		fs.setUsesDistinct(true);

		eodMouvements.setObjectArray(ec.objectsWithFetchSpecification(fs));

		myEOTableMouvements.updateData();

	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionSelectAll extends AbstractAction {

	    public ActionSelectAll() {
            super("Tout Sélectionner");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SELECT);
        }

        public void actionPerformed(ActionEvent e) {

        	NSApp.setWaitCursor(mainFrame);
            eodMouvements.selectObjectsIdenticalTo(eodMouvements.allObjects());
            myEOTableMouvements.updateData();
        	NSApp.setDefaultCursor(mainFrame);

        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionFind extends AbstractAction {

	    public ActionFind() {
            super("Rechercher");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_LOUPE);
        }

        public void actionPerformed(ActionEvent e) {


        	filter();

        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	public final class ActionPrint extends AbstractAction {

	    public ActionPrint() {
            super("Imprimer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
        }

        public void actionPerformed(ActionEvent e) {

        	NSMutableArray ids = new NSMutableArray();

        	for (int i=0;i<eodMouvements.selectedObjects().count();i++)	{

        		EOBudgetMouvements mouvement = (EOBudgetMouvements)eodMouvements.selectedObjects().objectAtIndex(i);
        		ids.addObject((ServerProxy.clientSideRequestPrimaryKeyForObject(ec, mouvement)).objectForKey("bmouId"));

        	}

    		EditionsCtrl.sharedInstance(ec).printVentilations(NSApp.getExerciceBudgetaire(), currentOrgan, ids);

        }
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionDelDateDebut extends AbstractAction {

		public ActionDelDateDebut() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_DELETE);
		}

		public void actionPerformed(ActionEvent e) {

			filtreDateDebut.setText("");

		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionDelDateFin extends AbstractAction {

		public ActionDelDateFin() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_DELETE);
		}

		public void actionPerformed(ActionEvent e) {

			filtreDateFin.setText("");

		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionGetDateDebut extends AbstractAction {

		public ActionGetDateDebut() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CALENDAR);
		}

		public void actionPerformed(ActionEvent e) {

			NSApp.setMyDateTextField(filtreDateDebut);
			NSApp.showDatePickerPanel(new Dialog(mainFrame));

		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionGetDateFin extends AbstractAction {

		public ActionGetDateFin() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CALENDAR);
		}

		public void actionPerformed(ActionEvent e) {

			NSApp.setMyDateTextField(filtreDateFin);
			NSApp.showDatePickerPanel(new Dialog(mainFrame));

		}
	}


	/**
	 * Listener sur le deuxieme niveau de l'arborescence budgetaire
	 * Mise à jour du troisieme niveau si deuxieme niveau selectionne
	 */
	private class ListenerMouvement implements ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			NSApp.setWaitCursor(mainFrame);

			currentMouvement = (EOBudgetMouvements)eodMouvements.selectedObject();

			if (currentMouvement != null && eodMouvements.selectedObjects().count() == 1)
				eodVentilations.setObjectArray(FinderBudgetMouvCredit.findMouvementsCreditForMouvement(ec, currentMouvement));
			else
				eodVentilations.setObjectArray(new NSArray());

			myEOTableVentilations.updateData();
			NSApp.setDefaultCursor(mainFrame);
		}

	}


	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class RendererVentilations extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEBITS=new Color(255,165,158);
		public final Color COULEUR_TEXTE_DEBITS = new Color(0,0,0);

		public final Color COULEUR_FOND_CREDITS=new Color(153,255,155);
		public final Color COULEUR_TEXTE_CREDITS = new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			String sens = ((EOBudgetMouvCredit)eodVentilations.displayedObjects().objectAtIndex(row)).typeSens().tyseLibelle();

			if ("DEBIT".equals(sens))	{
						leComposant.setBackground(COULEUR_FOND_DEBITS);
						leComposant.setForeground(COULEUR_TEXTE_DEBITS);
			}
			else	{
				leComposant.setBackground(COULEUR_FOND_CREDITS);
				leComposant.setForeground(COULEUR_TEXTE_CREDITS);

			}

			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}


	/**
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat
	 */
	private class ActionListenerDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(filtreDateDebut.getText()))	return;

			String myDate = DateCtrl.dateCompletion(filtreDateDebut.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date non valide","La date de début n'est pas valide !");
				filtreDateDebut.selectAll();
			}
			else	{
				filtreDateDebut.setText(myDate);
				filtreDateFin.requestFocus();
			}
		}
	}


	/**
	 * Classe d'ecoute sur la date de fin de contrat de travail.
	 * Permet d'effectuer la completion de cette date
	 */
	private class ActionListenerFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(filtreDateFin.getText()))	return;

			String myDate = DateCtrl.dateCompletion(filtreDateFin.getText());
			if ("".equals(myDate))	{
				filtreDateFin.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date de fin n'est pas valide !");
			}
			else
				filtreDateFin.setText(myDate);
		}
	}

	/**
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion pour la saisie de la date de debut de contrat
	 */
	private class ListenerTextFieldDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(filtreDateDebut.getText()))	return;

			String myDate = DateCtrl.dateCompletion(filtreDateDebut.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date non valide","La date de début n'est pas valide !");
				filtreDateDebut.selectAll();
			}
			else
				filtreDateDebut.setText(myDate);
		}
	}

	/**
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date
	 */
	private class ListenerTextFieldFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(filtreDateFin.getText()))	return;

			String myDate = DateCtrl.dateCompletion(filtreDateFin.getText());
			if ("".equals(myDate))	{
				filtreDateFin.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date de fin n'est pas valide !");
			}
			else
				filtreDateFin.setText(myDate);
		}
	}

}
