/*
 * Copyright (C) 2003 Université de La Rochelle
 *
 * This file is part of PrestationWeb.
 *
 * PrestationWeb is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * PrestationWeb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil;

import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;



/**
 * Classe permettant d'écrire dans le log.
 *
 * @author rprin
 * @deprecated
 */
public class ZLog {
	public static final boolean DEFAULTSHOWOUTPUT=false;
	public static final boolean DEFAULTSHOWTRACE=false;

	/**
	 * Longueur d'une ligne de séparation, par défaut 80 caractères.
	 */
	public int lenForSeparator = 80;

	/**
	 * Longueur par défaut de l'espace réservé à l'affichage de la clé lorsqu'on affiche une clé/valeur.
	 */
	public int defaultKeyLength = 35;


	/**
	 * Indique si le log sera réellement affiché.
	 * Ceci permet de le désactiver éventuellement au moment du passage en production.
	 */
	private boolean showOutput;

	/**
	 * Indique si les traces seront affichées.
	 */
	private boolean showTrace;





	/**
	 *
	 */
	public ZLog() {
		setShowOutput(DEFAULTSHOWOUTPUT);
		setShowTrace(DEFAULTSHOWTRACE);
	}

	public ZLog(final String userName) {
		setShowOutput(DEFAULTSHOWOUTPUT);
		setShowTrace(DEFAULTSHOWTRACE);
	}




	/**
	 * Ajoute une ligne de séparation
	 *
	 * @param sep
	 */
	public void appendSeparator(String sep) {
		if (sep==null) {
			sep="-";
		}
		directWriteln( ZStringUtil.extendWithChars( "",sep,lenForSeparator,false)    );
	}

	/**
	 * Ajoute un titre (avec la première lettre en majuscule), suivi d'une ligne de séparation.
	 * @param str
	 */
	public void appendTitle(String str) {
		directWriteln("");
		directWriteln( ZStringUtil.capitalizedString(str) );
		appendSeparator(null);
	}

	/**
	 * Ajoute un message entouré par deux mentions "ATTENTION".
	 *
	 * @param str
	 */
	public void appendWarning(String str) {
		append(":-(   ATTENTION >>>>" + str);
	}


	/**
	 * Ajoute un message précédé d'un smiley :-).
	 *
	 * @param str
	 */
	public void appendSuccess(String str) {
		append(":-)   " + str);
	}



	/**
	 * Ajoute au log une paire clé = valeur.
	 *
	 * @param key
	 * @param value
	 */
	public void appendKeyValue(String key, Object value) {
		appendKeyValue(key, value, defaultKeyLength);
	}

	/**
	 * Ajoute au log une paire clé = valeur en spécifiant le nombre de caractères à utiliser pour créer la colonne de la clé.
	 *
	 * @param key
	 * @param value
	 * @param keyLength
	 */
	public void appendKeyValue(String key, Object value, int keyLength) {
		if (key.length()>keyLength) {
			keyLength = key.length();
		}
		append(ZStringUtil.extendWithChars(key," ",keyLength,false) + " = " + value);
	}


	/**
	 * Ajoute au log un titre entouré par des caractères étoiles.
	 *
	 * @param str
	 */
	public void appendBigTitle(String str) {
		directWriteln("");
		appendSeparator("*");
		int nb = lenForSeparator/2 - str.length()/2 -1;
		String sep="";
		if (nb> 0 ) {
			sep = ZStringUtil.extendWithChars("","*",nb,false);
		}
		directWriteln(sep + " " + str+ " " + sep);
		appendSeparator("*");
		directWriteln("");
	}



	/**
	 *
	 * Ecrit dans le log une trace (avec n° de ligne du code source, type de l'Objet).
	 *
	 * @param pVarName
	 * @param pObj
	 */
	public void trace(Object pObj) {
		if (this.showTrace) {
			StringBuffer vLine = new StringBuffer();
			 vLine.append("-> ") ;
			 vLine.append(getTime());
			 vLine = vLine.append("> ");
			 vLine = vLine.append(this.getCallMethod(0));
			 vLine = vLine.append(" >>> ");
			 if (pObj!=null) {
				 vLine = vLine.append(pObj.toString());
			 }
			 else {
				 vLine = vLine.append("null");
			 }
			 if (pObj!=null) {
				 vLine = vLine.append("("+pObj.getClass().getName()+")");
			 }
			writeln(vLine.toString());
		 }
	}


	public void trace(NSArray objects) {
	    writeln("");
	    writeln(objects);
	    writeln("");
	}

	public void trace(EOEditingContext ec) {
	    writeln("");

	    writeln("Objets mis à jour dans l'editingContext " + ec.toString());
	    writeln(ec.updatedObjects());
	    writeln("-----------------------------------");

	    writeln("Objets supprimes dans l'editingContext " + ec.toString());
	    writeln(ec.deletedObjects());
	    writeln("-----------------------------------");

	    writeln("Objets inseres dans l'editingContext " + ec.toString());
	    writeln(ec.insertedObjects());
	    writeln("-----------------------------------");

	    writeln("");
	}

	public void trace(Map objects) {
	    writeln("");
	    writeln("Contenu de la map");

	    append(objects);
	    writeln("");
	}

	public void trace(Vector objects) {
	    writeln("");
	    append(objects);
	    writeln("");
	}
	public void trace(EOEnterpriseObject object) {
	    writeln("");
	    if (object==null) {
	        writeln("objet null");
	    }
	    else {
	        writeln(object.getClass().getName() + " ");
	    }
	    append(object);
	    writeln("");
	}

	public void append(Object obj) {
	    writeln(obj.toString());
	}

	public void append(Vector objects) {
		Iterator iter = objects.iterator();
		int i=0;
		while (iter.hasNext()) {
		    writeln(new String (""+i+"-->")+iter.next());
			i++;
		}
	}

	public void append(Map objects) {
		Iterator iter = objects.keySet().iterator();
		int i=0;
		while (iter.hasNext()) {
		    Object obj = iter.next();
		    writeln(""+ obj  +"-->"+ objects.get(obj) );
			i++;
		}
	}

	public void append(EOEnterpriseObject object) {
	    if (object!=null) {
			Iterator iter = object.attributeKeys().vector().iterator();
			while (iter.hasNext()) {
			    String obj = (String) iter.next();
			    writeln(""+ obj  +"-->"+ object.valueForKey(obj) );
			}

			Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
			while (iter2.hasNext()) {
			    String obj = (String) iter2.next();
			    writeln(""+ obj  +"-->"+ object.valueForKey(obj) );
			}
			Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
			while (iter3.hasNext()) {
			    String obj = (String) iter3.next();
			    writeln(""+ obj  +"-->"+ object.valueForKey(obj) );
			}
	    }
	}





	/**
	 *
	 * Ecrit (si showtrace est à true) dans le log une trace (avec n° de ligne du code source, type de l'Objet).
	 *
	 * @param pVarName
	 * @param pObj
	 */
	public void trace(String pVarName, Object pObj ) {
		if (this.showTrace) {
			StringBuffer vLine = new StringBuffer();
			 vLine.append("-> ") ;
			 vLine.append(getTime());
			 vLine = vLine.append("> ");
			 vLine = vLine.append(this.getCallMethod(0));
			 vLine = vLine.append(" >>> ");
			 if (pVarName != null) {
				 vLine = vLine.append(pVarName);
				 vLine = vLine.append( " = ");
			 }
			 if (pObj!=null) {
				 vLine = vLine.append(pObj.toString());
			 }
			 else {
				 vLine = vLine.append("null");
			 }
			 if (pObj!=null) {
				 vLine = vLine.append("("+pObj.getClass().getName()+")");
			 }
			writeln(vLine.toString());
		 }
	}


	protected String getTime() {
		return java.text.DateFormat.getTimeInstance().format(new java.util.Date());
	}



	/**
	 * Ajoute une ligne au log, si showOutput est à true.
	 * @param string
	 */
	public void append(String string) {
			writeln(string);
	}


	/**
	 * Ajoute une ligne au log, indépendamment de la valeur showOutput.
	 * @param string
	 */
	public void appendForceOutput(String string) {
		writeln(string, true);
	}



	/**
	 * Permet d'indique si le log sera réellement affiché dans la console.
	 * Ceci permet de le désactiver éventuellement au moment du passage en production.
	 */
	public void setShowOutput(boolean b) {
		showOutput = b;
	}


	/**
	 * Récupérer le nom de la méthode et la ligne dans le fichier qui a appelé le log
	 *
	 * @param profondeur profondeur d'appel
	 * @return
	 */
	private String getCallMethod(int profondeur) {
		Throwable tmp = new Throwable();
		//NSLog.out.appendln(new NSArray(tmp.getStackTrace()) );
		return tmp.getStackTrace()[profondeur+2].toString();
	}




	/**
	 * @param b
	 */
	public void setShowTrace(boolean b) {
		showTrace = b;
	}



	/**
	 * Ecrit s dans le log, si showOutput est true.
	 *
	 * @param s
	 */
	protected void writeln(final String s) {
		writeln(s, showOutput);
	}


	protected void writeln(final NSArray array) {
        if (array!=null)
	    for (int i = 0; i < array.count(); i++) {
            writeln(""+i+"-->" + array.objectAtIndex(i) );
        }
	}



	/**
	 * Ecrit s dans le log, si show est true.
	 * Dériver cette méthode pour modifier chacune des lignes.
	 *
	 * @param s
	 */
	protected void  writeln(final String s, boolean show) {
		if (show) {
			System.out.println(s);
		}
	}


	/**
	 *
	 * Ecrit dans le log, indépendamment de showOutput.
	 *
	 * @param s
	 */
	protected void directWriteln(String s) {
		System.out.println(s);
	}




	/**
	 * @return
	 */
	public boolean isShowOutput() {
		return showOutput;
	}

	/**
	 * @return
	 */
	public boolean isShowTrace() {
		return showTrace;
	}

}
