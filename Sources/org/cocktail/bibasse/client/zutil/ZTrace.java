//Copyright (C) 2003 Université de La Rochelle
//
//Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou
//le modifier conformément aux dispositions de la Licence //Publique générale
//GNU, telle que publiée par la Free Software Foundation ; version 2 de la licence,
//ou encore (à votre choix) toute version ultérieure.
//
//Ce programme est distribué dans l'espoir qu'il sera utile,
//mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
//COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour
//plus de détail, voir la Licence Publique générale GNU .
//
//Vous devez avoir reçu un exemplaire de la Licence Publique générale GNU
//en même temps que ce programme ; si ce n'est pas le cas, écrivez à la
//Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
//
//
//Copyright (C) 2003 Université de La Rochelle
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA

// Created by rprin on Thu Feb 13 2003
//

package org.cocktail.bibasse.client.zutil;
import java.io.PrintWriter;
import java.io.StringWriter;

import com.webobjects.foundation.NSLog;

/**
* Classe pour afficher des messages de log. Mettre la variable d'instance showTrace à false en production, pour éviter la surcharge en traitements du serveur.
 */
public class ZTrace extends Object {
    /**
    * Permet d'indiquer si on souhaite réellement afficher les messages. Mettre à false en production.
     */
    protected boolean showTrace=true;
    /**
    * Permet d'indiquer si on souhaite avoir l'heure de chaque message (à true par défaut)
     */
    protected boolean showTime=true;

    /**
    * Renvoi une chaine indiquant la classe et la méthode où l'instruction de log a été appelée.
     * Cette méthode crée une instance de java.lang.Throwable et utilise la méthode printStackTrace pour récupérer la trace. 
     *
     */
    protected String getCallMethod() {
        StringWriter sw = new StringWriter();
        new Throwable().printStackTrace(new PrintWriter(sw));
        String vStack = sw.toString();
        //4 fois car 1: cette méthode, 2: constructeur, 3: méthode statique, 4: la méthode appelante
        int beginPos = vStack.indexOf("at")+1;
        beginPos = vStack.indexOf("at", beginPos)+1;
        beginPos = vStack.indexOf("at", beginPos)+1;
        beginPos = vStack.indexOf("at", beginPos) + 2;
        int endPos = vStack.indexOf(")" , beginPos) +1;

        return vStack.substring(beginPos, endPos);
    }

    /**
    * Constructeur (privé) de la classe
     *
     * @param pVarname Nom de la variable
     * @param pObj Objet à tracer. Cette méthode utilisera NSLog.out.appendln(pObj)
     */
    protected ZTrace(String pVarName,Object pObj) {
        if (this.showTrace) {
            String vLine = new String("++ ");
            if (this.showTime) {
                vLine = vLine.concat(java.text.DateFormat.getTimeInstance().format(new java.util.Date()));
            }
            vLine = vLine.concat(" > ");
            vLine = vLine.concat(this.getCallMethod());
            vLine = vLine.concat(" >>> ");
            if (pVarName != null) {
                vLine = vLine.concat(pVarName);
                vLine = vLine.concat( " = ");
            }
            if (pObj!=null) {            	
                vLine = vLine.concat(pObj.toString());
            }
            else {
                vLine = vLine.concat("null");
            }
            if (pObj!=null) {
				vLine = vLine.concat("(type : "+pObj.getClass().getName()+")");
            }
            NSLog.out.appendln(vLine);
        }
    }

    /**
    * Méthode statique à appeler pour effectuer une inscription dans le log.
     * @param pVarname Nom de la variable.
     * @param pObj Objet à tracer. Utilisation de NSLog.out.appendln(pObj)     
    */
    public static void log(String pVarName,Object pObj) {
        new ZTrace(pVarName,pObj);
    }
    
    /**
     * Méthode statique à appeler pour effectuer une inscription dans le log.
     * @param pObj Objet à tracer.  Utilisation de NSLog.out.appendln(pObj)
     */
    public static void log(Object pObj) {
        new ZTrace(null,pObj);
    }
    
}

