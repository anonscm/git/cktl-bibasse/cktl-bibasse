package org.cocktail.bibasse.client.zutil;

import java.math.BigDecimal;

import com.webobjects.foundation.NSArray;

public class NumberCtrl {

	public static final String[] listeChiffres = {"0","1","2","3","4","5","6","7","8","9"};

	/** */
	public static boolean estUnChiffre(String chaine) {
		NSArray chiffres = new NSArray(listeChiffres);

		if (chiffres.containsObject(chaine))	return true;

		return false;
	}

        public static Number arrondir(Number number)  {
            BigDecimal	nombre=new BigDecimal(""+number);

            try
            {
                return nombre.setScale(2,BigDecimal.ROUND_HALF_UP);
            }
            catch (java.lang.ArithmeticException e)
            {
                return new Float(0.00);
            }
            catch (java.lang.IllegalArgumentException e)
            {
                return new Float(0.00);
            }
        }


        public static Number strToNumber(String str)  {
            BigDecimal	nombre;
            try {
                nombre = new BigDecimal(str);
                return nombre;
            }
            catch (java.lang.ArithmeticException e)  {
                return new Float(0.00);
            }
            catch (java.lang.IllegalArgumentException e)  {
                return new Float(0.00);
            }
        }

                


        
}
