

package org.cocktail.bibasse.client.zutil.logging;

import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.cocktail.bibasse.client.zutil.ZStringUtil;

import com.webobjects.eocontrol.EOEnterpriseObject;


/**
 * @author rodolphe.prin@univ-lr.fr
 */
public class ZLogger {
    public static final int LVL_VERBOSE=40; 
    public static final int LVL_DEBUG=30; 
    public static final int LVL_INFO=20; 
    public static final int LVL_WARNING=10; 
    public static final int LVL_ERROR=0; 
    public static final int LVL_NONE=-1; 
    
    public static final String VERBOSE="VERBOSE"; 
    public static final String DEBUG="DEBUG"; 
    public static final String INFO="INFO"; 
    public static final String WARNING="WARNING"; 
    public static final String ERROR="ERROR"; 
    public static final String NONE="NONE"; 

    
    public static int currentLevel = LVL_INFO;
    
    
    public static int lenForSeparator = 80;
    /**
     * Longueur par defaut de l'espace reserve a l'affichage 
     * de la cle lorsqu'on affiche une cle/valeur.
     */
    public static int defaultKeyLength = 35;    
    

    /**
     * Cree un titre entoure par des caracteres etoiles.
     *
     * @param str
     */
    protected static String createBigTitle(final String str) {
        final StringBuffer s = new StringBuffer();
        s.append("\n");
        s.append(createSeparator("*"));
        s.append("\n");
        final int nb = lenForSeparator/2 - str.length()/2 -1;
        if (nb > 0 ) {
            final String sep = ZStringUtil.extendWithChars("","*",nb,false);
            s.append(sep);
            s.append(str);
            s.append(sep);
        }
        else {
            s.append(str);
        }
        s.append("\n");
        s.append(createSeparator("*"));
        s.append("\n");
        return s.toString();
    }    
    
    
    protected static String createTitle(final String str) {
        final StringBuffer s = new StringBuffer();
        s.append("\n");
        s.append(ZStringUtil.capitalizedString(str));
        final int nb = str.length();
        final String sep = ZStringUtil.extendWithChars("","-",nb,false);
        s.append("\n");
        s.append(sep);
        s.append("\n");
        return s.toString();
    }        
    
    
    
    /**
     * Ajoute un message precede par "ATTENTION".
     *
     * @param str
     */
    protected static String createFailed(final String str) {
        return ":-(   ATTENTION >>>>" + str;
    }


    /**
     * Ajoute un message precede d'un smiley :-).
     *
     * @param str
     */
    protected static String  createSuccess(final String str) {
        return ":-)   " + str;
    }




    protected static String createKeyValue(final String key, final Object value) {
        return createKeyValue(key, value, defaultKeyLength);
    }

    /**
     * Ajoute au log une paire cle = valeur en specifiant le nombre de caracteres a utiliser 
     * pour creer la colonne de la cle.
     *
     * @param key
     * @param value
     * @param keyLength
     */
    protected static String createKeyValue(final String key, final Object value, int keyLength) {
        if (key.length()>keyLength) {
            keyLength = key.length();
        }
        return ZStringUtil.extendWithChars(key," ",keyLength,false) + " = " + value;
    }    
    
    
    protected static String createSeparator(String sep) {
        if (sep==null) {
            sep="-";
        }
        return ZStringUtil.extendWithChars( "",sep,lenForSeparator,false)   ;
    }    
    
    
    protected static void log(final Object obj, int level) {
        if (level <= currentLevel) {
            out(obj);
        }
    }
    
    
    
    
    
    protected static void out (final Object obj) {
        if (obj==null) {
            System.out.println("null");
        }
        else {
            System.out.println(obj);
        }
    }
    
    protected static void out (final String obj) {
        if (obj==null) {
            System.out.println("null");
        }
        else {
            System.out.println(obj);
        }
    }
    

    
    
    public static void verbose(final Object obj) {
        log(obj, LVL_VERBOSE);
    }
    
    public static void debug(final Object obj) {
        log(obj, LVL_DEBUG);
    }
    
    public static void info(final Object obj) {
        log(obj, LVL_INFO);
    }
    
    public static void warning(final Object obj) {
        log(obj, LVL_WARNING);
    }
    
    public static void error(final Object obj) {
        log(obj, LVL_ERROR);
    }
    
    public static void logBigTitle(final String string, int level) {
        if (level <= currentLevel) {
            out(createBigTitle(string));
        }
    }    
    
    public static void logTitle(final String string, int level) {
        if (level <= currentLevel) {
            out(createTitle(string));
        }
    }    
    
    public static void logKeyValue(final String key, final Object value, int level) {
        if (level <= currentLevel) {
            out(createKeyValue(key, value));
        }
    }    
    
    public static void logSuccess(final String string, int level) {
        if (level <= currentLevel) {
            out(createSuccess(string));
        }
    }    
    
    public static void logFailed(final String string, int level) {
        if (level <= currentLevel) {
            out(createFailed(string));
        }
    }    
    
    

    protected static void out(final Vector objects) {
        Iterator iter = objects.iterator();
        int i=0;
        while (iter.hasNext()) {
            out(new String (""+i+"-->")+iter.next());
            i++;
        }
    }

    protected static void out(final Map objects) {
        final Iterator iter = objects.keySet().iterator();
        int i=0;
        while (iter.hasNext()) {
            final Object obj = iter.next();
            out(""+ obj  +"-->"+ objects.get(obj) );
            i++;
        }
    }

    protected static void out(final EOEnterpriseObject object) {
        if (object!=null) {
            final Iterator iter = object.attributeKeys().vector().iterator();
            while (iter.hasNext()) {
                final String obj = (String) iter.next();
                out(""+ obj  +"-->"+ object.valueForKey(obj) );
            }

            Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
            while (iter2.hasNext()) {
                String obj = (String) iter2.next();
                out(""+ obj  +"-->"+ object.valueForKey(obj) );
            }
            Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
            while (iter3.hasNext()) {
                String obj = (String) iter3.next();
                out(""+ obj  +"-->"+ object.valueForKey(obj) );
            }
        }
    }


    public static final int getCurrentLevel() {
        return currentLevel;
    }


    public static final void setCurrentLevel(int currentLevel) {
        ZLogger.currentLevel = currentLevel;
    }


    
}
