/*
 * Copyright CRI - Universite de La Rochelle, 1995-2004
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.client.zutil.ui.forms;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.Format;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;



/**
 * @author rodolphe.prin@univ-lr.fr
 */
public class ZNumberField extends ZTextField {
    private final Format[] _editorsFormats;


    /**
     * @param modifiersFormats Les format d'edition
     * @param displayFormat Le format d'affichage
     */
    public ZNumberField(Format[] editorFormats, Format displayFormat) {
        super();
        _editorsFormats = editorFormats;
        setFormat(displayFormat);
        getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
        getMyTexfield().addFocusListener( new ZNumberFieldListener() );
    }

    /**
     * @param provider
     */
    public ZNumberField(IZTextFieldModel provider, Format[] editorFormats, Format displayFormat) {
        super(provider);
        _editorsFormats = editorFormats;
        setFormat(displayFormat);
        getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
        getMyTexfield().addFocusListener( new ZNumberFieldListener());
    }

	private final class ZNumberFieldListener implements FocusListener {
		public void focusGained(final FocusEvent  e) {
			((JTextComponent)e.getComponent()).selectAll();
		}

		/**
		* Methode appelee lorsque le focus quitte le champ.
		* Elle est implementee pour realiser le controle de la saisie.
		*/
		public void focusLost(final FocusEvent e){
			updateData();
		}
	}


	/**
     * @see org.cocktail.bibasse.client.zutil.ui.forms.ZTextField#getInnerValue()
     */
    protected Object getInnerValue() {
		String txt = getMyTexfield().getText();
		if (txt==null) {
			return null;
		}
        if (_editorsFormats.length==0) {
        	return txt;
        }
        Object res = null;
        boolean stop = false;
        txt = txt.replace('.',',');
        for (int i = 0; (i < _editorsFormats.length && !stop); i++) {
            final Format aFormat = _editorsFormats[i];
            try {
                res = aFormat.parseObject(txt);
                //si pas d'exception c'est ok
                stop = true;
            }
            catch (Exception e) {
                //on ne fait rien
            }
        }
        return res;
    }


}
