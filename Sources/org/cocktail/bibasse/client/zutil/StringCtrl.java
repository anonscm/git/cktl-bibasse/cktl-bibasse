package org.cocktail.bibasse.client.zutil;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

public class StringCtrl
{

	// Renvoie un tableau contenant les chaines separees par le separateur donne
	public static NSArray componentsSeparatedByString(String aString,String separateur)
	{
		NSMutableArray localMutableArray = new NSMutableArray();
		int index;		

		while ( (index = aString.indexOf(separateur)) >= 0)
		{			
			localMutableArray.addObject(aString.substring(0,index));
			aString = aString.substring(index+1,aString.length());
		}		

		localMutableArray.addObject(aString);	

		return localMutableArray;
	}

/** GET MOIS : renvoie le numero du mois formatte sur 2 caracteres */
	public static String formatter2Chiffres(int nombre)
	{
		if (nombre < 10)
			return "0" + nombre;

		return "" + nombre;
	}

// COMPONENTS JOINED BY STRING
	public static String componentsJoinedByString(NSArray anArray, String separateur)
	{
	String tempo="";
	int i;
	
		tempo = (String)anArray.objectAtIndex(0);
	
		for (i=1;i<anArray.count();i++)
			tempo = tempo + separateur + anArray.objectAtIndex(i);
	
		return tempo;
	}

// CAPITALIZED STRING
	public static String capitalizedString(String aString)
	{
		if ("".equals(aString))
			return "";

	    String debut = (aString.substring(0,1)).toUpperCase();
	    String fin = (aString.substring(1,aString.length())).toLowerCase();

	    return debut.concat(fin);
	}

    public static String recupererChaine(String laChaine)
    {
        if ((laChaine==null) ||  (laChaine == NSKeyValueCoding.NullValue.toString())  || (laChaine.equals("")) || (laChaine.equals("*nil*")) )
      return "";

        return laChaine;
    }

	public static int stringToInt(String num, int defaultValue) {
		try {
			num = num.trim();
			return Integer.valueOf(num).intValue();
		} catch(NumberFormatException ex) {
			return defaultValue;
		}
	}

	public static Integer stringToInteger(String num, int defaultValue) {
		try {
			num = num.trim();
			return Integer.valueOf(num);
		} catch(NumberFormatException ex) {
			return new Integer(defaultValue);
		}
	}

	public static boolean containsIgnoreCase(String s, String substring) {
		s = s.toUpperCase();
		substring = substring.toUpperCase();
		return (s.indexOf(substring) >= 0);
	}

	public static boolean startsWithIgnoreCase(String s, String substring) {
		s = s.toUpperCase();
		substring = substring.toUpperCase();
		return s.startsWith(substring);
	}

	public static String getSuffix(String s, String prefix) {
		s = s.toUpperCase();
		prefix = prefix.toUpperCase();
		if (s.startsWith(prefix))
		    return s.substring(prefix.length());

		return "";
	}

	public static String get0Int(int number, int digits) {
		String s = String.valueOf(number);
		for(;s.length() < digits; s = "0"+s );
		return s;
	}

	public static String toHttp(String aString) {
		int idx, startIdx;
		startIdx = 0;
		do {
			idx = aString.indexOf(" ", startIdx);
			if (idx != -1) {
				aString = aString.substring(0, idx)+"%20"+aString.substring(idx+1);
				startIdx = idx+3;
			}
		} while(idx != -1);
		return aString;
	}

// CHARACTER IS PRESENT
	public boolean  characterIsPresentInString(String chaine,String car)
	{
		// On rajoute un caractere au cas ou le caractere a tester soit en fin de chaine
		String chaineATester = chaine + "0";

		if (StringCtrl.componentsSeparatedByString(chaineATester,car).count() > 1)
			return true;

		return false;
	}

    /**
      * Renvoie la valeur a inserer dans la base de donnees.
     * La valeur est "NullValue", si le parametre est null,
     * et la meme chaine de caracteres, sinon.
     */
    public static Object dbValueForObject(Object anObject) {
      Object result;
      if (anObject instanceof String) {
            if ((anObject == null) || (((String)anObject).length() == 0))
              result = NSKeyValueCoding.NullValue;
            else
              result = anObject;
      } else {
            if (anObject == null)
              result = NSKeyValueCoding.NullValue;
            else
              result = anObject;
      }
      return result;
    }

    public static boolean isEmpty(String str) {
        return ((str==null) || (str.length()==0) || (str.equals("*nil*")));
    }

}
