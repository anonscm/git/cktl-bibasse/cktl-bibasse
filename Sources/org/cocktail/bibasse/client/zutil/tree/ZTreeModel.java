/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Simbud.
 *
 * Simbud is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Simbud is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil.tree;

import java.util.Vector;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;



/**
 * Modele d'un JTree. Accepte des obets de type ZTreeNode.
 * 
 * @author Rodolphe Prin
 */
public class ZTreeModel implements TreeModel {
	private Vector myListeners = new Vector();
	private ZTreeNode rootNode;
	
	

	/**
	 * @param root Objet racine
	 */
	public ZTreeModel(ZTreeNode root) {
		super();
		rootNode = root;
	}

	/**
	 * Returns the root of the tree. Returns null only if the tree has no nodes. 
	 * @see javax.swing.tree.TreeModel#getRoot()
	 */
	public Object getRoot() {
		return rootNode;
	}

	/**
	 * Returns the child of parent at index index in the parent's child array. 
	 * parent must be a node previously obtained from this data source. 
	 * This should not return null if index is a valid index for parent 
	 * (that is index >= 0 && index &lt; getChildCount(parent)). 
	 */
	public Object getChild(Object parent, int index) {
//		System.out.println("getChild(Object)");
		ZTreeNode obj = (ZTreeNode)parent;
		return obj.getChild(index);
	}


	/**
	 * Returns the number of children of parent. Returns 0 if the node is a leaf or if it has no children. 
	 * parent must be a node previously obtained from this data source. 
	 * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
	 */
	public int getChildCount(Object parent) {
		ZTreeNode obj = (ZTreeNode)parent;
		return obj.getChildCount();
	}


	
	/**
	 * Returns true if node is a leaf. It is possible for this method to return false even 
	 * if node has no children. 
	 * A directory in a filesystem, for example, may contain no files; 
	 * the node representing the directory is not a leaf, but it also has no children. 
	 * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
	 */
	public boolean isLeaf(Object arg0) {
		ZTreeNode obj = (ZTreeNode)arg0;
		return obj.isLeaf();
	}

	/**
	 * Messaged when the user has altered the value for the item identified by path to newValue. If newValue signifies a truly new value the model should post a treeNodesChanged event. 
	 * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
	 */
	public void valueForPathChanged(TreePath arg0, Object arg1) {
	}

	/**
	 * Returns the index of child in parent. If parent is null or child is null, returns -1.
	 * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
	 */
	public int getIndexOfChild(Object parent, Object child) {
		ZTreeNode obj = (ZTreeNode)parent;
		return obj.getIndexOfChild(obj);
	}
	
	

	/**
	 * Adds a listener for the TreeModelEvent posted after the tree changes. 
	 * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
	 */
	public void addTreeModelListener(TreeModelListener l) {
		myListeners.addElement(l);
	}

	/**
	 * Removes a listener previously added with addTreeModelListener. 
	 * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
	 */
	public void removeTreeModelListener(TreeModelListener l) {
		myListeners.removeElement(l);
	}

	//	////////////// Fire events //////////////////////////////////////////////

	/**
	 * The only event raised by this model is TreeStructureChanged with the
	 * root as path, i.e. the whole tree has changed.
	 */
	protected void fireTreeStructureChanged(ZTreeNode oldRoot) {
		int len = myListeners.size();
		TreeModelEvent e = new TreeModelEvent(this, new Object[] { oldRoot });
		for (int i = 0; i < len; i++) {
			((TreeModelListener) myListeners.elementAt(i)).treeStructureChanged(e);
		}
	}


}
