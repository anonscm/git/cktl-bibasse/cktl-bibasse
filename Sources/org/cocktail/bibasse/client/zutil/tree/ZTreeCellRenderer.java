/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil.tree;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * 
 * 
 * @author Rodolphe Prin
 */
/**
 * 
 * 
 * @author Rodolphe Prin
 */
public class ZTreeCellRenderer extends DefaultTreeCellRenderer {
	public static final int ZLEFT=JTextField.LEFT;
	public static final int ZRIGHT=JTextField.RIGHT;
	public static final int ZCENTER=JTextField.CENTER;

	protected Component superComponent;
	


	public ZTreeCellRenderer() {
		super();
	}
	
	
	
	
	

	/**
	 * Crée un texfield 
	 * @param libelle
	 * @param columns
	 * @param horizontalAlign
	 * @param sel
	 * @param BgColor Si null, la couleur de fond pour la non selection sera celle par défaut.
	 * @return
	 */
	public JTextField buildTextField(String libelle, int columns, int horizontalAlign, boolean sel, Color bgColor) {
		if (libelle==null) {
			libelle ="";
		}
		JTextField f = new JTextField(libelle);
//		System.out.println("libelle:"+libelle);
		f.setColumns(columns);
		if (libelle.length()>columns+3) {
			f.setText(libelle.substring(0, columns)+"...");
			setToolTipText(libelle);
		}
		else {
			setToolTipText(null);
		}
		f.setHorizontalAlignment(horizontalAlign);
//		f.setBorder(BorderFactory.createEmptyBorder());
		f.setBorder( BorderFactory.createEmptyBorder(0, 2, 0, 2));
		if (sel) {
			f.setBackground( getBackgroundSelectionColor() );
			f.setForeground( getTextSelectionColor());
		}
		else {
			if (bgColor!=null) {
				f.setBackground( bgColor );	
			}
			f.setForeground( getTextNonSelectionColor());
		}
		return f;
	}
	

	
	

	
	
	
	

}
