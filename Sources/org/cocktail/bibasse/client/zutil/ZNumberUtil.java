package org.cocktail.bibasse.client.zutil;

import java.math.BigDecimal;

/*
 * Fichier créé le 23 juil. 2003
 * par rprin
 */

/**
 * Classe regroupant des méthodes statiques utilitaires pour le traitement des
 * nombres
 * 
 * @author <a href="mailto:rodolphe.prin@univ-lr.fr">Rodolphe Prin</a>
 * 
 */
public abstract class ZNumberUtil extends Object {

    /**
     * Renvoie un nombre arrondi à deux décimales (ROUND_HALF_UP).
     * 
     * @param num
     */
    public static BigDecimal arrondi2(final BigDecimal num) {
        return num.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Renvoie un nombre arrondi à deux décimales (ROUND_HALF_UP).
     * 
     * @param num
     */
    public static BigDecimal arrondi2(final Number num) {
        final BigDecimal tmp = new BigDecimal(num.doubleValue());
        return tmp.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public static Number strToNumber(final String str) {
        final BigDecimal nombre;
        try {
            nombre = new BigDecimal(str);
            return nombre;
        } catch (java.lang.ArithmeticException e) {
            return new Float(0.00);
        } catch (java.lang.IllegalArgumentException e) {
            return new Float(0.00);
        }
    }

    public static BigDecimal arrondi2(Number num, Number defVal) {
        if (num == null) {
            num = defVal;
        }
        final BigDecimal tmp = new BigDecimal(num.doubleValue());
        return tmp.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
    
    /**
     * renvoie le BigDecimal qui a la plus petite valeur absolue.
     *
     * @param a
     * @param b
     * @return
     */
    public static final BigDecimal plusPetitValeurAbsolue(BigDecimal a, BigDecimal b) {
        if (a.abs().compareTo(b.abs()) > 0) {
            return b;
        }
        return a;
    }
    

}
