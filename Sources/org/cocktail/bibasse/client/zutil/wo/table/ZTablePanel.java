/*
 * Copyright CRI - Universite de La Rochelle, 1995-2004
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.client.zutil.wo.table;

import java.awt.BorderLayout;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author rodolphe.prin@univ-lr.fr
 */
public abstract class ZTablePanel extends ZAbstractPanel implements ZEOTable.ZEOTableListener {
    protected IZTablePanelMdl ctrl;

	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected final EODisplayGroup myDisplayGroup = new EODisplayGroup();
	protected TableSorter myTableSorter;
	protected HashMap colsMap = new LinkedHashMap();



    /**
     * @param editingContext
     */
    public ZTablePanel(IZTablePanelMdl listener) {
        super();
        ctrl = listener;
    }


	protected void initTableModel() {
		myTableModel = new ZEOTableModel(myDisplayGroup, colsMap.values());
		myTableSorter = new TableSorter (myTableModel);
	}



	/**
	 * Initialise la table et afficher (le modele doit exister)
	 */
	protected void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}



    public void initGUI() {
        initTableModel();
        initTable();
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }



    /**
     * @see org.cocktail.jefyadmin.client.CommonPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(ctrl.getData() );
        myEOTable.updateData();
    }


    public void setTableModel(ZEOTableModel mdl) {
        myTableModel = mdl;
        myEOTable.setModel(mdl);
    }


	public static interface IZTablePanelMdl {
        public void selectionChanged();
        public NSArray getData() throws Exception ;
        public void onDbClick();
        
	}







	/**
     * @see org.cocktail.bibasse.client.zutil.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        ctrl.onDbClick();
    }



    /**
     * @see org.cocktail.bibasse.client.zutil.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        ctrl.selectionChanged();
    }


    /**
     * @return
     */
    public EOEnterpriseObject selectedObject() {
        return (EOEnterpriseObject) myDisplayGroup.selectedObject();
    }

    public NSArray selectedObjects() {
        return myDisplayGroup.selectedObjects();
    }    

    public int selectedRowIndex() {
        return myEOTable.getSelectedRow();
    }

	/**
	 * Annule la saisie en cours dans la celule si celle-ci est en mode édition.
	 */
    public void cancelCellEditing() {
        myEOTable.cancelCellEditing();
    }
    public boolean isEditing() {
        return myEOTable.isEditing();
    }

    /**
     * Termine l'édition en cours dans la cellule éventuellement en mode edition.
     * @return True si l'édition a bien été validée.
     */
    public boolean stopCellEditing() {
        return myEOTable.stopCellEditing();
    }

    public EODisplayGroup getMyDisplayGroup() {
        return myDisplayGroup;
    }


    /**
     *
     */
    public void fireTableDataChanged() {
        myEOTable.getDataModel().fireTableDataChanged();
    }

    public void fireTableCellUpdated(final int row, final int col) {
        myEOTable.getDataModel().fireTableCellUpdated(row, myEOTable.convertColumnIndexToModel(col));
    }
    public void fireTableRowUpdated(final int row) {
        myEOTable.getDataModel().fireTableRowUpdated(row);
    }


    public ZEOTable getMyEOTable() {
        return myEOTable;
    }

    
    
    /**
     * 
     * @param columnModelIndex
     * @return
     */
    public TableColumn getTableColumnAtIndex(int columnModelIndex) {
        final Enumeration enumeration = myEOTable.getColumnModel().getColumns();
        for (; enumeration.hasMoreElements(); ) {
            final TableColumn col = (TableColumn)enumeration.nextElement();
            if (col.getModelIndex() == columnModelIndex) {
                return col;
            }
        }
        return null;
    }
        

}
