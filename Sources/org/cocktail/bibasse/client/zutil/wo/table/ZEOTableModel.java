/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil.wo.table;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.table.AbstractTableModel;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Modele à associer à une JTable qui permet d'affichier le contenu d'un displayGroup.
 *
 * @see org.cocktail.zutil.client.wo.ZEOTable
 * @see org.cocktail.zutil.client.wo.ZEOSelectionDialog
 * @author Rodolphe Prin
 */
public class ZEOTableModel extends AbstractTableModel {
	protected EODisplayGroup myDg;
	protected int innerColCount;
	protected int innerRowCount;
	protected ArrayList colTitles;
	protected ArrayList colAttributeNames;
	protected ArrayList myColumns;
	protected IZEOTableModelProvider myProvider;

	public ZEOTableModel(EODisplayGroup dg, Collection vColumns) {
	    super();
		myDg = dg;

		myColumns = new ArrayList(vColumns.size());
		myColumns.addAll(vColumns);

		final int x = vColumns.size();
		colAttributeNames = new ArrayList(x);
		colTitles = new ArrayList(x);

		for (int i = 0; i < x; i++) {
			colAttributeNames.add( ((ZEOTableModelColumn)myColumns.get(i)).getAttributeName());
			colTitles.add(((ZEOTableModelColumn)myColumns.get(i)).getTitle() );
		}
		updateInnerRowCount();
		updateInnerColCount();
	}

	public int getColumnCount() {
		return innerColCount;
	}

	public int getRowCount() {
		return innerRowCount;
	}

	/**
	 * Récupère la valeur depuis le displaygroup
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		return ((ZEOTableModelColumn) myColumns.get(columnIndex) ).getValueAtRow(rowIndex);
	}

	/**
	 * Renvoie la classe d'une colonne pour permettre à Swing d'afficher la bonne case.
	 * @see javax.swing.table.TableModel#getColumnClass(int)
	 */
	public Class getColumnClass(final int columnIndex) {
	    Class res=null;
		if (getColumn(columnIndex).getColumnClass()==null   ) {
		    res = super.getColumnClass(columnIndex);
		}
		else {
		    res = getColumn(columnIndex).getColumnClass();
		}
		return res;
	}

	public String getColumnName(final int column) {
		return (String) colTitles.get(column);
	}

	public boolean isCellEditable(final int rowIndex, final int columnIndex) {
	    boolean res = false;
		if (myProvider == null) {
		    res = getColumn(columnIndex).isEditable();
		} else {
		    res = (getColumn(columnIndex).isEditable() && myProvider.isRowEditable(rowIndex));
		}
		return res;
	}

	protected void updateSelection(final NSArray newSel) {
		myDg.setSelectedObjects(newSel);
	}

	/**
	 * Met à jour la selection du displaygroup
	 * @param array tableau des index du modele selectionnes.
	 */
	public void updateSelectedIndexes(final NSArray array) {
		final NSMutableArray sel = new NSMutableArray();
//		sel.removeAllObjects();
		for (int i = 0; i < array.count(); i++) {
//			sel.addObject((EOEnterpriseObject) myDg.displayedObjects().objectAtIndex(((Integer)array.objectAtIndex(i)).intValue()) );
		    //Normalement la référence à EOEnterpriseObject est inutile
			sel.addObject(myDg.displayedObjects().objectAtIndex(((Integer)array.objectAtIndex(i)).intValue()) );
		}
		updateSelection(sel);
	}




	/**
	 * Renvoie les objets sélectionnés dans le displaygroup
	 */
	public NSArray getSelectedEOObjects() {
		return myDg.selectedObjects();
	}


	public Object getSelectedObject() {
	    return myDg.selectedObject();
	}


	/**
	 * @return
	 */
	public EODisplayGroup getMyDg() {
		return myDg;
	}

	/**
	 * @return
	 */
	public int getInnerRowCount() {
		return innerRowCount;
	}

	/**
	 * @param i
	 */
	public void setInnerRowCount(final int i) {
		innerRowCount = i;
	}

	/**
	 * @return
	 */
//	public Vector getColAttributeNames() {
//		return colAttributeNames;
//	}


	/**
	 * A appeler lorsque le nombre d'enregistrement du modele change
	 * (ce n'est pas fait en dynamique pour des raisons de performance).
	 */
	public final void updateInnerRowCount() {
		innerRowCount = myDg.displayedObjects().count();
	}

	public final void updateInnerColCount() {
		innerColCount = colAttributeNames.size();
	}

	/**
	 * Renvoie le nom de l'attribut correspondant à l'index de colonne
	 * @param i
	 */
	public String getAttributeNameForColIndex(final int i) {
		return (String)colAttributeNames.get(i);
	}


	public ZEOTableModelColumn getColumn(final int i) {
		return (ZEOTableModelColumn) myColumns.get(i);
	}

//	public ZEOTableModelColumn getColumn(String attName) {
//		return (ZEOTableModelColumn)myColumns.elementAt(colAttributeNames.indexOf(attName));
//	}


		/*
	   * Méthode à implémenter dès qu'on n'est pas en lecture seule.
	   */
	 public void setValueAt(final Object value, final int row, final int col) {
		if (getColumn(col).isEditable() ) {
			getColumn(col).setValueAtRow(value, row);
			fireTableCellUpdated(row, col);
		}
	 }



	/**
	 * Renvoie toutes les EOs dont la colonne est égale à value.
	 * @param value
	 * @param col
	 * @return
	 */
	public ArrayList getRowsForValueAtCol(final Object value, final int col) {
		final ArrayList res = new ArrayList();
		for (int i = 0; i < innerRowCount; i++) {
			if (getValueAt(i, col).equals(value) ) {
				res.add(myDg.displayedObjects().objectAtIndex(i));
			}
		}
		return res;
	}

	/**
	 * Change la valeur d'une colonne pour tous les rows.
	 * @param value
	 * @param col
	 */
	public void setValueForCol(final Object value, final int col) {
		for (int i = 0; i < innerRowCount; i++) {
			setValueAt(value, i, col);
		}
	}




	/**
	 * @return
	 */
	public ArrayList getMyColumns() {
		return myColumns;
	}

	/**
	 * Méthode à appeler pour informer le modèle qu'il doit rafraichir l'affichage l'ensemble de la table à partir des données.
	 * L'appel à cette méthode entraine la perte de la ligne sélectionnée par l'utilisateur.
	 * Si les données mises à jour se limitent à quelques cellules, utiliser plutot fireTableCellUpdated.
	 */
	public void fireTableDataChanged() {
//	    System.out.println("ZEOTableModel.fireTableDataChanged() "+this);
		super.fireTableDataChanged();
	}


	/**
	 * Méthode à utiliser pour informer le modele que la valeur affichée dans une cellule donnée doit être rafrachie (la donnée a été mide à jour).
	 * Cette méthode à l'avantage de conserver la sélection de l'utilisateur, contrairement à la méthode fireTableDataChanged().
	 */
	public void fireTableCellUpdated(final int row, final int col) {
		super.fireTableCellUpdated(row, col);
	}
	public void fireTableRowUpdated(final int row) {
	    for (int i=0;i<innerColCount;i++) {
	        fireTableCellUpdated(row,i);
	    }
	}


	public interface IZEOTableModelProvider {
		/**
		 * Indique si une ligne est editable (à utiliser en combinaison avec setEditable() de la colonne). Si non défini on considere que la ligne est editable.
		 */
		public boolean isRowEditable(int row);
	}


	/**
	 * @return
	 */
	public IZEOTableModelProvider getMyProvider() {
		return myProvider;
	}

	/**
	 * @param provider
	 */
	public void setMyProvider(final IZEOTableModelProvider provider) {
		myProvider = provider;
	}

}
