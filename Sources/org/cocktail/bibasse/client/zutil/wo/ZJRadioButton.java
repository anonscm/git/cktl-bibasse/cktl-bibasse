/* ZJRadioButton.java created by rprin on Tue 23-Sep-2003 */
package org.cocktail.bibasse.client.zutil.wo;

import java.awt.Dimension;

import javax.swing.JRadioButton;

import com.webobjects.eocontrol.EOEnterpriseObject;

/**
* Classe représentant un bouton radio auquel on associe un objet EOEnterpriseObject.
 * @see ZEOMatrix
 */
public class ZJRadioButton extends JRadioButton {
    /** Objet métier associé*/
    private EOEnterpriseObject eo;

    public ZJRadioButton(EOEnterpriseObject peo, String libelle,boolean selected, Dimension dimension) {
        super(libelle, selected);
        this.setName(libelle);
        this.setToolTipText(libelle);
        this.setSize(dimension);
        eo=peo;
    }

    public EOEnterpriseObject eo() {
        return eo;
    }

}