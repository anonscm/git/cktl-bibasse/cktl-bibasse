package org.cocktail.bibasse.client.zutil.wo;

import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;



public class CRIWaitingFrame2 extends JFrame {
    // Titre de la fenetre, texte d'intro, texte du message
    String title, intro, message;
    JComponent myContentPane;
    // Les labels pour les textes : resp. intro et message
    JLabel sujet, texte;

    public CRIWaitingFrame2(String aTitle, String anIntro, String aMessage) {
        super(aTitle);

        // Initialisations
        title = aTitle;
        message = aMessage;
        intro = anIntro;
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        // Recuperer les dimensions de l'ecran
        int x = (int)this.getGraphicsConfiguration().getBounds().getWidth();
        int y = (int)this.getGraphicsConfiguration().getBounds().getHeight();

        myContentPane = createUI();
        myContentPane.setOpaque(true);
        this.setContentPane(myContentPane);
        this.setResizable(false);
        this.setLocation((x/2)-((int)this.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)this.getContentPane().getMinimumSize().getHeight()/2)));

        //Display the window.
        this.pack();
        this.setVisible(true);
        this.paintAll(this.getGraphics());
    }


    /**
        * Constructeur pour ne pas l'afficher directement.
     */
    public CRIWaitingFrame2(String aTitle, String anIntro, String aMessage, boolean hide) {
        super(aTitle);

        // Initialisations
        title = aTitle;
        message = aMessage;
        intro = anIntro;
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        // Recuperer les dimensions de l'ecran
        int x = (int)this.getGraphicsConfiguration().getBounds().getWidth();
        int y = (int)this.getGraphicsConfiguration().getBounds().getHeight();

        myContentPane = createUI();
        myContentPane.setOpaque(true);
        this.setContentPane(myContentPane);
        this.setResizable(false);
        this.setLocation((x/2)-((int)this.getContentPane().getMinimumSize().getWidth()/2), ((y/2)-((int)this.getContentPane().getMinimumSize().getHeight()/2)));
        this.pack();
        if (!hide) {
			show();	
        }
    }

    public void showTheFrame() {
        //Display the window.
        this.show();
        this.paintAll(this.getGraphics());
        validate();
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }


    public void hideTheFrame() {
        this.hide();
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    
    private JPanel createUI() {
        //Create the labels.
        sujet = new JLabel(intro);
        texte = new JLabel(message);
        sujet.setLabelFor(texte);

        //Create the panel we'll return and set up the layout.
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(20,20,10,20));
        sujet.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        texte.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Add the labels to the content pane.
        panel.add(sujet);
        panel.add(Box.createVerticalStrut(5)); //extra space
        panel.add(texte);

        //Add a vertical spacer that also guarantees us a minimum width:
        panel.add(Box.createRigidArea(new Dimension(150,10)));

        return panel;
    }


    

    
    public void setTitle(String aTitle) {
        title = aTitle;
        this.setTitle(title);
        this.paintAll(this.getGraphics());
    }

    public void setIntro(String anIntro)
    {
        intro = anIntro;
        sujet.setText(intro);
        this.paintAll(this.getGraphics());
    }

    public void setMessage(String aMessage) {
        message = aMessage;
        texte.setText(message);
        this.paintAll(this.getGraphics());
        validate();
    }
}
