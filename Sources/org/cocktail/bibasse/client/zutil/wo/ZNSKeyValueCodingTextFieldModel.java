package org.cocktail.bibasse.client.zutil.wo;

import org.cocktail.bibasse.client.zutil.ui.forms.ZTextField.IZTextFieldModel;

import com.webobjects.foundation.NSKeyValueCodingAdditions;

/**
 * Modele pour afficher le contenu d'un objet respectant l'interface NSKeyValueCodingAdditions 
 * @author rodolphe.prin@univ-lr.fr
 */
public abstract class ZNSKeyValueCodingTextFieldModel implements IZTextFieldModel {
    private final String _keyInObject;

    public ZNSKeyValueCodingTextFieldModel(final String keyInObject) {
        _keyInObject = keyInObject;
    }

    protected abstract NSKeyValueCodingAdditions getObject();


    public Object getValue() {
//        System.out.println("ZNSKeyValueCodingTextFieldModel.getValue() = " + getObject());
        if (getObject()==null) {
            return null;
        }
        final Object val = (_keyInObject.indexOf(".")>0 ? getObject().valueForKeyPath(  _keyInObject  ) : getObject().valueForKey(  _keyInObject  ));
//        System.out.println("ZNSKeyValueCodingTextFieldModel.getValue()=" + val);
        return val;
    }

    public void setValue(Object value) {
//        System.out.println("ZNSKeyValueCodingTextFieldModel.setValue() :  "+value );
//        System.out.println("ZNSKeyValueCodingTextFieldModel.getObject() :  "+getObject() );
        if (getObject()!=null) {
            if (_keyInObject.indexOf(".")>0) {
                getObject().takeValueForKeyPath(value, _keyInObject);
            }
            else {
                getObject().takeValueForKey(value, _keyInObject);
            }
        }
    }

}