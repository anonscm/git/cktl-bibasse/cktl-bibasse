/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil.wo;


import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZCommonDialog;
import org.cocktail.bibasse.client.zutil.ui.forms.ZActionField;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;



/**
 *
 *
 * @author Rodolphe Prin
 */
public class ZEOSrchDialog extends ZCommonDialog implements ZEOTable.ZEOTableListener  {
	private TableSorter myTableSorter;
	protected	ApplicationClient	myApp;
//	protected int modalResult;
//	private Window upperWindow;
	protected ZEOTableModel myTableModel;
	protected ZEOTable	myEOTable;
	private String 		topMessage;
	protected ZActionField	filterTextField;
	private EODisplayGroup _myDg;
	private Vector _myCols;
	private AbstractAction srchAction;
	protected JLabel tooltipIcon;

	/**
	 * @param win Dialog parent.
	 * @param title Titre à afficher dans la barre de titre
	 * @param dg DisplayGroup contenant les objets métiers
	 * @param cols Vector contenant des objets ZEOTableModelColumn
	 * @param message Message texte à afficher en haut de la fenêtre de sélection
	 */
	public ZEOSrchDialog(Dialog win, String title, EODisplayGroup dg, Vector cols, String message, AbstractAction action, EOEditingContext ec) {
		super(win,title, true);
		_myDg = dg;
		_myCols = cols;
		setModal(true);
//		upperWindow = win;
		myApp = (ApplicationClient)EOApplication.sharedApplication();
		srchAction = action;
		topMessage = message;
		initGUI();
	}


	public ZEOSrchDialog(Frame win, String title, EODisplayGroup dg,  Vector cols, String message, AbstractAction action) {
		super(win,title, true);
		_myDg = dg;
		_myCols = cols;
		setModal(true);
//		upperWindow = win;
		myApp = (ApplicationClient)EOApplication.sharedApplication();
		srchAction = action;
		topMessage = message;
		initGUI();
	}



	protected ZEOSrchDialog(Frame win, String title) {
		super(win,title, true);
		myApp = (ApplicationClient)EOApplication.sharedApplication();
//		upperWindow = win;
	}

	protected ZEOSrchDialog(Dialog win, String title) {
		super(win,title, true);
		myApp = (ApplicationClient)EOApplication.sharedApplication();
//		upperWindow = win;
	}

	protected void initDialog(EODisplayGroup dg,  Vector cols, String message, AbstractAction action) {
		_myDg = dg;
		_myCols = cols;
		setModal(true);

		myApp = (ApplicationClient)EOApplication.sharedApplication();
		srchAction = action;
		topMessage = message;
		initGUI();
	}

	private void initGUI() {
		initTableModel();
		initTable();
		JPanel mainPanel = new JPanel();
		JPanel tmpPanel;
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		tmpPanel = buildTopPanel();
		if (tmpPanel!=null) {
			mainPanel.add(tmpPanel, BorderLayout.PAGE_START);
		}
		tmpPanel = buildRightPanel();
		if (tmpPanel!=null) {
			mainPanel.add(tmpPanel, BorderLayout.LINE_END);
		}
		tmpPanel = buildBottomPanel() ;
		if (tmpPanel!=null) {
			mainPanel.add(tmpPanel, BorderLayout.PAGE_END);
		}
		tmpPanel = buildCenterPanel();
		if (tmpPanel!=null) {
			mainPanel.add(tmpPanel, BorderLayout.CENTER);
		}
		this.setContentPane(mainPanel);
		this.pack();
		this.validate();
	}



	protected void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

	}

	protected void initTableModel() {
		myTableModel = new ZEOTableModel(_myDg, _myCols);
		myTableSorter = new TableSorter (myTableModel);
	}

	protected JPanel buildTitle() {
		if (topMessage!=null) {
			JPanel title=new JPanel(new BorderLayout());
			title.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			JLabel msgLabel = new JLabel(topMessage);
			title.add(msgLabel, BorderLayout.LINE_START);
			title.add(new JPanel());
			return title;
		}
        return null;
	}

	protected JPanel buildFilterBox() {
		if (srchAction != null) {
			JPanel filterPanel = new JPanel();
			filterPanel.setLayout(new BorderLayout());
			filterPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			filterTextField = new ZActionField(srchAction);
//			filterTextField = new ZLabelActionField("Rechercher", srchAction);
			filterTextField.getMyTexfield().setColumns(40);
			filterPanel.add(filterTextField, BorderLayout.LINE_START);
			filterPanel.add(new JPanel(), BorderLayout.CENTER);
			return filterPanel;
		}
        return null;
	}

	protected JPanel buildButtonsPanel() {
		Action actionOk = new AbstractAction("Ok") {
			public void actionPerformed(ActionEvent e) {
                System.out.println("Ok.actionPerformed()");
				onOkClick();
			}
		};
		actionOk.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);

		Action actionCancel = new AbstractAction("Annuler") {
			public void actionPerformed(ActionEvent e) {
                System.out.println("cancel.actionPerformed()");
				onCancelClick();
			}
		};
		actionCancel.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);

		JButton btOk = new JButton(actionOk);
		JButton btCancel = new JButton(actionCancel);
		Dimension btSize = new Dimension(95,24);
		btOk.setMinimumSize(btSize);
		btCancel.setMinimumSize(btSize);
		btOk.setPreferredSize(btSize);
		btCancel.setPreferredSize(btSize);

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(btOk);
		buttonPanel.add(btCancel);
		return buttonPanel;
	}



	/**
	 * Construit le panel du haut (titre, boite de sélection)
	 * @return
	 */
	protected JPanel buildTopPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		Box box = Box.createVerticalBox();

		JPanel tmpPanel1 = buildTitle();
		if (tmpPanel1!=null) {
			Box box1 = Box.createHorizontalBox();
			box1.add(tmpPanel1);
			box1.add(Box.createHorizontalGlue());
			box.add(box1);
		}

		JPanel tmpPanel2 = buildFilterBox();
		if (tmpPanel2!=null) {
			Box box1 = Box.createHorizontalBox();
			if (tooltipIcon!=null) {
			    box1.add(tooltipIcon);
			}
			box1.add(tmpPanel2);
			box1.add(Box.createHorizontalGlue());
			box.add(box1);
		}
		box.add(Box.createVerticalGlue());
		panel.add(box);
		return panel;
	}



	protected JPanel buildBottomPanel() {
		return buildButtonsPanel();
	}

	protected JPanel buildRightPanel() {
		return null;
	}

	protected JPanel buildCenterPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JScrollPane myJscrollPane = new JScrollPane(myEOTable);
		panel.add(myJscrollPane, BorderLayout.CENTER);
		return panel;
	}

	/**
	 * Ouvre la fenetre en modal et renvoie le résultat (via getModalResult).
	 */
	public int  open()  {
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE );
		setModalResult(MRCANCEL);
		centerWindow();
		show();
		//en modal la fenetre reste active jusqu'au closeWindow...
		return getModalResult();
	}





	/**
	 *
	 */
	public int getModalResult() {
		return modalResult;
	}




//	/**
//	 * @param window
//	 */
//	public void setUpperWindow(Window window) {
//		upperWindow = window;
//	}


	public NSArray getSelectedEOObjects() {
		return myTableModel.getSelectedEOObjects();
	}

	public EOEnterpriseObject getSelectedEOObject() {
		return (EOEnterpriseObject)get_myDg().selectedObject();
	}



	public void setSelectionMode(int selMode) {
		myEOTable.setSelectionMode(selMode);
	}




	/**
	 * @return
	 */
	public EODisplayGroup get_myDg() {
		return _myDg;
	}

	/**
	 * @param group
	 */
	public void set_myDg(EODisplayGroup group) {
		_myDg = group;
	}

	public String getFilterText() {
		return filterTextField.getMyTexfield().getText();
	}


    public void onDbClick() {
        onOkClick();
    }


    public void onSelectionChanged() {
       return;
        
    }


    public final ZEOTable getMyEOTable() {
        return myEOTable;
    }



}
