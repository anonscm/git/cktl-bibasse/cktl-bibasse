/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Simbud.
 *
 * Simbud is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Simbud is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil.wo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;

import org.cocktail.fwkcktlwebapp.common.CktlDockClient;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

/**
 *
 *
 * @author Rodolphe Prin
 */
public class ZEOApplication extends EOApplication {
	private EOEditingContext			editingContext;
	public EOClientResourceBundle 		resourceBundle;

	/**
	 *
	 */
	public ZEOApplication() {
		super();
		editingContext = new EOEditingContext();
		resourceBundle = new EOClientResourceBundle();
	}


	public EOEditingContext editingContext()  {
		return editingContext;
	}
	public EOEditingContext getEditingContext()  {
		return editingContext;
	}

	public final String getIpAdress() {
	    try {
	        final String s = java.net.InetAddress.getLocalHost().getHostAddress();
	        final String s2 = java.net.InetAddress.getLocalHost().getHostName() ;
            return s+" / " + s2;
        } catch (UnknownHostException e) {
           return "Machine inconnue";

        }
	}




	/**
     * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
     */
    public final class MyByteArrayOutputStream extends ByteArrayOutputStream {
        private PrintStream out;
        public MyByteArrayOutputStream() {
            this(System.out);
        }
        public MyByteArrayOutputStream(PrintStream out) {
            super();
            this.out = out;
        }
        public synchronized void write(final int b) {
            super.write(b);
            out.write(b);
        }
        public synchronized void write(final byte[] b, final int off, final int len) {
                super.write(b, off, len);
                out.write(b, off, len);
        }
    }


	/**
     * Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
     */
    public final class MyFileOutputStream extends FileOutputStream {
        private final PrintStream out;

        public MyFileOutputStream(PrintStream out, File file) throws FileNotFoundException {
            super(file);
            this.out = out;
        }
        public synchronized void write(final int b) throws IOException {
            super.write(b);
            out.write(b);
        }
        public synchronized void write(final byte[] b, final int off, final int len) throws IOException {
                super.write(b, off, len);
                out.write(b, off, len);
        }
    }

    public String getCASUserName() {
        final NSDictionary arguments = arguments();
        final String dockPort = (String) arguments.valueForKey("LRAppDockPort");
        final String dockTicket = (String) arguments.valueForKey("LRAppDockTicket");
        String uname = null;
        if (dockPort!=null && dockTicket != null) {
            uname = CktlDockClient.getNetID(null, dockPort, dockTicket);
        }
        return uname;
    }


//
//    /**
//     * vérifie si la version currentVersion est >= à minVersion
//     * @param currentVersion doit etre de la forme xx.xx.xx
//     * @param minVersion doit etre de la forme xx.xx.xx
//     * @return
//     */
//    protected boolean checkCurrentJREVersionVsMin(final String currentVersion, final String minVersion) {
//        NSArray lesnummin = NSArray.componentsSeparatedByString(minVersion, ".");
//        NSArray lesnumactuel = NSArray.componentsSeparatedByString(currentVersion, ".");
//        String min, cur;
//        boolean res = true;
//        for (int i = 0; i < 2; i++) {
//            min = (String) lesnummin.objectAtIndex(i);
//            cur = (String) lesnumactuel.objectAtIndex(i);
//            if ((new Integer(cur)).intValue() < (new Integer(min)).intValue()) {
//                res = false;
//            }
//        }
//        return res;
//    }
//
//    /**
//     * vérifie si la version currentVersion est > à maxVersion
//     *
//     * @param currentVersion doit etre de la forme xx.xx.xx
//     * @param maxVersion doit etre de la forme xx.xx.xx
//     * @return
//     */
//    protected boolean checkCurrentJREVersionVsMax(final String currentVersion, final String maxVersion) {
////        final String maxx = maxVersion.replaceAll(".", "");
////        final String cur = currentVersion.replaceAll(".", "");
//        boolean res = true;
//        if (currentVersion.compareTo(maxVersion) >= 0) {
//            res = false;
//        }
//        return res;
//    }
//

    public String getJREVersion() {
        return System.getProperty("java.version");
    }


}
