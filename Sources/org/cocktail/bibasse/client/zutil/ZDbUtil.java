/* ZDbUtil.java created by rprin on Tue 06-May-2003 */

package org.cocktail.bibasse.client.zutil;
import com.webobjects.foundation.NSKeyValueCoding;



public class ZDbUtil  {
    
	/**
	  * Renvoie la valeur a inserer dans la base de donnees.
	 * La valeur est "NullValue", si le parametre est null,
	 * et la meme chaine de caracteres, sinon.
	 */
	public static Object dbValueForObject(Object anObject) {
	  Object result;
	  if (anObject instanceof String) {
		if ((anObject == null) || (((String)anObject).length() == 0))
		  result = NSKeyValueCoding.NullValue;
		else
		  result = anObject;
	  } else {
		if (anObject == null)
		  result = NSKeyValueCoding.NullValue;
		else
		  result = anObject;
	  }
	  return result;
	}        

    
	public static Integer dbIntegerValueForObject(Object anObject) {
		Integer result=null;
	  if (anObject instanceof String) {
		if ((anObject == null) || (((String)anObject).length() == 0))
		  result = null;
		else
		  result = new Integer(anObject.toString());
	  } else {
		if (anObject instanceof Number)
		  result = new Integer(  ((Number)anObject).intValue() );
	  }
	  return result;
	}     
    
}
