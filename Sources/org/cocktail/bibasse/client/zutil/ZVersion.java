/*
 * Copyright COCKTAIL, 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.client.zutil;

import java.util.StringTokenizer;

public class ZVersion implements Comparable {
    private int m_major;
    private int m_minor;
    private int m_micro;

    /**
     * Crée un nouvel objet.
     *
     * @param version Chaine du type xxx.yyy.zzz . Si la chaine se termine par _nnn, ce n'est pas pris en compte.
     * @return
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     * @throws NullPointerException
     */
    public static ZVersion getVersion(String version) throws NumberFormatException, IllegalArgumentException {
        if (version == null) {
            throw new NullPointerException("version");
        }

        if (version.indexOf("_")>-1 ) {
            version = version.substring(0, version.indexOf("_"));
        }

        final StringTokenizer tokenizer = new StringTokenizer(version, ".");
        final String[] levels = new String[tokenizer.countTokens()];
        for (int i = 0; i < levels.length; i++) {
            levels[i] = tokenizer.nextToken();
        }

        int major = -1;
        if (0 < levels.length) {
            major = Integer.parseInt(levels[0]);
        }

        int minor = 0;
        if (1 < levels.length) {
            minor = Integer.parseInt(levels[1]);
        }

        int micro = 0;
        if (2 < levels.length) {
            micro = Integer.parseInt(levels[2]);
        }

        return new ZVersion(major, minor, micro);
    }


    public ZVersion(final int major, final int minor, final int micro) {
        m_major = major;
        m_minor = minor;
        m_micro = micro;
    }


    public int getMajor() {
        return m_major;
    }


    public int getMinor() {
        return m_minor;
    }


    public int getMicro() {
        return m_micro;
    }

    public boolean complies(final ZVersion other) {
        if (other == null)
            return false;

        if (other.m_major == -1) {
            return true;
        }
        if (m_major != other.m_major) {
            return false;
        } else if (m_minor < other.m_minor) {
            return false;
        } else if (m_minor == other.m_minor && m_micro < other.m_micro) {
            return false;
        } else {
            return true;
        }
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + m_major;
		result = prime * result + m_micro;
		result = prime * result + m_minor;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ZVersion other = (ZVersion) obj;
		if (getMajor() != other.getMajor()) {
			return false;
		}
		if (getMinor() != other.getMinor()) {
			return false;
		}
		if (getMicro() != other.getMicro()) {
			return false;
		}

		return true;
	}

	public String toString() {
        return m_major + "." + m_minor + "." + m_micro;
    }

    public int compareTo(Object o) {
        if (o == null)
            throw new NullPointerException("o");

        ZVersion other = (ZVersion) o;
        int val = 0;

        if (getMajor() < other.getMajor())
            val = -1;
        if (0 == val && getMajor() > other.getMajor())
            val = 1;

        if (0 == val && getMinor() < other.getMinor())
            val = -1;
        if (0 == val && getMinor() > other.getMinor())
            val = 1;

        if (0 == val && getMicro() < other.getMicro())
            val = -1;
        if (0 == val && getMicro() > other.getMicro())
            val = 1;

        return val;
    }
}