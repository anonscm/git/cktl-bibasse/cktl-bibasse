package org.cocktail.bibasse.client.zutil;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Copyright (C) 2003 Université de La Rochelle
 *
 * This file is part of TestXml.
 *
 * TestXml is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * TestXml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * 
 * 
 * @author rprin
 */
public class ZChronometre {
	public static final String DEFAULTDATEFORMAT="HH:mm:ss ";
	
	private long startTime=0;
	
	private long endTime=0;
	
	private long lastTop=0;
	
	private long timeSinceLastTop=0;
	
	private long totalTime=0;
	
	private String dateFormat=DEFAULTDATEFORMAT;

	private SimpleDateFormat mySimpleDateFormat;
	
	private String name;
	
	/**
	 * 
	 */
	public ZChronometre(String pname) {
		super();
		reset();
		name = pname;
	}
	
	public void reset() {
		startTime = 0;
		endTime=0;
		lastTop=0;
		timeSinceLastTop=0;
		totalTime=0;
		mySimpleDateFormat = new SimpleDateFormat(dateFormat);
	}
	
	public void start(String mess) {
		startTime = (new Date()).getTime();
		lastTop = startTime;
		logStart(mess);
	}
	
	public void start() {
		start(null);
	}
	
	public void top(String mess) {
		Date now = new Date();
		timeSinceLastTop = now.getTime() - lastTop;
		lastTop = now.getTime();
		logTop(mess);		
	}
	public void top() {
		top(null);
	}	
	public void stop(String mess) {
		top(mess);
		endTime = lastTop;
		totalTime = endTime - startTime;
		logStop(mess);	
	}
	
	
	
	private void log() {
		System.out.print( "["+ name +"] ");
		System.out.print( mySimpleDateFormat.format(new Date(lastTop)));	
	}
	
	private void logStop(String mess) {
		if (mess!=null) {
			System.out.print(mess);		
		} 
		else {
			System.out.print("Arret");
		}
		System.out.print(" / Duree totale mesuree : ");
		System.out.print( String.valueOf( totalTime+" ms")  );
		System.out.println();				
	}
	
	private void logStart(String mess) {
		log();
		if (mess!=null) {
			System.out.print(mess);		
		} else {
			System.out.print("Demarrage");
		}
		System.out.println();				
	}	

	private void logTop(String mess) {
		log();
		if (mess!=null) {
			System.out.print(" "+mess);		
		}		
		System.out.print(" / Duree depuis le dernier top : ");
		System.out.print(String.valueOf( timeSinceLastTop+" ms")  );
		System.out.println();		
		
		System.out.println();				
	}	

	/**
	 * @return
	 */
	public String getDateFormat() {
		return dateFormat;
	}

	/**
	 * @param string
	 */
	public void setDateFormat(String string) {
		dateFormat = string;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}


}
