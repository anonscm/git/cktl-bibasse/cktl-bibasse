/*
 * Copyright (C) 2003 Université de La Rochelle
 *
 * This file is part of PrestationWeb.
 *
 * PrestationWeb is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * PrestationWeb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.bibasse.client.zutil;

/**
 * Permet de mémoriser dans un StringBuffer tout en les affichant dans la console des messages de log.
 * Utile pour les logs d'initialisation des applications, quand on veut envoyer le résultat de l'initialisation par mail par exemple.
 *
 * @author rprin
 * @deprecated
 */
public class ZLogWithBuffer extends ZLog {
        private StringBuffer stringBuffer;

        public ZLogWithBuffer() {
                super();
                stringBuffer = new StringBuffer();
        }

        /**
         * Ajoute uen ligne au log, ainsi qu'au buffer.
         * @param str
         */
        /*
        public void append(String str ) {
                super.append(str);
                stringBuffer.append(str+"\n");
        }

        */
        public String toString() {
                return stringBuffer.toString();
        }



        protected void directWriteln(final String s) {
                super.directWriteln(s);
                stringBuffer.append(s+"\n");
        }

        protected void  writeln(final String s, final boolean show) {
                super.writeln(s, show);
                if (show) {
                        stringBuffer.append(s+"\n");
                }
        }



}
