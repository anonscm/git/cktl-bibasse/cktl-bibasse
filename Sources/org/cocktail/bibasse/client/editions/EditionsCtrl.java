
package org.cocktail.bibasse.client.editions;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.cocktail.bibasse.client.ApplicationClient;
import org.cocktail.bibasse.client.ConstantesCocktail;
import org.cocktail.bibasse.client.ServerProxy;
import org.cocktail.bibasse.client.finder.FinderBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.metier.EOExercice;
import org.cocktail.bibasse.client.metier.EOOrgan;
import org.cocktail.bibasse.client.selectors.OrganSelectCtrl;
import org.cocktail.bibasse.client.ventilations.VentilationsHistorique;
import org.cocktail.bibasse.client.virements.VirementsHistorique;
import org.cocktail.bibasse.client.zutil.ui.ZAbstractPanel;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class EditionsCtrl extends CommonImprCtrl {

	// NATURE
	private final static String JASPER_NAME_BUDGET_NATURE_SAISIE = "bd_saisie_nature.jasper";
	private final static String JASPER_NAME_BUDGET_NATURE_SAISIE_DETAIL = "bd_saisie_natured.jasper";
	private final static String JASPER_NAME_BUDGET_NATURE_ANNUEL = "bd_annuel_nature.jasper";

	private final static String PDF_NAME_BUDGET_NATURE_SAISIE = "bd_saisie_nature.pdf";
	private final static String PDF_NAME_BUDGET_NATURE_SAISIE_DETAIL = "bd_saisie_natured.pdf";
	private final static String PDF_NAME_BUDGET_NATURE_ANNUEL = "bd_annuel_nature.pdf";

	// NATURE LOLF
	private final static String JASPER_NAME_BUDGET_NATURE_LOLF_SAISIE = "bd_saisie_nature_lolf.jasper";
	private final static String JASPER_NAME_BUDGET_NATURE_LOLF_ANNUEL = "bd_annuel_nature_lolf.jasper";

	private final static String PDF_NAME_BUDGET_NATURE_LOLF_SAISIE = "bd_saisie_nature_lolf.pdf";
	private final static String PDF_NAME_BUDGET_NATURE_LOLF_ANNUEL = "bd_annuel_nature_lolf.pdf";

	// GESTION DEPENSE
	private final static String JASPER_NAME_BUDGET_GESTION_SAISIE_DEPENSE = "bd_saisie_gestion_dep.jasper";
	private final static String JASPER_NAME_BUDGET_GESTION_SAISIE_DEPENSE_DETAIL = "bd_saisie_gestion_depd.jasper";
	private final static String JASPER_NAME_BUDGET_GESTION_ANNUEL_DEPENSE = "bd_annuel_gestion_dep.jasper";

	private final static String PDF_NAME_BUDGET_GESTION_SAISIE_DEPENSE = "bd_saisie_gestion_dep.pdf";
	private final static String PDF_NAME_BUDGET_GESTION_SAISIE_DEPENSE_DETAIL = "bd_saisie_gestion_depd.pdf";
	private final static String PDF_NAME_BUDGET_GESTION_ANNUEL_DEPENSE = "bd_annuel_gestion_dep.pdf";

	// GESTION RECETTE
	private final static String JASPER_NAME_BUDGET_GESTION_SAISIE_RECETTE = "bd_saisie_gestion_rec.jasper";
	private final static String JASPER_NAME_BUDGET_GESTION_SAISIE_RECETTE_DETAIL = "bd_saisie_gestion_recd.jasper";
	private final static String JASPER_NAME_BUDGET_GESTION_ANNUEL_RECETTE = "bd_annuel_gestion_rec.jasper";

	private final static String PDF_NAME_BUDGET_GESTION_SAISIE_RECETTE = "bd_saisie_gestion_rec.pdf";
	private final static String PDF_NAME_BUDGET_GESTION_SAISIE_RECETTE_DETAIL = "bd_saisie_gestion_recd.pdf";
	private final static String PDF_NAME_BUDGET_GESTION_ANNUEL_RECETTE = "bd_annuel_gestion_rec.pdf";

	private final static String JASPER_NAME_VENTILATIONS = "bd_ventilations.jasper";
	private final static String PDF_NAME_VENTILATIONS = "bd_ventilations.pdf";

	private final static String JASPER_NAME_VIREMENTS = "bd_virements.jasper";
	private final static String PDF_NAME_VIREMENTS_NATURE = "bd_virements_nature.pdf";
	private final static String PDF_NAME_VIREMENTS_GESTION = "bd_virements_gestion.pdf";

	private final static String JASPER_NAME_BUDGET_RCE = "budget_rce.jasper";
	private final static String PDF_NAME_BUDGET_RCE = "budget_rce.pdf";

	private final static String JASPER_NAME_PILOTAGE = "PilotageOrgan.jasper";
	private final static String PDF_NAME_PILOTAGE = "PilotageOrgan.pdf";

	private final static String JASPER_NAME_CTRL_SAISIE = "bibasse_report_ctrl.jasper";
	private final static String PDF_NAME_CTRL_SAISIE = "bibasse_report_ctrl.pdf";

	private static EditionsCtrl sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;

	private	JCheckBox	temUniv, temEtab, temUb, temCr, temDetailSaisie;
	private	CheckBoxListener checkBoxListener = new CheckBoxListener();

	protected	JFrame window;

	protected OrganSelectCtrl myOrganSelector;

	protected 	JPanel viewTableOrgan;
	protected	JComboBox budgetsSaisie;
	protected	JCheckBox checkBudgetGestionDepense, checkBudgetGestionRecette;

	private JLabel  labelGestionDetaille, labelNatureDetaille, labelNatureLolfDetaille, labelRce;

	protected ActionClose 					actionClose = new ActionClose();
	protected ActionRefreshBudgets 			actionRefreshBudgets = new ActionRefreshBudgets();
	protected ActionPrintBudgetGestion 		actionPrintBudgetGestion = new ActionPrintBudgetGestion();
	protected ActionPrintBudgetNature 		actionPrintBudgetNature = new ActionPrintBudgetNature();
	protected ActionPrintBudgetRce			actionPrintBudgetRce = new ActionPrintBudgetRce();
	protected ActionPrintBudgetNatureLolf	actionPrintBudgetNatureLolf = new ActionPrintBudgetNatureLolf();
	protected ActionPrintVentilations 		actionPrintVentilations = new ActionPrintVentilations();
	protected ActionPrintVirements 			actionPrintVirements = new ActionPrintVirements();
	protected ActionPrintPilotage 			actionPrintPilotage = new ActionPrintPilotage();

	private EOOrgan currentOrgan;
	private EOBudgetSaisie currentBudgetSaisie;

	private	BudgetSaisieListener budgetsListener = new BudgetSaisieListener();

	/**
	 *
	 *
	 */
	public EditionsCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();

		System.out.println("EditionsCtrl.EditionsCtrl() CONSTRUCTEUR !!!!!!!");

		if (myOrganSelector == null)
			myOrganSelector = new OrganSelectCtrl(ec, NSApp.getExerciceBudgetaire(), null, (NSArray)NSApp.getUserOrgans().valueForKey("organ"));

		// Notification de changement de ligne budgetaire
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("selectedOrganHasChanged",new Class[] {NSNotification.class}), "selectedOrganHasChanged",null);

		gui_initView();
	}

	/**
	 *
	 * @param editingContext
	 * @return
	 */
	public static EditionsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null) {
			sharedInstance = new EditionsCtrl(editingContext);
		}
		return sharedInstance;
	}

	public void exerciceHasChanged() {
		if (ec!=null) {
			sharedInstance=new EditionsCtrl(ec);
		}
		window.show(false);
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildPanelBudgetsSaisie()	{
		JPanel panel =new JPanel(new BorderLayout());

		JPanel panelTypesBudgets = new JPanel(new FlowLayout());
		panelTypesBudgets.add(new JLabel("Type de Budget : "));
		panelTypesBudgets.add(budgetsSaisie);

		JPanel panelNiveauImpression = new JPanel(new FlowLayout());
		panelNiveauImpression.add(new JLabel("Niveau d'impression : "));
		panelNiveauImpression.add(temUniv);
		panelNiveauImpression.add(temEtab);
		panelNiveauImpression.add(temUb);
		panelNiveauImpression.add(temCr);

		JPanel panelDetailSaisie = new JPanel(new FlowLayout());
		panelDetailSaisie.add(new JLabel("Détails : "));
		panelDetailSaisie.add(temDetailSaisie);

		panel.add(panelTypesBudgets, BorderLayout.NORTH);
		panel.add(panelNiveauImpression, BorderLayout.CENTER);
		panel.add(panelDetailSaisie, BorderLayout.SOUTH);

		return panel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildPanelGestion()	{

		JPanel panel =new JPanel(new BorderLayout());

		labelGestionDetaille = new JLabel("Budget de Gestion");

		JButton buttonPrint = new JButton(actionPrintBudgetGestion);
		buttonPrint.setPreferredSize(new Dimension(50,22));

		JPanel panelButtonGestion = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPrint}, BorderLayout.WEST);
		panelButtonGestion.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrintGestion = (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButtonGestion, labelGestionDetaille}, BorderLayout.WEST);
		panelPrintGestion.setBorder(BorderFactory.createEmptyBorder(2,20,2,0));

		JPanel panelCheckDepense = (JPanel)ZUiUtil.buildBoxLine(new Component[]{checkBudgetGestionDepense}, BorderLayout.WEST);
		panelCheckDepense.setBorder(BorderFactory.createEmptyBorder(5,20,2,0));

		JPanel panelCheckRecette = (JPanel)ZUiUtil.buildBoxLine(new Component[]{checkBudgetGestionRecette}, BorderLayout.WEST);
		panelCheckRecette.setBorder(BorderFactory.createEmptyBorder(5,20,2,0));

		JPanel panelCheckBoxs = (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelCheckDepense, panelCheckRecette}, BorderLayout.WEST);
		panelCheckBoxs.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		panel.add(ZUiUtil.buildBoxColumn(new Component[] {/*panelPrintGlobal, */panelPrintGestion, panelCheckBoxs}), BorderLayout.CENTER);

		return panel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildPanelNature()	{
		JPanel panel =new JPanel(new BorderLayout());

		labelNatureDetaille = new JLabel("Budget par NATURE");

		JButton buttonPrint = new JButton(actionPrintBudgetNature);
		buttonPrint.setPreferredSize(new Dimension(50,22));

		JPanel panelButtonGestion = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPrint}, BorderLayout.WEST);
		panelButtonGestion.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrintGestion = (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButtonGestion, labelNatureDetaille}, BorderLayout.WEST);
		panelPrintGestion.setBorder(BorderFactory.createEmptyBorder(2,20,2,0));

		panel.add(ZUiUtil.buildBoxColumn(new Component[] {panelPrintGestion}), BorderLayout.CENTER);

		return panel;
	}

	private JPanel gui_buildPanelNatureRce()	{
		JPanel panel =new JPanel(new BorderLayout());
		labelRce = new JLabel("Budget RCE");

		JButton buttonPrint = new JButton(actionPrintBudgetRce);
		buttonPrint.setPreferredSize(new Dimension(50,22));

		JPanel panelButton = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPrint}, BorderLayout.WEST);
		panelButton.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrint= (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButton, labelRce}, BorderLayout.WEST);
		panelPrint.setBorder(BorderFactory.createEmptyBorder(2,20,2,0));

		panel.add(ZUiUtil.buildBoxColumn(new Component[] {panelPrint}), BorderLayout.CENTER);
		return panel;
	}

	private JPanel gui_buildPanelNatureLolf()	{
		JPanel panel =new JPanel(new BorderLayout());
		labelNatureLolfDetaille = new JLabel("Repartition NATURE / LOLF");

		JButton buttonPrint = new JButton(actionPrintBudgetNatureLolf);
		buttonPrint.setPreferredSize(new Dimension(50,22));

		JPanel panelButton = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPrint}, BorderLayout.WEST);
		panelButton.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrint= (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButton, labelNatureLolfDetaille}, BorderLayout.WEST);
		panelPrint.setBorder(BorderFactory.createEmptyBorder(2,20,2,0));

		panel.add(ZUiUtil.buildBoxColumn(new Component[] {panelPrint}), BorderLayout.CENTER);
		return panel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildPanelVentilations()	{
		JPanel panel =new JPanel(new BorderLayout());

		JButton buttonPrint = new JButton(actionPrintVentilations);
		buttonPrint.setPreferredSize(new Dimension(50,22));

		JPanel panelButton = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPrint}, BorderLayout.WEST);
		panelButton.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrint = (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButton}, BorderLayout.WEST);
		panelPrint.setBorder(BorderFactory.createEmptyBorder(5,20,2,0));

		panel.add(ZUiUtil.buildBoxLine(new Component[] {panelPrint, panelPrint}), BorderLayout.CENTER);

		return panel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildPanelPilotage()	{
		JPanel panel =new JPanel(new BorderLayout());

		JButton buttonPilotage = new JButton(actionPrintPilotage);
		buttonPilotage.setPreferredSize(new Dimension(50,22));

		JPanel panelButton = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPilotage}, BorderLayout.WEST);
		panelButton.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrint = (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButton}, BorderLayout.WEST);
		panelPrint.setBorder(BorderFactory.createEmptyBorder(5,20,2,0));

		panel.add(ZUiUtil.buildBoxLine(new Component[] {panelPrint, panelPrint}), BorderLayout.CENTER);

		return panel;
	}

	/**
	 *
	 * @return
	 */
	private JPanel gui_buildPanelVirements()	{
		JPanel panel =new JPanel(new BorderLayout());

		JButton buttonPrint = new JButton(actionPrintVirements);
		buttonPrint.setPreferredSize(new Dimension(50,22));

		JPanel panelButton = (JPanel)ZUiUtil.buildBoxLine(new Component[]{buttonPrint}, BorderLayout.WEST);
		panelButton.setBorder(BorderFactory.createEmptyBorder(2,20,2,5));

		JPanel panelPrint = (JPanel)ZUiUtil.buildBoxLine(new Component[]{panelButton}, BorderLayout.WEST);
		panelPrint.setBorder(BorderFactory.createEmptyBorder(5,20,2,0));

		panel.add(ZUiUtil.buildBoxColumn(new Component[] {panelPrint, panelPrint}), BorderLayout.CENTER);

		return panel;
	}

	/**
	 *
	 *
	 */
	private void gui_initCheckBoxs()	{
		temUniv = new JCheckBox("Univ");
		temUniv.setFocusable(false);
		temEtab = new JCheckBox("ETAB");
		temEtab.setFocusable(false);
		temUb = new JCheckBox("UB");
		temUb.setFocusable(false);
		temCr = new JCheckBox("CR");
		temCr.setFocusable(false);

		temDetailSaisie = new JCheckBox("Imprimer le détail de la saisie");
		temDetailSaisie.setFocusable(false);
		temDetailSaisie.setSelected(true);
		temDetailSaisie.setEnabled(false);

		temUniv.addActionListener(checkBoxListener);
		temEtab.addActionListener(checkBoxListener);
		temUb.addActionListener(checkBoxListener);
		temCr.addActionListener(checkBoxListener);

		ButtonGroup group = new ButtonGroup();
		group.add(temUniv);
		group.add(temEtab);
		group.add(temUb);
		group.add(temCr);
	}

	/**
	 *
	 *
	 */
	private void gui_initComboBoxs()	{
		budgetsSaisie = new JComboBox();
		budgetsSaisie.setBackground(ConstantesCocktail.BG_COLOR_BLACK);
		budgetsSaisie.setForeground(ConstantesCocktail.BG_COLOR_YELLOW);
		budgetsSaisie.setPreferredSize(new Dimension(175, 21));

	}

	/**
	 *
	 *
	 */
	private void gui_initView()	{

		// Main Window
		window = new JFrame();
		window.setTitle("Gestion des Impressions - Budget " + NSApp.getExerciceBudgetaire().exeExercice());
		window.setSize(700,670);

		ArrayList arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelClose = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 100, 25));

		// CENTER ==> Types de credit, disponible et somme a ventiler

		gui_initCheckBoxs();

		gui_initComboBoxs();

		initBudgetsSaisie();

		checkBudgetGestionDepense = new JCheckBox("Dépense");
		checkBudgetGestionRecette = new JCheckBox("Recette");

		checkBudgetGestionDepense.setSelected(true);
		checkBudgetGestionRecette.setSelected(true);

		JPanel panelCheckBoxs = new JPanel(new BorderLayout());
		panelCheckBoxs.add(checkBudgetGestionDepense, BorderLayout.NORTH);
		panelCheckBoxs.add(checkBudgetGestionRecette, BorderLayout.SOUTH);

		JTextField titreGestion = new JTextField("Budget de GESTION");
		titreGestion.setFocusable(false);
		titreGestion.setEditable(false);
		titreGestion.setBackground(new Color(236,234,149));
		titreGestion.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JTextField titreNature = new JTextField("Budget par NATURE");
		titreNature.setFocusable(false);
		titreNature.setEditable(false);
		titreNature.setBackground(new Color(236,234,149));
		titreNature.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JTextField titreRce = new JTextField("Budget RCE");
		titreRce.setFocusable(false);
		titreRce.setEditable(false);
		titreRce.setBackground(new Color(236,234,149));
		titreRce.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JTextField titreNatureLolf = new JTextField("Repartition NATURE / LOLF");
		titreNatureLolf.setFocusable(false);
		titreNatureLolf.setEditable(false);
		titreNatureLolf.setBackground(new Color(236,234,149));
		titreNatureLolf.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JTextField titreVentilations = new JTextField("Ventilations");
		titreVentilations.setFocusable(false);
		titreVentilations.setEditable(false);
		titreVentilations.setBackground(new Color(236,234,149));
		titreVentilations.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JTextField titreVirements = new JTextField("Virements");
		titreVirements.setFocusable(false);
		titreVirements.setEditable(false);
		titreVirements.setBackground(new Color(236,234,149));
		titreVirements.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JTextField titrePilotage = new JTextField("Pilotage Budgétaire");
		titrePilotage.setFocusable(false);
		titrePilotage.setEditable(false);
		titrePilotage.setBackground(new Color(236,234,149));
		titrePilotage.setForeground(ConstantesCocktail.BG_COLOR_BLACK);

		JPanel panelVide = new JPanel(new BorderLayout());
		panelVide.setBorder(BorderFactory.createEmptyBorder(10,0,3,0));

		JPanel panelVideNature = new JPanel(new BorderLayout());
		panelVideNature.setBorder(BorderFactory.createEmptyBorder(10,0,3,0));

		JPanel panelVide2 = new JPanel(new BorderLayout());
		panelVide2.setBorder(BorderFactory.createEmptyBorder(10,0,3,0));

		JPanel panelVide3 = new JPanel(new BorderLayout());
		panelVide2.setBorder(BorderFactory.createEmptyBorder(10,0,3,0));

		arrayList = new ArrayList();
		arrayList.add(gui_buildPanelBudgetsSaisie());
		arrayList.add(panelVide);
		arrayList.add(titreGestion);
		arrayList.add(gui_buildPanelGestion());
		arrayList.add(panelVideNature);
		arrayList.add(titreNature);
		arrayList.add(gui_buildPanelNature());
		arrayList.add(titreRce);
		arrayList.add(gui_buildPanelNatureRce());
		arrayList.add(titreNatureLolf);
		arrayList.add(gui_buildPanelNatureLolf());
		arrayList.add(panelVide2);
		arrayList.add(titreVentilations);
		arrayList.add(gui_buildPanelVentilations());
		arrayList.add(panelVide3);
		arrayList.add(titreVirements);
		arrayList.add(gui_buildPanelVirements());
		arrayList.add(titrePilotage);
		arrayList.add(gui_buildPanelPilotage());

		JPanel panelCenter = new JPanel(new BorderLayout());
		panelCenter.setBorder(BorderFactory.createEmptyBorder(25,30,1,30));
		panelCenter.add(ZUiUtil.buildBoxColumn(arrayList), BorderLayout.NORTH);


		// SOUTH

		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelClose, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.add(myOrganSelector.mainPanel(), BorderLayout.WEST);
		mainView.add(panelCenter, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);

		window.setContentPane(mainView);
		ZUiUtil.centerWindow(window);
	}


	/**
	 *
	 *
	 */
	private void initBudgetsSaisie()	{

		NSArray budgets = FinderBudgetSaisie.findBudgetsSaisieForExercice(ec, NSApp.getExerciceBudgetaire(), null);

		EOBudgetSaisie myBudgetSaisie = currentBudgetSaisie;

		budgetsSaisie.removeAllItems();

		budgetsSaisie.addItem("*");
		budgetsSaisie.addItem("TOUS BUDGETS");

		for (int i=0;i<budgets.count();i++)	{
			budgetsSaisie.addItem((EOBudgetSaisie)budgets.objectAtIndex(i));
		}

		budgetsSaisie.addActionListener(budgetsListener);

		if (myBudgetSaisie != null)	{
			currentBudgetSaisie = myBudgetSaisie;
			budgetsSaisie.setSelectedItem(currentBudgetSaisie);
		}

	}

	/**
	 *
	 *
	 */
	public void budgetSaisieHasChanged()	{

		if (budgetsSaisie.getSelectedIndex() <2)	{
			currentBudgetSaisie = null;
		}
		else
			currentBudgetSaisie = (EOBudgetSaisie)budgetsSaisie.getSelectedItem();

		updateUI();

	}

	/**
	 *
	 *
	 */
	public void open()	{

		initBudgetsSaisie();

		updateUI();

		window.show();

	}


	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printBudgetNature(EOOrgan organ, EOExercice exercice, EOBudgetSaisie budgetSaisie,
			String type, int niveauDebut, int niveauFin)	{

		if (organ == null)	{
			EODialogs.runErrorDialog("ERREUR","Veuillez sélectionner une ligne budgétaire !");
			return;
		}

		NSMutableDictionary dico = new NSMutableDictionary();
		Integer bdsaid = null;

		if (budgetSaisie != null)
			bdsaid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, budgetSaisie).valueForKey("bdsaId");

		if (bdsaid != null)
			dico.setObjectForKey(bdsaid, "BDSAID");

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String filtreOrgan = "";
		switch (niveauDebut)	{
		case 0:filtreOrgan = filtreOrgan.concat(" o.org_univ = '" + organ.orgUniv() + "'");break;
		case 1:filtreOrgan = filtreOrgan.concat(" o.org_etab = '" + organ.orgEtab() + "'");break;
		case 2:filtreOrgan = filtreOrgan.concat(" o.org_ub = '" + organ.orgUb() + "'");break;
		case 3:filtreOrgan = filtreOrgan.concat(" o.org_ub = '" + organ.orgUb() + "' and o.org_cr = '" + organ.orgCr() + "'");break;
		}
		filtreOrgan = filtreOrgan.concat(" and o.org_niv = " + niveauFin);

		String requeteSqlSaisie =
			" select b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, b.ORG_ID, ORG_ETAB, ORG_ub, ORG_CR, ORG_LIB, num_section, num_bloc, "+
			" budgetaire, sum(BDSN_MONTANT_D) AS BDSN_MONTANT_D, sum(BDSN_MONTANT_R) AS BDSN_MONTANT_R "+
			" from jefy_budget.v_budget_saisie_naturet b, jefy_budget.v_organ o, jefy_budget.budget_saisie bs "+
			" where b.org_id = o.org_id "+
			" and b.bdsa_id = bs.bdsa_id "+
			" and b.exe_ordre = " + exercice.exeExercice() + " " +
			" and b.bdsa_id = " + bdsaid + " and " +
			filtreOrgan +
			" group by b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, num_section, num_bloc, budgetaire "+
			" order by b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB,b.ORG_ID,  num_section, num_bloc, budgetaire";


		String requeteSqlSaisieDetail =
			"select b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, b.ORG_ID, ORG_ETAB, ORG_ub, ORG_CR, ORG_LIB, num_section, num_bloc, "+
			" budgetaire, sum(BDSN_SAISI_D) AS BDSN_SAISI_D, sum(BDSN_MONTANT_D) AS BDSN_MONTANT_D, sum(BDSN_SAISI_R) AS BDSN_SAISI_R, sum(BDSN_MONTANT_R) AS BDSN_MONTANT_R "+
			", sum(BDSN_VOTE_D) AS BDSN_VOTE_D, sum(BDSN_VOTE_R) AS BDSN_VOTE_R"+
			" from jefy_budget.v_budget_saisie_naturet b, jefy_budget.v_organ o, jefy_budget.budget_saisie bs "+
			" where b.org_id = o.org_id "+
			" and b.bdsa_id = bs.bdsa_id "+
			" and b.exe_ordre = " + exercice.exeExercice() + " " +
			" and b.bdsa_id = " + bdsaid + " and " +
			filtreOrgan +
			" group by b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, num_section, num_bloc, budgetaire "+
			" order by b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, b.ORG_ID, num_section, num_bloc, budgetaire";

		String requeteSqlAnnuel =
			" select b.EXE_ORDRE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, num_section, num_bloc, budgetaire, "+
			" PRIM_DEP, REL_DEP, DBM_DEP, PRIM_REC, REL_REC, DBM_REC, TOTAL_DEP, TOTAL_REC "+
			" from jefy_budget.v_budget_annuel_naturet b, jefy_budget.v_organ o "+
			" where b.org_id = o.org_id "+
			" and b.exe_ordre = " + exercice.exeExercice() + " and "+
			filtreOrgan +
			" order by b.EXE_ORDRE, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, num_section, num_bloc, budgetaire";

		String pdfPath = "";

		try {

			if (type.equals("SAISIE"))	{

				// Impression du detail de la saisie
				if (!temDetailSaisie.isSelected())	{

					System.out.println("EditionsCtrl.printBudgetNature() - REQUETE SQL SAISIE ( " + budgetSaisie.bdsaLibelle() + ")\n" + requeteSqlSaisie);

					dico.setObjectForKey(requeteSqlSaisie, "REQUETE_SQL");
					pdfPath = imprimerReportByThreadPdf(
							ec,
							NSApp.getTemporaryDir(),
							dico,
							JASPER_NAME_BUDGET_NATURE_SAISIE,
							PDF_NAME_BUDGET_NATURE_SAISIE,
							null,
							window,
							null);
				}
				else	{

					System.out.println("EditionsCtrl.printBudgetNature() - REQUETE SQL SAISIE DETAIL ( " + budgetSaisie.bdsaLibelle() + ")\n" + requeteSqlSaisieDetail);

					dico.setObjectForKey(requeteSqlSaisieDetail, "REQUETE_SQL");
					pdfPath = imprimerReportByThreadPdf(
							ec,
							NSApp.getTemporaryDir(),
							dico,
							JASPER_NAME_BUDGET_NATURE_SAISIE_DETAIL,
							PDF_NAME_BUDGET_NATURE_SAISIE_DETAIL,
							null,
							window,
							null);
				}

			}
			else	{
				System.out.println("EditionsCtrl.printBudgetNature() - REQUETE SQL ANNUEL \n" + requeteSqlAnnuel);

				dico.setObjectForKey(requeteSqlAnnuel, "REQUETE_SQL");
				pdfPath = imprimerReportByThreadPdf(
						ec,
						NSApp.getTemporaryDir(),
						dico,
						JASPER_NAME_BUDGET_NATURE_ANNUEL,
						PDF_NAME_BUDGET_NATURE_ANNUEL,
						null,
						window,
						null);
			}

			NSApp.openFile(pdfPath);

		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}

	public void printBudgetNatureLolf(EOExercice exercice, EOBudgetSaisie budgetSaisie, String type)	{

		NSMutableDictionary dico = new NSMutableDictionary();
		Integer bdsaid = null;

		if (budgetSaisie != null)
			bdsaid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, budgetSaisie).valueForKey("bdsaId");

		if (bdsaid != null)
			dico.setObjectForKey(bdsaid, "BDSAID");

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String requeteSqlSaisie =
			"select b.EXE_ORDRE, b.bdsa_id, bdsa_libelle, b.ORG_ID, ORG_ETAB, ORG_LIB, TYAC_TYPE, DECAISSable tyac_decaiss, TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, "+
			"TCD_CODE, b.pco_num, pco_libelle, bdsl_saisi "+
			"from budget_saisie_nature_lolf b, v_organ o, v_type_credit_budget c, maracuja.plan_comptable p, v_type_action t, jefy_budget.budget_saisie bs "+
			"where b.org_id = o.org_id and b.tcd_ordre = c.tcd_ordre and b.pco_num=p.pco_num and b.exe_ordre = c.exe_ordre and b.exe_ordre = "+exercice.exeExercice()+
			" and b.tyac_id=t.tyac_id and b.exe_ordre=t.exe_ordre and b.bdsa_id=bs.bdsa_id and bdsl_saisi<>0 and b.bdsa_id = " + bdsaid +
			" ORDER BY EXE_ORDRE, ORG_ETAB, ORG_UB, ORG_CR, decaissable, TYAC_PROG, TYAC_CODE, TCD_CODE, pco_num";

		String requeteSqlAnnuel =
			"select b.EXE_ORDRE, b.ORG_ID, ORG_ETAB, ORG_LIB, TYAC_TYPE, DECAISSable tyac_decaiss, TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, "+
			"TCD_CODE, b.pco_num, pco_libelle, bdvl_PRIMITIFs primitif, bdvl_RELIQUATs reliquat, bdvl_DBMs dbm "+
			"from budget_vote_nature_lolf b, v_organ o, v_type_credit_budget c, maracuja.plan_comptable p, v_type_action t "+
			"where b.org_id = o.org_id and b.tcd_ordre = c.tcd_ordre and b.pco_num=p.pco_num and b.exe_ordre = c.exe_ordre and b.exe_ordre = "+exercice.exeExercice()+
			" and b.tyac_id=t.tyac_id and b.exe_ordre = t.exe_ordre "+
			"ORDER BY EXE_ORDRE, ORG_ETAB, ORG_UB, ORG_CR, decaissable, TYAC_PROG, TYAC_CODE, TCD_CODE, pco_num";

		String pdfPath = "";

		try {

			if (type.equals("SAISIE"))	{
				System.out.println("EditionsCtrl.printBudgetNatureLolf() - REQUETE SQL SAISIE ( " + budgetSaisie.bdsaLibelle() + ")\n" + requeteSqlSaisie);
				dico.setObjectForKey(requeteSqlSaisie, "REQUETE_SQL");
				pdfPath = imprimerReportByThreadPdf(ec, NSApp.getTemporaryDir(), dico, JASPER_NAME_BUDGET_NATURE_LOLF_SAISIE, PDF_NAME_BUDGET_NATURE_LOLF_SAISIE,
						null, window, null);
			}
			else	{
				System.out.println("EditionsCtrl.printBudgetNatureLolf() - REQUETE SQL ANNUEL \n" + requeteSqlAnnuel);
				dico.setObjectForKey(requeteSqlAnnuel, "REQUETE_SQL");
				pdfPath = imprimerReportByThreadPdf(ec, NSApp.getTemporaryDir(), dico, JASPER_NAME_BUDGET_NATURE_LOLF_ANNUEL, PDF_NAME_BUDGET_NATURE_LOLF_ANNUEL,
						null, window, null);
			}

			NSApp.openFile(pdfPath);
		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}
	}

	public void printBudgetRce(EOOrgan organ, EOExercice exercice, EOBudgetSaisie budgetSaisie)	{
		NSMutableDictionary dico = new NSMutableDictionary();
		Integer bdsaid=null, orgId=null;

		if (budgetSaisie != null)
			bdsaid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, budgetSaisie).valueForKey("bdsaId");
		if (bdsaid==null)
			return;
		if (organ!= null)
			orgId = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, organ).valueForKey("orgId");
		if (orgId==null)
			return;

		dico.setObjectForKey(bdsaid, "BDSAID");
		dico.setObjectForKey(orgId, "ORGID");

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		/*String requeteSqlSaisie =
			" select b.exe_ordre, b.bdsa_id, bdsa_libelle, num_section, num_bloc, budgetaire, "+
			" sum(BDSN_MONTANT_D) BDSN_MONTANT_D, SUM(BDSN_MONTANT_R) BDSN_MONTANT_R"+
			" from jefy_budget.V_BUDGET_SAISIE_NATURET b, jefy_budget.budget_saisie bs, jefy_admin.organ o"+
			" where b.bdsa_id = bs.bdsa_id and b.org_id=o.org_id and o.org_niv=0 ";
//		if (orgid!=null)
//			requeteSqlSaisie+=" and org_id = "+orgid;
		requeteSqlSaisie+=" and b.bdsa_id = "+bdsaid+
		    " group by b.exe_ordre, b.bdsa_id, bdsa_libelle, num_section, num_bloc, budgetaire"+
		    " order by b.exe_ordre, b.bdsa_id, num_section, num_bloc, budgetaire";
		 */
		String pdfPath = "";

		try {
			//		System.out.println("printBudgetRce() - REQUETE SQL SAISIE ( " + budgetSaisie.bdsaLibelle() + ")\n" + requeteSqlSaisie);
			//		dico.setObjectForKey(requeteSqlSaisie, "REQUETE_SQL");
			pdfPath = imprimerReportByThreadPdf(ec, NSApp.getTemporaryDir(), dico, JASPER_NAME_BUDGET_RCE, PDF_NAME_BUDGET_RCE, null, window, null);

			NSApp.openFile(pdfPath);
		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printVentilations(EOExercice exercice, EOOrgan organ, NSArray ids)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");


		String filtreOrgan = " and bmou_id in (";
		for (int i =0;i<ids.count();i++)	{

			filtreOrgan = filtreOrgan + ((Number)ids.objectAtIndex(i)).toString();

			if (i < ids.count()-1)
				filtreOrgan = filtreOrgan + ",";
		}
		filtreOrgan = filtreOrgan + ") ";

		String requeteSql =
			" select m.exe_ordre, bmou_id, bmou_libelle, to_date(bmou_creation,'dd/mm/yyyy') date_creation, bmou_creation, bm_montant, m.org_id, tcd_code, tyse_id, "+
			" org_ub||' '||org_cr||' '||org_souscr as ligne_budg, org_lib "+
			" from v_budget_ventilations m, v_organ o "+
			" where m.org_id = o.org_id "+
			" and m.exe_ordre = " + exercice.exeExercice() + " "+
			filtreOrgan +
			" order by m.exe_ordre,  date_creation desc,bmou_id , tyse_id, bm_montant, o.org_cr, tcd_code";

		String pdfPath = "";

		System.out.println("EditionsCtrl.printVentilations() SQL VENTILATIONS : \n " + requeteSql);

		try {
			dico.setObjectForKey(requeteSql, "REQUETE_SQL");

			pdfPath = imprimerReportByThreadPdf(
					ec,
					NSApp.getTemporaryDir(),
					dico,
					JASPER_NAME_VENTILATIONS,
					PDF_NAME_VENTILATIONS,
					null,
					window,
					null);

			NSApp.openFile(pdfPath);

		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}


	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printPilotage(EOOrgan organ, EOExercice exercice)	{

		NSMutableDictionary dico = new NSMutableDictionary();
		Integer orgid = null;

		dico.setObjectForKey(new BigDecimal(exercice.exeExercice().intValue()), "EXEORDRE");

		orgid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, currentOrgan).valueForKey("orgId");
		dico.setObjectForKey(new BigDecimal(orgid.intValue()), "ORGID");

		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String pdfPath = "";

		try {

			pdfPath = imprimerReportByThreadPdf(
					ec,
					NSApp.getTemporaryDir(),
					dico,
					JASPER_NAME_PILOTAGE,
					PDF_NAME_PILOTAGE,
					null,
					window,
					null);

			NSApp.openFile(pdfPath);

		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}


	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printCtrlSaisie(EOBudgetSaisie budgetSaisie)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		Integer bdsaid = null;

		if (budgetSaisie != null)
			bdsaid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, budgetSaisie).valueForKey("bdsaId");

		if (bdsaid != null)
			dico.setObjectForKey(bdsaid, "BDSAID");

		String pdfPath = "";

		try {

			pdfPath = imprimerReportByThreadPdf(
					ec,
					NSApp.getTemporaryDir(),
					dico,
					JASPER_NAME_CTRL_SAISIE,
					PDF_NAME_CTRL_SAISIE,
					null,
					window,
					null);

			NSApp.openFile(pdfPath);

		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}



	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printVirementsGestion(EOExercice exercice, EOOrgan organ, NSArray ids)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String filtreOrgan = " and bmou_id in (";
		for (int i =0;i<ids.count();i++)	{

			filtreOrgan = filtreOrgan + ((Number)ids.objectAtIndex(i)).toString();

			if (i < ids.count()-1)
				filtreOrgan = filtreOrgan + ",";
		}
		filtreOrgan = filtreOrgan + ") ";

		String requeteSqlGestion =
			" select m.exe_ordre, bmou_id, bmou_libelle, to_date(bmou_creation,'dd/mm/yyyy') date_creation, bmou_creation, bm_montant, m.org_id, tcd_code, "+
			" tyse_id, 2 as type_vir, org_ub||' '||org_cr||' '||org_souscr as ligne_budg, org_lib, "+
			" tyac_code as code_vir, tyac_libelle as code_libelle "+
			" from v_budget_virements_gestion m, v_organ o, v_type_action_budget a "+
			" where m.org_id = o.org_id and m.tyac_id = a.tyac_id and a.exe_ordre=m.exe_ordre"+
			" and m.exe_ordre = " + exercice.exeExercice()+ " "+
			filtreOrgan +
			" order by m.exe_ordre, date_creation desc, bmou_id, tcd_code desc, tyse_id, bm_montant, m.org_id,  tyac_code";

		String pdfPath = "";

		System.out.println("EditionsCtrl.printMouvement() SQL VIREMENT GESTION : \n " + requeteSqlGestion);

		try {
			dico.setObjectForKey(requeteSqlGestion, "REQUETE_SQL");

			pdfPath = imprimerReportByThreadPdf(
					ec,
					NSApp.getTemporaryDir(),
					dico,
					JASPER_NAME_VIREMENTS,
					PDF_NAME_VIREMENTS_GESTION,
					null,
					window,
					null);

			NSApp.openFile(pdfPath);

		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}


	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printVirementsNature(EOExercice exercice, EOOrgan organ, NSArray ids)	{

		NSMutableDictionary dico = new NSMutableDictionary();

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String filtreOrgan = " and bmou_id in (";
		for (int i =0;i<ids.count();i++)	{

			filtreOrgan = filtreOrgan + ((Number)ids.objectAtIndex(i)).toString();

			if (i < ids.count()-1)
				filtreOrgan = filtreOrgan + ",";
		}
		filtreOrgan = filtreOrgan + ") ";

		String requeteSqlNature =
			" select m.exe_ordre, bmou_id, bmou_libelle, to_date(bmou_creation,'dd/mm/yyyy') date_creation, bmou_creation, bm_montant, m.org_id, tcd_code, "+
			" tyse_id, 1 as type_vir, org_ub||' '||org_cr||' '||org_souscr as ligne_budg, org_lib, "+
			" m.pco_num as code_vir, pco_libelle as code_libelle "+
			" from v_budget_virements_nature m, v_organ o, v_plan_comptable_exer p "+
			" where m.org_id = o.org_id and m.pco_num = p.pco_num and m.exe_ordre=p.exe_ordre "+
			" and m.exe_ordre = " + exercice.exeExercice()+ " "+
			filtreOrgan +
			" order by m.exe_ordre, date_creation desc, bmou_id, tcd_code desc,tyse_id, bm_montant, m.org_id,  p.pco_num";

		String pdfPath = "";

		System.out.println("EditionsCtrl.printMouvement() SQL VIRELENT NATURE : \n " + requeteSqlNature);

		try {
			dico.setObjectForKey(requeteSqlNature, "REQUETE_SQL");

			pdfPath = imprimerReportByThreadPdf(
					ec,
					NSApp.getTemporaryDir(),
					dico,
					JASPER_NAME_VIREMENTS,
					PDF_NAME_VIREMENTS_NATURE,
					null,
					window,
					null);

			NSApp.openFile(pdfPath);
		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}



	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printBudgetGestionDepense(EOOrgan organ, EOExercice exercice, EOBudgetSaisie budgetSaisie,
			String type, int niveauDebut, int niveauFin)	{

		NSMutableDictionary dico = new NSMutableDictionary();
		Integer bdsaid = null;

		if (budgetSaisie != null)
			bdsaid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, budgetSaisie).valueForKey("bdsaId");

		if (bdsaid != null)
			dico.setObjectForKey(bdsaid, "BDSAID");

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String filtreOrgan = "";
		switch (niveauDebut)	{
		case 0:filtreOrgan = filtreOrgan.concat(" o.org_univ = '" + organ.orgUniv() + "'");break;
		case 1:filtreOrgan = filtreOrgan.concat(" o.org_etab = '" + organ.orgEtab() + "'");break;
		case 2:filtreOrgan = filtreOrgan.concat(" o.org_ub = '" + organ.orgUb() + "'");break;
		case 3:filtreOrgan = filtreOrgan.concat(" o.org_ub = '" + organ.orgUb() + "' and o.org_cr = '" + organ.orgCr() + "'");break;
		}
		filtreOrgan = filtreOrgan.concat(" and o.org_niv = " + niveauFin);

		String requeteSqlSaisie =

			" select b.EXE_ORDRE, b.BDSA_ID, BDSA_LIBELLE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, "+
			" b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, BDSG_MONTANT "+
			" from jefy_budget.v_budget_saisie_gestion_dep b, jefy_budget.v_organ o, jefy_budget.v_type_action_budget a, "+
			" jefy_budget.budget_saisie bs, jefy_admin.type_credit c "+
			" where b.org_id = o.org_id "+
			" and b.tyac_id = a.tyac_id and b.exe_ordre = a.exe_ordre "+
			" and b.bdsa_id = bs.bdsa_id "+
			" and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre "+
			" and (bdsg_montant <> 0 or bdsg_saisi <> 0)"+
			" and b.exe_ordre = " + exercice.exeExercice() + " and b.bdsa_id = " + bdsaid + " and " +
			filtreOrgan +
			" order by b.exe_ordre, ORG_ETAB, ORG_UB, ORG_CR, b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TCD_CODE";

		String requeteSqlSaisieDetail =

			" select b.EXE_ORDRE, b.BDSA_ID, BDSA_LIBELLE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, "+
			" b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, BDSG_VOTE, BDSG_MONTANT, BDSG_SAISI "+
			" from jefy_budget.v_budget_saisie_gestion_dep b, jefy_budget.v_organ o, jefy_budget.v_type_action_budget a, "+
			" jefy_budget.budget_saisie bs, jefy_admin.type_credit c "+
			" where b.org_id = o.org_id "+
			" and b.tyac_id = a.tyac_id and b.exe_ordre = a.exe_ordre "+
			" and b.bdsa_id = bs.bdsa_id "+
			" and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre "+
			" and (bdsg_montant <> 0 or bdsg_saisi <> 0)"+
			" and b.exe_ordre = " + exercice.exeExercice() + " and b.bdsa_id = " + bdsaid + " and " +
			filtreOrgan +
			" order by b.exe_ordre, ORG_ETAB, ORG_UB, ORG_CR, b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TCD_CODE";

		String requeteSqlAnnuel =
			" select b.EXE_ORDRE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, TYAC_TYPE, TYAC_DECAISS, TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, PRIMITIF, RELIQUAT, DBM "+
						" from jefy_budget.v_budget_annuel_gestion_dep b, jefy_budget.v_organ o, jefy_budget.v_type_credit_budget c  " +
			" where b.org_id = o.org_id and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre "+
			" and b.exe_ordre = " + exercice.exeExercice() + " and " +
			filtreOrgan +
			" ORDER BY EXE_ORDRE, ORG_ETAB, ORG_UB, ORG_CR, TYAC_DECAISS, TYAC_PROG, TYAC_CODE, TCD_CODE";

		String pdfPath = "";

		try {

			if ("SAISIE".equals(type))	{

				if (!temDetailSaisie.isSelected()) {
					dico.setObjectForKey(requeteSqlSaisie, "REQUETE_SQL");
					pdfPath = imprimerReportByThreadPdf(
							ec,
							NSApp.getTemporaryDir(),
							dico,
							JASPER_NAME_BUDGET_GESTION_SAISIE_DEPENSE,
							PDF_NAME_BUDGET_GESTION_SAISIE_DEPENSE,
							null,
							window,
							null);
				} else {

					dico.setObjectForKey(requeteSqlSaisieDetail, "REQUETE_SQL");
					pdfPath = imprimerReportByThreadPdf(
							ec,
							NSApp.getTemporaryDir(),
							dico,
							JASPER_NAME_BUDGET_GESTION_SAISIE_DEPENSE_DETAIL,
							PDF_NAME_BUDGET_GESTION_SAISIE_DEPENSE_DETAIL,
							null,
							window,
							null);
				}

			} else {
				System.out.println("EditionsCtrl.printBudgetGestionDepense() - REQUETE SQL ANNUEL \n" + requeteSqlAnnuel);
				dico.setObjectForKey(requeteSqlAnnuel, "REQUETE_SQL");
				pdfPath = imprimerReportByThreadPdf(
						ec,
						NSApp.getTemporaryDir(),
						dico,
						JASPER_NAME_BUDGET_GESTION_ANNUEL_DEPENSE,
						PDF_NAME_BUDGET_GESTION_ANNUEL_DEPENSE,
						null,
						window,
						null);
			}

			NSApp.openFile(pdfPath);
		} catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}

	}



	/**
	 *
	 * @param organ
	 * @param exercice
	 * @param budgetSaisie
	 */
	public void printBudgetGestionRecette(EOOrgan organ, EOExercice exercice, EOBudgetSaisie budgetSaisie,
			String type, int niveauDebut, int niveauFin)	{

		NSMutableDictionary dico = new NSMutableDictionary();
		Integer bdsaid = null;

		if (budgetSaisie != null)
			bdsaid = (Integer) ServerProxy.clientSideRequestPrimaryKeyForObject(ec, budgetSaisie).valueForKey("bdsaId");

		if (bdsaid != null)
			dico.setObjectForKey(bdsaid, "BDSAID");

		dico.setObjectForKey(exercice.exeExercice(), "EXEORDRE");
		dico.setObjectForKey(NSApp.getEtablissement(), "UNIV");
		dico.setObjectForKey(NSApp.getExerciceBudgetaire().exeExercice().toString(), "EXER");

		String filtreOrgan = "";
		switch (niveauDebut)	{
		case 0:filtreOrgan = filtreOrgan.concat(" o.org_univ = '" + organ.orgUniv() + "'");break;
		case 1:filtreOrgan = filtreOrgan.concat(" o.org_etab = '" + organ.orgEtab() + "'");break;
		case 2:filtreOrgan = filtreOrgan.concat(" o.org_ub = '" + organ.orgUb() + "'");break;
		case 3:filtreOrgan = filtreOrgan.concat(" o.org_ub = '" + organ.orgUb() + "' and o.org_cr = '" + organ.orgCr() + "'");break;
		}
		filtreOrgan = filtreOrgan.concat(" and o.org_niv = " + niveauFin);

		String requeteSqlSaisie =

			" select b.EXE_ORDRE, b.BDSA_ID, BDSA_LIBELLE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, "+
			" b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, BDSG_MONTANT "+
			" from jefy_budget.v_budget_saisie_gestion_rec b, jefy_budget.v_organ o, jefy_budget.v_type_action_budget a, " +
			" jefy_budget.budget_saisie bs, jefy_admin.type_credit c "+
			" where b.org_id = o.org_id "+
			" and b.tyac_id = a.tyac_id and b.exe_ordre = a.exe_ordre "+
			" and b.bdsa_id = bs.bdsa_id "+
			" and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre "+
			" and (bdsg_montant <> 0 or bdsg_saisi <> 0) "+
			" and b.exe_ordre = " + exercice.exeExercice() + " and b.bdsa_id = " + bdsaid + " and " +
			filtreOrgan +
			" order by b.exe_ordre, ORG_ETAB, ORG_UB, ORG_CR, b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TCD_CODE";

		String requeteSqlSaisieDetail =
			" select b.EXE_ORDRE, b.BDSA_ID, BDSA_LIBELLE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, "+
			" b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, BDSG_VOTE, BDSG_MONTANT, BDSG_SAISI " +
			" from jefy_budget.v_budget_saisie_gestion_rec b, jefy_budget.v_organ o, jefy_budget.v_type_action_budget a, " +
			" jefy_budget.budget_saisie bs, jefy_admin.type_credit c "+
			" where b.org_id = o.org_id "+
			" and b.tyac_id = a.tyac_id and b.exe_ordre = a.exe_ordre "+
			" and b.bdsa_id = bs.bdsa_id "+
			" and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre "+
			" and (bdsg_montant <> 0 or bdsg_saisi <> 0) "+
			" and b.exe_ordre = " + exercice.exeExercice() + " and b.bdsa_id = " + bdsaid + " and " +
			filtreOrgan +
			" order by b.exe_ordre, ORG_ETAB, ORG_UB, ORG_CR, b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TCD_CODE";

		String requeteSqlAnnuel =
			" select b.EXE_ORDRE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, TYAC_TYPE, TYAC_DECAISS, TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, PRIMITIF, RELIQUAT, DBM "+
			" from v_budget_annuel_gestion_rec b, v_organ o, v_type_credit_budget c  "+
			" where b.org_id = o.org_id and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre "+
			" and b.exe_ordre = " + exercice.exeExercice() + " and "+
			filtreOrgan +
			" ORDER BY EXE_ORDRE, ORG_ID, TYAC_DECAISS, TYAC_PROG, TYAC_CODE, TCD_CODE";

		String pdfPath = "";

		try {
			if (type.equals("SAISIE"))	{
				if (!temDetailSaisie.isSelected()){
					dico.setObjectForKey(requeteSqlSaisie, "REQUETE_SQL");
					pdfPath = imprimerReportByThreadPdf(ec, NSApp.getTemporaryDir(), dico, JASPER_NAME_BUDGET_GESTION_SAISIE_RECETTE,
							PDF_NAME_BUDGET_GESTION_SAISIE_RECETTE, null, window, null);
				}
				else	{
					dico.setObjectForKey(requeteSqlSaisieDetail, "REQUETE_SQL");
					pdfPath = imprimerReportByThreadPdf(ec, NSApp.getTemporaryDir(), dico, JASPER_NAME_BUDGET_GESTION_SAISIE_RECETTE_DETAIL,
							PDF_NAME_BUDGET_GESTION_SAISIE_RECETTE_DETAIL, null, window, null);
				}
			}
			else	{
				System.out.println("EditionsCtrl.printBudgetGestionRecette() - REQUETE SQL ANNUEL \n" + requeteSqlAnnuel);
				dico.setObjectForKey(requeteSqlAnnuel, "REQUETE_SQL");
				pdfPath = imprimerReportByThreadPdf(ec, NSApp.getTemporaryDir(), dico, JASPER_NAME_BUDGET_GESTION_ANNUEL_RECETTE,
						PDF_NAME_BUDGET_GESTION_ANNUEL_RECETTE, null, window, null);
			}
			NSApp.openFile(pdfPath);
		}
		catch (Exception ex)	{
			EODialogs.runErrorDialog("ERREUR",ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPrintVentilations extends AbstractAction {
		public ActionPrintVentilations() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}
		public void actionPerformed(ActionEvent e) {
			VentilationsHistorique.sharedInstance(ec, window).openHistorique(currentOrgan);
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPrintBudgetNature extends AbstractAction {
		public ActionPrintBudgetNature() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}
		public void actionPerformed(ActionEvent e) {
			int niveauFin = 0;
			if (temUniv.isSelected())
				niveauFin = 0;
			else
				if (temEtab.isSelected())
					niveauFin = 1;
				else
					if (temUb.isSelected())
						niveauFin = 2;
					else
						if (temCr.isSelected())
							niveauFin = 3;

			if (currentBudgetSaisie == null)	{
				printBudgetNature(currentOrgan, NSApp.getExerciceBudgetaire(), currentBudgetSaisie, "ANNUEL", currentOrgan.orgNiveau().intValue(), niveauFin);
			}
			else	{
				printBudgetNature(currentOrgan, NSApp.getExerciceBudgetaire(), currentBudgetSaisie, "SAISIE", currentOrgan.orgNiveau().intValue(), niveauFin);
			}
		}
	}

	private final class ActionPrintBudgetRce extends AbstractAction {

		public ActionPrintBudgetRce() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}

		public void actionPerformed(ActionEvent e) {
			if (currentBudgetSaisie != null)	{
				printBudgetRce(currentOrgan, NSApp.getExerciceBudgetaire(), currentBudgetSaisie);
			}
		}
	}

	private final class ActionPrintBudgetNatureLolf extends AbstractAction {
		public ActionPrintBudgetNatureLolf() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}
		public void actionPerformed(ActionEvent e) {
			if (currentBudgetSaisie == null)	{
				printBudgetNatureLolf(NSApp.getExerciceBudgetaire(), currentBudgetSaisie, "ANNUEL");
			}
			else	{
				printBudgetNatureLolf(NSApp.getExerciceBudgetaire(), currentBudgetSaisie, "SAISIE");
			}
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPrintBudgetGestion extends AbstractAction {

		public ActionPrintBudgetGestion() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}

		public void actionPerformed(ActionEvent e) {

			int niveauFin = 0;

			if (temUniv.isSelected())
				niveauFin = 0;
			else
				if (temEtab.isSelected())
					niveauFin = 1;
				else
					if (temUb.isSelected())
						niveauFin = 2;
					else
						if (temCr.isSelected())
							niveauFin = 3;

			if (currentBudgetSaisie == null)	{

				if (checkBudgetGestionDepense.isSelected())
					printBudgetGestionDepense(currentOrgan, NSApp.getExerciceBudgetaire(),
							currentBudgetSaisie,
							"ANNUEL",
							currentOrgan.orgNiveau().intValue(),
							niveauFin);

				if (checkBudgetGestionRecette.isSelected())
					printBudgetGestionRecette(currentOrgan, NSApp.getExerciceBudgetaire(),
							currentBudgetSaisie,
							"ANNUEL",
							currentOrgan.orgNiveau().intValue(),
							niveauFin);

			}
			else	{

				if (checkBudgetGestionDepense.isSelected())
					printBudgetGestionDepense(currentOrgan, NSApp.getExerciceBudgetaire(),
							currentBudgetSaisie,
							"SAISIE",
							currentOrgan.orgNiveau().intValue(),
							niveauFin);
				if (checkBudgetGestionRecette.isSelected())
					printBudgetGestionRecette(currentOrgan, NSApp.getExerciceBudgetaire(),
							currentBudgetSaisie,
							"SAISIE",
							currentOrgan.orgNiveau().intValue(),
							niveauFin);

			}

		}
	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPrintVirements extends AbstractAction {

		public ActionPrintVirements() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}

		public void actionPerformed(ActionEvent e) {

			VirementsHistorique.sharedInstance(ec, window).openHistorique(currentOrgan);

		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionPrintPilotage extends AbstractAction {

		public ActionPrintPilotage() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_IMPRIMER_16);
		}

		public void actionPerformed(ActionEvent e) {

			printPilotage(currentOrgan, NSApp.getExerciceBudgetaire());

		}
	}


	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionRefreshBudgets extends AbstractAction {

		public ActionRefreshBudgets() {
			super();
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_SELECT);
		}

		public void actionPerformed(ActionEvent e) {
			initBudgetsSaisie();
		}
	}

	/**
	 *
	 * @author cpinsard
	 *
	 */
	private final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
		}

		public void actionPerformed(ActionEvent e) {
			window.hide();
		}
	}


	/**
	 *
	 *
	 */
	public void majLibelles()	{

		switch (currentOrgan.orgNiveau().intValue())	{

		case 0 :
			if (temUniv.isSelected())	{
				labelNatureDetaille.setText("Imprimer le budget " + currentOrgan.orgUniv());
				labelGestionDetaille.setText("Imprimer le budget " + currentOrgan.orgUniv());
			}
			else
				if (temEtab.isSelected())	{
					labelNatureDetaille.setText("Imprimer tous les établissements");
					labelGestionDetaille.setText("Imprimer tous les établissements");
				}
				else
					if (temUb.isSelected())	{
						labelNatureDetaille.setText("Imprimer toutes les UB");
						labelGestionDetaille.setText("Imprimer toutes les UB");
					}
					else
						if (temCr.isSelected())	{
							labelNatureDetaille.setText("Imprimer tous les CR");
							labelGestionDetaille.setText("Imprimer tous les CR");
						}
			break;
		case 1 :
			if (temEtab.isSelected())	{
				labelNatureDetaille.setText("Imprimer l'établissement " + currentOrgan.getLongString());
				labelGestionDetaille.setText("Imprimer l'établissement " + currentOrgan.getLongString());
			}
			else
				if (temUb.isSelected())	{
					labelNatureDetaille.setText("Imprimer toutes les UB de l'établissement " + currentOrgan.orgEtab());
					labelGestionDetaille.setText("Imprimer toutes les UB de l'établissement " + currentOrgan.orgEtab());
				}
				else
					if (temCr.isSelected())	{
						labelNatureDetaille.setText("Imprimer tous CR de l'établissement"  + currentOrgan.orgEtab());
						labelGestionDetaille.setText("Imprimer tous CR de l'établissement"  + currentOrgan.orgEtab());
					}
			break;
		case 2 :
			if (temUb.isSelected())	{
				labelNatureDetaille.setText("Imprimer l'UB " + currentOrgan.orgUb());
				labelGestionDetaille.setText("Imprimer l'UB " + currentOrgan.orgUb());
			}
			else
				if (temCr.isSelected())	{
					labelNatureDetaille.setText("Imprimer tous les CR de l'UB "  + currentOrgan.orgUb());
					labelGestionDetaille.setText("Imprimer tous les CR de l'UB "  + currentOrgan.orgUb());
				}
			break;
		case 3 :
			labelNatureDetaille.setText("Imprimer le CR " + currentOrgan.getLongString());
			labelGestionDetaille.setText("Imprimer le CR " + currentOrgan.getLongString());
			break;
		}

	}


	/**
	 *
	 * @param notif
	 */
	public void selectedOrganHasChanged(NSNotification notif)	{

		currentOrgan = (EOOrgan)notif.object();

		updateUI();

		majLibelles();

	}


	/**
	 *
	 *
	 */
	private void updateUI()	{

		temUniv.removeActionListener(checkBoxListener);
		temEtab.removeActionListener(checkBoxListener);
		temUb.removeActionListener(checkBoxListener);
		temCr.removeActionListener(checkBoxListener);

		temUniv.setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() == 0);
		temEtab.setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() <= 1);
		temUb.setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() <= 2);
		temCr.setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() < 3);

		temUniv.setSelected(currentOrgan != null && currentOrgan.orgNiveau().intValue() == 0);
		temEtab.setSelected(currentOrgan != null && currentOrgan.orgNiveau().intValue() == 1);
		temUb.setSelected(currentOrgan != null && currentOrgan.orgNiveau().intValue() == 2);
		temCr.setSelected(currentOrgan != null && currentOrgan.orgNiveau().intValue() == 3);

		temDetailSaisie.setEnabled(false);//currentOrgan != null && budgetsSaisie.getSelectedIndex() > 1);

		actionPrintBudgetGestion.setEnabled(currentOrgan != null && budgetsSaisie.getSelectedIndex() > 0 && currentOrgan.orgNiveau().intValue() < 4);

		actionPrintBudgetNature.setEnabled(currentOrgan != null  && budgetsSaisie.getSelectedIndex() > 0 && currentOrgan.orgNiveau().intValue() < 4);
		actionPrintBudgetNatureLolf.setEnabled(budgetsSaisie.getSelectedIndex()>0);

		actionPrintVentilations.setEnabled(currentOrgan != null);
		actionPrintVirements.setEnabled(currentOrgan != null);

		actionPrintPilotage.setEnabled(currentOrgan != null && currentOrgan.orgNiveau().intValue() > 1);

		temUniv.addActionListener(checkBoxListener);
		temEtab.addActionListener(checkBoxListener);
		temUb.addActionListener(checkBoxListener);
		temCr.addActionListener(checkBoxListener);

	}


	public void onImprimer() {

	}


	public String title() {
		return null;
	}


	public Dimension defaultDimension() {
		return null;
	}


	public ZAbstractPanel mainPanel() {
		return null;
	}



	/**
	 *
	 * @author cpinsard
	 *
	 */
	private class BudgetSaisieListener implements ActionListener	{
		public BudgetSaisieListener() {super();}
		public void actionPerformed(ActionEvent anAction) {
			budgetSaisieHasChanged();
		}
	}

	/**
	 * Listener sur la coche temPayeLocale. Permet de actif le bouton permettant la saisie des infos paye
	 */
	private class CheckBoxListener extends AbstractAction	{
		public CheckBoxListener ()	{
			super();
		}

		public void actionPerformed(ActionEvent anAction)	{
			majLibelles();
		}

	}
}
