package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.bibasse.client.finder.FinderBudgetParametres;
import org.cocktail.bibasse.client.metier.EOBudgetParametres;
import org.cocktail.bibasse.client.metier.EOBudgetSaisie;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class Parametrage  {
	
	
	private static Parametrage sharedInstance;
	private EOEditingContext ec;
	private ApplicationClient NSApp;
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;
	
	protected   ButtonGroup typeSaisie;
	protected	JRadioButton checkSaisieUb, checkSaisieCr;
	
	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private TableSorter myTableSorter;
	
	protected ActionCancel 		actionCancel = new ActionCancel();
	protected ActionValidate 	actionValidate = new ActionValidate();
	
	protected JPanel viewTable;
		
	/**
	 * 
	 *
	 */
	public Parametrage(EOEditingContext editingContext, JFrame frame)	{
		
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		mainFrame = frame;
		
		initGUI();
		initView();

	}
	
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static Parametrage sharedInstance(EOEditingContext editingContext, JFrame frame)	{
		if (sharedInstance == null)	
			sharedInstance = new Parametrage(editingContext, frame);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
		mainWindow = new JDialog(mainFrame, "Paramètres Budget", true);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		
		myEOTable.setEnabled(false);
		
		ArrayList arrayList = new ArrayList();
		arrayList.add(actionCancel);
		arrayList.add(actionValidate);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 120, 21));                
		
		JPanel panelSouth = new JPanel(new BorderLayout());
		panelSouth.add(new JSeparator(), BorderLayout.NORTH);
		panelSouth.add(panelButtons, BorderLayout.EAST);
				
		checkSaisieCr = new JRadioButton(" CR ");
		checkSaisieUb = new JRadioButton(" UB ");
		
		checkSaisieCr.setFocusable(false);
		checkSaisieUb.setFocusable(false);

		checkSaisieCr.setFont(new Font("Arial", Font.BOLD, 13));
		checkSaisieUb.setFont(new Font("Arial", Font.BOLD, 13));

        typeSaisie = new ButtonGroup();
        typeSaisie.add(checkSaisieUb);        
        typeSaisie.add(checkSaisieCr);
        
		JPanel flowCheckBoxs = new JPanel(new FlowLayout());
		flowCheckBoxs.add(new JLabel("Niveau de saisie du BUDGET : "));
		flowCheckBoxs.add(checkSaisieUb);
		flowCheckBoxs.add(checkSaisieCr);

		JPanel panelCheckBoxs = new JPanel(new BorderLayout());
		panelCheckBoxs.add(flowCheckBoxs, BorderLayout.WEST);
		
		JPanel panelCenter = new JPanel(new BorderLayout());
		panelCenter.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));
		panelCenter.add(new JLabel("Comptes du budget par nature"), BorderLayout.NORTH);
		panelCenter.add(viewTable, BorderLayout.CENTER);
		panelCenter.add(panelCheckBoxs, BorderLayout.SOUTH);
		
		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(10,5,0,5));
		mainView.setPreferredSize(new Dimension(600, 250));
		mainView.add(panelCenter, BorderLayout.CENTER);
		mainView.add(panelSouth, BorderLayout.SOUTH);
		
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public void updateParametres()	{

		mainWindow.setTitle("Paramétrage BUDGET " + NSApp.getExerciceBudgetaire().exeExercice());
		
		// Si la saisie budgetaire est commencee, on ne peut plus modifier les masques
		if (EOBudgetSaisie.saisieBudgetaireEnCours(ec, NSApp.getExerciceBudgetaire()))	{
			checkSaisieCr.setEnabled(false);
			checkSaisieUb.setEnabled(false);
		}
		
		ZUiUtil.centerWindowInContainer(mainWindow);		

		updateData();
		
		mainWindow.show();

	}
	
	
	/**
	 * 
	 *
	 */
	public void updateData()	{
		
		eod.setObjectArray(FinderBudgetParametres.getParametres(ec, NSApp.getExerciceBudgetaire()));
		myEOTable.updateData();
		myTableModel.fireTableDataChanged();
		
		EOBudgetParametres paramNiveauSaisie = FinderBudgetParametres.findParametre(ec, NSApp.getExerciceBudgetaire(),"NIVEAU_SAISIE");

		checkSaisieUb.setSelected(false);
		checkSaisieCr.setSelected(false);		
			
		if ("UB".equals(paramNiveauSaisie.bparValue()))
			checkSaisieUb.setSelected(true);
		else
			checkSaisieCr.setSelected(true);
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eod = new EODisplayGroup();
		
		viewTable = new JPanel();
		
		initTableModel();
		initTable();
		
		myEOTable.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTable.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.setLayout(new BorderLayout());
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		
	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{
		
		myEOTable = new ZEOTable(myTableSorter);
//		myTableSorter.setTableHeader(myEOTable.getTableHeader());		
				
	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
		
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(eod, "bparKey", "Clé", 200);
		col1.setAlignment(SwingConstants.LEFT);
		myCols.add(col1);
		
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(eod, "bparValue", "Valeur", 50);
		col2.setAlignment(SwingConstants.LEFT);
		myCols.add(col2);
		
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(eod, "bparCommentaires", "Observations", 350);
		col3.setAlignment(SwingConstants.LEFT);
		myCols.add(col3);
		
		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);
		
	}
	

	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionCancel extends AbstractAction {
		
		public ActionCancel() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CANCEL);
		}
		
		public void actionPerformed(ActionEvent e) {
			mainWindow.dispose();
		}  
	} 
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionValidate extends AbstractAction {
		
		public ActionValidate() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_VALID);
		}
		
		public void actionPerformed(ActionEvent e) {
			
			try {

				EOBudgetParametres paramNiveauSaisie = FinderBudgetParametres.findParametre(ec, NSApp.getExerciceBudgetaire(),"NIVEAU_SAISIE");

				if (checkSaisieCr.isSelected())
					paramNiveauSaisie.setBparValue("CR");
				else
					paramNiveauSaisie.setBparValue("UB");
				
				ec.saveChanges();

				if (checkSaisieCr.isSelected())
					NSApp.setNiveauSaisie("CR");
				else
					NSApp.setNiveauSaisie("UB");
				
			}
			catch (Exception ex)	{
				ec.revert();
				EODialogs.runErrorDialog("ERREUR","Erreur de sauvegarde des paramètres !");
			}
			
			mainWindow.dispose();
		}  
	} 

		
}
