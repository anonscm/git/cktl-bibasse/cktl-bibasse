package org.cocktail.bibasse.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.bibasse.client.factory.FactoryBudgetVoteChapitreCtrl;
import org.cocktail.bibasse.client.finder.FinderBudgetMasqueNature;
import org.cocktail.bibasse.client.finder.FinderBudgetVoteChapitreCtrl;
import org.cocktail.bibasse.client.finder.FinderTypeEtat;
import org.cocktail.bibasse.client.metier.EOBudgetMasqueNature;
import org.cocktail.bibasse.client.metier.EOBudgetVoteChapitreCtrl;
import org.cocktail.bibasse.client.metier.EOTypeEtat;
import org.cocktail.bibasse.client.zutil.TableSorter;
import org.cocktail.bibasse.client.zutil.ui.ZUiUtil;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTable;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableCellRenderer;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.bibasse.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class LimitativiteCtrl {

	private static LimitativiteCtrl sharedInstance;
	private EOEditingContext ec;
	private	ApplicationClient NSApp;
	
	protected	JDialog mainWindow;
	protected	JFrame mainFrame;

	protected JPanel mainPanel, viewTableMasque, viewTableControle;	

	private EODisplayGroup eodMasque, eodControle;
	private ZEOTable myEOTableMasque, myEOTableControle;
	private ZEOTableModel myTableModelMasque, myTableModelControle;
	private TableSorter myTableSorterMasque, myTableSorterControle;
		
	private ControleRenderer	rendererControle = new ControleRenderer();
	private MasqueRenderer	rendererMasque = new MasqueRenderer();
	
	protected ActionAdd 			actionAdd = new ActionAdd();
	protected ActionDelete 			actionDelete = new ActionDelete();
	protected ActionClose 			actionClose = new ActionClose();
		
	/**
	 * 
	 *
	 */
	public LimitativiteCtrl(EOEditingContext editingContext)	{
		super();
		ec = editingContext;
		NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
		
		initView();
		initGUI();
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LimitativiteCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LimitativiteCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 *
	 */
	public void initView()	{
		
		mainWindow = new JDialog(mainFrame, "Limitativité des chapitres votés", true);

		viewTableMasque = new JPanel(new BorderLayout());
		viewTableControle = new JPanel(new BorderLayout());

		viewTableMasque.setBorder(BorderFactory.createEmptyBorder(2,5,2,0));		
		viewTableControle.setBorder(BorderFactory.createEmptyBorder(2,0,2,2));		

        // CENTER
        ArrayList arrayList = new ArrayList();
		
		JButton btnAddAction = new JButton(actionAdd);
		btnAddAction.setPreferredSize(new Dimension(50,23));
		btnAddAction.setHorizontalAlignment(0);
		btnAddAction.setVerticalAlignment(0);
		btnAddAction.setHorizontalTextPosition(0);
		btnAddAction.setVerticalTextPosition(0);

		JButton btnDelAction = new JButton(actionDelete);
		btnDelAction.setPreferredSize(new Dimension(50,23));
		btnDelAction.setHorizontalAlignment(0);
		btnDelAction.setVerticalAlignment(0);
		btnDelAction.setHorizontalTextPosition(0);
		btnDelAction.setVerticalTextPosition(0);
				
		arrayList.add(btnAddAction);
		arrayList.add(btnDelAction);
        
		Component panelTemp = ZUiUtil.buildBoxColumn(arrayList);

		JPanel panelCenter = new JPanel(new FlowLayout());
		panelCenter.setBorder(BorderFactory.createEmptyBorder(60, 10,10,10));
		panelCenter.add(panelTemp, BorderLayout.NORTH);
		
		arrayList = new ArrayList();
				
		arrayList.add(viewTableMasque);
		arrayList.add(panelCenter);
		arrayList.add(viewTableControle);
		
		JPanel panelGestion = new JPanel(new BorderLayout());
		panelGestion.add(ZUiUtil.buildBoxLine(arrayList), BorderLayout.CENTER);
		
		if (!NSApp.hasFonction(ConstantesCocktail.ID_FCT_MASQUE))	{
			actionAdd.setEnabled(false);
			actionDelete.setEnabled(false);
		}

        // Boutons
		arrayList = new ArrayList();
		arrayList.add(actionClose);
		JPanel panelButtons = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(arrayList, 110, 22));                

		JPanel southPanel = new JPanel(new BorderLayout());
		southPanel.setBorder(BorderFactory.createEmptyBorder(5,4,2,4));
		southPanel.add(new JSeparator(), BorderLayout.NORTH);
		southPanel.add(panelButtons, BorderLayout.EAST);

		JPanel mainView = new JPanel(new BorderLayout());
		mainView.setBorder(BorderFactory.createEmptyBorder(10,5,10,5));
		mainView.setPreferredSize(new Dimension(700, 500));
		mainView.add(panelGestion, BorderLayout.CENTER);
		mainView.add(southPanel, BorderLayout.SOUTH);
		
		mainWindow.setContentPane(mainView);
		mainWindow.pack();
		
		ZUiUtil.centerWindow(mainWindow);

	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public void open()	{
		
		load();
		
		mainWindow.show();

	}
		
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.karukera.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		
		eodMasque = new EODisplayGroup();
		eodControle = new EODisplayGroup();
		
		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering("typeCredit.tcdCode", EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering("planComptable.pcoNum", EOSortOrdering.CompareAscending));

		eodMasque.setSortOrderings(mySort);
		eodControle.setSortOrderings(mySort);
		
		initTableModel();
		initTable();
		
		myEOTableMasque.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableMasque.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableMasque.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableMasque.setBorder(BorderFactory.createEmptyBorder());
		viewTableMasque.removeAll();
		viewTableMasque.setLayout(new BorderLayout());
		viewTableMasque.add(new JScrollPane(myEOTableMasque), BorderLayout.CENTER);

			
		myEOTableControle.setBackground(ConstantesCocktail.COLOR_FOND_NOMENCLATURES);
		myEOTableControle.setSelectionBackground(ConstantesCocktail.COLOR_SELECTION_NOMENCLATURES);
		myEOTableControle.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableControle.setBorder(BorderFactory.createEmptyBorder());
		viewTableControle.removeAll();
		viewTableControle.setLayout(new BorderLayout());
		viewTableControle.add(new JScrollPane(myEOTableControle), BorderLayout.CENTER);

	}
	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable()	{
				
		myEOTableMasque = new ZEOTable(myTableSorterMasque);
		myTableModelMasque.addTableModelListener(new ListenerMasque());
					
//		myTableSorterMasque.setTableHeader(myEOTableMasque.getTableHeader());		
		
		myEOTableControle = new ZEOTable(myTableSorterControle);
		myTableModelControle.addTableModelListener(new ListenerControle());

//		myTableSorterControle.setTableHeader(myEOTableControle.getTableHeader());		

	}
	
	/**
	 * Initialise le modeele le la table à afficher.
	 *  
	 */
	private void initTableModel() {
						
		Vector myCols = new Vector();
		
		ZEOTableModelColumn col = new ZEOTableModelColumn(eodMasque, "typeCredit.tcdCode", "TCD", 30);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);
		col = new ZEOTableModelColumn(eodMasque, "planComptable.pcoNum", "Compte", 50);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);
		col = new ZEOTableModelColumn(eodMasque, "planComptable.pcoLibelle", "Libellé", 200);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererMasque);
		myCols.add(col);

		myTableModelMasque = new ZEOTableModel(eodMasque, myCols);
		myTableSorterMasque = new TableSorter(myTableModelMasque);

		// CONTROLE
		
		myCols = new Vector();
		
		col = new ZEOTableModelColumn(eodControle, "typeCredit.tcdCode", "TCD", 30);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(rendererControle);
		myCols.add(col);
		col = new ZEOTableModelColumn(eodControle, "planComptable.pcoNum", "Compte", 50);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererControle);
		myCols.add(col);
		col = new ZEOTableModelColumn(eodControle, "planComptable.pcoLibelle", "Libellé", 200);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(rendererControle);
		myCols.add(col);

		myTableModelControle = new ZEOTableModel(eodControle, myCols);
		myTableSorterControle = new TableSorter(myTableModelControle);
	
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ListenerControle implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {

		}
	}
			
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ListenerMasque implements TableModelListener 	{

		public void tableChanged(TableModelEvent e) {

		}
	}

	

	/**
	 * 
	 *
	 */
	public void updateUI()	{
		
		
	}

	
	/**
	 * 
	 *
	 */
	public void clean()	{
		
		eodMasque.setObjectArray(new NSArray());
		myEOTableMasque.updateData();
		eodControle.setObjectArray(new NSArray());
		myEOTableControle.updateData();
	}
		
	
	/**
	 *
	 */
	public void setUpdateEnable(boolean enable)	{
		
		actionAdd.setEnabled(enable);
		actionDelete.setEnabled(enable);
		
	}
	
	/**
	 * 
	 *
	 */
	public void load()	{

		NSApp.setWaitCursor(mainWindow);

		eodControle.setObjectArray(FinderBudgetVoteChapitreCtrl.findChapitresControles(ec, NSApp.getExerciceBudgetaire()));

		// Mise a jour du masque
		NSMutableArray masques = new NSMutableArray(FinderBudgetMasqueNature.findMasqueNatureDepense(ec, NSApp.getExerciceBudgetaire()));

		NSMutableArray masquesToDelete = new NSMutableArray();
		
		for (int i=0;i< eodControle.displayedObjects().count();i++)	{
			
			EOBudgetVoteChapitreCtrl controle = (EOBudgetVoteChapitreCtrl)eodControle.displayedObjects().objectAtIndex(i);
			
			masquesToDelete.addObject(FinderBudgetMasqueNature.findMasqueNature(ec ,controle.exercice(), controle.typeCredit(), controle.planComptable()));
			
		}
		
		masques.removeObjectsInArray(masquesToDelete);

		eodMasque.setObjectArray(masques);
		
		myEOTableControle.updateData();
		myEOTableMasque.updateData();

		myTableModelMasque.fireTableDataChanged();
		myTableModelControle.fireTableDataChanged();

		NSApp.setDefaultCursor(mainWindow);
	}
	
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class ControleRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			leComposant.setBackground(COULEUR_FOND_DEPENSE);	
			leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}

			return leComposant;
		}
	}
	
	
	
	/**
	 * Classe servant à colorer les cellules de la table affichant les options et remises.
	 *Certainement ameliorable en la rendant générique, indépendamment de la table (passer éventuellement par une interface).
	 */
	public class MasqueRenderer extends ZEOTableCellRenderer		{

		public final Color COULEUR_FOND_DEPENSE=new Color(218,221,255);
		public final Color COULEUR_TEXTE_DEPENSE=new Color(0,0,0);

		public final Color COULEUR_FOND_RECETTE=new Color(255, 207, 213);
		public final Color COULEUR_TEXTE_RECETTE=new Color(0,0,0);

		public final Color COULEUR_FOND_SELECTED=new Color(150,150,150);
		public final Color COULEUR_TEXTE_SELECTED=new Color(255,255,255);

		public void associerA(EOTable laTable)	{
			int indexColone;
			for(indexColone = 0; indexColone < laTable.table().getColumnModel().getColumnCount(); indexColone++)
				laTable.table().getColumnModel().getColumn(indexColone).setCellRenderer(this);
		}
		
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
						
			leComposant.setBackground(COULEUR_FOND_DEPENSE);
			leComposant.setForeground(COULEUR_TEXTE_DEPENSE);
			
			if(isSelected)	{
				leComposant.setBackground(COULEUR_FOND_SELECTED);
				leComposant.setForeground(COULEUR_TEXTE_SELECTED);
			}
			
			return leComposant;
		}
	}

	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_CLOSE);
        }
	    
        public void actionPerformed(ActionEvent e) {

        	mainWindow.dispose();
        
        }  
	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionAdd extends AbstractAction {

	    public ActionAdd() {
            super(null);
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_DROITE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	try {
        		
        		FactoryBudgetVoteChapitreCtrl myFactory = new FactoryBudgetVoteChapitreCtrl();
        		
        		for (int i=0;i<eodMasque.selectedObjects().count();i++)	{
        			
        			EOBudgetMasqueNature masque = (EOBudgetMasqueNature)eodMasque.selectedObjects().objectAtIndex(i);
        			
        			myFactory.creerBudgetVoteChapitreControle(
        					ec, 
        					NSApp.getExerciceBudgetaire(),
        					masque.planComptable(),
        					masque.typeCredit(), 
        					FinderTypeEtat.findTypeEtat(ec, EOTypeEtat.ETAT_VALIDE),
        					NSApp.getUtilisateur());        			
        		}
        		
        		ec.saveChanges();
        		
        		eodMasque.deleteSelection();        			
        		myEOTableMasque.updateData();
        		
        		eodControle.setObjectArray(FinderBudgetVoteChapitreCtrl.findChapitresControles(ec, NSApp.getExerciceBudgetaire()));
        		myEOTableControle.updateData();
        		
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        	}
        	
        }  
	} 
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public final class ActionDelete extends AbstractAction {

	    public ActionDelete() {
            super();
            this.putValue(AbstractAction.SMALL_ICON, ConstantesCocktail.ICON_FLECHE_GAUCHE);
        }
	    
        public void actionPerformed(ActionEvent e) {
        	
        	try {
        		        		
    			NSMutableArray listeMasques= new NSMutableArray(eodMasque.displayedObjects());

    			for (int i=0;i<eodControle.selectedObjects().count();i++)	{
        			
    				EOBudgetVoteChapitreCtrl controle = (EOBudgetVoteChapitreCtrl)eodControle.selectedObjects().objectAtIndex(i);

    				EOBudgetMasqueNature masqueNature = FinderBudgetMasqueNature.findMasqueNature(ec, NSApp.getExerciceBudgetaire(), controle.typeCredit(), controle.planComptable());
    				
        			listeMasques.addObject(masqueNature);
        			
        			ec.deleteObject(controle);           			

    			}        		

        		ec.saveChanges();
    			
    			myEOTableMasque.updateData();
    			
    			eodControle.deleteSelection();
       			myEOTableControle.updateData();       		    			

       			eodMasque.setObjectArray(listeMasques);
       			myEOTableMasque.updateData();
       			
        	}
        	catch (Exception ex)	{
        		ex.printStackTrace();
        	}
        	
        }  
	} 
	
	
}
