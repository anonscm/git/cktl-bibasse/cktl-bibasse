/* ImpressionsCtrl.java created by cpinsard on Wed 03-Mar-2004 */

package org.cocktail.bibasse.server.print;

import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;

import org.cocktail.bibasse.server.finder.Finder;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrintConst;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;

import com.webobjects.eocontrol.EOCustomObject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ImpressionsCtrl extends EOCustomObject {

    static String temporaryDir, osName;

    /**
  	* Recuperation d'un repertoire temporaraire ou ecrire les fichiers crees.
  	* @return Retourne le chemin complet du repertoire temporaire
  	*/
  	public static void initTemporaryDir() 	{

  		osName = System.getProperties().getProperty("os.name");

  		try {
  			temporaryDir = System.getProperty("java.io.tmpdir");

  			if (! temporaryDir.endsWith(File.separator) ) {
  				temporaryDir = temporaryDir + File.separator;
  			}
  		} catch (Exception e) {
  			temporaryDir = "/tmp";
  			System.out.println("Impossible de recuperer le repertoire temporaire !\nLe répertoire /tmp est choisi par défaut.");
  		}

  		File tmpDir = new File(temporaryDir);
  		if (!tmpDir.exists()) {
  			System.out.println("Tentative de creation du repertoire temporaire " + tmpDir);
  			try {
  				tmpDir.mkdirs();
  				System.out.println("Repertoire " + tmpDir + " cree.");
  			} catch (Exception e) {
  				System.out.println("Impossible de creer le repertoire " + tmpDir+". Le répertoire /tmp est choisi par défaut.");
  			  temporaryDir = "/tmp";
  			}
  		} else {
  			System.out.println("Repertoire Temporaire : " + tmpDir);
  		}
  	}

  	public static String temporaryDir()	{
  	    return temporaryDir;
  	}


    protected static void checkIfReportsExists(CktlPrinter printer, String maquetteID) throws Exception {
        if (!printer.checkTemplate(maquetteID)) {
           switch (printer.getErrorCode()) {
                case CktlPrintConst.ERR_CONNECT :
                    throw new Exception("Le service SIX " + " n'est pas disponible.");

                case CktlPrintConst.ERR_NO_TEMPLATE :
                    throw new Exception("Le modele d'impression "+maquetteID+" n'est pas reference dans la base de donnees du service d'impression.");

                case CktlPrintConst.ERR_OK :
                    break;

                default :
                    throw new Exception("Une erreur est survenue lors d'une operation SIX. Code erreur : " + printer.getErrorCode());
            }
        }
    }

    /**
     *
     * @param filePath
     * @param idMaquette
     * @return
     */
    public static NSDictionary imprimerPdf(String filePath, String idMaquette) {

        NSMutableDictionary dicoRetour = new NSMutableDictionary();

        // On suppose que cette methode existe
        Hashtable params = new Hashtable();

        EOEditingContext ec = new EOEditingContext();

        params.put("XML_PRINTER_DRIVER",Finder.getValueGrhumParametres(ec, "XML_PRINTER_DRIVER"));
        params.put("SIX_SERVICE_HOST",Finder.getValueGrhumParametres (ec, "SIX_SERVICE_HOST"));
        params.put("SIX_SERVICE_PORT",Finder.getValueGrhumParametres (ec, "SIX_SERVICE_PORT"));
        params.put("SIX_USE_COMPRESSION",Finder.getValueGrhumParametres (ec, "SIX_USE_COMPRESSION"));

        // On cree et on initialise le pilote d'impression
        CktlPrinter printer;

        try {
            printer = CktlPrinter.newDefaultInstance(params);
            checkIfReportsExists(printer, idMaquette);
        } catch (ClassNotFoundException e) {
            dicoRetour.setObjectForKey("SIX : Erreur serveur.\nMESSAGE : \n" + e.getMessage(), "message");
            return dicoRetour;
        } catch (Exception e) {
            dicoRetour.setObjectForKey("SIX : Erreur serveur.\nMESSAGE : \n" + e.getMessage(), "message");
            return dicoRetour;
        }

        String resultFile =  temporaryDir + "resultFile.pdf";
        String maquetteID = idMaquette;

        try {
            printer.printFileImmediate(maquetteID, filePath, resultFile);
        } catch (Exception e){
            dicoRetour.setObjectForKey("SIX : Erreur serveur.\nMESSAGE : \n" + e.getMessage(), "message");
            return dicoRetour;
        }

        if (printer.hasSuccess()) {
            System.out.println("OK. Reponse de pilote : "+printer.getMessage());
        } else {
            dicoRetour.setObjectForKey("SIX : Erreur serveur.\nMESSAGE : \n" + printer.getMessage(), "message");
            return dicoRetour;
        }

        NSData datas = null;

        try {
			FileInputStream inputStream = new FileInputStream(resultFile);
			datas = new NSData(inputStream, 1024);
		} catch (Exception e) {
        }

        dicoRetour.setObjectForKey(datas, "datas");
        return dicoRetour;
    }

}
