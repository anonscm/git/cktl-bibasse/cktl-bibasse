package org.cocktail.bibasse.server.cofisup;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.finder.FinderBudgetParametres;
import org.cocktail.bibasse.server.metier.EOBudgetParametres;
import org.cocktail.bibasse.server.utilities.VariableResolver;
import org.cocktail.common.cofisup.ETypeBudget;

import com.webobjects.foundation.NSArray;

/**
 * Classe de base des servcies fournisseurs de données au sein de Cofisup.
 */
public abstract class AbstractCofisupDataProvider {

	/** Clé montant. */
	public static final String KEY_MONTANT = "MONTANT";

	/** Variable Section. */
	public static final String VAR_SECTION = "section";

	/** Variable Exercice. */
	public static final String VAR_EXERCICE = "exercice";

	/** Variable organigrammesId. */
	public static final String VAR_ORG_IDS = "orgIds";

	/** Variable type de montant. */
	public static final String VAR_TYPE_MONTANT = "typeMt";

	/** Variable requete interne. */
	public static final String VAR_INNER_QUERY = "innerQuery";

	public static final NSArray KEYS_MONTANT = new NSArray(new Object[] {KEY_MONTANT});

	public static final String SUBQUERY_SELECT_ORGANIGRAMME = new StringBuilder()
		.append(" select o.org_id ")
		.append("   from jefy_admin.organ o ")
		.append("  start with o.org_pere in ( ")
		.append("    select org_id from jefy_admin.organ tmpO ")
		.append("     where tmpO.org_id in (:orgIds) ) ")
		.append("connect by prior o.org_id = o.org_pere ")
		.toString();

	public static final String FROM_TITRES_VISES = new StringBuilder()
		.append("  from jefy_recette.recette_ctrl_planco recPlanco ")
		.append("  join jefy_recette.recette rec on rec.rec_id = recPlanco.rec_id ")
		.append("  join jefy_recette.facture fac on rec.fac_id = fac.fac_id ")
		.append("  join maracuja.titre titre on titre.tit_id = recPlanco.tit_id ")
		.append("  join jefy_admin.type_credit typeCredit on typeCredit.tcd_ordre = fac.tcd_ordre ")
		.append(" where titre.exe_ordre = :exercice ")
		.append("   and titre.tit_etat = 'VISE' ")
		.append("   and fac.org_id in ( ")
		.append(          SUBQUERY_SELECT_ORGANIGRAMME)
		.append("       ) ")
		.toString();

	public static final String FROM_MANDATS_VISES = new StringBuilder()
		.append("  from jefy_depense.depense_ctrl_planco depPlanco ")
		.append("  join jefy_depense.depense_budget depense on depPlanco.dep_id = depense.dep_id ")
		.append("  join jefy_depense.engage_budget engage on depense.eng_id = engage.eng_id ")
		.append("  join jefy_admin.type_credit typeCredit on engage.tcd_ordre = typeCredit.tcd_ordre ")
		.append("  join maracuja.mandat mandat on mandat.man_id = depPlanco.man_id ")
		.append(" where mandat.exe_ordre = :exercice ")
		.append("   and (mandat.man_etat = 'VISE' or mandat.man_etat = 'PAYE') ")
		.append("   and engage.org_id in ( ")
		.append(			SUBQUERY_SELECT_ORGANIGRAMME)
		.append("      ) ")
		.toString();

	private VariableResolver varResolver;

	public AbstractCofisupDataProvider() {
		this.varResolver = new VariableResolver();
	}

	public Map<String, String> buildBudgetsNatureQueryParameters(Number exercice, Collection<? extends Number> organIds, ETypeBudget typeBudget) {
		Map<String, String> variables = new HashMap<String, String>();
		variables.put(VAR_EXERCICE, exercice.toString());
		variables.put(VAR_ORG_IDS, buildRestrictionOrganigrammes(organIds));
		variables.put(VAR_TYPE_MONTANT, buildTypeMontantNature(typeBudget));

		return variables;
	}

	public Map<String, String> buildBudgetsGestionQueryParameters(Number exercice, Collection<? extends Number> organIds, ETypeBudget typeBudget) {
		Map<String, String> variables = new HashMap<String, String>();
		variables.put(VAR_EXERCICE, exercice.toString());
		variables.put(VAR_ORG_IDS, buildRestrictionOrganigrammes(organIds));
		variables.put(VAR_TYPE_MONTANT, buildTypeMontantGestion(typeBudget));

		return variables;
	}

	public Map<String, String> buildRceExecutionBudgetaireQueryParameters(Number exercice, Collection<? extends Number> organIds) {
		Map<String, String> variables = new HashMap<String, String>();
		variables.put(VAR_EXERCICE, exercice.toString());
		variables.put(VAR_ORG_IDS, buildRestrictionOrganigrammes(organIds));

		return variables;
	}

	public boolean isComptePrisEnCompte(QueryExecutor executor, Number exercice, String budgetParametre) {
		EOBudgetParametres param = (EOBudgetParametres) executor.executeUniqueResult(
				FinderBudgetParametres.buildFetchSpecification(exercice, budgetParametre));
        boolean prisEnCompte = false;
        if (param != null && "OUI".equals(param.bparValue())) {
        	prisEnCompte = true;
        }
        return prisEnCompte;
	}

	public String wrapQuery(String outerQuery, String innerQuery) {
		return outerQuery.replaceFirst(":" + VAR_INNER_QUERY, innerQuery);
	}

	protected String buildTypeMontantNature(ETypeBudget typeBudget) {
		String typeMt = null;
		switch(typeBudget) {
		case BP :
			typeMt = "bvn.bdvn_primitifs";
			break;
		case BM :
			typeMt = "(bvn.bdvn_primitifs + bvn.bdvn_reliquats + bvn.bdvn_credits + bvn.bdvn_debits + bvn.bdvn_dbms)";
			break;
		default :
			typeMt = "bvn.bdvn_primitifs";
		}
		return typeMt;
	}

	protected String buildTypeMontantGestion(ETypeBudget typeBudget) {
		String typeMt = null;
		switch(typeBudget) {
		case BP :
			typeMt = "bvg.bdvg_primitifs";
			break;
		case BM :
			typeMt = "(bvg.bdvg_primitifs + bvg.bdvg_reliquats + bvg.bdvg_credits + bvg.bdvg_debits + bvg.bdvg_dbms)";
			break;
		default :
			typeMt = "bvg.bdvg_primitifs";
		}
		return typeMt;
	}

	public String buildRestrictionOrganigrammes(Collection<? extends Number> organIds) {
		if (organIds == null) {
			return StringUtils.EMPTY;
		}

		StringBuilder builder = new StringBuilder();
		Iterator<? extends Number> organsIterator = organIds.iterator();
		while (organsIterator.hasNext()) {
			Number orgId = organsIterator.next();
			builder.append(orgId);
			if (organsIterator.hasNext()) {
				builder.append(",");
			}
		}
		return builder.toString();
	}

	public BigDecimal getMontant(QueryExecutor executor, String query, Map<String, String> variables) {
		String sqlMontant = getVarResolver().resolve(query, variables);
		NSArray montantData = executor.execute(sqlMontant, KEYS_MONTANT);
		return extractMontantUnique(montantData);
	}

	public BigDecimal extractMontantUnique(NSArray resultats) {
		BigDecimal montant = BigDecimal.ZERO;
		if (resultats != null && resultats.count() == 1) {
			Map<String, Object> resultat = (Map<String, Object>) resultats.objectAtIndex(0);
			if (resultat.containsKey(KEY_MONTANT)) {
				Number montantAsNumber = (Number) resultat.get(KEY_MONTANT);
				montant = new BigDecimal(montantAsNumber.doubleValue());
			}
		}
		return montant;
	}

	public VariableResolver getVarResolver() {
		return varResolver;
	}

	public void setVarResolver(VariableResolver varResolver) {
		this.varResolver = varResolver;
	}

	protected class CofisupDataIterator implements Iterator<CofisupData> {

		private Set<String> keyset;
		private NSArray data;
		private Iterator<Map<String, Object>> dataIterator;

		public CofisupDataIterator(Set<String> keyset, NSArray data) {
			this.keyset = keyset;
			this.data = data;
			if (data != null) {
				this.dataIterator = data.iterator();
			}
		}

		public boolean hasNext() {
			boolean hasNext = false;
			if (dataIterator != null) {
				hasNext = dataIterator.hasNext();
			}
			return hasNext;
		}

		public CofisupData next() {
			Map<String, Object> localData = null;
			if (dataIterator != null) {
				localData = dataIterator.next();
			}
			return new CofisupData(keyset, localData);
		}

		public void remove() {
			if (dataIterator != null) {
				dataIterator.remove();
			}
		}
	}
}
