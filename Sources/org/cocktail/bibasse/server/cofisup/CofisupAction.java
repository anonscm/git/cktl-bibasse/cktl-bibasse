package org.cocktail.bibasse.server.cofisup;

import static org.cocktail.common.cofisup.CofisupActionConstants.INFO_JOB_FILE;
import static org.cocktail.common.cofisup.CofisupActionConstants.INFO_JOB_ID;
import static org.cocktail.common.cofisup.CofisupActionConstants.INFO_MSG;
import static org.cocktail.common.cofisup.CofisupActionConstants.INFO_STATUS;
import static org.cocktail.common.cofisup.CofisupActionConstants.STATUS_CANCELLED;
import static org.cocktail.common.cofisup.CofisupActionConstants.STATUS_DONE;
import static org.cocktail.common.cofisup.CofisupActionConstants.STATUS_ERROR;
import static org.cocktail.common.cofisup.CofisupActionConstants.STATUS_PENDING;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.cocktail.batch.Job;
import org.cocktail.bibasse.server.Application;
import org.cocktail.bibasse.server.cofisup.batch.JobTask;
import org.cocktail.common.cofisup.JobParameters;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;

/**
 * Classe gérant les appels Cofisup du client.
 */
public final class CofisupAction implements Serializable {

	/** Serial version ID.*/
	private static final long serialVersionUID = 1L;

	/* Attributes. */
	/** Moniteur les tâches asynchrones. */
    private ExecutorCompletionService<Job> executorCompletionService;

    /** Tâches asynchrones representant un job Cofisup. */
    private Map<Future<Job>, String> tasks;

    /** Nouvelle pile Base de données pour être autonome. */
    private EOObjectStoreCoordinator osc;

    /**
     * Constructeur.
     */
    public CofisupAction() {
        this.executorCompletionService = new ExecutorCompletionService<Job>(Executors.newSingleThreadExecutor());
        this.tasks = new HashMap<Future<Job>, String>();
        this.osc = null;
    }

    /**
     * Lance un job Cofisup de manière asynchrone.
     * @param parametres parametres de configuration du job.
     * @return informations relatives au job nouvelle lancé.
     * @throws Exception en cas d'anmolie pendant le traitement.
     */
    public NSArray clientStartJob(NSDictionary parametres) throws Exception {
    	NSMutableArray<String> jobResults = new NSMutableArray<String>();
    	JobParameters jobParametres = new JobParameters(parametres);

    	JobBuilder jobBuilder = new JobBuilder(newEc(getOrCreateOSC()), getWoApp().mainModelName());
    	final List<Job> jobs = jobBuilder.build(jobParametres);

    	for (Job currentJob : jobs) {
    		Future<Job> newTask = getExecutorCompletionService().submit(new JobTask(currentJob));
    		jobResults.addObject(currentJob.getId());
    		tasks.put(newTask, jobParametres.getLibelle());
    	}

    	return jobResults;
    }

    public NSDictionary clientInfosJob() throws Exception {
    	NSMutableDictionary<String, String> infos = new NSMutableDictionary<String, String>();
    	Future<Job> nextCompletedCofisupJob = getExecutorCompletionService().poll();

    	String status = STATUS_PENDING;
    	String jobId = null;
    	String jobFile = null;
    	String msg = null;
    	Job completedJob = null;

    	if (nextCompletedCofisupJob != null) {
    		try {
    			completedJob = nextCompletedCofisupJob.get();
    			status = STATUS_DONE;
    			jobId = completedJob.getId();
    			jobFile = completedJob.getFilename();
    		} catch (CancellationException ce) {
    			status = STATUS_CANCELLED;
    			System.out.println("Cofisup - Job annulé : " + ce.getMessage());
    		} catch (InterruptedException ie) {
    			status = STATUS_CANCELLED;
    			System.out.println("Cofisup - Job interrompu : " + ie.getMessage());
    		} catch (ExecutionException ee) {
    			status = STATUS_ERROR;
    			msg = tasks.get(nextCompletedCofisupJob);
    			String errMsg = "";
    			Throwable t = ExceptionUtils.getRootCause(ee);
    			if (t != null) {
    				errMsg = t.getMessage();
    			} else {
    				errMsg = ee.getMessage();
    			}
    			msg += " - " + errMsg;
    		} finally {
    			tasks.remove(nextCompletedCofisupJob);
    		}
    	}

    	infos.takeValueForKey(status, INFO_STATUS);
    	infos.takeValueForKey(jobId, INFO_JOB_ID);
    	infos.takeValueForKey(jobFile, INFO_JOB_FILE);
    	infos.takeValueForKey(msg, INFO_MSG);

    	return infos;
    }

    public NSData clientRetrieveFile(String jobId) throws Exception {
    	NSData fileAsData = null;
    	File jobFile = new File(jobId);
    	try {
    		fileAsData = new NSData(IOUtils.toByteArray(new BufferedReader(new FileReader(jobFile))));
    	} catch (Exception e) {
    		System.err.println("Cofisup - Echec envoi fichier serveur -> client : " + e);
    		throw e;
    	} finally {
    		jobFile.delete();
    	}

    	return fileAsData;
    }

    public Application getWoApp() {
    	return (Application) Application.application();
    }

	public EOEditingContext newEc(EOObjectStoreCoordinator osc) {
		return ERXEC.newEditingContext(osc);
	}

	public EOObjectStoreCoordinator getOrCreateOSC() {
		if (this.osc == null) {
			this.osc = new EOObjectStoreCoordinator();
		}
		return this.osc;
	}

	public ExecutorCompletionService<Job> getExecutorCompletionService() {
		return executorCompletionService;
	}

	public void setExecutorCompletionService(ExecutorCompletionService<Job> executorCompletionService) {
		this.executorCompletionService = executorCompletionService;
	}
}
