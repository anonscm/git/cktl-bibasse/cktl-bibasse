package org.cocktail.bibasse.server.cofisup;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXGenericRecord;

public interface QueryExecutor {

	public NSArray execute(String sqlQuery, NSArray sqlKeys);
	public ERXGenericRecord executeUniqueResult(EOFetchSpecification fetchSpecification);

}
