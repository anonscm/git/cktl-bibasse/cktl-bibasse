package org.cocktail.bibasse.server.cofisup;

import static org.cocktail.common.cofisup.ETypeEtablissement.NON_RCE;
import static org.cocktail.common.cofisup.ETypeEtablissement.RCE;
import static org.cocktail.common.cofisup.ETypeFichier.BPI;
import static org.cocktail.common.cofisup.ETypeFichier.CR;
import static org.cocktail.common.cofisup.ETypeFichier.RD;
import static org.cocktail.common.cofisup.ETypeFichier.TF;
import static org.cocktail.common.cofisup.ETypeNatureBudget.EPRD;
import static org.cocktail.common.cofisup.ETypeNatureBudget.NON_EPRD;
import static org.cocktail.common.cofisup.ETypeNatureBudget.PRINCIPAL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.cocktail.batch.Job;
import org.cocktail.batch.JobContext;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.BudgetsDetailsDepensesGestion;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.BudgetsDetailsNature;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.BudgetsDetailsRecettesGestion;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.ExecutionBudgetaireDetailsDepensesGestion;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.ExecutionBudgetaireDetailsDepensesNature;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.ExecutionBudgetaireDetailsRecettesGestion;
import org.cocktail.bibasse.server.cofisup.batch.data.nonrce.ExecutionBudgetaireDetailsRecettesNature;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.BudgetsBudgetPropreIntegre;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.BudgetsCompteResultat;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.BudgetsTableauFinancement;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.ExecutionBudgetaireBudgetPropreIntegre;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.ExecutionBudgetaireCompteResultat;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.ExecutionBudgetaireTableauFinancement;
import org.cocktail.bibasse.server.cofisup.batch.processor.BudgetsBPItemProcessor;
import org.cocktail.bibasse.server.cofisup.batch.processor.CompteResultatProcessor;
import org.cocktail.bibasse.server.cofisup.batch.processor.ExecutionBudgetaireBPIItemProcessor;
import org.cocktail.bibasse.server.cofisup.batch.processor.NonRceRecettesDepensesProcessor;
import org.cocktail.bibasse.server.cofisup.batch.processor.TableauFinancementProcessor;
import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupFilenameGenerator;
import org.cocktail.bibasse.server.cofisup.job.IJobBuilder;
import org.cocktail.bibasse.server.cofisup.job.JobInfosComplementairesBudgetPropreIntegre;
import org.cocktail.bibasse.server.cofisup.job.JobInfosComplementairesCompteResultat;
import org.cocktail.bibasse.server.cofisup.job.JobInfosComplementairesTableauFinancement;
import org.cocktail.bibasse.server.cofisup.job.JobNonRceBudget;
import org.cocktail.bibasse.server.cofisup.job.JobNonRceRecettesDepensesSplit;
import org.cocktail.bibasse.server.cofisup.job.JobRceExecution;
import org.cocktail.bibasse.server.cofisup.job.JobRceRdBudget;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.cofisup.ETypeBudget;
import org.cocktail.common.cofisup.ETypeEtablissement;
import org.cocktail.common.cofisup.ETypeFichier;
import org.cocktail.common.cofisup.ETypeNatureBudget;
import org.cocktail.common.cofisup.JobParameters;
import org.cocktail.common.factory.BanqueFactory;
import org.cocktail.common.metier.Banque;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe facilitant la création des jobs.
 */
public class JobBuilder {

	private static final JobType TYPE_RCE_RD_BUDGET  = new JobType(RCE, RD,  true);
	private static final JobType TYPE_RCE_RD_EXEC    = new JobType(RCE, RD,  false);
	private static final JobType TYPE_RCE_CR_BUDGET  = new JobType(RCE, CR,  true);
	private static final JobType TYPE_RCE_CR_EXEC    = new JobType(RCE, CR,  false);
	private static final JobType TYPE_RCE_TF_BUDGET  = new JobType(RCE, TF,  true);
	private static final JobType TYPE_RCE_TF_EXEC    = new JobType(RCE, TF,  false);
	private static final JobType TYPE_RCE_BPI_BUDGET = new JobType(RCE, BPI, true);
	private static final JobType TYPE_RCE_BPI_EXEC   = new JobType(RCE, BPI, false);

	private static final JobType TYPE_NON_RCE_RD_BUDGET_NATURE   = new JobType(NON_RCE, RD, true,  false);
	private static final JobType TYPE_NON_RCE_RD_BUDGET_GESTION  = new JobType(NON_RCE, RD, true,  true);
	private static final JobType TYPE_NON_RCE_RD_EXEC_NATURE     = new JobType(NON_RCE, RD, false, false);
	private static final JobType TYPE_NON_RCE_RD_EXEC_GESTION    = new JobType(NON_RCE, RD, false, true);
	private static final JobType TYPE_NON_RCE_CR_BUDGET  = new JobType(NON_RCE, CR, true);
	private static final JobType TYPE_NON_RCE_CR_EXEC    = new JobType(NON_RCE, CR, false);
	private static final JobType TYPE_NON_RCE_TF_BUDGET  = new JobType(NON_RCE, TF, true);
	private static final JobType TYPE_NON_RCE_TF_EXEC    = new JobType(NON_RCE, TF, false);

	private EOEditingContext ec;
	private String modelName;
	private Map<JobType, IJobBuilder> builders;
	private Banque banque;

	/**
	 * Constructeur.
	 * @param ec editingContext.
	 * @param modelName nom du model.
	 */
	public JobBuilder(EOEditingContext ec, String modelName) {
		this.ec = ec;
		this.modelName = modelName;
		this.builders = new HashMap<JobBuilder.JobType, IJobBuilder>();
		
		initBanque();
		initRceJobBuilders();
		initNonRceJobBuilders();
	}

	private void initBanque() {
	    BanqueFactory factory = new BanqueFactory();
	    this.banque = factory.creerBanque(this.ec);
	}
	
	private void initRceJobBuilders() {
		builders.put(TYPE_RCE_RD_BUDGET, new JobRceRdBudget());
		builders.put(TYPE_RCE_RD_EXEC, new JobRceExecution());
		builders.put(TYPE_RCE_CR_BUDGET,
				new JobInfosComplementairesCompteResultat(
						new BudgetsCompteResultat(), new CompteResultatProcessor()));
		builders.put(TYPE_RCE_CR_EXEC,
				new JobInfosComplementairesCompteResultat(
						new ExecutionBudgetaireCompteResultat(), new CompteResultatProcessor()));
		builders.put(TYPE_RCE_TF_BUDGET,
				new JobInfosComplementairesTableauFinancement(
						new BudgetsTableauFinancement(new BudgetsCompteResultat()),
						new TableauFinancementProcessor()));
		builders.put(TYPE_RCE_TF_EXEC,
				new JobInfosComplementairesTableauFinancement(
						new ExecutionBudgetaireTableauFinancement(new ExecutionBudgetaireCompteResultat()),
						new TableauFinancementProcessor()));
		builders.put(TYPE_RCE_BPI_BUDGET,
				new JobInfosComplementairesBudgetPropreIntegre(
						new BudgetsBudgetPropreIntegre(),
						new BudgetsBPItemProcessor()));
		builders.put(TYPE_RCE_BPI_EXEC,
				new JobInfosComplementairesBudgetPropreIntegre(
						new ExecutionBudgetaireBudgetPropreIntegre(),
						new ExecutionBudgetaireBPIItemProcessor()));
	}

	private void initNonRceJobBuilders() {
		builders.put(TYPE_NON_RCE_RD_BUDGET_NATURE,
				new JobNonRceBudget(new BudgetsDetailsNature(), new NonRceRecettesDepensesProcessor()));
		builders.put(TYPE_NON_RCE_RD_BUDGET_GESTION,
				new JobNonRceRecettesDepensesSplit(
						new BudgetsDetailsRecettesGestion(), new NonRceRecettesDepensesProcessor(),
						new BudgetsDetailsDepensesGestion(), new NonRceRecettesDepensesProcessor()));
		builders.put(TYPE_NON_RCE_RD_EXEC_NATURE,
				new JobNonRceRecettesDepensesSplit(
						new ExecutionBudgetaireDetailsRecettesNature(), new NonRceRecettesDepensesProcessor(),
						new ExecutionBudgetaireDetailsDepensesNature(), new NonRceRecettesDepensesProcessor()));
		builders.put(TYPE_NON_RCE_RD_EXEC_GESTION,
				new JobNonRceRecettesDepensesSplit(
						new ExecutionBudgetaireDetailsRecettesGestion(), new NonRceRecettesDepensesProcessor(),
						new ExecutionBudgetaireDetailsDepensesGestion(), new NonRceRecettesDepensesProcessor()));
		builders.put(TYPE_NON_RCE_CR_BUDGET,
				new JobInfosComplementairesCompteResultat(
						new BudgetsCompteResultat(), new CompteResultatProcessor()));
		builders.put(TYPE_NON_RCE_CR_EXEC,
				new JobInfosComplementairesCompteResultat(
						new ExecutionBudgetaireCompteResultat(), new CompteResultatProcessor()));
		builders.put(TYPE_NON_RCE_TF_BUDGET,
				new JobInfosComplementairesTableauFinancement(
						new BudgetsTableauFinancement(new BudgetsCompteResultat()),
						new TableauFinancementProcessor()));
		builders.put(TYPE_NON_RCE_TF_EXEC,
				new JobInfosComplementairesTableauFinancement(
						new ExecutionBudgetaireTableauFinancement(new ExecutionBudgetaireCompteResultat()),
						new TableauFinancementProcessor()));
	}

	public List<Job> build(JobParameters jobParameters) throws IOException {

		List<Job> jobs = new ArrayList<Job>();

		EOExercice exercice = getExercice(jobParameters.getExercice());

    	ETypeNatureBudget natureBudget = jobParameters.getNatureBudget();
    	ETypeFichier typeFichier = jobParameters.getTypeFichier();
    	ETypeBudget typeBudget = jobParameters.getTypeBudget();
    	ETypeEtablissement typeEtablissement = jobParameters.getTypeEtablissement();

		Set<EOOrgan> organigrammes = CofisupDao.instance().getOrganigrammes(ec, exercice, resolveCodesBase(natureBudget));

		JobType jobType = new JobType(typeEtablissement, typeFichier, !ETypeBudget.EB.equals(typeBudget), natureBudget.isGestion());
		IJobBuilder jobBuilder = builders.get(jobType);
		if (jobBuilder == null) {
			return jobs;
		}
		
		switch(natureBudget) {
		case AGREGE :
		case ETAB_NATURE :
		case ETAB_GESTION :
			jobs.add(newJob(jobBuilder, jobParameters, organigrammes, 1));
			break;
		default :
			for (EOOrgan organ : organigrammes) {
				jobs.add(newJob(jobBuilder, jobParameters, Collections.singleton(organ), organ.toNatureBudget().onbSequence()));
			}
			break;
		}

		return jobs;
	}

	private Job newJob(IJobBuilder builder, JobParameters jobParameters, Set<EOOrgan> organigrammes, int seq) throws IOException {
    	ETypeNatureBudget nature = jobParameters.getNatureBudget();
    	ETypeFichier typeFichier = jobParameters.getTypeFichier();
    	ETypeBudget typeBudget = jobParameters.getTypeBudget();
    	EOExercice exerciceEnCours = getExercice(jobParameters.getExerciceEnCours());
    	EOExercice exercice = getExercice(jobParameters.getExercice());
    	String codeUAI = CofisupDao.instance().getCodeUAIEtablissement(ec, exercice);
        String devise = CofisupDao.instance().getDevise(ec, exercice);
        Banque banqueLocale = getBanque();
        
		return builder.build(
				CofisupFilenameGenerator.generateFilename(
				    nature, seq, typeFichier, typeBudget, exercice.exeExercice(), codeUAI),
				buildContext(
				    typeFichier, typeBudget, nature, seq, exerciceEnCours, exercice, organigrammes, 
				    banqueLocale, devise));
	}

	private JobContext buildContext(ETypeFichier typeFichier, ETypeBudget typeBudget, 
            ETypeNatureBudget natureBudget, int sequence, EOExercice exerciceEnCours, 
            EOExercice exercice, Set<EOOrgan> organigrammes, Banque banque, String devise) {

		JobContext jobContext = new JobContext();
		jobContext.setEc(ec);
		jobContext.setExerciceEnCours(exerciceEnCours);
		jobContext.setExercice(exercice);
		jobContext.setModelName(modelName);
		jobContext.setSequence(sequence);
		jobContext.setTypeFichier(typeFichier);
		jobContext.setNatureBudget(natureBudget);
		jobContext.setTypeBudget(typeBudget);
		jobContext.setOrganigrammes(organigrammes);
		jobContext.setBanque(banque);
		jobContext.setDevise(devise);

		return jobContext;
	}

	private EOExercice getExercice(Number exerciceAsNum) {
		return EOExercice.fetchByKeyValue(ec, EOExercice.EXE_ORDRE_KEY, exerciceAsNum);
	}

	private Set<String> resolveCodesBase(ETypeNatureBudget natureBudget) {
		Set<String> codesNatures = null;
		switch(natureBudget) {
		case AGREGE :
		case ETAB_NATURE :
		case ETAB_GESTION :
			codesNatures = new HashSet<String>();
			Collections.addAll(codesNatures, PRINCIPAL.getCodeBase(), EPRD.getCodeBase(), NON_EPRD.getCodeBase());
			break;
		default :
			codesNatures = Collections.singleton(natureBudget.getCodeBase());
		}

		return codesNatures;
	}

	/**
	 * Decrit un type de job.
	 */
    private static final class JobType {
    	private ETypeEtablissement typeEtablissement;
    	private ETypeFichier typeFichier;
    	private boolean isBudget;
    	private boolean isGestion;

    	public JobType(ETypeEtablissement typeEtablissement, ETypeFichier typeFichier, boolean isBudget) {
    		this(typeEtablissement, typeFichier, isBudget, false);
    	}

    	public JobType(ETypeEtablissement typeEtablissement, ETypeFichier typeFichier, boolean isBudget, boolean isGestion) {
    		this.typeEtablissement = typeEtablissement;
    		this.typeFichier = typeFichier;
    		this.isBudget = isBudget;
    		this.isGestion = isGestion;
    	}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) { return true; }
			if (obj == null) { return false; }
			if (getClass() != obj.getClass()) {
				return false;
			}

			JobType other = (JobType) obj;
			return new EqualsBuilder()
            	.append(isBudget, other.isBudget())
            	.append(isGestion, other.isGestion())
            	.append(typeEtablissement, other.getTypeEtablissement())
            	.append(typeFichier, other.getTypeFichier())
            	.isEquals();
		}

		@Override
		public int hashCode() {
			return new HashCodeBuilder(17, 37)
				       .append(getTypeEtablissement())
				       .append(getTypeFichier())
				       .append(isBudget())
				       .append(isGestion())
				       .toHashCode();
		}

		public ETypeEtablissement getTypeEtablissement() {
			return typeEtablissement;
		}

		public ETypeFichier getTypeFichier() {
			return typeFichier;
		}

		public boolean isBudget() {
			return isBudget;
		}

		public boolean isGestion() {
			return isGestion;
		}
    }

    public Banque getBanque() {
        return banque;
    }
}
