package org.cocktail.bibasse.server.cofisup;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class EmptyDataProvider implements CofisupDataProvider {

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext context) {
		List<CofisupData> emptyList = Collections.emptyList();
		return emptyList.iterator();
	}
}
