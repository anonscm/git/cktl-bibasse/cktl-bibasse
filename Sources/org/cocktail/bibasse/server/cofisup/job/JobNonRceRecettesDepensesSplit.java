/*
 * Copyright COCKTAIL, 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.server.cofisup.job;

import java.io.File;
import java.io.IOException;

import org.cocktail.batch.Job;
import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.CofisupBeanLineAgreggator;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.api.ItemReader;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.api.Step;
import org.cocktail.batch.api.step.SimpleStep;
import org.cocktail.batch.item.file.FlatFileItemWriter;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.batch.RecetteDepenseBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.cofisup.batch.reader.StrategyItemReader;
import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.bibasse.server.cofisup.batch.writer.CompositeCloserItemWriter;
import org.cocktail.bibasse.server.cofisup.batch.writer.CompositeOpenerItemWriter;
import org.cocktail.bibasse.server.cofisup.batch.writer.RecetteDepenseRceItemWriter;
import org.cocktail.common.metier.Banque;

public class JobNonRceRecettesDepensesSplit implements IJobBuilder {

	private CofisupDataProvider recetteProvider;
	private ItemProcessor<CofisupData, RecetteDepenseBean> recetteProcessor;

	private CofisupDataProvider depenseProvider;
	private ItemProcessor<CofisupData, RecetteDepenseBean> depenseProcessor;

	public JobNonRceRecettesDepensesSplit(
			CofisupDataProvider recetteProvider, ItemProcessor<CofisupData, RecetteDepenseBean>  recetteProcessor,
			CofisupDataProvider depenseProvider, ItemProcessor<CofisupData, RecetteDepenseBean>  depenseProcessor) {
		this.recetteProvider = recetteProvider;
		this.recetteProcessor = recetteProcessor;
		this.depenseProvider = depenseProvider;
		this.depenseProcessor = depenseProcessor;
	}

	public Job build(String filename, JobContext jobContext) throws IOException {
	    Banque banque = jobContext.getBanque();
        String devise = jobContext.getDevise();
	    
		File cofisupFile = CofisupUtils.createTempFile(jobContext.getNatureBudget());
		RecetteDepenseRceItemWriter writerDelegate = new RecetteDepenseRceItemWriter();

		// step recette
		ItemReader<CofisupData> readerRecette = new StrategyItemReader(getRecetteProvider());

		ItemWriter<RecetteDepenseBean> writerRecette = new CompositeOpenerItemWriter<RecetteDepenseBean>(writerDelegate);
		FlatFileItemWriter<RecetteDepenseBean> fileWriter = new FlatFileItemWriter<RecetteDepenseBean>();
		fileWriter.setResource(cofisupFile);
		fileWriter.setHeaderCallback(writerDelegate);
		fileWriter.setFooterCallback(writerDelegate);
	    fileWriter.setLineAggregator(new CofisupBeanLineAgreggator<RecetteDepenseBean>(devise, banque));

		writerDelegate.setDelegate(fileWriter);

		Step stepRecette = new SimpleStep<CofisupData, RecetteDepenseBean>(
				jobContext, readerRecette, writerRecette, getRecetteProcessor());

		// step depense
		ItemReader<CofisupData> readerDepense = new StrategyItemReader(getDepenseProvider());
		ItemWriter<RecetteDepenseBean> writerDepense = new CompositeCloserItemWriter<RecetteDepenseBean>(writerDelegate);
		Step stepDepense = new SimpleStep<CofisupData, RecetteDepenseBean>(
				jobContext, readerDepense, writerDepense, getDepenseProcessor());

		// job
		Job job = new Job(cofisupFile.getAbsolutePath());
		job.setFilename(filename);
		job.addStep(stepRecette);
		job.addStep(stepDepense);

		return job;
	}

	public CofisupDataProvider getRecetteProvider() {
		return recetteProvider;
	}

	public void setRecetteProvider(CofisupDataProvider recetteProvider) {
		this.recetteProvider = recetteProvider;
	}

	public CofisupDataProvider getDepenseProvider() {
		return depenseProvider;
	}

	public void setDepenseProvider(CofisupDataProvider depenseProvider) {
		this.depenseProvider = depenseProvider;
	}

	public ItemProcessor<CofisupData, RecetteDepenseBean> getRecetteProcessor() {
		return recetteProcessor;
	}

	public void setRecetteProcessor(ItemProcessor<CofisupData, RecetteDepenseBean> recetteProcessor) {
		this.recetteProcessor = recetteProcessor;
	}

	public ItemProcessor<CofisupData, RecetteDepenseBean> getDepenseProcessor() {
		return depenseProcessor;
	}

	public void setDepenseProcessor(ItemProcessor<CofisupData, RecetteDepenseBean> depenseProcessor) {
		this.depenseProcessor = depenseProcessor;
	}
}
