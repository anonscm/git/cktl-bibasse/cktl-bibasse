package org.cocktail.bibasse.server.cofisup.job;

import org.cocktail.batch.api.CofisupBeanLineAgreggator;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.api.LineAggregator;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.batch.CompteResultatBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.common.metier.Banque;

public class JobInfosComplementairesCompteResultat extends AbstractJobInfosComplementaires<CompteResultatBean> {

    public JobInfosComplementairesCompteResultat(CofisupDataProvider dataProvider, ItemProcessor<CofisupData, CompteResultatBean> processor) {
        super(dataProvider, processor);
    }

    @Override
    public LineAggregator<CompteResultatBean> lineAggregator(String devise, Banque banque) {
        return new CofisupBeanLineAgreggator<CompteResultatBean>(devise, banque);
    }

}
