package org.cocktail.bibasse.server.cofisup.job;

import org.cocktail.batch.api.CofisupBeanLineAgreggator;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.api.LineAggregator;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.batch.BudgetPropreIntegreBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.common.metier.Banque;

public class JobInfosComplementairesBudgetPropreIntegre extends AbstractJobInfosComplementaires<BudgetPropreIntegreBean> {

    public JobInfosComplementairesBudgetPropreIntegre(CofisupDataProvider dataProvider, ItemProcessor<CofisupData, BudgetPropreIntegreBean> processor) {
        super(dataProvider, processor);
    }

    @Override
    public LineAggregator<BudgetPropreIntegreBean> lineAggregator(String devise, Banque banque) {
        return new CofisupBeanLineAgreggator<BudgetPropreIntegreBean>(devise, banque);
    }

}
