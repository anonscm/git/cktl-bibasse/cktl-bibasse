package org.cocktail.bibasse.server.cofisup.job;

import org.cocktail.batch.api.CofisupBeanLineAgreggator;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.api.LineAggregator;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.batch.TableauFinancementBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.common.metier.Banque;

public class JobInfosComplementairesTableauFinancement extends AbstractJobInfosComplementaires<TableauFinancementBean> {

    public JobInfosComplementairesTableauFinancement(CofisupDataProvider dataProvider,
            ItemProcessor<CofisupData, TableauFinancementBean> processor) {
        super(dataProvider, processor);
    }

    @Override
    public LineAggregator<TableauFinancementBean> lineAggregator(String devise, Banque banque) {
        return new CofisupBeanLineAgreggator<TableauFinancementBean>(devise, banque);
    }

}
