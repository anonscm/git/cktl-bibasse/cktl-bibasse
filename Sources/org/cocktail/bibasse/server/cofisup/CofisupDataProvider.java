package org.cocktail.bibasse.server.cofisup;

import java.util.Iterator;

import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

/**
 * Fournisseur de données pour les extractions Cofisup.
 *
 */
public interface CofisupDataProvider {

	/**
	 * @param executor executeur de requête.
	 * @param context contexte utilisée pour l'execution des requêtes.
	 * @return les données brutes à exploiter.
	 */
	Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext context);

}
