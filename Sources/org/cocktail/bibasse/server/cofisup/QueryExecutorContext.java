package org.cocktail.bibasse.server.cofisup;

import java.io.Serializable;
import java.util.Collection;

import org.cocktail.common.cofisup.ETypeBudget;

/**
 * Contexte d'execution d'une requete Cofisup.
 *
 */
public final class QueryExecutorContext implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = -3854628988877007823L;

	private Number exerciceEnCours;
	private Number exercice;
	private Collection<Integer> organIds;
	private ETypeBudget typeBudget;
	private String section;

	public Number getExerciceEnCours() {
		return exerciceEnCours;
	}

	public void setExerciceEnCours(Number exerciceEnCours) {
		this.exerciceEnCours = exerciceEnCours;
	}

	public Number getExercice() {
		return exercice;
	}

	public void setExercice(Number exercice) {
		this.exercice = exercice;
	}

	public Collection<Integer> getOrganIds() {
		return organIds;
	}

	public void setOrganIds(Collection<Integer> organIds) {
		this.organIds = organIds;
	}

	public ETypeBudget getTypeBudget() {
		return typeBudget;
	}

	public void setTypeBudget(ETypeBudget typeBudget) {
		this.typeBudget = typeBudget;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

}
