package org.cocktail.bibasse.server.cofisup.batch.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.cofisup.ETypeNatureBudget;
import org.cocktail.common.metier.Banque;
import org.cocktail.common.metier.Money;
import org.cocktail.common.metier.MoneyFormatter;

/**
 * Collection d'utilitaires destiné au module Cofisup.
 */
public final class CofisupUtils {

	/** Prefix fichier. */
	public static final String FILE_PREFIX = "CofisupBudget";

	/** Symbole formattage decimal Cofisup. */
	private static final DecimalFormatSymbols COFISUP_SYMBOLS = new DecimalFormatSymbols();

	/** Format decimal utilisé par Cofisup. */
	private static final DecimalFormat COFISUP_DECIMAL_FORMAT = new DecimalFormat("0.00");

	/** Separateur decimal par défaut. */
	private static final char DEFAULT_DECIMAL_SEPARATOR = ',';
	
	/** Nombre de decimal a prendre en compte pour Cofisup. */
	private static final int COFISUP_SCALE = 2;
	
	/** Formatteur de monnaie. */
	private static MoneyFormatter MONEY_FORMATTER;

	static {
		COFISUP_SYMBOLS.setDecimalSeparator(DEFAULT_DECIMAL_SEPARATOR);
		COFISUP_DECIMAL_FORMAT.setDecimalFormatSymbols(COFISUP_SYMBOLS);
		MONEY_FORMATTER = new MoneyFormatter(COFISUP_DECIMAL_FORMAT, COFISUP_SCALE, RoundingMode.HALF_UP);
	}

	
	/**
	 * Constructeur privé.
	 */
	private CofisupUtils() {
	}

	/**
	 * Retourne la liste des IDs des organigrammes passés en paramètre.
	 * @param organs les organigrammes dont il faut extraire les identifiants.
	 * @return les identifiants des organigrammes sous forme de liste ; une liste vide si les organigrammes sont null.
	 */
	public static Collection<Integer> extractIds(Collection<EOOrgan> organs) {
		if (organs == null) {
			return Collections.emptySet();
		}

		Set<Integer> orgIds = new HashSet<Integer>();
		for (EOOrgan organ : organs) {
			orgIds.add(organ.orgId());
		}

		return orgIds;
	}

	/**
	 * Creation d'un fichier temporaire.
	 * @param typeNatureBudget type nature budget.
	 * @return fichier temporaire.
	 * @throws IOException en cas d'anomalie lors de la création du fichier temporaire.
	 */
	public static File createTempFile(ETypeNatureBudget typeNatureBudget) throws IOException {
		return File.createTempFile(FILE_PREFIX + typeNatureBudget.toString(), null);
	}
	
	   
    public static Money convertToEuro(Banque banque, String deviseFrom, BigDecimal montant, Integer exercice) {
        return new Money(deviseFrom, montant).convertTo(banque, Banque.EURO_DEVISE, exercice);
    }

    public static String formatMontant(Banque banque, String deviseFrom, BigDecimal montant, String exercice) {
        return formatMontant(banque, deviseFrom, montant, Integer.valueOf(exercice));
    }
    
    public static String formatMontant(Banque banque, String deviseFrom, BigDecimal montant, Integer exercice) {
        return convertToEuro(banque, deviseFrom, montant, exercice)
                .format(CofisupUtils.moneyFormatter());
    }

	/**
	 * @return formatter a utiliser pour les montants Cofisup.
	 */
    public static MoneyFormatter moneyFormatter() {
        return MONEY_FORMATTER;
    }
}
