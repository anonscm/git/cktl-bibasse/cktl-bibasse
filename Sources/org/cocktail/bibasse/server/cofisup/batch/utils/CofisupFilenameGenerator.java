package org.cocktail.bibasse.server.cofisup.batch.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.bibasse.server.utilities.DateUtils;
import org.cocktail.common.cofisup.ETypeBudget;
import org.cocktail.common.cofisup.ETypeFichier;
import org.cocktail.common.cofisup.ETypeNatureBudget;

public class CofisupFilenameGenerator {

	/** Format d'affichage des dates dans Cofisup. */
	private static final DateFormat SDF_DATEHEURE = new SimpleDateFormat("yyyyMMdd-HHmmss");

	/** Separateur infos du nom de fichier. */
	private static final String FILENAME_SEPARATOR = "_";

	private static final String RECETTE_DEPENSE_LIBELLE = "RD";
	private static final String INFORMATIONS_COMPLEMENTAIRES_LIBELLE = "IC";

	/**
	 * Genere le nom du fichier Cofisup.
	 * @param natureBudget nature budget.
	 * @param sequence sequence a utiliser.
	 * @param typeFichier type de fichier.
	 * @param typeBudget type de budget.
	 * @param exercice exercice.
	 * @param codeUai code UAI de l'etablissement.
	 * @return le nom du fichier a utiliser.
	 */
	public static String generateFilename(ETypeNatureBudget natureBudget, int sequence, ETypeFichier typeFichier,
			ETypeBudget typeBudget, Integer exercice, String codeUai) {

		List<String> filenameElements = new ArrayList<String>();
		filenameElements.add(natureBudget.getCodeFichier());
		filenameElements.add(codeUai);
		filenameElements.add(typeBudget.toString());
		filenameElements.add(familleTypeFichier(typeFichier));
		filenameElements.add(natureBudget.getComposante());
		filenameElements.add(String.valueOf(exercice));
		filenameElements.add(String.valueOf(sequence));

		if (typeFichier.isInformationsComplementaires()) {
			filenameElements.add(typeFichier.toString());
		}

		filenameElements.add(SDF_DATEHEURE.format(DateUtils.now()));

		return  StringUtils.join(filenameElements, FILENAME_SEPARATOR);
	}

	protected static String familleTypeFichier(ETypeFichier typeFichier) {
		if (typeFichier.isInformationsComplementaires()) {
			return INFORMATIONS_COMPLEMENTAIRES_LIBELLE;
		}

		return RECETTE_DEPENSE_LIBELLE;
	}

}
