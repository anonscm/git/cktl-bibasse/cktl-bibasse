package org.cocktail.bibasse.server.cofisup.batch.writer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.AbstractItemWriter;
import org.cocktail.bibasse.server.metier.EOOrgan;

public abstract class AbstractCofisupItemWriter<T> extends AbstractItemWriter<T> {

	public static final String ITEM_SEPARATOR = ";";
	public static final String TYPE_ENREGISTREMENT_DEBUT = "P";
	public static final String TYPE_ENREGISTREMENT_FIN = "F";

	private static final DateFormat SDF = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

	/**
	 * @param context contexte d'execution du job.
	 * @return libelle de la composante.
	 */
	public String libelleComposante(JobContext context) {
		String libelleComposante = context.getNatureBudget().getLibelle();
		if (libelleComposante != null) {
			return libelleComposante;
		}

		EOOrgan organ = (EOOrgan) context.getOrganigrammes().toArray()[0];
		return organ.orgLibelle();
	}

	public DateFormat getDateFormatter() {
		return SDF;
	}

}
