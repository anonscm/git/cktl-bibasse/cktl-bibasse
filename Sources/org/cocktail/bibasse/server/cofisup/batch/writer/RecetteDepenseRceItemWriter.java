package org.cocktail.bibasse.server.cofisup.batch.writer;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.exception.ItemWriterException;
import org.cocktail.batch.item.file.FlatFileFooterCallback;
import org.cocktail.batch.item.file.FlatFileHeaderCallback;
import org.cocktail.bibasse.server.cofisup.CofisupDao;
import org.cocktail.bibasse.server.cofisup.batch.RecetteDepenseBean;
import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.common.cofisup.ETypeBudget;
import org.cocktail.common.metier.Banque;
import org.cocktail.common.metier.Money;

public class RecetteDepenseRceItemWriter extends AbstractCofisupItemWriter<RecetteDepenseBean> implements FlatFileHeaderCallback, FlatFileFooterCallback {

	private static final String TYPE_FICHIER = "RD";

	private CofisupDao cofisupDao;
	private ItemWriter<RecetteDepenseBean> delegate;
	private BigDecimal montantRecettes;
	private BigDecimal montantDepenses;

	public RecetteDepenseRceItemWriter() {
		this.cofisupDao = CofisupDao.instance();
		this.montantDepenses = BigDecimal.ZERO;
		this.montantRecettes = BigDecimal.ZERO;
	}

	@Override
	public void open(JobContext context) throws ItemWriterException {
		this.delegate.open(context);
	}

	@Override
	public void close(JobContext context) throws ItemWriterException {
		this.delegate.close(context);
	}

	public void writeHeader(JobContext context, Writer writer) throws IOException {
		writer.write(getHeader(context));
	}

	public void writeFooter(JobContext context, Writer writer) throws IOException {
		writer.write(getFooter(context));
	}

	public void write(JobContext context, List<? extends RecetteDepenseBean> items) throws ItemWriterException {
		// TODO filtrer les enregistrements dont le code lolf recette n'appartient pas a la liste de reference.
		majMontantsCumules(items, context.getDevise(), context.getBanque(), context.getExercice().exeExercice());
		delegate.write(context, items);
	}

	public void setDelegate(ItemWriter<RecetteDepenseBean> delegate) {
		this.delegate = delegate;
	}

	protected void majMontantsCumules(List<? extends RecetteDepenseBean> items, String devise, Banque banque, Integer exercice) {
		if (items == null) {
			return;
		}

		for (RecetteDepenseBean item : items) {
		    BigDecimal montant = arrondiMontantSiConversionDeMontant(item.getMontant(), devise, banque, exercice);
			if (item.isDepense()) {
				montantDepenses = montantDepenses.add(montant);
			} else {
				montantRecettes = montantRecettes.add(montant);
			}
		}
	}
	
	protected BigDecimal arrondiMontantSiConversionDeMontant(BigDecimal montant, String devise, Banque banque, Integer exercice) {
	    BigDecimal montantResultat = montant;
	    BigDecimal taux = banque.recupererTauxChange(devise, Banque.EURO_DEVISE, exercice);
	    if (BigDecimal.ONE.compareTo(taux) != 0) {
	        montantResultat = CofisupUtils.convertToEuro(banque, devise, montant, exercice)
	                               .setScale(CofisupUtils.moneyFormatter()).getMontant();
	    }
	    
	    return montantResultat;
	}

	protected String getHeader(JobContext context) {
		StringBuilder headBuilder = new StringBuilder();

		String codeUai = getCofisupDao().getCodeUAIEtablissement(context.getEc(), context.getExercice());
		String siret = getCofisupDao().getSiretEtablissement(context.getEc(), context.getExercice());
		String niveauBudget = context.getNatureBudget().getNiveau();
		String codeBudget = context.getNatureBudget().getCodeFichier();
		String composante = context.getNatureBudget().getComposante();
		String libelleComposante = libelleComposante(context);
		int sequence = context.getSequence();
		ETypeBudget typeBudget = context.getTypeBudget();
		EOExercice exercice = context.getExercice();
		String date = getDateFormatter().format(new Date());

		headBuilder.append(TYPE_ENREGISTREMENT_DEBUT).append(ITEM_SEPARATOR)
			.append(codeUai).append(ITEM_SEPARATOR)
			.append(siret).append(ITEM_SEPARATOR)
			.append(TYPE_FICHIER).append(ITEM_SEPARATOR)
			.append(niveauBudget).append(ITEM_SEPARATOR)
			.append(codeBudget).append(ITEM_SEPARATOR)
			.append(composante).append(ITEM_SEPARATOR)
			.append(sequence).append(ITEM_SEPARATOR)
			.append(libelleComposante).append(ITEM_SEPARATOR)
			.append(typeBudget).append(ITEM_SEPARATOR)
			.append(exercice.exeExercice()).append(ITEM_SEPARATOR)
			.append(date);

		return headBuilder.toString();
	}

	protected String getFooter(JobContext context) {
		StringBuilder footerBuilder = new StringBuilder();

		String devise = context.getDevise();
		String codeUai = getCofisupDao().getCodeUAIEtablissement(context.getEc(), context.getExercice());
		String siret = getCofisupDao().getSiretEtablissement(context.getEc(), context.getExercice());
		String codeBudget = context.getNatureBudget().getCodeFichier();
		String composante = context.getNatureBudget().getComposante();
		String libelleComposante = libelleComposante(context);
		int sequence = context.getSequence();
		ETypeBudget typeBudget = context.getTypeBudget();
		EOExercice exercice = context.getExercice();

		Money montantRecettesAsMoney = new Money(devise, montantRecettes);
		Money montantDepensesAsMoney = new Money(devise, montantDepenses);
		footerBuilder.append(TYPE_ENREGISTREMENT_FIN).append(ITEM_SEPARATOR)
			.append(codeUai).append(ITEM_SEPARATOR)
			.append(siret).append(ITEM_SEPARATOR)
			.append(TYPE_FICHIER).append(ITEM_SEPARATOR)
			.append(codeBudget).append(ITEM_SEPARATOR)
			.append(composante).append(ITEM_SEPARATOR)
			.append(sequence).append(ITEM_SEPARATOR)
			.append(libelleComposante).append(ITEM_SEPARATOR)
			.append(typeBudget).append(ITEM_SEPARATOR)
			.append(montantRecettesAsMoney.setScale(CofisupUtils.moneyFormatter()).format(CofisupUtils.moneyFormatter())).append(ITEM_SEPARATOR)
			.append(montantDepensesAsMoney.setScale(CofisupUtils.moneyFormatter()).format(CofisupUtils.moneyFormatter())).append(ITEM_SEPARATOR)
			.append(exercice.exeExercice());

		return footerBuilder.toString();
	}

	public CofisupDao getCofisupDao() {
		return cofisupDao;
	}

	public void setCofisupDao(CofisupDao cofisupDao) {
		this.cofisupDao = cofisupDao;
	}

}
