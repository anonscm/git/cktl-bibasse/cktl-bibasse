package org.cocktail.bibasse.server.cofisup.batch.writer;

import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.exception.ItemWriterException;
import org.cocktail.batch.item.file.FlatFileFooterCallback;
import org.cocktail.batch.item.file.FlatFileHeaderCallback;
import org.cocktail.bibasse.server.cofisup.CofisupDao;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.common.cofisup.ETypeBudget;

/**
 * Classe gérant l'ecriture des fichiers "Informations complémentaires" du module Cofisup.
 *
 * @author flagoueyte
 *
 * @param <T> represente le type des elements écrits.
 */
public class InformationsComplementairesItemWriter<T> extends AbstractCofisupItemWriter<T> implements FlatFileHeaderCallback, FlatFileFooterCallback {

	private static final String TYPE_FICHIER = "IC";

	private CofisupDao cofisupDao;
	private ItemWriter<T> delegate;

	/**
	 * Constructeur.
	 */
	public InformationsComplementairesItemWriter() {
		this.cofisupDao = CofisupDao.instance();
	}

	/**
	 * {@inheritDoc}
	 */
	public void writeHeader(JobContext context, Writer writer) throws IOException {
		writer.write(getHeader(context));
	}

	/**
	 * {@inheritDoc}
	 */
	public void writeFooter(JobContext context, Writer writer) throws IOException {
		writer.write(getFooter(context));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open(JobContext context) throws ItemWriterException {
		delegate.open(context);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close(JobContext context) throws ItemWriterException {
		delegate.close(context);
	}

	/**
	 * {@inheritDoc}
	 */
	public void write(JobContext context, List<? extends T> items) throws ItemWriterException {
		delegate.write(context, items);
	}

	protected String getHeader(JobContext context) {
		StringBuilder headBuilder = new StringBuilder();

		String codeUai = getCofisupDao().getCodeUAIEtablissement(context.getEc(), context.getExercice());
		String siret = getCofisupDao().getSiretEtablissement(context.getEc(), context.getExercice());
		String typeIC = context.getTypeFichier().toString();
		String niveauBudget = context.getNatureBudget().getNiveau();
		String codeBudget = context.getNatureBudget().getCodeFichier();
		String composante = context.getNatureBudget().getComposante();
		int sequence = context.getSequence();
		String libelleComposante = libelleComposante(context);
		ETypeBudget typeBudget = context.getTypeBudget();
		EOExercice exercice = context.getExercice();
		String date = getDateFormatter().format(new Date());

		headBuilder.append(TYPE_ENREGISTREMENT_DEBUT).append(ITEM_SEPARATOR)
			.append(codeUai).append(ITEM_SEPARATOR)
			.append(siret).append(ITEM_SEPARATOR)
			.append(TYPE_FICHIER).append(ITEM_SEPARATOR)
			.append(typeIC).append(ITEM_SEPARATOR)
			.append(niveauBudget).append(ITEM_SEPARATOR)
			.append(codeBudget).append(ITEM_SEPARATOR)
			.append(composante).append(ITEM_SEPARATOR)
			.append(sequence).append(ITEM_SEPARATOR)
			.append(libelleComposante).append(ITEM_SEPARATOR)
			.append(typeBudget).append(ITEM_SEPARATOR)
			.append(exercice.exeExercice()).append(ITEM_SEPARATOR)
			.append(date);

		return headBuilder.toString();
	}

	protected String getFooter(JobContext context) {
		StringBuilder footerBuilder = new StringBuilder();

		String codeUai = getCofisupDao().getCodeUAIEtablissement(context.getEc(), context.getExercice());
		String siret = getCofisupDao().getSiretEtablissement(context.getEc(), context.getExercice());
		String typeIC = context.getTypeFichier().toString();
		String codeBudget = context.getNatureBudget().getCodeFichier();
		String composante = context.getNatureBudget().getComposante();
		int sequence = context.getSequence();
		String libelleComposante = libelleComposante(context);
		ETypeBudget typeBudget = context.getTypeBudget();
		EOExercice exercice = context.getExercice();

		footerBuilder.append(TYPE_ENREGISTREMENT_FIN).append(ITEM_SEPARATOR)
			.append(codeUai).append(ITEM_SEPARATOR)
			.append(siret).append(ITEM_SEPARATOR)
			.append(TYPE_FICHIER).append(ITEM_SEPARATOR)
			.append(typeIC).append(ITEM_SEPARATOR)
			.append(codeBudget).append(ITEM_SEPARATOR)
			.append(composante).append(ITEM_SEPARATOR)
			.append(sequence).append(ITEM_SEPARATOR)
			.append(libelleComposante).append(ITEM_SEPARATOR)
			.append(typeBudget).append(ITEM_SEPARATOR)
			.append(exercice.exeExercice());

		return footerBuilder.toString();
	}

	public CofisupDao getCofisupDao() {
		return cofisupDao;
	}

	public void setCofisupDao(CofisupDao cofisupDao) {
		this.cofisupDao = cofisupDao;
	}

	public ItemWriter<T> getDelegate() {
		return delegate;
	}

	public void setDelegate(ItemWriter<T> delegate) {
		this.delegate = delegate;
	}
}
