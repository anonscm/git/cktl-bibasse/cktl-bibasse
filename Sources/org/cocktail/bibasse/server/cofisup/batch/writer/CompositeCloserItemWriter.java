package org.cocktail.bibasse.server.cofisup.batch.writer;

import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.exception.ItemWriterException;

public class CompositeCloserItemWriter<T> implements ItemWriter<T> {

	private ItemWriter<T> delegate;

	/**
	 * Constructeur.
	 * @param delegateWriter delegation du flux de sortie.
	 */
	public CompositeCloserItemWriter(ItemWriter<T> delegateWriter) {
		this.delegate = delegateWriter;
	}

	/**
	 * {@inheritDoc}
	 */
	public void open(JobContext context) throws ItemWriterException {
	}

	/**
	 * {@inheritDoc}
	 */

	public void write(JobContext context, List<? extends T> items) throws ItemWriterException {
		this.delegate.write(context, items);
	}

	/**
	 * {@inheritDoc}
	 */
	public void close(JobContext context) throws ItemWriterException {
		this.delegate.close(context);
	}

}
