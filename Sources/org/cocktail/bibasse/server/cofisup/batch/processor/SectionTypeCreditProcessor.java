package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;

public class SectionTypeCreditProcessor implements ItemProcessor<String, String> {

	public static final String FONCTIONNEMENT_INT = "1";
	public static final String FONCTIONNEMENT_STR = "F";
	public static final int FONCTIONNEMENT_COLSIZE = 2; 
	public static final String INVESTISSEMENT_INT = "2";
	public static final String INVESTISSEMENT_STR = "I";
	public static final int INVESTISSEMENT_COLSIZE = 3;
	public static final String PERSONNEL_INT = "3";
	public static final String PERSONNEL_STR = "P";

	private Map<String, String> typesCredits;

	/**
	 * Default constructor.
	 */
	public SectionTypeCreditProcessor() {
		this.typesCredits = new HashMap<String, String>();
		populateTypesCredits();
	}

	private void populateTypesCredits() {
		typesCredits.put(FONCTIONNEMENT_INT, FONCTIONNEMENT_STR);
		typesCredits.put(INVESTISSEMENT_INT, INVESTISSEMENT_STR);
		typesCredits.put(PERSONNEL_INT, PERSONNEL_STR);
	}

	/**
	 * {@inheritDoc}
	 */
	public String process(String item) throws ItemProcessingException {
		if (!typesCredits.containsKey(item)) {
			return StringUtils.EMPTY;
		}
		return typesCredits.get(item);
	}

}
