package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.math.BigDecimal;

import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.batch.CofisupBean;
import org.cocktail.bibasse.server.cofisup.batch.TableauFinancementBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class TableauFinancementProcessor implements ItemProcessor<CofisupData, TableauFinancementBean> {

	/**
	 * {@inheritDoc}
	 */
	public TableauFinancementBean process(CofisupData item) throws ItemProcessingException {

		// recuperation des donnees
		Number exercice = item.getAsNumber(CofisupData.KEY_EXERCICE);

		BigDecimal produits = item.getAsBigDecimal(CofisupData.KEY_MONTANT_PRODUITS);
		BigDecimal charges = item.getAsBigDecimal(CofisupData.KEY_MONTANT_CHARGES);

		BigDecimal chargeNonDecaissablesEtValeurNetteActifsCedes =
				item.getAsBigDecimal(CofisupData.KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES);

		BigDecimal produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure =
				item.getAsBigDecimal(CofisupData.KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION);

		BigDecimal chargesSection2 = item.getAsBigDecimal(CofisupData.KEY_MONTANT_CHARGES_SECTION_2);
		BigDecimal produitsSection2 = item.getAsBigDecimal(CofisupData.KEY_MONTANT_PRODUITS_SECTION_2);
		BigDecimal sommeCompte775 = item.getAsBigDecimal(CofisupData.KEY_MONTANT_COMPTE_775);

		// calcul donnees demandees
		BigDecimal capaciteAutoFinancement = capaciteAutoFinancement(
				produits, charges, chargeNonDecaissablesEtValeurNetteActifsCedes,
				produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure);

		String sensAutoFinancement = null;
		if (capaciteAutoFinancement.signum() < 0) {
			sensAutoFinancement = CofisupBean.INSUFFISANCE;
		} else {
			sensAutoFinancement = CofisupBean.CAPACITE;
		}

		BigDecimal fondDeRoulement = fondDeRoulement(capaciteAutoFinancement, produitsSection2, chargesSection2,
				sommeCompte775);

		String sensEquilibreFondDeRoulement = null;
		if (fondDeRoulement.signum() < 0) {
			sensEquilibreFondDeRoulement = CofisupBean.PRELEVEMENT;
		} else {
			sensEquilibreFondDeRoulement = CofisupBean.APPORT;
		}

		BigDecimal montantRessources = montantRessources(capaciteAutoFinancement, chargesSection2, fondDeRoulement);

		// creation bean
		TableauFinancementBean tableauFinancement = new TableauFinancementBean();
		tableauFinancement.setTypeEnregistrement(CofisupBean.ENREGISTREMENT_DETAILS);
		tableauFinancement.setMtCapaciteAutoFinancement(capaciteAutoFinancement.abs());
		tableauFinancement.setSensAutoFinancement(sensAutoFinancement);
		tableauFinancement.setMtRessources(montantRessources);
		tableauFinancement.setEquilibreFondDeRoulement(fondDeRoulement.abs());
		tableauFinancement.setSensEquilibreFondDeRoulement(sensEquilibreFondDeRoulement);
		tableauFinancement.setExercice(exercice);

		return tableauFinancement;
	}

	private BigDecimal capaciteAutoFinancement(BigDecimal produits, BigDecimal charges,
			BigDecimal chargeNonDecaissablesEtValeurNetteActifsCedes,
			BigDecimal produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure) {
		BigDecimal resultatExploitation = produits.subtract(charges);
		return resultatExploitation.add(chargeNonDecaissablesEtValeurNetteActifsCedes)
					.subtract(produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure);
	}

	private BigDecimal fondDeRoulement(BigDecimal capaciteAutoFinancement, BigDecimal produitsSection2,
			BigDecimal chargesSection2, BigDecimal sommeCompte775) {

		// si la CAF est < 0 on bascule le montant en IAF et on borne la CAF a 0
		// sinon l'IAF est egale à 0.
		BigDecimal insuffisanceAutoFinancement = capaciteAutoFinancement.negate().max(BigDecimal.ZERO);
		BigDecimal capaciteAutoFinancementMaj = capaciteAutoFinancement.max(BigDecimal.ZERO);

		BigDecimal ressourcesStables = capaciteAutoFinancementMaj.add(produitsSection2).add(sommeCompte775);
		BigDecimal actifsImmobilises = insuffisanceAutoFinancement.add(chargesSection2);

		return ressourcesStables.subtract(actifsImmobilises);
	}

	private BigDecimal montantRessources(BigDecimal capaciteAutoFinancement, BigDecimal chargesSection2,
				BigDecimal fondDeRoulement) {

		BigDecimal insuffisanceAutoFinancement = capaciteAutoFinancement.negate().max(BigDecimal.ZERO);
		BigDecimal augmentationFondDeRoulement = fondDeRoulement.max(BigDecimal.ZERO);

		return insuffisanceAutoFinancement.add(chargesSection2).add(augmentationFondDeRoulement);
	}
}
