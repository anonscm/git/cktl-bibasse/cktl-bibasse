package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.math.BigDecimal;

import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.batch.BudgetPropreIntegreBean;
import org.cocktail.bibasse.server.cofisup.batch.CofisupBean;
import org.cocktail.bibasse.server.cofisup.batch.CompteResultatBean;
import org.cocktail.bibasse.server.cofisup.batch.TableauFinancementBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class BudgetsBPItemProcessor implements ItemProcessor<CofisupData, BudgetPropreIntegreBean> {

	private CompteResultatProcessor crProcessor;
	private TableauFinancementProcessor tfProcessor;

	/**
	 * Constructeur.
	 */
	public BudgetsBPItemProcessor() {
		this.crProcessor = new CompteResultatProcessor();
		this.tfProcessor = new TableauFinancementProcessor();
	}

	/**
	 * {@inheritDoc}
	 */
	public BudgetPropreIntegreBean process(CofisupData item) throws ItemProcessingException {
		// recuperation des donnees brute
		Number exercice = item.getAsNumber(CofisupData.KEY_EXERCICE);
		BigDecimal ressourcesPropres = item.getAsBigDecimal(CofisupData.KEY_MONTANT_RESSOURCES_PROPRES);
		BigDecimal droitsADepenser = item.getAsBigDecimal(CofisupData.KEY_MONTANT_DROITS_DEPENSER);

		// recuperation des donnes pre-calculees
		CompteResultatBean rcBean = getCrProcessor().process(item);
		TableauFinancementBean tfBean = getTfProcessor().process(item);

		// calcul donnees demandees
		BigDecimal ressourcesLieesActivite = rcBean.getMtEquilibreBudgetairePrevisionnel().add(tfBean.getMtRessources());
		BigDecimal contributionEtablissement = contributionEtablissement(ressourcesPropres, droitsADepenser);

		// creation bean
		BudgetPropreIntegreBean budgetPropreIntegreBean = new BudgetPropreIntegreBean();
		budgetPropreIntegreBean.setTypeEnregistrement(CofisupBean.ENREGISTREMENT_DETAILS);
		budgetPropreIntegreBean.setRessourcesLieesActivite(ressourcesLieesActivite);
		budgetPropreIntegreBean.setContributionEtablissement(contributionEtablissement);
		budgetPropreIntegreBean.setExercice(exercice);

		return budgetPropreIntegreBean;
	}

	private BigDecimal contributionEtablissement(BigDecimal ressourcesPropres, BigDecimal droitsADepenser) {
		return ressourcesPropres.subtract(droitsADepenser);
	}

	public CompteResultatProcessor getCrProcessor() {
		return crProcessor;
	}

	public void setCrProcessor(CompteResultatProcessor crProcessor) {
		this.crProcessor = crProcessor;
	}

	public TableauFinancementProcessor getTfProcessor() {
		return tfProcessor;
	}

	public void setTfProcessor(TableauFinancementProcessor tfProcessor) {
		this.tfProcessor = tfProcessor;
	}
}
