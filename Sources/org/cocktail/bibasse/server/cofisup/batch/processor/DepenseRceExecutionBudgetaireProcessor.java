package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.util.Collection;
import java.util.List;

import org.cocktail.bibasse.server.cofisup.CofisupDao;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.ExecutionBudgetaireMontantsDepenses;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.formule.Rubrique;

import com.webobjects.eocontrol.EOEditingContext;

public class DepenseRceExecutionBudgetaireProcessor extends RceExecutionBudgetaireProcessor {

	private static final String SENS_COMPTE = "Dep";

	private CofisupDataProvider dataProvider;

	/**
	 * Constructeur.
	 * @param ec editing context.
	 * @param exerciceEnCours exercice budgetaire en cours.
	 * @param exercice exercice cible de l'extraction.
	 * @param organIds identifiants des lignes budgétaires cibles.
	 * @param modelName nom du modele base de données a utiliser.
	 */
	public DepenseRceExecutionBudgetaireProcessor(EOEditingContext ec, EOExercice exerciceEnCours, EOExercice exercice, Collection<EOOrgan> organIds,
			String modelName) {
		super(ec, exerciceEnCours, exercice, organIds, modelName);
		this.dataProvider = new ExecutionBudgetaireMontantsDepenses();
	}

	@Override
	public List<Rubrique> initRubriques(EOEditingContext ec, EOExercice exercice) {
		return CofisupDao.instance().listerRubriquesDepense(ec, exercice);
	}

	@Override
	public CofisupDataProvider dataProvider() {
		return this.dataProvider;
	}

	@Override
	public String sensCompte() {
		return SENS_COMPTE;
	}
}
