package org.cocktail.bibasse.server.cofisup.batch.processor;

import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.common.formule.Compte;

public class CompteProcessor implements ItemProcessor<CofisupData, Compte>{

	private static final String PREFIX = "c";

	/**
	 * {@inheritDoc}
	 */
	public Compte process(CofisupData item) throws ItemProcessingException {
		return new Compte(
				PREFIX + item.getAsString(CofisupData.KEY_COMPTE),
				item.getAsBigDecimal(CofisupData.KEY_MONTANT));
	}
}
