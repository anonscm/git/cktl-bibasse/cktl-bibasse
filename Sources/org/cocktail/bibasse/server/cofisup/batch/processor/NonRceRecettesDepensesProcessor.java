package org.cocktail.bibasse.server.cofisup.batch.processor;

import org.apache.commons.lang.StringUtils;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.batch.RecetteDepenseBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.metier.EOTypeCredit;

public class NonRceRecettesDepensesProcessor implements ItemProcessor<CofisupData, RecetteDepenseBean> {

	private ItemProcessor<String, String> sectionprocessor;

	public NonRceRecettesDepensesProcessor() {
		this.sectionprocessor = new SectionTypeCreditProcessor();
	}

	/**
	 * {@inheritDoc}
	 */
	public RecetteDepenseBean process(CofisupData item) throws ItemProcessingException {
		RecetteDepenseBean bean = new RecetteDepenseBean();
		String section = getSectionprocessor().process(item.getAsString(CofisupData.KEY_SECTION_CREDIT));
		String compte = formatCompteParSection(item.getAsString(CofisupData.KEY_COMPTE), section);
		
		bean.setTypeEnregistrement(RecetteDepenseBean.ENREGISTREMENT_DETAILS);
		bean.setSensCompte(sensCompte(item.getAsString(CofisupData.KEY_TYPE_CREDIT)));
		bean.setType(section);
		bean.setCode(compte);
		bean.setCodeDestination(item.getAsString(CofisupData.KEY_CODE_DESTINATION));
		bean.setMontant(item.getAsBigDecimal(CofisupData.KEY_MONTANT));
		bean.setExercice(item.getAsNumber(CofisupData.KEY_EXERCICE).toString());

		return bean;
	}

	private String sensCompte(String rawSensCompte) {
		String sensCompte = StringUtils.EMPTY;
		if (EOTypeCredit.TYPE_DEPENSE.equals(rawSensCompte)) {
			sensCompte = RecetteDepenseBean.TYPE_DEPENSE;
		} else if (EOTypeCredit.TYPE_RECETTE.equals(rawSensCompte)) {
			sensCompte = RecetteDepenseBean.TYPE_RECETTE;
		}

		return sensCompte;
	}
	
	private String formatCompteParSection(String compte, String section) {
		String compteFormatte = compte;
		if (SectionTypeCreditProcessor.FONCTIONNEMENT_STR.equals(section)) {
			compteFormatte = formatCompte(compteFormatte, SectionTypeCreditProcessor.FONCTIONNEMENT_COLSIZE);
		} else if (SectionTypeCreditProcessor.INVESTISSEMENT_STR.equals(section)) {
			compteFormatte = formatCompte(compteFormatte, SectionTypeCreditProcessor.INVESTISSEMENT_COLSIZE);
		}
		return compteFormatte;
	}

	private String formatCompte(String compte, int limite) {
		String compteFormatte = compte;
		if (compteFormatte.length() > limite) {
			compteFormatte = compteFormatte.substring(0, limite);
		}
		return compteFormatte;
	}
	
	public ItemProcessor<String, String> getSectionprocessor() {
		return sectionprocessor;
	}

	public void setSectionprocessor(ItemProcessor<String, String> sectionprocessor) {
		this.sectionprocessor = sectionprocessor;
	}
}
