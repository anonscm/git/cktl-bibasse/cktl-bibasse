package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.util.Collection;
import java.util.List;

import org.cocktail.bibasse.server.cofisup.CofisupDao;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.ExecutionBudgetaireMontantsRecettes;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.formule.Rubrique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe réalisant le traitement des établissements RCE pour les fichiers liés à l'exécution budgétaire en RECETTE.
 */
public class RecetteRceExecutionBudgetaireProcessor extends RceExecutionBudgetaireProcessor {

	private static final String SENS_COMPTE = "Rec";

	private CofisupDataProvider dataProvider;

	/**
	 * Constructeur.
	 * @param ec editing context.
	 * @param exerciceEnCours exercice budgétaire en cours.
	 * @param exercice exercice cible de l'extraction.
	 * @param organIds liste des identifiants des lignes budgétaires utilisées.
	 * @param modelName nom du modèle base de données à utiliser.
	 */
	public RecetteRceExecutionBudgetaireProcessor(EOEditingContext ec, EOExercice exerciceEnCours,
			EOExercice exercice, Collection<EOOrgan> organIds, String modelName) {
		super(ec, exerciceEnCours, exercice, organIds, modelName);
		this.dataProvider = new ExecutionBudgetaireMontantsRecettes();
	}

	@Override
	public List<Rubrique> initRubriques(EOEditingContext ec, EOExercice exercice) {
		return CofisupDao.instance().listerRubriquesRecette(ec, exercice);
	}

	@Override
	public CofisupDataProvider dataProvider() {
		return this.dataProvider;
	}

	@Override
	public String sensCompte() {
		return SENS_COMPTE;
	}
}
