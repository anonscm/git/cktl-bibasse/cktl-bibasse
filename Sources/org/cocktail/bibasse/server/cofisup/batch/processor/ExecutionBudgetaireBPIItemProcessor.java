package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.math.BigDecimal;

import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.batch.BudgetPropreIntegreBean;
import org.cocktail.bibasse.server.cofisup.batch.CofisupBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class ExecutionBudgetaireBPIItemProcessor implements ItemProcessor<CofisupData, BudgetPropreIntegreBean> {

	/**
	 * {@inheritDoc}
	 */
	public BudgetPropreIntegreBean process(CofisupData item) throws ItemProcessingException {
		Number exercice = item.getAsNumber(CofisupData.KEY_EXERCICE);
		BigDecimal ressourcesLieesActivite = item.getAsBigDecimal(CofisupData.KEY_MONTANT_RESSOURCES_LIEES_ACTIVITE);
		BigDecimal ressourcesPropres = item.getAsBigDecimal(CofisupData.KEY_MONTANT_RESSOURCES_PROPRES);
		BigDecimal droitsADepenser = item.getAsBigDecimal(CofisupData.KEY_MONTANT_DROITS_DEPENSER);

		// creation bean
		BudgetPropreIntegreBean budgetPropreIntegreBean = new BudgetPropreIntegreBean();
		budgetPropreIntegreBean.setTypeEnregistrement(CofisupBean.ENREGISTREMENT_DETAILS);
		budgetPropreIntegreBean.setRessourcesLieesActivite(ressourcesLieesActivite);
		budgetPropreIntegreBean.setContributionEtablissement(contributionEtablissement(ressourcesPropres, droitsADepenser));
		budgetPropreIntegreBean.setExercice(exercice);

		return budgetPropreIntegreBean;
	}

	//FIXME duplication
	private BigDecimal contributionEtablissement(BigDecimal ressourcesPropres, BigDecimal droitsADepenser) {
		return ressourcesPropres.subtract(droitsADepenser);
	}
}
