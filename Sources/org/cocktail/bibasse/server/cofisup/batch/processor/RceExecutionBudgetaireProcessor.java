package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.WebObjectQueryExecutor;
import org.cocktail.bibasse.server.cofisup.batch.RecetteDepenseBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.formule.Compte;
import org.cocktail.common.formule.ElmtNomenclatureContext;
import org.cocktail.common.formule.Rubrique;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe réalisant le traitement des établissements RCE pour les fichiers liés à l'exécution budgétaire.
 */
public abstract class RceExecutionBudgetaireProcessor implements ItemProcessor<CofisupData, RecetteDepenseBean> {

	private EOEditingContext ec;
	private EOExercice exerciceEnCours;
	private EOExercice exercice;
	private Collection<EOOrgan> organIds;
	private QueryExecutor executor;
	private List<Rubrique> rubriquesEnCache;
	private ElmtNomenclatureContext contextEnCours;
	private String sectionEnCours;
	private ItemProcessor<String, String> sectionProcessor;

	/**
	 * Constructeur.
	 * @param ec editing context.
	 * @param exerciceEnCours exercice budgétaire en cours.
	 * @param exercice exercice cible de l'extraction.
	 * @param organIds liste des orgranigrammes ids.
	 * @param modelName nom du modèle Base de données.
	 */
	public RceExecutionBudgetaireProcessor(EOEditingContext ec, EOExercice exerciceEnCours, EOExercice exercice,
			Collection<EOOrgan> organIds, String modelName) {
		this.ec = ec;
		this.exerciceEnCours = exerciceEnCours;
		this.exercice = exercice;
		this.organIds = organIds;
		this.executor = new WebObjectQueryExecutor(ec, modelName);
		this.rubriquesEnCache = initRubriques(ec, exercice);
		this.sectionProcessor = new SectionTypeCreditProcessor();
		this.contextEnCours = null;
		this.sectionEnCours = null;
	}

	/* Abstract methods. */

	/**
	 * Initialise les rubriques.
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @return les rubriques.
	 */
	public abstract List<Rubrique> initRubriques(EOEditingContext ec, EOExercice exercice);

	/**
	 * @return le fournisseur de données pour ce traitement.
	 */
	public abstract CofisupDataProvider dataProvider();

	/**
	 * @return sens du compte.
	 */
	public abstract String sensCompte();

	/* Concrete Methods. */
	/**
	 * {@inheritDoc}
	 */
	public RecetteDepenseBean process(CofisupData item) throws ItemProcessingException {
		CofisupDataProvider montantsProvider = dataProvider();

		// traitement d'un item (section / codeNomenclature)
		// les Items doivent etre triés par section pour limiter le rechargement des données
		String sectionItem = item.getAsString(CofisupData.KEY_SECTION_CREDIT);
		String codeNomenclature = item.getAsString(CofisupData.KEY_NOMENCLATURE_CODE);

		// detecter changement de section et chargement des données
		if (isNouvelleSection(sectionItem)) {

			contextEnCours = new ElmtNomenclatureContext();
			for (Rubrique rubrique : rubriquesEnCache) {
				contextEnCours.put(rubrique.getCode(), new Rubrique(rubrique.getCode(), rubrique.getExpression()));
			}

			Iterator<CofisupData> montantsDepense = montantsProvider.getData(getExecutor(), buildQueryExecutorCtx());

			CompteProcessor compteProcessor = new CompteProcessor();
			while (montantsDepense.hasNext()) {
				CofisupData montantDepense = montantsDepense.next();
				Compte compte = compteProcessor.process(montantDepense);
				contextEnCours.put(compte.getCode(), compte);
			}
		}

		RecetteDepenseBean bean = new RecetteDepenseBean();
		bean.setTypeEnregistrement(RecetteDepenseBean.ENREGISTREMENT_DETAILS);
		bean.setSensCompte(sensCompte());
		bean.setType(sectionProcessor.process(sectionItem));
		bean.setCode(codeNomenclature);
		bean.setCodeDestination("");
		bean.setMontant(contextEnCours.resolve(codeNomenclature));
		bean.setExercice(exercice.exeExercice().toString());

		return bean;
	}

	private boolean isNouvelleSection(String section) {
		boolean isNew = sectionEnCours == null || !sectionEnCours.equals(section);
		if (isNew) {
			sectionEnCours = section;
		}
		return isNew;
	}

	private QueryExecutorContext buildQueryExecutorCtx() {
		QueryExecutorContext ctx = new QueryExecutorContext();
		ctx.setExerciceEnCours(getExerciceEnCours().exeExercice());
		ctx.setExercice(getExercice().exeExercice());
		ctx.setOrganIds(CofisupUtils.extractIds(getOrganIds()));
		ctx.setSection(getSectionEnCours());

		return ctx;
	}

	public EOEditingContext getEc() {
		return ec;
	}

	public EOExercice getExerciceEnCours() {
		return exerciceEnCours;
	}

	public EOExercice getExercice() {
		return exercice;
	}

	public Collection<EOOrgan> getOrganIds() {
		return organIds;
	}

	public QueryExecutor getExecutor() {
		return executor;
	}

	public void setExecutor(QueryExecutor executor) {
		this.executor = executor;
	}

	public List<Rubrique> getRubriquesEnCache() {
		return rubriquesEnCache;
	}

	public String getSectionEnCours() {
		return sectionEnCours;
	}
}
