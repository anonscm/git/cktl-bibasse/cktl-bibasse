package org.cocktail.bibasse.server.cofisup.batch.processor;

import java.math.BigDecimal;

import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.bibasse.server.cofisup.batch.CofisupBean;
import org.cocktail.bibasse.server.cofisup.batch.CompteResultatBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class CompteResultatProcessor implements ItemProcessor<CofisupData, CompteResultatBean> {

	/**
	 * {@inheritDoc}
	 */
	public CompteResultatBean process(CofisupData item) throws ItemProcessingException {
		Number exercice = item.getAsNumber(CofisupData.KEY_EXERCICE);
		BigDecimal charges = item.getAsBigDecimal(CofisupData.KEY_MONTANT_CHARGES);
		BigDecimal produits = item.getAsBigDecimal(CofisupData.KEY_MONTANT_PRODUITS);

		BigDecimal resultatExploitation = resultatExploitation(produits, charges);
		BigDecimal equilibreBudPrevisionnel = equilibreBudgetaire(produits, charges);
		String sensResultat = null;
		if (resultatExploitation.signum() < 0) {
			sensResultat = CofisupBean.PERTE;
		} else {
			sensResultat = CofisupBean.BENEFICE;
		}

		CompteResultatBean processedCompteResultat = new CompteResultatBean();
		processedCompteResultat.setTypeEnregistrement(CofisupBean.ENREGISTREMENT_DETAILS);
		processedCompteResultat.setMtResultatExploitation(resultatExploitation.abs());
		processedCompteResultat.setSensResultat(sensResultat);
		processedCompteResultat.setMtEquilibreBudgetairePrevisionnel(equilibreBudPrevisionnel);
		processedCompteResultat.setExercice(exercice);

		return processedCompteResultat;
	}

	private BigDecimal resultatExploitation(BigDecimal produits, BigDecimal charges) {
		return produits.subtract(charges);
	}

	private BigDecimal equilibreBudgetaire(BigDecimal produits, BigDecimal charges) {
		BigDecimal resultatExploitation = resultatExploitation(produits, charges);
		return charges.add(resultatExploitation.max(BigDecimal.ZERO));
	}
}
