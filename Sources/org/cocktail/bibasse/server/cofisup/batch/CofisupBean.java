package org.cocktail.bibasse.server.cofisup.batch;

import java.io.Serializable;
import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.common.metier.Banque;
import org.cocktail.common.metier.Money;

/**
 * Classe parente des objets Cofisup à écrire dans les fichiers.
 *
 */
public abstract class CofisupBean implements ICofisupBean, Serializable {

	public static final String ENREGISTREMENT_DETAILS = "D";
	public static final String BENEFICE = "B";
	public static final String PERTE = "P";
	public static final String CAPACITE = "C";
	public static final String INSUFFISANCE = "I";
	public static final String APPORT = "A";
	public static final String PRELEVEMENT = "P";
	public static final String DEFAULT_ITEM_SEPARATOR = ";";

	private static final long serialVersionUID = 1L;

	private String itemSeparator;

	/**
	 * Constructeur par défaut.
	 */
	public CofisupBean() {
		this.itemSeparator = DEFAULT_ITEM_SEPARATOR;
	}

	public String getItemSeparator() {
		return itemSeparator;
	}

	public void setItemSeparator(String itemSeparator) {
		this.itemSeparator = itemSeparator;
	}
	
	/**
	 * Aggregage les champs fournis en appelant leur methode toString.
	 * Le separateur utilisé est celui défini dans par la méthode setItemSeparator ou celui par défaut.
	 * @param fields champs a concatener.
	 * @return une chaine de caratere representant les champs concatenes en utilisant un separateur.
	 */
	public String aggregate(Object[] fields) {
	    StringBuilder builder = new StringBuilder(30);
	    for (int idx = 0; idx < fields.length; idx++) {
	        builder.append(fields[idx]);
	        if (idx < fields.length - 1) {
	            builder.append(getItemSeparator());
	        }
	    }
	    return builder.toString();
	}
}
