package org.cocktail.bibasse.server.cofisup.batch;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.common.metier.Banque;

/**
 * Cofisup Recette / Depense Bean.
 */
public class RecetteDepenseBean extends CofisupBean {

	/** Serial version ID. */
	private static final long serialVersionUID = 1L;

	public static final String TYPE_RECETTE = "Rec";
	public static final String TYPE_DEPENSE = "Dep";

	private String typeEnregistrement;
	private String sensCompte;
	private String type;
	private String code;
	private String codeDestination;
	private BigDecimal montant;
	private String exercice;

	/**
	 * Constructeur.
	 */
	public RecetteDepenseBean() {
	}

	public boolean isDepense() {
		return isSensCompte(TYPE_DEPENSE);
	}

	public boolean isRecette() {
		return isSensCompte(TYPE_RECETTE);
	}

	private boolean isSensCompte(String sensCompte) {
		return sensCompte.equals(getSensCompte());
	}

	public String getTypeEnregistrement() {
		return typeEnregistrement;
	}
	public void setTypeEnregistrement(String typeEnregistrement) {
		this.typeEnregistrement = typeEnregistrement;
	}
	public String getSensCompte() {
		return sensCompte;
	}
	public void setSensCompte(String sensCompte) {
		this.sensCompte = sensCompte;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeDestination() {
		return codeDestination;
	}
	public void setCodeDestination(String codeDestination) {
		this.codeDestination = codeDestination;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}
	public String getExercice() {
		return exercice;
	}
	public void setExercice(String exercice) {
		this.exercice = exercice;
	}

	public String convertToString(String devise, Banque banque) {
	    return aggregate(new Object[] {
                  typeEnregistrement, 
                  sensCompte, 
                  type, 
                  code, 
                  codeDestination, 
                  CofisupUtils.formatMontant(banque, devise, montant, exercice), 
                  exercice
	    });
    }

    @Override
    public String toString() {
        return "RecetteDepenseBean [typeEnregistrement=" + typeEnregistrement + ", sensCompte=" + sensCompte + ", type=" + type + ", code="
                + code + ", codeDestination=" + codeDestination + ", montant=" + montant + ", exercice=" + exercice + "]";
    }
}
