package org.cocktail.bibasse.server.cofisup.batch.data.nonrce;

import static org.cocktail.bibasse.server.metier.EOTypeCredit.TYPE_RECETTE;
import java.util.Iterator;

import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class ExecutionBudgetaireDetailsRecettesGestion extends AbstractCofisupNonRceDataProvider implements CofisupDataProvider {

	private static final String QUERY = new StringBuilder(400)
		.append("select '").append(TYPE_RECETTE).append("', tc.tcd_sect, '', '', titre.exe_ordre, nvl(sum(recPlanco.rpco_ht_saisie), 0) ")
		.append("  from jefy_recette.recette_ctrl_planco recPlanco ")
		.append("  join jefy_recette.recette rec on rec.rec_id = recPlanco.rec_id ")
		.append("  join jefy_recette.facture fac on rec.fac_id = fac.fac_id ")
		.append("  join maracuja.titre titre on titre.tit_id = recPlanco.tit_id ")
		.append("  join jefy_admin.type_credit tc on tc.tcd_ordre = fac.tcd_ordre ")
		.append(" where titre.exe_ordre = :exercice ")
		.append("   and titre.tit_etat = 'VISE' ")
		.append("   and fac.org_id in ( ")
		.append(          SUBQUERY_SELECT_ORGANIGRAMME)
		.append("       ) ")
		.append("   and tc.tcd_type = 'RECETTE' ")
		.append("   and tc.tcd_budget <> 'RESERVE' ")
		.append(" group by tc.tcd_sect, titre.exe_ordre ")
		.append(" order by tc.tcd_sect, titre.exe_ordre ")
		.toString();

	/**
	 * {@inheritDoc}
	 */
	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		return getDataGestion(executor, ctx, QUERY);
	}

}
