package org.cocktail.bibasse.server.cofisup.batch.data.nonrce;

import static org.cocktail.bibasse.server.metier.EOTypeCredit.TYPE_RECETTE;

import java.util.Iterator;

import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class BudgetsDetailsRecettesGestion extends AbstractCofisupNonRceDataProvider implements CofisupDataProvider {

	private static final String QUERY = new StringBuilder(600)
		.append("select '").append(TYPE_RECETTE).append("', tc.tcd_sect, '', '', bvg.exe_ordre, nvl(sum(:typeMt), 0) ")
		.append("  from jefy_budget.budget_vote_gestion bvg ")
		.append("  join jefy_admin.type_credit tc on bvg.tcd_ordre = tc.tcd_ordre ")
		.append(" where bvg.exe_ordre = :exercice ")
		.append("   and tc.tcd_budget <> 'RESERVE' ")
		.append("   and tc.tcd_type = 'RECETTE' ")
		.append("   and bvg.org_id in ( ").append(SUBQUERY_SELECT_ORGANIGRAMME).append(") ")
		.append(" group by tc.tcd_sect, bvg.exe_ordre ")
		.append(" order by tc.tcd_sect ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		return getDataGestion(executor, ctx, QUERY);
	}

}
