package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

import com.webobjects.foundation.NSArray;

public class ExecutionBudgetaireMontantsRecettes extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final String[] KEY_NAMES = new String[] {CofisupData.KEY_COMPTE, CofisupData.KEY_MONTANT};
	private static final NSArray KEYS_QUERY = new NSArray(KEY_NAMES);
	private static final Set<String> KEYSET = new HashSet<String>(Arrays.asList(KEY_NAMES));

	private static final String QUERY = new StringBuilder(600)
		.append("select recPlanco.pco_num as compte, NVL(SUM(recPlanco.rpco_ht_saisie), 0) as montant ")
		.append(FROM_TITRES_VISES)
		.append("   and typeCredit.tcd_type = 'RECETTE' ")
		.append("   and typeCredit.tcd_budget <> 'RESERVE' ")
		.append("   and typeCredit.tcd_sect = :section ")
		.append(" group by recPlanco.pco_num ")
		.append(" order by typeCredit.tcd_sect ")
		.toString();

	/**
	 * {@inheritDoc}
	 */
	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = new HashMap<String, String>();
		variables.put(VAR_EXERCICE, ctx.getExercice().toString());
		variables.put(VAR_ORG_IDS, buildRestrictionOrganigrammes(ctx.getOrganIds()));
		variables.put(VAR_SECTION, ctx.getSection().toString());

		String sqlQuery = getVarResolver().resolve(QUERY, variables);
		NSArray montantsDepense = executor.execute(sqlQuery, KEYS_QUERY);

		return new CofisupDataIterator(KEYSET, montantsDepense);
	}

}
