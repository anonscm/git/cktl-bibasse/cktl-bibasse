package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

import com.webobjects.foundation.NSArray;

public class ExecutionBudgetaireMontantsDepenses extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final String[] KEY_NAMES = new String[] {CofisupData.KEY_COMPTE, CofisupData.KEY_MONTANT};
	private static final NSArray KEYS_QUERY = new NSArray(KEY_NAMES);
	private static final Set<String> KEYSET = new HashSet<String>(Arrays.asList(KEY_NAMES));

	private final String QUERY = new StringBuilder(400)
		.append("SELECT depPlanco.pco_num AS compte, NVL(SUM(depPlanco.dpco_montant_budgetaire), 0) AS montant ")
		.append("  FROM jefy_depense.depense_ctrl_planco depPlanco ")
		.append("  JOIN jefy_depense.depense_budget depense ON depPlanco.dep_id = depense.dep_id ")
		.append("  JOIN jefy_depense.engage_budget engage ON depense.eng_id = engage.eng_id ")
		.append("  JOIN jefy_admin.type_credit typeCredit ON engage.tcd_ordre = typeCredit.tcd_ordre ")
		.append("  JOIN maracuja.mandat mandat ON mandat.man_id = depPlanco.man_id ")
		.append(" WHERE mandat.exe_ordre = :exercice ")
		.append("   AND (mandat.man_etat = 'VISE' OR mandat.man_etat = 'PAYE') ")
		.append("  AND engage.org_id IN ( ")
		.append(         SUBQUERY_SELECT_ORGANIGRAMME)
		.append("      ) ")
		.append("  AND typeCredit.tcd_type = 'DEPENSE' ")
		.append("  AND typeCredit.tcd_budget <> 'RESERVE' ")
		.append("  AND typeCredit.tcd_sect = :section ")
		.append("GROUP BY depPlanco.pco_num ")
		.append("ORDER BY typeCredit.tcd_sect ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {

		Map<String, String> variables = new HashMap<String, String>();
		variables.put(VAR_EXERCICE, ctx.getExercice().toString());
		variables.put(VAR_ORG_IDS, buildRestrictionOrganigrammes(ctx.getOrganIds()));
		variables.put(VAR_SECTION, ctx.getSection().toString());

		String sqlQuery = getVarResolver().resolve(QUERY, variables);
		NSArray montantsDepense = executor.execute(sqlQuery, KEYS_QUERY);

		return new CofisupDataIterator(KEYSET, montantsDepense);
	}
}
