package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

import com.webobjects.foundation.NSArray;

public class ExecutionBugetaireDetailsDepenses extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final String[] KEY_NAMES = new String[]
			{CofisupData.KEY_SECTION_CREDIT, CofisupData.KEY_NOMENCLATURE_CODE};

	private static final NSArray KEYS_QUERY = new NSArray(KEY_NAMES);
	private static final Set<String> KEYSET = new HashSet<String>(Arrays.asList(KEY_NAMES));

	private static final String QUERY = new StringBuilder()
		.append("select tc.tcd_sect, nec.nec_code ")
		.append("  from jefy_admin.type_credit tc, jefy_admin.nomenclature_etat_credit nec ")
		.append(" where tc.exe_ordre = :exercice and tcd_type = 'DEPENSE' ")
		.append(" group by tc.tcd_sect, nec.nec_code ")
		.append(" order by tc.tcd_sect, nec.nec_code ")
		.toString();

	/**
	 * {@inheritDoc}
	 */
	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildQueryParameters(ctx.getExercice());
		String sqlQuery = getVarResolver().resolve(QUERY, variables);
		NSArray results = executor.execute(sqlQuery, KEYS_QUERY);

		return new CofisupDataIterator(KEYSET, results);
	}

	/**
	 * @param exercice exercice.
	 * @return couple clé / valeur a utiliser lors du remplacement des variables SQL.
	 */
	public Map<String, String> buildQueryParameters(Number exercice) {
		Map<String, String> variables = new HashMap<String, String>();
		variables.put(VAR_EXERCICE, exercice.toString());
		return variables;
	}
}
