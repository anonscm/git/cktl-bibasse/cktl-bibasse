package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.EmptyDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.metier.EOBudgetParametres;


public class BudgetsTableauFinancement extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final Set<String> KEYS = new HashSet<String>(
			Arrays.asList(CofisupData.KEY_EXERCICE, CofisupData.KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES,
					CofisupData.KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION,
					CofisupData.KEY_MONTANT_CHARGES_SECTION_2, CofisupData.KEY_MONTANT_PRODUITS_SECTION_2,
					CofisupData.KEY_MONTANT_COMPTE_775));

	private static final String QUERY_CHARGES_NON_DECAISSABLES_ET_VALEUR_ACTIFS_CEDES = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and (bvn.pco_num like '68%' or bvn.pco_num like '675%') ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private static final String QUERY_BUDGETS_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTIONS = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and (bvn.pco_num like '775%' or bvn.pco_num like '776%' or bvn.pco_num like '777%' or bvn.pco_num like '78%') ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private static final String QUERY_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and (bvn.pco_num like '775%' or bvn.pco_num like '776%' or bvn.pco_num like '78%') ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private static final String QUERY_MONTANT_CHARGES_SECTION_2 = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) as montant from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and bvn.tcd_ordre in ( ")
		.append("         select tc.tcd_ordre from jefy_admin.type_credit tc ")
		.append("          where tc.tcd_type = 'DEPENSE' and tc.tcd_sect = 2 ")
		.append("            and tc.tcd_budget <> 'RESERVE' ")
		.append("       ) ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private static final String QUERY_MONTANT_PRODUITS_SECTION_2 = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) as montant from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and bvn.tcd_ordre in ( ")
		.append("         select tc.tcd_ordre from jefy_admin.type_credit tc ")
		.append("          where tc.tcd_type = 'RECETTE' and tc.tcd_sect = 2 ")
		.append("            and tc.tcd_budget <> 'RESERVE' ")
		.append("       ) ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private static final String QUERY_MONTANT_COMPTE_775 = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) as montant from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and bvn.pco_num like '775%' ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private CofisupDataProvider dataProvider;

	public BudgetsTableauFinancement(CofisupDataProvider dataProvider) {
		initDataProvider(dataProvider);
	}

	private void initDataProvider(CofisupDataProvider dataProvider) {
		if (dataProvider != null) {
			this.dataProvider = dataProvider;
		} else {
			this.dataProvider = new EmptyDataProvider();
		}
	}

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildBudgetsNatureQueryParameters(ctx.getExercice(), ctx.getOrganIds(), ctx.getTypeBudget());

		BigDecimal montantChargeNonDecaissablesEtValeurNetteActifsCedes =
				getMontant(executor, QUERY_CHARGES_NON_DECAISSABLES_ET_VALEUR_ACTIFS_CEDES, variables);

		BigDecimal montantProduitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure =
				produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigureMontantBudgets(
						executor, ctx.getExercice(), variables);

		BigDecimal chargesSection2 = getMontant(executor, QUERY_MONTANT_CHARGES_SECTION_2, variables);
		BigDecimal produitsSection2 = getMontant(executor, QUERY_MONTANT_PRODUITS_SECTION_2, variables);
		BigDecimal compte775 = compte775(executor, ctx.getExercice(), variables);

		Map<String, Object> tableauFinancementMap = new HashMap<String, Object>();
		tableauFinancementMap.put(CofisupData.KEY_EXERCICE, ctx.getExercice());
		tableauFinancementMap.put(
				CofisupData.KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES,
				montantChargeNonDecaissablesEtValeurNetteActifsCedes);
		tableauFinancementMap.put(
				CofisupData.KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION,
				montantProduitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure);
		tableauFinancementMap.put(CofisupData.KEY_MONTANT_PRODUITS_SECTION_2, produitsSection2);
		tableauFinancementMap.put(CofisupData.KEY_MONTANT_CHARGES_SECTION_2, chargesSection2);
		tableauFinancementMap.put(CofisupData.KEY_MONTANT_COMPTE_775, compte775);

		CofisupData tableauFinancementData = new CofisupData(KEYS, tableauFinancementMap)
			.addAll(dataProvider.getData(executor, ctx));

		return Arrays.asList(tableauFinancementData).iterator();
	}

	private BigDecimal produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigureMontantBudgets(
			QueryExecutor executor, Number exercice, Map<String, String> variables) {

		String queryBudgetsProduits = QUERY_BUDGETS_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTIONS;
		if (isComptePrisEnCompte(executor, exercice, EOBudgetParametres.param_777_EN_PRODUIT)) {
			queryBudgetsProduits = QUERY_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS;
        }

		return getMontant(executor, queryBudgetsProduits, variables);
	}

	private BigDecimal compte775(QueryExecutor executor, Number exercice, Map<String, String> variables) {
		BigDecimal compte775 = null;

		if (isComptePrisEnCompte(executor, exercice, EOBudgetParametres.param_775_FDR)) {
			compte775 = getMontant(executor, QUERY_MONTANT_COMPTE_775, variables);
		} else {
			compte775 = BigDecimal.ZERO;
		}

		return compte775;
	}
}
