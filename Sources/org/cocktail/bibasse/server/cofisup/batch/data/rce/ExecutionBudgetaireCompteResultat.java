package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class ExecutionBudgetaireCompteResultat extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final Set<String> KEYS = new HashSet<String>(
			Arrays.asList(CofisupData.KEY_MONTANT_PRODUITS, CofisupData.KEY_MONTANT_CHARGES, CofisupData.KEY_EXERCICE));

	private static final String QUERY_MONTANT_PRODUITS_TITRES_VISES = new StringBuilder(600)
		.append("select nvl(sum(recPlanco.rpco_ht_saisie), 0) as montant ")
		.append(FROM_TITRES_VISES)
		.append("   and recPlanco.pco_num like '7%' ")
		.toString();

	private static final String QUERY_MONTANT_CHARGES_MANDATS_VISES = new StringBuilder(600)
		.append("select nvl(sum(depPlanco.dpco_montant_budgetaire), 0) as montant ")
		.append(FROM_MANDATS_VISES)
		.append("   and depPlanco.pco_num like '6%' ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildRceExecutionBudgetaireQueryParameters(ctx.getExercice(), ctx.getOrganIds());

		BigDecimal mtRecettes = getMontant(executor, QUERY_MONTANT_PRODUITS_TITRES_VISES, variables);
		BigDecimal mtDepenses = getMontant(executor, QUERY_MONTANT_CHARGES_MANDATS_VISES, variables);

		Map<String, Object> compteResultatAsMap = new HashMap<String, Object>();
		compteResultatAsMap.put(CofisupData.KEY_MONTANT_PRODUITS, mtRecettes);
		compteResultatAsMap.put(CofisupData.KEY_MONTANT_CHARGES, mtDepenses);
		compteResultatAsMap.put(CofisupData.KEY_EXERCICE, ctx.getExercice());

		return Arrays.asList(new CofisupData(KEYS, compteResultatAsMap)).iterator();
	}
}
