package org.cocktail.bibasse.server.cofisup.batch.data.nonrce;

import java.util.Iterator;

import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class ExecutionBudgetaireDetailsDepensesNature extends AbstractCofisupNonRceDataProvider implements CofisupDataProvider {

	private static final String QUERY = new StringBuilder(400)
		.append("select tc.tcd_type, tc.tcd_sect, mnChpt.pco_chapitre, '', mandat.exe_ordre, nvl(sum(depPlanco.dpco_montant_budgetaire), 0) ")
		.append("  from jefy_depense.depense_ctrl_planco depPlanco ")
		.append("  join jefy_depense.depense_budget depense on depPlanco.dep_id = depense.dep_id ")
		.append("  join jefy_depense.engage_budget engage on depense.eng_id = engage.eng_id ")
		.append("  join jefy_admin.type_credit tc on engage.tcd_ordre = tc.tcd_ordre ")
		.append("  join maracuja.mandat mandat on mandat.man_id = depPlanco.man_id ")
		.append("  join maracuja.plan_comptable_exer planco on (depPlanco.pco_num = planco.pco_num and depPlanco.exe_ordre = planco.exe_ordre) ")
		.append("  join maracuja.v_planco_chapitre mnChpt on (planco.pco_num = mnChpt.pco_num) ")
		.append(" where mandat.exe_ordre = :exercice ")
		.append("   and (mandat.man_etat = 'VISE' OR mandat.man_etat = 'PAYE') ")
		.append("   and engage.org_id IN ( ")
		.append(         SUBQUERY_SELECT_ORGANIGRAMME)
		.append("       ) ")
		.append("   and tc.tcd_type = 'DEPENSE' ")
		.append("   and tc.tcd_budget <> 'RESERVE' ")
		.append(" group by tc.tcd_type, tc.tcd_sect, mnChpt.pco_chapitre, mandat.exe_ordre ")
		.append(" order by tc.tcd_type, tc.tcd_sect, mnChpt.pco_chapitre ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		return getDataNature(executor, ctx, QUERY);
	}

}
