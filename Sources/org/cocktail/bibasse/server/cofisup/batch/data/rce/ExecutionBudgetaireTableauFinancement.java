package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.metier.EOBudgetParametres;

public class ExecutionBudgetaireTableauFinancement extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final Set<String> KEYSET = new HashSet<String>(
			Arrays.asList(CofisupData.KEY_EXERCICE, CofisupData.KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES,
					CofisupData.KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION,
					CofisupData.KEY_MONTANT_CHARGES_SECTION_2, CofisupData.KEY_MONTANT_PRODUITS_SECTION_2,
					CofisupData.KEY_MONTANT_COMPTE_775));

	private static final String QUERY_CHARGES_NON_DECAISSABLES_ET_VALEUR_ACTIFS_CEDES = new StringBuilder(200)
		.append("select nvl(sum(depPlanco.dpco_montant_budgetaire), 0) as montant ")
		.append(FROM_MANDATS_VISES)
		.append("   and (depPlanco.pco_num like '68%' or depPlanco.pco_num like '675%') ")
		.toString();

	private static final StringBuilder BASIC_QUERY_PRODUITS = new StringBuilder(200)
		.append("select nvl(sum(recPlanco.rpco_ht_saisie), 0) as montant ")
		.append(FROM_TITRES_VISES);

	private static final String QUERY_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTIONS = new StringBuilder(200)
		.append(BASIC_QUERY_PRODUITS)
		.append("   and (recPlanco.pco_num like '775%' or recPlanco.pco_num like '776%' or recPlanco.pco_num like '777%' or recPlanco.pco_num like '78%') ")
		.toString();

	private static final String QUERY_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS = new StringBuilder(200)
		.append(BASIC_QUERY_PRODUITS)
		.append("   and (recPlanco.pco_num like '775%' or recPlanco.pco_num like '776%' or recPlanco.pco_num like '78%') ")
		.toString();

	private static final String QUERY_MONTANT_CHARGES_SECTION_2 = new StringBuilder(200)
		.append("select nvl(sum(depPlanco.dpco_montant_budgetaire), 0) as montant ")
		.append(FROM_MANDATS_VISES)
		.append("   and typeCredit.tcd_type = 'DEPENSE' ")
		.append("   and typeCredit.tcd_sect = 2 ")
		.append("   and typeCredit.tcd_budget <> 'RESERVE' ")
		.toString();

	private static final String QUERY_MONTANT_PRODUITS_SECTION_2 = new StringBuilder(200)
		.append(BASIC_QUERY_PRODUITS)
		.append("   and typeCredit.tcd_type = 'RECETTE' ")
		.append("   and typeCredit.tcd_sect = 2 ")
		.append("   and typeCredit.tcd_budget <> 'RESERVE' ")
		.toString();

	private static final String QUERY_MONTANT_COMPTE_775 = new StringBuilder(200)
		.append(BASIC_QUERY_PRODUITS)
		.append("   and recPlanco.pco_num like '775%' ")
		.toString();

	private CofisupDataProvider dataProvider;

	public ExecutionBudgetaireTableauFinancement(CofisupDataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildRceExecutionBudgetaireQueryParameters(ctx.getExercice(), ctx.getOrganIds());

		BigDecimal montantChargeNonDecaissablesEtValeurNetteActifsCedes = getMontant(executor,
				QUERY_CHARGES_NON_DECAISSABLES_ET_VALEUR_ACTIFS_CEDES, variables);

		BigDecimal montantProduitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure =
				produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure(executor, ctx.getExercice(), variables);

		BigDecimal chargesSection2 = getMontant(executor, QUERY_MONTANT_CHARGES_SECTION_2, variables);

		BigDecimal produitsSection2 = getMontant(executor, QUERY_MONTANT_PRODUITS_SECTION_2, variables);

		BigDecimal compte775 = compte775(executor, ctx.getExercice(), variables);

		Map<String, Object> tableauFinancementMap = new HashMap<String, Object>();
		tableauFinancementMap.put(CofisupData.KEY_EXERCICE, ctx.getExercice());
		tableauFinancementMap.put(
				CofisupData.KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES,
				montantChargeNonDecaissablesEtValeurNetteActifsCedes);
		tableauFinancementMap.put(
				CofisupData.KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION,
				montantProduitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure);
		tableauFinancementMap.put(CofisupData.KEY_MONTANT_PRODUITS_SECTION_2, produitsSection2);
		tableauFinancementMap.put(CofisupData.KEY_MONTANT_CHARGES_SECTION_2, chargesSection2);
		tableauFinancementMap.put(CofisupData.KEY_MONTANT_COMPTE_775, compte775);

		CofisupData tableauFinancementData = new CofisupData(KEYSET, tableauFinancementMap);

		// on ajoute les donnees provenant du dataProvider helper. (compteResultat dans ce cas).
		if (dataProvider != null) {
			Iterator<CofisupData> iteDataProvider = dataProvider.getData(executor, ctx);
			while (iteDataProvider.hasNext()) {
				CofisupData currentData = iteDataProvider.next();
				tableauFinancementData = tableauFinancementData.add(currentData);
			}
		}

		return Arrays.asList(tableauFinancementData).iterator();
	}

	private BigDecimal produitsNonEncaissablesEtCessionActifsEtQuotepartSubventionsSiConfigure(
			QueryExecutor executor, Number exercice, Map<String, String> variables) {

		String queryBudgetsProduits = QUERY_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTIONS;
		if (isComptePrisEnCompte(executor, exercice, EOBudgetParametres.param_777_EN_PRODUIT)) {
			queryBudgetsProduits = QUERY_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS;
        }

		return getMontant(executor, queryBudgetsProduits, variables);
	}

	private BigDecimal compte775(QueryExecutor executor, Number exercice, Map<String, String> variables) {
		BigDecimal compte775 = null;

		if (isComptePrisEnCompte(executor, exercice, EOBudgetParametres.param_775_FDR)) {
			compte775 = getMontant(executor, QUERY_MONTANT_COMPTE_775, variables);
		} else {
			compte775 = BigDecimal.ZERO;
		}

		return compte775;
	}

	public CofisupDataProvider getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(CofisupDataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}
}
