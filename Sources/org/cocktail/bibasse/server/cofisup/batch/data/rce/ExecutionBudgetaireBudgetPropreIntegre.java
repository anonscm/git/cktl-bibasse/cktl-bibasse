package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class ExecutionBudgetaireBudgetPropreIntegre extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final Set<String> KEYSET = new HashSet<String>(
			Arrays.asList(CofisupData.KEY_MONTANT_RESSOURCES_LIEES_ACTIVITE, CofisupData.KEY_MONTANT_RESSOURCES_PROPRES,
					CofisupData.KEY_MONTANT_DROITS_DEPENSER, CofisupData.KEY_EXERCICE));

	private static final String QUERY_MONTANT_TITRES_VISES = new StringBuilder(200)
		.append("select nvl(sum(recPlanco.rpco_ht_saisie), 0) as montant ")
		.append(FROM_TITRES_VISES)
		.toString();

	private static final String QUERY_MONTANT_DROITS_A_DEPENSER = new StringBuilder(200)
		.append(QUERY_MONTANT_TITRES_VISES)
		.append("   and (recPlanco.pco_num like '7%' or recPlanco.pco_num like '1%') ")
		.toString();

	private static final String QUERY_MONTANT_RESSOURCES_PROPRES = new StringBuilder(200)
		.append("select nvl(sum(depPlanco.dpco_montant_budgetaire), 0) as montant ")
		.append(FROM_MANDATS_VISES)
		.append("   and (depPlanco.pco_num like '6%' or depPlanco.pco_num like '2%') ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildRceExecutionBudgetaireQueryParameters(ctx.getExercice(), ctx.getOrganIds());

		BigDecimal ressourcesLieesActivite = getMontant(executor, QUERY_MONTANT_TITRES_VISES, variables);
		BigDecimal ressourcesPropres = getMontant(executor, QUERY_MONTANT_RESSOURCES_PROPRES, variables);
		BigDecimal droitsADepenser = getMontant(executor, QUERY_MONTANT_DROITS_A_DEPENSER, variables);

		Map<String, Object> budgetPropreIntegreMap = new HashMap<String, Object>();
		budgetPropreIntegreMap.put(CofisupData.KEY_EXERCICE, ctx.getExercice());
		budgetPropreIntegreMap.put(CofisupData.KEY_MONTANT_RESSOURCES_LIEES_ACTIVITE, ressourcesLieesActivite);
		budgetPropreIntegreMap.put(CofisupData.KEY_MONTANT_RESSOURCES_PROPRES, ressourcesPropres);
		budgetPropreIntegreMap.put(CofisupData.KEY_MONTANT_DROITS_DEPENSER, droitsADepenser);

		return Arrays.asList(new CofisupData(KEYSET, budgetPropreIntegreMap)).iterator();
	}

}
