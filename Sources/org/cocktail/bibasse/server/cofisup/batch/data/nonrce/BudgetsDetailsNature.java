package org.cocktail.bibasse.server.cofisup.batch.data.nonrce;

import java.util.Iterator;

import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class BudgetsDetailsNature extends AbstractCofisupNonRceDataProvider implements CofisupDataProvider {

	private static final String QUERY = new StringBuilder(600)
		.append("select tc.tcd_type, tc.tcd_sect, mnChpt.pco_chapitre, '', bvn.exe_ordre, nvl(sum(:typeMt), 0) ")
		.append("  from jefy_budget.budget_vote_nature bvn ")
		.append("  join jefy_budget.budget_masque_nature mn on (mn.exe_ordre = bvn.exe_ordre and mn.pco_num = bvn.pco_num and mn.tcd_ordre = bvn.tcd_ordre) ")
		.append("  join maracuja.plan_comptable_exer planco on (mn.pco_num_vote = planco.pco_num and mn.exe_ordre = planco.exe_ordre) ")
		.append("  join maracuja.v_planco_chapitre mnChpt on planco.pco_num = mnChpt.pco_num ")
		.append("  join jefy_admin.type_credit tc on bvn.tcd_ordre = tc.tcd_ordre ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and bvn.org_id in ( ").append(SUBQUERY_SELECT_ORGANIGRAMME).append(") ")
		.append("   and (tc.tcd_type = 'DEPENSE' or tc.tcd_type = 'RECETTE') ")
		.append("   and tc.tcd_budget <> 'RESERVE' ")
		.append(" group by tc.tcd_type, tc.tcd_sect, mnChpt.pco_chapitre, bvn.exe_ordre ")
		.append(" order by tc.tcd_type, tc.tcd_sect, mnChpt.pco_chapitre ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		return getDataNature(executor, ctx, QUERY);
	}

}
