package org.cocktail.bibasse.server.cofisup.batch.data.nonrce;

import static org.cocktail.bibasse.server.metier.EOTypeCredit.TYPE_DEPENSE;

import java.util.Iterator;

import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class BudgetsDetailsDepensesGestion extends AbstractCofisupNonRceDataProvider implements CofisupDataProvider {

	private static final String QUERY = new StringBuilder(400)
		.append("select '").append(TYPE_DEPENSE).append("', tc.tcd_sect, '', lolfDep.tyac_root_code, bvg.exe_ordre, nvl(sum(:typeMt), 0) ")
		.append("  from jefy_budget.budget_vote_gestion bvg ")
		.append("  join jefy_budget.v_lolf_dep_hierarchique_action lolfDep on (bvg.tyac_id = lolfDep.tyac_id and bvg.exe_ordre = lolfDep.exe_ordre) ")
		.append("  join jefy_admin.nomenclature_lolf_dest_ref lolfDepRef on (lolfDep.tyac_root_code = lolfDepRef.nldr_code and lolfDep.exe_ordre = lolfDepRef.exe_ordre) ")
		.append("  join jefy_admin.type_credit tc on bvg.tcd_ordre = tc.tcd_ordre ")
		.append(" where bvg.exe_ordre = :exercice ")
		.append("   and bvg.org_id in ( ").append(SUBQUERY_SELECT_ORGANIGRAMME).append(") ")
		.append("   and lolfDepRef.nldr_type = 'NON_RCE' ")
		.append(" group by tc.tcd_sect, lolfDep.tyac_root_code, bvg.exe_ordre ")
		.append(" order by tc.tcd_sect, lolfDep.tyac_root_code ")
		.toString();

	/**
	 * {@inheritDoc}
	 */
	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		return getDataGestion(executor, ctx, QUERY);
	}
}
