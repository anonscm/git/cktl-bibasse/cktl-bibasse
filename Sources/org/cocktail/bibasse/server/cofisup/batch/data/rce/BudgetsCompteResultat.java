package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class BudgetsCompteResultat extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final Set<String> KEYSET = new HashSet<String>(
			Arrays.asList(CofisupData.KEY_MONTANT_PRODUITS, CofisupData.KEY_MONTANT_CHARGES, CofisupData.KEY_EXERCICE));

	private static final String QUERY_NATURE_MONTANT_DEPENSES_PREVISIONNEL_VOTE = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and bvn.pco_num like '6%' ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private static final String QUERY_NATURE_MONTANT_RECETTES_PREVISIONNEL_VOTE = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and bvn.pco_num like '7%' ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildBudgetsNatureQueryParameters(ctx.getExercice(), ctx.getOrganIds(), ctx.getTypeBudget());

		BigDecimal mtRecettes = getMontant(executor, QUERY_NATURE_MONTANT_RECETTES_PREVISIONNEL_VOTE, variables);
		BigDecimal mtDepenses = getMontant(executor, QUERY_NATURE_MONTANT_DEPENSES_PREVISIONNEL_VOTE, variables);

		Map<String, Object> compteResultatAsMap = new HashMap<String, Object>();
		compteResultatAsMap.put(CofisupData.KEY_MONTANT_PRODUITS, mtRecettes);
		compteResultatAsMap.put(CofisupData.KEY_MONTANT_CHARGES, mtDepenses);
		compteResultatAsMap.put(CofisupData.KEY_EXERCICE, ctx.getExercice());

		return Arrays.asList(new CofisupData(KEYSET, compteResultatAsMap)).iterator();
	}
}
