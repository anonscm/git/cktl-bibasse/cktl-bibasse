package org.cocktail.bibasse.server.cofisup.batch.data.nonrce;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

import com.webobjects.foundation.NSArray;

public class AbstractCofisupNonRceDataProvider extends AbstractCofisupDataProvider {

	private static final String[] KEY_NAMES = new String[]
			{CofisupData.KEY_TYPE_CREDIT, CofisupData.KEY_SECTION_CREDIT, CofisupData.KEY_COMPTE,
			 CofisupData.KEY_CODE_DESTINATION, CofisupData.KEY_EXERCICE, CofisupData.KEY_MONTANT};

	private static final NSArray<String> KEYS_QUERY = new NSArray<String>(KEY_NAMES);
	private static final Set<String> KEYSET = new HashSet<String>(Arrays.asList(KEY_NAMES));

	public Iterator<CofisupData> getDataGestion(QueryExecutor executor, QueryExecutorContext ctx, String parameterizedSuery) {
		return getData(executor, parameterizedSuery,
				buildBudgetsGestionQueryParameters(ctx.getExercice(), ctx.getOrganIds(), ctx.getTypeBudget()));
	}

	public Iterator<CofisupData> getDataNature(QueryExecutor executor, QueryExecutorContext ctx, String parameterizedSuery) {
		return getData(executor, parameterizedSuery,
				buildBudgetsNatureQueryParameters(ctx.getExercice(), ctx.getOrganIds(), ctx.getTypeBudget()));
	}

	private Iterator<CofisupData> getData(QueryExecutor executor, String query, Map<String, String> variables) {
		String sqlResolvedQuery = getVarResolver().resolve(query, variables);
		NSArray results = executor.execute(sqlResolvedQuery, KEYS_QUERY);
		return new CofisupDataIterator(KEYSET, results);
	}

}
