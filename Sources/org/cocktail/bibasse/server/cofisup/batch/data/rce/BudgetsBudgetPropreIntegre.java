package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class BudgetsBudgetPropreIntegre extends AbstractCofisupDataProvider implements CofisupDataProvider {

	public static final Set<String> KEYSET = new HashSet<String>(
			Arrays.asList(CofisupData.KEY_MONTANT_RESSOURCES_PROPRES, CofisupData.KEY_MONTANT_DROITS_DEPENSER, CofisupData.KEY_EXERCICE));

	private static final String QUERY_MONTANT_RESSOURCES_PROPRES = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) as montant from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and (bvn.pco_num like '7%' or bvn.pco_num like '1%') ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	private BudgetsTableauFinancement budgetsTableauFinancement;

	public BudgetsBudgetPropreIntegre() {
		this.budgetsTableauFinancement = new BudgetsTableauFinancement(new BudgetsCompteResultat());
	}

	private static final String QUERY_MONTANT_DROITS_A_DEPENSER = new StringBuilder(200)
		.append("select nvl(sum(:typeMt), 0) as montant from jefy_budget.budget_vote_nature bvn ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and (bvn.pco_num like '6%' or bvn.pco_num like '2%') ")
		.append("   and bvn.org_id in (").append(SUBQUERY_SELECT_ORGANIGRAMME).append(")")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildBudgetsNatureQueryParameters(ctx.getExercice(), ctx.getOrganIds(), ctx.getTypeBudget());

		BigDecimal ressourcesPropres = getMontant(executor, QUERY_MONTANT_RESSOURCES_PROPRES, variables);
		BigDecimal droitsADepenser = getMontant(executor, QUERY_MONTANT_DROITS_A_DEPENSER, variables);

		Map<String, Object> budgetPropreIntegreMap = new HashMap<String, Object>();
		budgetPropreIntegreMap.put(CofisupData.KEY_EXERCICE, ctx.getExercice());
		budgetPropreIntegreMap.put(CofisupData.KEY_MONTANT_RESSOURCES_PROPRES, ressourcesPropres);
		budgetPropreIntegreMap.put(CofisupData.KEY_MONTANT_DROITS_DEPENSER, droitsADepenser);

		CofisupData bpiData = new CofisupData(KEYSET, budgetPropreIntegreMap)
			.addAll(budgetsTableauFinancement.getData(executor, ctx));

		return Arrays.asList(bpiData).iterator();
	}
}
