package org.cocktail.bibasse.server.cofisup.batch.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

public class CofisupData implements Serializable {

	public static final String KEY_TYPE_CREDIT = "TYPE_CREDIT";
	public static final String KEY_SECTION_CREDIT = "SECTION_CREDIT";
	public static final String KEY_CODE_DESTINATION = "CODE_DESTINATION";
	public static final String KEY_EXERCICE = "EXERCICE";
	public static final String KEY_COMPTE = "COMPTE";
	public static final String KEY_MONTANT = "MONTANT";
	public static final String KEY_MONTANT_RESSOURCES_LIEES_ACTIVITE = "KEY_MONTANT_RESSOURCES_LIEES_ACTIVITE";
	public static final String KEY_MONTANT_RESSOURCES_PROPRES = "KEY_MONTANT_RESSOURCES_PROPRES";
	public static final String KEY_MONTANT_DROITS_DEPENSER = "KEY_MONTANT_DROITS_DEPENSER";
	public static final String KEY_MONTANT_CHARGES = "MT_CHARGES";
	public static final String KEY_MONTANT_PRODUITS = "MT_PRODUITS";
	public static final String KEY_MONTANT_CHARGES_SECTION_2 = "MT_CHARGES_SECTION_2";
	public static final String KEY_MONTANT_PRODUITS_SECTION_2 = "MT_PRODUITS_SECTION_2";
	public static final String KEY_MONTANT_COMPTE_775 = "MT_COMPTE_775";
	public static final String KEY_NOMENCLATURE_CODE = "CODE";

	public static final String KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES =
			"MT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES";

	public static final String KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION =
			"MT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION";

	/** Serial version Id. */
	private static final long serialVersionUID = -6671677515710653413L;

	private Set<String> keys;
	private Map<String, Object> data;

	/**
	 * Constructeur par défaut.
	 */
	public CofisupData() {
		this.keys = new HashSet<String>();
		this.data = new HashMap<String, Object>();
	}

	/**
	 * Constructeur avec initialisation.
	 * @param keys liste les cles theoriquement disponibles.
	 * @param data donnees disponibles (une cle theorique peut ne pas exister).
	 */
	public CofisupData(Set<String> keys, Map<String, Object> data) {
		this.keys = keys;
		this.data = data;
	}

	private Object get(String key) {
		Object result = null;
		if (data != null) {
			result = data.get(key);
		}

		return result;
	}

	public String getAsString(String key) {
		String resultAsStr = StringUtils.EMPTY;
		Object result = get(key);

		if (result instanceof String) {
			resultAsStr = (String) result;
		}
		return resultAsStr;
	}

	public BigDecimal getAsBigDecimal(String key) {
		BigDecimal resultAsBig = null;

		Object result = get(key);
		if (result instanceof Number) {
			resultAsBig = BigDecimal.valueOf(((Number) result).doubleValue());
		}
		return resultAsBig;
	}

	public Number getAsNumber(String key) {
		Number resultAsNb = null;

		Object result = get(key);
		if (result instanceof Number) {
			resultAsNb = (Number) result;
		}
		return resultAsNb;
	}

	public CofisupData addAll(Iterator<CofisupData> dataIterator) {
		if (dataIterator == null || !dataIterator.hasNext()) {
			return this;
		}

		CofisupData result = null;
		CofisupData currentData = null;
		while (dataIterator.hasNext()) {
			currentData = dataIterator.next();
			result = add(currentData);
		}

		return result;
	}

	public CofisupData add(CofisupData cofisupDataToAdd) {
		Set<String> newKeys = new HashSet<String>(this.keys);
		newKeys.addAll(cofisupDataToAdd.getKeys());

		Map<String, Object> newData = new HashMap<String, Object>(this.data);
		newData.putAll(cofisupDataToAdd.getData());

		return new CofisupData(newKeys, newData);
	}

	public void add(String key, Object value) {
		this.keys.add(key);
		this.data.put(key, value);
	}

	public Set<String> getKeys() {
		return keys;
	}

	public Map<String, Object> getData() {
		return data;
	}
}
