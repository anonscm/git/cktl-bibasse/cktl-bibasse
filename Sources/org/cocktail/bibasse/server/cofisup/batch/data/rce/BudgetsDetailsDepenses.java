package org.cocktail.bibasse.server.cofisup.batch.data.rce;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cocktail.bibasse.server.cofisup.AbstractCofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutor;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

import com.webobjects.foundation.NSArray;

public class BudgetsDetailsDepenses extends AbstractCofisupDataProvider implements CofisupDataProvider {

	private static final String[] KEY_NAMES = new String[]
			{ 	CofisupData.KEY_SECTION_CREDIT, CofisupData.KEY_COMPTE,
				CofisupData.KEY_EXERCICE, CofisupData.KEY_MONTANT };

	private static final NSArray KEYS_QUERY = new NSArray(KEY_NAMES);
	private static final Set<String> KEYSET = new HashSet<String>(Arrays.asList(KEY_NAMES));

	private static final String QUERY = new StringBuilder(600)
		.append("select tc.tcd_sect, mn.pco_num_vote, bvn.exe_ordre, nvl(sum(:typeMt), 0) ")
		.append("  from jefy_budget.budget_vote_nature bvn ")
		.append("  join jefy_budget.budget_masque_nature mn on (mn.exe_ordre = bvn.exe_ordre and mn.pco_num = bvn.pco_num and mn.tcd_ordre = bvn.tcd_ordre) ")
		.append("  join maracuja.plan_comptable_exer planco on (mn.pco_num_vote = planco.pco_num and mn.exe_ordre = planco.exe_ordre) ")
		.append("  join jefy_admin.type_credit tc on bvn.tcd_ordre = tc.tcd_ordre ")
		.append(" where bvn.exe_ordre = :exercice ")
		.append("   and tc.tcd_budget <> 'RESERVE' ")
		.append("   and tc.tcd_type = 'DEPENSE' ")
		.append("   and bvn.org_id in ( ").append(SUBQUERY_SELECT_ORGANIGRAMME).append(") ")
		.append(" group by tc.tcd_sect, mn.pco_num_vote, bvn.exe_ordre ")
		.append(" order by tc.tcd_sect ")
		.toString();

	public Iterator<CofisupData> getData(QueryExecutor executor, QueryExecutorContext ctx) {
		Map<String, String> variables = buildBudgetsNatureQueryParameters(ctx.getExercice(), ctx.getOrganIds(), ctx.getTypeBudget());
		String sqlQuery = getVarResolver().resolve(QUERY, variables);
		NSArray results = executor.execute(sqlQuery, KEYS_QUERY);

		return new CofisupDataIterator(KEYSET, results);
	}
}
