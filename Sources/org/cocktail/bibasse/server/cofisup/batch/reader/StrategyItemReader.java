package org.cocktail.bibasse.server.cofisup.batch.reader;

import java.util.Iterator;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.AbstractItemReader;
import org.cocktail.bibasse.server.cofisup.CofisupDataProvider;
import org.cocktail.bibasse.server.cofisup.QueryExecutorContext;
import org.cocktail.bibasse.server.cofisup.WebObjectQueryExecutor;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;

public class StrategyItemReader extends AbstractItemReader<CofisupData> {

	private CofisupDataProvider dataProvider;

	/**
	 * Constructeur.
	 * @param dataProvider fournisseur de données.
	 */
	public StrategyItemReader(CofisupDataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	@Override
	public Iterator<CofisupData> itemsIterator(JobContext jobContext) {
		return dataProvider.getData(
				new WebObjectQueryExecutor(jobContext.getEc(), jobContext.getModelName()),
				buildQueryExecutorContext(jobContext));
	}

	private QueryExecutorContext buildQueryExecutorContext(JobContext jobContext) {
		QueryExecutorContext context = new QueryExecutorContext();
		context.setExerciceEnCours(jobContext.getExerciceEnCours().exeExercice());
		context.setExercice(jobContext.getExercice().exeExercice());
		context.setOrganIds(CofisupUtils.extractIds(jobContext.getOrganigrammes()));
		context.setTypeBudget(jobContext.getTypeBudget());
		return context;
	}

	/* Setter & Getter */
	public CofisupDataProvider getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(CofisupDataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

}
