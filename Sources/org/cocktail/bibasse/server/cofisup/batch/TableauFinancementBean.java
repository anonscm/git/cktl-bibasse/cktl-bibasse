package org.cocktail.bibasse.server.cofisup.batch;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.common.metier.Banque;

/**
 * Cofisup Tableau de financement Bean.
 */
public class TableauFinancementBean extends CofisupBean {

	private static final long serialVersionUID = 1L;

	private String typeEnregistrement;
	private BigDecimal mtCapaciteAutoFinancement;
	private String sensAutoFinancement;
	private BigDecimal mtRessources;
	private BigDecimal equilibreFondDeRoulement;
	private String sensEquilibreFondDeRoulement;
	private Number exercice;

	/**
	 * Constructeur.
	 */
	public TableauFinancementBean() {
	}
	
	public String getSensAutoFinancement() {
		return sensAutoFinancement;
	}

	public void setSensAutoFinancement(String sensAutoFinancement) {
		this.sensAutoFinancement = sensAutoFinancement;
	}

	public BigDecimal getMtRessources() {
		return mtRessources;
	}

	public void setMtRessources(BigDecimal mtRessources) {
		this.mtRessources = mtRessources;
	}

	public BigDecimal getEquilibreFondDeRoulement() {
		return equilibreFondDeRoulement;
	}

	public void setEquilibreFondDeRoulement(BigDecimal equilibreFondDeRoulement) {
		this.equilibreFondDeRoulement = equilibreFondDeRoulement;
	}

	public String getSensEquilibreFondDeRoulement() {
		return sensEquilibreFondDeRoulement;
	}

	public void setSensEquilibreFondDeRoulement(String sensEquilibreFondDeRoulement) {
		this.sensEquilibreFondDeRoulement = sensEquilibreFondDeRoulement;
	}

	public Number getExercice() {
		return exercice;
	}

	public void setExercice(Number exercice) {
		this.exercice = exercice;
	}

	public String getTypeEnregistrement() {
		return typeEnregistrement;
	}

	public void setTypeEnregistrement(String typeEnregistrement) {
		this.typeEnregistrement = typeEnregistrement;
	}

	public BigDecimal getMtCapaciteAutoFinancement() {
		return mtCapaciteAutoFinancement;
	}

	public void setMtCapaciteAutoFinancement(BigDecimal mtCapaciteAutoFinancement) {
		this.mtCapaciteAutoFinancement = mtCapaciteAutoFinancement;
	}
	
	public String convertToString(String devise, Banque banque) {
	    return aggregate(new Object[] {
                   typeEnregistrement,
                   CofisupUtils.formatMontant(banque, devise, mtCapaciteAutoFinancement, exercice.intValue()),
                   sensAutoFinancement,
                   CofisupUtils.formatMontant(banque, devise, mtRessources, exercice.intValue()),
                   CofisupUtils.formatMontant(banque, devise, equilibreFondDeRoulement, exercice.intValue()),
                   sensEquilibreFondDeRoulement,
                   exercice
	    });
    }

    @Override
    public String toString() {
        return "TableauFinancementBean [typeEnregistrement=" + typeEnregistrement + ", mtCapaciteAutoFinancement="
                + mtCapaciteAutoFinancement + ", sensAutoFinancement=" + sensAutoFinancement + ", mtRessources=" + mtRessources
                + ", equilibreFondDeRoulement=" + equilibreFondDeRoulement + ", sensEquilibreFondDeRoulement="
                + sensEquilibreFondDeRoulement + ", exercice=" + exercice + "]";
    }

}
