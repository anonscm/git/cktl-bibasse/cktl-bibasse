package org.cocktail.bibasse.server.cofisup.batch.step;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.api.ItemReader;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.api.step.SimpleStep;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.batch.exception.ItemReaderException;
import org.cocktail.batch.exception.ItemWriterException;
import org.cocktail.batch.exception.StepException;
import org.cocktail.bibasse.server.cofisup.CofisupDao;
import org.cocktail.bibasse.server.cofisup.batch.RecetteDepenseBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.cofisup.batch.processor.CompteProcessor;
import org.cocktail.bibasse.server.cofisup.batch.processor.SectionTypeCreditProcessor;
import org.cocktail.bibasse.server.finder.FinderTypeCredit;
import org.cocktail.bibasse.server.metier.EOTypeCredit;
import org.cocktail.common.formule.Compte;
import org.cocktail.common.formule.ElmtNomenclatureContext;
import org.cocktail.common.formule.Rubrique;

public class RceDetailsRecettesStep extends SimpleStep<CofisupData, RecetteDepenseBean> {

	private List<String> sectionsTypesCreditRecette;
	private List<Rubrique> rubriques;
	private Map<String, ElmtNomenclatureContext> elContextParTypeCredit;
	private ItemProcessor<String, String> sectionProcessor;

	/**
	 * Constructeur.
	 * @param jobContext contexte d'execution du job.
	 * @param reader flux de lecture des données.
	 * @param writer flux de sortie des données Cofisup.
	 * @param processor transforme les données entrantes en données sortantes.
	 */
	public RceDetailsRecettesStep(JobContext jobContext,
			ItemReader<CofisupData> reader,
			ItemWriter<RecetteDepenseBean> writer,
			ItemProcessor<CofisupData, RecetteDepenseBean> processor) {
		super(jobContext, reader, writer, processor);
		this.sectionProcessor = new SectionTypeCreditProcessor();
	}

	@Override
	public void beforeExecute(ItemReader<CofisupData> reader, ItemWriter<RecetteDepenseBean> writer) throws StepException {
		super.beforeExecute(reader, writer);
		doInit();
	}

	/* FLA :
	 * Si on essaie de sortir cette logique dans un Processor, cela donne :
	 * - reader : lecture des sections credit
	 * - processor :
	 *  -- avoir une phase pour savoir si on a commencé l'accumulation des données ou non
	 * 	-- pour chaque section, recuperer tous les items
	 *  -- conversion de chaque item en compte (gestion contexte + ...)
	 *  -- deleguer conversion ()
	 * - writer : ecriture bean
	 */
	@Override
	public void doExecute(ItemReader<CofisupData> reader, ItemWriter<RecetteDepenseBean> writer) throws StepException {
		try {

			List<RecetteDepenseBean> items = new ArrayList<RecetteDepenseBean>();
			JobContext jobContext = getJobContext();

			// regrouper par section
			ItemProcessor<CofisupData, Compte> compteProcessor = new CompteProcessor();
			CofisupData item = reader.read(jobContext);
			while (item != null) {
				Compte compte = compteProcessor.process(item);
				String typeCredit = item.getAsString(CofisupData.KEY_SECTION_CREDIT);
				elContextParTypeCredit.get(typeCredit).put(compte.getCode(), compte);

				item = reader.read(jobContext);
			}

			// traitement
			for (String section : sectionsTypesCreditRecette) {
				ElmtNomenclatureContext elContext = elContextParTypeCredit.get(section);
				for (Rubrique rubrique : rubriques) {
					BigDecimal montant = elContext.resolve(rubrique.getCode());

					RecetteDepenseBean bean = new RecetteDepenseBean();
					bean.setTypeEnregistrement(RecetteDepenseBean.ENREGISTREMENT_DETAILS);
					bean.setSensCompte(RecetteDepenseBean.TYPE_RECETTE);
					bean.setType(sectionProcessor.process(section));
					bean.setCode(rubrique.getCode());
					bean.setCodeDestination("");
					bean.setMontant(montant);
					bean.setExercice(jobContext.getExercice().exeExercice().toString());

					items.add(bean);
				}
			}

			writer.write(jobContext, items);

		} catch (ItemReaderException ire) {
			throw new StepException(ire);
		} catch (ItemProcessingException ipe) {
			throw new StepException(ipe);
		} catch (ItemWriterException iwe) {
			throw new StepException(iwe);
		}
	}

	private void doInit() {
		JobContext jobContext = getJobContext();
		this.sectionsTypesCreditRecette = FinderTypeCredit.instance().findSections(
				jobContext.getEc(), jobContext.getExercice(), null, EOTypeCredit.TYPE_RECETTE);
		this.rubriques = CofisupDao.instance().listerRubriquesRecette(jobContext.getEc(), jobContext.getExercice());
		this.elContextParTypeCredit = new HashMap<String, ElmtNomenclatureContext>();

		for (String section : sectionsTypesCreditRecette) {
			ElmtNomenclatureContext context = new ElmtNomenclatureContext();
			for (Rubrique rubrique : rubriques) {
				context.put(rubrique.getCode(), new Rubrique(rubrique.getCode(), rubrique.getExpression()));
			}
			elContextParTypeCredit.put(section, context);
		}
	}
}
