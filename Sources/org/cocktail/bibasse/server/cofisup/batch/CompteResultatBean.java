package org.cocktail.bibasse.server.cofisup.batch;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.common.metier.Banque;

/**
 * Cofisup Compte de Résultat Bean.
 */
public class CompteResultatBean extends CofisupBean {

	private static final long serialVersionUID = 1L;

	private String typeEnregistrement;
	private BigDecimal mtResultatExploitation;
	private String sensResultat;
	private BigDecimal mtEquilibreBudgetairePrevisionnel;
	private Number exercice;

	/**
	 * Constructeur.
	 */
	public CompteResultatBean() {
	}

	public String getTypeEnregistrement() {
		return typeEnregistrement;
	}

	public void setTypeEnregistrement(String typeEnregistrement) {
		this.typeEnregistrement = typeEnregistrement;
	}

	public BigDecimal getMtResultatExploitation() {
		return mtResultatExploitation;
	}

	public void setMtResultatExploitation(BigDecimal mtResultatExploitation) {
		this.mtResultatExploitation = mtResultatExploitation;
	}

	public String getSensResultat() {
		return sensResultat;
	}

	public void setSensResultat(String sensResultat) {
		this.sensResultat = sensResultat;
	}

	public BigDecimal getMtEquilibreBudgetairePrevisionnel() {
		return mtEquilibreBudgetairePrevisionnel;
	}

	public void setMtEquilibreBudgetairePrevisionnel(BigDecimal mtEquilibreBudgetairePrevisionnel) {
		this.mtEquilibreBudgetairePrevisionnel = mtEquilibreBudgetairePrevisionnel;
	}

	public Number getExercice() {
		return exercice;
	}

	public void setExercice(Number exercice) {
		this.exercice = exercice;
	}

    public String convertToString(String devise, Banque banque) {
        return aggregate(new Object[] {
                  typeEnregistrement, 
                  CofisupUtils.formatMontant(banque, devise, mtResultatExploitation, exercice.intValue()),
                  sensResultat,
                  CofisupUtils.formatMontant(banque, devise, mtEquilibreBudgetairePrevisionnel, exercice.intValue()), 
                  exercice
        });
    }
	
    @Override
    public String toString() {
        return "CompteResultatBean [typeEnregistrement=" + typeEnregistrement + ", mtResultatExploitation=" + mtResultatExploitation
                + ", sensResultat=" + sensResultat + ", mtEquilibreBudgetairePrevisionnel=" + mtEquilibreBudgetairePrevisionnel
                + ", exercice=" + exercice + "]";
    }
	
}
