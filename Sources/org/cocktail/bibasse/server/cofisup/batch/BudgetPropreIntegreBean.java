package org.cocktail.bibasse.server.cofisup.batch;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.common.metier.Banque;

/**
 * Cofisup BPI Bean.
 */
public class BudgetPropreIntegreBean extends CofisupBean {

	private static final long serialVersionUID = 1L;

	private String typeEnregistrement;
	private BigDecimal ressourcesLieesActivite;
	private BigDecimal contributionEtablissement;
	private Number exercice;

	public String getTypeEnregistrement() {
		return typeEnregistrement;
	}
	public void setTypeEnregistrement(String typeEnregistrement) {
		this.typeEnregistrement = typeEnregistrement;
	}
	public BigDecimal getRessourcesLieesActivite() {
		return ressourcesLieesActivite;
	}
	public void setRessourcesLieesActivite(BigDecimal ressourcesLieesActivite) {
		this.ressourcesLieesActivite = ressourcesLieesActivite;
	}
	public BigDecimal getContributionEtablissement() {
		return contributionEtablissement;
	}
	public void setContributionEtablissement(BigDecimal contributionEtablissement) {
		this.contributionEtablissement = contributionEtablissement;
	}
	public Number getExercice() {
		return exercice;
	}
	public void setExercice(Number exercice) {
		this.exercice = exercice;
	}

    public String convertToString(String devise, Banque banque) {
        return aggregate(new Object[] {
                  typeEnregistrement, 
                  CofisupUtils.formatMontant(banque, devise, ressourcesLieesActivite, exercice.intValue()), 
                  CofisupUtils.formatMontant(banque, devise, contributionEtablissement, exercice.intValue()), 
                  exercice
        });
    }
	
    @Override
    public String toString() {
        return "BudgetPropreIntegreBean [typeEnregistrement=" + typeEnregistrement + ", ressourcesLieesActivite=" + ressourcesLieesActivite
                + ", contributionEtablissement=" + contributionEtablissement + ", exercice=" + exercice + "]";
    }
}
