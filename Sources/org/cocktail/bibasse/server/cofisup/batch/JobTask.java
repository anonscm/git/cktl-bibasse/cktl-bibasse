package org.cocktail.bibasse.server.cofisup.batch;

import java.util.concurrent.Callable;

import org.cocktail.batch.Job;
import org.cocktail.common.cofisup.exception.CofisupException;

public class JobTask implements Callable<Job> {

	private final Job job;

	public JobTask(final Job job) {
		this.job = job;
	}

	public Job call() throws Exception {
		try {
			job.execute();
			return job;
		} catch (Throwable t) {
			Thread.currentThread().interrupt();
			throw new CofisupException(t);
		}
	}



}
