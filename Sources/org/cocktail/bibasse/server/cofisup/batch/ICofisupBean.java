package org.cocktail.bibasse.server.cofisup.batch;

import org.cocktail.common.metier.Banque;

public interface ICofisupBean {

    String convertToString(String devise, Banque banque);
    
}
