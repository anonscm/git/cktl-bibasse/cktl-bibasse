package org.cocktail.bibasse.server.cofisup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cocktail.bibasse.server.finder.Finder;
import org.cocktail.bibasse.server.finder.FinderNomenclatureEtatDetailleCredits;
import org.cocktail.bibasse.server.finder.FinderNomenclaturePrevisionsRecettes;
import org.cocktail.bibasse.server.finder.FinderOrgan;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOJefyAdminParametre;
import org.cocktail.bibasse.server.metier.EOJefyAdminTypeNatureBudget;
import org.cocktail.bibasse.server.metier.EONomenclatureEtatDetailleCredits;
import org.cocktail.bibasse.server.metier.EONomenclaturePrevisionsRecettes;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.cofisup.exception.CofisupException;
import org.cocktail.common.formule.Rubrique;
import org.cocktail.common.metier.IJefyAdminParametre;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Simple DAO Cofisup.
 */
public abstract class CofisupDao {

	private static final CofisupDao INSTANCE = new CofisupDao() {
	};

	/**
	 * @return singleton instance.
	 */
	public static CofisupDao instance() {
		return INSTANCE;
	}

	/**
	 * Constructeur par défaut.
	 */
	public CofisupDao() {
	}

	/**
	 * Retourne les organigrammes portant les natures spécifiées.
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @param naturesBudget natures cherchées.
	 * @return les organigrammes portant les natures spécifiées.
	 */
    public Set<EOOrgan> getOrganigrammes(EOEditingContext ec, EOExercice exercice, Set<String> naturesBudget) {
    	Set<EOOrgan> organigrammes = new HashSet<EOOrgan>();
    	if (naturesBudget == null) {
    		return organigrammes;
    	}

    	for (String natureBudget : naturesBudget) {
	    	organigrammes.addAll(FinderOrgan.instance().findOrgansByNatureBudget(ec, exercice, natureBudget));
    	}
    	return organigrammes;
    }

    /**
     * Retourne le code UAI de l'etablissement principal.
     * @param ec editing context.
     * @param exercice exercice.
     * @return code UAI de l'etablissement.
     */
	public String getCodeUAIEtablissement(EOEditingContext ec, EOExercice exercice) {
		NSArray organs = FinderOrgan.instance().findOrgansByNatureBudget(
				ec, exercice, EOJefyAdminTypeNatureBudget.NATURE_PRINCIPALE);

		if (organs == null || organs.isEmpty()) {
			throw new CofisupException(CofisupException.ERR_CODE_UAI_NO_PRINCIPAL);
		}

		EOOrgan organUAI = (EOOrgan) organs.get(0);
		String cStructure = organUAI.cStructure();
		String codeUAI = null;
		if (cStructure != null) {
			EOStructure structure = EOStructure.structurePourCode(ec, cStructure);
			if (structure != null && structure.cRne() != null) {
				codeUAI = structure.cRne();
			}
		}

		if (codeUAI == null) {
			throw new CofisupException(CofisupException.ERR_CODE_UAI_NOT_FOUND);
		}

		return codeUAI;
	}

	/**
	 * Retourne le numéro de SIRET de l'établissement.
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @return le numéro de SIRET de l'établissement pour l'exercice spécifié.
	 */
	public String getSiretEtablissement(EOEditingContext ec, EOExercice exercice) {
		NSMutableArray<EOQualifier> qualsArray = new NSMutableArray<EOQualifier>();
		EOQualifier qualParametre = EOQualifier.qualifierWithQualifierFormat(
				"parKey = %@", new NSArray<String>(EOJefyAdminParametre.SIRET_PARAMETRE));
		EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(
				"exeOrdre = %@", new NSArray<Integer>(exercice.exeOrdre()));
		qualsArray.addObject(qualParametre);
		qualsArray.addObject(qualExercice);
		EOAndQualifier qualSiret = new EOAndQualifier(qualsArray);

		EOJefyAdminParametre parametre = (EOJefyAdminParametre)
				Finder.fetchObject(ec, EOJefyAdminParametre.ENTITY_NAME, qualSiret, null);
		return StringCtrl.keepDigits(parametre.parValue());
	}

	/**
	 * Charge les rubriques spécifiques aux dépenses pour l'exercice spécifié.
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @return les rubriques spécifiques aux dépenses pour l'exercice spécifié.
	 */
	public List<Rubrique> listerRubriquesDepense(EOEditingContext ec, EOExercice exercice) {
		List<Rubrique> rubriques = new ArrayList<Rubrique>();
		NSArray codesNomenclatures = FinderNomenclatureEtatDetailleCredits.instance().findAll(ec, exercice);
		for (int idx = 0; idx < codesNomenclatures.count(); idx++) {
			EONomenclatureEtatDetailleCredits currentCode =
					(EONomenclatureEtatDetailleCredits) codesNomenclatures.objectAtIndex(idx);
			rubriques.add(new Rubrique(currentCode.necCode(), currentCode.necDetails()));
		}
		return rubriques;
	}

	/**
	 * Charge les rubriques spécifiques aux recettes pour l'exercice spécifié.
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @return les rubriques spécifiques aux recettes pour l'exercice spécifié.
	 */
	public List<Rubrique> listerRubriquesRecette(EOEditingContext ec, EOExercice exercice) {
		List<Rubrique> rubriques = new ArrayList<Rubrique>();
		NSArray codesNomenclatures = FinderNomenclaturePrevisionsRecettes.instance().findAll(ec, exercice);
		for (int idx = 0; idx < codesNomenclatures.count(); idx++) {
			EONomenclaturePrevisionsRecettes currentCode =
					(EONomenclaturePrevisionsRecettes) codesNomenclatures.objectAtIndex(idx);
			rubriques.add(new Rubrique(currentCode.nprCode(), currentCode.nprDetails()));
		}
		return rubriques;
	}

	public String getDevise(EOEditingContext ec, EOExercice exercice) {
	    NSMutableArray<EOQualifier> qualsArray = new NSMutableArray<EOQualifier>();
	    EOQualifier qualParametre = EOQualifier.qualifierWithQualifierFormat(
            "parKey = %@", new NSArray<String>(IJefyAdminParametre.PARAM_DEVISE_DEV_CODE));
	    EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(
	        "exeOrdre = %@", new NSArray<Integer>(exercice.exeOrdre()));
	    
	    qualsArray.addObject(qualParametre);
	    qualsArray.addObject(qualExercice);

	    EOAndQualifier qualSiret = new EOAndQualifier(qualsArray);
	    EOJefyAdminParametre parametre = (EOJefyAdminParametre) Finder.fetchObject(
	        ec, EOJefyAdminParametre.ENTITY_NAME, qualSiret, null);
	    
	    return parametre.parValue();
	}
}
