package org.cocktail.bibasse.server.cofisup;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXGenericRecord;

public class WebObjectQueryExecutor implements QueryExecutor {

	private EOEditingContext ec;
	private String modelName;

	public WebObjectQueryExecutor(EOEditingContext ec, String modelName) {
		this.ec = ec;
		this.modelName = modelName;
	}

	public NSArray execute(String sqlQuery, NSArray sqlKeys) {
		NSArray results = null;
		try {
			results = EOUtilities.rawRowsForSQL(getEc(), getModelName(), sqlQuery, sqlKeys);
		} catch (Exception e) {
			System.err.println("QueryExecutor - Erreur lors de l'execution de la requete " + sqlQuery + " : " + e);
			results = NSArray.EmptyArray;
		}
		return results;
	}

	public ERXGenericRecord executeUniqueResult(EOFetchSpecification fetchSpecification) {
		ERXGenericRecord result = null;
		try {
			NSArray results = getEc().objectsWithFetchSpecification(fetchSpecification);

			if (results.count() == 0) {
				result = null;
			} else {
				result = (ERXGenericRecord) results.objectAtIndex(0);
			}

			return result;
		} catch (Exception e) {
			System.err.println("WebObjectQueryExecutor. Erreur executeUniqueResult. Cause : " + e);
			return null;
		}
	}

	public EOEditingContext getEc() {
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
}
