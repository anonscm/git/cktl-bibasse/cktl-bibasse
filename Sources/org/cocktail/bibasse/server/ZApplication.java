/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Dictionary;
import java.util.TimeZone;

import org.cocktail.bibasse.server.utilities.ZLog;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrintConst;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;

public abstract class ZApplication extends CktlWebApplication {

	private static final String BIBASSE_CONFIG_FILENAME = "Bibasse.config";

	protected abstract boolean initApplicationSpecial();
	protected abstract String[] requiredParams();
	protected abstract String[] maquettesSix();
	protected abstract String versionTxt();
	protected abstract String fwkVersionTxt();
	protected abstract String fwkVersionNum();
	protected abstract String parametresTableName();

	private NSMutableDictionary dicoBdConnexionServerName;
	private NSMutableDictionary dicoBdConnexionServerId;
	private NSMutableDictionary appParametres;
	private MyByteArrayOutputStream redirectedOutStream, redirectedErrStream;
	private ZLog myInitLog;

	private A_CktlVersion version;
	private A_CktlVersion versionDatabase;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String configFileName() {
		return BIBASSE_CONFIG_FILENAME;
	}

	/**
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#initApplication()
	 */
	@Override
	public final void initApplication() {
		super.initApplication();

		System.out.println("Lancement de l'application serveur " + this.name() + " !");

		// init versions
		this.version = new Version();
		this.versionDatabase = new VersionDatabase();

		// redirection des logs pour les garder en mémoire + log d'init...
		redirectLogs();
		myInitLog = new ZLog();
		myInitLog.appendBigTitle("INITIALISATION");

		boolean ok1 = init();
		boolean ok2 = checkUp();
		System.out.println("ZApplication.initApplication()");
		boolean ok3 = initApplicationSpecial();

		if (!ok1 || !ok2 || !ok3) {
			myInitLog.append("");
			myInitLog.appendWarning("Il y a eu des ERREURS D'INITIALISATION !!!");
		}

		myInitLog.appendBigTitle("INITIALISATION TERMINEE");
	}

    @Override
    public A_CktlVersion appCktlVersion() {
        return version;
    }

    @Override
    public A_CktlVersion appCktlVersionDb() {
        return versionDatabase;
    }

	/**
	 *
	 * @return
	 */
	private boolean init() {
		System.out.println("ZApplication.init()");
		boolean ok = true;

		// full logs?
		if (getParam("LOG_SQL_GENERATION") != null) {
			if (getParam("LOG_SQL_GENERATION").equals("TRUE") || getParam("LOG_SQL_GENERATION").equals("1")) {
				NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
				NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupSQLGeneration);
				NSLog.allowDebugLoggingForGroups(NSLog.DebugGroupDatabaseAccess);
			}
		}

		// on ajuste le timezone
		initTimeZones(myInitLog, "GMT");

		return ok;
	}

	/**
	 * vérification de tout ce qui peut être vérifié au lancement du serveur...
	 *
	 * @return true si tout est ok, false si il y a des warnings
	 */
	private boolean checkUp() {
		boolean ok = true;

		// Versions...
		myInitLog.appendTitle("Versions");
		myInitLog.appendKeyValue("Version Java Runtime", System.getProperty("java.version"));
		myInitLog.appendKeyValue("Version de " + name(), versionTxt());
		myInitLog.appendKeyValue("Version du Framework", fwkVersionTxt());

		// Proprietes systeme...
		myInitLog.appendTitle("Proprietes systeme");
		myInitLog.appendMap(System.getProperties());

		// Base de données...
		myInitLog.appendTitle("Base de donnees");
		if (checkBdConnection()) {
			myInitLog.appendSuccess("La connexion a la base de donnees est active");
		} else {
			myInitLog.appendWarning("La connexion a la base de donnees n'est pas active!!");
			ok = false;
		}

		// vérif des modèles...
		if (!checkModels()) {
			myInitLog.appendWarning("Les modeles pointent vers differentes instances de base de donnees (cf. ci-dessus), ceci peut causer des problemes d'incoherence lors de l'execution.");
			ok = false;
		}

		return ok;
	}

	private boolean checkModels() {
		//On récupère tous les modeles utilisés
		dicoBdConnexionServerName = new NSMutableDictionary();
		dicoBdConnexionServerId = new NSMutableDictionary();
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		for (int i = 0; i < vModelGroup.models().count(); i++) {
			EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
			dicoBdConnexionServerName.takeValueForKey(bdConnexionServerName(tmpEOModel), tmpEOModel.name());
			dicoBdConnexionServerId.takeValueForKey(bdConnexionServerId(tmpEOModel), tmpEOModel.name());
			myInitLog.append("");
			myInitLog.append("Modele " + tmpEOModel.name() + " : ");
			myInitLog.appendKeyValue("  * Connexion base de donnees", bdConnexionUrl(tmpEOModel));
			myInitLog.appendKeyValue("  * Instance", bdConnexionServerId(tmpEOModel));
			myInitLog.appendKeyValue("  * User base de donnees", bdConnexionUser(tmpEOModel));
		}

		//vérifier que tous les modeles sont sur le m?me serverId
		String sid = null;
		boolean erreurSid = false;
		for (int i = 0; i < vModelGroup.models().count(); i++) {
			EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
			if (sid == null) {
				sid = bdConnexionServerId(tmpEOModel);
			} else if (!sid.toUpperCase().equals(bdConnexionServerId(tmpEOModel).toUpperCase())) {
				erreurSid = true;
			}
		}
		return !erreurSid;
	}

	protected boolean checkBdConnection() {
		return CktlDataBus.isDatabaseConnected();
	}

	public String bdConnexionInfo() {
		String info = (String) dicoBdConnexionServerName.valueForKey(mainModelName());
		if (info.length() > 0) {
			info = "@" + info;
		}
		info = (String) dicoBdConnexionServerId.valueForKey(mainModelName()) + info;
		return info;
	}

	/**
	 * Renvoie l'url de connexion à la base de données pour le modele spécifié en parametre.
	 *
	 * @param model
	 * @return
	 */
	private String bdConnexionUrl(EOModel model) {
		NSDictionary vDico = model.connectionDictionary();
		return (String) vDico.valueForKey("URL");
	}

	/**
	 * Renvoie le user base de données
	 *
	 * @return
	 */
	private String bdConnexionUser(EOModel model) {
		NSDictionary vDico = model.connectionDictionary();
		return (String) vDico.valueForKey("username");
	}

	/**
	 * Renvoie le serverid de la base de données (par exemple gest).
	 *
	 * @return
	 */
	private String bdConnexionServerId(EOModel model) {
		String[] parts = bdSecondPartUrl(model);
		String serverBdName = null;
		if (parts.length > 1) {
			serverBdName = parts[parts.length - 1];
		}
		return serverBdName;
	}

	/**
	 * Renvoie le serverid de la base de données (par exemple jane).
	 *
	 * @return
	 */
	private String bdConnexionServerName(EOModel model) {
		String[] parts = bdSecondPartUrl(model);
		String serverName = null;
		if (parts.length > 0) {
			serverName = parts[0];
		}
		return serverName;
	}

	private String[] bdSecondPartUrl(EOModel model) {
		String url = bdConnexionUrl(model);
		String[] res;
		//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
		//On sépare la partie jdbc de la partie server
		res = url.split("@");
		if (res.length > 1) {
			String serverUrl = res[1];
			res = serverUrl.split(":");
			if (res.length > 0) {
				return res;
			}
		}
		return new String[0];
	}

	/**
	 * Initialise le TimeZone à utiliser pour l'application.
	 */
	private void initTimeZones(ZLog myLog, String timezone) {
		myLog.appendTitle("Initialisation du NSTimeZone");
		myLog.appendKeyValue("NSTimeZone par defaut recupere sur le systeme (avant initialisation) ", NSTimeZone.defaultTimeZone(), 70);
		NSTimeZone theTimezone = NSTimeZone.timeZoneWithName("GMT", false); // Valeur par défaut
		if (timezone != null && !timezone.trim().equals("")) {
			NSTimeZone ntz = NSTimeZone.timeZoneWithName(timezone, false);
			if (ntz == null) {
				myLog.appendWarning("Le Timezone defini n'est pas valide (" + timezone + ")");
			} else {
				theTimezone = ntz;
			}
		}
		TimeZone.setDefault(theTimezone);
		NSTimeZone.setDefaultTimeZone(theTimezone);

		myLog.appendKeyValue("NSTimeZone par defaut utilise dans l'application", NSTimeZone.defaultTimeZone(), 70);
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		myLog.appendKeyValue("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone", ntf.defaultParseTimeZone(), 70);
		myLog.appendKeyValue("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone", ntf.defaultFormatTimeZone(), 70);
		myLog.append("");
	}

	/**
	 * Recupere une valeur dans la table parametresTableName(). Si elle n'existe pas dans la table, va la chercher dans
	 * le configFileName(), et si n'existe pas va dans la table configTableName()
	 *
	 * @param paramKey
	 *            La cle a rechercher
	 * @return La premiere valeur associée à la clé paramkey.
	 * @see Application#appParametres
	 */
	public String getParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		} else {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}

	/**
	 * Récupère x valeur(s) dans la table Parametres. Si elle n'existe pas dans la table, va la chercher dans le
	 * configFileName(), et si n'existe pas va dans la table configTableName()
	 *
	 * @param paramKey
	 *            La clé à rechercher
	 * @return La(les) valeur(s) associée(s) à la clé paramkey.
	 * @see Application#appParametres
	 */
	public NSArray getParams(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			a = config().valuesForKey(paramKey);
		}
		if (a == null) {
			a = new NSArray();
		}
		return a;
	}

	/**
	 * vérifie si tous les param?tres nécessaires à l'application sont bien présents et initialisés.
	 */
	private boolean checkRequiredParams() {
		boolean ok = true;
		myInitLog.appendTitle("Verification des parametres");
		for (int i = 0; i < requiredParams().length; i++) {
			String p = getParam(requiredParams()[i]);
			if (p == null || p.trim().length() == 0) {
				myInitLog.appendWarning("Le parametre " + requiredParams()[i] + " est absent ou vide. Initialisez-le dans la table "
						+ parametresTableName() + ", ou " + configTableName() + " ou bien dans le fichier " + configFileName());
				ok = false;
			} else {
				//On affiche la paire clé/valeur dans le log
				myInitLog.appendKeyValue(requiredParams()[i], p);
			}
		}
		myInitLog.append("");
		return ok;
	}

	/**
	 * @return Les parametres de l'application stockés dans la table parametresTableName()
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			java.util.Enumeration enumerator = vParam.objectEnumerator();
			while (enumerator.hasMoreElements()) {
				vTmpRec = (EOGenericRecord) enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
						|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					a.addObject(vTmpRec.valueForKey("paramValue"));
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
		}
		return appParametres;
	}

	private void redirectLogs() {
		redirectedOutStream = new MyByteArrayOutputStream(System.out, 102400); // limite le log en mémoire à 100Ko
		redirectedErrStream = new MyByteArrayOutputStream(System.err, 102400); // limite le log en mémoire à 100Ko
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		// on force la sortie du NSLog (qui était initialisé par défaut sur System.xxx AVANT le changement)
		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	// Retourne les logs out et err
	public String outLog() {
		return redirectedOutStream.toString();
	}

	public String errLog() {
		return redirectedErrStream.toString();
	}

	/**
	 * Récupère le contenu du fichier fileName
	 *
	 * @return
	 */
	public String fileContent(String fileName, String filePath) {
		try {
			File f = new File(filePath);
			int size = (int) f.length();
			FileInputStream in = new FileInputStream(f);
			byte[] data = new byte[size];
			in.read(data);
			in.close();
			return new String(data);
		}
		catch (FileNotFoundException fnfe) {
			System.err.println("Fichier " + fileName + " introuvable !");
			return "";
		}
		catch (IOException ioe) {
			System.err.println("Erreur lors de la lecture du fchier " + fileName + " : " + ioe);
			return "";
		}
	}

	/**
	 * Renvoie la version du runtime Java utilisé.
	 *
	 * @return
	 */
	public String getJREVersion() {
		return System.getProperty("java.version");
	}

	/**
	 * vérifie si le mod?le d'impression est bien disponible.
	 */
	public void checkIfReportsExists(String sixServiceHost, String sixServicePort, CktlPrinter printer, String maquetteID) throws Exception {
		if (!printer.checkTemplate(maquetteID)) {
			switch (printer.getErrorCode()) {
			case CktlPrintConst.ERR_CONNECT:
				throw new Exception("Le service SIX " + sixServiceHost + " : " + sixServicePort + " n'est pas disponible.");

			case CktlPrintConst.ERR_NO_TEMPLATE:
				throw new Exception("Le modele d'impression " + maquetteID + " n'est pas reference dans la base de donnees du service d'impression.");

			case CktlPrintConst.ERR_OK:
				break;

			default:
				throw new Exception("Une erreur est survenue lors d'une operation SIX. Code erreur : " + printer.getErrorCode());
			}
		}
	}

	/**
	 * vérifie la disponibilité du service d'impression.
	 *
	 * @return
	 */
	public CktlPrinter checkIfPrintServiceAvailable(CktlConfig config) throws Exception {
		CktlPrinter printer = null;
		try {
			try {
				printer = CktlPrinter.newDefaultInstance(config);
				myInitLog.appendSuccess("Le moteur d'impression est accessible");
			}
			catch (ClassNotFoundException e) {
				throw new Exception(
						"La classe necessaire à l'appel au service d'impression n'est pas accessible (param?tre XML_PRINTER_DRIVER). Les frameworks ne doivent pas etre à jour.");
			}
			catch (Exception e1) {
				throw new Exception(
						"Impossible d'initialiser le client du service d'impression. Certains parametres peuvent contenir des valeurs erronees (parametres commencant par SIX_).");
			}

			if (printer != null) {
				Dictionary dicoprint = printer.checkService();
				if (dicoprint == null) {
					throw new Exception("Le service d'impression est indisponible.");
				}
				myInitLog.appendSuccess("Le service d'impression est disponible.");
				myInitLog.appendKeyValue("SERVICE_NAME", dicoprint.get(CktlPrintConst.SERVICE_NAME_KEY));
				myInitLog.appendKeyValue("SERVICE_VERSION_KEY", dicoprint.get(CktlPrintConst.SERVICE_VERSION_KEY));
				myInitLog.appendKeyValue("SERVICE_DESCRIPTION_KEY", dicoprint.get(CktlPrintConst.SERVICE_DESCRIPTION_KEY));
				myInitLog.append("");
				return printer;
			}
		}
		catch (Exception e) {
			myInitLog.appendWarning(e.getMessage());
			return null;
		}
		return null;
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les conservant dans un ByteArray...
	// Garde en mémoire un log de taille entre maxCount/2 et maxCount octets
	public class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		protected int maxCount;

		public MyByteArrayOutputStream() {
			this(System.out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this(out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out, int maxCount) {
			super(maxCount);
			this.out = out;
			this.maxCount = maxCount;
		}

		public synchronized void write(int b) {
			if (maxCount > 0 && count + 1 > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			if (maxCount > 0 && count + len > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b, off, len);
			out.write(b, off, len);
		}

		private void shift(int shift) {
			for (int i = shift; i < count; i++) {
				buf[i - shift] = buf[i];
			}
			count = count - shift;
		}
	}

}