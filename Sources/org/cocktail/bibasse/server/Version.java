package org.cocktail.bibasse.server;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;

/**
 * Gere les versions minimales requises apr l'&application.
 *
 */
public class Version extends A_CktlVersion {

	public static final String APPLICATIONFINALNAME = VersionMe.APPLICATIONFINALNAME;
	public static final String APPLICATIONINTERNALNAME = VersionMe.APPLICATIONINTERNALNAME;

	// Version minimum de la base de donnees (user mjefy_budget) necessaire pour fonctionner avec cette version
	public static final String MINDBVERSION = "1.3.2.0";

	/** Version du JRE */
	private static final String JRE_VERSION_MIN = "1.5.0.0";
	private static final String JRE_VERSION_MAX = null;

	/** Version de la base de données requise */
	private static final String BD_VERSION_MIN = "1.3.2.0";
	private static final String BD_VERSION_MAX = null;

	public String name() {
		return APPLICATIONFINALNAME;
	}

	public String internalName() {
		return APPLICATIONINTERNALNAME;
	}

	public int versionNumMaj() {
		return VersionMe.VERSIONNUMMAJ;
	}

	public int versionNumMin() {
		return VersionMe.VERSIONNUMMIN;
	}

	public int versionNumPatch() {
		return VersionMe.VERSIONNUMPATCH;
	}

	public int versionNumBuild() {
		return VersionMe.VERSIONNUMBUILD;
	}

	public String date() {
		return VersionMe.VERSIONDATE;
	}

	// liste des dependances
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true),
				new CktlVersionRequirements(new VersionDatabase(), BD_VERSION_MIN, BD_VERSION_MAX, true),
		};
	}


}
