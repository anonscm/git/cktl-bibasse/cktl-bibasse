package org.cocktail.bibasse.server;

import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser;

/**
 * Fournit la version du user JEFY_BUDGET installée.
 *
 */
public class VersionDatabase extends CktlVersionOracleUser {

	private static String NAME = "BD USER JEFY_BUDGET";
	private static String DB_USER_TABLE_NAME = "JEFY_BUDGET.DB_VERSION";
	private static String DB_VERSION_DATE_COLUMN_NAME = "DB_INSTALL_DATE";
	private static String DB_VERSION_ID_COLUMN_NAME = "DB_VERSION_ID";

	public String dbUserTableName() {
		return DB_USER_TABLE_NAME;
	}

	public String dbVersionDateColumnName() {
		return DB_VERSION_DATE_COLUMN_NAME;
	}

	public String dbVersionIdColumnName() {
		return DB_VERSION_ID_COLUMN_NAME;
	}

	public String name() {
		return NAME;
	}

	public String sqlRestriction() {
		return "";
	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}

}
