package org.cocktail.bibasse.server.factory;

import java.math.BigDecimal;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class SaisieBudgetCtrl {


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void traiterMouvement(EOEditingContext ec, NSDictionary parametres) throws Exception	{

			System.out.println("SaisieBudgetCtrl.traiterMouvement() - MOUVEMENT - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"traiterMouvement",parametres);

	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void annulerMouvement(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.annulerMouvement() - MOUVEMENT - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"annulerMouvement",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.annulerMouvement() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur d'annulation du mouvement budgétaire !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void viderBudgetOrgan(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.viderBudgetOrgan() - VIDER BUDGET ORGAN - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"viderBudgetOrgan",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.viderBudgetOrgan() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de reinitialisation du budget !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void deverouillerLbud(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.deverouillerLbud() - DEVEROUILLER CR - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"deverouillerSaisie",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.deverouillerLbud() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de deverouillage de la ligne budgetaire !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void validerLbud(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.validerLigneBudgetaire() - VALIDATION CR - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"validerSaisieBudgetOrgan",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.validerLigneBudgetaire() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de validation budgétaire !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void controlerLbud(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.controlerLbud() - CONTROLE CR - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"controlerSaisieBudgetOrgan",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.controlerLbud() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de contrôle budgétaire !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void cloturerLbud(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.cloturerLbud() - CLOTURE CR - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec, "cloturerSaisieBudgetOrgan", parametres);
		} catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.cloturerLbud() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de clôture budgétaire !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void voterBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.voterBudget() - VOTE BUDGET - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"voterSaisieBudget",parametres);
		} catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.voterBudget() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors du vote du budget !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void refuserBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.refuserBudget() - REFUSER BUDGET - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"refuserSaisieBudget",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.refuserBudget() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors du refus du budget !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void changerSaisieBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.changerSaisieBudget() - CHANGE SAISIE BUDGET - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"changerSaisieBudget",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.changerSaisieBudget() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors du changement de type de budget !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void supprimerSaisieBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.supprimerSaisieBudget() - SUPPRESSION SAISIE BUDGET - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"supprimerSaisieBudget",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.supprimerSaisieBudget() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de la suppression d'une ligne budgetaire !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void supprimerSaisieDbm(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.supprimerSaisieDbm() - SUPPRESSION SAISIE DBM - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"supprimerSaisieDbm",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.supprimerSaisieDbm() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de la suppression d'une DBM !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void supprimerBudget(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.supprimerBudget() - SUPPRESSION SAISIE DBM - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"supprimerBudget",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.supprimerSaisieDbm() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de la suppression d'un budget !\nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void initialiserBudgetDbm(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.initialiserBudgetDbm() - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"initialiserBudgetDbm",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.initialiserBudgetDbm() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de l'initialisation du budget DBM !/nMESSAGE : " + e.getMessage());
		}
	}



	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void initialiserBudgetReliquat(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.initialiserBudgetReliquat() - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"initialiserBudgetReliquat",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.initialiserBudgetReliquat() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de l'initialisation du budget RELIQUAT !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void initialiserNatureLolf(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.initialiserNatureLolf() - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"initialiserNatureLolf",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.initialiserNatureLolf() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de l'initialisation Nature/Lolf !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void cloturerNatureLolf(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.cloturerNatureLolf() - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"cloturerNatureLolf",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.cloturerNatureLolf() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de la cloture Nature/Lolf !/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void consoliderBudgetSaisie(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.consoliderBudgetSaisie() - CONSOLIDATION BUDGET - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"consoliderSaisieBudget",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.consoliderBudgetSaisie() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de la consolidation du budget !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void consoliderBudgetsVotes(EOEditingContext ec, NSDictionary parametres) throws Exception	{
		try {
			System.out.println("SaisieBudgetCtrl.consoliderBudgetsVotes() - CONSOLIDATION BUDGETS VOTES - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"consoliderBudgetsVotes",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.consoliderBudgetsVotes() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur lors de la consolidation des budgets votes !/nMESSAGE : " + e.getMessage());
		}
	}

	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void setBudgetsVotesOrgan(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.setBudgetsVotesOrgan() - SET BDS_VOTE_ORGAN - PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"setBdsVoteOrgan",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.setBudgetsVotesOrgan() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de validation budgétaire !/nMESSAGE : " + e.getMessage());
		}
	}



	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void addMasqueNature(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.addMasqueNature() PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"addMasqueNature",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.addMasqueNature() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de mise a jour du masque nature !/nMESSAGE : " + e.getMessage());
		}
	}



	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void delMasqueNature(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.delMasqueNature() PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"delMasqueNature",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.delMasqueNature() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de mise a jour du masque nature !/nMESSAGE : " + e.getMessage());
		}
	}



	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void dupliquerMasque(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.dupliquerMasque() PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"dupliquerMasque",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.dupliquerMasque() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de duplication du masque de saisie!/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void addMasqueGestion(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.addMasqueGestion() PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"addMasqueGestion",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.addMasqueGestion() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de mise à jour du masque gestion!/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static void delMasqueGestion(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		try {
			System.out.println("SaisieBudgetCtrl.delMasqueGestion() PARAMETRES : " + parametres);
			EOUtilities.executeStoredProcedureNamed(ec,"delMasqueGestion",parametres);
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.delMasqueGestion() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de mise à jour du masque gestion!/nMESSAGE : " + e.getMessage());
		}
	}


	/**
	 *
	 * @param ec
	 * @param parametres
	 * @throws Exception
	 */
	public static BigDecimal getDisponible(EOEditingContext ec, NSDictionary parametres) throws Exception	{

		NSDictionary retourProc = new NSDictionary();

		try {
			retourProc = EOUtilities.executeStoredProcedureNamed(ec,"disponible",parametres);

			return new BigDecimal(retourProc.objectForKey("04dispo").toString());
		}
		catch (Exception e) {
			System.out.println("SaisieBudgetCtrl.getDisponible() - ERREUR");
			e.printStackTrace();
			throw new Exception("Erreur de récupération du disponible budgétaire !/nMESSAGE : " + e.getMessage());
		}
	}

}
