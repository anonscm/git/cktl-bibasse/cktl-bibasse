/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrgan.java instead.
package org.cocktail.bibasse.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOOrgan extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Organ";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.V_ORGAN";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "orgId";

	public static final String C_STRUCTURE_KEY = "cStructure";
    public static final er.extensions.eof.ERXKey<java.lang.String> C_STRUCTURE = new er.extensions.eof.ERXKey<java.lang.String>("cStructure");
	public static final String ONB_ID_KEY = "onbId";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> ONB_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("onbId");
	public static final String ORG_CR_KEY = "orgCr";
    public static final er.extensions.eof.ERXKey<java.lang.String> ORG_CR = new er.extensions.eof.ERXKey<java.lang.String>("orgCr");
	public static final String ORG_DATE_CLOTURE_KEY = "orgDateCloture";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> ORG_DATE_CLOTURE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("orgDateCloture");
	public static final String ORG_DATE_OUVERTURE_KEY = "orgDateOuverture";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> ORG_DATE_OUVERTURE = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("orgDateOuverture");
	public static final String ORG_ETAB_KEY = "orgEtab";
    public static final er.extensions.eof.ERXKey<java.lang.String> ORG_ETAB = new er.extensions.eof.ERXKey<java.lang.String>("orgEtab");
	public static final String ORG_ID_KEY = "orgId";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> ORG_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("orgId");
	public static final String ORG_LIBELLE_KEY = "orgLibelle";
    public static final er.extensions.eof.ERXKey<java.lang.String> ORG_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("orgLibelle");
	public static final String ORG_LUCRATIVITE_KEY = "orgLucrativite";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> ORG_LUCRATIVITE = new er.extensions.eof.ERXKey<java.lang.Integer>("orgLucrativite");
	public static final String ORG_NIVEAU_KEY = "orgNiveau";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> ORG_NIVEAU = new er.extensions.eof.ERXKey<java.lang.Integer>("orgNiveau");
	public static final String ORG_SOUSCR_KEY = "orgSouscr";
    public static final er.extensions.eof.ERXKey<java.lang.String> ORG_SOUSCR = new er.extensions.eof.ERXKey<java.lang.String>("orgSouscr");
	public static final String ORG_UB_KEY = "orgUb";
    public static final er.extensions.eof.ERXKey<java.lang.String> ORG_UB = new er.extensions.eof.ERXKey<java.lang.String>("orgUb");
	public static final String ORG_UNIV_KEY = "orgUniv";
    public static final er.extensions.eof.ERXKey<java.lang.String> ORG_UNIV = new er.extensions.eof.ERXKey<java.lang.String>("orgUniv");

	// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_PERE_KEY = "orgPere";
	public static final String TYOR_ID_KEY = "tyorId";

	//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String ONB_ID_COLKEY = "ONB_ID";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_DATE_CLOTURE_COLKEY = "ORG_DATE_CLOTURE";
	public static final String ORG_DATE_OUVERTURE_COLKEY = "ORG_DATE_OUVERTURE";
	public static final String ORG_ETAB_COLKEY = "ORG_ETAB";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORG_LIBELLE_COLKEY = "ORG_LIB";
	public static final String ORG_LUCRATIVITE_COLKEY = "ORG_LUCRATIVITE";
	public static final String ORG_NIVEAU_COLKEY = "ORG_NIV";
	public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String ORG_UNIV_COLKEY = "ORG_UNIV";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_PERE_COLKEY = "ORG_PERE";
	public static final String TYOR_ID_COLKEY = "TYOR_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOExercice> EXERCICE = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOExercice>("exercice");
	public static final String ORGAN_FILS_KEY = "organFils";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrgan> ORGAN_FILS = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrgan>("organFils");
	public static final String ORGAN_PERE_KEY = "organPere";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrgan> ORGAN_PERE = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrgan>("organPere");
	public static final String TO_NATURE_BUDGET_KEY = "toNatureBudget";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrganNatureBudget> TO_NATURE_BUDGET = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrganNatureBudget>("toNatureBudget");
	public static final String TYPE_ORGAN_KEY = "typeOrgan";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeOrgan> TYPE_ORGAN = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeOrgan>("typeOrgan");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public Integer onbId() {
    return (Integer) storedValueForKey(ONB_ID_KEY);
  }

  public void setOnbId(Integer value) {
    takeStoredValueForKey(value, ONB_ID_KEY);
  }

  public String orgCr() {
    return (String) storedValueForKey(ORG_CR_KEY);
  }

  public void setOrgCr(String value) {
    takeStoredValueForKey(value, ORG_CR_KEY);
  }

  public NSTimestamp orgDateCloture() {
    return (NSTimestamp) storedValueForKey(ORG_DATE_CLOTURE_KEY);
  }

  public void setOrgDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, ORG_DATE_CLOTURE_KEY);
  }

  public NSTimestamp orgDateOuverture() {
    return (NSTimestamp) storedValueForKey(ORG_DATE_OUVERTURE_KEY);
  }

  public void setOrgDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, ORG_DATE_OUVERTURE_KEY);
  }

  public String orgEtab() {
    return (String) storedValueForKey(ORG_ETAB_KEY);
  }

  public void setOrgEtab(String value) {
    takeStoredValueForKey(value, ORG_ETAB_KEY);
  }

  public Integer orgId() {
    return (Integer) storedValueForKey(ORG_ID_KEY);
  }

  public void setOrgId(Integer value) {
    takeStoredValueForKey(value, ORG_ID_KEY);
  }

  public String orgLibelle() {
    return (String) storedValueForKey(ORG_LIBELLE_KEY);
  }

  public void setOrgLibelle(String value) {
    takeStoredValueForKey(value, ORG_LIBELLE_KEY);
  }

  public Integer orgLucrativite() {
    return (Integer) storedValueForKey(ORG_LUCRATIVITE_KEY);
  }

  public void setOrgLucrativite(Integer value) {
    takeStoredValueForKey(value, ORG_LUCRATIVITE_KEY);
  }

  public Integer orgNiveau() {
    return (Integer) storedValueForKey(ORG_NIVEAU_KEY);
  }

  public void setOrgNiveau(Integer value) {
    takeStoredValueForKey(value, ORG_NIVEAU_KEY);
  }

  public String orgSouscr() {
    return (String) storedValueForKey(ORG_SOUSCR_KEY);
  }

  public void setOrgSouscr(String value) {
    takeStoredValueForKey(value, ORG_SOUSCR_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public String orgUniv() {
    return (String) storedValueForKey(ORG_UNIV_KEY);
  }

  public void setOrgUniv(String value) {
    takeStoredValueForKey(value, ORG_UNIV_KEY);
  }

  public org.cocktail.bibasse.server.metier.EOExercice exercice() {
    return (org.cocktail.bibasse.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.bibasse.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOOrgan organPere() {
    return (org.cocktail.bibasse.server.metier.EOOrgan)storedValueForKey(ORGAN_PERE_KEY);
  }

  public void setOrganPereRelationship(org.cocktail.bibasse.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOOrgan oldValue = organPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_PERE_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOOrganNatureBudget toNatureBudget() {
    return (org.cocktail.bibasse.server.metier.EOOrganNatureBudget)storedValueForKey(TO_NATURE_BUDGET_KEY);
  }

  public void setToNatureBudgetRelationship(org.cocktail.bibasse.server.metier.EOOrganNatureBudget value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOOrganNatureBudget oldValue = toNatureBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NATURE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NATURE_BUDGET_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOTypeOrgan typeOrgan() {
    return (org.cocktail.bibasse.server.metier.EOTypeOrgan)storedValueForKey(TYPE_ORGAN_KEY);
  }

  public void setTypeOrganRelationship(org.cocktail.bibasse.server.metier.EOTypeOrgan value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOTypeOrgan oldValue = typeOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORGAN_KEY);
    }
  }

  public NSArray organFils() {
    return (NSArray)storedValueForKey(ORGAN_FILS_KEY);
  }

  public NSArray organFils(EOQualifier qualifier) {
    return organFils(qualifier, null);
  }

  public NSArray organFils(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = organFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToOrganFilsRelationship(org.cocktail.bibasse.server.metier.EOOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
  }

  public void removeFromOrganFilsRelationship(org.cocktail.bibasse.server.metier.EOOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
  }

  public org.cocktail.bibasse.server.metier.EOOrgan createOrganFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Organ");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_FILS_KEY);
    return (org.cocktail.bibasse.server.metier.EOOrgan) eo;
  }

  public void deleteOrganFilsRelationship(org.cocktail.bibasse.server.metier.EOOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganFilsRelationships() {
    Enumeration objects = organFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganFilsRelationship((org.cocktail.bibasse.server.metier.EOOrgan)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOOrgan avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOrgan createEOOrgan(EOEditingContext editingContext, String cStructure
, String orgCr
, String orgEtab
, Integer orgId
, String orgLibelle
, Integer orgLucrativite
, Integer orgNiveau
, String orgSouscr
, String orgUb
, String orgUniv
, org.cocktail.bibasse.server.metier.EOExercice exercice, org.cocktail.bibasse.server.metier.EOTypeOrgan typeOrgan			) {
    EOOrgan eo = (EOOrgan) createAndInsertInstance(editingContext, _EOOrgan.ENTITY_NAME);
		eo.setCStructure(cStructure);
		eo.setOrgCr(orgCr);
		eo.setOrgEtab(orgEtab);
		eo.setOrgId(orgId);
		eo.setOrgLibelle(orgLibelle);
		eo.setOrgLucrativite(orgLucrativite);
		eo.setOrgNiveau(orgNiveau);
		eo.setOrgSouscr(orgSouscr);
		eo.setOrgUb(orgUb);
		eo.setOrgUniv(orgUniv);
    eo.setExerciceRelationship(exercice);
    eo.setTypeOrganRelationship(typeOrgan);
    return eo;
  }


	  public EOOrgan localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrgan)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrgan creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrgan creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOrgan object = (EOOrgan)createAndInsertInstance(editingContext, _EOOrgan.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EOOrgan localInstanceIn(EOEditingContext editingContext, EOOrgan eo) {
    EOOrgan localInstance = (eo == null) ? null : (EOOrgan)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOrgan#localInstanceIn a la place.
   */
	public static EOOrgan localInstanceOf(EOEditingContext editingContext, EOOrgan eo) {
		return EOOrgan.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EOOrgan fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrgan fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrgan)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOOrgan fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrgan eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrgan)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrgan fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrgan eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrgan ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOOrgan fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}
