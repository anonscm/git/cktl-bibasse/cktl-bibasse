/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetMouvCredit.java instead.
package org.cocktail.bibasse.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOBudgetMouvCredit extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "BudgetMouvementsCredit";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Mouvements_Credit";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdmcId";

	public static final String BDMC_MONTANT_KEY = "bdmcMontant";
    public static final er.extensions.eof.ERXKey<java.math.BigDecimal> BDMC_MONTANT = new er.extensions.eof.ERXKey<java.math.BigDecimal>("bdmcMontant");

	// Attributs non visibles
	public static final String BDMC_ID_KEY = "bdmcId";
	public static final String BMOU_ID_KEY = "bmouId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYSE_ID_KEY = "tyseId";

	//Colonnes dans la base de donnees
	public static final String BDMC_MONTANT_COLKEY = "BDMC_MONTANT";

	public static final String BDMC_ID_COLKEY = "BDMC_ID";
	public static final String BMOU_ID_COLKEY = "BMOU_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYSE_ID_COLKEY = "TYSE_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOExercice> EXERCICE = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOExercice>("exercice");
	public static final String MOUVEMENT_KEY = "mouvement";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOBudgetMouvements> MOUVEMENT = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOBudgetMouvements>("mouvement");
	public static final String ORGAN_KEY = "organ";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrgan> ORGAN = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOOrgan>("organ");
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeCredit> TYPE_CREDIT = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeCredit>("typeCredit");
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeEtat> TYPE_ETAT = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeEtat>("typeEtat");
	public static final String TYPE_SENS_KEY = "typeSens";
	public static final er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeSens> TYPE_SENS = new er.extensions.eof.ERXKey<org.cocktail.bibasse.server.metier.EOTypeSens>("typeSens");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public java.math.BigDecimal bdmcMontant() {
    return (java.math.BigDecimal) storedValueForKey(BDMC_MONTANT_KEY);
  }

  public void setBdmcMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDMC_MONTANT_KEY);
  }

  public org.cocktail.bibasse.server.metier.EOExercice exercice() {
    return (org.cocktail.bibasse.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.bibasse.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOBudgetMouvements mouvement() {
    return (org.cocktail.bibasse.server.metier.EOBudgetMouvements)storedValueForKey(MOUVEMENT_KEY);
  }

  public void setMouvementRelationship(org.cocktail.bibasse.server.metier.EOBudgetMouvements value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOBudgetMouvements oldValue = mouvement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOUVEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MOUVEMENT_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOOrgan organ() {
    return (org.cocktail.bibasse.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.bibasse.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.bibasse.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.bibasse.server.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.bibasse.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.bibasse.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }

  public org.cocktail.bibasse.server.metier.EOTypeSens typeSens() {
    return (org.cocktail.bibasse.server.metier.EOTypeSens)storedValueForKey(TYPE_SENS_KEY);
  }

  public void setTypeSensRelationship(org.cocktail.bibasse.server.metier.EOTypeSens value) {
    if (value == null) {
    	org.cocktail.bibasse.server.metier.EOTypeSens oldValue = typeSens();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SENS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SENS_KEY);
    }
  }


/**
 * Créer une instance de EOBudgetMouvCredit avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetMouvCredit createEOBudgetMouvCredit(EOEditingContext editingContext, java.math.BigDecimal bdmcMontant
, org.cocktail.bibasse.server.metier.EOExercice exercice, org.cocktail.bibasse.server.metier.EOBudgetMouvements mouvement, org.cocktail.bibasse.server.metier.EOOrgan organ, org.cocktail.bibasse.server.metier.EOTypeCredit typeCredit, org.cocktail.bibasse.server.metier.EOTypeEtat typeEtat			) {
    EOBudgetMouvCredit eo = (EOBudgetMouvCredit) createAndInsertInstance(editingContext, _EOBudgetMouvCredit.ENTITY_NAME);
		eo.setBdmcMontant(bdmcMontant);
    eo.setExerciceRelationship(exercice);
    eo.setMouvementRelationship(mouvement);
    eo.setOrganRelationship(organ);
    eo.setTypeCreditRelationship(typeCredit);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }


	  public EOBudgetMouvCredit localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetMouvCredit)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetMouvCredit creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetMouvCredit creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOBudgetMouvCredit object = (EOBudgetMouvCredit)createAndInsertInstance(editingContext, _EOBudgetMouvCredit.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EOBudgetMouvCredit localInstanceIn(EOEditingContext editingContext, EOBudgetMouvCredit eo) {
    EOBudgetMouvCredit localInstance = (eo == null) ? null : (EOBudgetMouvCredit)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetMouvCredit#localInstanceIn a la place.
   */
	public static EOBudgetMouvCredit localInstanceOf(EOEditingContext editingContext, EOBudgetMouvCredit eo) {
		return EOBudgetMouvCredit.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EOBudgetMouvCredit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetMouvCredit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetMouvCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetMouvCredit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetMouvCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetMouvCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetMouvCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetMouvCredit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetMouvCredit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetMouvCredit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetMouvCredit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetMouvCredit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}
