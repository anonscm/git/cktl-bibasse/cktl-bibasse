

// EOOrgan.java
//

package org.cocktail.bibasse.server.metier;


import com.webobjects.foundation.NSValidation;

public class EOOrgan extends _EOOrgan
{
    public EOOrgan() {
        super();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    /**
     * Retourne true si cette ligne bdugetaire correspond a la nature indiquée.
     * @param typeNature nature a verifier.
     * @return true si cette ligne budgetaire est de la nature indiquée ; false sinon.
     */
    public boolean isTypeNature(String typeNature) {
    	if (typeNature == null) {
    		return false;
    	}
    	return toNatureBudget() != null && toNatureBudget().toTypeNatureBudget() != null
    			&& typeNature.equals(toNatureBudget().toTypeNatureBudget().tnbCode());
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();

    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {


    }

    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

    }

}
