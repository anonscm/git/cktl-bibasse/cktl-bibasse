/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import org.cocktail.bibasse.server.components.Main;
import org.cocktail.bibasse.server.print.ImpressionsCtrl;
import org.cocktail.fwkcktlacces.server.handler.FwkCktlResourcesInJarRequestHandler;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.foundation._NSUtilities;
import com.webobjects.jdbcadaptor.JDBCContext;
import com.woinject.WOInject;

public class Application extends ZApplication {

	public static final boolean SHOWSQLLOGS = false;

	private static final String CONFIG_TABLE_NAME = "FwkCktlWebApp_GrhumParametres";
	private static final int LOG_OUT_TAILLE_MAXI = 100000;

	private String temporaryDir;
	private String osName;
	private HashMap mySessions = new HashMap();

	private MyByteArrayOutputStream redirectedOutStream, redirectedErrStream; // Nouvelles sorties

	/**
	 *
	 */
	public static void main(String argv[]) {
//		ERXApplication.main(argv, Application.class);
		NSLegacyBundleMonkeyPatch.apply();
        WOInject.init("org.cocktail.bibasse.server.Application", argv);
	}


	/**
	 * Initialisation de l'application. S'execute une seule fois pour toutes les sessions
	 */
	public Application() {
		super();

		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
		   registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}
		registerRequestHandler(new FwkCktlResourcesInJarRequestHandler(),  "_wr_");
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		_NSUtilities.setClassForName(Main.class, "Main");
	}

	@Override
	public String configTableName() {
		return CONFIG_TABLE_NAME;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebApplication#getSessionTimeoutPage(com.webobjects.appserver.WOContext)
	 */
	public WOComponent getSessionTimeoutPage(WOContext arg0) {
		return null;
	}

	protected boolean initApplicationSpecial() {
		initTimeZones();

		ImpressionsCtrl.initTemporaryDir();

		// redirection des logs
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));

		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);

		if (showSqlLogs()) {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(-1);
		} else {
			System.out.println("Application.Application() - Affichage des logs SQL : inactif");
		}

		return true;
	}

	/**
	 *
	 * @return
	 */
	protected boolean showSqlLogs() {
		if (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS;
		}
		return config().valueForKey("SHOWSQLLOGS").equals("1");
	}

	public String outLogs() {
		return redirectedOutStream.toString();
	}

	public String errLogs() {
		return redirectedErrStream.toString();
	}

	/**
	 *
	 */
	public String mainModelName() {
		return "Bibasse";
	}

	public String version() {
		return VersionMe.appliVersion();
	}

	public String copyright() {
		return "@c";
	}

	/**
	 *
	 */
	public String bdConnexionName() {
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed(mainModelName());
		NSDictionary vDico = vModel.connectionDictionary();

		NSArray url = NSArray.componentsSeparatedByString((String) vDico.valueForKey("URL"), "@");

		System.out.println("URL de CONNEXION : " + (String) vDico.valueForKey("URL"));

		if (url.count() == 0) {
			return " Connexion ??";
		}

		if (url.count() == 1) {
			return (String) url.objectAtIndex(0);
		}

		return (String) (NSArray.componentsSeparatedByString((String) vDico.valueForKey("URL"), "@")).objectAtIndex(1);
	}

	/**
	 *
	 * @param model
	 * @return
	 */
	public String bdConnexionUrl(EOModel model) {
		NSDictionary vDico = model.connectionDictionary();
		String url = (String) vDico.valueForKey("URL");
		if (url == null) {

			System.out.println("ATTENTION : URL de connexion a la base de donnees nulle!!!!");

			url = "n/a";
		}
		return url;
	}

	/**
	 * Initialise le TimeZone à utiliser pour l'application.
	 */
	protected void initTimeZones() {
		System.out.println("Initialisation du NSTimeZone");
		System.out.println("NSTimeZone par defaut recupere sur le systeme (avant initialisation) : " + NSTimeZone.defaultTimeZone());

		String tz = (String) config().valueForKey("DEFAULT_TIME_ZONE");
		System.out.println("Application.initTimeZones() Time ZONE PARAMETRES : " + tz);

		if ((tz == null) || (tz.length() == 0)) {
			System.out.println("Le parametre DEFAULT_NS_TIMEZONE n'est pas defini dans le fichier .config.");
			TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
			NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
		} else {
			NSTimeZone ntz = NSTimeZone.timeZoneWithName(tz, false);
			if (ntz == null) {
				System.out.println("Le parametre DEFAULT_NS_TIMEZONE defini dans le fichier .config n'est pas valide (" + tz + ")");
				TimeZone.setDefault( TimeZone.getTimeZone("Europe/Paris" ) );
				NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false) );
			}
			else {
				TimeZone.setDefault( ntz);
				NSTimeZone.setDefaultTimeZone( ntz );
			}
		}

		System.out.println("NSTimeZone par defaut utilise dans l'application" + NSTimeZone.defaultTimeZone());
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		System.out.println("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone" + ntf.defaultParseTimeZone());
		System.out.println("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone" + ntf.defaultFormatTimeZone());
	}

	/**
	 * Recuperation d'un repertoire temporaraire ou ecrire les fichiers crees
	 *
	 * @return Retourne le chemin complet du repertoire temporaire.
	 */
	public void initForPlatform() {

		osName = System.getProperties().getProperty("os.name");
		System.out.println("OS NAME : " + osName);

		try {
			temporaryDir = System.getProperty("java.io.tmpdir");

			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
		} catch (Exception e) {
			System.out.println("Impossible de recuperer le repertoire temporaire !");
		}

		File tmpDir = new File(temporaryDir);
		if (!tmpDir.exists()) {
			System.out.println("Tentative de creation du repertoire temporaire " + tmpDir);
			try {
				tmpDir.mkdirs();
				System.out.println("Repertoire " + tmpDir + " cree.");
			} catch (Exception e) {
				System.out.println("Impossible de creer le repertoire " + tmpDir);
			}
		} else {
			System.out.println("Repertoire Temporaire : " + tmpDir);
		}
	}

	/**
	 *
	 * @return
	 */
	public String temporaryDir() {
		return temporaryDir;
	}

	/**
	 *
	 *
	 */
	public void cleanLogs() {
		redirectedErrStream.reset();
		redirectedOutStream.reset();

	}

	/**
	 *
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 * @return
	 */
	public boolean sendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) {
		return mailBus().sendMail(mailFrom, mailTo, "", mailSubject, mailBody);
	}

	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		public MyByteArrayOutputStream() {
			this(System.out);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}

		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);
			out.write(b, off, len);

			if (count > LOG_OUT_TAILLE_MAXI) {
				String logServerOut = redirectedOutStream.toString();
				logServerOut = logServerOut.substring((LOG_OUT_TAILLE_MAXI / 2), logServerOut.length());

				// On reaffecte cette valeur a la variable redirectOutStream
				redirectedOutStream.reset();
				try {
					redirectedOutStream.write(logServerOut.getBytes());
				} catch (Exception e) {
				}
			}

		}
	}

	protected String[] requiredParams() {
		return null;
	}

	protected String[] maquettesSix() {
		return null;
	}

	public String versionTxt() {
		return appCktlVersion().txtVersion();
	}

	public String fwkVersionTxt() {
		return null;
	}

	protected String fwkVersionNum() {
		return null;
	}

	protected String parametresTableName() {
		return configTableName();
	}

	/**
	 * @return
	 */
	public String getReportsLocation() {
		System.out.println("Application.getReportsLocation()");
		if ((config().stringForKey("REPORTS_LOCATION") != null) && (config().stringForKey("REPORTS_LOCATION").length() > 0)) {
			System.out.println("Application.getReportsLocation() LOCATION : " + config().stringForKey("REPORTS_LOCATION"));
			return config().stringForKey("REPORTS_LOCATION");
		}
		final String aPath = path().concat("/Contents/Resources/Reports/jasper/");
		return aPath;
	}

	public Connection getJDBCConnection() {
		return ((JDBCContext) getAdaptorContext()).connection();
	}

	public EODatabaseContext getDatabaseContext() {
		return CktlDataBus.databaseContext();
	}

	public EOAdaptorChannel getAdaptorChannel() {
		return getDatabaseContext().availableChannel().adaptorChannel();
	}

	/**
	 * Utile pour récupérer la connection vers la base de données.
	 *
	 * @return
	 */
	public EOAdaptorContext getAdaptorContext() {
		return getAdaptorChannel().adaptorContext();
	}

	public HashMap getMySessions() {
		return mySessions;
	}

	public void setMySessions(final HashMap mySessions) {
		this.mySessions = mySessions;
	}

	/**
	 * Ecrit la liste des utilisateurs connetces dans la console.
	 */
	public final void listConnectedUsers() {
		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final Session element = (Session) iter.next();
			System.out.println("Application.listConnectedUsers()" + element.getInfoConnectedUser());
		}

	}

	public String getTemporaryDir() {
		return temporaryDir;
	}

	public String getOsName() {
		return osName;
	}
}