package org.cocktail.bibasse.server;

import static org.cocktail.common.SessionConstants.KEY_ERRNO;
import static org.cocktail.common.SessionConstants.KEY_KO_CODE;
import static org.cocktail.common.SessionConstants.KEY_MSG;
import static org.cocktail.common.SessionConstants.KEY_OK_CODE;
import static org.cocktail.common.SessionConstants.KEY_PERS_ID;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import org.cocktail.bibasse.server.cofisup.CofisupAction;
import org.cocktail.bibasse.server.factory.SaisieBudgetCtrl;
import org.cocktail.bibasse.server.finder.Finder;
import org.cocktail.bibasse.server.metier.EOBudgetMouvements;
import org.cocktail.bibasse.server.metier.EOBudgetSaisie;
import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.bibasse.server.metier.EOPlanComptable;
import org.cocktail.bibasse.server.metier.EOTypeAction;
import org.cocktail.bibasse.server.metier.EOTypeCredit;
import org.cocktail.bibasse.server.metier.EOTypeSaisie;
import org.cocktail.bibasse.server.reports.DefaultReport;
import org.cocktail.bibasse.server.reports.DefaultReport.IDefaultReportListener;
import org.cocktail.bibasse.server.reports.ZAbstractReport;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.invocation.CktlClientInvocation;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class Session extends CktlWebSession {

	private static final String ADMIN_PASSWORD = "wxc";

	Application	myApp;

    private ZAbstractReport lastReport;
    private ZAbstractReport currentReport;
    private Exception lastPrintError;
    private PrintTaskThread printTaskThread;
    public CofisupAction cofisupAction;

    private String	login;

    // Cofisup attributes
    private ExecutorService executorService;
    private Map<String, Future<?>> tasksMap;

	public CktlClientInvocation cktlClientInvocation;

	public Session() {
		super();
		myApp = (Application) Application.application();
		cktlClientInvocation = new CktlClientInvocation(this);

		connectedUser.setDateConnection(DateCtrl.now());
        myApp.getMySessions().put(sessionID(), this);



        this.executorService = Executors.newSingleThreadExecutor();
        this.tasksMap = Collections.synchronizedMap(new HashMap<String, Future<?>>());

        this.cofisupAction = new CofisupAction();
	}


	/**
	 * Verification du login/mot de passe.
	 */
	public NSDictionary clientSideRequestCheckPassword(String login, String passwd) {

		CktlConfig cfg = myApp.config();

		if (cfg == null) {
			return null;
		}

		SAUTClient sautClient = new SAUTClient(cfg.stringForKey("SAUT_URL"));
		String alias = cfg.stringForKey("APP_ALIAS");
		java.util.Properties prop = SAUTClient.toProperties(sautClient.requestDecryptedUserInfo(login, passwd, alias));

		NSMutableDictionary myDico = new NSMutableDictionary();
		myDico.setObjectForKey(prop.getProperty(KEY_ERRNO), KEY_ERRNO);
		myDico.setObjectForKey(prop.getProperty(KEY_MSG), KEY_MSG);

		if (prop.getProperty(KEY_PERS_ID) != null) {
			myDico.setObjectForKey(prop.getProperty(KEY_PERS_ID), KEY_PERS_ID);
		}

		return myDico.immutableClone();
	}

	public NSDictionary clientSideRequestCheckPasswordDirectConnect(String login, String password) {
		NSMutableDictionary resultsDico = new NSMutableDictionary();

		EOCompte compte = findCompteForLogin(defaultEditingContext(), login);
		if (compte == null) {
			resultsDico.setObjectForKey(KEY_KO_CODE, KEY_ERRNO);
			resultsDico.setObjectForKey("Connexion Refusée.\nLogin inexistant dans la base de données !", KEY_MSG);
			return resultsDico;
		}

		if (!Boolean.TRUE.equals(checkPassword(password, compte.cptPasswd()))) {
			resultsDico.setObjectForKey(KEY_KO_CODE, KEY_ERRNO);
			resultsDico.setObjectForKey("Connexion Refusée.\nLe mot de passe saisi est erroné !", KEY_MSG);
			return resultsDico;
		}

		resultsDico.setObjectForKey(KEY_OK_CODE, KEY_ERRNO);
		resultsDico.setObjectForKey(compte.persId(), KEY_PERS_ID);

		return resultsDico;
	}

	public NSDictionary clientSideRequestCheckPasswordCAS(String login) {
		NSMutableDictionary resultsDico = new NSMutableDictionary();
		EOCompte compte = findCompteForLogin(defaultEditingContext(), login);
		if (compte == null) {
			resultsDico.setObjectForKey(KEY_KO_CODE, KEY_ERRNO);
			resultsDico.setObjectForKey("Login inexistant dans la base de données !", KEY_MSG);
			return resultsDico;
		}

		resultsDico.setObjectForKey(KEY_OK_CODE, KEY_ERRNO);
		resultsDico.setObjectForKey(compte.persId(), KEY_PERS_ID);

		return resultsDico;
	}

	/**
	 *
	 * @param ec
	 * @param login
	 * @return
	 */
	protected EOCompte findCompteForLogin(EOEditingContext ec, String login) {
		EOCompte compte = EOCompte.compteForLogin(ec, login);
		if (compte != null && compte.toVlans() != null && EOVlans.VLAN_P.equals(compte.toVlans().cVlan())) {
			return compte;
		}
		return null;
	}


	protected Boolean checkPassword(String crypted, String uncrypted) {
		if (crypted.equals(ADMIN_PASSWORD)) {
			return Boolean.TRUE;
		}
		return checkCryptedPassword(crypted, uncrypted);
	}


	private Boolean checkCryptedPassword(String passwd, String cryptedPasswd) {
		// Les mots de passe cryptés sont ils identiques
		if (!passwd.equals(cryptedPasswd))	{
			if (!CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_UNIX, passwd, cryptedPasswd))	{
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	public String clientSideRequestVersion() { return ((Application)WOApplication.application()).version();}
	public String clientSideRequestCopyright() { return ((Application)WOApplication.application()).copyright();}
	public String clientSideRequestBdConnexionName() { return ((Application)WOApplication.application()).bdConnexionName();}

	public void clientSideRequestCleanLogs() throws Exception	{
		myApp.cleanLogs();
	}

	public String clientSideRequestOutLog() throws Exception	{
		return myApp.outLogs();
	}

	public String clientSideRequestErrLog() throws Exception	{
		return myApp.errLogs();
	}

	/**
	 *
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public final NSArray clientSideRequestSqlQuery(final String sql) throws Exception {
		return clientSideRequestSqlQuery(sql, null);
	}

	/**
	 *
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public final NSArray clientSideRequestSqlQuery(final String sql, final NSArray keys) throws Exception {
		try {
			return EOUtilities.rawRowsForSQL(defaultEditingContext(), myApp.mainModelName(), sql, keys);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/** verifie le login/mot de passe avec CRIPassword */
	public void clientSideRequestSetLoginParametres(String setlogin, String setIp) {

		login = setlogin;
		connectedUser.setIp(setIp);

	}



	/**
	 * Permet d'envoyer un mail à partir du client.
	 *
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) throws Exception {
		try {
			if (!myApp.sendMail(mailFrom,mailTo,mailCC, mailSubject,mailBody)) {
				throw new Exception ("Erreur lors de l'envoi du mail.");
			}
		} catch (Throwable e) {
			throw new Exception (e.getMessage());
		}

	}

	/**
	 * Recuperation d'une cle primaire
	 *
	 * @param eo Objet pour lequel on veut recuperer la cle primaire
	 *
	 * @return Retourne un dictionnaire contenant les cles primaires et leur valeur
	 */
	public  NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) 	{
		NSDictionary myResult = EOUtilities.primaryKeyForObject(defaultEditingContext(),eo);
		return myResult;
	}

	/**
	 * Recuperation d'une cle primaire
	 *
	 * @param id GlobalID pour lequel on veut recuperer la cle primaire
	 *
	 * @return Retourne un dictionnaire contenant les cles primaires et leur valeur
	 */
	public  NSDictionary clientSideRequestPrimaryKeyForGlobalID (EOGlobalID id) 	{
		EOEditingContext editingContext = new EOEditingContext(defaultEditingContext().parentObjectStore());
		EOGenericRecord record = (EOGenericRecord)editingContext.faultForGlobalID(id, editingContext);

		NSDictionary myResult = EOUtilities.primaryKeyForObject(editingContext, record);
		return myResult;
	}


	/**
	 * Cloturer la repartition nature/lolf par type de credit
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestInitialiserNatureLolf(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey(EOBudgetSaisie.BDSA_ID_KEY), "01bdsaid");

		SaisieBudgetCtrl.initialiserNatureLolf(defaultEditingContext(), dicoProc);
	}

	/**
	 * Cloturer la repartition nature/lolf par type de credit
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestCloturerNatureLolf(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOTypeCredit typeCredit= (EOTypeCredit)parametres.objectForKey("EOTypeCredit");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey(EOBudgetSaisie.BDSA_ID_KEY), "01bdsaid");
		dicoProc.setObjectForKey((EOUtilities.primaryKeyForObject(defaultEditingContext(),typeCredit)).objectForKey(EOTypeCredit.TCD_ORDRE_KEY), "02tcdordre");

		SaisieBudgetCtrl.cloturerNatureLolf(defaultEditingContext(), dicoProc);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestConsoliderBudget(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		dicoProc.setObjectForKey(primaryKey, "bdsaid");

		SaisieBudgetCtrl.consoliderBudgetSaisie(defaultEditingContext(), dicoProc);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestConsoliderTousBudgets(NSDictionary parametres) throws Exception	{

		Number exercice = (Number)parametres.objectForKey("exercice");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey(exercice, "exeordre");

		SaisieBudgetCtrl.consoliderBudgetsVotes(defaultEditingContext(), dicoProc);
	}

	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestInitialiserBudgetDbm(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		dicoProc.setObjectForKey(primaryKey, "bdsaid");

		SaisieBudgetCtrl.initialiserBudgetDbm(defaultEditingContext(), dicoProc);
	}


	/**
	 * Consolidation du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestInitialiserBudgetReliquat(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		dicoProc.setObjectForKey(primaryKey, "bdsaid");

		SaisieBudgetCtrl.initialiserBudgetReliquat(defaultEditingContext(), dicoProc);
	}

	public void clientSideRequestInitialiserBudgetInitial(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		dicoProc.setObjectForKey(primaryKey, "bdsaid");

		SaisieBudgetCtrl.initialiserBudgetDbm(defaultEditingContext(), dicoProc);
	}


	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestVoterBudget(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		dicoProc.setObjectForKey(primaryKey, "bdsaid");

		SaisieBudgetCtrl.voterBudget(defaultEditingContext(), dicoProc);
	}


	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestRefuserBudget(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		dicoProc.setObjectForKey(primaryKey, "bdsaid");

		SaisieBudgetCtrl.refuserBudget(defaultEditingContext(), dicoProc);
	}


	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestChangerSaisieBudget(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOTypeSaisie typeSaisie = (EOTypeSaisie)parametres.objectForKey("EOTypeSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyTypeSaisie = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeSaisie)).objectForKey("tysaId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyTypeSaisie, "02tysaid");

		SaisieBudgetCtrl.changerSaisieBudget(defaultEditingContext(), dicoProc);
	}


	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestSupprimerSaisieBudget(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");

		NSMutableDictionary dicoProc = new NSMutableDictionary();
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.supprimerSaisieBudget(defaultEditingContext(), dicoProc);
	}

	/**
	 * Suppression d'un budget
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestSupprimerBudget(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number) (EOUtilities.primaryKeyForObject(
				defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		String libelleBudget = (String) budgetSaisie.bdsaLibelle();
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(libelleBudget, "02bdsalibelle");

		SaisieBudgetCtrl.supprimerBudget(defaultEditingContext(), dicoProc);
	}

	/**
	 * Gestion d'un mouvement : Ventilation ou Virement.
	 * Mise a jour du budget
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestTraiterMouvement(NSDictionary parametres) throws Exception	{

		EOBudgetMouvements mouvement = (EOBudgetMouvements)parametres.objectForKey("EOBudgetMouvement");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),mouvement)).objectForKey("bmouId");

		dicoProc.setObjectForKey(primaryKey, "bmouid");

		SaisieBudgetCtrl.traiterMouvement(defaultEditingContext(), dicoProc);
	}


	/**
	 * Annule un mouvement. Annulation du mouvement et des details du mouvement
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestAnnulerMouvement(NSDictionary parametres) throws Exception	{

		EOBudgetMouvements mouvement = (EOBudgetMouvements)parametres.objectForKey("EOBudgetMouvement");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKey = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),mouvement)).objectForKey("bmouId");

		dicoProc.setObjectForKey(primaryKey, "bmouid");

		SaisieBudgetCtrl.annulerMouvement(defaultEditingContext(), dicoProc);
	}



	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestDeverouillerLbud(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.deverouillerLbud(defaultEditingContext(), dicoProc);
	}


	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestValiderLbud(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.validerLbud(defaultEditingContext(), dicoProc);
	}

	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestViderBudgetOrgan(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.viderBudgetOrgan(defaultEditingContext(), dicoProc);
	}


	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestControlerLbud(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.controlerLbud(defaultEditingContext(), dicoProc);
	}

	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestCloturerLbud(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.cloturerLbud(defaultEditingContext(), dicoProc);

	}

	/**
	 * Vote du budget
	 * Mise a jour du budget pour les lignes budgetaires de niveau 0,1,2
	 *
	 * @param parametres parametres necessaires a l'execution de la procedure stockee
	 */
	public void clientSideRequestSetBudgetsVotesOrgan(NSDictionary parametres) throws Exception	{

		EOBudgetSaisie budgetSaisie = (EOBudgetSaisie)parametres.objectForKey("EOBudgetSaisie");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyBudget = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),budgetSaisie)).objectForKey("bdsaId");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		dicoProc.setObjectForKey(primaryKeyBudget, "01bdsaid");
		dicoProc.setObjectForKey(primaryKeyOrgan, "02orgid");

		SaisieBudgetCtrl.setBudgetsVotesOrgan(defaultEditingContext(), dicoProc);
	}




	public void clientSideRequestAddMasqueNature(NSDictionary parametres) throws Exception	{

		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");
		EOTypeCredit typeCredit = (EOTypeCredit)parametres.objectForKey("EOTypeCredit");
		EOPlanComptable planco = (EOPlanComptable)parametres.objectForKey("EOPlanComptable");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		Number primaryKeyTypeCredit = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeCredit)).objectForKey("tcdOrdre");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),exercice)).objectForKey("exeOrdre");

		dicoProc.setObjectForKey(primaryKeyTypeCredit, "01tcdordre");
		dicoProc.setObjectForKey(planco.pcoNum(), "02pconum");
		dicoProc.setObjectForKey(primaryKeyExercice, "03exeordre");

		SaisieBudgetCtrl.addMasqueNature(defaultEditingContext(), dicoProc);

	}


	public void clientSideRequestDupliquerMasque(NSDictionary parametres) throws Exception	{

		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		dicoProc.setObjectForKey(exercice.exeExercice(), "01exeordre");
		dicoProc.setObjectForKey(parametres.objectForKey("typeMasque"), "02typemasque");

		System.out.println("Session.clientSideRequestDupliquerMasque() DICO PROC : " + dicoProc);

		SaisieBudgetCtrl.dupliquerMasque(defaultEditingContext(), dicoProc);

	}


	public void clientSideRequestAddMasqueGestion(NSDictionary parametres) throws Exception	{

		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");
		EOTypeAction typeAction = (EOTypeAction)parametres.objectForKey("EOTypeAction");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		Number primaryKeyLolf = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeAction)).objectForKey("tyacId");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),exercice)).objectForKey("exeOrdre");

		dicoProc.setObjectForKey(primaryKeyLolf, "01lolfid");
		dicoProc.setObjectForKey(typeAction.tyacType(), "02lolftype");
		dicoProc.setObjectForKey(primaryKeyExercice, "03exeordre");

		System.out.println("Session.clientSideRequestAddMasqueGestion() DICO PROC : " + dicoProc);

		SaisieBudgetCtrl.addMasqueGestion(defaultEditingContext(), dicoProc);

	}


	/**
	 *
	 * @param parametres
	 * @throws Exception
	 */
	public void clientSideRequestDelMasqueGestion(NSDictionary parametres) throws Exception	{

		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");
		EOTypeAction typeAction = (EOTypeAction)parametres.objectForKey("EOTypeAction");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		Number primaryKeyLolf = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeAction)).objectForKey("tyacId");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),exercice)).objectForKey("exeOrdre");

		dicoProc.setObjectForKey(primaryKeyLolf, "01lolfid");
		dicoProc.setObjectForKey(primaryKeyExercice, "02exeordre");

		System.out.println("Session.clientSideRequestDelMasqueGestion() DICO PROC : " + dicoProc);

		SaisieBudgetCtrl.delMasqueGestion(defaultEditingContext(), dicoProc);

	}


	public void clientSideRequestDelMasqueNature(NSDictionary parametres) throws Exception	{

		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");
		EOTypeCredit typeCredit = (EOTypeCredit)parametres.objectForKey("EOTypeCredit");
		EOPlanComptable planco = (EOPlanComptable)parametres.objectForKey("EOPlanComptable");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();

		Number primaryKeyTypeCredit = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeCredit)).objectForKey("tcdOrdre");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),exercice)).objectForKey("exeOrdre");

		dicoProc.setObjectForKey(primaryKeyTypeCredit, "01tcdordre");
		dicoProc.setObjectForKey(planco.pcoNum(), "02pconum");
		dicoProc.setObjectForKey(primaryKeyExercice, "03exeordre");

		SaisieBudgetCtrl.delMasqueNature(defaultEditingContext(), dicoProc);

	}


/**
 *
 * @param parametres
 * @return
 * @throws Exception
 */
	public BigDecimal clientSideRequestGetDisponible(NSDictionary parametres) throws Exception	{

		EOTypeCredit typeCredit = (EOTypeCredit)parametres.objectForKey("EOTypeCredit");
		EOOrgan organ = (EOOrgan)parametres.objectForKey("EOOrgan");
		EOExercice exercice = (EOExercice)parametres.objectForKey("EOExercice");

		// Parametres de la procedure
		NSMutableDictionary dicoProc = new NSMutableDictionary();
		Number primaryKeyTypeCredit = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),typeCredit)).objectForKey("tcdOrdre");
		Number primaryKeyOrgan = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),organ)).objectForKey("orgId");
		Number primaryKeyExercice = (Number)(EOUtilities.primaryKeyForObject(defaultEditingContext(),exercice)).objectForKey("exeOrdre");
		dicoProc.setObjectForKey(primaryKeyOrgan, "01orgid");
		dicoProc.setObjectForKey(primaryKeyTypeCredit, "02tcdordre");
		dicoProc.setObjectForKey(primaryKeyExercice, "03exeordre");

		BigDecimal dispo = SaisieBudgetCtrl.getDisponible(defaultEditingContext(), dicoProc);

		return dispo;
	}

	/**
	 *
	 * @param paramKey
	 * @return
	 */
	public String clientSideRequestGetParam(String paramKey) {

		CktlConfig cfg = myApp.config();
		return cfg.stringForKey(paramKey);
	}





    public final class PrintTaskThread extends Thread {
        public final static int FORMATXLS = 0;
        public final static int FORMATPDF = 1;
        private final Map _params;
        private final String _sqlQuery;
        private final String _jasperFileName;
        private MyDefaultReportListener reportListener;
        private NSData pdf;
        private final int _printFormat;
        private final byte _modeBlank;

        /**
         *
         */
        public PrintTaskThread(String sqlQuery, final String jasperFileName, final Map parameters, final int printFormat, final Boolean printIfEmpty) {
            super();
            _params = parameters;
            _sqlQuery = sqlQuery;
            _jasperFileName = jasperFileName;
            _printFormat = printFormat;
            if (printIfEmpty.booleanValue()) {
                _modeBlank = JasperReport.WHEN_NO_DATA_TYPE_ALL_SECTIONS_NO_DETAIL;
            } else {
                _modeBlank = JasperReport.WHEN_NO_DATA_TYPE_NO_PAGES;
            }
        }

        /**
         * @throws JRException
         * @see java.lang.Thread#run()
         */
        public void run() {
            try {
                lastReport = null;
                pdf = null;
                reportListener = new MyDefaultReportListener();
                currentReport = DefaultReport.createDefaultReport(myApp.getJDBCConnection(), _sqlQuery, _jasperFileName, _params, getReportListener());

                currentReport.setWhenNoDataType(_modeBlank);
                currentReport.prepareDataSource();
                currentReport.printReport();

                lastReport = currentReport;

                Thread.yield();

                ByteArrayOutputStream s;

                switch (_printFormat) {
                case FORMATPDF:
                    s = lastReport.getPdfOutputStream();
                    break;

                case FORMATXLS:
                    s = lastReport.getXlsOutputStream();
                    break;

                default:
                    s = lastReport.getPdfOutputStream();
                    break;
                }

                pdf = new NSData(s.toByteArray());

            } catch (Exception e) {
                lastPrintError = e;
                e.printStackTrace();
            }
        }

        /**
         * @see java.lang.Thread#interrupt()
         */
        public void interrupt() {
            reportListener = null;
            super.interrupt();
            //            currentReport.setInterrupted(true);
        }

        public synchronized MyDefaultReportListener getReportListener() {
            return reportListener;
        }

        public NSData getPdf() {
            return pdf;
        }
    }


    public class MyDefaultReportListener implements IDefaultReportListener {
        private int _pageNum = -1;
        private int _pageCount = -1;
        private int _pageTotalCount = -1;
        private boolean dataSourceCreated = false;
        private boolean reportBuild = false;
        private boolean reportExported = false;

        public synchronized int getPageCount() {
            return _pageCount;
        }

        public synchronized int getPageTotalCount() {
            return _pageTotalCount;
        }

        public synchronized int getPageNum() {
            return _pageNum;
        }

        public synchronized boolean isDataSourceCreated() {
            return dataSourceCreated;
        }

        public synchronized boolean isReportExported() {
            return reportExported;
        }

        public synchronized boolean isReportBuild() {
            return reportBuild;
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.DefaultReport.IDefaultReportListener#afterDataSourceCreated()
         */
        public synchronized void afterDataSourceCreated() {
            dataSourceCreated = true;
            Thread.yield();
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.ReportFactory.IReportFactorylistener#afterReportBuild()
         */
        public synchronized void afterReportBuild(int pageCount) {
            //            getSessLog().trace("");
            reportBuild = true;
            _pageTotalCount = pageCount;
            Thread.yield();
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.ReportFactory.IReportFactorylistener#afterPageExport(int, int)
         */
        public synchronized void afterPageExport(int pageNum, int pageCount) {
            //            getSessLog().trace("");
            _pageCount = pageCount;
            _pageNum = pageNum;
            Thread.yield();
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.ReportFactory.IReportFactorylistener#afterReportExport()
         */
        public synchronized void afterReportExport() {
            //            getSessLog().trace("");
            reportExported = true;
            Thread.yield();
        }

    }



    public final void clientSideRequestPrintByThread(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId, Boolean printIfEmpty) throws Exception {
        try {
            System.out.println("Session.clientSideRequestPrint() DEBUT");
            //          ZAbstractReport currentReport=null;

            HashMap params = null;

//            Hashtable params = null;
            if (parametres != null) {
                params = new HashMap(parametres.hashtable());
                System.out.println("parametres : " + params);
            }

            printTaskThread = null;
            lastReport = null;
            lastPrintError = null;

            String jasperId = idreport;
            System.out.println("Session.clientSideRequestPrintByThread() JASPER ID : " + jasperId);
            if (customJasperId != null) {
                jasperId = customJasperId;
                System.out.println("Utilisation dun report personnalise :" + customJasperId);
            }

            final String realJasperFilName = getRealJasperFileName(jasperId);

            //Recupérer le chemin du repertoire du report
            final File f = new File(realJasperFilName);
            String rep = f.getParent();
            if (rep != null) {
                if (!rep.endsWith(File.separator)) {
                    rep = rep + File.separator;
                }
            }
            params.put("REP_BASE_PATH", rep);



            printTaskThread = new PrintTaskThread(sqlQuery, realJasperFilName, params,PrintTaskThread.FORMATPDF, printIfEmpty);

            printTaskThread.setDaemon(true);
            printTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

            //On lance la génération du report et on s'en va...
           // ZLogger.info(login + " : Tache d'impression lancee pour" + realJasperFilName + " ...");
            printTaskThread.start();

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }


    public final NSData clientSideRequestPrintDifferedGetPdf() throws Exception {
        if (lastPrintError != null) {
            throw lastPrintError;
        }
        Thread.yield();
        if (lastReport != null && printTaskThread != null) {
            return printTaskThread.getPdf();
        }
        return null;
    }

    public final NSDictionary clientSideRequestGetPrintProgression() throws Exception {
        if (printTaskThread == null || printTaskThread.getReportListener() == null) {
            throw new Exception("Pas de listener defini");
        }

        NSMutableDictionary dictionary = new NSMutableDictionary();
        dictionary.takeValueForKey(new Integer(printTaskThread.getReportListener().getPageCount()), "PAGE_COUNT");
        dictionary.takeValueForKey(new Integer(printTaskThread.getReportListener().getPageTotalCount()), "PAGE_TOTAL_COUNT");
        dictionary.takeValueForKey(new Integer(printTaskThread.getReportListener().getPageNum()), "PAGE_NUM");
        dictionary.takeValueForKey(new Boolean(printTaskThread.getReportListener().isDataSourceCreated()), "DATASOURCE_CREATED");
        dictionary.takeValueForKey(new Boolean(printTaskThread.getReportListener().isReportExported()), "REPORT_EXPORTED");
        dictionary.takeValueForKey(new Boolean(printTaskThread.getReportListener().isReportBuild()), "REPORT_BUILD");
        //	    getSessLog().trace(dictionary);
        return dictionary.immutableClone();
    }

    /**
     * On vérifie si le report est défini dans la table, sinon renvoie le chemin d'accés au
     * report fourni par défaut par l'application.
     * @param fn
     * @return
     */
    public final String getRealJasperFileName(String rname) {
        String loc = null;
        if (rname==null)
        	return loc;
        EOEnterpriseObject obj = Finder.fetchObject(defaultEditingContext(), "Report", "repId=%@", new NSArray(new Object[] { rname }), true);
        if (obj != null) {
            loc = (String) obj.valueForKey("repLocation");
            if (loc != null) {
              //  ZLogger.debug(login + " : Report " + rname + " defini dans la table REPORT : " + loc);
                return loc;
            }
        }

        if (loc == null) {
            System.out.println("Session.getRealJasperFileName() Report " + rname + " NON defini dans la table REPORT, utilisation du report par defaut");
            loc = myApp.getReportsLocation() + rname;
        }
        return loc;
    }


    public final void clientSideRequestPrintKillCurrentTask() {
        System.out.println("Interruption de la tache d'impression en cours... en fait elle n'est pas tuee comme on n'a pas de controle");

        if (printTaskThread != null && printTaskThread.isAlive() && !printTaskThread.isInterrupted()) {
            printTaskThread.interrupt();
            printTaskThread = null;
        }
    }

    private final ConnectedUser connectedUser = new ConnectedUser();


    public String getInfoConnectedUser() {
        return connectedUser.toString();
    }

    public NSArray clientSideRequestGetConnectedUsers() {
        NSMutableArray t = new NSMutableArray();
        for (Iterator iter = myApp.getMySessions().values().iterator(); iter.hasNext();) {
            Session element = (Session) iter.next();
            t.addObject(element.connectedUser.toNSDictionary());
        }
        return t;
    }

    /**
     *
     */
    public void terminate() {

        myApp.getMySessions().remove(sessionID());
        super.terminate();
    }


 /**
     * Represente un client connecte.
     */
    public final class ConnectedUser {
        public Date getDateConnection() {
            return dateConnection;
        }

        public void setDateConnection(Date dateConnection) {
            this.dateConnection = dateConnection;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getLogin() {
            return login;
        }

        private Date dateConnection;
        private Date dateLastHeartBeat;

        private String ip;
        private String sessionID;

        /**
         * @see java.lang.Object#toString()
         */
        public String toString() {
            return getLogin() + " connecte depuis le " + dateConnection + "(IP:" + getIp() + ")";
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public Date getDateLastHeartBeat() {
            return dateLastHeartBeat;
        }

        public void setDateLastHeartBeat(Date dateLastHeartBeat) {
            this.dateLastHeartBeat = dateLastHeartBeat;
        }

        public HashMap toHashMap() {
            HashMap t = new HashMap();
            t.put("dateConnection", dateConnection);
            t.put("sessionID", sessionID);
            t.put("dateLastHeartBeat", dateLastHeartBeat);
            t.put("ip", getIp());
            t.put("login", getLogin());
            return t;
        }

        public NSDictionary toNSDictionary() {
            NSMutableDictionary t = new NSMutableDictionary();
            t.takeValueForKey(dateConnection, "dateConnection");
            t.takeValueForKey(sessionID, "sessionID");
            t.takeValueForKey(dateLastHeartBeat, "dateLastHeartBeat");
            t.takeValueForKey(getIp(), "ip");
            t.takeValueForKey(getLogin(), "login");
            return t;
        }

    }

    public NSArray clientSideRequestStartJob(NSDictionary parametres) throws Exception {
    	return cofisupAction.clientStartJob(parametres);
    }

    public NSDictionary clientSideRequestInfosJob() throws Exception {
    	return cofisupAction.clientInfosJob();
    }

    public NSData clientSideRequestRetrieveFile(String jobId) throws Exception {
    	return cofisupAction.clientRetrieveFile(jobId);
    }

    public ExecutorService getExecutorService() {
    	return this.executorService;
    }


	public CofisupAction getCofisupAction() {
		return cofisupAction;
	}


	public void setCofisupAction(CofisupAction cofisupAction) {
		this.cofisupAction = cofisupAction;
	}

	public String clientSideRequestVersionRaw() {
		return VersionMe.appliVersion();
		//return VersionCommon.rawVersion();
		//return VersionCommon.VERSIONNUMMAJ + "." + VersionCommon.VERSIONNUMMIN + "." + VersionCommon.VERSIONNUMBUILD + "." + VersionCommon.VERSIONNUMPATCH;
	}

	public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		return (path.startsWith("session"));
	}

}