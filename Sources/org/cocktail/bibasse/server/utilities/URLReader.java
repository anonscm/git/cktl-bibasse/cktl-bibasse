package org.cocktail.bibasse.server.utilities;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class URLReader {
	
	public URLReader() {
	}
	
	public String getURLZip(String args) throws Exception {
		URL monZipURL = new URL(args);
		ZipInputStream monZipStream = new ZipInputStream(monZipURL.openStream());
		ZipEntry entries = monZipStream.getNextEntry();
		
		int size = (int)entries.getSize();
		byte [] data = new byte[size];
		int bytesRead = 0;
		
		while (bytesRead<size && bytesRead != -1)
		{bytesRead+=monZipStream.read(data,bytesRead,size-bytesRead);}
		
		return new String(data);
	}
}
