package org.cocktail.bibasse.server.utilities;

import java.util.Map;

/**
 * Remplace au sein d'une requete les variables par leur valeur.
 */
public class VariableResolver {

	public static final String PREFIX = ":";

	/**
	 * Produit une requete dont les variables ont été remplacées.
	 * @param sqlQuery requete SQL.
	 * @param variables cle / valeur a utiliseR.
	 * @return une requete dont les variables ont été remplacées.
	 */
	public String resolve(String sqlQuery, Map<String, String> variables) {
		String sqlQueryResolved = sqlQuery;
		for (String variable : variables.keySet()) {
			sqlQueryResolved = sqlQueryResolved.replaceAll(PREFIX + variable, variables.get(variable));
		}
		return sqlQueryResolved;
	}

}
