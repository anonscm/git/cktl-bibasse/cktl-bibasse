package org.cocktail.bibasse.server.utilities;
import java.util.Iterator;
import java.util.Map;

/**
 * Classe permettant d'écrire dans le log.
 * 
 * @author rprin
 */
public class ZLog {
	public static final boolean DEFAULTSHOWOUTPUT = true;
	public static final boolean DEFAULTSHOWTRACE = true;
	
	/**
	 * Longueur d'une ligne de séparation, par défaut 80 caract?res.
	 */	
	public int lenForSeparator = 80;
	
	/**
	 * Longueur par défaut de l'espace réservé à l'affichage de la clé lorsqu'on affiche une clé/valeur. 
	 */
	public int defaultKeyLength = 35;
	
	/**
	 * Indique si le log sera réellement affiché. 
	 * Ceci permet de le désactiver éventuellement au moment du passage en production.
	 */
	private boolean showOutput;
	
	/**
	 * Indique si les traces seront affichées. 	 
	 */	
	private boolean showTrace;
	
	public ZLog() {
		setShowOutput(DEFAULTSHOWOUTPUT);
		setShowTrace(DEFAULTSHOWTRACE);
	}

	public ZLog(String userName) {
		setShowOutput(DEFAULTSHOWOUTPUT);
		setShowTrace(DEFAULTSHOWTRACE);
	}
	
	/**
	 * Ajoute une ligne de séparation
	 * 
	 * @param sep
	 */
	public void appendSeparator(String sep) {
		if (sep==null) {
			sep="-";
		}
		directWriteln( extendWithChars( "", sep, lenForSeparator, false) );
	}
	
	/**
	 * Ajoute un titre (avec la premi?re lettre en majuscule), suivi d'une ligne de séparation.
	 * @param str
	 */
	public void appendTitle(String str) {	
		directWriteln("");
		directWriteln( capitalizedString(str) );
		appendSeparator(null);
	}	
	
	/**
	 * Ajoute un message précédé par "ATTENTION".
	 * 
	 * @param str
	 */
	public void appendWarning(String str) {	
		append(":-(   ATTENTION >>>>" + str);
	}	
	
	/**
	 * Ajoute un message précédé d'un smiley :-).
	 * 
	 * @param str
	 */
	public void appendSuccess(String str) {	
		append(":-)   " + str);
	}	
	
	/**
	 * Ajoute au log une paire clé = valeur.
	 * 
	 * @param key
	 * @param value
	 */
	public void appendKeyValue(String key, Object value) {	
		appendKeyValue(key, value, defaultKeyLength);	
	}	

	/**
	 * Ajoute au log une paire clé = valeur en spécifiant le nombre de caract?res à utiliser pour créer la colonne de la clé.
	 * 
	 * @param key
	 * @param value
	 * @param keyLength
	 */
	public void appendKeyValue(String key, Object value, int keyLength) {
		if (key.length()>keyLength) {
			keyLength = key.length(); 		
		}
		append(extendWithChars(key," ",keyLength,false) + " = " + value);
	}	
	
	/**
	 * Ajoute au log un titre entouré par des caractères étoiles.
	 * 
	 * @param str
	 */
	public void appendBigTitle(String str) {
		directWriteln("");	
		appendSeparator("*");
		int nb = lenForSeparator/2 - str.length()/2 -1;
		String sep="";
		if (nb> 0 ) {
			sep = extendWithChars("","*",nb,false);  	
		}
		directWriteln(sep + " " + str+ " " + sep); 
		appendSeparator("*");
		directWriteln("");	
	}

	/**
	 * 
	 * Ecrit dans le log une trace (avec n° de ligne du code source, type de l'Objet).
	 * 
	 * @param pVarName
	 * @param pObj
	 */
	public void trace(Object pObj) {
		if (this.showTrace) {
			StringBuffer vLine = new StringBuffer();
			 vLine.append("-> ") ;
			 vLine.append(getTime());
			 vLine = vLine.append("> ");
			 vLine = vLine.append(this.getCallMethod(0));
			 vLine = vLine.append(" >>> ");
			 if (pObj!=null) {            	
				 vLine = vLine.append(pObj.toString());
			 }
			 else {
				 vLine = vLine.append("null");
			 }
			 if (pObj!=null) {
				 vLine = vLine.append("("+pObj.getClass().getName()+")");
			 }
			writeln(vLine.toString());
		 }	
	}

	/**
	 * 
	 * Ecrit (si showtrace est à true) dans le log une trace (avec n° de ligne du code source, type de l'Objet).
	 * 
	 * @param pVarName
	 * @param pObj
	 */
	public void trace(String pVarName, Object pObj ) {
		if (this.showTrace) {
			StringBuffer vLine = new StringBuffer();
			 vLine.append("-> ") ;
			 vLine.append(getTime());
			 vLine = vLine.append("> ");
			 vLine = vLine.append(this.getCallMethod(0));
			 vLine = vLine.append(" >>> ");
			 if (pVarName != null) {
				 vLine = vLine.append(pVarName);
				 vLine = vLine.append( " = ");
			 }
			 if (pObj!=null) {            	
				 vLine = vLine.append(pObj.toString());
			 }
			 else {
				 vLine = vLine.append("null");
			 }
			 if (pObj!=null) {
				 vLine = vLine.append("("+pObj.getClass().getName()+")");
			 }
			writeln(vLine.toString());
		 }		
	}

	protected String getTime() {
		return java.text.DateFormat.getTimeInstance().format(new java.util.Date());
	}

	/**
	 * Ajoute une ligne au log, si showOutput est à true.
	 * @param string
	 */
	public void append(String string) {
			writeln(string);	
	}
	
	/**
	 * Ajoute une ligne au log, indépendamment de la valeur showOutput.
	 * @param string
	 */
	public void appendForceOutput(String string) {
		writeln(string, true);	
	}	

	/**
	 * Permet d'indique si le log sera réellement affiché dans la console.
	 * Ceci permet de le désactiver éventuellement au moment du passage en production.
	 */
	public void setShowOutput(boolean b) {
		showOutput = b;
	}

	/**
	 * Récupérer le nom de la méthode et la ligne dans le fichier qui a appelé le log
	 * 
	 * @param profondeur profondeur d'appel
	 * @return
	 */
	private String getCallMethod(int profondeur) {
		Throwable tmp = new Throwable();
		return tmp.getStackTrace()[profondeur+2].toString();
	}	

	/**
	 * @param b
	 */
	public void setShowTrace(boolean b) {
		showTrace = b;
	}
	
	/**
	 * Ecrit s dans le log, si showOutput est true.
	 * 
	 * @param s
	 */
	protected void writeln(String s) {
		writeln(s, showOutput);	
	}

	/**
	 * Ecrit s dans le log, si show est true.
	 * Dériver cette méthode pour modifier chacune des lignes.
	 * 
	 * @param s
	 */
	protected void  writeln(String s, boolean show) {
		if (show) {
			System.out.println(s);
		}
	}
	
	/**
	 * 
	 * Ecrit dans le log, indépendamment de showOutput.
	 * 
	 * @param s
	 */
	protected void directWriteln(String s) {
		System.out.println(s);
	}
	
	/**
	 * @return
	 */
	public boolean isShowOutput() {
		return showOutput;
	}

	/**
	 * @return
	 */
	public boolean isShowTrace() {
		return showTrace;
	}

	/**
	 * Ecrit une map dans le log. Parcourt toutes les paires clés/valeurs et les écrit dans le log.
	 * Utile pour afficher des NSDictionary. 
	 * 
	 * @param map
	 */
	public void appendMap(Map map) {
		String key;
		//Calculer la largeur max des clés
		int max = 0;
		for (Iterator iter = map.keySet().iterator(); iter.hasNext();) {
			key = iter.next().toString();
			if (key.length()>max) {
				max = key.length();
			}
		}		
		for (Iterator iter = map.keySet().iterator(); iter.hasNext();) {
			key = iter.next().toString();
			appendKeyValue(key, map.get(key).toString(),max);
		}		
	}

	private String extendWithChars(String s, String addChars, int length, boolean inFront) {
		for(;s.length() < length; s = ((inFront)?(addChars+s):(s+addChars)));
		return s;
	}
	private String capitalizedString(String aString) {
		if ("".equals(aString))
			return "";
		String debut = (aString.substring(0,1)).toUpperCase();
		String fin = (aString.substring(1,aString.length())).toLowerCase();
		return debut.concat(fin);
	}

}
