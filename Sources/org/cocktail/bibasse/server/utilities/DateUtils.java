package org.cocktail.bibasse.server.utilities;

import java.util.Date;

/**
 * Date utilitaires.
 */
public final class DateUtils {

	/**
	 * Constructeur privé.
	 */
	private DateUtils() {
	}

	/**
	 * @return la date actuelle.
	 */
	public static Date now() {
		return new Date();
	}

}
