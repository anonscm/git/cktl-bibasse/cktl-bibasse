/*
 * Copyright COCKTAIL, 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.server.finder;

import java.io.Serializable;

import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOJefyAdminTypeNatureBudget;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.bibasse.server.metier.EOOrganNatureBudget;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderOrgan implements Serializable {

	/** serial version UID. */
	private static final long serialVersionUID = 1L;

	private static final FinderOrgan INSTANCE = new FinderOrgan();

	/**
	 * @return singleton instance.
	 */
	public static FinderOrgan instance() {
		return INSTANCE;
	}

	public NSArray findOrgansByNatureBudget(EOEditingContext ec, EOExercice exercice, String natureBudget) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOOrgan.EXERCICE_KEY + " = %@",
				new NSArray(exercice)));

		String qualTypeNatureBudget = new StringBuilder()
			.append(EOOrgan.TO_NATURE_BUDGET_KEY).append(".")
			.append(EOOrganNatureBudget.TO_TYPE_NATURE_BUDGET_KEY).append(".")
			.append(EOJefyAdminTypeNatureBudget.TNB_CODE_KEY)
			.append(" = '").append(natureBudget).append("'")
			.toString();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(qualTypeNatureBudget,	null));

		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

}
