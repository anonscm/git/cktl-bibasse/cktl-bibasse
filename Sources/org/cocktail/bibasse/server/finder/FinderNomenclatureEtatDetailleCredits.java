package org.cocktail.bibasse.server.finder;

import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EONomenclatureEtatDetailleCredits;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Finder noimenclature etat détaillé des dépenses.
 */
public final class FinderNomenclatureEtatDetailleCredits {

	private static final FinderNomenclatureEtatDetailleCredits INSTANCE = new FinderNomenclatureEtatDetailleCredits();

	/**
	 * @return singleton.
	 */
	public static FinderNomenclatureEtatDetailleCredits instance() {
		return INSTANCE;
	}

	private FinderNomenclatureEtatDetailleCredits() {
	}

	/**
	 * Retourne les etats detaillées des dépenses pour l'exercice spécifié.
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @return les etats detaillées des dépenses pour l'exercice spécifié.
	 */
	public NSArray findAll(EOEditingContext ec, EOExercice exercice)	{
		EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(
				EONomenclatureEtatDetailleCredits.TO_EXERCICE_KEY + " = %@", new NSArray(exercice));

		return ec.objectsWithFetchSpecification(
				new EOFetchSpecification(EONomenclatureEtatDetailleCredits.ENTITY_NAME, qualExercice, null));
	}
}
