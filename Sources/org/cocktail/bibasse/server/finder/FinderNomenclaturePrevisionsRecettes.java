package org.cocktail.bibasse.server.finder;

import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EONomenclaturePrevisionsRecettes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Finder des nomenclatures de previsions de recette.
 */
public final class FinderNomenclaturePrevisionsRecettes {

	private static final FinderNomenclaturePrevisionsRecettes INSTANCE = new FinderNomenclaturePrevisionsRecettes();

	/**
	 * @return singleton.
	 */
	public static FinderNomenclaturePrevisionsRecettes instance() {
		return INSTANCE;
	}

	private FinderNomenclaturePrevisionsRecettes() {
	}

	/**
	 * @param ec editing context.
	 * @param exercice exercice.
	 * @return les nomenclatures de prévisions de recettes pour l'exercice spécifié.
	 */
	public NSArray findAll(EOEditingContext ec, EOExercice exercice)	{
		EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(
				EONomenclaturePrevisionsRecettes.TO_EXERCICE_KEY + " = %@", new NSArray(exercice));
		return ec.objectsWithFetchSpecification(
				new EOFetchSpecification(EONomenclaturePrevisionsRecettes.ENTITY_NAME, qualExercice, null));
	}
}
