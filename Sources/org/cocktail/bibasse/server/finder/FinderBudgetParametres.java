/*
 * Copyright COCKTAIL, 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.server.finder;

import org.cocktail.bibasse.server.metier.EOBudgetParametres;
import org.cocktail.bibasse.server.metier.EOExercice;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetParametres {

	public static EOFetchSpecification buildFetchSpecification(Number exercice, String key)	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat
				(EOBudgetParametres.EXERCICE_KEY + "." + EOExercice.EXE_ORDRE_KEY + " = %@",
				new NSArray(exercice)));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOBudgetParametres.BPAR_KEY_KEY + " = '" + key + "'", null));

		return new EOFetchSpecification(
				EOBudgetParametres.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
	}

	/**
	* Recuperation d'une valeur pour une cle donnee
	*
	* @param ec Editing context global de l'application
	* @param key Cle de la valeur a retourner.
	*/
	public static String getValue(EOEditingContext ec, Number exercice, String key)	{
		String parametreResult = null;
		try {
			NSArray params = ec.objectsWithFetchSpecification(buildFetchSpecification(exercice, key));

			if (params.count() == 0) {
				System.out.println("Parametre manquant : " + key + ", exercice : " + exercice);
				parametreResult = null;
			} else {
				parametreResult = ((EOBudgetParametres) params.objectAtIndex(0)).bparValue();
			}

			return parametreResult;
		} catch (Exception e) {
			System.err.println("FinderBudgetParametres - Parametre manquant : " + key + ", exercice : " + exercice + ". Cause : " + e);
			return null;
		}
	}
}
