package org.cocktail.bibasse.server.finder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderTypeCredit {

	public static FinderTypeCredit instance() {
		return INSTANCE;
	}

	private static final FinderTypeCredit INSTANCE = new FinderTypeCredit();

	private FinderTypeCredit() {
	}

	public List<EOTypeCredit> findTypesCredit(EOEditingContext ec, EOExercice exercice, String section, String type) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		NSArray mySort = new NSArray(new EOSortOrdering("tcdCode", EOSortOrdering.CompareAscending));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice = %@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdBudget != %@", new NSArray(EOTypeCredit.TYPE_BUDGET_RESERVE)));

		if (section != null) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdSect = %@", new NSArray(section)));
		}

		if (type != null) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("tcdType = %@", new NSArray(type)));
		}

		EOFetchSpecification fs = new EOFetchSpecification(EOTypeCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);

		return ec.objectsWithFetchSpecification(fs);
	}

	public List<String> findSections(EOEditingContext ec, EOExercice exercice, String section, String type) {
		List<EOTypeCredit> typesCredits = findTypesCredit(ec, exercice, section, type);
		Set<String> sections = new HashSet<String>();
		List<String> sectionsTriees = new ArrayList<String>();

		for (EOTypeCredit typeCredit : typesCredits) {
			sections.add(typeCredit.tcdSect());
		}

		sectionsTriees.addAll(sections);
		Collections.sort(sectionsTriees, new Comparator<String>() {
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});

		return sectionsTriees;
	}
}
