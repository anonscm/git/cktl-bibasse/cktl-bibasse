/*
 * Copyright COCKTAIL, 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.bibasse.server.finder;

import org.cocktail.fwkcktlwebapp.common.metier.EOGrhumParametres;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author cpinsard
 *
 */
public class Finder {

	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sortOrderings, boolean refresh)	{

		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}


	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String stringQualifier, NSArray sortOrderings, boolean refresh)	{

		try {

			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);

			EOFetchSpecification fs = new EOFetchSpecification(entityName, myQualifier, sortOrderings);
			fs.setRefreshesRefetchedObjects(refresh);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
	}


	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{

		try {
			EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
			fs.setRefreshesRefetchedObjects(true);
			return (EOEnterpriseObject)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 *
	 * @param ec
	 * @param entityName
	 * @param qualifier
	 * @param sort
	 * @return
	 */
	public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sort)	{

		EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, sort);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);

	}


		/**
		* Recuperation d'une valeur pour une cle donnee
		*
		* @param ec Editing context global de l'application
		* @param key Cle de la valeur a retourner.
	*/
		public static String getValueGrhumParametres(EOEditingContext ec, String key)	{
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("paramKey = %@", new NSArray(key));
			EOFetchSpecification fs = new EOFetchSpecification(EOGrhumParametres.ENTITY_NAME, qual, null);
			NSArray params = ec.objectsWithFetchSpecification(fs);
			fs.setRefreshesRefetchedObjects(true);
			try {return ((EOGrhumParametres) params.objectAtIndex(0)).paramValue();}
			catch (Exception e) {return null;}
		}


}
