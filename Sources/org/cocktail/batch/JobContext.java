package org.cocktail.batch;

import java.util.Set;

import org.cocktail.bibasse.server.metier.EOExercice;
import org.cocktail.bibasse.server.metier.EOOrgan;
import org.cocktail.common.cofisup.ETypeBudget;
import org.cocktail.common.cofisup.ETypeFichier;
import org.cocktail.common.cofisup.ETypeNatureBudget;
import org.cocktail.common.metier.Banque;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe decrivant le contexte d'execution d'un job.
 */
public class JobContext {

	private EOEditingContext ec;
	private String modelName;
	private ETypeFichier typeFichier;
	private ETypeNatureBudget natureBudget;
	private int sequence;
	private ETypeBudget typeBudget;
	private EOExercice exerciceEnCours;
	private EOExercice exercice;
	private Set<EOOrgan> organigrammes;
	private Banque banque;
	private String devise;

	public JobContext() {
	    super();
	}
	
	public EOEditingContext getEc() {
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
	}

	public ETypeFichier getTypeFichier() {
		return typeFichier;
	}

	public void setTypeFichier(ETypeFichier typeFichier) {
		this.typeFichier = typeFichier;
	}

	public ETypeNatureBudget getNatureBudget() {
		return natureBudget;
	}

	public void setNatureBudget(ETypeNatureBudget natureBudget) {
		this.natureBudget = natureBudget;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public ETypeBudget getTypeBudget() {
		return typeBudget;
	}

	public void setTypeBudget(ETypeBudget typeBudget) {
		this.typeBudget = typeBudget;
	}

	public EOExercice getExerciceEnCours() {
		return exerciceEnCours;
	}

	public void setExerciceEnCours(EOExercice exerciceEnCours) {
		this.exerciceEnCours = exerciceEnCours;
	}

	public EOExercice getExercice() {
		return exercice;
	}

	public void setExercice(EOExercice exercice) {
		this.exercice = exercice;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Set<EOOrgan> getOrganigrammes() {
		return organigrammes;
	}

	public void setOrganigrammes(Set<EOOrgan> organigrammes) {
		this.organigrammes = organigrammes;
	}

    public Banque getBanque() {
        return banque;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

}
