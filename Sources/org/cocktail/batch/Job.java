package org.cocktail.batch;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.batch.api.Step;
import org.cocktail.batch.exception.JobException;
import org.cocktail.batch.exception.StepException;

public class Job {

	private String id;
	private String filename;
	private List<Step> steps;
	private JobContext context;

	public Job(String id) {
		this.id = id;
		this.filename = null;
		this.steps = new ArrayList<Step>();
		this.context = new JobContext();
	}

	public final void execute() throws JobException {
		try {
			for (Step step : steps) {
				step.execute();
			}
		} catch (StepException se) {
			throw new JobException(se);
		}
	}

	public void addStep(Step step) {
		this.steps.add(step);
	}

	public JobContext getContext() {
		return context;
	}

	public void setContext(JobContext context) {
		this.context = context;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
