package org.cocktail.batch.exception;

public class StepException extends Exception {

	public StepException() {
		super();
	}

	public StepException(String message) {
		super(message);
	}

	public StepException(String message, Throwable cause) {
		super(message, cause);
	}

	public StepException(Throwable cause) {
		super(cause);
	}
}
