package org.cocktail.batch.exception;

public class ItemWriterException extends Exception {

	public ItemWriterException() {
		super();
	}

	public ItemWriterException(String message) {
		super(message);
	}

	public ItemWriterException(String message, Throwable cause) {
		super(message, cause);
	}

	public ItemWriterException(Throwable cause) {
		super(cause);
	}
}
