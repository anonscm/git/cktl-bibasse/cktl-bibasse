package org.cocktail.batch.exception;

public class ItemReaderException extends Exception {

	public ItemReaderException() {
		super();
	}

	public ItemReaderException(String message) {
		super(message);
	}

	public ItemReaderException(String message, Throwable cause) {
		super(message, cause);
	}

	public ItemReaderException(Throwable cause) {
		super(cause);
	}
}
