package org.cocktail.batch.item.file;

import java.io.IOException;
import java.io.Writer;

import org.cocktail.batch.JobContext;

public interface FlatFileFooterCallback {

	void writeFooter(JobContext context, Writer writer) throws IOException;

}
