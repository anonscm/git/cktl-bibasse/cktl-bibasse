package org.cocktail.batch.item.file;

import java.io.IOException;
import java.io.Writer;

import org.cocktail.batch.JobContext;

/**
 * Interface decrivant l'ecriture d'un entete dans un fichier plat.
 *
 * @author flagoueyte.
 *
 */
public interface FlatFileHeaderCallback {

	/**
	 * Ecrit un entete dans le flux fourni en parametre.
	 * @param context context du job.
	 * @param writer flux de sortie.
	 * @throws IOException en cas d'exception durant le traitement.
	 */
	void writeHeader(JobContext context, Writer writer) throws IOException;

}
