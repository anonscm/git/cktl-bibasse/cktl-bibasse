package org.cocktail.batch.item.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.api.LineAggregator;
import org.cocktail.batch.api.PassThroughLineAggregator;
import org.cocktail.batch.exception.ItemWriterException;

/**
 * Classe gérant l'ecriture dans un fichier plat.
 *
 * @param <T> type d'element à écrire.
 */
public class FlatFileItemWriter<T> implements ItemWriter<T> {

	private static final String DEFAULT_LINE_SEPARATOR = System.getProperty("line.separator");

	private File resource;
	private FlatFileHeaderCallback headerCallback;
	private FlatFileFooterCallback footerCallback;
	private String lineSeparator = DEFAULT_LINE_SEPARATOR;
	private boolean shouldDeleteIfExists = true;
	private String encoding = OutputState.DEFAULT_CHARSET;
	private OutputState state = null;
	private LineAggregator<T> lineAggregator;

	/**
	 * Constructeur par defaut.
	 */
	public FlatFileItemWriter() {
		this.lineAggregator = new PassThroughLineAggregator<T>();
	}

	public void open(JobContext context) throws ItemWriterException {
		if (!getOutputState().isInitialized()) {
			doOpen(context);
		}
	}

	public void close(JobContext context) throws ItemWriterException {
			if (getOutputState() != null) {
				try {
					if (footerCallback != null && state.outputBufferedWriter != null) {
						footerCallback.writeFooter(context, state.outputBufferedWriter);
						state.outputBufferedWriter.flush();
					}
				} catch (IOException ioe) {
					throw new ItemWriterException(ioe);
				} finally {
					try {
						state.close();
					} catch (IOException innerIoe) {
						System.out.println("Cofisup - FlatFileItemWiter - Impossible de fermer le flux.");
					} finally {
						state = null;
					}
				}
			}
	}

	public void write(JobContext context, List<? extends T> items) throws ItemWriterException {
		if (items == null) {
			return;
		}

		OutputState localState = getOutputState();

		if (!localState.isInitialized()) {
			throw new ItemWriterException("Writer must be open before it can be written to");
		}

		StringBuilder lines = new StringBuilder();
		int lineCount = 0;
		for (T item : items) {
			lines.append(getLineAggregator().aggregate(item) + lineSeparator);
			lineCount++;
		}

		try {
			localState.write(lines.toString());
			localState.linesWritten += lineCount;
		} catch (IOException ioe) {
			throw new ItemWriterException(ioe);
		}
	}

	public OutputState getOutputState() {
		if (state == null) {
			File file = getResource();
			state = new OutputState();
			state.setDeleteIfExists(shouldDeleteIfExists);
			state.setEncoding(encoding);
		}
		return state;
	}

	private void doOpen(JobContext context) throws ItemWriterException {
		OutputState outputState = getOutputState();
		try {
			outputState.initializeBufferedWriter();
		} catch (IOException ioe) {
			throw new ItemWriterException("Failed to initialize writer", ioe);
		}

		if (headerCallback != null) {
			try {
				headerCallback.writeHeader(context, outputState.outputBufferedWriter);
				outputState.write(lineSeparator);
			} catch (IOException e) {
				throw new ItemWriterException("Could not write headers.  The file may be corrupt.", e);
			}
		}
	}

	public FlatFileHeaderCallback getHeaderCallback() {
		return headerCallback;
	}

	public void setHeaderCallback(FlatFileHeaderCallback headerCallback) {
		this.headerCallback = headerCallback;
	}

	public FlatFileFooterCallback getFooterCallback() {
		return footerCallback;
	}

	public void setFooterCallback(FlatFileFooterCallback footerCallback) {
		this.footerCallback = footerCallback;
	}

	public String getLineSeparator() {
		return lineSeparator;
	}

	public void setLineSeparator(String lineSeparator) {
		this.lineSeparator = lineSeparator;
	}

	public File getResource() {
		return resource;
	}

	public void setResource(File resource) {
		this.resource = resource;
	}

	public LineAggregator<T> getLineAggregator() {
		return lineAggregator;
	}

	public void setLineAggregator(LineAggregator<T> lineAggregator) {
		this.lineAggregator = lineAggregator;
	}

	private class OutputState {
		private static final String DEFAULT_CHARSET = "UTF-8";

		Writer outputBufferedWriter;
		String encoding = DEFAULT_CHARSET;

		long linesWritten = 0;
		boolean shouldDeleteIfExists = true;
		boolean initialized = false;

		/**
		 * Close the open resource and reset counters.
		 * @throws IOException
		 */
		public void close() throws IOException {
			initialized = false;
			if (outputBufferedWriter != null) {
				outputBufferedWriter.close();
			}
		}

		/**
		 * @param line
		 * @throws IOException
		 */
		public void write(String line) throws IOException {
			if (!initialized) {
				initializeBufferedWriter();
			}

			outputBufferedWriter.write(line);
			outputBufferedWriter.flush();
		}

		/**
		 * Creates the buffered writer for the output file channel based on
		 * configuration information.
		 * @throws IOException
		 */
		private void initializeBufferedWriter() throws IOException {
			File file = getResource();
			outputBufferedWriter = getBufferedWriter(file, encoding);
			outputBufferedWriter.flush();
			linesWritten = 0;
			initialized = true;
		}

		public boolean isInitialized() {
			return initialized;
		}

		/**
		 * Returns the buffered writer opened to the beginning of the file
		 * specified by the absolute path name contained in absoluteFileName.
		 * @throws IOException
		 */
		private Writer getBufferedWriter(File file, String encoding) throws IOException {
			try {
				return new BufferedWriter(new FileWriter(file));
			} catch (UnsupportedCharsetException ucse) {
				throw new IOException("Bad encoding configuration for output file " + file + " / Cause : " + ucse.getMessage());
			} catch (FileNotFoundException e) {
				throw new IOException();
			}
		}

		/**
		 * @param shouldDeleteIfExists
		 */
		public void setDeleteIfExists(boolean shouldDeleteIfExists) {
			this.shouldDeleteIfExists = shouldDeleteIfExists;
		}

		/**
		 * @param encoding
		 */
		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}

	}
}
