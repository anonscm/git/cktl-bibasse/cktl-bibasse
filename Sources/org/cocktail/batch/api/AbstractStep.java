package org.cocktail.batch.api;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.exception.ItemProcessingException;
import org.cocktail.batch.exception.ItemReaderException;
import org.cocktail.batch.exception.ItemWriterException;
import org.cocktail.batch.exception.StepException;

/**
 * Classe abstraite fournissant les operations / workflow de base d'une etape d'un job.
 * @author flagoueyte.
 *
 * @param <K>
 * @param <V>
 */
public abstract class AbstractStep<K, V> implements Step {

	private JobContext jobContext;
	private ItemReader<K> reader;
	private ItemWriter<V> writer;
	private ItemProcessor<K, V> processor;

	/**
	 * Constructeur.
	 * @param jobContext context du job.
	 * @param reader flux de lecture associé a l'etape.
	 * @param writer flux d'ecriture associé à l'etape.
	 * @param processor composant de transfomration des elements lus avant ecriture.
	 */
	public AbstractStep(JobContext jobContext, ItemReader<K> reader, ItemWriter<V> writer, ItemProcessor<K, V> processor) {
		this.jobContext = jobContext;
		this.reader = reader;
		this.writer = writer;
		this.processor = processor;
	}

	/**
	 * {@inheritDoc}
	 */
	public void execute() throws StepException {
		beforeExecute(reader, writer);
		doExecute(reader, writer);
		afterExecute(reader, writer);
	}

	protected void doExecute(ItemReader<K> reader, ItemWriter<V> writer) throws StepException {
		try {

			List<V> processedItems = new ArrayList<V>();
			K item = reader.read(jobContext);
			while (item != null) {
				processedItems.add(processor.process(item));
				item = reader.read(jobContext);
			}
			writer.write(jobContext, processedItems);

		} catch (ItemReaderException ire) {
			throw new StepException(ire);
		} catch (ItemProcessingException ipe) {
			throw new StepException(ipe);
		} catch (ItemWriterException iwe) {
			throw new StepException(iwe);
		}
	}

	/**
	 * Operations a effectuer AVANT le debut du traitement.
	 * @param reader flux entree des elements.
	 * @param writer flux de sortie des elements.
	 * @throws StepException en cas d'exception durant les operations.
	 */
	public abstract void beforeExecute(ItemReader<K> reader, ItemWriter<V> writer) throws StepException;

	/**
	 * Operations a effectuer APRES le debut du traitement.
	 * @param reader flux entree des elements.
	 * @param writer flux de sortie des elements.
	 * @throws StepException en cas d'exception durant les operations.
	 */
	public abstract void afterExecute(ItemReader<K> reader, ItemWriter<V> writer) throws StepException;

	public ItemProcessor<K, V> getProcessor() {
		return processor;
	}

	public void setProcessor(ItemProcessor<K, V> processor) {
		this.processor = processor;
	}

	public JobContext getJobContext() {
		return jobContext;
	}

	public void setJobContext(JobContext jobContext) {
		this.jobContext = jobContext;
	}

	public ItemReader<K> getReader() {
		return reader;
	}

	public void setReader(ItemReader<K> reader) {
		this.reader = reader;
	}

	public ItemWriter<V> getWriter() {
		return writer;
	}

	public void setWriter(ItemWriter<V> writer) {
		this.writer = writer;
	}
}
