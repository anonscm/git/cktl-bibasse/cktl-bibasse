package org.cocktail.batch.api;

import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.exception.ItemWriterException;

/**
 *
 * Interface d'ecriture d'elements.
 *
 * @author flagoueyte.
 *
 * @param <T> type d'elements a ecrire.
 */
public interface ItemWriter<T> {

	/**
	 * Ouverture du flux de sortie.
	 * @param context context du job.
	 * @throws ItemWriterException en cas d'exception durant le traitement.
	 */
	void open(JobContext context) throws ItemWriterException;

	/**
	 * Ecriture des elements.
	 * @param context context du job.
	 * @param items liste d'elements a ecrire.
	 * @throws ItemWriterException en cas d'exception durant le traitement.
	 */
	void write(JobContext context, List<? extends T> items) throws ItemWriterException;

	/**
	 * Fermeture du flux de sortie.
	 * @param context context du job.
	 * @throws ItemWriterException en cas d'exception durant le traitement.
	 */
	void close(JobContext context) throws ItemWriterException;
}

