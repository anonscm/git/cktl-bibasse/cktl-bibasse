package org.cocktail.batch.api;

public class PassThroughLineAggregator<T> implements LineAggregator<T> {

	public String aggregate(T item) {
		return item.toString();
	}

}
