package org.cocktail.batch.api;

import org.cocktail.batch.exception.StepException;

/**
 * Interface d'une etape d'un job.
 *
 * @author flagoueyte.
 *
 */
public interface Step {

	/**
	 * Une etape consiste d'un traitement.
	 * @throws StepException en cas d'exception durant le traitement.
	 */
	 void execute() throws StepException;

}
