package org.cocktail.batch.api;

import java.util.Iterator;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.exception.ItemReaderException;

public abstract class AbstractItemReader<T> implements ItemReader<T> {

	private Iterator<T> itemIterator;
	private EStatus status;

	/**
	 * Constructeur.
	 */
	public AbstractItemReader() {
		this.itemIterator = null;
		this.status = EStatus.READY;
	}

	/**
	 * {@inheritDoc}
	 */
	public T read(JobContext context) throws ItemReaderException {
		if (getStatus().isReady()) {
			setItemIterator(itemsIterator(context));
			status = EStatus.PENDING;
		}

		T item = null;
		if (getItemIterator().hasNext()) {
			item = getItemIterator().next();
		}
		return item;
	}

	/* Abstract methods */
	public abstract Iterator<T> itemsIterator(JobContext context);

	/* Getter & Setter */
	public EStatus getStatus() {
		return status;
	}

	public Iterator<T> getItemIterator() {
		return itemIterator;
	}

	public void setItemIterator(Iterator<T> itemIterator) {
		this.itemIterator = itemIterator;
	}
}
