package org.cocktail.batch.api;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.exception.ItemReaderException;

/**
 * Interface de lecture d'elements (fichier, base, jms etc).
 * @author flagoueyte
 *
 * @param <T> type de l'element lu.
 */
public interface ItemReader<T> {

	/**
	 * Lecture d'un element.
	 * @param context contexte du job.
	 * @return element lu.
	 * @throws ItemReaderException en cas d'exception pendant l'operation de lecture.
	 */
	T read(JobContext context) throws ItemReaderException;
}
