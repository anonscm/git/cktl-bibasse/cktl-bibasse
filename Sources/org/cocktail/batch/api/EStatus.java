package org.cocktail.batch.api;

public enum EStatus {

	READY,
	PENDING,
	COMPLETED;

	public boolean isComplete() {
		return COMPLETED.equals(this);
	}

	public boolean isPending() {
		return PENDING.equals(this);
	}

	public boolean isReady() {
		return READY.equals(this);
	}
}
