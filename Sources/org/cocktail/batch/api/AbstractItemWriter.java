package org.cocktail.batch.api;

import java.util.List;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.exception.ItemWriterException;

/**
 * Implementation abstraite d'ecriture d'un flux.
 * Elle ajoute
 *
 * @author flagoueyte.
 *
 * @param <T>
 */
public abstract class AbstractItemWriter<T> implements ItemWriter<T> {

	/**
	 * No op.
	 * @param context context du job.
	 * @throws ItemWriterException en cas d'exception durant le traitement.
	 */
	public void open(JobContext context) throws ItemWriterException {
	}

	/**
	 * No op.
	 * @param context context du job.
	 * @throws ItemWriterException en cas d'exception durant le traitement.
	 */
	public void close(JobContext context) throws ItemWriterException {
	}

	/**
	 * {@inheritDoc}
	 */
	public abstract void write(JobContext context, List<? extends T> items) throws ItemWriterException;

}
