package org.cocktail.batch.api.step;

import org.cocktail.batch.JobContext;
import org.cocktail.batch.api.AbstractStep;
import org.cocktail.batch.api.ItemProcessor;
import org.cocktail.batch.api.ItemReader;
import org.cocktail.batch.api.ItemWriter;
import org.cocktail.batch.exception.ItemWriterException;
import org.cocktail.batch.exception.StepException;

/**
 *
 * Classe abstraite decrivant une etape composé du workflow suivant :
 * - ouverture du flux entree
 * - traitement
 * - ecriture puis fermeture du flux.
 *
 * Classe a privilégier.
 *
 * @author flagoueyte.
 *
 * @param <K>
 * @param <V>
 */
public class SimpleStep<K, V> extends AbstractStep<K, V> {

	/**
	 * Constructeur.
	 * @param jobContext context du job.
	 * @param reader flux de lecture associé a l'etape.
	 * @param writer flux d'ecriture associé à l'etape.
	 * @param processor composant de transfomration des elements lus avant ecriture.
	 */
	public SimpleStep(JobContext jobContext, ItemReader<K> reader,	ItemWriter<V> writer, ItemProcessor<K, V> processor) {
		super(jobContext, reader, writer, processor);
	}

	@Override
	public void beforeExecute(ItemReader<K> reader, ItemWriter<V> writer) throws StepException {
		try {
			writer.open(getJobContext());
		} catch (ItemWriterException iwe) {
			throw new StepException(iwe);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void afterExecute(ItemReader<K> reader, ItemWriter<V> writer) throws StepException {
		try {
			writer.close(getJobContext());
		} catch (ItemWriterException iwe) {
			throw new StepException(iwe);
		}
	}
}
