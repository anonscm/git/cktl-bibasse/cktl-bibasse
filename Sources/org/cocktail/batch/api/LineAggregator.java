package org.cocktail.batch.api;

public interface LineAggregator<T> {

	String	aggregate(T item);

}
