package org.cocktail.batch.api;

import org.cocktail.bibasse.server.cofisup.batch.ICofisupBean;
import org.cocktail.common.metier.Banque;

public class CofisupBeanLineAgreggator<T extends ICofisupBean> implements LineAggregator<T> {

    private String devise;
    private Banque banque;
    
    public CofisupBeanLineAgreggator(String devise, Banque banque) {
        super();
        this.devise = devise;
        this.banque = banque;
    }

    public String aggregate(ICofisupBean item) {
        return item.convertToString(devise, banque);
    }

}
