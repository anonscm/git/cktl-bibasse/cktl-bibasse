package org.cocktail.batch.api;

import org.cocktail.batch.exception.ItemProcessingException;

/**
 * Interface decrivant le traitement de transformation d'un element vers un autre.
 * @author flagoueyte.
 *
 * @param <T> type element entree a covnertir.
 * @param <R> type element de sortie converti.
 */
public interface ItemProcessor<T, R> {

	/**
	 * Transforme item de type T en element de type R.
	 * @param item item a convertir.
	 * @return item converti.
	 * @throws ItemProcessingException en cas d'erreur lors de la transformation.
	 */
	R process(T item) throws ItemProcessingException;

}
