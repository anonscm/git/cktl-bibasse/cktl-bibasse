package org.cocktail.common.metier;

public interface IJefyAdminParametre {

    String ENTITY_NAME = "JefyAdminParametre";
    String PAR_KEY_KEY = "parKey";
    String PAR_VALUE_KEY = "parValue";
    String EXE_ORDRE_KEY = "exeOrdre";
    
    String PARAM_DEVISE_DEV_CODE = "DEVISE_DEV_CODE";
    String PARAM_DEVISE_TAUX = "DEVISE_TAUX";
    
}
