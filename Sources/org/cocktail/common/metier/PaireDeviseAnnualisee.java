package org.cocktail.common.metier;

public class PaireDeviseAnnualisee {

    private String deviseFrom;
    private String deviseTo;
    private Integer exercice;
    
    public PaireDeviseAnnualisee(String deviseFrom, String deviseTo, Integer exercice) {
        this.deviseFrom = deviseFrom;
        this.deviseTo = deviseTo;
        this.exercice = exercice;
    }

    public String getDeviseFrom() {
        return deviseFrom;
    }

    public void setDeviseFrom(String deviseFrom) {
        this.deviseFrom = deviseFrom;
    }

    public String getDeviseTo() {
        return deviseTo;
    }

    public void setDeviseTo(String deviseTo) {
        this.deviseTo = deviseTo;
    }

    public Integer getExercice() {
        return exercice;
    }

    public void setExercice(Integer exercice) {
        this.exercice = exercice;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((deviseFrom == null) ? 0 : deviseFrom.hashCode());
        result = prime * result + ((deviseTo == null) ? 0 : deviseTo.hashCode());
        result = prime * result + ((exercice == null) ? 0 : exercice.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        PaireDeviseAnnualisee other = (PaireDeviseAnnualisee) obj;
        if (deviseFrom == null) {
            if (other.deviseFrom != null) {
                return false;
            }
        } else if (!deviseFrom.equals(other.deviseFrom)) {
            return false;
        }
        if (deviseTo == null) {
            if (other.deviseTo != null) {
                return false;
            }
        } else if (!deviseTo.equals(other.deviseTo)) {
            return false;
        }
        
        if (exercice == null) {
            if (other.exercice != null) {
                return false;
            }
        } else if (!exercice.equals(other.exercice)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "PaireDeviseAnnualisee [deviseFrom=" + deviseFrom + ", deviseTo=" + deviseTo + ", exercice=" + exercice + "]";
    }
}
