package org.cocktail.common.metier;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MoneyFormatter {

    private DecimalFormat format;
    private int scale;
    private RoundingMode roundingMode;
    
    public MoneyFormatter(DecimalFormat format, int scale, RoundingMode roundingMode) {
        super();
        this.format = format;
        this.scale = scale;
        this.roundingMode = roundingMode;
    }

    public DecimalFormat getFormat() {
        return format;
    }

    public int getScale() {
        return scale;
    }

    public RoundingMode getRoundingMode() {
        return roundingMode;
    }
    
}
