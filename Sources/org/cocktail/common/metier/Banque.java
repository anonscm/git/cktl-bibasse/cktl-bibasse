package org.cocktail.common.metier;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Banque {

    public static final String EURO_DEVISE = "EUR";
    
    private Map<PaireDeviseAnnualisee, BigDecimal> tauxChange;
    
    public Banque() {
        this.tauxChange = new HashMap<PaireDeviseAnnualisee, BigDecimal>();
    }
    
    public BigDecimal recupererTauxChange(String from, String to, Integer exercice) {
        if (from.equals(to)) {
            return BigDecimal.ONE; 
        }
        
        PaireDeviseAnnualisee paire = new PaireDeviseAnnualisee(from, to, exercice);
        return tauxChange.get(paire);
    }
    
    public void ajouterTauxChange(String from, String to, Integer exercice, BigDecimal taux) {
        this.tauxChange.put(new PaireDeviseAnnualisee(from, to, exercice), taux);
    }
    
    public Money convertTo(Money from, String deviseTo, Integer exercice) {
        BigDecimal taux = recupererTauxChange(from.getDevise(), deviseTo, exercice);
        BigDecimal montantConverti = from.getMontant().multiply(taux);
        return new Money(deviseTo, montantConverti);
    }
}
