package org.cocktail.common.metier;

import java.math.BigDecimal;

public class Money {

    private String devise;
    private BigDecimal montant;
    
    public Money(String monnaie, BigDecimal montant) {
        this.devise = monnaie;
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String monnaie) {
        this.devise = monnaie;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }
    
    public Money convertTo(Banque banque, String deviseTo, Integer exercice) {
        return banque.convertTo(this, deviseTo, exercice);
    }
    
    public String format(MoneyFormatter formatter) {
        if (formatter == null) {
            return "";
        }
        return formatter.getFormat().format(getMontant().setScale(formatter.getScale(), formatter.getRoundingMode()));
    }
    
    public Money setScale(MoneyFormatter formatter) {
        if (formatter == null) {
            return new Money(devise, montant);
        }
        
        return new Money(devise, montant.setScale(formatter.getScale(), formatter.getRoundingMode()));
    }

    @Override
    public String toString() {
        return "Money [devise=" + devise + ", montant=" + montant + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((devise == null) ? 0 : devise.hashCode());
        result = prime * result + ((montant == null) ? 0 : montant.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Money other = (Money) obj;
        if (devise == null) {
            if (other.devise != null) {
                return false;
            }
        } else if (!devise.equals(other.devise)) {
            return false;
        }
        if (montant == null) {
            if (other.montant != null) {
                return false;
            }
        } else if (!montant.equals(other.montant)) {
            return false;
        }
        return true;
    }
}
