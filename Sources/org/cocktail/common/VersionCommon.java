package org.cocktail.common;

import java.io.Serializable;

/**
 * Definit la version de maniere commune au client et au serveur.
 *
 */
@Deprecated
public final class VersionCommon implements Serializable {

	/** Serial verison Id. */
	private static final long serialVersionUID = 1L;

	public static final int VERSIONNUMMAJ   = 1;
	public static final int VERSIONNUMMIN   = 3;
	public static final int VERSIONNUMPATCH = 4;
	public static final int VERSIONNUMBUILD = 0;

	public static final String VERSIONNUM = VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH;
	public static final String VERSIONDATE = "24/10/2013";

	/**
	 * Constructeur privé.
	 */
	private VersionCommon() {
	}

	/**
	 * Numero de version.
	 * @return version.
	 */
	public static String rawVersion() {
		return VERSIONNUM;
	}

	/**
	 * Affichage de la version depuis la ligne de commande.
	 * @param args arguments.
	 */
	public static void main(String[] args) {
		System.out.println(rawVersion());
	}
}
