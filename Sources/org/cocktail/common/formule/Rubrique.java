package org.cocktail.common.formule;

import java.math.BigDecimal;

import org.mvel2.MVEL;

/**
 * Une rubrique est définit par un code identificateur ainsi qu'une expression.
 * L'expression fait intervenir d'autres rubriques et/ou des comptes.
 *
 * @author flagoueyte
 *
 */
public class Rubrique extends AbstractElementNomenclature {

	private String expression;

	/**
	 * Constructeur.
	 * @param code code de la rubrique.
	 * @param expression expression (c12 + MSDe par exemple).
	 */
	public Rubrique(String code, String expression) {
		super(code);
		this.expression = expression;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	/**
	 * Retourne le montant de cette rubrique en evaluant son expression.
	 * @return le montant de cette rubrique.
	 */
	public BigDecimal getMontant() {
		BigDecimal resultat = MVEL.eval(expression, getContext(), BigDecimal.class);
		if (resultat == null) {
			return BigDecimal.ZERO;
		}
		return resultat;
	}

	@Override
	public String toString() {
		return "code: " + getCode() + " | expression: " + getExpression();
	}
}
