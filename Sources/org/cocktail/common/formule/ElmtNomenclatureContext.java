package org.cocktail.common.formule;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.mvel2.integration.PropertyHandlerFactory;

/**
 * Definit un contexte de nomenclature.
 *
 * @author flagoueyte
 *
 */
public class ElmtNomenclatureContext {

	private Map<String, IElementNomenclature> context;

	/**
	 * Constructeur.
	 */
	public ElmtNomenclatureContext() {
		this.context = new HashMap<String, IElementNomenclature>();
		PropertyHandlerFactory.registerPropertyHandler(getClass(), new ElmtNomenclatureHandler());
	}

	/**
	 * Clés du contexte.
	 * @return cles du contexte.
	 */
	public Set<String> keySet() {
		return context.keySet();
	}

	/**
	 * Retourne l'element de nomenclature du contexte identifié par son code.
	 * Si le code n'existe pas un Compte d'un montant égal à 0 est retourné pour le code fourni.
	 * @param code code de l'element cherché.
	 * @return element de nomenclature du contexte identifié par son code.
	 */
	public IElementNomenclature get(String code) {
		String lowerCode = code.toLowerCase();
		IElementNomenclature elementNomenclature = null;
		if (context.containsKey(lowerCode)) {
			elementNomenclature = (IElementNomenclature) context.get(lowerCode);
		} else {
			elementNomenclature = new Compte(lowerCode, 0.0d);
			elementNomenclature.setContext(this);
		}
		return elementNomenclature;
	}

	/**
	 * Ajoute un element de nomenclature au contexte.
	 * @param code code de l'element.
	 * @param elmtNomenclature element.
	 * @return l'element ajouté.
	 */
	public IElementNomenclature put(String code, IElementNomenclature elmtNomenclature) {
		elmtNomenclature.setContext(this);
		return this.context.put(code.toLowerCase(), elmtNomenclature);
	}

	/**
	 * Retourne le montant pour le code demande.
	 * @param code code fourni.
	 * @return montant associé au code.
	 */
	public BigDecimal resolve(String code) {
		return get(code).getMontant();
	}

}
