package org.cocktail.common.formule;

import java.math.BigDecimal;


/**
 * Represente un compte du plan comptable.
 * Le code est identifié par "c" suivi du numéro du compte.
 *
 * @author flagoueyte
 *
 */
public class Compte extends AbstractElementNomenclature {

	private BigDecimal montantPropre;

	/**
	 * Constructeur.
	 * @param code numero de compte.
	 * @param montant montant propre du compte.
	 */
	public Compte(String code, BigDecimal montant) {
		super(code);
		this.montantPropre = montant;
	}

	/**
	 * Constructeur.
	 * @param code numero de compte.
	 * @param montant montant propre du compte.
	 */
	public Compte(String code, double montant) {
		this(code, BigDecimal.valueOf(montant));
	}

	public BigDecimal getMontant() {
		return montantPropre.add(getSommeMontantsDesSousComptes(getContext()));
	}

	/**
	 * Retourne le montant de l'ensemble des sous-compte de ce compte.
	 * Le montant propre du compte n'est pas inclu.
	 * @param context contexte contenant l'ensemble des comptes disponibles.
	 * @return le montant de l'ensemble des sous-compte de ce compte.
	 */
	public BigDecimal getSommeMontantsDesSousComptes(ElmtNomenclatureContext context) {
		BigDecimal cumul = BigDecimal.ZERO;
		for (String autreCompte : context.keySet()) {
			if (isUnSousCompte(autreCompte)) {
				//cumul = cumul.add(context.resolve(autreCompte));
			    
			    IElementNomenclature sousCompteAsElem = context.get(autreCompte);
			    if (sousCompteAsElem instanceof Compte) {
			        Compte sousCompte = (Compte) sousCompteAsElem;
			        cumul = cumul.add(sousCompte.getMontantPropre());
			    }
			    
			}
		}
		return cumul;
	}

	private boolean isUnSousCompte(String autreCompte) {
		return !autreCompte.equalsIgnoreCase(getCode()) && autreCompte.startsWith(getCode());
	}

	public BigDecimal getMontantPropre() {
		return montantPropre;
	}

	public void setMontantPropre(BigDecimal montantPropre) {
		this.montantPropre = montantPropre;
	}


	@Override
	public String toString() {
		return "code: " + getCode() + " | mtPropre: " + getMontantPropre();
	}

}
