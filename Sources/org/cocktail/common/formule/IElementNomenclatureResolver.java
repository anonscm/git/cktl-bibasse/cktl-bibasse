package org.cocktail.common.formule;

public interface IElementNomenclatureResolver {

	IElementNomenclature resolve();

}
