package org.cocktail.common.formule;



public abstract class AbstractElementNomenclature implements IElementNomenclature {

	private String code;
	private ElmtNomenclatureContext context;

	public AbstractElementNomenclature(String code) {
		this.code = code;
		this.context = new ElmtNomenclatureContext();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ElmtNomenclatureContext getContext() {
		return context;
	}

	public void setContext(ElmtNomenclatureContext context) {
		this.context = context;
	}
}
