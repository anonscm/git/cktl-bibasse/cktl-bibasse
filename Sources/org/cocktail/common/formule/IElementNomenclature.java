package org.cocktail.common.formule;

import java.math.BigDecimal;

public interface IElementNomenclature {

	public String getCode();
	public BigDecimal getMontant();
	public void setContext(ElmtNomenclatureContext context);

}
