package org.cocktail.common.formule;

public interface INomenclatureEtatDetaille {

	String getCode();
	String getExpression();

}
