package org.cocktail.common.formule;

import org.mvel2.integration.PropertyHandler;
import org.mvel2.integration.VariableResolverFactory;

public class ElmtNomenclatureHandler implements PropertyHandler {

	public Object getProperty(String name, Object contextObj, VariableResolverFactory variableFactory) {
		ElmtNomenclatureContext context = (ElmtNomenclatureContext) contextObj;
		return context.resolve(name);
	}

	public Object setProperty(String name, Object contextObj, VariableResolverFactory variableFactory, Object value) {
		throw new UnsupportedOperationException();
	}
}
