package org.cocktail.common.cofisup;

/**
 * Constantes Cofisup.
 */
public final class CofisupActionConstants {

	/** Status terminé. */
	public static final String STATUS_DONE = "done";

	/** Status en cours. */
	public static final String STATUS_PENDING = "pending";

	/**Status erreur. */
	public static final String STATUS_ERROR = "error";

	/** Status annulé. */
	public static final String STATUS_CANCELLED = "cancel";

	/** Cle pour recuperer le status. */
	public static final String INFO_STATUS = "status";

	/** Cle pour recuperer l'identifiant du job. */
	public static final String INFO_JOB_ID = "jobId";

	/** Cle pour recuperer le fichier. */
	public static final String INFO_JOB_FILE = "jobFile";

	/** Cle pour recuperer les messages. */
	public static final String INFO_MSG = "msg";

	/** Nom de l'action pour démarrer un job. */
	public static final String ACTION_START_JOB = "clientSideRequestStartJob";

	/** Nom de la méthode pour récupérer les informations relatives a un job. */
	public static final String ACTION_INFOS_JOB = "clientSideRequestInfosJob";

	/** Nom de la méthode pour récupérer le fichier résultat d'un job. */
	public static final String ACTION_GET_FILE = "clientSideRequestRetrieveFile";

	/**
	 * Constructeur privé.
	 */
	private CofisupActionConstants() {
	}
}
