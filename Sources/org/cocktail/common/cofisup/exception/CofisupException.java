package org.cocktail.common.cofisup.exception;

public class CofisupException extends RuntimeException {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	public static final String ERR_CODE_UAI_NO_ORGAN = "Code UAI introuvable - Aucune ligne budgetaire typée nature budget.";
	public static final String ERR_CODE_UAI_NO_PRINCIPAL = "Code UAI introuvable - Aucune ligne bdugétaire typée 'PRINCIPAL'.";
	public static final String ERR_CODE_UAI_NOT_FOUND = "Code UAI non renseigné pour la structure rattachée à l'établissement.";

	public CofisupException() {
		super();
	}

	public CofisupException(String message) {
		super(message);
	}

	public CofisupException(String message, Throwable cause) {
		super(message, cause);
	}

	public CofisupException(Throwable cause) {
		super(cause);
	}
}
