package org.cocktail.common.cofisup;


public enum ETypeNatureBudget {

	AGREGE   ("",          "BA",      "BA",   "1", "Budget agrege hors SIE"),
	PRINCIPAL("PRINCIPAL", "BP",      "BP",   "2"),
	EPRD     ("EPRD",      "BAEPRD",  "FOND", "2"),
	NON_EPRD ("NON_EPRD",  "BANEPRD", "SAIC", "2"),
	BPI      ("",          "",        "",     ""),
	BPI_IUT  ("BPI_IUT",   "BPI",     "IUT",  "3"),
	BPI_ESPE ("BPI_ESPE",  "BPI",     "ESPE", "3"),
	SIE      ("SIE",       "SIE",     "SIE",  "-"),

	ETAB_NATURE      ("",          "BNE",      "BNE",  "1", "Budget de l'etablissement hors SIE"),
	ETAB_GESTION     ("",          "BGE",      "BGE",  "1", "Budget de l'etablissement hors SIE"),
	PRINC_NATURE     ("PRINCIPAL", "BPN",      "BPN",  "2"),
	PRINC_GESTION    ("PRINCIPAL", "BPG",      "BPG",  "2"),
	EPRD_NATURE      ("EPRD",      "BAEPRDN",  "FOND", "2"),
	EPRD_GESTION     ("EPRD",      "BAEPRDG",  "FOND", "2"),
	NON_EPRD_NATURE  ("NON_EPRD",  "BANEPRDN", "SAIC", "2"),
	NON_EPRD_GESTION ("NON_EPRD",  "BANEPRDG", "SAIC", "2"),
	SIE_NATURE       ("SIE",       "SIEN",     "SIE",  "-"),
	SIE_GESTION      ("SIE",       "SIEG",     "SIE",  "-");

	private String codeBase;
	private String codeFichier;
	private String composante;
	private String niveau;
	private String libelle;

	private ETypeNatureBudget(String codeBase, String codeFichier, String composante, String niveau) {
		this(codeBase, codeFichier, composante, niveau, null);
	}

	private ETypeNatureBudget(String codeBase, String codeFichier, String composante, String niveau, String libelle) {
		this.codeBase = codeBase;
		this.codeFichier = codeFichier;
		this.composante = composante;
		this.niveau = niveau;
		this.libelle = libelle;
	}

	/**
	 * @return true si cette nature budget est de type GESTION ; false sinon.
	 */
	public boolean isGestion() {
		boolean isGestion = false;

		switch(this) {
		case ETAB_GESTION :
		case PRINC_GESTION :
		case EPRD_GESTION :
		case NON_EPRD_GESTION :
		case SIE_GESTION :
			isGestion = true;
			break;
		default :
		}

		return isGestion;
	}

	public boolean isBPI() {
		return BPI.equals(this);
	}

	public String getCodeFichier() {
		return codeFichier;
	}

	public String getNiveau() {
		return niveau;
	}

	public String getComposante() {
		return composante;
	}

	public String getLibelle() {
		return libelle;
	}

	public String getCodeBase() {
		return codeBase;
	}
}

