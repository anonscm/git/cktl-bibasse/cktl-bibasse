package org.cocktail.common.cofisup;

public enum ETypeEtablissement {
	RCE,
	NON_RCE;

	public boolean isRce() {
		return RCE.equals(this);
	}
}
