package org.cocktail.common.cofisup;

import java.io.Serializable;
import java.util.Map;

/**
 * Bean décrivant les parametres d'un Job Cofisup.
 */
public class JobParameters implements Serializable {

	public static final String PARAM_EXERCICE_EN_COURS = "exerciceEnCours";
	public static final String PARAM_EXERCICE = "exercice";
	public static final String PARAM_TYPE_ETABLISSEMENT = "typeEtablissement";
	public static final String PARAM_NATURE_BUDGET = "natureBudget";
	public static final String PARAM_TYPE_BUDGET = "typeBudget";
	public static final String PARAM_TYPE_FICHIER = "typeFichier";
	public static final String PARAM_LIBELLE = "libelle";

	private static final long serialVersionUID = 1L;
	private Number exerciceEnCours;
	private Number exercice;
	private ETypeBudget typeBudget;
	private ETypeFichier typeFichier;
	private ETypeEtablissement typeEtablissement;
	private ETypeNatureBudget natureBudget;
	private String libelle;

	public JobParameters(Map<String, Object> parameters) {
		this((Number) parameters.get(JobParameters.PARAM_EXERCICE_EN_COURS),
			(Number) parameters.get(JobParameters.PARAM_EXERCICE),
			(String) parameters.get(JobParameters.PARAM_TYPE_BUDGET),
			(String) parameters.get(JobParameters.PARAM_TYPE_FICHIER),
			(String) parameters.get(JobParameters.PARAM_TYPE_ETABLISSEMENT),
			(String) parameters.get(JobParameters.PARAM_NATURE_BUDGET),
			(String) parameters.get(JobParameters.PARAM_LIBELLE));
	}

	public JobParameters(Number exerciceEnCours, Number exercice, String typeBudget, String typeFichier, String typeEtablissement, String natureBudget, String libelle) {
		this.exerciceEnCours = exerciceEnCours;
		this.exercice = exercice;
		this.typeBudget = ETypeBudget.valueOf(typeBudget);
		this.typeFichier = ETypeFichier.valueOf(typeFichier);
		this.typeEtablissement = ETypeEtablissement.valueOf(typeEtablissement);
		this.natureBudget = ETypeNatureBudget.valueOf(natureBudget);
		this.libelle = libelle;
	}

	public Number getExerciceEnCours() {
		return exerciceEnCours;
	}

	public void setExerciceEnCours(Number exerciceEnCours) {
		this.exerciceEnCours = exerciceEnCours;
	}

	public Number getExercice() {
		return exercice;
	}

	public void setExercice(Number exercice) {
		this.exercice = exercice;
	}

	public ETypeBudget getTypeBudget() {
		return typeBudget;
	}

	public void setTypeBudget(ETypeBudget typeBudget) {
		this.typeBudget = typeBudget;
	}

	public ETypeFichier getTypeFichier() {
		return typeFichier;
	}

	public void setTypeFichier(ETypeFichier typeFichier) {
		this.typeFichier = typeFichier;
	}

	public ETypeEtablissement getTypeEtablissement() {
		return typeEtablissement;
	}

	public void setTypeEtablissement(ETypeEtablissement typeEtablissement) {
		this.typeEtablissement = typeEtablissement;
	}

	public ETypeNatureBudget getNatureBudget() {
		return natureBudget;
	}

	public void setNatureBudget(ETypeNatureBudget natureBudget) {
		this.natureBudget = natureBudget;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
}