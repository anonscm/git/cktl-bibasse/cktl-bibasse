package org.cocktail.common.cofisup;

import java.util.EnumSet;

public enum ETypeFichier {

	RD(""),
	CR("Compte de résultat"),
	TF("Tableau de financement"),
	BPI("Budget propre intégré");

	private String libelle;

	private ETypeFichier(String libelle) {
		this.libelle = libelle;
	}

	public boolean isBPI() {
		return BPI.equals(this);
	}

	public String getLibelle() {
		return libelle;
	}

	public boolean isInformationsComplementaires() {
		return EnumSet.of(CR, TF, BPI).contains(this);
	}
}
