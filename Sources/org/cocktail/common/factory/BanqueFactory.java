package org.cocktail.common.factory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.common.metier.Banque;
import org.cocktail.common.metier.IJefyAdminParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class BanqueFactory {

    public Banque creerBanque(EOEditingContext ec) {
        Banque banque = new Banque();
        for (InfosDeviseParametree infos : recupererInfosDeviseParExercice(ec)) {
            banque.ajouterTauxChange(
                infos.getCode(), 
                Banque.EURO_DEVISE, 
                infos.getExercice(), 
                infos.tauxAsNumber());
        }
        return banque;
    }

    protected List<InfosDeviseParametree> recupererInfosDeviseParExercice(EOEditingContext ec) {
        // collection de code / exercice
        NSArray codesDevises = recupererPararametre(ec, IJefyAdminParametre.PARAM_DEVISE_DEV_CODE);
        
        // collection de taux / exercice
        NSArray tauxDevises = recupererPararametre(ec, IJefyAdminParametre.PARAM_DEVISE_TAUX);
        
        List<InfosDeviseParametree> infosDevises = new ArrayList<BanqueFactory.InfosDeviseParametree>();
        for (int idxCodes = 0; idxCodes < codesDevises.count(); idxCodes++) {
            EOEnterpriseObject currentCodeDevise = (EOEnterpriseObject) codesDevises.objectAtIndex(idxCodes);
            String code = null;
            Integer exercice = null;
            
            if (currentCodeDevise.valueForKey(IJefyAdminParametre.PAR_VALUE_KEY) != null) {
                code = currentCodeDevise.valueForKey(IJefyAdminParametre.PAR_VALUE_KEY).toString();
            }
            if (currentCodeDevise.valueForKey(IJefyAdminParametre.EXE_ORDRE_KEY) != null) {
                exercice = Integer.valueOf(currentCodeDevise.valueForKey(IJefyAdminParametre.EXE_ORDRE_KEY).toString());
            }
            String taux = rechercheTauxDevisePourExercice(tauxDevises, exercice);
            if (taux != null) {
                infosDevises.add(new InfosDeviseParametree(code, taux, exercice));
            }
        }
        
        return infosDevises;
    }
    
    protected String rechercheTauxDevisePourExercice(NSArray tauxDevises, Integer exercice) {
        if (exercice == null) {
            return null;
        }
        
        String taux = null;
        for (int idxTx = 0; idxTx < tauxDevises.count(); idxTx++) {
            EOEnterpriseObject currentTxDevise = (EOEnterpriseObject) tauxDevises.objectAtIndex(idxTx);
            Integer currentExercice = Integer.valueOf(currentTxDevise.valueForKey(IJefyAdminParametre.EXE_ORDRE_KEY).toString());
            if (currentExercice.equals(exercice)) {
                taux = currentTxDevise.valueForKey(IJefyAdminParametre.PAR_VALUE_KEY).toString();
                break;
            }
        }
        return taux;
    }
    
    protected NSArray recupererPararametre(EOEditingContext ec, String parametre) {
        EOQualifier qualParametre = EOQualifier.qualifierWithQualifierFormat(
            IJefyAdminParametre.PAR_KEY_KEY + "= %@", new NSArray<String>(parametre));
        return fetch(ec, IJefyAdminParametre.ENTITY_NAME, qualParametre);
    }
    
    private NSArray fetch(EOEditingContext ec, String entityName, EOQualifier qualifier)   {
        try {
            EOFetchSpecification fs = new EOFetchSpecification(entityName, qualifier, null);
            fs.setRefreshesRefetchedObjects(true);
            return ec.objectsWithFetchSpecification(fs);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static class InfosDeviseParametree {
        private String code;
        private String taux;
        private Integer exercice;

        public InfosDeviseParametree(String code, String taux, Integer exercice) {
            this.code = code;
            this.taux = taux;
            this.exercice = exercice;
        }
        
        public String getCode() {
            return code;
        }
        
        public String getTaux() {
            return taux;
        }
        
        public Integer getExercice() {
            return exercice;
        }
        
        public BigDecimal tauxAsNumber() {
            if (taux == null) {
                return null;
            }
            taux = taux.replaceAll("\\,", ".")
                       .replaceAll(" ", "")
                       .trim();
            return BigDecimal.valueOf(Double.valueOf(taux));
        }
    }
}
