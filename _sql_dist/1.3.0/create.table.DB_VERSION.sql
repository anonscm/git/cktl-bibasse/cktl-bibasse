CREATE TABLE JEFY_BUDGET.DB_VERSION
(
  DB_VERSION_LIBELLE  VARCHAR2(15 BYTE)         NOT NULL,
  DB_VERSION_DATE     DATE                      NOT NULL,
  DB_INSTALL_DATE     DATE,
  DB_COMMENT          VARCHAR2(100 BYTE),
  DB_VERSION_ID       NUMBER(4)                 NOT NULL
)
TABLESPACE GFC;

COMMENT ON TABLE JEFY_BUDGET.DB_VERSION IS 'Historique des versions du schema du user JEFY_BUDGET';
COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_VERSION_LIBELLE IS 'Libelle de la version';
COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_VERSION_DATE IS 'Date de release de la version';
COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_INSTALL_DATE IS 'Date d''installation de la version. Si non renseigne, la version n''est pas completement installee';
COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_COMMENT IS 'Le commentaire/une courte description de cette version de la base de donnees.';
COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_VERSION_ID IS 'Clef primaire de JEFY_BUDGET.DB_VERSION';