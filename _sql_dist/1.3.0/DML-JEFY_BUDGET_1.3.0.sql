--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  JEFY_BUDGET
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 1.3.0
-- Date de publication : 14/10/2011 
-- Licence : CeCILL version 2
--
--

----------------------------------------------
-- 
-- 
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_jefy_bud_1300;
commit;

drop procedure grhum.inst_patch_jefy_bud_1300;