DROP VIEW JEFY_BUDGET.V_PLAN_COMPTABLE_EXER;

/* Formatted on 2011/03/31 17:12 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW jefy_budget.v_plan_comptable_exer (exe_ordre,
                                                                pco_num,
                                                                pco_libelle,
                                                                pco_num_vote,
                                                                chap_libelle
                                                               )
AS
   SELECT DISTINCT exe_ordre, pco_num, chap, pco_num_vote, pco
              FROM (SELECT m.exe_ordre, m.pco_num, p1.pco_libelle chap,
                           pco_num_vote, p2.pco_libelle pco
                      FROM budget_masque_nature m,
                           maracuja.plan_comptable_exer p1,
                           maracuja.plan_comptable_exer p2
                     WHERE m.exe_ordre = p1.exe_ordre
                       AND m.exe_ordre = p2.exe_ordre
                       AND m.pco_num = p1.pco_num
                       AND pco_num_vote = p2.pco_num
                    UNION ALL
                    SELECT m.exe_ordre, pco_num_vote, p1.pco_libelle,
                           pco_num_vote, p2.pco_libelle
                      FROM budget_masque_nature m,
                           maracuja.plan_comptable_exer p1,
                           maracuja.plan_comptable_exer p2
                     WHERE m.exe_ordre = p1.exe_ordre
                       AND m.exe_ordre = p2.exe_ordre
                       AND pco_num_vote = p1.pco_num
                       AND pco_num_vote = p2.pco_num)
   UNION ALL
   SELECT exe_ordre, pco_num, pco_libelle, pco_num, pco_libelle
     FROM maracuja.plan_comptable_exer p
    WHERE pco_num LIKE '0%' AND pco_niveau = 3;


