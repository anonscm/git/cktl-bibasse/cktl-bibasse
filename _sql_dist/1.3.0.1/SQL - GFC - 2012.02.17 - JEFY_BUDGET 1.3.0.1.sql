SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_BUDGET
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.0.1
-- Date de publication :  17/02/2012
-- Licence : CeCILL version 2
--
--



whenever sqlerror exit sql.sqlcode ;


CREATE OR REPLACE PACKAGE JEFY_BUDGET.Api_report IS

 --
 -- renvoie l'ID du plus proche parent de l'action votee
FUNCTION get_Dep_Action_votee(
  lolfId    integer,
  exeOrdre  integer
  ) RETURN INTEGER;

FUNCTION get_Rec_Action_votee(
  lolfId    integer,
  exeOrdre  integer
  ) RETURN INTEGER;

-- renvoie le pcoNum parent vote le plus proche. Si pas de parent vote, renvoie le pcoNum
FUNCTION get_pconum_vote(
  pcoNum    varchar2,
  exeOrdre  integer
  ) RETURN varchar2;
    
-- renvoie l'action LOLF pcpale (définie par le prog du ministère) niveau 1 dans jefy_admin
FUNCTION get_action_RCE(
  lolfid    integer
  ) RETURN integer;

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_BUDGET.Api_report IS


FUNCTION get_Dep_Action_votee(
   lolfId    integer, 
   exeOrdre  integer
) RETURN INTEGER
IS
   flag NUMBER;
   lolfDepense jefy_admin.lolf_nomenclature_depense%rowtype;
BEGIN
   -- si action est votee, on la renvoie
   select count(*) into flag from jefy_budget.budget_vote_gestion qb where qb.tyac_id=lolfId and exe_ordre=exeOrdre;
   if (flag>0) then
      return lolfId;
   end if;

   -- sinon on cherche avec l action pere
   select * into lolfDepense from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfId;
   if (lolfDepense.lolf_Pere is null or lolfDepense.lolf_Niveau<=0) then
      return lolfId;
   end if;

   return get_Dep_Action_votee(lolfDepense.lolf_Pere, exeOrdre);
end;


FUNCTION get_Rec_Action_votee(
   lolfId    integer,
   exeOrdre  integer
) RETURN INTEGER
IS
   flag NUMBER;
   lolfRecette jefy_admin.lolf_nomenclature_recette%rowtype;
BEGIN
   select count(*) into flag from jefy_budget.budget_vote_gestion qb where qb.tyac_id=lolfId and exe_ordre=exeOrdre;
   if flag>0 then
      return lolfId;
   end if;

   -- sinon on cherche avec l action pere
   select * into lolfRecette from jefy_admin.lolf_nomenclature_recette where lolf_id=lolfId;
   if (lolfRecette.lolf_Pere is null or lolfRecette.lolf_Niveau<=0) then
      return lolfId;
   end if;

   return get_Rec_Action_votee(lolfRecette.lolf_Pere, exeOrdre);
end;

FUNCTION get_pconum_vote(
  pcoNum    varchar2,
  exeOrdre  integer
  ) RETURN varchar2
IS
   flag NUMBER;
   res integer;
BEGIN
     if (pconum is null or length(pconum)<=2) then
        return pconum;
     end if;

     select max(length(pco_num)) into flag from jefy_budget.budget_vote_nature where exe_ordre=exeordre and pconum like pco_num||'%';
     if flag is null or flag=length(pconum) then 
        return pconum; 
     end if;
     
     return substr(pconum,1,flag);
end;

FUNCTION get_action_RCE(
   lolfid    integer
) RETURN integer
IS
   cpt integer;
   lolfDepense jefy_admin.lolf_nomenclature_depense%rowtype;
BEGIN
   -- verifier que les parametres sont bien remplis
   IF (lolfId IS NULL ) THEN
      RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
   END IF;

   -- si action est une "vraie" action, on la renvoie
   select count(*) into cpt from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfId and lolf_niveau = 1;
   if (cpt= 1) then
      return lolfId;
   end if;
   
   -- sinon on cherche avec l'action pere
   select * into lolfDepense from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfId;
   While lolfDepense.lolf_Niveau > 1
   Loop
      select * into lolfDepense from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfDepense.lolf_Pere;
   End loop ;
            
   return lolfDepense.lolf_id;
end;

END;
/
