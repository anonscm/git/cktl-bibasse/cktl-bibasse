-- Fichier :  no 1/2
-- Type : DDL
-- Schema modifie :  JEFY_BUDGET
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.3.1
-- Date de publication : 12/03/2012
-- Licence : CeCILL version 2

whenever sqlerror exit sql.sqlcode;


create or replace view jefy_budget.v_planco_credit as 
select t.exe_ordre, p.pcc_ordre, p.tcd_ordre, p.pco_num, p.pla_quoi, p.pcc_Etat
from maracuja.planco_credit p, jefy_admin.type_credit t where t.tcd_ordre=p.tcd_ordre;


create or replace procedure grhum.inst_patch_jefy_bud_1310 is
 nb integer;
begin
	 insert into jefy_budget.db_version values ('1.3.1.0',to_date('12/03/2012','dd/mm/yyyy'),sysdate,null,1310);
end;
/