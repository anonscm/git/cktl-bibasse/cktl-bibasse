-- Fichier :  no 2/2
-- Type : DML
-- Schema modifie :  JEFY_BUDGET
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.3.1
-- Date de publication : 12/03/2012
-- Licence : CeCILL version 2


whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_bud_1310;
commit;

drop procedure grhum.inst_patch_jefy_bud_1310;

