CREATE OR REPLACE PACKAGE JEFY_BUDGET.Api_report IS

 --
 -- renvoie l'ID du plus proche parent de l'action votee
FUNCTION get_Dep_Action_votee(
  lolfId    integer,
  exeOrdre  integer
  ) RETURN INTEGER;

FUNCTION get_Rec_Action_votee(
  lolfId    integer,
  exeOrdre  integer
  ) RETURN INTEGER;

-- renvoie le pcoNum parent vote le plus proche. Si pas de parent vote, renvoie le pcoNum
FUNCTION get_pconum_vote(
  pcoNum    varchar2,
  exeOrdre  integer
  ) RETURN varchar2;
  
-- renvoie l'action LOLF pcpale (d�finie par le prog du minist�re) niveau 1 dans jefy_admin
FUNCTION get_action_RCE(
  lolfid    integer
  ) RETURN integer;


END;
/
CREATE OR REPLACE PACKAGE BODY JEFY_BUDGET.Api_report IS

    FUNCTION get_Dep_Action_votee(
          lolfId    integer,
          exeOrdre  integer
  ) RETURN INTEGER

    IS
        flag NUMBER;
        lolfDepense jefy_admin.lolf_nomenclature_depense%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;
         IF (exeOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre exeOrdre est null ');
         END IF;

          ----------------------------------------------
         -- verifier que le lolfId existe dans les depenses
--         SELECT COUNT(*) INTO flag FROM jefy_admin.lolf_nomenclature_depense WHERE lolf_id = lolfId ;
--         IF (flag = 0) THEN
--             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_depense correspondant a lolf_id=' || lolfId ||' introuvable' );
--         END IF;

        -- si action est votee, on la renvoie
        select count(*) into flag
            from jefy_budget.budget_vote_gestion qb,
            jefy_admin.lolf_nomenclature_depense l
            where qb.tyac_id=l.lolf_id and exe_ordre=exeOrdre
            and l.lolf_id=lolfId;

        if (flag>0) then
            return lolfId;
        else
            -- sinon on cherche avec l action pere
            select * into lolfDepense from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfId;
            if (lolfDepense.lolf_Pere is null or lolfDepense.lolf_Niveau<=0) then
                return lolfId;
            else
                return get_Dep_Action_votee(lolfDepense.lolf_Pere, exeOrdre);
            end if;
        end if;


    end;





FUNCTION get_Rec_Action_votee(
          lolfId    integer,
          exeOrdre  integer
  ) RETURN INTEGER

    IS
        flag NUMBER;
        lolfRecette jefy_admin.lolf_nomenclature_recette%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;
         IF (exeOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre exeOrdre est null ');
         END IF;

          ----------------------------------------------
         -- verifier que le lolfId existe dans les depenses
--         SELECT COUNT(*) INTO flag FROM jefy_admin.lolf_nomenclature_recette WHERE lolf_id = lolfId ;
--         IF (flag = 0) THEN
--             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_recette correspondant a lolf_id=' || lolfId ||' introuvable' );
--         END IF;

        -- si action est votee, on la renvoie
        select count(*) into flag
            from jefy_budget.budget_vote_gestion qb,
            jefy_admin.lolf_nomenclature_recette l
            where qb.tyac_id=l.lolf_id and exe_ordre=exeOrdre
            and l.lolf_id=lolfId;

        if (flag>0) then
            return lolfId;
        else
            -- sinon on cherche avec l action pere
            select * into lolfRecette from jefy_admin.lolf_nomenclature_recette where lolf_id=lolfId;
            if (lolfRecette.lolf_Pere is null or lolfRecette.lolf_Niveau<=0) then
                return lolfId;
            else
                return get_Rec_Action_votee(lolfRecette.lolf_Pere, exeOrdre);
            end if;
        end if;


    end;


FUNCTION get_pconum_vote(
  pcoNum    varchar2,
  exeOrdre  integer
  ) RETURN varchar2
    IS
        flag NUMBER;
        res integer;
    BEGIN

        -- si pcoNum est vote, on le renvoie
        select count(*) into flag
            from jefy_budget.budget_vote_nature qb
            where exe_ordre=exeOrdre
            and qb.pco_num=pcoNum;

        if (flag>0) then
            res := pcoNum;
        else
            -- sinon on cherche avec le pcoNumPere
            if (length(pcoNum) <=1 ) then
                res:= null;
            else
                res := get_pconum_vote(substr(pcoNum,1,length(pcoNum)-1), exeOrdre);

            end if;
        end if;

        if (res is null) then
            res := pcoNum;
        end if;
        return res;
    end;



FUNCTION get_action_RCE(
  lolfid    integer
  ) RETURN integer

IS
        cpt integer;
        lolfDepense jefy_admin.lolf_nomenclature_depense%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        -- si action est une "vraie" action, on la renvoie
        select count(*) into cpt
            from jefy_admin.lolf_nomenclature_depense
            where lolf_id=lolfId and lolf_niveau = 1;

        if (cpt= 1) then
            return lolfId;
        else
            -- sinon on cherche avec l'action pere
            select * into lolfDepense from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfId;
            While lolfDepense.lolf_Niveau > 1
            Loop
                select * into lolfDepense from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfDepense.lolf_Pere;
                    
            End loop ;
            
            return lolfDepense.lolf_id;

        end if;


    end;
    


END;
/
