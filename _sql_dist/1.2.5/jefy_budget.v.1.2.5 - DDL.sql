
-- A compiler dans le user JEFY_BUDGET

CREATE OR REPLACE VIEW JEFY_BUDGET.V_BUDGET_SAISIE_GESTION_DEP
(EXE_ORDRE, BDSA_ID, ORG_ID, TYAC_TYPE, TYAC_DECAISS, 
 TYAC_PROG, TYAC_ID, TCD_ORDRE, BDSG_VOTE, BDSG_SAISI, 
 BDSG_MONTANT)
AS 
SELECT b.exe_ordre, bdsa_id, org_id, a.tyac_type, decaissable, tyac_prog, b.tyac_id, tcd_ordre, SUM(bdsg_vote), SUM(bdsg_saisi), SUM(b.BDSG_MONTANT)
FROM BUDGET_SAISIE_GESTION b, v_type_action_budget a
WHERE b.TYAC_ID = a.TYAC_ID AND a.TYAC_TYPE = 'DEPENSE' AND b.exe_ordre = a.exe_ordre
GROUP BY b.exe_ordre, bdsa_id, org_id, a.tyac_type, tyac_prog, decaissable, b.tyac_id, tcd_ordre
UNION ALL
SELECT b.exe_ordre, bdsa_id, org_id, a.tyac_type, decaissable, tyac_prog, b.tyac_id, tcd_ordre, SUM(bdcg_vote), SUM(bdcg_saisi), SUM(b.BDCG_CALCUL)
FROM BUDGET_CALCUL_GESTION b, v_type_action_budget a
WHERE b.TYAC_ID = a.TYAC_ID AND a.TYAC_TYPE = 'DEPENSE' AND b.exe_ordre = a.exe_ordre
GROUP BY b.exe_ordre, bdsa_id, org_id, a.tyac_type, tyac_prog, decaissable, b.tyac_id, tcd_ordre;

CREATE OR REPLACE VIEW JEFY_BUDGET.V_BUDGET_SAISIE_GESTION_REC
(EXE_ORDRE, BDSA_ID, ORG_ID, TYAC_TYPE, TYAC_DECAISS, 
 TYAC_PROG, TYAC_ID, TCD_ORDRE, BDSG_VOTE, BDSG_SAISI, 
 BDSG_MONTANT)
AS 
SELECT b.exe_ordre, bdsa_id, org_id, a.tyac_type, decaissable, tyac_prog, b.tyac_id, tcd_ordre, 
SUM(BDSG_VOTE), SUM(bdsg_saisi), SUM(b.BDSG_MONTANT)
FROM BUDGET_SAISIE_GESTION b, v_type_action_budget a
WHERE b.TYAC_ID = a.TYAC_ID AND a.TYAC_TYPE = 'RECETTE' AND b.exe_ordre = a.exe_ordre
GROUP BY b.exe_ordre, bdsa_id, org_id, a.tyac_type, tyac_prog, decaissable, b.tyac_id, tcd_ordre
UNION ALL
SELECT b.exe_ordre, bdsa_id, org_id, a.tyac_type, decaissable, tyac_prog, b.tyac_id, tcd_ordre, 
SUM(BDCG_VOTE), SUM(bdcg_saisi), SUM(b.BDCG_CALCUL)
FROM BUDGET_CALCUL_GESTION b, v_type_action_budget a
WHERE b.TYAC_ID = a.TYAC_ID AND a.TYAC_TYPE = 'RECETTE' AND b.exe_ordre = a.exe_ordre
GROUP BY b.exe_ordre, bdsa_id, org_id, a.tyac_type, tyac_prog, decaissable, b.tyac_id, tcd_ordre;

CREATE OR REPLACE FORCE VIEW jefy_budget.budget_rce_rec (exe_ordre,
                                                         bdsa_id,
                                                         bdsa_libelle,
                                                         org_id,
                                                         section,
                                                         num_bloc,
                                                         credits,
                                                         montant
                                                        )
AS
   SELECT   bdn.exe_ordre, bs.bdsa_id, bs.bdsa_libelle, o.org_id, tcd_sect, num_bloc,
            credits, SUM (bdn.bdsn_montant) montant
       FROM jefy_budget.budget_saisie bs,
            jefy_admin.type_credit tc,
            jefy_admin.organ o,
            (SELECT exe_ordre, bdsa_id, org_id, tcd_ordre, pco_num,
                    bdsn_montant,
                    DECODE (SUBSTR (pco_num, 1, 1), '7', 1, 4) num_bloc,
                    DECODE (SUBSTR (pco_num, 1, 2),
                            '74', 'Subventions d''Exploitation',
                            '13', 'Subventions d''Investissement',
                            'Autres Ressources'
                           ) credits
               FROM jefy_budget.v_budget_saisie_nature
              WHERE bdsn_montant <> 0 AND nature = 'R'
                    AND pco_num NOT LIKE '0%') bdn
      WHERE bdn.tcd_ordre = tc.tcd_ordre
        AND bdn.bdsa_id = bs.bdsa_id
        AND bdn.org_id = o.org_id
        AND pco_num NOT LIKE '0%'
       -- AND org_niv = 0
   GROUP BY bdn.exe_ordre,
            bs.bdsa_id,
            bs.bdsa_libelle,
            o.org_id,
            tc.tcd_sect,
            num_bloc,
            credits
   ORDER BY bdn.exe_ordre, bs.bdsa_id, tc.tcd_sect, credits;

CREATE OR REPLACE FORCE VIEW jefy_budget.budget_rce_dep (exe_ordre,
                                                         bdsa_id,
                                                         bdsa_libelle,
                                                         org_id,
                                                         num_bloc,
                                                         tyacp_id,
                                                         lolf_code,
                                                         section,
                                                         credits,
                                                         montant
                                                        )
AS
   SELECT   bdg.exe_ordre, bs.bdsa_id, bs.bdsa_libelle,o.org_id,
            DECODE (tcd_sect, 2, 4, 1), bdg.tyacp_id,
            DECODE (l.lolf_code,
                    l.lolf_abreviation, l.lolf_code,
                    l.lolf_code || ' ' || l.lolf_abreviation
                   ) lolf_code,
            tcd_sect,
            DECODE (tcd_sect,
                    3, 'Masse Salariale',
                    1, 'Fonctionnement',
                    2, 'Investissement'
                   ),
            SUM (bdg.bdsg_montant) montant
       FROM jefy_budget.budget_saisie bs,
            jefy_admin.lolf_nomenclature_depense l,
            jefy_admin.type_credit tc,
            jefy_admin.organ o,
            (SELECT exe_ordre, bdsa_id, org_id, tyac_id,
                    jefy_budget.api_report.get_action_rce (tyac_id) tyacp_id,
                    tcd_ordre, bdsg_montant
               FROM jefy_budget.v_budget_saisie_gestion_dep) bdg
      WHERE bdg.tyacp_id = l.lolf_id
        AND bdg.tcd_ordre = tc.tcd_ordre
        AND bdg.bdsa_id = bs.bdsa_id
        AND bdg.org_id = o.org_id
        --AND org_niv = 0
   GROUP BY bdg.exe_ordre,
            bs.bdsa_id,
            bs.bdsa_libelle,
            o.org_id,
            bdg.tyacp_id,
            DECODE (l.lolf_code,
                    l.lolf_abreviation, l.lolf_code,
                    l.lolf_code || ' ' || l.lolf_abreviation
                   ),
            tc.tcd_sect
   ORDER BY bdg.exe_ordre,
            bs.bdsa_id,
            DECODE (l.lolf_code,
                    l.lolf_abreviation, l.lolf_code,
                    l.lolf_code || ' ' || l.lolf_abreviation
                   ),
            tc.tcd_sect;
            