--------------------------------------------------------
--  DDL for Package BUDGET_MOTEUR
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_BUDGET"."BUDGET_MOTEUR" IS


/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

 -- Version du : 09/01/2007

 -- procedures budgetaires
  PROCEDURE consolider_saisie_budget(bdsaid INTEGER);
  PROCEDURE consolider_etablissement(bdsaid INTEGER);

  PROCEDURE calculer_caf_iaf_fdr(bdsaid INTEGER);
  PROCEDURE calculer_caf_iaf_fdr_saisi(bdsaid INTEGER) ;
  PROCEDURE controler_saisie_budget(bdsaid INTEGER);
  PROCEDURE fermer_saisie_budget(bdsaid INTEGER);

  PROCEDURE voter_saisie_budget(bdsaid INTEGER);
  PROCEDURE voter_saisie_budget_calcul(bdsaid INTEGER);
  PROCEDURE refuser_saisie_budget(bdsaid INTEGER);
  PROCEDURE changer_saisie_budget(bdsaid INTEGER,tysaid INTEGER) ;

  PROCEDURE deverrouiller_saisie(bdsaid INTEGER, orgid INTEGER);
  PROCEDURE valider_saisie_budget_organ(bdsaid INTEGER, orgid INTEGER);
  PROCEDURE controler_saisie_budget_organ(bdsaid INTEGER, orgid INTEGER);
  PROCEDURE cloturer_saisie_budget_organ(bdsaid INTEGER, orgid INTEGER);

procedure controle_saisie_budgetaire(bdsaid integer, orgid integer);

  -- fonction de verification du disponible
  FUNCTION verif_dispo_bdsaid (bdsaid INTEGER) RETURN VARCHAR;
  FUNCTION verif_dispo_bdsnid (bdsnid INTEGER) RETURN VARCHAR;
  FUNCTION verif_dispo_bdsgid (bdsgid INTEGER) RETURN VARCHAR;

  -- procedures de controle DB
  PROCEDURE controle_etat(bdsaid INTEGER);
  PROCEDURE controle_organ(bdsaid INTEGER);
  PROCEDURE controle_imputation(bdsaid INTEGER);
  PROCEDURE controle_actions(bdsaid INTEGER);
  PROCEDURE controle_exercice(bdsaid INTEGER);

-- init des budget_saisie_xxx avec bdxx_vote
  PROCEDURE set_bds_vote(bdsaid INTEGER);
  PROCEDURE set_bds_vote_organ(bdsaid INTEGER, orgid INTEGER);
  PROCEDURE set_bdsn_vote(bdsnid INTEGER);
  PROCEDURE set_bdsg_vote(bdsgid INTEGER);

  -- procedures   outils
  PROCEDURE vider_consolidation(bdsaid INTEGER);
  PROCEDURE creer_consolidation(bdsaid INTEGER);
  PROCEDURE controle_disponible(bdsaid INTEGER);
  PROCEDURE vider_budget_organ(bdsaid INTEGER, orgid INTEGER);

  FUNCTION get_parametre(KEY VARCHAR,   execice INTEGER) RETURN VARCHAR;
  FUNCTION get_orgid_niv_orgid(orgid INTEGER,   niveau INTEGER) RETURN INTEGER;
  FUNCTION get_chapitre_vote(pconum VARCHAR,exeordre INTEGER, tcdordre integer) RETURN VARCHAR ;

  --PROCEDURE set_ligne_nature(bdsnid INTEGER,orgid INTEGER);
  --PROCEDURE set_ligne_gestion(bdsgid INTEGER,orgid INTEGER);

  PROCEDURE set_ligne_nature_calcul(bdsnid INTEGER,orgid INTEGER);
  PROCEDURE set_ligne_gestion_calcul(bdsgid INTEGER,orgid INTEGER);

  PROCEDURE set_ligne_nature_vote(bdsnid INTEGER);
  PROCEDURE set_ligne_nature_lolf_vote(bdslid INTEGER);
  PROCEDURE set_ligne_gestion_vote(bdsgid INTEGER);

  PROCEDURE set_ligne_nature_vote_calcul(bdcnid INTEGER);
  PROCEDURE set_ligne_gestion_vote_calcul(bdcgid INTEGER);

  PROCEDURE set_budfonc(bdsgid INTEGER);
  PROCEDURE set_ligne_budfonc(bdsgid INTEGER);
  PROCEDURE set_ligne_budfonc_conv(bdsgid INTEGER);

  -- procedures qui gere la protection des credits lors des DBMs NEGATIVES
  PROCEDURE set_budfonc_reserve(bdsaid INTEGER);
  PROCEDURE set_budfonc_avant_vote_dbm(exeordre INTEGER);
  PROCEDURE set_ligne_budfonc_reserve(exeordre INTEGER,tcdordre INTEGER,orgid INTEGER,montant NUMBER);
  PROCEDURE set_ligne_budfonc_conv_reserve(exeordre INTEGER,tcdordre INTEGER,orgid INTEGER,montant NUMBER);

  PROCEDURE verifier_dep_nature_gestion(bdsaid INTEGER);
  PROCEDURE verifier_dep_rec_nature(bdsaid INTEGER);

  PROCEDURE initialiser_budget_dbm(bdsaid INTEGER);
  PROCEDURE initialiser_budget_reliquat(bdsaid INTEGER);

  FUNCTION conv_ra(orgid INTEGER,exeordre INTEGER) RETURN INTEGER;

  PROCEDURE VIDER_MASQUE_VIDE;


PROCEDURE Consolider_Tous_Budgets(exeordre INTEGER);
PROCEDURE Maj_Comptes_Regul_Votes(exeordre INTEGER);

FUNCTION controle_saisie_dbm_gestion(bdsaid INTEGER, orgid INTEGER) RETURN VARCHAR;
FUNCTION controle_saisie_dbm_nature(bdsaid INTEGER, orgid INTEGER) RETURN VARCHAR;

FUNCTION controle_saisie_dbm_nat_imp(bdsaid INTEGER, orgid INTEGER,tcdordre INTEGER,pconum VARCHAR) RETURN VARCHAR;
FUNCTION controle_saisie_dbm_ges_imp(bdsaid INTEGER, orgid INTEGER,tcdordre INTEGER,tyacid INTEGER) RETURN VARCHAR;

FUNCTION controle_saisie_dbm_exe_imp(bdsaid INTEGER, orgid INTEGER,tcdordre INTEGER) RETURN VARCHAR;

PROCEDURE supprimer_budget (bdsaid INTEGER, bdsaLibelle VARCHAR2);
PROCEDURE supprimer_saisie_budget (bdsaid INTEGER,orgid INTEGER);

PROCEDURE Add_Masque_Nature ( tcdordre NUMBER, pconum VARCHAR, exeordre NUMBER);
PROCEDURE Add_Masque_Gestion ( lolfid NUMBER, lolftype v_lolf_nomenclature.tyac_type%TYPE, exeordre NUMBER);

PROCEDURE del_Masque_Gestion ( lolfid NUMBER, exeordre NUMBER);
PROCEDURE del_Masque_nature ( tcdordre NUMBER, pconum varchar, exeordre NUMBER);

procedure dupliquer_masque(exeordre number, typemasque varchar);
FUNCTION get_etat_saisie(bdsaid INTEGER, orgid INTEGEr) RETURN VARCHAR;

  -- procedures de lock afin de gere les DEADLOCKs
  PROCEDURE lock_table_budget_saisie;
  PROCEDURE lock_table_budget_vote;
  PROCEDURE lock_table_budget_mouvement;
  PROCEDURE lock_table_budget_execution;

-- saisie nature lolf
  PROCEDURE cloturer_nature_lolf(bdsaid INTEGER, tcdordre INTEGER);
  PROCEDURE init_saisie_nature_lolf(bdsaid integer);
END;
/
