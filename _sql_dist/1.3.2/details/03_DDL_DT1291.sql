--------------------------------------------------------
--  DDL for View V_LOLF_NOMENCLATURE
--------------------------------------------------------

CREATE OR REPLACE FORCE VIEW "JEFY_BUDGET"."V_LOLF_NOMENCLATURE"
	("EXE_ORDRE", "TYAC_ID", "TYAC_CODE", "TYAC_LIBELLE", "TYAC_ABREGE", "TYAC_NIVEAU", "TYAC_PROG", "TYAC_TYPE", "TYET_ID")
AS
	SELECT exe_ordre, lolf_id, lolf_code, lolf_libelle, lolf_abreviation, lolf_niveau, DECODE(SUBSTR(lolf_code,0,1),'1','1','2','2','3'), 'DEPENSE', tyet_id
      FROM v_lolf_nomenclature_bud_dep
     WHERE lolf_niveau >=1

    UNION ALL

    SELECT exe_ordre, lolf_id, lolf_code, lolf_libelle, lolf_abreviation, lolf_niveau, DECODE(SUBSTR(lolf_code,0,1),'1','1','2','2','3'),'RECETTE', tyet_id
      FROM v_lolf_nomenclature_bud_rec
     WHERE lolf_niveau >=1;
