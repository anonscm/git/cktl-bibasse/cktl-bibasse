--------------------------------------------------------
--  DDL for Package Body BUDGET_MOTEUR
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_BUDGET"."BUDGET_MOTEUR" IS -- procedure pour une budget saisie

/*******************************************
CONSOLIDER_SAISIE_BUDGET
Consolidation du budget pour les niveaux 0,1,2. Puis calcul des CAF, IAF et Fonds de roulement.
*******************************************/
  PROCEDURE consolider_saisie_budget(bdsaid INTEGER) IS

  cpt INTEGER;

  lignenature BUDGET_SAISIE_NATURE % ROWTYPE;
  lignegestion BUDGET_SAISIE_GESTION % ROWTYPE;

  currentBudgetSaisie BUDGET_SAISIE%ROWTYPE;

  CURSOR nature(niveau INTEGER) IS
  SELECT b.*
  FROM BUDGET_SAISIE_NATURE b,
    v_organ o
  WHERE b.bdsa_id = bdsaid
   AND b.tyet_id NOT IN(2)
   AND b.org_id = o.org_id
   AND o.org_niv = niveau
   AND (bdsn_saisi <> 0 OR bdsn_vote <>0 OR bdsn_montant <> 0);

  CURSOR gestion(niveau INTEGER) IS
  SELECT b.*
  FROM BUDGET_SAISIE_GESTION b,
    v_organ o
  WHERE b.bdsa_id = bdsaid
   AND b.tyet_id NOT IN(2)
   AND b.org_id = o.org_id
   AND o.org_niv = niveau
   AND (bdsg_saisi <> 0 OR bdsg_vote <> 0 OR bdsg_montant <> 0);

  orgnivcursor INTEGER;
  orgnivid INTEGER;
  orgidconsolidation INTEGER;
  BEGIN
  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;

  SELECT * INTO currentBudgetSaisie FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;

  IF (currentBudgetSaisie.tyet_id NOT IN (2))
  THEN

    -- remise a zero des consolidations
    Budget_Moteur.vider_consolidation(bdsaid);
    Budget_Moteur.creer_consolidation(bdsaid);

    END IF;

    -- Algo : faire la consolidation au niveau composante des niveaux 3 et 4
    -- attention niv 2 = omme(niv2)+ somme(niv3)+ somme(niv4)
    -- un niveau 3 n est pas la somme des niveaux 4 : CONVENTIONS (saisies)
    -- niveau 1 = somme des niveaux 2
    -- niveau 0 = somme des niveaux 1

    orgnivcursor := 4;
    orgnivid := 2;

    LOOP

      OPEN nature(orgnivcursor);
      LOOP
        FETCH nature
        INTO lignenature;
        EXIT
      WHEN nature % NOTFOUND;

      orgidconsolidation := Budget_Moteur.get_orgid_niv_orgid(lignenature.org_id,   orgnivid);

      -- pour les orgniv de niveau 0 1 2 on utilise le chapitre du VOTE !

      Budget_Moteur.set_ligne_nature_calcul(lignenature.bdsn_id,orgidconsolidation);

    END LOOP;

    CLOSE nature;

    OPEN gestion(orgnivcursor);
    LOOP
      FETCH gestion
      INTO lignegestion;
      EXIT
    WHEN gestion % NOTFOUND;

    orgidconsolidation := Budget_Moteur.get_orgid_niv_orgid(lignegestion.org_id,   orgnivid);
    Budget_Moteur.set_ligne_gestion_calcul(lignegestion.bdsg_id,orgidconsolidation);

    END LOOP;

  CLOSE gestion;

  orgnivcursor := orgnivcursor -1;

  IF orgnivcursor >= 3 THEN
    orgnivid := 2;
  ELSE
    orgnivid := orgnivid -1;
  END IF;

  IF orgnivcursor = 0 THEN
    EXIT;
  END IF;

END LOOP;

consolider_etablissement(bdsaid);

 calculer_caf_iaf_fdr(bdsaid);

 calculer_caf_iaf_fdr_saisi(bdsaid);

END;



/*******************************************
CONSOLIDER_ETABLISSEMENT
Consolidation du budget pour les niveaux 0,1. Puis calcul des CAF, IAF et Fonds de roulement.
*******************************************/
PROCEDURE consolider_etablissement(bdsaid INTEGER) IS
  cpt INTEGER;

  lignenature BUDGET_CALCUL_NATURE % ROWTYPE;
  lignegestion BUDGET_CALCUL_GESTION % ROWTYPE;

  CURSOR nature(niveau INTEGER) IS
  SELECT b.*
  FROM BUDGET_CALCUL_NATURE b,
    v_organ o
  WHERE b.bdsa_id = bdsaid
   AND b.tyet_id NOT IN(2)
   AND b.org_id = o.org_id
   AND o.org_niv = niveau
   AND (bdcn_vote <> 0 OR bdcn_calcul <> 0 OR bdcn_calcul <> 0);

  CURSOR gestion(niveau INTEGER) IS
  SELECT b.*
  FROM BUDGET_CALCUL_GESTION b,
    v_organ o
  WHERE b.bdsa_id = bdsaid
   AND b.tyet_id NOT IN(2)
   AND b.org_id = o.org_id
   AND o.org_niv = niveau
   AND (bdcg_vote <> 0 OR bdcg_saisi <> 0 OR bdcg_calcul <> 0);

  orgnivcursor INTEGER;
  orgnivid INTEGER;
  orgidconsolidation INTEGER;

chapitrevote BUDGET_MASQUE_NATURE.pco_num%TYPE;

  BEGIN
  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;
    -- Algo : faire la consolidation au niveau Univ et Etablissement
    -- niveau 1 = somme des niveaux 2
    -- niveau 0 = somme des niveaux 1

    orgnivcursor := 2;
    orgnivid := 1;

    LOOP

      OPEN nature(orgnivcursor);
      LOOP
        FETCH nature
        INTO lignenature;
        EXIT
      WHEN nature % NOTFOUND;
              orgidconsolidation := Budget_Moteur.get_orgid_niv_orgid(lignenature.org_id,   orgnivid);
              chapitrevote:=Budget_Moteur.get_chapitre_vote( lignenature.pco_num,lignenature.exe_ordre, lignenature.tcd_ordre);

                SELECT COUNT(*) INTO cpt
                FROM BUDGET_CALCUL_NATURE
                WHERE bdsa_id = bdsaid
                AND org_id = orgidconsolidation
                 AND tcd_ordre = lignenature.tcd_ordre
                 AND pco_num = chapitrevote
                 AND exe_ordre = lignenature.exe_ordre;

                 IF cpt = 0 THEN
                   INSERT
                   INTO BUDGET_CALCUL_NATURE(bdcn_id,   bdcn_calcul,  bdsa_id,   org_id,   tcd_ordre,   exe_ordre,   pco_num,   tyet_id, BDCN_VOTE, BDCN_SAISI)
                   VALUES(
                   budget_calcul_nature_seq.NEXTVAL,   --BDCN_ID,
                   0,   --BDCN_XXX,
                   lignenature.bdsa_id,   --BDSA_ID,
                   orgidconsolidation,   --ORG_ID,
                   lignenature.tcd_ordre,   --TCD_ORDRE,
                   lignenature.exe_ordre,   --EXE_ORDRE,
                  chapitrevote,   --PCO_NUM,
                   103,
                   0,
                   0
                   );
             END IF;

                UPDATE BUDGET_CALCUL_NATURE
                SET bdcn_calcul = bdcn_calcul + lignenature.bdcn_calcul,
--ajout du 09/01/2007
bdcn_saisi =bdcn_saisi + lignenature.bdcn_saisi ,
bdcn_vote = bdcn_vote + lignenature.bdcn_vote
                WHERE bdsa_id = bdsaid
                AND org_id = orgidconsolidation
                 AND tcd_ordre = lignenature.tcd_ordre
                 AND pco_num = chapitrevote
                  AND exe_ordre = lignenature.exe_ordre;

    END LOOP;

    CLOSE nature;


    -- Gestion
    OPEN gestion(orgnivcursor);
    LOOP
      FETCH gestion
      INTO lignegestion;
      EXIT
    WHEN gestion % NOTFOUND;

                  orgidconsolidation := Budget_Moteur.get_orgid_niv_orgid(lignegestion.org_id,   orgnivid);

                SELECT COUNT(*)
                INTO cpt
                FROM BUDGET_CALCUL_GESTION
                WHERE bdsa_id = bdsaid
                AND org_id = orgidconsolidation
                 AND tcd_ordre = lignegestion.tcd_ordre
                 AND tyac_id = lignegestion.tyac_id
                 AND exe_ordre = lignegestion.exe_ordre;

                IF cpt = 0 THEN

                INSERT
                INTO BUDGET_CALCUL_GESTION(bdcg_id,   bdcg_calcul,   bdsa_id,   org_id,   tcd_ordre,   exe_ordre,   tyac_id,   tyet_id, BDCG_VOTE, BDCG_SAISI)
                VALUES(budget_calcul_gestion_seq.NEXTVAL,   --BDcG_ID,
                0,   --BDCG_XXX,
                lignegestion.bdsa_id,   --BDSA_ID,
                orgidconsolidation,   --ORG_ID,
                lignegestion.tcd_ordre,   --TCD_ORDRE,
                lignegestion.exe_ordre,   --EXE_ORDRE,
                lignegestion.tyac_id,   --TYAC_ID,
                103,
                0,
                0
                );

                END IF;

                UPDATE BUDGET_CALCUL_GESTION
                SET bdcg_calcul = bdcg_calcul + lignegestion.bdcg_calcul,
--ajout du 09/01/2007
bdcg_saisi =bdcg_saisi + lignegestion.bdcg_saisi ,
bdcg_vote = bdcg_vote + lignegestion.bdcg_vote
                WHERE bdsa_id = bdsaid
                AND org_id = orgidconsolidation
                 AND tcd_ordre = lignegestion.tcd_ordre
                 AND tyac_id = lignegestion.tyac_id
                 AND exe_ordre = lignegestion.exe_ordre;


    END LOOP;

  CLOSE gestion;


  orgnivcursor := orgnivcursor -1;
  orgnivid := orgnivid -1;

    IF orgnivcursor = 0 THEN
    EXIT;
  END IF;

END LOOP;

calculer_caf_iaf_fdr(bdsaid);
calculer_caf_iaf_fdr_saisi(bdsaid);

UPDATE BUDGET_CALCUL_NATURE SET bdcn_vote = 0 WHERE pco_num  LIKE '0%';

END;


/****************************************************
CALCULER_CAF_IAF_FDR
*****************************************************/
PROCEDURE calculer_caf_iaf_fdr(bdsaid INTEGER)
IS

  CURSOR organs IS
  SELECT org_id FROM jefy_admin.organ WHERE org_niv < 3;

  currentOrgan jefy_admin.organ%ROWTYPE;
  currentBudget BUDGET_SAISIE%ROWTYPE;

  orgid INTEGER;

somme_60_66 NUMBER;
somme_67_68 NUMBER;
somme_675_68 NUMBER;
somme_70_76 NUMBER;
somme_77_78 NUMBER;
somme_771_775_776_777_78 NUMBER;
somme_775 NUMBER;

somme_classe1 NUMBER;
somme_classe2 NUMBER;

resultatBrutDepense NUMBER;
resultatBrutRecette NUMBER;

caf NUMBER;
iaf NUMBER;

augmentationFDR NUMBER;
diminutionFDR NUMBER;

nb integer;
param budget_parametres.BPAR_VALUE%type;

BEGIN

  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;

SELECT * INTO currentBudget FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;

-- On traite chaque ligne budgetaire de niveau 0,1 et 2.
    OPEN organs;
    LOOP  FETCH organs  INTO orgid;
     EXIT  WHEN organs % NOTFOUND;

     SELECT * INTO currentOrgan FROM jefy_admin.organ WHERE org_id = orgid;

SELECT SUM(bdcn_calcul) INTO somme_60_66 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '60%' OR pco_num LIKE '61%' OR pco_num LIKE '62%' OR pco_num LIKE '63%'
OR pco_num LIKE '64%' OR pco_num LIKE '65%' OR pco_num LIKE '66%' OR pco_num LIKE '69%' );

SELECT SUM(bdcn_calcul) INTO somme_67_68 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '67%' OR pco_num LIKE '68%');

SELECT SUM(bdcn_calcul) INTO somme_675_68 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '675%' OR pco_num LIKE '68%');

SELECT SUM(bdcn_calcul) INTO somme_70_76 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '70%' OR pco_num LIKE '71%' OR pco_num LIKE '72%' OR pco_num LIKE '73%'
OR pco_num LIKE '74%' OR pco_num LIKE '75%' OR pco_num LIKE '76%' OR pco_num LIKE '79%');

SELECT SUM(bdcn_calcul) INTO somme_77_78 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '77%' OR pco_num LIKE '78%');

select count(*) into nb from budget_parametres where bpar_key='777_EN_PRODUIT' and exe_ordre=currentBudget.exe_ordre;
if nb=0 then param:='NON';
else
  select bpar_value into param from budget_parametres where bpar_key='777_EN_PRODUIT' and exe_ordre=currentBudget.exe_ordre;
end if;

if param='NON' then
  SELECT nvl(SUM(bdcn_calcul),0) INTO somme_771_775_776_777_78 FROM BUDGET_CALCUL_NATURE
    WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '775%' OR pco_num LIKE '776%' OR pco_num LIKE '777%'  OR pco_num LIKE '78%');
else
  SELECT nvl(SUM(bdcn_calcul),0) INTO somme_771_775_776_777_78 FROM BUDGET_CALCUL_NATURE
    WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '775%' OR pco_num LIKE '776%' /*OR pco_num LIKE '777%' */ OR pco_num LIKE '78%');
end if;

select count(*) into nb from budget_parametres where bpar_key='775_FDR' and exe_ordre=currentBudget.exe_ordre;
if nb=0 then param:='NON';
else
  select bpar_value into param from budget_parametres where bpar_key='775_FDR' and exe_ordre=currentBudget.exe_ordre;
end if;

if param='NON' then
  somme_775:=0;
else
  SELECT nvl(SUM(bdcn_calcul),0) INTO somme_775 FROM BUDGET_CALCUL_NATURE WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '775%');
end if;


-- CLASSE 1 : Types de credit RECETTE section 2
SELECT  SUM(bdcn_calcul)  INTO somme_classe1
FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid
AND tcd_ordre IN (SELECT tcd_ordre FROM jefy_admin.type_credit WHERE tcd_type = 'RECETTE' AND tcd_sect = 2 AND tcd_code NOT IN ('00'));

-- CLASSE 2 : Types de credit DEPENSE section 2
SELECT  SUM(bdcn_calcul) INTO somme_classe2
FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid
AND tcd_ordre IN (SELECT tcd_ordre FROM jefy_admin.type_credit WHERE tcd_type = 'DEPENSE' AND tcd_sect = 2 AND tcd_code NOT IN ('00'));

resultatBrutDepense := (NVL(somme_70_76,0) + NVL(somme_77_78, 0)) - (NVL(somme_60_66,0) + NVL(somme_67_68,0));

IF (resultatBrutDepense < 0) THEN resultatBrutDepense := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = resultatBrutDepense
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_EXCEDENT_FONC');

resultatBrutRecette := (NVL(somme_60_66,0)  + NVL(somme_67_68,0) ) - (NVL(somme_70_76,0)  + NVL(somme_77_78,0) );
IF (resultatBrutRecette < 0) THEN resultatBrutRecette := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = resultatBrutRecette
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_DEFICIT_FONC');


/*IF (currentOrgan.org_niv = 0)
THEN
RAISE_APPLICATION_ERROR(-20001, '7076 : '||somme_70_76||' - 77_78 : '||somme_77_78||'  -  60_66 : '
||somme_60_66||' -  67_68 : '||somme_67_68 );
END IF;
*/

-- CAF
caf := ((NVL(somme_70_76,0) + NVL(somme_77_78,0)) - (NVL(somme_60_66,0) + NVL(somme_67_68,0))) + NVL(somme_675_68,0) - NVL(somme_771_775_776_777_78,0);

-- IAF
iaf := -caf;

IF (caf < 0) THEN caf := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = caf
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_CAF');

IF (iaf < 0) THEN iaf := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = iaf
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_IAF');

-- FDR : Augmentation + Diminution
augmentationFDR := (caf + nvl(somme_classe1,0)+ somme_775) - (iaf + nvl(somme_classe2,0) );
IF (augmentationFDR < 0) THEN augmentationFDR := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = NVL(augmentationFDR, 0)
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_AUGMENTATION_FDR');

diminutionFDR := (iaf + NVL(somme_classe2, 0)) - (caf + NVL(somme_classe1, 0)+somme_775)  ;

IF (diminutionFDR < 0) THEN diminutionFDR := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = NVL(diminutionFDR, 0)
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE  exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_DIMINUTION_FDR');

    END LOOP;
  CLOSE organs;

END;




/****************************************************
CALCULER_CAF_IAF_FDR
*****************************************************/
PROCEDURE calculer_caf_iaf_fdr_saisi(bdsaid INTEGER)
IS

  CURSOR organs IS
  SELECT org_id FROM jefy_admin.organ WHERE org_niv < 3;

  currentOrgan jefy_admin.organ%ROWTYPE;
  currentBudget BUDGET_SAISIE%ROWTYPE;

  orgid INTEGER;

somme_60_66 NUMBER;
somme_67_68 NUMBER;
somme_675_68 NUMBER;
somme_70_76 NUMBER;
somme_77_78 NUMBER;
somme_771_775_776_777_78 NUMBER;
somme_775 number;

somme_classe1 NUMBER;
somme_classe2 NUMBER;

resultatBrutDepense NUMBER;
resultatBrutRecette NUMBER;

caf NUMBER;
iaf NUMBER;

augmentationFDR NUMBER;
diminutionFDR NUMBER;

nb integer;
param budget_parametres.BPAR_VALUE%type;

BEGIN

  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;

SELECT * INTO currentBudget FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;

-- On traite chaque ligne budgetaire de niveau 0,1 et 2.
    OPEN organs;
    LOOP  FETCH organs  INTO orgid;
     EXIT  WHEN organs % NOTFOUND;

     SELECT * INTO currentOrgan FROM jefy_admin.organ WHERE org_id = orgid;

SELECT SUM(bdcn_saisi) INTO somme_60_66 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '60%' OR pco_num LIKE '61%' OR pco_num LIKE '62%' OR pco_num LIKE '63%'
OR pco_num LIKE '64%' OR pco_num LIKE '65%' OR pco_num LIKE '66%' OR pco_num LIKE '69%');

SELECT SUM(bdcn_saisi) INTO somme_67_68 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '67%' OR pco_num LIKE '68%');

SELECT SUM(bdcn_saisi) INTO somme_675_68 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '675%' OR pco_num LIKE '68%');

SELECT SUM(bdcn_saisi) INTO somme_70_76 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '70%' OR pco_num LIKE '71%' OR pco_num LIKE '72%' OR pco_num LIKE '73%'
OR pco_num LIKE '74%' OR pco_num LIKE '75%' OR pco_num LIKE '76%' OR pco_num LIKE '79%');

SELECT SUM(bdcn_saisi) INTO somme_77_78 FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '77%' OR pco_num LIKE '78%');

select count(*) into nb from budget_parametres where bpar_key='777_EN_PRODUIT' and exe_ordre=currentBudget.exe_ordre;
if nb=0 then param:='NON';
else
  select bpar_value into param from budget_parametres where bpar_key='777_EN_PRODUIT' and exe_ordre=currentBudget.exe_ordre;
end if;

if param='NON' then
  SELECT nvl(SUM(bdcn_saisi),0) INTO somme_771_775_776_777_78 FROM BUDGET_CALCUL_NATURE
    WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '775%' OR pco_num LIKE '776%' OR pco_num LIKE '777%'  OR pco_num LIKE '78%');
else
  SELECT nvl(SUM(bdcn_saisi),0) INTO somme_771_775_776_777_78 FROM BUDGET_CALCUL_NATURE
    WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '775%' OR pco_num LIKE '776%' /*OR pco_num LIKE '777%' */ OR pco_num LIKE '78%');
end if;

select count(*) into nb from budget_parametres where bpar_key='775_FDR' and exe_ordre=currentBudget.exe_ordre;
if nb=0 then param:='NON';
else
  select bpar_value into param from budget_parametres where bpar_key='775_FDR' and exe_ordre=currentBudget.exe_ordre;
end if;

if param='NON' then
  somme_775:=0;
else
  SELECT nvl(SUM(bdcn_saisi),0) INTO somme_775 FROM BUDGET_CALCUL_NATURE WHERE bdsa_id = bdsaid AND org_id = orgid AND (pco_num LIKE '775%');
end if;

-- CLASSE 1 : Types de credit RECETTE section 2
SELECT  SUM(bdcn_saisi)  INTO somme_classe1
FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid
AND tcd_ordre IN (SELECT tcd_ordre FROM jefy_admin.type_credit WHERE tcd_type = 'RECETTE' AND tcd_sect = 2 AND tcd_code NOT IN ('00'));

-- CLASSE 2 : Types de credit DEPENSE section 2
SELECT  SUM(bdcn_saisi) INTO somme_classe2
FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid AND org_id = orgid
AND tcd_ordre IN (SELECT tcd_ordre FROM jefy_admin.type_credit WHERE tcd_type = 'DEPENSE' AND tcd_sect = 2 AND tcd_code NOT IN ('00'));

resultatBrutDepense := (NVL(somme_70_76,0) + NVL(somme_77_78, 0)) - (NVL(somme_60_66,0) + NVL(somme_67_68,0));

IF (resultatBrutDepense < 0) THEN resultatBrutDepense := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_saisi = resultatBrutDepense
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_EXCEDENT_FONC');

resultatBrutRecette := (NVL(somme_60_66,0)  + NVL(somme_67_68,0) ) - (NVL(somme_70_76,0)  + NVL(somme_77_78,0) );
IF (resultatBrutRecette < 0) THEN resultatBrutRecette := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_saisi = resultatBrutRecette
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_DEFICIT_FONC');


/*IF (currentOrgan.org_niv = 0)
THEN
RAISE_APPLICATION_ERROR(-20001, '7076 : '||somme_70_76||' - 77_78 : '||somme_77_78||'  -  60_66 : '
||somme_60_66||' -  67_68 : '||somme_67_68 );
END IF;
*/

-- CAF
caf := ((NVL(somme_70_76,0) + NVL(somme_77_78,0)) - (NVL(somme_60_66,0) + NVL(somme_67_68,0))) + NVL(somme_675_68,0) - NVL(somme_771_775_776_777_78,0);

-- IAF
iaf := -caf;

IF (caf < 0) THEN caf := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_saisi = caf
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_CAF');

IF (iaf < 0) THEN iaf := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_saisi = iaf
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_IAF');

-- FDR : Augmentation + Diminution
augmentationFDR := (caf + nvl(somme_classe1,0)+ somme_775) - (iaf + nvl(somme_classe2,0) );
IF (augmentationFDR < 0) THEN augmentationFDR := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_saisi = NVL(augmentationFDR, 0)
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_AUGMENTATION_FDR');

diminutionFDR := (iaf + NVL(somme_classe2, 0) ) - (caf + NVL(somme_classe1, 0)+ somme_775)  ;

IF (diminutionFDR < 0) THEN diminutionFDR := 0; END IF;
UPDATE BUDGET_CALCUL_NATURE
SET bdcn_saisi = NVL(diminutionFDR, 0)
WHERE bdsa_id = bdsaid AND org_id = orgid AND pco_num = (SELECT bpar_value FROM BUDGET_PARAMETRES
WHERE  exe_ordre  = currentBudget.exe_ordre AND bpar_key = 'COMPTE_DIMINUTION_FDR');

    END LOOP;
  CLOSE organs;

END;





/****************************************************
CONTROLER_SAISIE_BUDGET
*****************************************************/
PROCEDURE controler_saisie_budget(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- VERIFIER LES POSITIFS PROV INIT RELIQ

END;



/*************************************************
FERMER_SAISIE_BUDGET
***********************************************/
PROCEDURE fermer_saisie_budget(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- VERIFIER OUVERTURE ET TYPE DE LA SAISIE
END;


/*************************************************
INITIALISER_BUDGET_DBM

Initialisation d'un budget DBM avec le budget vote en cours.
On initialise tous les CR avec les colonnes bdvg_votes et bdvn_votes des tables budget_vote_gestion et budget_vote_nature.

***********************************************/
PROCEDURE initialiser_budget_dbm(bdsaid INTEGER) IS

current_budget_saisie BUDGET_SAISIE%ROWTYPE;

cpt INTEGER;

BEGIN

DELETE FROM BUDGET_SAISIE_GESTION  WHERE bdsa_id = bdsaid;
DELETE FROM BUDGET_SAISIE_NATURE  WHERE bdsa_id = bdsaid;

SELECT * INTO current_budget_saisie FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;

INSERT INTO BUDGET_SAISIE_GESTION
(BDSG_ID, BDSA_ID, ORG_ID, TCD_ORDRE, TYAC_ID, BDSG_VOTE, BDSG_SAISI, BDSG_MONTANT, EXE_ORDRE, TYET_ID)
SELECT
BUDGET_SAISIE_GESTION_seq.NEXTVAL, bdsaid, ORG_ID, TCD_ORDRE, TYAC_ID, BDvg_votes, 0, BDvg_votes, EXE_ORDRE, 100
FROM BUDGET_VOTE_GESTION WHERE exe_ordre = current_budget_saisie.exe_ordre;

INSERT INTO BUDGET_SAISIE_NATURE
(BDSN_ID, BDSA_ID, ORG_ID, TCD_ORDRE, PCO_NUM, BDSN_VOTE, BDSN_SAISI, BDSN_MONTANT, EXE_ORDRE, TYET_ID)
SELECT
BUDGET_SAISIE_NATURE_seq.NEXTVAL, bdsaid, ORG_ID, TCD_ORDRE, PCO_NUM, BDvN_VOTEs, 0, BDVN_votes, EXE_ORDRE, 100
FROM BUDGET_VOTE_NATURE WHERE exe_ordre = current_budget_saisie.exe_ordre;

END;


/*************************************************
INITIALISER_BUDGET_RELIQUAT
***********************************************/
PROCEDURE initialiser_budget_reliquat(bdsaid INTEGER) IS

current_budget_saisie BUDGET_SAISIE%ROWTYPE;
cpt INTEGER;

BEGIN

DELETE FROM BUDGET_SAISIE_GESTION  WHERE bdsa_id = bdsaid;
DELETE FROM BUDGET_SAISIE_NATURE  WHERE bdsa_id = bdsaid;

SELECT * INTO current_budget_saisie FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;

INSERT INTO BUDGET_SAISIE_GESTION
(BDSG_ID, BDSA_ID, ORG_ID, TCD_ORDRE, TYAC_ID, BDSG_VOTE, BDSG_SAISI, BDSG_MONTANT, EXE_ORDRE, TYET_ID)
SELECT
BUDGET_SAISIE_GESTION_seq.NEXTVAL, bdsaid, ORG_ID, TCD_ORDRE, TYAC_ID, BDvg_votes, 0, BDvg_votes, EXE_ORDRE, 100
FROM BUDGET_VOTE_GESTION WHERE exe_ordre = current_budget_saisie.exe_ordre;

INSERT INTO BUDGET_SAISIE_NATURE
(BDSN_ID, BDSA_ID, ORG_ID, TCD_ORDRE, PCO_NUM, BDSN_VOTE, BDSN_SAISI, BDSN_MONTANT, EXE_ORDRE, TYET_ID)
SELECT
BUDGET_SAISIE_NATURE_seq.NEXTVAL, bdsaid, ORG_ID, TCD_ORDRE, PCO_NUM, BDvN_VOTEs, 0, BDVN_votes, EXE_ORDRE, 100
FROM BUDGET_VOTE_NATURE WHERE exe_ordre = current_budget_saisie.exe_ordre;


END;



/**************************************
VOTER_SAISIE_BUDGET
*************************************/
PROCEDURE voter_saisie_budget(bdsaid INTEGER) IS
cpt INTEGER;

id INTEGER;
exeordre integer;
bparvalue budget_parametres.bpar_value%type;

CURSOR c_nature IS SELECT bdsn_id FROM BUDGET_SAISIE_NATURE WHERE bdsa_id = bdsaid;
CURSOR c_nature_lolf IS SELECT bdsl_id FROM BUDGET_SAISIE_NATURE_LOLF WHERE bdsa_id = bdsaid and bdsl_saisi<>0;
CURSOR c_gestion IS SELECT bdsg_id FROM BUDGET_SAISIE_GESTION WHERE bdsa_id = bdsaid;

BEGIN

-- TODO
-- controle des masses par niveau
-- controle des chap budgetaire par niveau
-- niveau 1 = niveau 2 = (niveau 3 + niveau4)
-- etc...
  -- lock des tables
  Budget_Moteur.lock_table_budget_vote;
  Budget_Moteur.lock_table_budget_saisie;

-- verifier SI TOUTES LES SAISIES SONT CLOTUREES
SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_NATURE WHERE bdsa_id = bdsaid AND tyet_id IN(100, 1, 101, 104);
IF cpt != 0 THEN
  RAISE_APPLICATION_ERROR(-20001,   'BUDGET NATURE - LES SAISIES NE SONT PAS TOUTES CLOTUREES');
END IF;

SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_GESTION WHERE bdsa_id = bdsaid AND tyet_id IN(100, 1, 101, 104);
IF cpt != 0 THEN
  RAISE_APPLICATION_ERROR(-20001,   'BUDGET GESTION - LES SAISIES NE SONT PAS TOUTES CLOTUREES');
END IF;

SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_GESTION WHERE bdsa_id = bdsaid AND tyet_id IN(100, 1, 101, 104);
IF cpt != 0 THEN
  RAISE_APPLICATION_ERROR(-20001,   'BUDGET GESTION - LES SAISIES NE SONT PAS TOUTES CLOTUREES');
END IF;

select exe_ordre into exeordre from budget_saisie where bdsa_id=bdsaid;

select count(*) into cpt from budget_parametres where exe_ordre=exeordre and bpar_key='REPART_NATURE_LOLF';
if cpt=1 then
    select bpar_value into bparvalue from budget_parametres where exe_ordre=exeordre and bpar_key='REPART_NATURE_LOLF';
    if bparvalue='OUI' then
        SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_nature_lolf WHERE bdsa_id = bdsaid;
        IF cpt = 0 THEN
          RAISE_APPLICATION_ERROR(-20001, 'REPARTITION NATURE/LOLF - Il faut saisir cette repartition avant de voter le budget');
        end if;

        SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_nature_lolf WHERE bdsa_id = bdsaid AND tyet_id<>budget_etat.get_typecloture;
        IF cpt != 0 THEN
          RAISE_APPLICATION_ERROR(-20001, 'REPARTITION NATURE/LOLF - LES SAISIES NE SONT PAS TOUTES CLOTUREES');

          DELETE FROM BUDGET_SAISIE_NATURE_LOLF WHERE bdsa_id=bdsaid AND tyet_id=budget_etat.get_typeannule;
          UPDATE BUDGET_SAISIE_NATURE_LOLF SET tyet_id=104 WHERE tyet_id=budget_etat.get_typecloture AND bdsa_id=bdsaid;

          OPEN c_nature_lolf;
          LOOP
             FETCH c_nature_lolf INTO id;
             EXIT WHEN c_nature_lolf%NOTFOUND;
             Budget_Moteur.set_ligne_nature_lolf_vote(id);
          END LOOP;
          CLOSE c_nature_lolf;
        END IF;
    end if;
end if;


-- ON SUPPRIME LES SAISIES ANNULEES
DELETE FROM BUDGET_SAISIE_NATURE WHERE bdsa_id = bdsaid AND tyet_id = 2;
DELETE FROM BUDGET_SAISIE_GESTION WHERE bdsa_id = bdsaid AND tyet_id = 2;

-- REFAIRE UNE DERNIERE CONSOLIDATION
--Budget_Moteur.consolider_saisiebudget(bdsaid);

-- CAF IAF IAF ... sont a l etat CLOTURE !!!!!

-- VERIFIER DEPENSE = RECETTE (NATURE)
Budget_Moteur.verifier_dep_rec_nature(bdsaid);

-- VERIFIER DEPENSE GESTION = DEPENSE NATURE
Budget_Moteur.verifier_dep_nature_gestion(bdsaid);

-- PASSAGE A L ETAT VOTE ... DATE_VOTE

UPDATE BUDGET_SAISIE_NATURE SET tyet_id = 104 WHERE tyet_id = 102 AND bdsa_id = bdsaid;
UPDATE BUDGET_SAISIE_GESTION SET tyet_id = 104 WHERE tyet_id = 102 AND bdsa_id = bdsaid;


OPEN c_nature;
LOOP
  FETCH c_nature INTO id;
  EXIT WHEN c_nature % NOTFOUND;
Budget_Moteur.set_ligne_nature_vote(id);
-- TODO CONTROLE DISPO
END LOOP;
CLOSE c_nature;

OPEN c_gestion;
LOOP
FETCH c_gestion INTO id;
EXIT WHEN c_gestion % NOTFOUND;
Budget_Moteur.set_ligne_gestion_vote(id);
Budget_Moteur.set_budfonc(id);
-- TODO CONTROLE DISPO
END LOOP;
CLOSE c_gestion;

Budget_Moteur.voter_saisie_budget_calcul(bdsaid);

-- budget_saisie : date de vote et etat VOTE
UPDATE BUDGET_SAISIE SET bdsa_date_validation = SYSDATE, tyet_id = 104 WHERE bdsa_id = bdsaid;

-- verification au cas ou ....
DELETE FROM jefy_budget.BUDGET_EXEC_CREDIT WHERE TCD_ORDRE IN
  (SELECT tcd_ordre FROM jefy_admin.TYPE_CREDIT WHERE TCD_TYPE != 'DEPENSE');

DELETE FROM jefy_budget.BUDGET_EXEC_CREDIT_CONV WHERE TCD_ORDRE IN
  (SELECT tcd_ordre FROM jefy_admin.TYPE_CREDIT WHERE TCD_TYPE != 'DEPENSE');

UPDATE BUDGET_EXEC_CREDIT SET bdxc_disponible=bdxc_ouverts -bdxc_engagements -bdxc_mandats WHERE exe_ordre = exeordre;
UPDATE BUDGET_EXEC_CREDIT_CONV SET  becc_disponible = becc_ouverts -becc_engagements -becc_mandats WHERE exe_ordre = exeordre;

Maj_Comptes_Regul_Votes(exeordre);

END;



/**************************************************
VOTER_SAISIE_BUDGET_CALCUL
***************************************************/
PROCEDURE voter_saisie_budget_calcul(bdsaid INTEGER) IS
cpt INTEGER;

id INTEGER;
CURSOR c_nature IS
SELECT bdcn_id
FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid;

CURSOR c_gestion IS
SELECT bdcg_id
FROM BUDGET_CALCUL_GESTION
WHERE bdsa_id = bdsaid;

BEGIN

Budget_Moteur.lock_table_budget_vote;
Budget_Moteur.lock_table_budget_saisie;

OPEN c_nature;
LOOP
  FETCH c_nature
  INTO id;
  EXIT
WHEN c_nature % NOTFOUND;
 Budget_Moteur.set_ligne_nature_vote_calcul(id);
END LOOP;

CLOSE c_nature;

OPEN c_gestion;
LOOP
FETCH c_gestion
INTO id;
EXIT
WHEN c_gestion % NOTFOUND;
 Budget_Moteur.set_ligne_gestion_vote_calcul(id);
END LOOP;
CLOSE c_gestion;

END;


/************************************************
REFUSER_SAISIE_BUDGET
*************************************************/
PROCEDURE refuser_saisie_budget(bdsaid INTEGER) IS
budgetsaisie BUDGET_SAISIE%ROWTYPE;
BEGIN


SELECT * INTO budgetsaisie
FROM BUDGET_SAISIE
WHERE bdsa_id = bdsaid;

IF (budgetsaisie.tysa_id = 2)
THEN
UPDATE BUDGET_SAISIE SET tyet_id = Budget_Etat.get_typeannule
WHERE tysa_id = 1;
END IF;

-- si TYAC_ID = 1 PROVISOIRE on passe en INITIAL = 2
IF (budgetsaisie.tysa_id = 1) THEN
  Budget_Moteur.changer_saisie_budget(bdsaid,2);
END IF;


-- si TYAC_ID = 2 INITIAL on passe en PROVISOIRE = 1
IF (budgetsaisie.tysa_id = 2) THEN
 Budget_Moteur.changer_saisie_budget(bdsaid,1);
END IF;


END;



/*************************************************
CHANGER_SAISIE_BUDGET
************************************************/
PROCEDURE changer_saisie_budget(bdsaid INTEGER,tysaid INTEGER) IS
 budgetsaisie BUDGET_SAISIE%ROWTYPE;
 cpt INTEGER;
BEGIN
 SELECT * INTO budgetsaisie
 FROM BUDGET_SAISIE
 WHERE bdsa_id = bdsaid;

IF (tysaid IN(1,2,3)) THEN
 SELECT COUNT(*) INTO cpt
 FROM BUDGET_SAISIE
 WHERE tysa_id = tysaid AND tyet_id != Budget_Etat.get_typeannule
 AND exe_ordre = budgetsaisie.exe_ordre;


-- on n a pas deux saisies  init prov ou reliq
 IF (cpt > 0) THEN
  RAISE_APPLICATION_ERROR(-20001,   'IMPOSSIBLE D AVOIR DEUX  SAISIE DE MEME TYPE ');
 END IF;

END IF;

-- on ne modifie pas une saisie de CONVENTION OU DBM
 IF (budgetsaisie.tysa_id IN(4,5)) THEN
  RAISE_APPLICATION_ERROR(-20001,   'IMPOSSIBLE DE MODIFIER UNE SAISIE DE DBM OU CONVENTION');
 END IF;



-- on ne modifie pas les saisies deja vote
 IF (budgetsaisie.tyet_id = 104) THEN
  RAISE_APPLICATION_ERROR(-20001,   'IMPOSSIBLE DE MODIFIER UNE SAISIE VOTE');
 END IF;



 UPDATE BUDGET_SAISIE SET tysa_id = tysaid
 WHERE bdsa_id = bdsaid;


END;

/****************************************
VERIF_DISPO_BDSA_ID
***************************************/
FUNCTION verif_dispo_bdsaid (bdsaid INTEGER) RETURN VARCHAR
IS
 cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;


RETURN 'OK';
END;



/****************************************
VERIF_DISPO_BDSN_ID
***************************************/
FUNCTION verif_dispo_bdsnid (bdsnid INTEGER) RETURN VARCHAR
IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- verifier si bdvn_vote = bdsn_vote (pco/exe/tcd)

-- verifier si bdvn_ouvert + bdsn_saisie > 0 (pco/exe/tcd)


RETURN 'OK';
END;



/****************************************
VERIF_DISPO_BDSGID
***************************************/
FUNCTION verif_dispo_bdsgid (bdsgid INTEGER) RETURN VARCHAR
IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- verifier si bdvg_vote = bdsg_vote (tyac/exe/tcd)

-- verifier si bdvg_ouvert + bdsg_saisie > 0 (tyac/exe/tcd)

-- verifier budfonc_exec_credit ou budfonc_exec_conv

RETURN 'OK';
END;





/****************************************
CONTROLE_ETAT
***************************************/
PROCEDURE controle_etat(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- VERIFIER QUE LES ETATS SONT BIEN COHERENTS
END;



/****************************************
CONTROLE_ORGAN
***************************************/
PROCEDURE controle_organ(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;
END;




/****************************************
CONTROLE_IMPUTATION
***************************************/
PROCEDURE controle_imputation(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- verifier les imputations NON VALIDE

-- VERIFIER LES AUTORISATION TCD_CODE - IMPUTATION
END;



/****************************************
CONTROLE_ACTIONS
***************************************/
PROCEDURE controle_actions(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- VERIFIER QUE LES ACTIONS SONT VALIDES

END;




/******************************************
CONTROLE_EXERCICE
*****************************************/
PROCEDURE controle_exercice(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- VERIFIER LE DEROULEMENT DES ETAPES BUDGETAIRES.
END;

PROCEDURE set_bds_vote(bdsaid INTEGER) IS
cpt INTEGER;
bdsnid INTEGER;
bdsgid INTEGER;

CURSOR gestion IS SELECT bdsg_id FROM BUDGET_SAISIE_GESTION
WHERE bdsa_id = bdsaid;

CURSOR nature IS SELECT bdsn_id FROM BUDGET_SAISIE_NATURE
WHERE bdsa_id = bdsaid;


BEGIN
OPEN gestion;
LOOP
FETCH gestion INTO bdsgid;
EXIT WHEN gestion%NOTFOUND;
Budget_Moteur.set_bdsg_vote(bdsgid);
END LOOP;
CLOSE gestion;

OPEN nature;
LOOP
FETCH nature INTO bdsnid;
EXIT WHEN nature%NOTFOUND;
dbms_output.put_line('BDSN ID modifie : '||bdsnid);
Budget_Moteur.set_bdsn_vote(bdsnid);
END LOOP;
CLOSE nature;

END;




/*********************************************
SET_BDSN_VOTE
Mise a jour des champs bdsn_vote dans la table budget_saisie_nature a partir de la table budget_vote_nature.
***********************************************/
PROCEDURE set_bdsn_vote(bdsnid INTEGER) IS
cpt INTEGER;
budgetnature BUDGET_SAISIE_NATURE%ROWTYPE;
bdvnvotes BUDGET_VOTE_NATURE.bdvn_votes%TYPE;
BEGIN
SELECT *
INTO budgetnature
FROM BUDGET_SAISIE_NATURE
WHERE bdsn_id = bdsnid;

SELECT COUNT(*) INTO cpt FROM BUDGET_VOTE_NATURE
WHERE exe_ordre = budgetnature.exe_ordre
AND pco_num = budgetnature.pco_num
AND org_id = budgetnature.org_id
AND tcd_ordre = budgetnature.tcd_ordre;

IF cpt = 1 THEN

SELECT bdvn_votes INTO bdvnvotes FROM BUDGET_VOTE_NATURE
WHERE exe_ordre = budgetnature.exe_ordre
AND pco_num = budgetnature.pco_num
AND org_id = budgetnature.org_id
AND tcd_ordre = budgetnature.tcd_ordre;

UPDATE BUDGET_SAISIE_NATURE
SET bdsn_vote = bdvnvotes, bdsn_montant = bdvnvotes+bdsn_saisi
WHERE bdsn_id = bdsnid;

ELSE
UPDATE BUDGET_SAISIE_NATURE
SET bdsn_vote = 0
WHERE bdsn_id = bdsnid;
END IF;

END;





/*********************************************
SET_BDSG_VOTE
Mise a jour des champs bdsg_vote dans la table budget_saisie_gestion a partir de la table budget_vote_gestion
***********************************************/
PROCEDURE set_bdsg_vote(bdsgid INTEGER) IS
cpt INTEGER;
budgetgestion BUDGET_SAISIE_GESTION%ROWTYPE;
bdvgvotes BUDGET_VOTE_GESTION.bdvg_votes%TYPE;
BEGIN
SELECT *
INTO budgetgestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsg_id = bdsgid;

SELECT COUNT(*) INTO cpt FROM BUDGET_VOTE_GESTION
WHERE exe_ordre = budgetgestion.exe_ordre
AND tyac_id = budgetgestion.tyac_id
AND org_id = budgetgestion.org_id
AND tcd_ordre = budgetgestion.tcd_ordre;

IF cpt = 1 THEN

SELECT bdvg_votes INTO bdvgvotes FROM BUDGET_VOTE_GESTION
WHERE exe_ordre = budgetgestion.exe_ordre
AND tyac_id = budgetgestion.tyac_id
AND org_id = budgetgestion.org_id
AND tcd_ordre = budgetgestion.tcd_ordre;

UPDATE BUDGET_SAISIE_GESTION
SET bdsg_vote = bdvgvotes,  bdsg_montant = bdvgvotes+bdsg_saisi
WHERE bdsg_id = bdsgid;
ELSE
UPDATE BUDGET_SAISIE_GESTION
SET bdsg_vote = 0
WHERE bdsg_id = bdsgid;
END IF;

END;




/*********************************************
SET_BDS_VOTE_ORGAN
Mise a jour des champs bdsn_vote dans la table budget_saisie_nature a partir de la table budget_vote_nature pour un ORG_ID et un budget
***********************************************/
PROCEDURE set_bds_vote_organ(bdsaid INTEGER, orgid INTEGER)
 IS

bdsgid INTEGER;
bdsnid INTEGER;

CURSOR gestion IS SELECT bdsg_id FROM BUDGET_SAISIE_GESTION
WHERE bdsa_id = bdsaid AND org_id = orgid;

CURSOR nature IS SELECT bdsn_id FROM BUDGET_SAISIE_NATURE
WHERE bdsa_id = bdsaid  AND org_id = orgid;

BEGIN

OPEN gestion;
LOOP
FETCH gestion INTO bdsgid;
EXIT WHEN gestion%NOTFOUND;
Budget_Moteur.set_bdsg_vote(bdsgid);
END LOOP;
CLOSE gestion;

OPEN nature;
LOOP
FETCH nature INTO bdsnid;
EXIT WHEN nature%NOTFOUND;
Budget_Moteur.set_bdsn_vote(bdsnid);
END LOOP;
CLOSE nature;

END;



/***********************************************
VIDER_CONSOLIDATION
Suppression de la consolidation budgetaire. On vide les tables budget_calcul Gestion et Nature
************************************************/
PROCEDURE vider_consolidation(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;
--TODO controler l etat de la saisie

DELETE FROM BUDGET_CALCUL_NATURE
WHERE bdsa_id = bdsaid
 AND org_id IN
(SELECT org_id
 FROM v_organ
 WHERE org_niv IN(0,    1,    2))
AND tyet_id = 103;

DELETE FROM BUDGET_CALCUL_GESTION
WHERE bdsa_id = bdsaid
 AND org_id IN
(SELECT org_id
 FROM v_organ
 WHERE org_niv IN(0,    1,    2))
AND tyet_id = 103;

END;


/***********************************************
VIDER_BUDGET_ORGAN
Suppression de la consolidation budgetaire. On vide les tables budget_calcul Gestion et Nature
************************************************/
PROCEDURE vider_budget_organ(bdsaid INTEGER, orgid INTEGER) IS
cpt INTEGER;
BEGIN
  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;
--TODO controler l etat de la saisie

DELETE FROM BUDGET_SAISIE_NATURE
WHERE bdsa_id = bdsaid
 AND org_id = orgid;

DELETE FROM BUDGET_SAISIE_GESTION
WHERE bdsa_id = bdsaid
 AND org_id = orgid;

END;



/***********************************************
CREER_CONSOLIDATION
Initialisation de la consolidation budgetaire avec tous les bdcg_calcul et bdcn_calcul = 0
La consolidation du budget par nature se fait au niveau des chapitres votes
*********************************************/
PROCEDURE creer_consolidation(bdsaid INTEGER) IS
exeordre INTEGER;

cpt INTEGER;
currentBudgetSaisie BUDGET_SAISIE%ROWTYPE;
BEGIN
  -- lock des tables
  Budget_Moteur.lock_table_budget_saisie;

--TODO controler l etat de la saisie
SELECT exe_ordre INTO exeordre FROM BUDGET_SAISIE
WHERE bdsa_id = bdsaid;

INSERT INTO BUDGET_CALCUL_NATURE
SELECT
budget_saisie_nature_seq.NEXTVAL,--BDCN_ID
bdsaid,--BDSA_ID
o.org_id,--ORG_ID
tcd_ordre,--TCD_ORDRE
tt.PCO_num_VOTE,--pco_num
0,--BDCN_CALCUL
exeordre,--EXE_ORDRE
103,--TYET_ID
0,
0
FROM V_BUDGET_MASQUE_SAISIE_NATURE tt , v_organ o
WHERE o.org_niv IN (0,1,2)
AND tt.exe_ordre = exeordre;

/*
INSERT INTO BUDGET_CALCUL_NATURE
SELECT
budget_saisie_nature_seq.NEXTVAL,--BDCN_ID
bdsaid,--BDSA_ID
o.org_id,--,--ORG_ID
tc.tcd_ordre,--TCD_ORDRE
tt.PCO_num_vote,--pco_num
0,--BDCN_CALCUL
exeordre,--EXE_ORDRE
103--TYET_ID
FROM V_BUDGET_MASQUE_SAISIE_NATURE tt, v_organ o,jefy_admin.type_credit tc
WHERE o.org_niv IN (0,1,2)
AND tc.exe_ordre = exeordre
AND tc.tcd_code = '00'
AND tt.exe_ordre = exeordre;*/

INSERT INTO BUDGET_CALCUL_GESTION
SELECT
budget_saisie_gestion_seq.NEXTVAL,--BDCG_ID
bdsaid,--BDSA_ID
o.org_id,--ORG_ID
TCD_ORDRE,--tcd_ordre
TYAC_ID ,--tyac_id
0,--BDCG_CALCUL
exeordre,--EXE_ORDRE
103,--TYET_ID
0,
0
FROM v_budget_masque_SAISIE_GESTION tt,v_organ o
WHERE o.org_niv IN (0,1,2)
AND tt.exe_ordre = exeordre;

END;



/**********************************************
CONTROLE_DISPONIBLE
**********************************************/
PROCEDURE controle_disponible(bdsaid INTEGER) IS
cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;

-- VERIFIER LE DISPONIBLE BUDGETAIRE budget_exec
-- LORS D UNE REDUCTION DES CREDITS
-- ATTENTION AU PARAMETRAGE
-- CHAPITRE BUDGETAIRE
-- MASSE DE CREDIT
-- NIVEAU DU CONTROLE

END;

FUNCTION get_parametre(KEY VARCHAR,   execice INTEGER) RETURN VARCHAR IS cpt INTEGER;
BEGIN
SELECT 1
INTO cpt
FROM dual;
RETURN 'rien';

END;

FUNCTION get_chapitre_vote(pconum VARCHAR,exeordre INTEGER, tcdordre integer) RETURN VARCHAR IS
chapvote BUDGET_MASQUE_NATURE.pco_num%TYPE;
cpt INTEGER;
BEGIN

-- si compte 0xxx on revoie le compte
IF pconum LIKE '0%' THEN
   RETURN pconum;
END IF;

SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_MASQUE_NATURE
WHERE pco_num_vote = pconum AND exe_ordre = exeordre and tcd_ordre=tcdordre;

-- si compte = chapitre de vote on renvoie ce compte
IF (cpt != 0) THEN
   RETURN pconum;
END IF;

SELECT COUNT(*) INTO cpt FROM jefy_budget.BUDGET_MASQUE_NATURE
WHERE pco_num = pconum AND exe_ordre = exeordre and tcd_ordre=tcdordre;

-- on recherche le compte de vote de ce compte d imputation
IF cpt > 0 THEN
  SELECT DISTINCT pco_num_vote INTO chapvote
  FROM jefy_budget.BUDGET_MASQUE_NATURE
  WHERE pco_num = pconum AND exe_ordre = exeordre and tcd_ordre=tcdordre;
ELSE
  RAISE_APPLICATION_ERROR(-20001,   'PROBLEME DE CHAPITRE BUDGETAIRE VOTE : '||pconum);
END IF;

RETURN chapvote;

END;

FUNCTION get_orgid_niv_orgid(orgid INTEGER,   niveau INTEGER) RETURN INTEGER
IS
org INTEGER;
niv INTEGER;
orgidtemp INTEGER;
BEGIN

dbms_output.put_line('******* GET ORG ID NIV ORDRE ID ************');
dbms_output.put_line('Org ID : '||orgid||' , Niveau  : '||niveau);

SELECT org_niv
INTO niv
FROM v_organ
WHERE org_id = orgid;

if niveau=niv then
  return orgid;
end if;

IF niveau > niv THEN
RAISE_APPLICATION_ERROR(-20001,   'PROBLEME NIVEAU BUDGETAIRE');
END IF;


orgidtemp := orgid;
LOOP
SELECT org_pere
INTO org
FROM v_organ
WHERE org_id = orgidtemp;

SELECT org_niv
INTO niv
FROM v_organ
WHERE org_id = org;

IF niv = niveau THEN
EXIT;
ELSE
orgidtemp := org;
END IF;

END LOOP;

dbms_output.put_line('RETOUR : '||org);
dbms_output.put_line('*******************');

RETURN org;
END;



/************************************************************
SET_LIGNE_GESTION_CALCUL
************************************************************/
PROCEDURE set_ligne_gestion_calcul(bdsgid INTEGER,orgid INTEGER) IS
cpt INTEGER;
  lignegestion BUDGET_SAISIE_GESTION % ROWTYPE;
BEGIN

SELECT *
INTO lignegestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsg_id =bdsgid;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_CALCUL_GESTION
WHERE org_id = orgid
 AND tcd_ordre = lignegestion.tcd_ordre
 AND tyac_id = lignegestion.tyac_id
 AND exe_ordre = lignegestion.exe_ordre
 AND bdsa_id = lignegestion.bdsa_id;

IF cpt = 0 THEN

INSERT
INTO BUDGET_CALCUL_GESTION(bdcg_id,   bdcg_calcul,   bdsa_id,   org_id,   tcd_ordre,   exe_ordre,   tyac_id,   tyet_id,bdcg_vote,bdcg_saisi)
VALUES(budget_saisie_gestion_seq.NEXTVAL,   --BDcG_ID,
0,   --BDCG_XXX,
lignegestion.bdsa_id,   --BDSA_ID,
orgid,   --ORG_ID,
lignegestion.tcd_ordre,   --TCD_ORDRE,
lignegestion.exe_ordre,   --EXE_ORDRE,
lignegestion.tyac_id,   --TYAC_ID,
103,
0,
0
);

END IF;

UPDATE BUDGET_CALCUL_GESTION
SET bdcg_calcul = bdcg_calcul +  lignegestion.bdsg_vote + lignegestion.bdsg_saisi,
--ajout du 09/01/2007
bdcg_saisi =bdcg_saisi + lignegestion.bdsg_saisi ,
bdcg_vote = bdcg_vote + lignegestion.bdsg_vote
WHERE org_id = orgid
 AND tcd_ordre = lignegestion.tcd_ordre
 AND tyac_id = lignegestion.tyac_id
 AND exe_ordre = lignegestion.exe_ordre
 AND bdsa_id = lignegestion.bdsa_id;

END;


/************************************************************
SET_LIGNE_NATURE_CALCUL
************************************************************/
PROCEDURE set_ligne_nature_calcul(bdsnid INTEGER,orgid INTEGER) IS
cpt INTEGER;

  lignenature BUDGET_SAISIE_NATURE % ROWTYPE;
chapitrevote BUDGET_MASQUE_NATURE.pco_num%TYPE;

BEGIN
SELECT *
INTO lignenature
FROM BUDGET_SAISIE_NATURE
WHERE bdsn_id =bdsnid;

chapitrevote:=Budget_Moteur.get_chapitre_vote( lignenature.pco_num,lignenature.exe_ordre, lignenature.tcd_ordre);

SELECT COUNT(*)
INTO cpt
FROM BUDGET_CALCUL_NATURE
WHERE org_id = orgid
 AND tcd_ordre = lignenature.tcd_ordre
 AND pco_num = chapitrevote
 AND exe_ordre = lignenature.exe_ordre
 AND bdsa_id = lignenature.bdsa_id;

IF cpt = 0 THEN

INSERT
INTO BUDGET_CALCUL_NATURE(bdcn_id,   bdcn_calcul,  bdsa_id,   org_id,   tcd_ordre,   exe_ordre,   pco_num,   tyet_id,bdcn_vote,bdcn_saisi)
VALUES(budget_saisie_nature_seq.NEXTVAL,   --BDCN_ID,
0,   --BDCN_XXX,
lignenature.bdsa_id,   --BDSA_ID,
orgid,   --ORG_ID,
lignenature.tcd_ordre,   --TCD_ORDRE,
lignenature.exe_ordre,   --EXE_ORDRE,
chapitrevote,   --PCO_NUM,
103,
0,
0
);

END IF;

UPDATE BUDGET_CALCUL_NATURE
SET bdcn_calcul = bdcn_calcul + lignenature.bdsn_vote + lignenature.bdsn_saisi,
--ajout du 09/01/2007
bdcn_saisi =bdcn_saisi + lignenature.bdsn_saisi ,
bdcn_vote = bdcn_vote + lignenature.bdsn_vote
WHERE org_id = orgid
 AND tcd_ordre = lignenature.tcd_ordre
 AND pco_num = chapitrevote
 AND exe_ordre = lignenature.exe_ordre
 AND bdsa_id = lignenature.bdsa_id;


END;



/****************************************************
SET_LIGNE_NATURE_VOTE
**************************************************/
PROCEDURE set_ligne_nature_vote(bdsnid INTEGER) IS
cpt INTEGER;
nature BUDGET_SAISIE_NATURE % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO nature
FROM BUDGET_SAISIE_NATURE
WHERE bdsn_id = bdsnid;

SELECT tysa_id
INTO saisie
FROM BUDGET_SAISIE
WHERE bdsa_id = nature.bdsa_id;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_VOTE_NATURE
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;

-- ATTENTION PAS DE VALEUR NEGATIVE

IF cpt = 0 THEN
INSERT
INTO BUDGET_VOTE_NATURE(bdvn_conventions,   bdvn_credits,   bdvn_dbms,   bdvn_debits,   bdvn_id,   bdvn_ouverts,   bdvn_primitifs,   bdvn_provisoires,   bdvn_reliquats,   bdvn_votes,   exe_ordre,   org_id,   pco_num,   tcd_ordre)
VALUES(0,   0,   0,   0,   budget_vote_nature_seq.NEXTVAL,   0,   0,   0,   0,   0,   nature.exe_ordre,   nature.org_id,   nature.pco_num,   nature.tcd_ordre);
END IF;

-- suivant la saisie on impacte la bonne colonne.

IF (saisie = 1) THEN
-- provisoire

UPDATE BUDGET_VOTE_NATURE
SET bdvn_primitifs = bdvn_primitifs + nature.bdsn_saisi,
  bdvn_provisoires = bdvn_provisoires + nature.bdsn_saisi,
  bdvn_votes = bdvn_votes + nature.bdsn_saisi,
  bdvn_ouverts = bdvn_ouverts + nature.bdsn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 2) THEN
-- INITIAL

UPDATE BUDGET_VOTE_NATURE
SET bdvn_primitifs = bdvn_provisoires + nature.bdsn_saisi,
  bdvn_votes = bdvn_votes + nature.bdsn_saisi,
  bdvn_ouverts = bdvn_ouverts + nature.bdsn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 3) THEN
-- RELIQUAT

UPDATE BUDGET_VOTE_NATURE
SET bdvn_reliquats = bdvn_reliquats + nature.bdsn_saisi,
  bdvn_votes = bdvn_votes + nature.bdsn_saisi,
  bdvn_ouverts = bdvn_ouverts + nature.bdsn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 4) THEN
-- DBM

UPDATE BUDGET_VOTE_NATURE
SET bdvn_dbms = bdvn_dbms + nature.bdsn_saisi,
  bdvn_votes = bdvn_votes + nature.bdsn_saisi,
  bdvn_ouverts = bdvn_ouverts + nature.bdsn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 5) THEN
-- CONVENTIONS

UPDATE BUDGET_VOTE_NATURE
SET bdvn_conventions = bdvn_conventions + nature.bdsn_saisi,
  bdvn_votes = bdvn_votes + nature.bdsn_saisi,
  bdvn_ouverts = bdvn_ouverts + nature.bdsn_saisi + bdvn_debits + bdvn_credits
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;


END;

/****************************************************
SET_LIGNE_NATURE_LOLF_VOTE
**************************************************/
PROCEDURE set_ligne_nature_lolf_vote(bdslid INTEGER) IS
   cpt INTEGER;
   nature BUDGET_SAISIE_NATURE_LOLF%ROWTYPE;
   saisie INTEGER;
BEGIN

   SELECT * INTO nature FROM BUDGET_SAISIE_NATURE_LOLF WHERE bdsl_id = bdslid;
   SELECT tysa_id INTO saisie FROM BUDGET_SAISIE WHERE bdsa_id = nature.bdsa_id;

   SELECT COUNT(*) INTO cpt FROM BUDGET_VOTE_NATURE_LOLF WHERE exe_ordre=nature.exe_ordre AND org_id=nature.org_id
      AND pco_num=nature.pco_num AND tcd_ordre=nature.tcd_ordre AND tyac_id=nature.tyac_id;

   -- ATTENTION PAS DE VALEUR NEGATIVE

   IF cpt = 0 THEN
      INSERT INTO BUDGET_VOTE_NATURE_LOLF VALUES(budget_vote_nature_lolf_seq.NEXTVAL, 0,0,0,0,0,0,0,0,0, nature.org_id, nature.exe_ordre,
         nature.pco_num, nature.tcd_ordre, nature.tyac_id);
   END IF;

   -- suivant la saisie on impacte la bonne colonne.
   IF (saisie = 1) THEN -- provisoire
      UPDATE BUDGET_VOTE_NATURE_LOLF
      SET bdvl_primitifs = bdvl_primitifs + nature.bdsl_saisi,
          bdvl_provisoires = bdvl_provisoires + nature.bdsl_saisi,
          bdvl_votes = bdvl_votes + nature.bdsl_saisi,
          bdvl_ouverts = bdvl_ouverts + nature.bdsl_saisi
      WHERE exe_ordre=nature.exe_ordre AND org_id=nature.org_id AND pco_num=nature.pco_num AND tcd_ordre=nature.tcd_ordre and tyac_id=nature.tyac_id;
   END IF;

   IF (saisie = 2) THEN -- INITIAL
      UPDATE BUDGET_VOTE_NATURE_LOLF
      SET bdvl_primitifs = bdvl_provisoires + nature.bdsl_saisi,
          bdvl_votes = bdvl_votes + nature.bdsl_saisi,
          bdvl_ouverts = bdvl_ouverts + nature.bdsl_saisi
      WHERE exe_ordre=nature.exe_ordre AND org_id=nature.org_id AND pco_num=nature.pco_num AND tcd_ordre=nature.tcd_ordre and tyac_id=nature.tyac_id;
   END IF;

   IF (saisie = 3) THEN -- RELIQUAT
      UPDATE BUDGET_VOTE_NATURE_LOLF
      SET bdvl_reliquats = bdvl_reliquats + nature.bdsl_saisi,
          bdvl_votes = bdvl_votes + nature.bdsl_saisi,
          bdvl_ouverts = bdvl_ouverts + nature.bdsl_saisi
      WHERE exe_ordre=nature.exe_ordre AND org_id=nature.org_id AND pco_num=nature.pco_num AND tcd_ordre=nature.tcd_ordre and tyac_id=nature.tyac_id;
   END IF;

   IF (saisie = 4) THEN -- DBM
      UPDATE BUDGET_VOTE_NATURE_LOLF
      SET bdvl_dbms = bdvl_dbms + nature.bdsl_saisi,
          bdvl_votes = bdvl_votes + nature.bdsl_saisi,
          bdvl_ouverts = bdvl_ouverts + nature.bdsl_saisi
      WHERE exe_ordre=nature.exe_ordre AND org_id=nature.org_id AND pco_num=nature.pco_num AND tcd_ordre=nature.tcd_ordre and tyac_id=nature.tyac_id;
   END IF;

   IF (saisie = 5) THEN -- CONVENTIONS
      UPDATE BUDGET_VOTE_NATURE_LOLF
      SET bdvl_conventions = bdvl_conventions + nature.bdsl_saisi,
          bdvl_votes = bdvl_votes + nature.bdsl_saisi,
          bdvl_ouverts = bdvl_ouverts + nature.bdsl_saisi
      WHERE exe_ordre=nature.exe_ordre AND org_id=nature.org_id AND pco_num=nature.pco_num AND tcd_ordre=nature.tcd_ordre and tyac_id=nature.tyac_id;
   END IF;
END;

PROCEDURE set_ligne_gestion_vote(bdsgid INTEGER) IS
cpt INTEGER;
gestion BUDGET_SAISIE_GESTION % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO gestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsg_id = bdsgid;

SELECT tysa_id
INTO saisie
FROM BUDGET_SAISIE
WHERE bdsa_id = gestion.bdsa_id;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_VOTE_GESTION
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;

-- ATTENTION PAS DE VALEUR NEGATIVE

IF cpt = 0 THEN
INSERT
INTO BUDGET_VOTE_GESTION(bdvg_conventions,   bdvg_credits,   bdvg_dbms,   bdvg_debits,   bdvg_id,   bdvg_ouverts,   bdvg_primitifs,   bdvg_provisoires,   bdvg_reliquats,   bdvg_votes,   exe_ordre,   org_id,   tcd_ordre,   tyac_id)
VALUES(0,   0,   0,   0,   budget_vote_gestion_seq.NEXTVAL,   0,   0,   0,   0,   0,   gestion.exe_ordre,   gestion.org_id,   gestion.tcd_ordre,   gestion.tyac_id);
END IF;

-- suivant la saisie on impacte la bonne colonne.
IF (saisie = 1) THEN
-- provisoire

UPDATE BUDGET_VOTE_GESTION
SET bdvg_primitifs = bdvg_primitifs + gestion.bdsg_saisi,
  bdvg_provisoires = bdvg_provisoires + gestion.bdsg_saisi,
  bdvg_votes = bdvg_votes + gestion.bdsg_saisi,
  bdvg_ouverts = bdvg_ouverts + gestion.bdsg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 2) THEN
--primitif
UPDATE BUDGET_VOTE_GESTION
SET bdvg_primitifs = bdvg_primitifs + gestion.bdsg_saisi,
  bdvg_votes = bdvg_votes + gestion.bdsg_saisi,
  bdvg_ouverts = bdvg_ouverts + gestion.bdsg_saisi -- -bdvg_debits + bdvg_credits
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 3) THEN
-- reliquats

UPDATE BUDGET_VOTE_GESTION
SET bdvg_reliquats = bdvg_reliquats + gestion.bdsg_saisi,
  bdvg_votes = bdvg_votes + gestion.bdsg_saisi,
  bdvg_ouverts = bdvg_ouverts + gestion.bdsg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 4) THEN
-- dbms

UPDATE BUDGET_VOTE_GESTION
SET bdvg_dbms = bdvg_dbms + gestion.bdsg_saisi,
  bdvg_votes = bdvg_votes + gestion.bdsg_saisi,
  bdvg_ouverts = bdvg_ouverts + gestion.bdsg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 5) THEN
-- conventions

UPDATE BUDGET_VOTE_GESTION
SET bdvg_conventions = bdvg_conventions + gestion.bdsg_saisi,
  bdvg_votes = bdvg_votes + gestion.bdsg_saisi,
  bdvg_ouverts = bdvg_ouverts + gestion.bdsg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;


END;
PROCEDURE set_ligne_nature_vote_calcul(bdcnid INTEGER) IS
cpt INTEGER;
nature BUDGET_CALCUL_NATURE % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO nature
FROM BUDGET_CALCUL_NATURE
WHERE bdcn_id = bdcnid;

SELECT tysa_id
INTO saisie
FROM BUDGET_SAISIE
WHERE bdsa_id = nature.bdsa_id;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_VOTE_NATURE_CALCUL
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;

-- ATTENTION PAS DE VALEUR NEGATIVE

IF cpt = 0 THEN
INSERT
INTO BUDGET_VOTE_NATURE_CALCUL(bvnc_conventions,   bvnc_credits,   bvnc_dbms,   bvnc_debits,   bvnc_id,   bvnc_ouverts,   bvnc_primitifs,   bvnc_provisoires,   bvnc_reliquats,   bvnc_votes,   exe_ordre,   org_id,   pco_num,   tcd_ordre)
VALUES(0,   0,   0,   0,   budget_vote_nature_seq.NEXTVAL,   0,   0,   0,   0,   0,   nature.exe_ordre,   nature.org_id,   nature.pco_num,   nature.tcd_ordre);
END IF;

-- suivant la saisie on impacte la bonne colonne.

IF (saisie = 1) THEN
-- provisoire

UPDATE BUDGET_VOTE_NATURE_CALCUL
SET bvnc_primitifs = bvnc_primitifs + nature.bdcn_calcul,
  bvnc_provisoires = bvnc_provisoires + nature.bdcn_calcul,
  bvnc_votes = bvnc_votes + nature.bdcn_calcul,
  bvnc_ouverts = bvnc_ouverts + nature.bdcn_calcul
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 2) THEN
-- INITIAL

UPDATE BUDGET_VOTE_NATURE_CALCUL
SET bvnc_primitifs = bvnc_provisoires + nature.bdcn_saisi,
  bvnc_votes = bvnc_votes + nature.bdcn_saisi,
  bvnc_ouverts = bvnc_ouverts + nature.bdcn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 3) THEN
-- RELIQUAT

UPDATE BUDGET_VOTE_NATURE_CALCUL
SET bvnc_reliquats = bvnc_reliquats + nature.bdcn_saisi,
  bvnc_votes = bvnc_votes + nature.bdcn_saisi,
  bvnc_ouverts = bvnc_ouverts + nature.bdcn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 4) THEN
-- DBM

UPDATE BUDGET_VOTE_NATURE_CALCUL
SET bvnc_dbms = bvnc_dbms + nature.bdcn_saisi,
  bvnc_votes = bvnc_votes + nature.bdcn_saisi,
  bvnc_ouverts = bvnc_ouverts + nature.bdcn_saisi
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;

IF (saisie = 5) THEN
-- CONVENTIONS

UPDATE BUDGET_VOTE_NATURE_CALCUL
SET bvnc_conventions = bvnc_conventions + nature.bdcn_calcul,
  bvnc_votes = bvnc_votes + nature.bdcn_calcul,
  bvnc_ouverts = bvnc_ouverts + nature.bdcn_calcul
WHERE exe_ordre = nature.exe_ordre
 AND org_id = nature.org_id
 AND pco_num = nature.pco_num
 AND tcd_ordre = nature.tcd_ordre;
END IF;


END;

PROCEDURE set_ligne_gestion_vote_calcul(bdcgid INTEGER) IS
cpt INTEGER;
gestion BUDGET_CALCUL_GESTION % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO gestion
FROM BUDGET_CALCUL_GESTION
WHERE bdcg_id = bdcgid;

SELECT tysa_id
INTO saisie
FROM BUDGET_SAISIE
WHERE bdsa_id = gestion.bdsa_id;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_VOTE_GESTION_CALCUL
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;

-- ATTENTION PAS DE VALEUR NEGATIVE

IF cpt = 0 THEN
INSERT
INTO BUDGET_VOTE_GESTION_CALCUL(bvgc_conventions,   bvgc_credits,   bvgc_dbms,   bvgc_debits,   bvgc_id,   bvgc_ouverts,   bvgc_primitifs,   bvgc_provisoires,   bvgc_reliquats,   bvgc_votes,   exe_ordre,   org_id,   tcd_ordre,   tyac_id)
VALUES(0,   0,   0,   0,   budget_vote_gestion_seq.NEXTVAL,   0,   0,   0,   0,   0,   gestion.exe_ordre,   gestion.org_id,   gestion.tcd_ordre,   gestion.tyac_id);
END IF;

-- suivant la saisie on impacte la bonne colonne.
IF (saisie = 1) THEN
-- provisoire

UPDATE BUDGET_VOTE_GESTION_CALCUL
SET bvgc_primitifs = bvgc_primitifs + gestion.bdcg_calcul,
  bvgc_provisoires = bvgc_provisoires + gestion.bdcg_calcul,
  bvgc_votes = bvgc_votes + gestion.bdcg_calcul,
  bvgc_ouverts = bvgc_ouverts + gestion.bdcg_calcul
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 2) THEN
--primitif
UPDATE BUDGET_VOTE_GESTION_CALCUL
SET bvgc_primitifs = bvgc_primitifs + gestion.bdcg_saisi,
  bvgc_votes = bvgc_votes + gestion.bdcg_saisi,
  bvgc_ouverts = bvgc_ouverts + gestion.bdcg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 3) THEN
-- reliquats

UPDATE BUDGET_VOTE_GESTION_CALCUL
SET bvgc_reliquats = bvgc_reliquats + gestion.bdcg_saisi,
  bvgc_votes = bvgc_votes + gestion.bdcg_saisi,
  bvgc_ouverts = bvgc_ouverts + gestion.bdcg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 4) THEN
-- dbms

UPDATE BUDGET_VOTE_GESTION_CALCUL
SET bvgc_dbms = bvgc_dbms + gestion.bdcg_saisi,
  bvgc_votes = bvgc_votes + gestion.bdcg_saisi,
  bvgc_ouverts = bvgc_ouverts + gestion.bdcg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 5) THEN
-- conventions

UPDATE BUDGET_VOTE_GESTION_CALCUL
SET bvgc_conventions = bvgc_conventions + gestion.bdcg_calcul,
  bvgc_votes = bvgc_votes + gestion.bdcg_calcul,
  bvgc_ouverts = bvgc_ouverts + gestion.bdcg_calcul
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tyac_id = gestion.tyac_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;


END;

PROCEDURE set_budfonc(bdsgid INTEGER) IS
cpt INTEGER;
gestion BUDGET_SAISIE_GESTION % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO gestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsg_id = bdsgid;
-- si convention RA : CONTROLE POCHE BUDGETAIRE
-- alors set_ligne_budfonc_conv
-- sinon set_ligne_budfonc
saisie := Budget_Moteur.conv_ra(gestion.org_id,gestion.exe_ordre);

IF(saisie = 0) THEN
Budget_Moteur.set_ligne_budfonc(bdsgid);
ELSE
Budget_Moteur.set_ligne_budfonc_conv(bdsgid);
END IF;

END;

PROCEDURE set_ligne_budfonc(bdsgid INTEGER) IS
cpt INTEGER;
gestion BUDGET_SAISIE_GESTION % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO gestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsg_id = bdsgid;

SELECT tysa_id
INTO saisie
FROM BUDGET_SAISIE
WHERE bdsa_id = gestion.bdsa_id;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_EXEC_CREDIT
WHERE tcd_ordre = gestion.tcd_ordre
 AND org_id = gestion.org_id
 AND exe_ordre = gestion.exe_ordre;

IF cpt = 0 THEN

INSERT
INTO BUDGET_EXEC_CREDIT(bdxc_id,   bdxc_provisoires,   bdxc_primitifs,   bdxc_reliquats,   bdxc_dbms,   bdxc_ouverts,   bdxc_votes,   bdxc_debits,   bdxc_credits,   bdxc_engagements,   bdxc_mandats,   bdxc_disponible,   bdxc_reserve,   bdxc_dispo_reserve,   bdxc_conventions,   org_id,   tcd_ordre,   exe_ordre)
VALUES(budget_exec_credit_seq.NEXTVAL,   --BDXC_ID,
0,   --BDXC_PROVISOIRES,
0,   --BDXC_PRIMITIFS,
0,   --BDXC_RELIQUATS,
0,   --BDXC_DBMS ,
0,   --BDXC_OUVERTS ,
0,   --BDXC_VOTES ,
0,   --BDXC_DEBITS ,
0,   --BDXC_CREDITS,
0,   --BDXC_ENGAGEMENTS ,
0,   --BDXC_MANDATS ,
0,   --BDXC_DISPONIBLE ,
0,   --BDXC_RESERVE
0,   --BDXC_DISPO_RESERVE,
0,   --BDXC_CONVENTIONS ,
gestion.org_id,   gestion.tcd_ordre,   gestion.exe_ordre);
END IF;

--raise_application_error (-20001,'???-'||gestion.bdsa_id||'-??');

-- suivant la saisie on impacte la bonne colonne.
IF (saisie = 1) THEN
-- provisoire

UPDATE BUDGET_EXEC_CREDIT
SET bdxc_primitifs = bdxc_primitifs+gestion.bdsg_saisi,
  bdxc_provisoires = bdxc_provisoires+gestion.bdsg_saisi,
  bdxc_votes = bdxc_votes+gestion.bdsg_saisi,
  bdxc_ouverts = bdxc_ouverts+gestion.bdsg_saisi,
  bdxc_disponible = bdxc_ouverts
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie = 2) THEN
--primitif

UPDATE BUDGET_EXEC_CREDIT
SET bdxc_primitifs = bdxc_primitifs+gestion.bdsg_saisi,
  bdxc_votes = bdxc_votes+gestion.bdsg_saisi,
  bdxc_ouverts = bdxc_ouverts+gestion.bdsg_saisi,
  bdxc_disponible =    bdxc_ouverts -bdxc_engagements -bdxc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;

END IF;

IF (saisie = 3) THEN
--reliquats

UPDATE BUDGET_EXEC_CREDIT
SET bdxc_reliquats = bdxc_reliquats+gestion.bdsg_saisi,
  bdxc_votes = bdxc_votes + gestion.bdsg_saisi,
  bdxc_ouverts = bdxc_ouverts + gestion.bdsg_saisi,
  bdxc_disponible =    bdxc_ouverts -bdxc_engagements -bdxc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;

END IF;

IF (saisie = 4) THEN
--dbms

UPDATE BUDGET_EXEC_CREDIT
SET bdxc_dbms = bdxc_dbms + gestion.bdsg_saisi,
  bdxc_votes = bdxc_votes + gestion.bdsg_saisi,
  bdxc_ouverts = bdxc_ouverts + gestion.bdsg_saisi,
  bdxc_disponible =    bdxc_ouverts -bdxc_engagements -bdxc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;

END IF;

IF (saisie = 5) THEN
--dbms

UPDATE BUDGET_EXEC_CREDIT
SET bdxc_conventions = bdxc_conventions + gestion.bdsg_saisi,
  bdxc_votes = bdxc_votes + gestion.bdsg_saisi,
  bdxc_ouverts = bdxc_ouverts + gestion.bdsg_saisi,
  bdxc_disponible =    bdxc_ouverts -bdxc_engagements -bdxc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;

END IF;

DELETE FROM jefy_budget.BUDGET_EXEC_CREDIT WHERE TCD_ORDRE IN
(SELECT tcd_ordre FROM jefy_admin.TYPE_CREDIT WHERE TCD_TYPE = 'RECETTE');

END;

PROCEDURE set_ligne_budfonc_conv(bdsgid INTEGER) IS
cpt INTEGER;
gestion BUDGET_SAISIE_GESTION % ROWTYPE;
saisie INTEGER;
BEGIN

SELECT *
INTO gestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsg_id = bdsgid;

SELECT tysa_id
INTO saisie
FROM BUDGET_SAISIE
WHERE bdsa_id = gestion.bdsa_id;

SELECT COUNT(*)
INTO cpt
FROM BUDGET_EXEC_CREDIT_CONV
WHERE tcd_ordre = gestion.tcd_ordre
 AND org_id = gestion.org_id
 AND exe_ordre = gestion.exe_ordre;

IF cpt = 0 THEN

INSERT
INTO BUDGET_EXEC_CREDIT_CONV(becc_id,   becc_provisoires,   becc_primitifs,   becc_reliquats,   becc_dbms,   becc_ouverts,   becc_votes,   becc_debits,   becc_credits,   becc_engagements,   becc_mandats,   becc_disponible,   becc_conventions,   org_id,   tcd_ordre,   exe_ordre, becc_reserve)
VALUES(budget_exec_credit_seq.NEXTVAL,   --BecC_ID,
0,   --BecC_PROVISOIRES,
0,   --BecC_PRIMITIFS,
0,   --BecC_RELIQUATS,
0,   --BecC_DBMS ,
0,   --BecC_OUVERTS ,
0,   --BecC_VOTES ,
0,   --BecC_DEBITS ,
0,   --BecC_CREDITS,
0,   --BecC_ENGAGEMENTS ,
0,   --BecC_MANDATS ,
0,   --BecC_DISPONIBLE ,
0,   --BecC_CONVENTIONS ,
gestion.org_id,   gestion.tcd_ordre,   gestion.exe_ordre,
0
);
END IF;

-- suivant la saisie on impacte la bonne colonne.
IF (saisie= 1) THEN
-- provisoire

UPDATE BUDGET_EXEC_CREDIT_CONV
SET becc_primitifs = becc_primitifs+gestion.bdsg_saisi,
  becc_provisoires = becc_provisoires+gestion.bdsg_saisi,
  becc_votes = becc_votes+gestion.bdsg_saisi,
  becc_ouverts = becc_ouverts+gestion.bdsg_saisi,
  becc_disponible = becc_disponible+gestion.bdsg_saisi
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie= 2) THEN
--primitif

UPDATE BUDGET_EXEC_CREDIT_CONV
SET becc_primitifs = gestion.bdsg_saisi,
  becc_votes = gestion.bdsg_saisi,
  becc_ouverts = gestion.bdsg_saisi -becc_debits + becc_credits,
  becc_disponible = becc_ouverts -becc_engagements -becc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie= 3) THEN
--reliquats

UPDATE BUDGET_EXEC_CREDIT_CONV
SET becc_reliquats = gestion.bdsg_saisi,
  becc_votes = becc_votes + gestion.bdsg_saisi,
  becc_ouverts = becc_ouverts + gestion.bdsg_saisi,
  becc_disponible = becc_ouverts -becc_engagements -becc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;
END IF;

IF (saisie=4) THEN
--dbms

UPDATE BUDGET_EXEC_CREDIT_CONV
SET becc_dbms = becc_dbms + gestion.bdsg_saisi,
  becc_votes = becc_votes + gestion.bdsg_saisi,
  becc_ouverts = becc_ouverts + gestion.bdsg_saisi,
  becc_disponible = becc_ouverts -becc_engagements -becc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;

END IF;

IF (saisie= 5) THEN
--dbms

UPDATE BUDGET_EXEC_CREDIT_CONV
SET becc_conventions = becc_conventions + gestion.bdsg_saisi,
  becc_votes = becc_votes + gestion.bdsg_saisi,
  becc_ouverts = becc_ouverts + gestion.bdsg_saisi,
  becc_disponible = becc_ouverts -becc_engagements -becc_mandats
WHERE exe_ordre = gestion.exe_ordre
 AND org_id = gestion.org_id
 AND tcd_ordre = gestion.tcd_ordre;

END IF;

DELETE FROM jefy_budget.BUDGET_EXEC_CREDIT WHERE TCD_ORDRE IN
(SELECT tcd_ordre FROM jefy_admin.TYPE_CREDIT WHERE TCD_TYPE = 'RECETTE');

END;


PROCEDURE set_budfonc_reserve(bdsaid INTEGER)
IS
cpt INTEGER;
exeordre INTEGER;
orgid INTEGER;
tcdordre INTEGER;
montant NUMBER(12,2);

-- on recupere les monvements negatifs de la DBM en COURS !
CURSOR c1 IS
SELECT exe_ordre ,tcd_ordre ,org_id ,ABS(montant)
FROM (
  SELECT exe_ordre ,tcd_ordre ,org_id ,SUM(bdsg_saisi) montant
  FROM BUDGET_SAISIE_GESTION
  WHERE bdsa_id = bdsaid
  GROUP BY exe_ordre ,tcd_ordre ,org_id)
WHERE montant < 0 ;


BEGIN

 -- on boucle sur les saisie en cours
 -- pour traiter les credit votés NEGATIFS
OPEN C1;
LOOP
FETCH c1 INTO exeordre,tcdordre,orgid,montant;
EXIT WHEN c1%NOTFOUND;
cpt := conv_ra(orgid,exeordre);

IF (cpt = 0) THEN
 Budget_Moteur.set_ligne_budfonc_reserve(exeordre ,tcdordre ,orgid ,montant );
ELSE
 Budget_Moteur.set_ligne_budfonc_conv_reserve(exeordre ,tcdordre ,orgid ,montant );
END IF;

END LOOP;
CLOSE C1;


END;

PROCEDURE set_budfonc_avant_vote_dbm(exeordre INTEGER)
IS
cpt INTEGER;
BEGIN
 SELECT COUNT(*) INTO cpt FROM dual;

 -- on remet les credits des reserves dans le dispo
 -- afin de vote la DBM
 UPDATE jefy_budget.BUDGET_EXEC_CREDIT
 SET bdxc_disponible = bdxc_disponible + bdxc_reserve,bdxc_reserve = 0
 WHERE exe_ordre = exeordre;

  UPDATE jefy_budget.BUDGET_EXEC_CREDIT_CONV
 SET becc_disponible = becc_disponible + becc_reserve,becc_reserve = 0
 WHERE exe_ordre = exeordre;

END;

PROCEDURE set_ligne_budfonc_reserve(exeordre INTEGER,tcdordre INTEGER,orgid INTEGER,montant NUMBER)
IS
cpt INTEGER;
tmpbudgetexec BUDGET_EXEC_CREDIT%ROWTYPE;
BEGIN

 SELECT COUNT(*) INTO cpt
 FROM jefy_budget.BUDGET_EXEC_CREDIT
 WHERE exe_ordre = exeordre
 AND org_id = orgid
 AND tcd_ordre = tcdordre;

 -- on passe la reserve a abs(montant_negatif)
-- si le disponible le permet et maj du dispo

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE IL N EXISTE PAS DE CREDITS');
END IF;

-- on remets a zero partout !!!!!!
set_budfonc_avant_vote_dbm(exeordre);

-- a replace les reserves !!!!
 SELECT * INTO tmpbudgetexec
 FROM jefy_budget.BUDGET_EXEC_CREDIT
 WHERE exe_ordre = exeordre
 AND org_id = orgid
 AND tcd_ordre = tcdordre;

 UPDATE jefy_budget.BUDGET_EXEC_CREDIT
 SET bdxc_reserve = montant , bdxc_disponible = bdxc_disponible - montant
 WHERE exe_ordre = exeordre
 AND org_id = orgid
 AND tcd_ordre = tcdordre;


END;


PROCEDURE set_ligne_budfonc_conv_reserve(exeordre INTEGER,tcdordre INTEGER,orgid INTEGER,montant NUMBER)
IS
cpt INTEGER;
tmpbudgetexec BUDGET_EXEC_CREDIT_CONV%ROWTYPE;

BEGIN

 SELECT COUNT(*) INTO cpt
 FROM jefy_budget.BUDGET_EXEC_CREDIT_CONV
 WHERE exe_ordre = exeordre
 AND org_id = orgid
 AND tcd_ordre = tcdordre;

 -- on passe la reserve a abs(montant_negatif)
-- si le disponible le permet et maj du dispo

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE IL N EXISTE PAS DE CREDITS');
END IF;


 SELECT * INTO tmpbudgetexec
 FROM jefy_budget.BUDGET_EXEC_CREDIT_CONV
 WHERE exe_ordre = exeordre
 AND org_id = orgid
 AND tcd_ordre = tcdordre;

 IF (tmpbudgetexec.becc_disponible - montant < 0) THEN
  RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE RESERVE LES CREDITS DE CETTE DBM NEGATIVE');
 END IF;

 UPDATE jefy_budget.BUDGET_EXEC_CREDIT_CONV
 SET becc_reserve = montant , becc_disponible = becc_disponible - montant
 WHERE exe_ordre = exeordre
 AND org_id = orgid
 AND tcd_ordre = tcdordre;
END;




PROCEDURE verifier_dep_nature_gestion(bdsaid INTEGER) IS

tmpgestion NUMBER(12,   2);
tmpnature NUMBER(12,   2);
racine INTEGER;

BEGIN
racine := Budget_Etat.get_organ_racine;
SELECT SUM(bdsg_montant)
INTO tmpgestion
FROM BUDGET_SAISIE_GESTION
WHERE bdsa_id = bdsaid
 AND org_id = racine
 AND tcd_ordre IN
(SELECT tcd_ordre
 FROM jefy_admin.type_credit
 WHERE tcd_type = 'DEPENSE'
 AND tcd_budget != 'RESERVE')
;

SELECT SUM(bdsn_montant)
INTO tmpnature
FROM BUDGET_SAISIE_NATURE
WHERE bdsa_id = bdsaid
 AND org_id = racine
 AND tcd_ordre IN
(SELECT tcd_ordre
 FROM jefy_admin.type_credit
 WHERE tcd_type = 'DEPENSE'
 AND tcd_budget != 'RESERVE')
;

IF tmpnature != tmpgestion THEN
RAISE_APPLICATION_ERROR(-20001,   'NATURE ('||tmpnature||') ET GESTION ('||tmpgestion||') DESEQUILIBRE EN DEPENSE !');
END IF;

END;



PROCEDURE verifier_dep_rec_nature(bdsaid INTEGER) IS

tmpdep NUMBER(12,   2);
tmprec NUMBER(12,   2);

racine INTEGER;

BEGIN
racine := Budget_Etat.get_organ_racine;

SELECT SUM(bdsn_montant)
INTO tmpdep
FROM BUDGET_SAISIE_NATURE b,
  maracuja.plan_comptable p
WHERE bdsa_id = bdsaid
 AND p.pco_num = b.pco_num
 AND p.pco_nature = 'D'
 AND org_id = racine
 AND tcd_ordre IN
(SELECT tcd_ordre
 FROM jefy_admin.type_credit
 WHERE tcd_type = 'RECETTE'
 AND tcd_budget != 'RESERVE')
;

SELECT SUM(bdsn_montant)
INTO tmprec
FROM BUDGET_SAISIE_NATURE b,
  maracuja.plan_comptable p
WHERE bdsa_id = bdsaid
 AND p.pco_num = b.pco_num
 AND p.pco_nature = 'R'
 AND org_id = racine
 AND tcd_ordre IN
(SELECT tcd_ordre
 FROM jefy_admin.type_credit
 WHERE tcd_type = 'RECETTE'
 AND tcd_budget != 'RESERVE')
;

IF tmpdep != tmprec THEN
RAISE_APPLICATION_ERROR(-20001,   'NATURE ET GESTION DESEQUILIBRE EN DEPENSE !');
END IF;

END;



/******************************************************

******************************************************/
FUNCTION conv_ra(orgid INTEGER,exeordre INTEGER) RETURN INTEGER IS
tyorlibelle jefy_admin.type_organ.tyor_libelle%TYPE;
cpt INTEGER;
BEGIN
-- verifier si c est uns convention RA
-- si RA return 1 sinon 0
--SELECT tyor_libelle
--INTO tyorlibelle
--FROM jefy_admin.organ o,
--  jefy_admin.type_organ t
--WHERE o.tyor_id = t.tyor_id AND o.org_id = orgid;

-- on regarde dans la table convention_limitative
-- si l orgid pour cet exercice est present
-- alors c est une convention RA (limitative budgetairement)
SELECT COUNT(*)
INTO cpt
FROM accords.convention_limitative
WHERE org_id = orgid
AND exe_ordre  = exeordre;

-- convention Ressource Affecteé
IF cpt > 0 THEN
 RETURN 0;
ELSE
 RETURN 0;
END IF;

END;


/***********************************
DEVEROUILLER SAISIE
Deblocage de la saisie d'un CR. Passage de l'etat 'VALIDE' a 'EN COURS'.
***********************************/
PROCEDURE deverrouiller_saisie(bdsaid INTEGER, orgid INTEGER) IS
cpt INTEGER;
BEGIN

UPDATE BUDGET_SAISIE_GESTION SET tyet_id = 100 WHERE bdsa_id = bdsaid AND org_id = orgid;

UPDATE BUDGET_SAISIE_NATURE SET tyet_id = 100 WHERE bdsa_id = bdsaid AND org_id = orgid;

END;


/***********************************
CONTROLE_SAISIE_BUDGETAIRE
Controle des masses de credit en depense, et des budgets ouverts lors d'une saisie negative en DBM.
************************************/
procedure controle_saisie_budgetaire(bdsaid integer, orgid integer) is

cursor c_types_credit(exeordre integer, tcdtype varchar2) is
select t.* from jefy_admin.type_credit t where (tcd_type='DEPENSE' or tcd_type=tcdtype) and tcd_budget='EXECUTOIRE' and exe_ordre = exeordre order by tcd_code;

current_budget_saisie budget_saisie%ROWTYPE;

current_type_credit jefy_admin.type_credit%ROWTYPE;

erreur_dbm VARCHAR2(500);
montcd varchar2(20);
param_RepartNatureLolf 	budget_parametres.bpar_value%type;
param_CtrlSaisieRecette	budget_parametres.bpar_value%type;

masse_gestion number;
masse_nature number;
cpt integer;
tcdordre integer;

begin

    select * into current_budget_saisie from budget_saisie where bdsa_id = bdsaid;

    -- etendre le controle aux recettes en fonction des parametres.
    montcd:='DEPENSE';
    param_RepartNatureLolf := jefy_budget.get_parametre(current_budget_saisie.exe_ordre, 'REPART_NATURE_LOLF');
    param_CtrlSaisieRecette := jefy_budget.get_parametre(current_budget_saisie.exe_ordre, 'CTRL_SAISIE_RECETTE');

    if param_RepartNatureLolf = 'OUI' or param_CtrlSaisieRecette = 'OUI' then
	    montcd:='RECETTE';
    end if;

    OPEN c_types_credit(current_budget_saisie.exe_ordre, montcd);
    LOOP
    FETCH c_types_credit INTO current_type_credit;
    EXIT WHEN c_types_credit%NOTFOUND;

        -- Verification des sommes saisies par masse de crédit
        select sum(bdsn_saisi) into masse_nature from budget_saisie_nature b
        where b.bdsa_id = bdsaid and b.tcd_ordre = current_type_credit.tcd_ordre and b.org_id = orgid;

        select sum(bdsg_saisi) into masse_gestion from budget_saisie_gestion b
        where b.bdsa_id = bdsaid and b.tcd_ordre = current_type_credit.tcd_ordre and b.org_id = orgid;

        if (masse_gestion != masse_nature)
        then

            raise_application_error(-20001, 'ERREUR de SAISIE ! Pour le type de credit '||current_type_credit.tcd_code||' , la masse saisie en GESTION ('||masse_gestion||') est différente de la masse saisie en NATURE ('||masse_nature||') !');

        end if;

    END LOOP;
    CLOSE c_types_credit;


    -- Controle saisie DBM
    if (current_budget_saisie.tysa_id = 4)
    then
          erreur_dbm := controle_saisie_dbm_gestion(bdsaid, orgid);

          IF (erreur_dbm != 'OK')
          THEN
                RAISE_APPLICATION_ERROR (-20001, erreur_dbm);
          END IF;

            erreur_dbm := controle_saisie_dbm_nature(bdsaid, orgid);

          IF (erreur_dbm != 'OK')
          THEN
                RAISE_APPLICATION_ERROR (-20001, erreur_dbm);
          END IF;
     end if;

end;


/***********************************
VALIDER_SAISIE_BUDGET_ORGAN
Validation de la saisie d'une UB ou d'un CR. Passage a l'etat 'VALIDE' apres controles de saisie.
***********************************/
PROCEDURE valider_saisie_budget_organ(bdsaid INTEGER, orgid INTEGER) IS


BEGIN

    controle_saisie_budgetaire(bdsaid, orgid);

    UPDATE BUDGET_SAISIE_GESTION SET tyet_id = 1 WHERE bdsa_id = bdsaid AND org_id = orgid;

    UPDATE BUDGET_SAISIE_NATURE SET tyet_id = 1 WHERE bdsa_id = bdsaid AND org_id = orgid;


END;

/***********************************
CONTROLER_SAISIE_BUDGET_ORGAN
Controle de la saisie d'une UB ou d'un CR. Passage a l'etat 'CONTROLE' apres controles de saisie.
***********************************/
PROCEDURE controler_saisie_budget_organ(bdsaid INTEGER, orgid INTEGER) IS

BEGIN

    controle_saisie_budgetaire(bdsaid, orgid);

    UPDATE BUDGET_SAISIE_GESTION SET tyet_id = 101 WHERE bdsa_id = bdsaid AND org_id = orgid;

    UPDATE BUDGET_SAISIE_NATURE SET tyet_id = 101 WHERE bdsa_id = bdsaid AND org_id = orgid;


END;

/***********************************
CLOTURER_SAISIE_BUDGET_ORGAN
Cloture de la saisie d'une UB ou d'un CR. Passage a l'etat 'CLOTURE' apres controles de saisie.
***********************************/
PROCEDURE cloturer_saisie_budget_organ(bdsaid INTEGER, orgid INTEGER) IS

BEGIN

    controle_saisie_budgetaire(bdsaid, orgid);

    UPDATE BUDGET_SAISIE_GESTION SET tyet_id = 102 WHERE bdsa_id = bdsaid AND org_id = orgid;

    UPDATE BUDGET_SAISIE_NATURE SET tyet_id = 102 WHERE bdsa_id = bdsaid AND org_id = orgid;

END;


/********************************************
VIDER_MASQUE_VIDE
*********************************************/
PROCEDURE vider_masque_vide AS

CURSOR c1 IS
SELECT bdsa_id,org_id , SUM(montant) FROM (
SELECT bdsa_id,org_id , SUM(bdsn_montant) montant FROM BUDGET_SAISIE_NATURE
GROUP BY bdsa_id,org_id
UNION ALL
SELECT bdsa_id,org_id , SUM(bdsg_montant) montant FROM BUDGET_SAISIE_GESTION
GROUP BY bdsa_id,org_id
)
WHERE montant = 0
GROUP BY bdsa_id,org_id;

orgid INTEGER;
bdsaid INTEGER;
montant NUMBER(12,2);


BEGIN
OPEN c1;
LOOP
FETCH c1 INTO bdsaid,orgid,montant;
EXIT WHEN c1%NOTFOUND;

DELETE FROM BUDGET_SAISIE_GESTION WHERE org_id = orgid AND bdsa_id = bdsaid;
DELETE FROM BUDGET_SAISIE_NATURE WHERE org_id = orgid AND bdsa_id = bdsaid;
END LOOP;

END;



/*******************************************************
                       LOCKS
*******************************************************/
PROCEDURE lock_table_budget_saisie
IS
BEGIN
 LOCK TABLE BUDGET_CALCUL_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_CALCUL_NATURE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_SAISIE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_SAISIE_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_SAISIE_NATURE IN EXCLUSIVE MODE;
END;

PROCEDURE lock_table_budget_vote
IS
BEGIN
 LOCK TABLE BUDGET_CALCUL_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_CALCUL_NATURE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_EXEC_CREDIT IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_EXEC_CREDIT_CONV IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_PARAM_EDITIONS IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_SAISIE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_SAISIE_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_SAISIE_NATURE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_GESTION_CALCUL IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_NATURE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_NATURE_CALCUL IN EXCLUSIVE MODE;
END;

PROCEDURE lock_table_budget_mouvement
IS
BEGIN
 LOCK TABLE BUDGET_MOUVEMENTS IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_MOUVEMENTS_CREDIT IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_MOUVEMENTS_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_MOUVEMENTS_NATURE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_GESTION IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_GESTION_CALCUL IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_NATURE IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_VOTE_NATURE_CALCUL IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_EXEC_CREDIT IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_EXEC_CREDIT_CONV IN EXCLUSIVE MODE;
END;

PROCEDURE lock_table_budget_execution
IS
BEGIN
 LOCK TABLE BUDGET_EXEC_CREDIT IN EXCLUSIVE MODE;
 LOCK TABLE BUDGET_EXEC_CREDIT_CONV IN EXCLUSIVE MODE;
END;

/****************************************************
SUPPRIMER__BUDGET
Annulation de la saisie d'un budget.
On vide les tables SAISIE et CALCUL en Gestion et en Nature.
****************************************************/
PROCEDURE supprimer_budget (bdsaid INTEGER, bdsaLibelle VARCHAR2)
IS
	current_budget_saisie BUDGET_SAISIE%ROWTYPE;
    cpt INTEGER;
BEGIN
	SELECT count(*) INTO cpt FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;
	IF (cpt = 0) THEN
		  RAISE_APPLICATION_ERROR (-20001, 'Le budget ' || bdsaLibelle || ' n''existe pas');
	END IF;

	SELECT * INTO current_budget_saisie FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;
	IF (current_budget_saisie.tyet_id <> budget_etat.get_TYPEVOTE)
	THEN
		DELETE FROM BUDGET_SAISIE_NATURE_LOLF WHERE bdsa_id = bdsaid;
        DELETE FROM BUDGET_CALCUL_GESTION WHERE bdsa_id = bdsaid;
        DELETE FROM BUDGET_CALCUL_NATURE WHERE bdsa_id = bdsaid;
        DELETE FROM BUDGET_SAISIE_GESTION WHERE bdsa_id = bdsaid;
        DELETE FROM BUDGET_SAISIE_NATURE WHERE bdsa_id = bdsaid;
        DELETE FROM BUDGET_SAISIE WHERE bdsa_id = bdsaid;
    ELSE
    	RAISE_APPLICATION_ERROR (-20010, 'Le budget ' || bdsaLibelle || ' est voté. Impossible de le supprimer.');
	END IF;
END;

/**********************************************
CONSOLIDER_TOUS_BUDGETS
On vide toutes les tables de consolidation et on reprend tous les budgets pour relancer la consolidation budétaire.
**********************************************/
PROCEDURE Consolider_Tous_Budgets(exeordre INTEGER)
IS

CURSOR c_budgets
IS SELECT * FROM BUDGET_SAISIE WHERE exe_ordre = exeordre
ORDER BY bdsa_id ASC;

current_budget BUDGET_SAISIE%ROWTYPE;

BEGIN

DELETE FROM BUDGET_CALCUL_GESTION WHERE exe_ordre = exeordre;

DELETE FROM BUDGET_CALCUL_NATURE WHERE exe_ordre = exeordre;

DELETE FROM BUDGET_VOTE_GESTION_CALCUL WHERE exe_ordre = exeordre;

DELETE FROM BUDGET_VOTE_NATURE_CALCUL WHERE exe_ordre = exeordre;

OPEN C_budgets;
LOOP
FETCH c_budgets INTO current_budget;
EXIT WHEN c_budgets%NOTFOUND;

            Budget_Moteur.consolider_saisie_budget(current_budget.bdsa_id);

            -- Mise a jour des tables budget_vote_calcul
            -- On ne lance cette procedure que pour les budgets VOTES
            IF (current_budget.tyet_id = 104)
            THEN
                   Budget_Moteur.voter_saisie_budget_calcul(current_budget.bdsa_id);
            END IF;

END LOOP;
CLOSE c_budgets;

-- Mise a jour des comptes de regul dans la table budget_vote_nature_calcul.
Maj_Comptes_Regul_Votes(exeordre);

END;

/****************************************************
MAJ_COMPTES_REGUL_VOTES
Mise a jour des tables BUDGET_VOTE au niveau des comptes CAF, IAF, Fonds de roulement, etc ...
***************************************************/
PROCEDURE Maj_Comptes_Regul_Votes(exeordre INTEGER)
IS

-- Tous les comptes de Regul (005,006,010,011,012,013)
CURSOR c_comptes_regul IS
SELECT DISTINCT pco_num FROM BUDGET_SAISIE_NATURE WHERE exe_ordre=exeordre and pco_num LIKE '0%' ORDER BY pco_num;

-- On met a jour toutes les lignes budgetaires de niveau 0,1 ou 2.
CURSOR c_organ IS
SELECT DISTINCT org_id FROM jefy_admin.v_organ WHERE exe_ordre=exeordre and org_niv IN (0,1,2);

CURSOR c_organ_ub IS
SELECT DISTINCT org_id FROM jefy_admin.v_organ WHERE exe_ordre=exeordre and org_niv IN (2);

current_budget BUDGET_SAISIE%ROWTYPE;

cpt INTEGER;
orgid INTEGER;

compte_regul VARCHAR2(10);

montant_a_traiter NUMBER;

niveau_saisie varchar2(2);

BEGIN

-- Le calcul de ces comptes se fait par rapport a la table budget_calcul_nature.

montant_a_traiter := 0;

-- On recupere le dernier budget vote.
SELECT * INTO current_budget FROM BUDGET_SAISIE WHERE bdsa_id IN (
SELECT MAX(bdsa_id) FROM BUDGET_SAISIE WHERE exe_ordre=exeordre and tyet_id = 104  AND tysa_id IN (1,2,3,4));

OPEN C_organ;
LOOP
FETCH c_organ INTO orgid;
EXIT WHEN c_organ%NOTFOUND;

            OPEN C_comptes_regul;
            LOOP
            FETCH C_comptes_regul INTO compte_regul;
            EXIT WHEN C_comptes_regul%NOTFOUND;

                         SELECT bdcn_calcul INTO montant_a_traiter
                         FROM BUDGET_CALCUL_NATURE
                         WHERE org_id = orgid AND pco_num = compte_regul AND bdsa_id = current_budget.bdsa_id;

                         IF (current_budget.tysa_id = 1)  -- PROVISOIRE
                         THEN
                             UPDATE BUDGET_VOTE_NATURE_CALCUL
                             SET bvnc_provisoires = montant_a_traiter, bvnc_primitifs = 0, bvnc_reliquats = 0, bvnc_dbms = 0,
                               bvnc_ouverts=montant_a_traiter, bvnc_votes=montant_a_traiter
                              WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                          END IF;

                           IF (current_budget.tysa_id = 2)  -- PRIMITIF
                          THEN
                             UPDATE BUDGET_VOTE_NATURE_CALCUL
                             SET bvnc_provisoires = 0, bvnc_primitifs = montant_a_traiter, bvnc_reliquats = 0, bvnc_dbms = 0,
                               bvnc_ouverts=montant_a_traiter, bvnc_votes=montant_a_traiter
                              WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                          END IF;

                           IF (current_budget.tysa_id = 3)  -- RELIQUATS
                          THEN
                             UPDATE BUDGET_VOTE_NATURE_CALCUL
                             SET bvnc_provisoires = 0, bvnc_primitifs = 0, bvnc_reliquats = montant_a_traiter, bvnc_dbms = 0,
                               bvnc_ouverts=montant_a_traiter, bvnc_votes=montant_a_traiter
                              WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                          END IF;

                           IF (current_budget.tysa_id = 4)  -- DBM
                          THEN
                             UPDATE BUDGET_VOTE_NATURE_CALCUL
                             SET bvnc_provisoires = 0, bvnc_primitifs = 0, bvnc_reliquats = 0, bvnc_dbms = montant_a_traiter,
                               bvnc_ouverts=montant_a_traiter, bvnc_votes=montant_a_traiter
                              WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                          END IF;

             END LOOP;
             CLOSE C_comptes_regul;

-- Mise a jour des budgets ouverts et votes.
UPDATE BUDGET_VOTE_NATURE_CALCUL
SET bvnc_ouverts =  BVNC_PROVISOIRES + BVNC_PRIMITIFS + BVNC_RELIQUATS + BVNC_DBMS,
        bvnc_votes = BVNC_PROVISOIRES + BVNC_PRIMITIFS + BVNC_RELIQUATS + BVNC_DBMS
WHERE exe_ordre=exeordre and org_id = orgid AND pco_num LIKE '0%';

END LOOP;
CLOSE c_organ;

select bpar_value into niveau_saisie from budget_parametres where bpar_key = 'NIVEAU_SAISIE' and exe_ordre = current_budget.exe_ordre;


-- Si on saisit le budget au niveau UB on met a jour les comptes de regul de la table budget_vote_nature a partir de ceux de budget_saisie_nature
if (niveau_saisie = 'UB')
then

    OPEN C_organ_ub;
    LOOP
    FETCH c_organ_ub INTO orgid;
    EXIT WHEN c_organ_ub%NOTFOUND;


                OPEN C_comptes_regul;
                LOOP
                FETCH C_comptes_regul INTO compte_regul;
                EXIT WHEN C_comptes_regul%NOTFOUND;

                 select count(*) into cpt from BUDGET_SAISIE_NATURE
                    WHERE org_id = orgid AND pco_num = compte_regul AND bdsa_id = current_budget.bdsa_id;

                 if cpt=1 then
                             SELECT bdsn_montant INTO montant_a_traiter
                             FROM BUDGET_SAISIE_NATURE
                             WHERE org_id = orgid AND pco_num = compte_regul AND bdsa_id = current_budget.bdsa_id;

                             IF (current_budget.tysa_id = 1)  -- PROVISOIRE
                             THEN
                                 UPDATE BUDGET_VOTE_NATURE
                                 SET bdvn_provisoires = montant_a_traiter, bdvn_primitifs = 0, bdvn_reliquats = 0, bdvn_dbms = 0,
                                   bdvn_ouverts=montant_a_traiter, bdvn_votes=montant_a_traiter
                                  WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                              END IF;

                               IF (current_budget.tysa_id = 2)  -- PRIMITIF
                              THEN
                                 UPDATE BUDGET_VOTE_NATURE
                                 SET bdvn_provisoires = 0, bdvn_primitifs = montant_a_traiter, bdvn_reliquats = 0, bdvn_dbms = 0,
                                   bdvn_ouverts=montant_a_traiter, bdvn_votes=montant_a_traiter
                                  WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                              END IF;

                               IF (current_budget.tysa_id = 3)  -- RELIQUATS
                              THEN
                                 UPDATE BUDGET_VOTE_NATURE
                                 SET bdvn_provisoires = 0, bdvn_primitifs = 0, bdvn_reliquats = montant_a_traiter, bdvn_dbms = 0,
                                   bdvn_ouverts=montant_a_traiter, bdvn_votes=montant_a_traiter
                                  WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                              END IF;

                               IF (current_budget.tysa_id = 4)  -- DBM
                              THEN
                                 UPDATE BUDGET_VOTE_NATURE
                                 SET bdvn_provisoires = 0, bdvn_primitifs = 0, bdvn_reliquats = 0, bdvn_dbms = montant_a_traiter,
                                   bdvn_votes=montant_a_traiter,  bdvn_ouverts=montant_a_traiter
                                  WHERE org_id = orgid AND exe_ordre = exeordre AND pco_num = compte_regul;
                              END IF;
                 end if;

                 END LOOP;
                 CLOSE C_comptes_regul;


    -- Mise a jour des budgets ouverts et votes.
    UPDATE BUDGET_vote_nature
    SET bdvn_ouverts =  bdvn_PROVISOIRES + bdvn_PRIMITIFS + bdvn_RELIQUATS + bdvn_DBMS,
            bdvn_votes = bdvn_PROVISOIRES + bdvn_PRIMITIFS + bdvn_RELIQUATS + bdvn_DBMS
    WHERE exe_ordre=exeordre and org_id = orgid AND pco_num LIKE '0%';


    END LOOP;
    CLOSE c_organ_ub;

end if;

END;


/*********************************************

**********************************************/
FUNCTION controle_saisie_dbm_gestion(bdsaid INTEGER, orgid INTEGER) RETURN VARCHAR
IS

retour VARCHAR2(200);

erreur_dbm VARCHAR2(500);

erreurs_depense_gestion INTEGER;
erreurs_recette_gestion INTEGER;

BEGIN

retour := 'OK';

                SELECT COUNT(*) INTO erreurs_depense_gestion
                FROM  BUDGET_SAISIE_GESTION b, BUDGET_VOTE_GESTION bv
                WHERE b.bdsa_id = bdsaid
                                AND b.org_id = orgid
                                AND b.org_id = bv.org_id
                                AND b.tcd_ordre = bv.tcd_ordre
                                AND b.tyac_id = bv.tyac_id
                AND (ABS(b.bdsg_saisi) > bv.bdvg_ouverts   OR ABS(b.bdsg_saisi) > bv.bdvg_votes)
                                AND b.bdsg_saisi < 0;

                IF (erreurs_depense_gestion > 0)
                THEN
                                            -- Controle de la saisie de la dbm ==> Si le budget saisi est négatif, le budget ouvert ne doit pas etre au final < 0
                            SELECT 'Montant saisi pour l''action '||l.lolf_code||' : '||b.bdsg_saisi||'  >  Budget Ouvert : '||bv.bdvg_ouverts||' ou > Budget Voté : '||bdvg_votes INTO retour
                            FROM BUDGET_SAISIE bud, BUDGET_SAISIE_GESTION b, BUDGET_VOTE_GESTION bv, jefy_admin.organ o,
                            jefy_admin.type_credit t , (SELECT * FROM jefy_admin.lolf_nomenclature_depense UNION ALL SELECT * FROM jefy_admin.lolf_nomenclature_recette )  l
                            WHERE bud.bdsa_id = b.bdsa_id AND  b.org_id = bv.org_id AND b.tyac_id = bv.tyac_id AND b.tcd_ordre = bv.tcd_ordre
                            AND bud.bdsa_id = bdsaid  AND b.tyac_id = l.lolf_id AND o.org_id = orgid
                            AND b.org_id = o.org_id AND b.tcd_ordre = t.tcd_ordre AND (ABS(b.bdsg_saisi) > bv.bdvg_ouverts   OR ABS(b.bdsg_saisi) > bv.bdvg_votes) AND b.bdsg_saisi < 0
                            AND ROWNUM = 1
                            ORDER BY o.org_ub, o.org_cr, t.tcd_code;

                            RETURN retour;

                END IF;

RETURN retour;

END;


/************************************************************

**********************************************************/
FUNCTION controle_saisie_dbm_nature(bdsaid INTEGER, orgid INTEGER) RETURN VARCHAR
IS

retour VARCHAR2(200);

erreur_dbm VARCHAR2(500);

erreurs_depense_gestion INTEGER;
erreurs_recette_gestion INTEGER;

BEGIN

retour := 'OK';

                SELECT COUNT(*) INTO erreurs_depense_gestion
                FROM  BUDGET_SAISIE_NATURE b, BUDGET_VOTE_NATURE bv
                WHERE b.bdsa_id = bdsaid
                                AND b.org_id = orgid
                                AND b.org_id = bv.org_id
                                AND b.tcd_ordre = bv.tcd_ordre
                                AND b.pco_num = bv.pco_num
                                AND (ABS(b.bdsn_saisi) > bv.bdvn_ouverts   OR ABS(b.bdsn_saisi) > bv.bdvn_votes)
                                AND b.bdsn_saisi < 0;

                IF (erreurs_depense_gestion > 0)
                THEN
                            -- Controle de la saisie de la dbm ==> Si le budget saisi est négatif, le budget ouvert ne doit pas etre au final < 0
                            SELECT 'Montant saisi pour le compte '||p.pco_num||' : '||b.bdsn_saisi||'  >  Budget Ouvert : '||bv.bdvn_ouverts||' ou > Budget Voté : '||bdvn_votes INTO retour
                            FROM BUDGET_SAISIE bud, BUDGET_SAISIE_NATURE b, BUDGET_VOTE_NATURE bv, jefy_admin.organ o,
                            jefy_admin.type_credit t , maracuja.plan_comptable p
                            WHERE bud.bdsa_id = b.bdsa_id AND  b.org_id = bv.org_id AND b.pco_num = bv.pco_num AND b.tcd_ordre = bv.tcd_ordre
                            AND bud.bdsa_id = bdsaid  AND b.pco_num = p.pco_num AND o.org_id = orgid
                            AND b.org_id = o.org_id AND b.tcd_ordre = t.tcd_ordre AND (ABS(b.bdsn_saisi) > bv.bdvn_ouverts   OR ABS(b.bdsn_saisi) > bv.bdvn_votes) AND b.bdsn_saisi < 0
                            AND ROWNUM = 1
                            ORDER BY o.org_ub, o.org_cr, t.tcd_code;

                            RETURN retour;

                END IF;

RETURN retour;

END;



/*********************************************

**********************************************/
FUNCTION controle_saisie_dbm_ges_imp(bdsaid INTEGER, orgid INTEGER,tcdordre INTEGER,tyacid INTEGER) RETURN VARCHAR
IS

retour VARCHAR2(200);

erreur_dbm VARCHAR2(500);

erreurs_depense_gestion INTEGER;
erreurs_recette_gestion INTEGER;

BEGIN

retour := 'OK';

                SELECT COUNT(*) INTO erreurs_depense_gestion
                FROM  BUDGET_SAISIE_GESTION b, BUDGET_VOTE_GESTION bv
                WHERE b.bdsa_id = bdsaid
                                AND b.org_id = orgid
                                AND b.org_id = bv.org_id
                                AND b.tcd_ordre = bv.tcd_ordre
                                AND b.tyac_id = bv.tyac_id
                                AND (ABS(b.bdsg_saisi) > bv.bdvg_ouverts  OR ABS(b.bdsg_saisi) > bv.bdvg_votes)
                                AND b.bdsg_saisi < 0
                                AND b.tyac_id = tyacid
                                AND b.tcd_ordre = tcdordre;

                IF (erreurs_depense_gestion > 0)
                THEN
                                            -- Controle de la saisie de la dbm ==> Si le budget saisi est négatif, le budget ouvert ne doit pas etre au final < 0
                            SELECT 'Montant saisi pour l''action '||l.lolf_code||' : '||b.bdsg_saisi||'  >  Budget Ouvert : '||bv.bdvg_ouverts||' ou > Budget Voté : '||bdvg_votes INTO retour
                            FROM BUDGET_SAISIE bud, BUDGET_SAISIE_GESTION b, BUDGET_VOTE_GESTION bv, jefy_admin.organ o,
                            jefy_admin.type_credit t , (SELECT * FROM jefy_admin.lolf_nomenclature_depense UNION ALL SELECT * FROM jefy_admin.lolf_nomenclature_recette )  l
                            WHERE bud.bdsa_id = b.bdsa_id AND  b.org_id = bv.org_id AND b.tyac_id = bv.tyac_id AND b.tcd_ordre = bv.tcd_ordre
                            AND bud.bdsa_id = bdsaid  AND b.tyac_id = l.lolf_id AND o.org_id = orgid
                            AND b.org_id = o.org_id AND b.tcd_ordre = t.tcd_ordre AND (ABS(b.bdsg_saisi) > bv.bdvg_ouverts  OR ABS(b.bdsg_saisi) > bv.bdvg_votes) AND b.bdsg_saisi < 0
--                            AND ROWNUM = 1
                                                        AND b.tyac_id = tyacid
                                                        AND b.tcd_ordre = tcdordre;
                            --ORDER BY o.org_ub, o.org_cr, t.tcd_code;

                            RETURN retour;

                END IF;

RETURN retour;

END;



FUNCTION controle_saisie_dbm_nat_imp(bdsaid INTEGER, orgid INTEGER,tcdordre INTEGER,pconum VARCHAR) RETURN VARCHAR
IS

retour VARCHAR2(200);

erreur_dbm VARCHAR2(500);

erreurs_depense_gestion INTEGER;
erreurs_recette_gestion INTEGER;

BEGIN

retour := 'OK';

                SELECT COUNT(*) INTO erreurs_depense_gestion
                FROM  BUDGET_SAISIE_NATURE b, BUDGET_VOTE_NATURE bv
                WHERE b.bdsa_id = bdsaid
                                AND b.org_id = orgid
                                AND b.org_id = bv.org_id
                                AND b.tcd_ordre = bv.tcd_ordre
                                AND b.pco_num = bv.pco_num
                                AND (ABS(b.bdsn_saisi) > bv.bdvn_ouverts  OR ABS(b.bdsn_saisi) > bv.bdvn_votes)
                                AND b.tcd_ordre =tcdordre
                                AND b.pco_num = pconum
                                AND b.bdsn_saisi < 0;

                IF (erreurs_depense_gestion > 0)
                THEN
                                            -- Controle de la saisie de la dbm ==> Si le budget saisi est négatif, le budget ouvert ne doit pas etre au final < 0
                            SELECT 'Montant saisi pour le compte '||p.pco_num||' : '||b.bdsn_saisi||'  >  Budget Ouvert : '||bv.bdvn_ouverts||' ou > Budget Voté : '||bdvn_votes INTO retour
                            FROM BUDGET_SAISIE bud, BUDGET_SAISIE_NATURE b, BUDGET_VOTE_NATURE bv, jefy_admin.organ o,
                            jefy_admin.type_credit t , maracuja.plan_comptable p
                            WHERE bud.bdsa_id = b.bdsa_id AND  b.org_id = bv.org_id AND b.pco_num = bv.pco_num AND b.tcd_ordre = bv.tcd_ordre
                            AND bud.bdsa_id = bdsaid  AND b.pco_num = p.pco_num AND o.org_id = orgid
                            AND b.org_id = o.org_id AND b.tcd_ordre = t.tcd_ordre AND (ABS(b.bdsn_saisi) > bv.bdvn_ouverts  OR ABS(b.bdsn_saisi) > bv.bdvn_votes) AND b.bdsn_saisi < 0
                                                                                        AND b.tcd_ordre =tcdordre
                                AND b.pco_num = pconum;

                            RETURN retour;

                END IF;

RETURN retour;

END;





FUNCTION controle_saisie_dbm_exe_imp(bdsaid INTEGER, orgid INTEGER,tcdordre INTEGER) RETURN VARCHAR
IS

retour VARCHAR2(200);
erreur_dbm VARCHAR2(500);
erreurs_dispo INTEGER;

BEGIN

retour := 'OK';

SELECT COUNT(*)  INTO erreurs_dispo
FROM
(
SELECT o.org_id, o.org_ub, o.org_cr, t.tcd_code, SUM(bdsg_saisi) , bdxc.bdxc_disponible
FROM BUDGET_SAISIE_GESTION b, jefy_admin.organ o, jefy_admin.type_credit t , BUDGET_EXEC_CREDIT bdxc
WHERE bdsa_id  = bdsaid
AND b.org_id = o.org_id AND b.tcd_ordre = t.tcd_ordre AND t.tcd_type = 'DEPENSE'  AND o.org_id = orgid
AND b.org_id = bdxc.org_id AND b.tcd_ordre = bdxc.tcd_ordre AND t.tcd_ordre = tcdordre
GROUP BY o.org_id, o.org_ub, o.org_cr, t.tcd_code , bdxc.bdxc_disponible
HAVING SUM(bdsg_saisi) < 0 AND ( SUM(bdsg_saisi)  + bdxc_disponible < 0)
);


IF (erreurs_dispo > 0)
THEN

             SELECT 'Disponible Insuffisant. Total Saisi '||saisi||' > Dispo : '||dispo INTO retour
            FROM (
            SELECT o.org_id, o.org_ub, o.org_cr, t.tcd_code, SUM(bdsg_saisi)  saisi, bdxc.bdxc_disponible dispo
            FROM BUDGET_SAISIE_GESTION b, jefy_admin.organ o, jefy_admin.type_credit t , BUDGET_EXEC_CREDIT bdxc
            WHERE bdsa_id  = bdsaid
            AND b.org_id = o.org_id AND b.tcd_ordre = t.tcd_ordre AND t.tcd_type = 'DEPENSE'  AND o.org_id = orgid
            AND b.org_id = bdxc.org_id AND b.tcd_ordre = bdxc.tcd_ordre AND t.tcd_ordre = tcdordre
            GROUP BY o.org_id, o.org_ub, o.org_cr, t.tcd_code , bdxc.bdxc_disponible
            HAVING SUM(bdsg_saisi) < 0 AND ( SUM(bdsg_saisi)  + bdxc_disponible < 0)
            );

            RETURN retour;

END IF;

RETURN retour;

END;



/********************************************
SUPPRIMER_SAISIE_BUDGET
Suppression de la saisie d'une UB ou d'un CR.
********************************************/
PROCEDURE  supprimer_saisie_budget (bdsaid INTEGER,orgid INTEGER)
IS

cpt INTEGER;

BEGIN

SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_GESTION
WHERE org_id = orgid
AND bdsa_id =bdsaid
AND (bdsg_saisi != 0 OR bdsg_vote !=0);

IF  cpt > 0 THEN
  RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER CE MASQUE DE SAISIE ! SAISIE EN COURS.');
END IF;

SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_NATURE
WHERE org_id = orgid
AND bdsa_id =bdsaid
AND ( bdsn_saisi != 0 OR  bdsn_vote !=0);

IF  cpt > 0 THEN
  RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER CE MASQUE DE SAISIE ! SAISIE EN COURS.');
END IF;

DELETE FROM BUDGET_SAISIE_GESTION
WHERE org_id = orgid
AND bdsa_id =bdsaid;

DELETE FROM BUDGET_SAISIE_NATURE
WHERE org_id = orgid
AND bdsa_id =bdsaid;

DELETE FROM BUDGET_CALCUL_GESTION
WHERE org_id = orgid
AND bdsa_id =bdsaid;

DELETE FROM BUDGET_CALCUL_NATURE
WHERE org_id = orgid
AND bdsa_id =bdsaid;

END;


/*************************************
ADD_MASQUE_NATURE
Ajout d'un compte dans le masque du budget par nature.
Le compte est ajouté a tous les budgets saisis pour l'annee concernee
**************************************/
PROCEDURE Add_Masque_Nature ( tcdordre NUMBER, pconum VARCHAR, exeordre NUMBER)
IS

CURSOR c_budgets
IS SELECT bdsa_id FROM BUDGET_SAISIE WHERE exe_ordre = exeordre AND tyet_id  NOT IN (2);

currentPlanco maracuja.plan_comptable%ROWTYPE;

pconum_reference BUDGET_MASQUE_NATURE.pco_num%TYPE;
tcd_ordre_reference jefy_admin.type_credit.tcd_ordre%TYPE;

bdsaid INTEGER;
cpt INTEGER;
tyetid integer;

BEGIN

SELECT * INTO currentPlanco FROM maracuja.plan_comptable WHERE pco_num = pconum;

-- Insertion dans la masque du budget de gestion
SELECT COUNT(*) INTO cpt FROM BUDGET_MASQUE_NATURE WHERE exe_ordre = exeordre AND pco_num = pconum and tcd_ordre = tcdordre;

IF (cpt = 0)
THEN

            INSERT INTO BUDGET_MASQUE_NATURE
            (
            BMN_ID, EXE_ORDRE, PCO_NUM, PCO_NUM_VOTE, TCD_ORDRE, TYET_ID
            )
            VALUES
            (
            budget_masque_nature_seq.NEXTVAL, exeordre, pconum, pconum, tcdordre, 1
            );

END IF;

-- On parcourt tous les budgets et on ajoute l'action correspondante

        OPEN c_budgets;
        LOOP
        FETCH c_budgets
        INTO bdsaid;
        EXIT
        WHEN c_budgets % NOTFOUND;

            SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_NATURE WHERE bdsa_id = bdsaid;

            if cpt > 0 then

                    SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_NATURE
                    WHERE bdsa_id = bdsaid AND pco_num = pconum AND tcd_ordre = tcdordre;

                    IF (cpt = 0)
                    THEN

                        select count(*) into cpt from BUDGET_SAISIE_NATURE b, jefy_admin.type_credit t
                        WHERE bdsa_id = bdsaid AND b.tcd_ordre = tcdordre;

                        if (cpt > 0)
                        then

                            SELECT pco_num INTO pconum_reference
                            FROM BUDGET_SAISIE_NATURE b, jefy_admin.type_credit t
                            WHERE bdsa_id = bdsaid AND b.tcd_ordre = tcdordre AND ROWNUM = 1;

                            INSERT INTO BUDGET_SAISIE_NATURE
                            (
                            BDSN_ID, BDSA_ID, ORG_ID, TCD_ORDRE, PCO_NUM, BDSN_VOTE, BDSN_SAISI, BDSN_MONTANT, EXE_ORDRE, TYET_ID
                            )
                            SELECT
                            budget_saisie_nature_seq.NEXTVAL, BDSA_ID, ORG_ID, TCD_ORDRE, pconum, 0, 0, 0, EXE_ORDRE, TYET_ID
                            FROM BUDGET_SAISIE_NATURE WHERE bdsa_id = bdsaid AND pco_num = pconum_reference AND tcd_ordre = tcdordre;

                        else

                            select tyet_id into tyetid from budget_saisie_nature where bdsa_id = bdsaid and rownum = 1;

                            INSERT INTO BUDGET_SAISIE_NATURE
                            (
                            BDSN_ID, BDSA_ID, ORG_ID, TCD_ORDRE, PCO_NUM, BDSN_VOTE, BDSN_SAISI, BDSN_MONTANT, EXE_ORDRE, tyet_id
                            )
                            SELECT
                            budget_saisie_nature_seq.NEXTVAL, bdsaid, ORG_ID, tcdordre, pconum, 0, 0, 0, exeordre, tyetid
                            FROM jefy_admin.organ o
                            WHERE org_id in (select distinct org_id from budget_saisie_nature where bdsa_id = bdsaid);

                        end if;

                    END IF;
            end if;

        END LOOP;
        CLOSE c_budgets;

END;


/****************************************

******************************************/
PROCEDURE Add_Masque_Gestion ( lolfid NUMBER, lolftype v_lolf_nomenclature.tyac_type%TYPE, exeordre NUMBER)
IS

CURSOR c_budgets
IS SELECT bdsa_id FROM BUDGET_SAISIE WHERE exe_ordre = exeordre AND tyet_id  NOT IN (2, 104);

lolfid_reference BUDGET_MASQUE_GESTION.tyac_id%TYPE;
bdsaid INTEGER;
cpt INTEGER;

BEGIN

-- Insertion dans la masque du budget de gestion
SELECT COUNT(*) INTO cpt FROM BUDGET_MASQUE_GESTION WHERE exe_ordre = exeordre AND tyac_id = lolfid;

IF (cpt = 0)
THEN

            INSERT INTO BUDGET_MASQUE_GESTION
            (
            BMG_ID, EXE_ORDRE, TYAC_ID, TYET_ID
            )
            VALUES
            (
            budget_masque_gestion_seq.NEXTVAL, exeordre, lolfid, 1
            );

END IF;

-- On parcourt tous les budgets et on ajoute l'action correspondante

        OPEN c_budgets;
        LOOP
        FETCH c_budgets
        INTO bdsaid;
        EXIT
        WHEN c_budgets % NOTFOUND;

        SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_GESTION WHERE bdsa_id = bdsaid;

        if cpt > 0 then

            SELECT COUNT(*) INTO cpt FROM BUDGET_SAISIE_GESTION
            WHERE bdsa_id = bdsaid AND tyac_id = lolfid;

            IF (cpt = 0)
            THEN
                INSERT INTO BUDGET_SAISIE_GESTION
                (
                BDSG_ID, BDSA_ID, ORG_ID, TCD_ORDRE, TYAC_ID, BDSG_VOTE, BDSG_SAISI, BDSG_MONTANT, EXE_ORDRE, TYET_ID
                )
                SELECT
                BUDGET_SAISIE_GESTION_seq.NEXTVAL, bdsaid, ORG_ID, TCD_ORDRE, lolfid,
                0, 0, 0, exeordre, TYET_ID
                FROM (select distinct s.org_id, mc.tcd_ordre, s.tyet_id
                  from budget_saisie_gestion s, budget_masque_credit mc, v_type_credit_budget t
                  where s.bdsa_id=bdsaid and mc.exe_ordre=exeordre and t.tcd_type=lolftype and mc.tcd_ordre=t.tcd_ordre);

            END IF;

        end if;

        END LOOP;
        CLOSE c_budgets;

END;



/****************************************

******************************************/
PROCEDURE del_Masque_Gestion ( lolfid NUMBER, exeordre NUMBER)
IS

cpt INTEGER;

current_action v_lolf_nomenclature%ROWTYPE;

montant_action number;

BEGIN



select * into current_action from v_lolf_nomenclature where tyac_id = lolfid and exe_ordre = exeordre;


-- On controle qu'aucaun budget n'ait ete vote avant la suppression
select count(*) into cpt from budget_saisie where exe_ordre = exeordre and tyet_id = 104;

if (cpt > 0)
then
    raise_application_error(-20001, 'Vous ne pouvez pas supprimer l''action '||current_action.tyac_code||'  , des budgets ont déjà  été votés pour l''exercice '||exeordre||' !');
end if;

-- Pour pouvoir supprimer une action, il faut vérifier qu'elle n'ait servi dans aucun budget
-- ==> Tous les montants Saisi, Vote et Montant doivent etre a 0.

select sum(bdsg_vote+bdsg_saisi+bdsg_montant) into montant_action
from budget_saisie_gestion bsg, budget_saisie bs
where bsg.bdsa_id = bs.bdsa_id and bs.exe_ordre = exeordre and bsg.tyac_id = lolfid;


if (montant_action > 0)
then
    raise_application_error(-20001, 'Vous ne pouvez pas supprimer l''action '||current_action.tyac_code||' , elle est déjà  utilisée dans la saisie budgétaire '||exeordre||' !');
end if;

-- On supprime l'action sélectionnée

delete from budget_vote_gestion where tyac_id = lolfid and exe_ordre = exeordre and bdvg_votes = 0 and bdvg_ouverts = 0;

delete from budget_calcul_gestion where tyac_id = lolfid and bdcg_calcul = 0 and bdsa_id in (select bdsa_id from budget_saisie where exe_ordre = exeordre);

delete from budget_saisie_gestion where tyac_id = lolfid and bdsg_montant = 0 and bdsa_id in (select bdsa_id from budget_saisie where exe_ordre = exeordre);

delete from budget_masque_gestion where tyac_id = lolfid and exe_ordre = exeordre;


END;



/****************************************

******************************************/
PROCEDURE del_Masque_nature ( tcdordre NUMBER, pconum varchar,  exeordre NUMBER)
IS

--cpt INTEGER;
nb INTEGER;

--current_plan_comptable maracuja.plan_comptable%ROWTYPE;

--montant_action number;

BEGIN


/*select count(*) into cpt from budget_saisie where exe_ordre = exeordre and tyet_id = 104;

if (cpt > 0)
then
    raise_application_error(-20001, 'Vous ne pouvez pas supprimer le compte '||pconum||' , des budgets ont déjà  été votés pour l''exercice '||exeordre||' !');
end if;

select * into current_plan_comptable from maracuja.plan_comptable where pco_num = pconum;
*/
-- Pour pouvoir supprimer un compte, il faut vérifier qu'il n'ait servi dans aucun budget
-- ==> Tous les montants Saisi, Vote et Montant doivent etre a 0.

select count(*) into nb
from budget_saisie_nature bsn, budget_saisie bs
where bsn.bdsa_id = bs.bdsa_id and bs.exe_ordre = exeordre and bsn.tcd_ordre = tcdordre and bsn.pco_num = pconum
  and (bdsn_vote<>0 or bdsn_saisi<>0 or bdsn_montant<>0);

if (nb > 0)
then
    raise_application_error(-20001, 'Vous ne pouvez pas supprimer le compte '||pconum||' , il est déjà  utilisé dans la saisie budgétaire '||exeordre||' !');
end if;

-- On supprime l'action sélectionnée

delete from budget_vote_nature where pco_num = pconum and tcd_ordre = tcdordre and exe_ordre = exeordre and bdvn_votes = 0 and bdvn_ouverts = 0;

delete from budget_calcul_nature where pco_num = pconum and tcd_ordre = tcdordre and bdcn_calcul = 0 and bdsa_id in (select bdsa_id from budget_saisie where exe_ordre = exeordre);

delete from budget_saisie_nature
where pco_num = pconum and tcd_ordre = tcdordre and bdsn_montant = 0 and bdsa_id in (select bdsa_id from budget_saisie where exe_ordre = exeordre);

delete from budget_masque_nature where pco_num = pconum and tcd_ordre = tcdordre and exe_ordre = exeordre;


END;



/***************************************
DUPLIQUER_MASQUE.
Duplication d'un masque de saisie (Credit, Gestion ou Nature) d'une annee sur l'autre
*****************************************/
procedure dupliquer_masque(exeordre number, typemasque varchar)
is

cursor c_old_masque_credit(oldexercice integer)
is select * from budget_masque_credit where exe_ordre = oldexercice;
old_masque_credit budget_masque_credit%ROWTYPE;

cursor c_old_masque_gestion(oldexercice integer)
is select * from budget_masque_gestion where exe_ordre = oldexercice;
old_masque_gestion budget_masque_gestion%ROWTYPE;

cursor c_old_masque_nature(oldexercice integer)
is select b.* from budget_masque_nature b, jefy_admin.type_credit t
where b.exe_ordre = oldexercice and b.tcd_ordre = t.tcd_ordre and t.exe_ordre = oldexercice
and t.tcd_code in (select distinct tcd_code from jefy_admin.type_credit where tcd_ordre in(select tcd_ordre from budget_masque_credit where exe_ordre = exeordre));

old_masque_nature budget_masque_nature%ROWTYPE;

tcdcode jefy_admin.type_credit.tcd_code%TYPE;

tcdordre integer;
cpt integer;

begin

if (typemasque = 'CREDIT')
then

    -- Types de credit
    OPEN c_old_masque_credit(exeordre - 1);
    LOOP
    FETCH c_old_masque_credit
    INTO old_masque_credit;
    EXIT
    WHEN c_old_masque_credit % NOTFOUND;

        select tcd_code into tcdcode from jefy_admin.type_credit where tcd_ordre = old_masque_credit.tcd_ordre;

        select tcd_ordre into tcdordre from jefy_admin.type_credit where exe_ordre = exeordre and tcd_code = tcdcode;

        select count(*) into cpt from budget_masque_credit where exe_ordre = exeordre and tcd_ordre = tcdordre;

        if (cpt = 0)
        then
            insert into budget_masque_credit
            (BMC_ID, EXE_ORDRE, TCD_ORDRE, TYET_ID)
            values(budget_masque_credit_seq.nextval, exeordre, tcdordre, 1);
        end if;

    END LOOP;
       CLOSE c_old_masque_credit;

end if;

if (typemasque = 'GESTION')
then

    OPEN c_old_masque_gestion(exeordre - 1);
    LOOP
    FETCH c_old_masque_gestion
    INTO old_masque_gestion;
    EXIT
    WHEN c_old_masque_gestion % NOTFOUND;

        select count(*) into cpt from budget_masque_gestion where exe_ordre = exeordre and tyac_id = old_masque_gestion.tyac_id;

        if (cpt = 0)
        then
            select count(*) into cpt from
            (
               SELECT lolf_id FROM v_lolf_nomenclature_bud_dep where exe_ordre=exe_ordre
               union all
               SELECT lolf_id FROM v_lolf_nomenclature_bud_rec where exe_ordre=exe_ordre
            ) where lolf_id=old_masque_gestion.tyac_id;

            if cpt>0 then
               insert into budget_masque_gestion (BMG_ID, EXE_ORDRE, TYAC_ID, TYET_ID)
                 values(budget_masque_gestion_seq.nextval, exeordre, old_masque_gestion.tyac_id, 1);
            end if;
        end if;

    END LOOP;
       CLOSE c_old_masque_gestion;

end if;


if (typemasque = 'NATURE')
then

    OPEN c_old_masque_nature(exeordre - 1);
    LOOP
    FETCH c_old_masque_nature
    INTO old_masque_nature;
    EXIT
    WHEN c_old_masque_nature % NOTFOUND;

        select tcd_code into tcdcode from jefy_admin.type_credit where tcd_ordre = old_masque_nature.tcd_ordre;

        select tcd_ordre into tcdordre from jefy_admin.type_credit where exe_ordre = exeordre and tcd_code = tcdcode;

        select count(*) into cpt from budget_masque_nature where exe_ordre = exeordre and tcd_ordre = tcdordre and pco_num = old_masque_nature.pco_num;

        if (cpt = 0)
        then

            insert into budget_masque_nature
            (BMN_ID, EXE_ORDRE, PCO_NUM, PCO_NUM_VOTE, TCD_ORDRE, TYET_ID)
            values(budget_masque_nature_seq.nextval, exeordre, old_masque_nature.pco_num, old_masque_nature.pco_num_vote, tcdordre, 1);

        end if;

    END LOOP;
       CLOSE c_old_masque_nature;

end if;


end;


FUNCTION get_etat_saisie(bdsaid INTEGER, orgid INTEGER) RETURN VARCHAR
Is

retour varchar2(200);
cpt integer;

current_saisie_nature budget_saisie_nature%ROWTYPE;
etat jefy_admin.type_etat.tyet_libelle%TYPE;

begin

select count(*) into cpt from budget_saisie_nature where bdsa_id = bdsaid and org_id = orgid;

if (cpt = 0)
then
    retour := 'NON INITIALISE';
else

    select * into current_saisie_nature from budget_saisie_nature where bdsa_id = bdsaid
    and org_id = orgid and rownum = 1;

    select tyet_libelle into retour from jefy_admin.type_etat where tyet_id = current_saisie_nature.tyet_id;

end if;


return retour;

end;

-- saisie nature lolf
  PROCEDURE cloturer_nature_lolf(bdsaid INTEGER, tcdordre INTEGER) is

    cursor c_action is select distinct tyac_id from budget_saisie_nature_lolf where bdsa_id=bdsaid and tcd_ordre=tcdordre;
    cursor c_nature is select distinct pco_num from budget_saisie_nature_lolf where bdsa_id=bdsaid and tcd_ordre=tcdordre;

    somme_nature  budget_saisie_nature.bdsn_saisi%type;
    somme_gestion budget_saisie_gestion.bdsg_saisi%type;
    somme_natlolf budget_saisie_nature_lolf.bdsl_saisi%type;

    my_tyac_id budget_saisie_nature_lolf.tyac_id%type;
    my_pco_num budget_saisie_nature_lolf.pco_num%type;
    my_tyac_code v_type_action.tyac_code%type;
  begin
    OPEN c_nature;
    LOOP
       FETCH c_nature INTO my_pco_num;
       EXIT WHEN c_nature%NOTFOUND;

       select nvl(sum(bdsn_saisi),0) into somme_nature from budget_saisie_nature l, budget_masque_nature m
          where l.bdsa_id=bdsaid and l.tcd_ordre=tcdordre and nvl(m.pco_num_vote, l.pco_num)=my_pco_num and l.pco_num=m.pco_num(+) and l.tcd_ordre=m.tcd_ordre(+) and l.exe_ordre=m.exe_ordre(+);
       select nvl(sum(bdsl_saisi),0) into somme_natlolf from budget_saisie_nature_lolf where bdsa_id=bdsaid and tcd_ordre=tcdordre and pco_num=my_pco_num;

       if somme_nature<>somme_natlolf then
          RAISE_APPLICATION_ERROR(-20001,'Pb de montant pour la repartition de l''imputation '||my_pco_num||
             ', bud.gestion='||somme_nature||' , repartition='||somme_natlolf);
       end if;

       END LOOP;
       CLOSE c_nature;

    OPEN c_action;
    LOOP
       FETCH c_action INTO my_tyac_id;
       EXIT WHEN c_action%NOTFOUND;

       select nvl(sum(bdsg_saisi),0) into somme_gestion from budget_saisie_gestion where bdsa_id=bdsaid and tcd_ordre=tcdordre and tyac_id=my_tyac_id;
       select nvl(sum(bdsl_saisi),0) into somme_natlolf from budget_saisie_nature_lolf where bdsa_id=bdsaid and tcd_ordre=tcdordre and tyac_id=my_tyac_id;

       if somme_gestion<>somme_natlolf then
          select tyac_code into my_tyac_code from v_type_action a, jefy_admin.type_credit t
             where t.tcd_ordre=tcdordre and t.exe_ordre=a.exe_ordre and tyac_id=my_tyac_id;
          RAISE_APPLICATION_ERROR(-20001,'Pb de montant pour la repartition de l''action '||my_tyac_code||
             ', bud.gestion='||somme_gestion||' , repartition='||somme_natlolf);
       end if;

       END LOOP;
       CLOSE c_action;

    update budget_saisie_nature_lolf set tyet_id=102 where bdsa_id=bdsaid and tcd_ordre=tcdordre;

  end;

  PROCEDURE init_saisie_nature_lolf(bdsaid integer) is
    cpt       integer;
    tyetid    budget_saisie.tyet_id%type;
    exeordre  budget_saisie.exe_ordre%type;
    bparvalue budget_parametres.bpar_value%type;
  begin
      select count(*) into cpt from budget_saisie where bdsa_id=bdsaid;
      if cpt=0 then
          RAISE_APPLICATION_ERROR(-20001,'Initialisation impossible, ce budget_saisie n''existe pas  ('||bdsaid||')');
      end if;

      select tyet_id, exe_ordre into tyetid, exeordre from budget_saisie where bdsa_id=bdsaid;
      if tyetid<>budget_etat.get_typeencours then
          RAISE_APPLICATION_ERROR(-20001,'Initialisation impossible, ce budget_saisie n''est pas en cours de saisie');
      end if;

      select count(*) into cpt from budget_parametres where exe_ordre=exeordre and bpar_key='REPART_NATURE_LOLF';
      if cpt=0 then return; end if;
      if cpt<>1 then
          RAISE_APPLICATION_ERROR(-20001,'Initialisation impossible, plusieurs parametres REPART_NATURE_LOLF trouves pour l''exercice '||exeordre);
      end if;

      select bpar_value into bparvalue from budget_parametres where exe_ordre=exeordre and bpar_key='REPART_NATURE_LOLF';
      if bparvalue<>'OUI' then return; end if;

      select count(*) into cpt from budget_saisie_nature where /*bdsn_saisi<>0 and*/ tyet_id<>budget_etat.get_typecloture and bdsa_id=bdsaid;
      if cpt>0 then
          RAISE_APPLICATION_ERROR(-20001,'Initialisation impossible, il y a au moins un budget par nature en cours de saisie');
      end if;

      select count(*) into cpt from budget_saisie_gestion where /*bdsg_saisi<>0 and*/ tyet_id<>budget_etat.get_typecloture and bdsa_id=bdsaid;
      if cpt>0 then
          RAISE_APPLICATION_ERROR(-20001,'Initialisation impossible, il y a au moins un budget de gestion en cours de saisie');
      end if;

      select count(*) into cpt from budget_saisie_nature_lolf where bdsa_id=bdsaid;
      if cpt>0 then
          RAISE_APPLICATION_ERROR(-20001,'L''initialisation a deja ete effectuee');
      end if;

      insert into budget_saisie_nature_lolf
         select budget_saisie_nature_lolf_seq.nextval, s.bdsa_id, o.org_id, s.tcd_ordre, s.pco_num, s.tyac_id, 0, 0, 0, s.exe_ordre, budget_etat.get_typeencours
         from (
            select distinct n.bdsa_id bdsa_id, n.tcd_ordre tcd_ordre, nvl(m.pco_num_vote, n.pco_num) pco_num, g.tyac_id tyac_id, n.exe_ordre exe_ordre
            from budget_saisie_nature n, budget_saisie_gestion g, budget_masque_nature m
            where n.bdsa_id=g.bdsa_id and n.bdsa_id=bdsaid and n.tcd_ordre=g.tcd_ordre and n.bdsn_saisi<>0 and g.bdsg_saisi<>0
              and n.pco_num=m.pco_num(+) and n.tcd_ordre=m.tcd_ordre(+) and n.exe_ordre=m.exe_ordre(+)
         ) s, jefy_admin.organ o where o.org_niv=0;

  end;

END;
/
