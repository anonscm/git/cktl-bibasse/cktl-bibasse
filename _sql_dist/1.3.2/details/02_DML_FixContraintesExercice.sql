-- Fichier :  no 1/1
-- Schema modifie :  JEFY_BUDGET
-- Schema d'execution du script : GRHUM
-- Licence : CeCILL version 2
--------------------------------------------------------
--  EXPLICATIONS :
--  Maracuja a evolué pour intégrer un plan comptable par exercice (table PLAN_COMPTABLE_EXER)
--  Les applications ont été modifiées en conséquence : elles utilisent désormais cette nouvelle table.
--  Par contre les contraintes d'intégrité dans la base de donnée ne sont pas à jour et continuent de référencer la table MARACUJA.PLAN_COMPATBLE.
--  Les requêtes ci-dessous corrigent ce problème en réalisant les opérations suivantes :
--    1) On ajoute automatiquement au plan comptable par exercice (maracuja.plan_comptable_exer)
--       les numéros de compte listés dans les tables de jefy_budget mais manquantes au niveau du plan comptable par exercice.
--       Ces nouveaux comptes sont par défaut mis en 'ANNULE' et le reste de leurs informations provient de la table maracuja.plan_comptable.
--
--    2) Dans un second temps, les contraintes d'intégrité sont supprimées puis re-créées pour utiliser le plan comptable et l'exercice.
--------------------------------------------------------

SET define OFF
SET scan OFF

--------------------------------------------------------
--  TABLE BUDGET_VOTE_NATURE
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement,'ANNULE',pco_j_exercice,pco_j_fin_exercice , pco_j_be , pco_compte_be,pco_sens_solde
    from jefy_budget.budget_vote_nature t
   inner join maracuja.plan_comptable pco on (pco.pco_num=t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_vote_nature
       DROP
 CONSTRAINT FK_BDVN_PLANCO;

ALTER TABLE jefy_budget.budget_vote_nature
        ADD
 CONSTRAINT FK_BDVN_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

 --------------------------------------------------------
--  TABLE BUDGET_MASQUE_NATURE (PCO_NUM)
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement,'ANNULE',pco_j_exercice,pco_j_fin_exercice , pco_j_be , pco_compte_be,pco_sens_solde
    from jefy_budget.budget_masque_nature t
   inner join maracuja.plan_comptable pco on (pco.pco_num=t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_masque_nature
       DROP
 CONSTRAINT FK_BMN_PLANCO;

ALTER TABLE jefy_budget.budget_masque_nature
        ADD
 CONSTRAINT FK_BMN_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_MASQUE_NATURE (PCO_NUM_VOTE)
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num_vote, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement,'ANNULE',pco_j_exercice,pco_j_fin_exercice , pco_j_be , pco_compte_be,pco_sens_solde
    from jefy_budget.budget_masque_nature t
   inner join maracuja.plan_comptable pco on (pco.pco_num=t.pco_num_vote)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num_vote and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_masque_nature
       DROP
 CONSTRAINT FK_BMN_PCO_VOTE;

ALTER TABLE jefy_budget.budget_masque_nature
        ADD
 CONSTRAINT FK_BMN_PCO_VOTE
FOREIGN KEY (pco_num_vote, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_MOUVEMENTS_NATURE
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_mouvements_nature t
   inner join maracuja.plan_comptable pco on (pco.pco_num=t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_mouvements_nature
       DROP
 CONSTRAINT FK_BDMN_PLANCO;

ALTER TABLE jefy_budget.budget_mouvements_nature
        ADD
 CONSTRAINT FK_BDMN_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_SAISIE_NATURE
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_saisie_nature t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_saisie_nature
       DROP
 CONSTRAINT FK_BDSN_PLANCO;

ALTER TABLE jefy_budget.budget_saisie_nature
        ADD
 CONSTRAINT FK_BDSN_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_SAISIE_NATURE_LOLF
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_saisie_nature_lolf t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_saisie_nature_lolf
       DROP
 CONSTRAINT FK_BDSL_PLANCO;

ALTER TABLE jefy_budget.budget_saisie_nature_lolf
        ADD
 CONSTRAINT FK_BDSL_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_VOTE_CHAPITRE_CTRL
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_vote_chapitre_ctrl t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_vote_chapitre_ctrl
       DROP
 CONSTRAINT FK_BVCC_PLANCO;

ALTER TABLE jefy_budget.budget_vote_chapitre_ctrl
        ADD
 CONSTRAINT FK_BVCC_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE PROPOSITION_BUDG_NAT_LOLF
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.proposition_budg_nat_lolf t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.proposition_budg_nat_lolf
       DROP
 CONSTRAINT FK_PROP_BUD_NATL_PCO_NUM;

ALTER TABLE jefy_budget.proposition_budg_nat_lolf
        ADD
 CONSTRAINT FK_PROP_BUD_NATL_PCO_NUM
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE PREVISION_BUDG_NAT_LOLF
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.prevision_budg_nat_lolf t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.prevision_budg_nat_lolf
       DROP
 CONSTRAINT FK_PREV_BUD_NATL_PCO_NUM;

ALTER TABLE jefy_budget.prevision_budg_nat_lolf
        ADD
 CONSTRAINT FK_PREV_BUD_NATL_PCO_NUM
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_VOTE_NATURE_SAISIE
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_vote_nature_saisie t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_vote_nature_saisie
       DROP
 CONSTRAINT FK_BVNS_PLANCO;

ALTER TABLE jefy_budget.budget_vote_nature_saisie
        ADD
 CONSTRAINT FK_BVNS_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_CALCUL_NATURE
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_calcul_nature t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_calcul_nature
       DROP
 CONSTRAINT FK_BDCN_PLANCO;

ALTER TABLE jefy_budget.budget_calcul_nature
        ADD
 CONSTRAINT FK_BDCN_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

--------------------------------------------------------
--  TABLE BUDGET_VOTE_NATURE_LOLF
--------------------------------------------------------
insert into maracuja.plan_comptable_exer (pcoe_id, exe_ordre, pco_num, pco_niveau, pco_budgetaire, pco_emargement, pco_libelle, pco_nature, pco_sens_emargement, pco_validite, pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde)
  select maracuja.plan_comptable_exer_seq.nextval, t.exe_ordre, t.pco_num, pco_niveau, pco_budgetaire, pco_emargement,pco_libelle,
         pco_nature, pco_sens_emargement, 'ANNULE', pco_j_exercice, pco_j_fin_exercice, pco_j_be, pco_compte_be, pco_sens_solde
    from jefy_budget.budget_vote_nature_lolf t
   inner join maracuja.plan_comptable pco on (pco.pco_num = t.pco_num)
   where not exists (select 1 from maracuja.plan_comptable_exer pco where pco.pco_num = t.pco_num and pco.exe_ordre = t.exe_ordre);

ALTER TABLE jefy_budget.budget_vote_nature_lolf
       DROP
 CONSTRAINT FK_BDVL_PLANCO;

ALTER TABLE jefy_budget.budget_vote_nature_lolf
        ADD
 CONSTRAINT FK_BDVL_PLANCO
FOREIGN KEY (pco_num, exe_ordre)
 REFERENCES maracuja.plan_comptable_exer (pco_num, exe_ordre);

