SET define OFF
SET scan OFF

-- Maj version
insert into jefy_budget.db_version values ('1.3.2.0', to_date('06/06/2013','dd/mm/yyyy'), sysdate, 'Extractions Cofisup Budget', 1320);
commit;
