SET define OFF
SET scan OFF

--------------------------------------------------------
--  DDL for View V_LOLF_DEP_HIERARCHIQUE_ACTION
--------------------------------------------------------
CREATE OR REPLACE FORCE VIEW "JEFY_BUDGET"."V_LOLF_DEP_HIERARCHIQUE_ACTION"
	("EXE_ORDRE", "TYAC_ID", "TYAC_CODE", "TYET_ID", "TYAC_ROOT_CODE")
AS

	SELECT lolfDep.exe_ordre, lolfHieararchie.lolf_id, lolfHieararchie.lolf_code, lolfHieararchie.tyet_id, lolfHieararchie.rootCode
	  FROM (SELECT lolf_id, lolf_code, tyet_id, replace( sys_connect_by_path( decode(level, 1, lolf_code), '~'), '~') as rootCode
              FROM jefy_admin.lolf_nomenclature_depense
             START WITH lolf_pere in ( SELECT lolf_id FROM jefy_admin.lolf_nomenclature_depense WHERE lolf_niveau = 0 )
           CONNECT BY prior lolf_id = lolf_pere
             ORDER BY lolf_id) lolfHieararchie
      JOIN jefy_budget.v_lolf_nomenclature_bud_dep lolfDep on lolfHieararchie.lolf_id = lolfDep.lolf_id;

