SET define OFF

grant select on jefy_admin.parametre to jefy_budget;
grant select on jefy_admin.nomenclature_etat_credit to jefy_budget;
grant select on jefy_admin.nomenclature_prevision_recette to jefy_budget;
grant select on jefy_admin.nomenclature_lolf_dest_ref to jefy_budget;
grant select on jefy_admin.organ_nature_budget to jefy_budget;
grant select on jefy_admin.type_nature_budget to jefy_budget;

grant select on maracuja.titre to jefy_budget;
grant select on maracuja.v_planco_chapitre to jefy_budget;

grant select on jefy_recette.facture to jefy_budget;
grant select on jefy_recette.facture to jefy_budget;
grant select on jefy_recette.recette to jefy_budget;
grant select on jefy_recette.recette_ctrl_planco to jefy_budget;

grant select on jefy_depense.depense_ctrl_action to jefy_budget;

