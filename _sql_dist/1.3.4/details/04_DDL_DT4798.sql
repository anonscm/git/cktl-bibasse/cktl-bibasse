--------------------------------------------------------
--  DDL for Function GET_PARAMETRE
--------------------------------------------------------
CREATE OR REPLACE FUNCTION "JEFY_BUDGET"."GET_PARAMETRE" (
		a_exe_ordre  jefy_budget.budget_parametres.exe_ordre%type,
		a_par_key    jefy_budget.budget_parametres.bpar_key%type)
	RETURN jefy_budget.budget_parametres.bpar_value%type
IS
 	my_nb        integer;
  	my_par_value jefy_budget.budget_parametres.bpar_value%type;
BEGIN

	select count(*) into my_nb from budget_parametres where bpar_key = a_par_key and exe_ordre = a_exe_ordre;
	if my_nb = 1 then
		select bpar_value into my_par_value from budget_parametres where bpar_key = a_par_key and exe_ordre = a_exe_ordre;
		return my_par_value;
	end if;

	return null;
END;
/
