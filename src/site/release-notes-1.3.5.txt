Bibasse Release Notes - 1.3.5
=============================

-DG #5271 : [DT5953] Faute d'orthographe sur message d'erreur
-DG #5586 : Cofisup / Noms fichiers
-DG #5621 : Orthographe incorrecte de "fonds"

Cette version inclut également la génération simplifiée d'un JNLP. Vous trouverez un formulaire en vous rendant sur l'URL du type :
http:// VOTRESERVEUR/cgi-bin/WebObjects/Bibasse.woa/wa/jnlp

Vous pourrez renseigner l'URL de Bibasse sur le serveur web ainsi que sur le serveur d'application, et générer un JNLP à partir de ces informations.