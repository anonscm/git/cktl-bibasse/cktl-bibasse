package org.cocktail.bibasse.client.cofisup;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.cocktail.common.formule.Compte;
import org.cocktail.common.formule.ElmtNomenclatureContext;
import org.cocktail.common.formule.IElementNomenclature;
import org.cocktail.common.formule.Rubrique;
import org.junit.Test;

public class ElmtNomenclatureContextTest {

	@Test
	public void montantDunCompteEgaleSommeDeSesSousComptesEtDeLuiMeme() throws Exception {
		ElmtNomenclatureContext context = new ElmtNomenclatureContext();
		IElementNomenclature compte321 = new Compte("c321", 60);
		IElementNomenclature compte3211 = new Compte("C3211", 80);
		IElementNomenclature compte6451 = new Compte("C6451", 50);
		IElementNomenclature compte6452 = new Compte("c6452", 100);
		context.put(compte321.getCode(), compte321);
		context.put(compte3211.getCode(), compte3211);

		context.put(compte6451.getCode(), compte6451);
		context.put(compte6452.getCode(), compte6452);

		assertEquals(BigDecimal.valueOf(150.0), context.resolve("c645"));
		assertEquals(BigDecimal.valueOf(140.0), context.resolve("c321"));
		assertEquals(BigDecimal.valueOf(50.0), context.resolve("c6451"));
		assertEquals(BigDecimal.valueOf(0.0), context.resolve("c5212"));
	}

	@Test
	public void montantDunCompteEgaleSommeDeSesSousComptesQdLeCompteNexistePasDsLeContexte() throws Exception {
		ElmtNomenclatureContext context = new ElmtNomenclatureContext();
		IElementNomenclature compte321 = new Compte("c321", 60);
		IElementNomenclature compte3211 = new Compte("C3221", 80);
		context.put(compte321.getCode(), compte321);
		context.put(compte3211.getCode(), compte3211);

		assertEquals(BigDecimal.valueOf(140.0), context.resolve("C32"));
	}

	@Test
	public void montantDuneRubriqueEgaleRubriqueCible() throws Exception {
		ElmtNomenclatureContext context = new ElmtNomenclatureContext();

		IElementNomenclature compte6451 = new Compte("c6451", 50);
		IElementNomenclature compte6452 = new Compte("c6452", 80);
		IElementNomenclature rubrique125 = new Rubrique("MS125", "");
		IElementNomenclature rubrique126 = new Rubrique("MS126", "c6451");
		IElementNomenclature rubrique127 = new Rubrique("MS127", "c6452");
		IElementNomenclature rubrique128 = new Rubrique("MS128", "MS127");

		context.put(compte6451.getCode(), compte6451);
		context.put(compte6452.getCode(), compte6452);
		context.put(rubrique125.getCode(), rubrique125);
		context.put(rubrique126.getCode(), rubrique126);
		context.put(rubrique127.getCode(), rubrique127);
		context.put(rubrique128.getCode(), rubrique128);

		//Plante car vide
		assertEquals(BigDecimal.ZERO, rubrique125.getMontant());
		assertEquals(BigDecimal.valueOf(50.0), rubrique126.getMontant());
		assertEquals(BigDecimal.valueOf(80.0), rubrique127.getMontant());
		assertEquals(BigDecimal.valueOf(80.0), rubrique128.getMontant());
	}

	@Test
	public void testComptesImbriques() throws Exception {
		ElmtNomenclatureContext context = new ElmtNomenclatureContext();
		
		//  50 sur le 7512 et 200 sur le 666
		IElementNomenclature compte75 = new Compte("c75", 100);
		IElementNomenclature compte751 = new Compte("c751", 30);
		IElementNomenclature compte7512 = new Compte("c7512", 50);
		IElementNomenclature compte666 = new Compte("c666", 200);
		context.put(compte75.getCode(), compte75);
		context.put(compte751.getCode(), compte751);
		context.put(compte7512.getCode(), compte7512);
		context.put(compte666.getCode(), compte666);

		context.put("calculSomme", new Rubrique("calculSomme", "c75 + c666"));
		assertEquals(BigDecimal.valueOf(380.0), context.resolve("calculSomme"));
	}

}
