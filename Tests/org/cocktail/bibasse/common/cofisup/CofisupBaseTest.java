package org.cocktail.bibasse.common.cofisup;

import java.math.BigDecimal;

import org.cocktail.common.metier.Banque;

public class CofisupBaseTest {

    protected static final String XPF_DEVISE = "XPF";
    protected static final String EURO_DEVISE = "EUR";
    protected static final int EXERCICE = 2013;
    
    protected Banque createSimpleBanque() {
        Banque banque = new Banque();
        banque.ajouterTauxChange(EURO_DEVISE, EURO_DEVISE, EXERCICE, BigDecimal.ONE);
        banque.ajouterTauxChange(XPF_DEVISE, EURO_DEVISE, EXERCICE, BigDecimal.valueOf(0.00838d));
        return banque;
    }
    
}
