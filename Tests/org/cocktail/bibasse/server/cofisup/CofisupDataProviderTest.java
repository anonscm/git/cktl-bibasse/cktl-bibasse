package org.cocktail.bibasse.server.cofisup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.bibasse.server.cofisup.batch.data.rce.BudgetsDetailsRecettes;
import org.cocktail.common.cofisup.ETypeBudget;
import org.junit.Test;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXGenericRecord;

public class CofisupDataProviderTest {

	public static final String BIBASSE_MODEL_NAME = "Bibasse";

	@Test
	public void constructionParametresRequetesPourFichierRce() {
		Number exercice = Integer.valueOf(2011);
		List<Integer> organIds = Arrays.asList(1, 2);
		ETypeBudget typeBudget = ETypeBudget.BP;

		AbstractCofisupDataProvider provider = new BudgetsDetailsRecettes();
		Map<String, String> parametres = provider.buildBudgetsNatureQueryParameters(exercice, organIds, typeBudget);

		assertEquals(3, parametres.size());
		assertTrue(parametres.containsKey(AbstractCofisupDataProvider.VAR_EXERCICE));
		assertTrue(parametres.containsKey(AbstractCofisupDataProvider.VAR_ORG_IDS));
		assertTrue(parametres.containsKey(AbstractCofisupDataProvider.VAR_TYPE_MONTANT));
	}

	@Test
	public void recupererDonnees() throws Exception {
		CofisupDataProvider provider = new BudgetsDetailsRecettes();
		Number exercice = Integer.valueOf(2011);
		List<Integer> organIds = Arrays.asList(1, 2);
		ETypeBudget typeBudget = ETypeBudget.BP;

		NSArray mockResults = new NSArray(new Object[] {});

		Iterator<CofisupData> data = provider.getData(
				queryExecutor(mockResults, null), queryExecutorContext(exercice, organIds, typeBudget));

		assertNotNull(data);
	}

	@Test
	public void generationParametreOrganigrammes() throws Exception {
		AbstractCofisupDataProvider provider = new BudgetsDetailsRecettes();
		assertEquals("", provider.buildRestrictionOrganigrammes(null));
		assertEquals("1", provider.buildRestrictionOrganigrammes(Arrays.asList(1)));
		assertEquals("1,2,3", provider.buildRestrictionOrganigrammes(Arrays.asList(1, 2, 3)));
	}

	private QueryExecutor queryExecutor(NSArray results, ERXGenericRecord uniqueResult) {
		final NSArray immutableResults = new NSArray(results);
		final ERXGenericRecord immutableUniqueResult = uniqueResult;
		return new QueryExecutor() {
			public NSArray execute(String sqlQuery, NSArray sqlKeys) {
				return immutableResults;
			}

			public ERXGenericRecord executeUniqueResult(EOFetchSpecification fetchSpecification) {
				return immutableUniqueResult;
			}
		};
	}

	private QueryExecutorContext queryExecutorContext(Number exercice, List<Integer> organIds, ETypeBudget typeBudget) {
		QueryExecutorContext ctx = new QueryExecutorContext();
		ctx.setExercice(exercice);
		ctx.setOrganIds(organIds);
		ctx.setTypeBudget(typeBudget);
		return ctx;
	}
}
