package org.cocktail.bibasse.server.cofisup.batch;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.cocktail.bibasse.common.cofisup.CofisupBaseTest;
import org.cocktail.common.metier.Banque;
import org.junit.Test;

public class TableauFinancementBeanTest extends CofisupBaseTest {

    @Test
    public void testConvertToString() {
        String typeEnregistrement = "typeEnregistrement";
        BigDecimal mtCapaciteAutoFinancement = new BigDecimal("3265");
        String sensAutoFinancement = "C";
        BigDecimal mtRessources = new BigDecimal("3265.54");
        BigDecimal equilibreFondDeRoulement =  new BigDecimal("321654987.22");
        String sensEquilibreFondDeRoulement = "P";
        Number exercice = Integer.valueOf(2013);
        
        TableauFinancementBean bean = new TableauFinancementBean();
        bean.setTypeEnregistrement(typeEnregistrement);
        bean.setMtCapaciteAutoFinancement(mtCapaciteAutoFinancement);
        bean.setSensAutoFinancement(sensAutoFinancement);
        bean.setMtRessources(mtRessources);
        bean.setEquilibreFondDeRoulement(equilibreFondDeRoulement);
        bean.setSensEquilibreFondDeRoulement(sensEquilibreFondDeRoulement);
        bean.setExercice(exercice);
        
        String result = "typeEnregistrement;3265,00;C;3265,54;321654987,22;P;2013";
        assertEquals(result, bean.convertToString(Banque.EURO_DEVISE, createSimpleBanque()));
    }

}
