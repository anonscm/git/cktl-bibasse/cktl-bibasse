package org.cocktail.bibasse.server.cofisup.batch;

import static org.junit.Assert.*;

import org.cocktail.common.metier.Banque;
import org.junit.Before;
import org.junit.Test;

public class CofisupBeanTest {

    @Test
    public void testitemSeparator() {
        CofisupBean bean = createSimpleCofisupBean();
        assertEquals(";", bean.getItemSeparator());
        
        bean.setItemSeparator("|");
        assertEquals("|", bean.getItemSeparator());
    }

    @Test
    public void testAggregate() {
        CofisupBean bean = createSimpleCofisupBean();
        
        Object[] fields = new Object[] {"chps1", 12, 0.00838, "fin"};
        assertEquals("chps1;12;0.00838;fin", bean.aggregate(fields));
    }
    
    public CofisupBean createSimpleCofisupBean() {
        return new CofisupBean() {
            public String convertToString(String devise, Banque banque) {
                return null;
            }
        };
    }
}
