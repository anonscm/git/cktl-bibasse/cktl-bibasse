package org.cocktail.bibasse.server.cofisup.batch;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.cocktail.bibasse.common.cofisup.CofisupBaseTest;
import org.cocktail.bibasse.server.cofisup.batch.utils.CofisupUtils;
import org.cocktail.common.metier.Banque;
import org.junit.Before;
import org.junit.Test;

public class CompteResultatBeanTest extends CofisupBaseTest {

    @Test
    public void test() {
        String typeEnregistrement = "typeEnregistrement";
        BigDecimal mtResultatExploitation = BigDecimal.valueOf(12355.33d);
        String sensResultat = "D";
        BigDecimal mtEquilibreBudgetairePrevisionnel = BigDecimal.valueOf(354d);
        Integer exercice = 2013;
        
        CompteResultatBean bean = new CompteResultatBean();
        bean.setTypeEnregistrement(typeEnregistrement);
        bean.setMtResultatExploitation(mtResultatExploitation);
        bean.setSensResultat(sensResultat);
        bean.setMtEquilibreBudgetairePrevisionnel(mtEquilibreBudgetairePrevisionnel);
        bean.setExercice(exercice);
        
        String result = "typeEnregistrement;12355,33;D;354,00;2013"; 
        assertEquals(result, bean.convertToString(Banque.EURO_DEVISE, createSimpleBanque()));
    }
}
