package org.cocktail.bibasse.server.cofisup.batch.utils;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.cocktail.bibasse.server.utilities.DateUtils;
import org.cocktail.common.cofisup.ETypeBudget;
import org.cocktail.common.cofisup.ETypeFichier;
import org.cocktail.common.cofisup.ETypeNatureBudget;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DateUtils.class)
public class CofisupFilenameGeneratorTest {

	@Test
	public void testGenerateFilenameRecetteDepense() {
		PowerMockito.mockStatic(DateUtils.class);
		Calendar cal = Calendar.getInstance();
		cal.set(2013, 0, 10, 11, 30, 45);
		Mockito.when(DateUtils.now()).thenReturn(cal.getTime());

		assertEquals(
				"BP_0251215K_BP_RD_BP_2013_1_20130110-113045",
				CofisupFilenameGenerator.generateFilename(ETypeNatureBudget.PRINCIPAL, 1, ETypeFichier.RD, ETypeBudget.BP, Integer.valueOf(2013), "0251215K"));
	}

	@Test
	public void testGenerateFilenameInformationsComplementaires() {
		PowerMockito.mockStatic(DateUtils.class);
		Calendar cal = Calendar.getInstance();
		cal.set(2013, 0, 10, 11, 30, 45);
		Mockito.when(DateUtils.now()).thenReturn(cal.getTime());

		assertEquals(
				"BPI_0251215K_EB_IC_IUT_2012_2_TF_20130110-113045",
				CofisupFilenameGenerator.generateFilename(ETypeNatureBudget.BPI_IUT, 2, ETypeFichier.TF, ETypeBudget.EB, Integer.valueOf(2012), "0251215K"));
	}

}
