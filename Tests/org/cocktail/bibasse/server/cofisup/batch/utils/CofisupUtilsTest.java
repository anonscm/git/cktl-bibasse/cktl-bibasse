package org.cocktail.bibasse.server.cofisup.batch.utils;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.cocktail.bibasse.common.cofisup.CofisupBaseTest;
import org.cocktail.common.metier.Banque;
import org.cocktail.common.metier.Money;
import org.junit.Test;

public class CofisupUtilsTest extends CofisupBaseTest {

	@Test
	public void testFormatMontant() {
	    Banque banque = createSimpleBanque();
	    
		assertEquals("1000,00", CofisupUtils.formatMontant(banque, EURO_DEVISE, new BigDecimal("1000.0000"), EXERCICE));
		assertEquals("1000,55", CofisupUtils.formatMontant(banque, EURO_DEVISE, new BigDecimal("1000.5500"), EXERCICE));
		assertEquals("1000,56", CofisupUtils.formatMontant(banque, EURO_DEVISE, new BigDecimal("1000.5550"), EXERCICE));
		assertEquals("1000,55", CofisupUtils.formatMontant(banque, EURO_DEVISE, new BigDecimal("1000.5530"), EXERCICE));
		assertEquals("5600875,55", CofisupUtils.formatMontant(banque, EURO_DEVISE, new BigDecimal("5600875.5530"), EXERCICE));
        assertEquals("10,33", CofisupUtils.formatMontant(banque, XPF_DEVISE, new BigDecimal("1233"), EXERCICE));
	}
	
	@Test
	public void testMoneyFormatter() {
	    assertEquals(RoundingMode.HALF_UP, CofisupUtils.moneyFormatter().getRoundingMode());
	    assertEquals(new DecimalFormat("0.00"), CofisupUtils.moneyFormatter().getFormat());
	    assertEquals(2, CofisupUtils.moneyFormatter().getScale());
	}
	
	@Test
    public void testConvertEuroToEuro() {
	    Banque banque = createSimpleBanque();
	    BigDecimal mtEnEuro = BigDecimal.valueOf(1233.68d);
	    assertEquals(new Money(EURO_DEVISE, mtEnEuro), CofisupUtils.convertToEuro(banque, EURO_DEVISE, mtEnEuro, EXERCICE));
	}

	@Test
	public void testConvertXpfToEuro() {
	    Banque banque = createSimpleBanque();
	    BigDecimal mtEnXpf = BigDecimal.valueOf(1233);
	    BigDecimal mtEnEuro = BigDecimal.valueOf(10.33254d);
	    assertEquals(new Money(EURO_DEVISE, mtEnEuro), CofisupUtils.convertToEuro(banque, XPF_DEVISE, mtEnXpf, EXERCICE));
	}
}
