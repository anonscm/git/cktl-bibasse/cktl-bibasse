package org.cocktail.bibasse.server.cofisup.batch.writer;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.cocktail.bibasse.common.cofisup.CofisupBaseTest;
import org.cocktail.common.metier.Banque;
import org.junit.Test;

public class RecetteDepenseRceItemWriterTest extends CofisupBaseTest {

    @Test
    public void testArrondiMontantSansConversion() {
        Banque banque = createSimpleBanque();
        BigDecimal montantEnEuro = BigDecimal.valueOf(1549888554.15d);
        Integer exercice = 2013;
        
        RecetteDepenseRceItemWriter writer = new RecetteDepenseRceItemWriter();
        
        assertEquals(
            BigDecimal.valueOf(1549888554.15d), 
            writer.arrondiMontantSiConversionDeMontant(montantEnEuro, Banque.EURO_DEVISE, banque, exercice));
    }

    @Test
    public void testArrondiMontantAvecConversion() {
        Banque banque = createSimpleBanque();
        BigDecimal montantEnFrancsPacifique = BigDecimal.valueOf(1549888554.15d);
        Integer exercice = 2013;
        
        RecetteDepenseRceItemWriter writer = new RecetteDepenseRceItemWriter();
        
        assertEquals(
            BigDecimal.valueOf(12988066.08d),
            writer.arrondiMontantSiConversionDeMontant(montantEnFrancsPacifique, XPF_DEVISE, banque, exercice));
    }
    
}
