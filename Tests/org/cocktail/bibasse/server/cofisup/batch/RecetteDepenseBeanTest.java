package org.cocktail.bibasse.server.cofisup.batch;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.cocktail.bibasse.common.cofisup.CofisupBaseTest;
import org.cocktail.common.metier.Banque;
import org.junit.Test;

public class RecetteDepenseBeanTest extends CofisupBaseTest {

    @Test
    public void testConvertToString() {
        String typeEnregistrement = "typeEnregistrement";
        String sensCompte = "D";
        String type = "R";
        String code = "code";
        String codeDestination = "codeDest";
        BigDecimal montant = BigDecimal.valueOf(354.2d);
        String exercice = "2013";
        
        RecetteDepenseBean bean = new RecetteDepenseBean();
        bean.setTypeEnregistrement(typeEnregistrement);
        bean.setSensCompte(sensCompte);
        bean.setType(type);
        bean.setCode(code);
        bean.setCodeDestination(codeDestination);
        bean.setMontant(montant);
        bean.setExercice(exercice);
        
        String result = "typeEnregistrement;D;R;code;codeDest;354,20;2013";
        assertEquals(result, bean.convertToString(Banque.EURO_DEVISE, createSimpleBanque()));
    }

}
