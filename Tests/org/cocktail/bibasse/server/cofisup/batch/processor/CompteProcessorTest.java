package org.cocktail.bibasse.server.cofisup.batch.processor;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.math.BigDecimal;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.cocktail.common.formule.Compte;
import org.junit.Test;

public class CompteProcessorTest {

	@Test
	public void testProcess() throws Exception {
		String compte = "6412";
		String expectedCompte = "c" + compte;
		double mtPropre = 125.368d;
		CofisupData item = mockCofisupData(compte, mtPropre);

		CompteProcessor processorUnderTest = new CompteProcessor();
		Compte compteResult = processorUnderTest.process(item);
		assertNotNull(compteResult);
		assertEquals(expectedCompte, compteResult.getCode());
		assertEquals(BigDecimal.valueOf(mtPropre), compteResult.getMontantPropre());
	}

	protected CofisupData mockCofisupData(String compte, double montant) {
		CofisupData cofisupData = new CofisupData();
		cofisupData.add(CofisupData.KEY_COMPTE, compte);
		cofisupData.add(CofisupData.KEY_MONTANT, montant);
		return cofisupData;
	}

}
