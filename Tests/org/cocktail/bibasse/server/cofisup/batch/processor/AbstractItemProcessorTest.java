package org.cocktail.bibasse.server.cofisup.batch.processor;

import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;

public class AbstractItemProcessorTest {

	public CofisupData mockCompteResultatData(int exercice, double charges, double produits) {
		CofisupData cofisupData = new CofisupData();
		cofisupData.add(CofisupData.KEY_EXERCICE, exercice);
		cofisupData.add(CofisupData.KEY_MONTANT_CHARGES, charges);
		cofisupData.add(CofisupData.KEY_MONTANT_PRODUITS, produits);
		return cofisupData;
	}


	public CofisupData mockTableauFinancementData(int exercice, double charges, double produits,
			double chargesNonDecaissablesEtActifsCedes, double produitsNonEncaissablesEtCessionActifsEtQuoteSub,
			double chargesSection2, double produitsSection2, double mtCompte775) {
		CofisupData data = new CofisupData();
		data.add(CofisupData.KEY_EXERCICE, exercice);
		data.add(CofisupData.KEY_MONTANT_CHARGES, charges);
		data.add(CofisupData.KEY_MONTANT_PRODUITS, produits);
		data.add(CofisupData.KEY_MONTANT_CHARGES_NON_DECAISSABLES_ET_ACTIFS_CEDES, chargesNonDecaissablesEtActifsCedes);
		data.add(CofisupData.KEY_MONTANT_PRODUITS_NON_ENCAISSABLES_ET_CESSION_ACTIFS_ET_QUOTEPART_SUBVENTION,
				produitsNonEncaissablesEtCessionActifsEtQuoteSub);
		data.add(CofisupData.KEY_MONTANT_CHARGES_SECTION_2, chargesSection2);
		data.add(CofisupData.KEY_MONTANT_PRODUITS_SECTION_2, produitsSection2);
		data.add(CofisupData.KEY_MONTANT_COMPTE_775, mtCompte775);

		return data;
	}

	public CofisupData mockBPIData(int exercice, double ressPropres, double droitsDep, double charges, double produits,
			double chargesNonDecaissablesEtActifsCedes, double produitsNonEncaissablesEtCessionActifsEtQuoteSub,
			double chargesSection2, double produitsSection2, double mtCompte775) {
		CofisupData data = new CofisupData();
		data.add(CofisupData.KEY_EXERCICE, exercice);
		data.add(CofisupData.KEY_MONTANT_RESSOURCES_PROPRES, ressPropres);
		data.add(CofisupData.KEY_MONTANT_DROITS_DEPENSER, droitsDep);

		data = data.add(mockCompteResultatData(exercice, charges, produits));
		data = data.add(mockTableauFinancementData(exercice, charges, produits, chargesNonDecaissablesEtActifsCedes,
				produitsNonEncaissablesEtCessionActifsEtQuoteSub, chargesSection2, produitsSection2, mtCompte775));

		return data;
	}
}
