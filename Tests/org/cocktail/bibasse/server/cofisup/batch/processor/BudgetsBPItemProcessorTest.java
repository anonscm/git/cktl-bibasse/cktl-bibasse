package org.cocktail.bibasse.server.cofisup.batch.processor;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.BudgetPropreIntegreBean;
import org.cocktail.bibasse.server.cofisup.batch.CofisupBean;
import org.junit.Test;

public class BudgetsBPItemProcessorTest extends AbstractItemProcessorTest {

	@Test
	public void testProcess() throws Exception {
		final int EXERCICE = 2012;
		double ressPropres = 122.33d;
		double produits = 108.87d;
		double produitsSection2 = 13.11d;
		double mtCompte775 = 4.74d;
		double produitsNonEncaissablesEtCessionActifsEtQuoteSub = 55.1d;

		double droitsDep = 130.44d;
		double charges = 100.16d;
		double chargesSection2 = 21d;
		double chargesNonDecaissablesEtActifsCedes = 68d;

		BigDecimal RESSOURCES_LIEES_ACTIVITE = BigDecimal.valueOf(148.33d);

		BudgetsBPItemProcessor processorUnderTest = new BudgetsBPItemProcessor();
		BudgetPropreIntegreBean beanResult = processorUnderTest.process(
			mockBPIData(EXERCICE, ressPropres, droitsDep, charges, produits, chargesNonDecaissablesEtActifsCedes,
					produitsNonEncaissablesEtCessionActifsEtQuoteSub, chargesSection2, produitsSection2, mtCompte775));

		assertNotNull(beanResult);
		assertEquals(CofisupBean.ENREGISTREMENT_DETAILS, beanResult.getTypeEnregistrement());
		assertEquals("", RESSOURCES_LIEES_ACTIVITE, beanResult.getRessourcesLieesActivite());
		assertEquals(BigDecimal.valueOf(-8.11d), beanResult.getContributionEtablissement());
		assertEquals(EXERCICE, beanResult.getExercice());
	}
}
