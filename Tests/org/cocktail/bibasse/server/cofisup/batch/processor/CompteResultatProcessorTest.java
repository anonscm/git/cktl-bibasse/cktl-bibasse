package org.cocktail.bibasse.server.cofisup.batch.processor;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.CompteResultatBean;
import org.cocktail.bibasse.server.cofisup.batch.data.CofisupData;
import org.junit.Test;

public class CompteResultatProcessorTest extends AbstractItemProcessorTest {

	@Test
	public void testProcessBenefice() throws Exception {
		int exercice = 2012;
		double mtCharges = 125.5d;
		double mtProduits = 172.68d;
		CofisupData item = mockCompteResultatData(exercice, mtCharges, mtProduits);

		CompteResultatProcessor processorUnderTest = new CompteResultatProcessor();
		CompteResultatBean beanResult = processorUnderTest.process(item);
		assertNotNull(beanResult);
		assertEquals("D", beanResult.getTypeEnregistrement());
		assertEquals(BigDecimal.valueOf(47.18d), beanResult.getMtResultatExploitation());
		assertEquals("B", beanResult.getSensResultat());
		assertEquals(BigDecimal.valueOf(172.68d), beanResult.getMtEquilibreBudgetairePrevisionnel());
		assertEquals(Integer.valueOf(2012), beanResult.getExercice());
	}

	@Test
	public void testProcessPerte() throws Exception {
		int exercice = 2012;
		double mtCharges = 199.51d;
		double mtProduits = 172.68d;
		CofisupData item = mockCompteResultatData(exercice, mtCharges, mtProduits);

		CompteResultatProcessor processorUnderTest = new CompteResultatProcessor();
		CompteResultatBean beanResult = processorUnderTest.process(item);
		assertNotNull(beanResult);
		assertEquals("D", beanResult.getTypeEnregistrement());
		assertEquals(BigDecimal.valueOf(26.83d), beanResult.getMtResultatExploitation());
		assertEquals("P", beanResult.getSensResultat());
		assertEquals(BigDecimal.valueOf(199.51d), beanResult.getMtEquilibreBudgetairePrevisionnel());
		assertEquals(Integer.valueOf(2012), beanResult.getExercice());
	}
}
