package org.cocktail.bibasse.server.cofisup.batch.processor;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

public class SectionTypeCreditProcessorTest {

	@Test
	public void testProcess() throws Exception {
		SectionTypeCreditProcessor processorUnderTest = new SectionTypeCreditProcessor();
		assertEquals("F", processorUnderTest.process("1"));
		assertEquals("I", processorUnderTest.process("2"));
		assertEquals("P", processorUnderTest.process("3"));
		assertEquals("", processorUnderTest.process("4"));
	}

}
