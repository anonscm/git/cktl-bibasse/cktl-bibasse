package org.cocktail.bibasse.server.cofisup.batch.processor;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.math.BigDecimal;

import org.cocktail.bibasse.server.cofisup.batch.TableauFinancementBean;
import org.junit.Test;

public class TableauFinancementProcessorTest extends AbstractItemProcessorTest {

	@Test
	public void testProcess() throws Exception {
		int exercice = 2012;
		double produits = 108.87d;
		double produitsSection2 = 13.11d;
		double mtCompte775 = 4.74d;
		double produitsNonEncaissablesEtCessionActifsEtQuoteSub = 55.1d;
		double charges = 100.16d;
		double chargesSection2 = 21d;
		double chargesNonDecaissablesEtActifsCedes = 68d;

		TableauFinancementProcessor processorUnderTest = new TableauFinancementProcessor();
		TableauFinancementBean beanResult = processorUnderTest.process(mockTableauFinancementData(
				exercice, charges, produits, chargesNonDecaissablesEtActifsCedes,
				produitsNonEncaissablesEtCessionActifsEtQuoteSub, chargesSection2, produitsSection2, mtCompte775));

		assertNotNull(beanResult);
		assertEquals("D", beanResult.getTypeEnregistrement());
		assertEquals(BigDecimal.valueOf(21.61d), beanResult.getMtCapaciteAutoFinancement());
		assertEquals("C", beanResult.getSensAutoFinancement());
		assertEquals(BigDecimal.valueOf(39.46d), beanResult.getMtRessources());
		assertEquals(BigDecimal.valueOf(18.46d), beanResult.getEquilibreFondDeRoulement());
		assertEquals("A", beanResult.getSensEquilibreFondDeRoulement());
		assertEquals(Integer.valueOf(2012), beanResult.getExercice());
	}

}
