package org.cocktail.common.metier;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class BanqueTest {

    private static final double TAUX_CHANGE_EUR_GBP_2014 = 0.825165d;
    private static final double TAUX_CHANGE_EUR_GBP_2013 = 0.71d;

    @Test
    public void testBanque() {
        Banque banque = new Banque();
        assertNull(banque.recupererTauxChange("EUR", "GBP", 2014));
        
        banque.ajouterTauxChange("EUR", "GBP", Integer.valueOf(2014), BigDecimal.valueOf(TAUX_CHANGE_EUR_GBP_2014));
        assertEquals(BigDecimal.valueOf(TAUX_CHANGE_EUR_GBP_2014), banque.recupererTauxChange("EUR", "GBP", 2014));
        
        banque.ajouterTauxChange("EUR", "GBP", Integer.valueOf(2013), BigDecimal.valueOf(TAUX_CHANGE_EUR_GBP_2013));
        assertEquals(BigDecimal.valueOf(TAUX_CHANGE_EUR_GBP_2013), banque.recupererTauxChange("EUR", "GBP", 2013));
    }
    
    @Test
    public void testRecupererTauxChangeSiMonnaieIdentique() {
        Banque banque = new Banque();
        banque.ajouterTauxChange("EUR", "EUR", Integer.valueOf(2014), BigDecimal.valueOf(0.325655d));
        assertEquals(BigDecimal.ONE, banque.recupererTauxChange("EUR", "EUR", 2014));
        assertEquals(BigDecimal.ONE, banque.recupererTauxChange("EUR", "EUR", 2013));
    }

}
