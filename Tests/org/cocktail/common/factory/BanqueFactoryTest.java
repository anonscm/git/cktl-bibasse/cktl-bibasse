package org.cocktail.common.factory;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class BanqueFactoryTest {

    private static final String CODE = "code";
    private static final Integer EXERCICE = Integer.valueOf(2013);
    
    @Test
    public void testConversionTauxEnBigDecimal() {
        assertEquals(BigDecimal.valueOf(1.0d), tauxInfosDevisesAsNumber("1"));
        assertEquals(BigDecimal.valueOf(1.12d), tauxInfosDevisesAsNumber("1.12"));
        assertEquals(BigDecimal.valueOf(1.12d), tauxInfosDevisesAsNumber("1,12"));
        assertEquals(BigDecimal.valueOf(0.00838d), tauxInfosDevisesAsNumber("0.00838"));
        assertEquals(BigDecimal.valueOf(0.00838d), tauxInfosDevisesAsNumber("0,008 38"));
    }

    private BigDecimal tauxInfosDevisesAsNumber(String taux) {
        return new BanqueFactory.InfosDeviseParametree(CODE, taux, EXERCICE).tauxAsNumber();
    }
    
}
