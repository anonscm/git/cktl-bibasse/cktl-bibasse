-- variables "communes": exercice, bdsa_id
-- variables au choix du menu : ORG_etab, org_UB, org_cr

select b.EXE_ORDRE, b.BDSA_ID, BDSA_LIBELLE, b.ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB,
b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TYAC_LIBELLE, TCD_CODE, BDSG_MONTANT
from v_budget_saisie_gestion_rec b, v_organ o, v_type_action_budget a,
budget_saisie bs, jefy_admin.type_credit c
where b.org_id = o.org_id
and b.tyac_id = a.tyac_id and b.exe_ordre = a.exe_ordre
and b.bdsa_id = bs.bdsa_id
and b.tcd_ordre = c.tcd_ordre and b.exe_ordre = c.exe_ordre
and bdsg_montant <> 0
and b.exe_ordre = v_exe_ordre and b.bdsa_id = v_bdsa_id
and ********* choix impression dans organigramme budg *********
order by b.exe_ordre, ORG_ETAB, ORG_UB, ORG_CR, b.TYAC_TYPE, TYAC_DECAISS, b.TYAC_PROG, TYAC_CODE, TCD_CODE


