-- select pour un exercice
-- variable : exercice, org_UB
********** A pr�voir de date � date *****************

select m.exe_ordre, bmou_id, bmou_libelle, bmou_creation, bm_montant, m.org_id, tcd_code, tyse_id,
org_ub||' '||org_cr||' '||org_souscr as ligne_budg, org_lib
from v_budget_ventilations m, v_organ o
where m.org_id = o.org_id
and m.exe_ordre = v_exercice
and org_ub = v_orgub
order by m.exe_ordre, bmou_creation, bmou_id, tyse_id, bm_montant, m.org_id, tcd_code

